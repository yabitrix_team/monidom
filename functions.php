<?php

use yii\helpers\FileHelper;

/**
 * Функция для логирования ошибок, с учетом передаваемых email
 *
 * @param        $msg   - сообщение об ошибке
 * @param string $file  - файл вызова
 * @param int    $line  - строка вызова
 * @param null   $email - адресса получателей писем об ошибках
 *
 * @return bool - вернет истину если отправка email удалась иначе ложь
 * @throws \yii\base\Exception
 */
function carLog( $msg, $file = __FILE__, $line = __LINE__, $email = null ) {

//	$email = isset( $email ) && filter_var( $email, FILTER_VALIDATE_EMAIL ) ? $email :
//		Yii::$app->params['core']['email']['admin'];
//	if ( $email ) {
//		$msg        = <<<TXT
//В Личном Кабинете Партнера произошла ошибка
//Содержание ошибки: $msg
//Файл: $file
//Линия: $line
//TXT;
//		$pathCarlog = rtrim( Yii::getAlias( '@app' ), '/' ) . "/runtime/carlog/";
//		if ( ! is_dir( $pathCarlog ) ) {
//			FileHelper::createDirectory( $pathCarlog );
//		}
//		// вычисление хэша сообщения - выявление уникальных сообщений (+вырезаем все цифры)
//		$hash = md5( preg_replace( "/\d/s", "", $msg ) );
//		// отправка однотипных сообщений каждые 5 минут
//		if (
//			! file_exists( $pathCarlog . $hash )
//			||
//			(
//				( $time = file_get_contents( $pathCarlog . $hash ) )
//				&& (int) $time + 5 * 60 < time()
//			) ) {
//			file_put_contents( $pathCarlog . $hash, time() );
//			 @error_log( $msg, 1, $email );
//		}
//	}
}

/**
 * Функция генерации уникального GUID
 *
 * @param bool $trim не оборачивать результат в фигурные скобки
 *
 * @return string GUID
 */
function GUIDv4( $trim = true ) {

	// Windows
	if ( function_exists( 'com_create_guid' ) === true ) {
		if ( $trim === true ) {
			return trim( com_create_guid(), '{}' );
		} else {
			return com_create_guid();
		}
	}
// OSX/Linux
	if ( function_exists( 'openssl_random_pseudo_bytes' ) === true ) {
		$data    = openssl_random_pseudo_bytes( 16 );
		$data[6] = chr( ord( $data[6] ) & 0x0f | 0x40 );    // set version to 0100
		$data[8] = chr( ord( $data[8] ) & 0x3f | 0x80 );    // set bits 6-7 to 10

		return vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split( bin2hex( $data ), 4 ) );
	}
// Fallback (PHP 4.2+)
	mt_srand( (double) microtime() * 10000 );
	$charid = strtolower( md5( uniqid( rand(), true ) ) );
	$hyphen = chr( 45 );                  // "-"
	$lbrace = $trim ? "" : chr( 123 );    // "{"
	$rbrace = $trim ? "" : chr( 125 );    // "}"
	$guidv4 = $lbrace .
	          substr( $charid, 0, 8 ) . $hyphen .
	          substr( $charid, 8, 4 ) . $hyphen .
	          substr( $charid, 12, 4 ) . $hyphen .
	          substr( $charid, 16, 4 ) . $hyphen .
	          substr( $charid, 20, 12 ) .
	          $rbrace;

	return $guidv4;
}

function p($v)
{
	print_r($v);die();
}