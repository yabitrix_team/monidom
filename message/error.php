<?php
//общие ошибки в виде код=>сообщение, передаем в AppResponse::get(), c 1 по 100 коды зарезервированы
return [
    205  => 'Некорректно заполнены поля',
    //работа с смс
    820  => 'Ошибка на стороне сервера, не указаны или указаны некорректно параметры для проверки СМС кода',
    822  => 'Не переданы данные для отправки СМС кода',
    824  => 'Не переданы данные для проверки СМС кода',
    826  => 'Для текущего пользователя отсутствуют операции подтверждения через СМС код',
    828  => 'Код подтверждения операции, отправленный через СМС, указан неверно',
    830  => 'Код подтверждения был указан неверно %d раза. Вы исчерпали лимит подтверждения операции.',
    832  => 'Доступ к получению СМС кода подтверждения операции заблокирован на %s в связи с превышением %d попыток ввода',
    842  => 'Не указан логин при записи операции кода подтверждения',
    844  => 'Не указан код подтверждения операции',
    846  => 'Не указан логин при установке статуса подтверждения операции по коду СМС',
    848  => 'Не указан логин при обновлении записи подтверждения операции по коду СМС',
    850  => 'Не указан логин при получении записи подтверждения операции по коду',
    //работа с пользователем
    1020 => 'В конфигурациях некорректно указаны параметры, обратитесь к администратору',
    1150 => 'Пользователь с указанным номером телефона уже зарегистрирован',
    //работа с заявкой
    3410 => 'Текущая заявка заблокирована',
    3420 => 'Текущая заявка доступна для редактирования только в Личном кабинете',
    3430 => 'Текущая заявка заблокирована партнером',
];