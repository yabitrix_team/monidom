<?php

/**
 * Class BaseApplication
 * Используется для объявления компонентов, спользуемых в web и консольных решениях
 *
 * @property app\modules\app\v1\components\RedisQueueSenderComponent   $redisQueueSender            - компонент
 *           отправки  сообщений в очередь сообщений, с последующим распределением пользоватлям
 * @property app\modules\app\v1\components\FileComponent               $file                        - компонент для
 *           регистрации файлов
 * @property app\modules\app\v1\components\MsgComponent                $msg                         - компонент для
 *           работы с сообщениями ошибок и не только ошибок
 * @property app\modules\notification\components\NotificationComponent $notify                      - компонент для
 *           работы с уведомлениями
 * @property app\modules\app\v1\components\SoapClientComponent         $soapCMR                     - компонент для
 *           интеграции по SOAP протоколу
 * @property app\modules\app\v1\components\SoapClientComponent         $soapCRM                     - компонент для
 *           интеграции по SOAP протоколу
 * @property app\modules\app\v1\components\SoapClientComponent         $soapSMS                     - компонент для
 *           интеграции по SOAP протоколу
 * @property app\modules\app\v1\components\CardVerifierComponent       $cardVerifier                - компонент для
 *           проверки карты
 * @property app\modules\rmq\components\RmqBase                        $queueMFOReceive             - компонент для
 *           работы с очередями rmq
 * @property app\modules\rmq\components\RmqBase                        $queueMFOSend                - компонент для
 *           работы с очередями rmq
 * @property  understeam\fcm\Client                                    $fcm                         - компонент для
 *            работы с сервисом Firebase Cloud Messaging
 * @property app\modules\app\v1\components\AccreditationFileComponent  $accreditationFile           - файл аккредитации
 * @property app\modules\app\v1\components\LCRMComponent               $lcrmMP                      - компонент для
 *           интеграции с сервисом LCRM для пользователя МП
 * @property app\modules\app\v1\components\LCRMComponent               $lcrmLKK                     - компонент для
 *           интеграции с сервисом LCRM для пользователя LKK
 * @property app\modules\app\v1\components\CRIBComponent               $cribMP                      - компонент для
 *           интеграции с сервисом CRIB для пользователя МП
 * @property app\modules\app\v1\components\CRIBComponent               $cribLKK                     - компонент для
 *           интеграции с сервисом CRIB для пользователя LKK
 */
abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Class WebApplication
 * Используется для объявления компонентов, спользуемых ТОЛЬКО в web
 */
class WebApplication extends yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Используется для объявления компонентов, спользуемых ТОЛЬКО в консольном приложении
 *
 * @property \yii\db\Connection $pdb соедиенение с параметром PDO::ATTR_PERSISTENT => true
 */
class ConsoleApplication extends yii\console\Application
{
}

class Yii extends yii\console\Application
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}