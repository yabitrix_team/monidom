<?php

namespace app\controllers;

use app\modules\app\v1\classes\Tools;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller {
	/**
	 * @var string - папка содержащая стили с хэшем с хэшем в названии
	 */
	protected $dirCss = '/static/%s/css/';
	/**
	 * @var string - папка содержащая скрипты с хэшем в названии
	 */
	protected $dirJs = '/static/%s/js/';
	/**
	 * @var array - масссив содержащий названия файлов без хэша, учитывается порядок указанаия
	 *            выборка будет ограничена указанными файлами
	 */
	protected $yaMetrika   = '';
	protected $fileMetrika = '';
	protected $arSortJs    = [
		'manifest',
		'vendor',
		'app',
	];

	public function getFileMetrika() {

		$this->fileMetrika = $_SERVER['DOCUMENT_ROOT'] . "/../config/metrika.js.php";
		if ( file_exists( $this->fileMetrika ) ) {
			return $this->yaMetrika = include( $this->fileMetrika );
		}

		return false;
	}

	public function actionLogin() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	/**
	 * Метод получения html для страницы с учетом подключения стилей и скриптов с changehash в названии
	 *
	 * @return string - вернет строку содержащую html
	 */
	protected function getHTML( $name ) {

		try {

			$dirCss          = sprintf( $this->dirCss, $name );
			$dirJs           = sprintf( $this->dirJs, $name );

			$favicons = '<link rel="icon" type="image/png" href="/static/img/icons/favicon-16x16.png" sizes="16x16">
						 <link rel="icon" type="image/png" href="/static/img/icons/favicon-32x32.png" sizes="32x32">
						 <link rel="icon" type="image/png" href="/static/img/icons/favicon-96x96.png" sizes="96x96">
						 <link rel="apple-touch-icon" sizes="114x114" href="/static/img/icons/apple-touch-icon-180x180.png">
						 <link rel="apple-touch-icon" sizes="60x60" href="/static/img/icons/apple-touch-icon-60x60.png">
						 <link rel="apple-touch-icon" sizes="120x120" href="/static/img/icons/apple-touch-icon-120x120.png">
						 <link rel="apple-touch-icon" sizes="76x76" href="/static/img/icons/apple-touch-icon-76x76.png">
						 <link rel="apple-touch-icon" sizes="152x152" href="/static/img/icons/apple-touch-icon-152x152.png">
						 <meta name="msapplication-TileColor" content="#ffffff">
						 <meta name="msapplication-TileImage" content="/static/img/icons/mstile-150x150.png">';

			$css             = Tools::getIncludedFilesInDir( $dirCss, [], '/static/css/', '.' );
			$js              = Tools::getIncludedFilesInDir( $dirJs, $this->arSortJs, '/static/js/', '.' );

			$this->yaMetrika = $this->getFileMetrika();

			return '
        <!DOCTYPE html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no"><title>CarMoney</title>'. $favicons . $css . '</head><body><div id="app"></div>' . $js . $this->yaMetrika . '</body></html>
        ';
		} catch ( \Exception $e ) {
			//todo[echmaster]: 11.05.2018 Добавить логирование, ошибка возможна если некорркетно указана директория к скриптам или стилям
			echo $e->getMessage();

			return 'Произошла ошибка, обратитесь в службу технической поддержки';
		}
	}

	public function actionRegister() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionPartner() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionPartnerAdmin() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( 'admin' );
	}

	public function actionMrp() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( 'manager' );
	}

	public function actionClient() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionClientPublic() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( 'client' );
	}

	public function actionPay() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionAgent() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionPrint() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionService() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionPanel() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

	public function actionLead() {

		Yii::$app->response->format = Response::FORMAT_HTML;
		echo $this->getHTML( $this->action->id );
	}

    public function actionCall() {

        Yii::$app->response->format = Response::FORMAT_HTML;
        echo $this->getHTML( $this->action->id );
    }

	public function behaviors() {

		return [
			'access' => [
				'class'        => AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'print' ],
						'roles'   => [ '?', '@' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'partner' ],
						'roles'   => [ 'role_user_partner' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'partner-admin' ],
						'roles'   => [ 'role_partner_admin' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'client' ],
						'roles'   => [ 'role_client', 'role_mobile_application' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'agent' ],
						'roles'   => [ 'role_agent' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'panel' ],
						'roles'   => [ 'role_support', 'role_admin' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'lead' ],
						'roles'   => [ 'role_lead_agent' ],
					],
                    [
                        'allow'   => true,
                        'actions' => [ 'call' ],
                        'roles'   => [ 'role_call' ],
                    ],
					[
						'allow'   => true,
						'actions' => [ 'client-public', 'register', 'login', 'mrp' ],
						'roles'   => [ '?' ],
					],
					[
						'allow'   => true,
						'actions' => [
							'service',
							'pay',
						],
						'roles'   => [ '?', '@' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						Yii::$app->response->redirect( \Yii::$app->getUser()->loginUrl . '' )->send();
					} else {
						\Yii::$app->user->logout( true );
						Yii::$app->response->redirect( \Yii::$app->getUser()->loginUrl . '' )->send();
					}
				},
			],
		];
	}
}