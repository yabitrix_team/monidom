<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode( $this->title ) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<table>
	<? if ( $code ) { ?>
        <tr>
            <td><b>Code заявки:</b></td>
            <td><?= $code ?></td>
        </tr>
        <tr>
            <td><b>Код заявки:</b></td>
            <td><?= $num ?></td>
        </tr>
	<? } ?>
	<? if ( $client ) { ?>
        <tr>
            <td><b>Клиент:</b></td>
            <td><?= $client ?></td>
        </tr>
	<? } ?>
	<? if ( $user_phone ) { ?>
        <tr>
            <td><b>Пользователь:</b></td>
            <td><?= $user_fio ?></td>
        </tr>
        <tr>
            <td><b>Телефон:</b></td>
            <td><?= $user_phone ?></td>
        </tr>
        <tr>
            <td><b>E-mail пользователя:</b></td>
            <td><?= $user_email ?></td>
        </tr>
        <tr>
            <td><b>Точка:</b></td>
            <td><?= $point ?></td>
        </tr>
        <tr>
            <td><b>Партнер по точке:</b></td>
            <td><?= $partner_point ?></td>
        </tr>
        <tr>
            <td><b>Партнер агента:</b></td>
            <td><?= $partner_agent ?></td>
        </tr>
	<? } ?>
	<? if ( $email ) { ?>
        <tr>
            <td><b>E-mail:</b></td>
            <td><?= $email ?></td>
        </tr>
	<? } ?>
    <tr>
        <td><b>User agent:</b></td>
        <td><?= $user_agent ?></td>
    </tr>
    <tr>
        <td><b>Операционная система:</b></td>
        <td><?= $os ?></td>
    </tr>
    <tr>
        <td><b>Браузер:</b></td>
        <td><?= $browser ?></td>
    </tr>
    <tr>
        <td><b>Размер браузера:</b></td>
        <td><?= $sizeBrowser ?></td>
    </tr>
    <tr>
        <td><b>Размер экрана монитора:</b></td>
        <td><?= $sizeScreen ?></td>
    </tr>
    <hr>
    <tr>
        <td><b>Сообщение:</b></td>
        <td>
            <pre><?= $messageText ?></pre>
        </td>
    </tr>
</table>
<hr>
<? if ( $imageFileToEmbed ) { ?>
    <img src="<?= $message->embed( $imageFileToEmbed ); ?>">
<? } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
