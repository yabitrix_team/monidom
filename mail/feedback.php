<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode( $this->title ) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<table>
    <tr>
        <td><b>Имя пользователя:</b></td>
        <td><?= $name ?></td>
    </tr>
    <tr>
        <td><b>E-mail:</b></td>
        <td><?= $email ?></td>
    </tr>
    <tr>
        <td><b>Сообщение:</b></td>
        <td><pre><?= $message ?></pre></td>
    </tr>
</table>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
