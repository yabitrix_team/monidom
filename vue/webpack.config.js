'use strict';
  const utils = require('./build/utils');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./build/webpack.base.conf');
const baseDevConf = require('./build/dev.base.conf');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const portfinder = require('portfinder');
const cabinet = require('./config/cabinet');
const config = require('./config');
let devWebpackConfig;

try { // Создан тестовый пример с Менеджером чтобы показать как это работает
  console.log(`USE DECORATOR: ${cabinet.name}`);

  const decoratorConf = require(`./build/decorators/dev/${cabinet.name.toLowerCase()}.conf.js`);
  devWebpackConfig = merge(baseWebpackConfig, decoratorConf);
} catch (e) {
  console.log(`DECORATOR NOT FOUND OR ANOTHER ERROR: ${e}`);

  devWebpackConfig = merge(baseWebpackConfig, baseDevConf);
}

module.exports = new Promise((resolve, reject) => {
  portfinder.basePort = process.env.PORT || config.dev.port;
  portfinder.getPort((err, port) => {
    if (err) {
      reject(err)
    } else {
      // publish the new Port, necessary for e2e tests
      process.env.PORT = port;
      // add port to devServer config
      devWebpackConfig.devServer.port = port;

      // Add FriendlyErrorsPlugin
      devWebpackConfig.plugins.push(new FriendlyErrorsPlugin({
        compilationSuccessInfo: {
          messages: [`Your application is running here: http://${devWebpackConfig.devServer.host}:${port}`],
        },
        onErrors: config.dev.notifyOnErrors
                  ? utils.createNotifierCallback()
                  : undefined
      }));

      resolve(devWebpackConfig)
    }
  })
});
