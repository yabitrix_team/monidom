const configs = require('./cabinet.list');

let scriptName = ['default'];

if (process && process.env && process.env['npm_lifecycle_event']) {
  scriptName = process.env['npm_lifecycle_event'].replace(/dev-|build-|all-|prod-/, '');
}

const cabinet = scriptName[0].toUpperCase() + scriptName.slice(1);

module.exports = (cabinet && configs[cabinet]) ? { name:cabinet, port:configs[cabinet].port } : { name: 'lint', port: 1488 };
