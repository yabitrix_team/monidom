const list = {
  Login: {
    port: 8080,
  },
  Partner: {
    port: 8081,
  },
  Client: {
    port: 8082,
  },
  Agent: {
    port: 8083,
  },
  Print: {
    port: 8084,
  },
  Register: {
    port: 8085,
  },
  Pay: {
    port: 8086,
  },
  Service: {
    port: 8087,
  },
  Manager: {
    port: 8088,
  },
  Admin: {
    port: 8089,
  },
  Panel: {
    port: 8090,
  },
  Lead: {
    port: 8091,
  },
  Call: {
    port: 8092,
  },
};
module.exports = list;
