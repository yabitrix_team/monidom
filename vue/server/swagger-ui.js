const fs = require('fs');
const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const read = require('read-data');

const filename = './doc/api.yaml';
let server = null;
fs.watch(filename, { encoding: 'buffer' }, (eventType, filename) => {
  if (filename && eventType === 'change') {
    console.log(eventType, 'reloading server');
    init();
  }
});

function init() {
  if (server !== null) {
    server.close();
  }

  const swaggerDocument = read.sync(filename);

  app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  server = app.listen(3000);
  console.log('server started http://localhost:3000');
}
init();

