// include dependencies
var express = require('express');
var proxy = require('http-proxy-middleware');
var config = require('../src/config.js');
var cabinets = require('../config/cabinet.list');

console.log(config.domain);
var domains = Object.keys(cabinets).map((i, e) => {
  return {
    domain: i.toLowerCase(),
    port: cabinets[i].port
  };
});

const hosts = Object.keys(config.domains);

var router = {};
hosts.forEach(host => {
  domains.forEach(i => {
    router[i.domain + '.local.' + host] = 'http://localhost:' + i.port;
  });
})


console.log(router);
// proxy middleware options
var options = {
        target: hosts, // target host
        changeOrigin: true,               // needed for virtual hosted sites
        ws: true,                         // proxy websockets
        router: router,
    };

// create the proxy (without context)
var exampleProxy = proxy(options);

// mount `exampleProxy` in web server
var app = express();
    app.use('/', exampleProxy);
    app.listen(80);
