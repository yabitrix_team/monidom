const express = require('express');
const http = require('http');
const url = require('url');
const WebSocket = require('ws');

const app = express();

app.use((req, res) => {
  res.send({ msg: 'hello' });
});

const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws, req) => {
  // eslint-disable-next-line
  const location = url.parse(req.url, true)


  ;
  // You might use location.query.access_token to authenticate or share sessions
  // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

  console.log('connected');

  ws.on('message', (message) => {
    console.log('received: %s', message);
    const inputData = JSON.parse(message);
    if (inputData.action === 'getFirstDocuments') {
      setTimeout(() => {
        if (ws.readyState !== 3) {
          try {
            ws.send(JSON.stringify({
              mutation: 'SOCKET_FIRST_DOCUMENTS',
              data: [
                {
                  id: 1,
                  name: 'Анкета',
                  text: 'Подписано на трех листах, в двух экземплярах',
                  link: '/download/1234567890',
                },
                {
                  id: 2,
                  name: 'Согласие на обработку ПД',
                  text: 'Подписано на трех листах, в двух экземплярах',
                  link: '/download/1234567890',
                },
              ],
            }));
            console.log('sended');
          } catch (e) {
            console.log('error', e);
          }
        }
      }, 3000);
    }
    if (inputData.action === 'getSecondDocuments') {
      setTimeout(() => {
        if (ws.readyState !== 3) {
          try {
            ws.send(JSON.stringify({
              mutation: 'SOCKET_SECOND_DOCUMENTS',
              data: [
                {
                  id: 1,
                  name: 'Договор',
                  text: 'Подписан на трех листах, в двух экземплярах',
                  link: '/download/1234567890',
                },
                {
                  id: 2,
                  name: 'График платежей',
                  text: 'Подписан на трех листах, в двух экземплярах',
                  link: '/download/1234567890',
                },
              ],
            }));
            console.log('sended');
          } catch (e) {
            console.log('error', e);
          }
        }
      }, 3000);
    }
  });

  ws.on('error', e => console.log(`errored ${e}`));


  ws.on('close', () => {
    console.log('disconnected');
  });

  // setTimeout(() => {
  //   if (ws.readyState !== 3) {
  //     try {
  //       ws.send(JSON.stringify({
  //         mutation: 'SOCKET_DOCUMENTS',
  //         data: 'testing mutation',
  //       }));
  //     } catch (e) {
  //       console.log('error', e);
  //     }
  //   }
  // }, 500);

  // setInterval(() => {
  //   if (ws.readyState !== 3) {
  //     try {
  //       ws.send('{}');
  //     } catch (e) {
  //       console.log('error', e);
  //     }
  //   }
  // }, 15000);
});

server.listen(9998, () => {
  console.log('Listening on %d', server.address().port);
});
