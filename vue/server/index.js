const express = require('express');
const bodyParser = require('body-parser');
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

const app = express();
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb',
}));
app.use(bodyParser.json({ limit: '50mb' }));


// для всех запросов ставим заголовки
app.all('*', (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Auth');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

// для всех но только методом options
app.options('*', (req, res) => {
  // res.writeHead(200, { 'Content-type': 'text-json' });
  res.send('Success');
});

app.route('/')
  .get((req, res) => {
    res.send('it works!');
  });

app.get('/fake/v1/auto-years', (req, res) => {
  res.json([
    {
      id: 1,
      name: '2003',
    },
    {
      id: 2,
      name: '2004',
    },
    {
      id: 3,
      name: '2005',
    },
  ]);
});

app.post('/fake/v1/service/sms/:code', (req, res) => {
  res.json({
    error: [],
    data: {
      check_code: true,
    },
  });
});

app.get('/fake/v1/request/:hash', (req, res) => {
  // проверка прав на заявку
  res.json({
    result: 1,
    request: {
      code: req.params.hash,
      summ: '500000',
      date_return: '2018-02-22T22:16:00.000Z',
      auto_brand_id: {
        id: '2',
        name: 'Audi',
      },
      auto_model_id: {
        id: '5',
        name: 'A6',
      },
      auto_year: '2017',
      auto_price: '1000000',
      credit_product_id: 1,
      client_first_name: 'Иван',
      client_last_name: 'Иванов',
      client_patronymic: 'Петрович',
      client_passport_number: '1010',
      client_passport_serial_number: '999888',
      client_birthday: '2018-02-14T22:17:00.000Z',
      client_region_id: {
        id: 1,
        name: 'Московская область',
      },
      client_mobile_phone: '9851234567',
      comment_client: 'Очень важный клиент',
    },
  });
});

app.get('/fake/v1/info/regions', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Московская область',
    },
    {
      id: 2,
      name: 'Москва',
    },
    {
      id: 3,
      name: 'Тверская область',
    },
  ]);
});

app.get('/download/1234567890', (req, res) => {
  const file = `${__dirname}/../static/i/arrow.svg`;
  res.download(file);
});

app.get('/fake/v1/info/employments', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Безработный',
    },
    {
      id: 2,
      name: 'Пенсионер',
    },
    {
      id: 3,
      name: 'Менеджер',
    },
  ]);
});

app.get('/fake/v1/info/workplace-periods', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'дней',
    },
    {
      id: 2,
      name: 'месяцев',
    },
    {
      id: 3,
      name: 'лет',
    },
  ]);
});

app.get('/fake/v1/info/people-relations', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'брат',
    },
    {
      id: 2,
      name: 'сестра',
    },
    {
      id: 3,
      name: 'друг',
    },
  ]);
});

app.post('/fake/v1/request', (req, res) => {
  // console.log(req.body);
  // типа сохранили заявку в бекенде
  res.json({
    result: '1',
    id: '12345',
  });
});

app.post('/fake/v1/calculate', (req, res) => {
  const data = `request_sum=${req.body.summ}&return_date=${req.body.date}`;
  const xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener('readystatechange', function () {
    if (this.readyState === 4) {
      res.json(this.responseText);
    }
  });

  xhr.open('POST', 'http://api2751.carmoney.ru/check/');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.setRequestHeader('lock-token', 'a09se8rjhq3oeQ089-fd82q3peepqej');
  xhr.setRequestHeader('api-key', 'a98703wh92o3r87qhr3r1');
  xhr.setRequestHeader('Cache-Control', 'no-cache');
  xhr.setRequestHeader('Postman-Token', '318256de-3f50-17fc-9ed5-1eba901b3631');

  xhr.send(data);
});

app.post('/fake/v1/send-file', (req, res) => {
  // сохранили файл в беке для дальнейшего использования
  res.json({
    result: '1',
    id: '12345',
  });
});

/**
 * сохраняем фотграфию
 */
app.post('/fake/v1/request/file', (req, res) => {
  res.json({
    result: '1',
    id: (Math.random() * (9999 - 1111)) + 1111,
  });
});

/**
 * получаем фотографию
 */
app.get('/fake/v1/request/file', (req, res) => {
  // сохранили файл в беке для дальнейшего использования
  res.json({
    result: '1',
    url: 'fake url',
  });
});

/**
 * Удаляем фотографию
 */
app.delete('/fake/v1/request/file', (req, res) => {
  res.json({
    result: 'ок',
  });
});

app.post('/fake/v1/save-files', (req, res) => {
  // сохранили файл в беке для дальнейшего использования
  res.json({
    result: '1',
  });
});

try {
  app.listen(8078, () => {
    console.log('Server started at http://127.0.0.1:8078');
  });
} catch (err) {
  console.log(err);
}

process.on('uncaughtException', (e) => {
  if (e.code === 'EADDRINUSE') {
    console.log('Skipping API server, port in use.');
  }
});
