import Vue from 'vue';

import Vuex from 'vuex';
import fs from 'fs';
import path from 'path';
import axios from 'axios';


// ===
// Utility functions
// ===

// https://vue-test-utils.vuejs.org/
import vueTestUtils from '@vue/test-utils';
// https://lodash.com/
import _ from 'lodash';

Vue.config.productionTip = false;

// ===
// Global helpers
// ===
global.Vue = Vue;

// https://vue-test-utils.vuejs.org/api/#mount
global.mount = vueTestUtils.mount;

// https://vue-test-utils.vuejs.org/api/#shallowmount
global.shallowMount = vueTestUtils.shallowMount;

https://vue-test-utils.vuejs.org/api/#createlocalvue
global.createLocalVue = vueTestUtils.createLocalVue;

// A special version of `shallowMount` for view components
global.shallowMountView = (Component, options = {}) =>
  global.shallowMount(Component, {
    ...options,
    stubs: {
      Layout: {
        functional: true,
        render(h, { slots }) {
          return <div>{slots().default}</div>;
        },
      },
      ...(options.stubs || {}),
    },
  });

// A helper for creating Vue component mocks
global.createComponentMocks = ({ store, router, style, mocks, stubs}, localVue = vueTestUtils.createLocalVue()) => {
  // Use a local version of Vue, to avoid polluting the global
  // Vue and thereby affecting other tests.
  // https://vue-test-utils.vuejs.org/api/#createlocalvue

  const returnOptions = { localVue };

  // https://vue-test-utils.vuejs.org/api/options.html#stubs
  returnOptions.stubs = stubs || {};
  // https://vue-test-utils.vuejs.org/api/options.html#mocks
  returnOptions.mocks = mocks || {};

  // Converts a `store` option shaped like:
  //
  // store: {
  //   someModuleName: {
  //     state: { ... },
  //     getters: { ... },
  //     actions: { ... },
  //   },
  //   anotherModuleName: {
  //     getters: { ... },
  //   },
  // },
  //
  // to a store instance, with each module namespaced by
  // default, just like in our app.
  // only modules nothing else.
  if (store) {
    localVue.use(Vuex);
    returnOptions.store = new Vuex.Store({
      modules: Object.keys(store)
        .map(moduleName => {
          const storeModule = store[moduleName];
          return {
            [moduleName]: {
              state: storeModule.state || {},
              getters: storeModule.getters || {},
              actions: storeModule.actions || {},
              mutations: storeModule.mutations || {},
              namespaced:
                typeof storeModule.namespaced === 'undefined'
                  ? true
                  : storeModule.namespaced,
            },
          };
        })
        .reduce((moduleA, moduleB) => Object.assign({}, moduleA, moduleB), {}),
    });
  }

  // If using `router: true`, we'll automatically stub out
  // components from Vue Router.
  if (router) {
    returnOptions.stubs['router-link'] = true;
    returnOptions.stubs['router-view'] = true;
  }

  // If a `style` object is provided, mock some styles.
  if (style) {
    returnOptions.mocks.$style = style;
  }

  return returnOptions;
};

global.createModuleStore = (vuexModule, options = {}) => {
  vueTestUtils.createLocalVue().use(Vuex);
  const store = new Vuex.Store({
    ..._.cloneDeep(vuexModule),
    modules: {
      auth: {
        namespaced: true,
        state: {
          currentUser: options.currentUser,
        },
      },
    },
  });
  axios.defaults.headers.common.Authorization = options.currentUser
    ? options.currentUser.token
    : '';
  if (vuexModule.actions.init) {
    store.dispatch('init');
  }
  return store;
};

//
// Mock window properties not handled by jsdom
//

Object.defineProperty(window, 'localStorage', {
  value: (function() {
    let store = {};
    return {
      getItem(key) {
        return store[key] || null;
      },
      setItem(key, value) {
        store[key] = value.toString();
      },
      clear() {
        store = {};
      },
    };
  })(),
});



