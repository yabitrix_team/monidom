import Vuex from 'vuex';
import FileUploader from '@/components/FileUploader';
import {shallowMount, createLocalVue } from '@vue/test-utils';
import myFormatPlugin from '@/plugins/moneyformat';
import formatterPlugin from '@/plugins/formatter';
import ToPlugin from '@/plugins/to';
import Validate from '@/plugins/validate';
import error from '@/plugins/error';
import ValidateRu from '@/plugins/validate';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(error);
localVue.use(myFormatPlugin);
localVue.use(formatterPlugin);
localVue.use(ToPlugin);
localVue.use(Validate);
localVue.use(ValidateRu);

describe('FileUploader', () => {
  const wrapper = shallowMount(FileUploader, {
    mocks: {
      $store: {
        state:{
          request: {
            testFiles:[],
          },
        },
      },
    },
    localVue,
    propsData: {
      saveTo: 'test_files',
    },
  });

  it('count right sizes', () => {
    expect(wrapper.vm.getSizes(160, 160, 160, 160)).toEqual(expect.objectContaining({ height: 160, width: 160 }));
    expect(wrapper.vm.getSizes(160, 160, 1600, 1600)).toEqual(expect.objectContaining({ height: 160, width: 160 }));
    expect(wrapper.vm.getSizes(1600, 1600, 160, 160)).toEqual(expect.objectContaining({ height: 160, width: 160 }));
    expect(wrapper.vm.getSizes(161, 162, 163, 164)).toEqual(expect.objectContaining({ height: 161, width: 162 }));
    expect(wrapper.vm.getSizes(164, 163, 162, 161)).toEqual(expect.objectContaining({ height: 162, width: 161 }));
    expect(wrapper.vm.getSizes(161, 162, 164, 163)).toEqual(expect.objectContaining({ height: 161, width: 162 }));
    expect(wrapper.vm.getSizes(161, 164, 162, 163)).toEqual(expect.objectContaining({ height: 160, width: 163 }));
    expect(wrapper.vm.getSizes(161, 164, 163, 162)).toEqual(expect.objectContaining({ height: 159, width: 162 }));
    expect(wrapper.vm.getSizes(164, 161, 163, 162)).toEqual(expect.objectContaining({ height: 163, width: 160 }));
    expect(wrapper.vm.getSizes(164, 163, 161, 162)).toEqual(expect.objectContaining({ height: 161, width: 160 }));
  });
});


