import Vuex from 'vuex';
import Component from '@/components/Partner/StepFinishAfterApprove';
import {shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

let store = new Vuex.Store({
  state: {
    data: {},
    request: {
      docPack2: [
        {
          code: 'd34846feadd2e0e139c342876a1422d1',
          name: 'Индивидуальные условия по договору',
          description: 'Подписаны на всех листах, в двух экземплярах',
          url: '/doc/d34846feadd2e0e139c342876a1422d1',
        },
        {
          code: '4a81312836b3e16e78746517a2395fc1',
          name: 'Акт приема-передачи ПТС',
          description: 'Подписан на одном листе, в двух экземплярах',
          url: '/doc/4a81312836b3e16e78746517a2395fc1'
        },
        {
          code: '82d1d381aec99a23b00c23fbbc7af13e',
          name: 'График платежей',
          description: 'Подписан на одном листе, в двух экземплярах',
          url: '/doc/82d1d381aec99a23b00c23fbbc7af13e'
        },
      ],
    },
  },
  mutations: {},
  actions: {
    hasSecondPack(context, data) {
    },
  }
})


describe('StepFinishAfterApprove', () => {
  const routes = [
    {
      path: '/request/:code',
      component: Component,
    },
    {
      path: '/' ,
      redirect: '/request/string',
    }
  ];

  const router = new VueRouter({
    routes
  });

  const wrapper = shallowMount(Component, {
    store,
    router,
    localVue,
    propsData: {
      saveTo: 'test_files',
    },
  });

  it('renders right content', () => {
    expect(wrapper.vm.blockPassword).toBe(false);
    wrapper.vm.$store.state.request.docPack2.forEach(element => {
      wrapper.vm.checkedDocuments.push(element.code);
    });
    wrapper.vm.submit();
    expect(wrapper.vm.blockPassword).toBe(true);
  });
});


