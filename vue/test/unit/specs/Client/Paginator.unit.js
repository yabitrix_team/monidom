import paginator from '@/components/Pagination';
import {shallowMount, createLocalVue} from '@vue/test-utils';
import VueRouter from 'vue-router';

describe('Paginator', () => {

  it('show right route', () => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  const routes = [
    {
      path: '/example/:page',
      component: paginator
    },
    {
      path: '/',
      redirect: '/example/String',
    }
  ];

  const router = new VueRouter({
    routes
  });


  let wrapper;

  wrapper = shallowMount(paginator, {
    localVue,
    router,
    propsData: {
      'pageCount': 6,
      'paginationPattern': '/example/pagination',
      'paginationRouteParam': 'page'
    },
  });

    expect(wrapper.vm.pagin).toBeFalsy();
  });

  it('show right route', () => {
    const localVue = createLocalVue();
    localVue.use(VueRouter);
    const routes = [
      {
        path: '/',
        redirect: '/example/String',
      },
      {
        path: '/example/:page',
        component: paginator
      },
    ];

    const router = new VueRouter({
      routes
    });




    let wrapper = shallowMount(paginator, {
      localVue,
      router,
      propsData: {
        'pageCount': 6,
        'paginationPattern': '/example/pagination',
        'paginationRouteParam': 'page'
      },
    });

    expect(wrapper.vm.pagin).toBe(1);
    expect(wrapper.vm.isBackDisabled).toBe(true);
    expect(wrapper.vm.isNextDisabled).toBe(false);
    expect(wrapper.vm.pageInc()).toBe(wrapper.vm.pagin + 1);
    expect(wrapper.vm.pageDec()).toBe(1);
  })
});

