import Vue from 'vue'
import {EmitterClass} from '@/plugins/sockets/Emitter';


describe ("Emitter.js", () => {

  let vm = new Vue();
  let Emitter;

  beforeEach (() => {
    Emitter = new EmitterClass();
  })

  it ('registers an handler', () => {
    expect(Emitter.listeners.size).toEqual(0);
    Emitter.addListener('[event_type]', (value) => {}, vm);
    expect(Emitter.listeners.size).toEqual(1);
  })

  it ('unregisters a registered handler', () => {
    let event_type = 'atype', cb = (value) => {}
    // should.not.exist()
    // console.log(Emitter.listeners.get(event_type));
    expect(Emitter.listeners.get(event_type)).toBeUndefined()
    Emitter.addListener(event_type, cb, vm)
    expect(Emitter.listeners.get(event_type).length).toEqual(1)
    Emitter.removeListener(event_type, cb, vm)
    expect(Emitter.listeners.get(event_type).length).toEqual(0)
  })

  it ('fires an events', (done) => {
    let event_type = 'syn', expected_response = 'ack'
    expect(Emitter.listeners.size).toEqual(0)
    Emitter.addListener(event_type, (value) => {
      expect(value).toEqual(expected_response)
      done()
    }, vm)
    expect(Emitter.listeners.size).toEqual(1)
    Emitter.emit(event_type, expected_response)
  })

})
