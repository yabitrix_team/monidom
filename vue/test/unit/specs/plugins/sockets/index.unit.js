import Vue from 'vue'
import VueSock from '@/plugins/sockets/index'

import { Server } from 'mock-socket'

describe("index.js", () => {

  let mockServer

  it ('can be bound to the onopen event', (done) => {
    mockServer = new Server('ws://localhost:8080');
    Vue.use(VueSock, 'ws://localhost:8080');
    let vm = new Vue();
    vm.$connect();
    vm.$options.sockets.onopen = (data) => {
      expect(data.type).toEqual('open');
      mockServer.stop(done);
    }
  })
});
