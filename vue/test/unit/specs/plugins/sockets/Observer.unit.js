import Vue from 'vue';
import Emitter from '@/plugins/sockets/Emitter';
import Observer from '@/plugins/sockets/Observer';

import { Server, WebSocket } from 'mock-socket';

describe('Observer.js', () => {
  let observer, mockServer;

  let wsUrl = 'ws://localhost:8080';

  it('fires onopen event', (done) => {
    mockServer = new Server(wsUrl);
    mockServer.on('connection', ws => {
      ws.send('hi');
    })
    let vm = new Vue();
    observer = new Observer(wsUrl);
    Emitter.addListener('onopen', (data) => {
      expect(data.type).toEqual('open');
      mockServer.stop(done);
    }, vm);
  });

  it('fires onopen event skip scheme', (done) => {
    mockServer = new Server(wsUrl)
    mockServer.on('connection', ws => {
      ws.send('hi');
    })
    let vm = new Vue()
    observer = new Observer(wsUrl)
    Emitter.addListener('onopen', (data) => {
      expect(data.type).toEqual('open')
      mockServer.stop(done)
    }, vm);
  });
})
