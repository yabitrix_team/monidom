import {validateCardHolderName as valid} from '@/utils/validate';

describe('Validate card holder', () => {
  it('should return correct value', () => {
    expect(valid('Agatha MacDonald')).toBe(true);
    expect(valid('Adam Gordon')).toBe(true);
    expect(valid('Amanda Brian')).toBe(true);
    expect(valid('Audrey Smith')).toBe(true);
    expect(valid('Allan Butler')).toBe(true);
    expect(valid('Alison Black')).toBe(true);
    expect(valid('Bruce Robertson')).toBe(true);
    expect(valid('Boris Jones')).toBe(true);
    expect(valid('Carl Murphy')).toBe(true);
    expect(valid('123')).toBe(false);
    expect(valid('##@#ASESW')).toBe(false);
    expect(valid('@#WWRDS @@!RSE')).toBe(false);
    expect(valid('12123')).toBe(false);
    expect(valid('asdacxas')).toBe(false);
    expect(valid('ывфывф фывфывфыв')).toBe(false);
    expect(valid('ЫФВФЫ ""!ЫВЫФЫВ')).toBe(false);
  });
});
