import {validateCardNumber as valid} from '@/utils/validate';

describe('Validate card number', () => {
  it('should return correct value', () => {
    expect(valid('4242 4242 4242 4242')).toBe(true);
    expect(valid('4444 4444 4444 4448')).toBe(true);
    expect(valid('4242 4242 4242 4242 42')).toBe(true);
    expect(valid('4242 4242 4242 4242 42 8')).toBe(true);

    expect(valid('4242 4242 4242 4241')).toBe(false);
    expect(valid('4444 4444 4444 4444')).toBe(false);
    expect(valid('4242 4242 4242 4242 41')).toBe(false);
    expect(valid('4242 4242 4242 4242 42 4')).toBe(false);

    expect(valid('')).toBe(false);
    expect(valid({})).toBe(false);
    expect(valid([])).toBe(false);
    expect(valid(null)).toBe(false);
    expect(valid(undefined)).toBe(false);

    expect(valid('4242 4242 4242 4242z')).toBe(false);
    expect(valid('4242 4242 4242 4241z')).toBe(false);
  });

});
