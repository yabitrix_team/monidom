import { clearPhone as ClearPhone, isPhone, isCellular } from '@/utils/ClearPhone.js';

describe('Clear', () => {
  it('Right clear phone', () => {
    expect(ClearPhone('8911-111-11-11')).toBe('9111111111');
    expect(ClearPhone('8*?(?*;*)(Й*(;Й№к911-111-11-11')).toBe('9111111111');
    expect(ClearPhone('8000')).toBe('8000');
    expect(ClearPhone('login')).toBe('login');
    expect(ClearPhone('8login')).toBe('8login');
  });

  it('Right recognize phone', () => {
    expect(isPhone('8000')).toBe(false);
    expect(isPhone('8000000000')).toBe(true);
  });

  it('Right recognize cellular phone', () => {
    expect(isCellular('8000')).toBe(false);
    expect(isCellular('4950000000')).toBe(false);
    expect(isCellular('9000000000')).toBe(true);
  });
});
