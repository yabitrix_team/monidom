import {validateRu as valid} from '@/utils/validate';

describe('Validate ru symbols', () => {
  it('should return correct value', () => {
    expect(valid('123')).toBe(false);
    expect(valid('123ц')).toBe(false);
    expect(valid('123 ц')).toBe(false);
    expect(valid('wqeц')).toBe(false);
    expect(valid('@#$%^')).toBe(false);
    expect(valid('@#$%^цц')).toBe(false);
    expect(valid(' ')).toBe(true);
    expect(valid('ццц')).toBe(true);
    expect(valid('ццц цц')).toBe(true);
    expect(valid('ЦЦЦЦЦ')).toBe(true);
  });
});
