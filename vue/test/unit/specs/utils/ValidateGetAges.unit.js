import {getAges} from '@/utils/validate';

describe('Validate ru symbols', () => {
  const cp = {
    productInfo:
    {
      "guid":"42c491bf-8768-11e8-8154-00155d01bf07",
      "name":"Автозайм _RBP 36 от 75 000",
      "is_additional_loan":"0",
      "percent_per_day":"0.27",
      "month_num":"36",
      "summ_min":"75000",
      "summ_max":"1000000",
      "age_min":"21",
      "age_max":"68",
      "loan_period_min":"36",
      "loan_period_max":"36"
    }
  };

  it('should return correct value', () => {
    expect(getAges.call(cp, 'age_min')).toBe(21);
    expect(getAges.call(cp, 'age_max')).toBe(65);
    expect(getAges('age_min')).toBe(21);
    expect(getAges('age_max')).toBe(68);
  });
});
