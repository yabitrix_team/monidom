import {validateRuAddress as valid} from '@/utils/validate';

describe('Validate ru symbols', () => {
  it('should return correct value', () => {
    expect(valid('м')).toBe(true);
    expect(valid('123ц')).toBe(true);
    expect(valid('123 ц')).toBe(true);
    expect(valid('wqeц')).toBe(false);
    expect(valid('@#$%^')).toBe(false);
    expect(valid('@#$%^цц')).toBe(false);
    expect(valid(' ')).toBe(true);
    expect(valid('ццц')).toBe(true);
    expect(valid('ццц цц')).toBe(true);
    expect(valid('Москва, Кремль, 1п')).toBe(true);
    expect(valid('Москва, Старая площадь, 8/5 ст1')).toBe(true);
    expect(valid('Москва, Малая Ордынка, 13а ст6')).toBe(true);
    expect(valid('Москва, Пятницкая, 40-42 ст1')).toBe(true);
    expect(valid('Москва, ул. Пятницкая, 40-42 ст1')).toBe(true);
  });
});
