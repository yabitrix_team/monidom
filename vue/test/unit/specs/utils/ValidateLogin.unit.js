import {validateLogin as valid} from '@/utils/validate';

describe('Validate login symbols', () => {
  it('should return correct value', () => {
    expect(valid('qwertyuioasdfghjklzxcvbnm__sasdasxcxv123151368726fdsaf')).toBe(true);
    expect(valid('ajshvgdiyfy34287347823hr73f478')).toBe(true);
    expect(valid('^&%@$^&>>')).toBe(false);
    expect(valid('..asda23e23')).toBe(false);
    expect(valid('adasd12312ec..')).toBe(false);
    expect(valid(' casdasda asda ')).toBe(false);
  });
});
