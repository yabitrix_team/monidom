/*
 * custom matchers
 */
const customMatchers = {};

customMatchers.toBeAComponent = function toBeAComponent(options) {
  function isAComponent() {
    return _.isPlainObject(options) && typeof options.render === 'function';
  }
  if (isAComponent()) {
    return {
      message: () =>
        `expected ${this.utils.printReceived(
          options
        )} not to be a Vue component`,
      pass: true,
    };
  }
  return {
    message: () =>
      `expected ${this.utils.printReceived(
        options
      )} to be a valid Vue component, exported from a .vue file`,
    pass: false,
  };
};

// https://facebook.github.io/jest/docs/en/expect.html#expectextendmatchers
global.expect.extend(customMatchers);
