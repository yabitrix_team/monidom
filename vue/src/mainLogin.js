// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueMask from 'v-mask';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import Cleave from './components/Cleave';

import AppLogin from './AppLogin';
import router from './router/login';
import './assets/scss/index.scss';
import store from './store/login';
import myFormatPlugin from './plugins/moneyformat';
import formatterPlugin from './plugins/formatter';
import ToPlugin from './plugins/to';
// import config from './config';
import Api from './plugins/apiLogin';
import Validate from './plugins/validate';


Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(myFormatPlugin);
Vue.use(formatterPlugin);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(VueMask);
Vue.use(Validate);

Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);

Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { AppLogin },
  template: '<AppLogin/>',
});
