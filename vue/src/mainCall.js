// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import Cleave from 'vue-cleave';
// import VueQrcode from '@xkeshi/vue-qrcode';
import VueMask from 'v-mask';
import AppCall from '@/components/Call/AppCall';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome/index';
import ToggleButton from 'vue-js-toggle-button';
import router from './router/call';

import './assets/scss/index.scss';
import store from './store/call/';
import myFormatPlugin from './plugins/moneyformat';
import ToPlugin from './plugins/to';
import formatterPlugin from './plugins/formatter';
// import config from './config';
import Api from './plugins/apiCall';
import Validate from './plugins/validate';
import ValidateRu from './plugins/validateRu';

Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(myFormatPlugin);
Vue.use(formatterPlugin);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(ToggleButton);
Vue.use(VueMask);
Vue.use(Validate);
Vue.use(ValidateRu);

Vue.component('v-select', vSelect);
Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);
Vue.component('font-awesome', FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { AppCall },
  template: '<AppCall/>',
});
