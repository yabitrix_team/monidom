import _ from 'lodash';
import { required, cellular } from '../rules';

export default _.values({ required, cellular });
