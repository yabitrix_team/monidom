import _ from 'lodash';
import { required, onlyRussian } from '../rules';

export default _.values({ required, onlyRussian });

