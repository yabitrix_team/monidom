import _ from 'lodash';
import { required, strong } from '../rules';

export default _.values({ required, strong });
