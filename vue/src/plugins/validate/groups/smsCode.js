import _ from 'lodash';
import { required } from '../rules';

export default _.values({ required });

