import lastName from './lastName';
import firstName from './firstName';
import password from './password';
import phone from './phone';
import patronymic from './patronymic';
import smsCode from './smsCode';


export {
  lastName,
  firstName,
  password,
  phone,
  patronymic,
  smsCode,
};
