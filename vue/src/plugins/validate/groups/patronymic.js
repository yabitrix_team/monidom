import _ from 'lodash';
import { onlyRussian } from '../rules';

export default _.values({ onlyRussian });
