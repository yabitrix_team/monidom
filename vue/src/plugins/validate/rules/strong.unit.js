import strong from './strong';

describe('Пароль неподходящий потому-что', () => {
  it('нет буквы и заглавной буквы 123456', () => {
    expect(strong.rule('123456')).toBe(true);
  });

  it('слишком короткий', () => {
    expect(strong.rule('p123')).toBe(true);
  });

  it('Нет цифры', () => {
    expect(strong.rule('password')).toBe(true);
  });

  it('есть пробел !Q2w3e4 r', () => {
    expect(strong.rule('!Q2w3e4 r')).toBe(true);
  });

});

describe('Не ругаемся на пароль', () => {
  it('хороший пароль Q12345', () => {
    expect(strong.rule('Q12345')).toBeFalsy();
  });

  it('хороший пароль q12345', () => {
    expect(strong.rule('q12345')).toBeFalsy();
  });

  it('хороший пароль !q2w3e4r', () => {
    expect(strong.rule('!q2w3e4r')).toBeFalsy();
  });

  it('хороший пароль Q2w3e4rr', () => {
    expect(strong.rule('Q2w3e4rr')).toBeFalsy();
  });

  it('хороший пароль !Q2w3e4r', () => {
    expect(strong.rule('!Q2w3e4r')).toBeFalsy();
  });

  it('хороший пароль 2w3e4rQ!', () => {
    expect(strong.rule('2w3e4rQ!')).toBeFalsy();
  });

  it('хороший пароль 2w3e4r!Q', () => {
    expect(strong.rule('2w3e4r!Q')).toBeFalsy();
  });
});

describe('Не пустое сообщение', () => {
  it('сообщение об ошибке заполнено', () => {
    expect(strong.message).toBeDefined;
  });
});
