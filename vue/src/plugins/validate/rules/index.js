import cellular from './cellular';
import onlyRussian from './onlyRussian';
import required from './required';
import strong from './strong';

export {
  cellular,
  onlyRussian,
  required,
  strong,
};
