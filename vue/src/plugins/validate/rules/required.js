export default {
  rule: a => typeof a === 'string' && a.length < 1,
  message: 'Поле должно быть заполнено',
};
