import cellular from './cellular';


describe('Поле не заполненено', () => {
  it('пустое значние', () => {
    expect(cellular.rule('')).toBe(true);
  });
});

describe('Поле не заполненено', () => {
  it('пустое значение', () => {
    expect(cellular.rule('(495) 845 45-35')).toBe(true);
  });
});

describe('Поле не заполненено', () => {
  it('пустое значение', () => {
    expect(cellular.rule('945-845-45-35')).toBe(false);
  });
});

describe('Не пустое сообщение', () => {
  it('сообщение об ошибке заполнено', () => {
    expect(cellular.message).toBeDefined;
  });
});
