import { isCellular, clearPhone } from '@/utils/ClearPhone';

export default {
  rule: a => !isCellular(clearPhone(a)) || clearPhone(a).length !== 10,
  message: 'Введите номер мобильного телефона',
};
