import required from './required';

describe('Поле заполнено', () => {
  it('нет буквы и заглавной буквы 123456', () => {
    expect(required.rule('123456')).toBeFalsy();
  });
});

describe('Поле не заполненено', () => {
  it('пустое значчние', () => {
    expect(required.rule('')).toBe(true);
  });
});

describe('Не пустое сообщение', () => {
  it('сообщение об ошибке заполнено', () => {
    expect(required.message).toBeDefined;
  });
});
