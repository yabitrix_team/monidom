import onlyRussian from './onlyRussian';

describe('Выдает ошибку если, не только русские символы', () => {
  it('123', () => {
    expect(onlyRussian.rule('123')).toBe(true);
  });
  it('123ц', () => {
    expect(onlyRussian.rule('123ц')).toBe(true);
  });
  it('123 ц', () => {
    expect(onlyRussian.rule('123 ц')).toBe(true);
  });
  it('wqeц', () => {
    expect(onlyRussian.rule('wqeц')).toBe(true);
  });
  it('@#$%^', () => {
    expect(onlyRussian.rule('@#$%^')).toBe(true);
  });
  it('@#$%^цц', () => {
    expect(onlyRussian.rule('@#$%^цц')).toBe(true);
  });
  it(' ', () => {
    expect(onlyRussian.rule(' ')).toBeFalsy();
  });
  it('ццц', () => {
    expect(onlyRussian.rule('ццц')).toBeFalsy();
  });
  it('ццц цц', () => {
    expect(onlyRussian.rule('ццц цц')).toBeFalsy();
  });
  it('ЦЦЦЦЦ', () => {
    expect(onlyRussian.rule('ЦЦЦЦЦ')).toBeFalsy();
  });
});

describe('Не пустое сообщение', () => {
  it('сообщение об ошибке заполнено', () => {
    expect(onlyRussian.message).toBeDefined;
  });
});
