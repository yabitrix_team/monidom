import { topBarHeight } from '@/utils/StepPosition';
/**
 * миксин для валидации полей при вызове применяет функции из обьекта валидации
 * для в корне нужно разместить структуру вида:
      summ: '', // данные
      ...
      // объект валидации
      validation: {
        summ: {
          rules: [] // массив правил из папки rules
        },
        ...
*/
const Plugin = {
  install(Vue) {
    // eslint-disable-next-line
    Vue.mixin({
      mounted() {
        if (this.$options.validation) {
          Object.keys(this.$options.validation).forEach((item) => {
            // adding items to validation object
            if (!('message' in this.$options.validation[item])) {
              this.$options.validation[item].message = [];
            }
            // Vue.set make reactive all props in validation object
            this.$set(this.$options.validation[item], 'invalid', !!this.$options.validation[item].invalid);
            this.$options.validation[item].func = function func(vue, e) {
              this.invalid = false;
              const messages = [];
              this.rules.forEach((ruleItem) => {
                if (ruleItem.rule(vue[e], this, vue, e)) {
                  messages.push(ruleItem.message);
                  this.invalid = true;
                }
              });
              // it show only first error from list
              this.message = messages[0];
            };
          });
        }
      },
      methods: {
        // validation method for field of for all items in validation object
        validate(name = null) {
          let validation = {};
          if (this.$options.validation) {
            validation = this.$options.validation || {};
          } else {
            // for support legacy object
            // TODO remove it
            validation = this.validation || {};
          }

          let valid = true;

          if (name === null) {
            // validate all object
            Object.keys(validation).forEach((e) => {
              if (typeof validation[e].func === 'function') {
                validation[e].func(this, e);
              }
              if (validation[e].invalid) {
                valid = false;
              }
            });
          } else if (validation[name] !== undefined) {
            // validate item with given name
            if (typeof validation[name].func === 'function') {
              validation[name].func(this, name);
            }
            if (validation[name].invalid) {
              valid = false;
            }
          }

          Vue.nextTick(() => {
            const el = document.querySelector('.error');
            if (el && el.scrollIntoView) {
              el.scrollIntoView(true);
              window.scrollBy(0, -topBarHeight());
            }
          });
          return valid;
        },
      },
    });
  },
};

export { Plugin as default };
