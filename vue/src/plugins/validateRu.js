import { validateRu, validateRuAddress, validateEng, validateCardHolderName, validateLastName, validateLogin, validateCardNumber, validateEmail } from '../utils/validate';

const Validate = {
  install(Vue) {
    Vue.prototype.$validateRu = validateRu;
    Vue.prototype.$validateEng = validateEng;
    Vue.prototype.$validateRuAddress = validateRuAddress;
    Vue.prototype.$validateCardHolderName = validateCardHolderName;
    Vue.prototype.$validateCardNumber = validateCardNumber;
    Vue.prototype.$validateLastName = validateLastName;
    Vue.prototype.$validateEmail = validateEmail;
    Vue.prototype.$validateLogin = validateLogin;
  },
};

export { Validate as default };
