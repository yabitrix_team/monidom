const FormatterPlugin = {
  install(Vue) {
    // eslint-disable-next-line
    Vue.prototype.$formatter = function $formatter(str, num) {
      const digits = str.toString().split('');
      digits.splice(num, 0, ' ');
      return digits.join('');
    };
  },
};

export { FormatterPlugin as default };
