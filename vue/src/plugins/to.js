import to from '../utils/To';

const Plugin = {
  install(Vue) {
    // eslint-disable-next-line
    Vue.prototype.$to = to;
  },
};

export { Plugin as default };
