/**
 * Плагин добавляет во все компоненты функцию для форматирования денежных значений.
 * Использование:
 * В компоненте вызвать:
 *
 * this.$moneyFormat(str)
 *
 * Например: 1000 преобразуется в 1 000
 *           123213 перобразуется в 123 213
 * ----------
 *  const str = 1000;
 *  const str2 = this.$moneyFormat(str);
 *  console.log(str, str2) // выдаст -> 1000, 1 000
 * ----------
 */
import moneyFormat from '../utils/MoneyFormat';

const MoneyFormatPlugin = {
  install(Vue) {
    // eslint-disable-next-line
    Vue.prototype.$moneyFormat = moneyFormat;
  },
};

export { MoneyFormatPlugin as default };
