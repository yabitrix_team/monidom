import Api from '../api/register';

const ApiPlugin = {
  install(Vue) {
    const api = new Api();
    // eslint-disable-next-line
    Vue.prototype.$api = api;
  },
};

export { ApiPlugin as default };
