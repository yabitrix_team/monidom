import Api from '../api/admin';

const ApiPlugin = {
  install(Vue) {
    const api = new Api();
    // eslint-disable-next-line
    Vue.prototype.$api = api;
  },
};

export { ApiPlugin as default };
