import error from '../api/errorStore';

const ApiPlugin = {
  install(Vue) {
    // eslint-disable-next-line
    Vue.prototype.$error = error;
  },
};

export { ApiPlugin as default };
