// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ToggleButton from 'vue-js-toggle-button';
import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import Cleave from './components/Cleave';
import AppPrint from './AppPrint';
import router from './router/print';
import './assets/scss/index.scss';
import ToPlugin from './plugins/to';
import Api from './plugins/apiPrint';
import Validate from './plugins/validate';
import store from './store/print/';

Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(ToggleButton);
Vue.use(Validate);
Vue.component('v-select', vSelect);
Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);
Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { AppPrint },
  template: '<AppPrint/>',
});
