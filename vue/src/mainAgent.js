// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import Cleave from 'vue-cleave';
import VueQrcode from '@xkeshi/vue-qrcode';
import 'blueimp-canvas-to-blob';
import ToggleButton from 'vue-js-toggle-button';

import Cookies from 'js-cookie';
import VueClipboard from 'vue-clipboard2';
import AppAgent from './AppAgent';
import router from './router/agent';
import error from './plugins/error';
import './assets/scss/index.scss';
import store from './store/agent/';
import myFormatPlugin from './plugins/moneyformat';
import ToPlugin from './plugins/to';
import formatterPlugin from './plugins/formatter';
import config from './config';
import Api from './plugins/apiAgent';
import Datepicker from './components/Global/Datepicker';
import ValidateRu from './plugins/validateRu';
import Validate from './plugins/validate';
import VueNativeSock from './plugins/sockets/index';

Vue.use(error);
Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(myFormatPlugin);
Vue.use(formatterPlugin);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(
  VueNativeSock,
  `${config.ws.protocol}://${config.ws.server}?code=${Cookies.get('code')}`,
  {
    store,
    format: 'json',
    reconnection: true,
  },
);
Vue.use(ToggleButton);
Vue.use(VueClipboard);
Vue.use(Validate);
Vue.use(ValidateRu);

Vue.component('v-select', vSelect);
Vue.component('datepicker', Datepicker);
Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);
Vue.component('qrcode', VueQrcode);

Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { AppAgent },
  template: '<AppAgent/>',
});
