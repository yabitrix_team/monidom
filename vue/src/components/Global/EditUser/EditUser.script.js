import { mapGetters } from 'vuex';
import InputWrap from '../../InputWrap';

export default {
  data() {
    return {
      username: '',
      firstName: '',
      patronymic: '',
      lastName: '',
      role: '',
      validation: {
        username: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (typeof vue[e] === 'string' && vue[e].length < 1) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
              if (!vue.$validateLogin(vue[e])) {
                this.invalid = true;
                this.message = 'Некорректный логин';
              }
            }
          },
        },
        // firstName: {
        //   invalid: false,
        //   message: '',
        //   func(vue, e) {
        //     if (typeof vue[e] === 'string' && vue[e].length < 1) {
        //       this.invalid = true;
        //       this.message = 'Поле должно быть заполнено';
        //     } else {
        //       this.invalid = false;
        //       this.message = '';
        //     }
        //   },
        // },
        // patronymic: {
        //   invalid: false,
        //   message: '',
        //   func(vue, e) {
        //     this.invalid = false;
        //     this.message = '';
        //     if (String(vue[e]).length > 0 && !vue.$validateLastName(vue[e])) {
        //       this.invalid = true;
        //       this.message = 'Разрешены только русские символы';
        //     }
        //   },
        // },
        // lastName: {
        //   invalid: false,
        //   message: '',
        //   func(vue, e) {
        //     if (typeof vue[e] === 'string' && vue[e].length < 1) {
        //       this.invalid = true;
        //       this.message = 'Поле должно быть заполнено';
        //     } else {
        //       this.invalid = false;
        //       this.message = '';
        //     }
        //   },
        // },
        // role: {
        //   invalid: false,
        //   message: '',
        //   func(vue, e) {
        //     if (typeof vue[e] === 'string' && vue[e].length < 1) {
        //       this.invalid = true;
        //       this.message = 'Поле должно быть заполнено';
        //     } else {
        //       this.invalid = false;
        //       this.message = '';
        //     }
        //   },
        // },
      },
    };
  },
  created() {
    // eslint-disable-next-line
    Object.keys(this.user).forEach(el => this[el] = this.user[el]);
  },
  methods: {
    submit() {
      if (this.validate()) {
        this.$store.dispatch('editUser/updateUser',
          {
            username: this.user.username,
            newUsername: this.username,
          },
        );
      }
    },
  },
  computed: {
    ...mapGetters('editUser', [
      'user',
      'errorUpdate',
      'successUpdate',
    ]),
    ...mapGetters([
      'groups',
    ]),
  },
  components: {
    InputWrap,
  },
};
