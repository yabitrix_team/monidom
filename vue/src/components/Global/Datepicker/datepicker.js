import DateUtils from '@/utils/DateUtils';
import DateLanguages from '@/utils/DateLanguages';
import { directive as onClickaway } from 'vue-clickaway';
import moment from 'moment';

export default {
  directives: {
    onClickaway,
  },
  props: {
    value: {
      validator(val) {
        return val === null || val instanceof Date || typeof val === 'string' || typeof val === 'number';
      },
    },
    events: {
      type: Object,
      default: () => ({}),
    },
    name: String,
    refName: {
      type: String,
      default: 'input',
    },
    id: String,
    format: {
      type: [String, Function],
      default: 'dd MMM yyyy',
    },
    language: {
      type: String,
      default: 'en',
    },
    openDate: {
      validator(val) {
        return val === null || val instanceof Date || typeof val === 'string';
      },
    },
    fullMonthName: Boolean,
    disabled: Object,
    highlighted: Object,
    placeholder: String,
    inline: Boolean,
    calendarClass: [String, Object],
    inputClass: [String, Object],
    wrapperClass: [String, Object],
    mondayFirst: Boolean,
    clearButton: Boolean,
    clearButtonIcon: String,
    calendarButton: Boolean,
    calendarButtonIcon: String,
    calendarButtonIconContent: String,
    bootstrapStyling: Boolean,
    initialView: String,
    disabledPicker: Boolean,
    required: Boolean,
    minimumView: {
      type: String,
      default: 'day',
    },
    maximumView: {
      type: String,
      default: 'year',
    },
  },
  data() {
    const startDate = this.openDate ? new Date(this.openDate) : new Date();
    return {
      /*
       * Vue cannot observe changes to a Date Object so date must be stored as a timestamp
       * This represents the first day of the current viewing month
       * {Number}
       */
      pageTimestamp: startDate.setDate(1),
      /*
       * Selected Date
       * {Date}
       */
      selectedDate: null,
      /*
       * Flags to show calendar views
       * {Boolean}
       */
      showDayView: false,
      showMonthView: false,
      showYearView: false,
      /*
       * Positioning
       */
      calendarHeight: 0,
      tempInput: '',
    };
  },
  watch: {
    value(value) {
      this.setValue(value);
    },
    openDate() {
      this.setPageDate();
    },
    initialView() {
      this.setInitialView();
    },
  },
  computed: {
    computedInitialView() {
      if (!this.initialView) {
        return this.minimumView;
      }

      return this.initialView;
    },
    pageDate() {
      return new Date(this.pageTimestamp);
    },
    formattedValue() {
      if (this.tempInput) {
        return this.tempInput;
      }
      if (!this.selectedDate) {
        return null;
      }
      return typeof this.format === 'function'
        ? this.format(this.selectedDate)
        : DateUtils.formatDate(new Date(this.selectedDate), this.format, this.translation);
    },
    translation() {
      return DateLanguages.translations[this.language];
    },
    currMonthName() {
      const monthName =
        this.fullMonthName ? this.translation.months.original : this.translation.months.abbr;
      return DateUtils.getMonthNameAbbr(this.pageDate.getMonth(), monthName);
    },
    currYear() {
      return this.pageDate.getFullYear();
    },
    /**
     * Returns the day number of the week less one for the first of the current month
     * Used to show amount of empty cells before the first in the day calendar layout
     * @return {Number}
     */
    blankDays() {
      const d = this.pageDate;
      const dObj = new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());
      if (this.mondayFirst) {
        return dObj.getDay() > 0 ? dObj.getDay() - 1 : 6;
      }
      return dObj.getDay();
    },
    daysOfWeek() {
      if (this.mondayFirst) {
        const tempDays = this.translation.days.slice();
        tempDays.push(tempDays.shift());
        return tempDays;
      }
      return this.translation.days;
    },
    days() {
      const d = this.pageDate;
      const days = [];
      // set up a new date object to the beginning of the current 'page'
      const dObj = new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());
      const daysInMonth = DateUtils.daysInMonth(dObj.getFullYear(), dObj.getMonth());
      // eslint-disable-next-line
      for (let i = 0; i < daysInMonth; i++) {
        days.push({
          date: dObj.getDate(),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedDate(dObj),
          isDisabled: this.isDisabledDate(dObj),
          isHighlighted: this.isHighlightedDate(dObj),
          isHighlightStart: this.isHighlightStart(dObj),
          isHighlightEnd: this.isHighlightEnd(dObj),
          isToday: dObj.toDateString() === (new Date()).toDateString(),
          isWeekend: dObj.getDay() === 0 || dObj.getDay() === 6,
          isSaturday: dObj.getDay() === 6,
          isSunday: dObj.getDay() === 0,
        });
        dObj.setDate(dObj.getDate() + 1);
      }
      return days;
    },
    months() {
      const d = this.pageDate;
      const months = [];
      // set up a new date object to the beginning of the current 'page'
      const dObj = new Date(d.getFullYear(), 0, d.getDate(), d.getHours(), d.getMinutes());
      // eslint-disable-next-line
      for (let i = 0; i < 12; i++) {
        months.push({
          month: DateUtils.getMonthName(i, this.translation.months.original),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedMonth(dObj),
          isDisabled: this.isDisabledMonth(dObj),
        });
        dObj.setMonth(dObj.getMonth() + 1);
      }
      return months;
    },
    years() {
      const d = this.pageDate;
      const years = [];
      // set up a new date object to the beginning of the current 'page'
      const dObj =
        new Date(
          Math.floor(d.getFullYear() / 10) * 10,
          d.getMonth(),
          d.getDate(),
          d.getHours(),
          d.getMinutes());
      // eslint-disable-next-line
      for (let i = 0; i < 10; i++) {
        years.push({
          year: dObj.getFullYear(),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedYear(dObj),
          isDisabled: this.isDisabledYear(dObj),
        });
        dObj.setFullYear(dObj.getFullYear() + 1);
      }
      return years;
    },
    calendarStyle() {
      return {
        position: this.isInline ? 'static' : undefined,
      };
    },
    isOpen() {
      return this.showDayView || this.showMonthView || this.showYearView;
    },
    isInline() {
      return !!this.inline;
    },
    isRtl() {
      return this.translation.rtl === true;
    },
    isYmd() {
      return this.translation.ymd === true;
    },
  },
  methods: {
    /**
     * Close all calendar layers
     */
    close(full) {
      this.showDayView = false;
      this.showMonthView = false;
      this.showYearView = false;
      if (!this.isInline) {
        if (full) this.$emit('closed');
        document.removeEventListener('click', this.clickOutside, false);
      }
    },
    emitBlur() {
      if (this.value) {
        if (!moment(this.$refs[this.refName].value, 'DD.MM.YYYY').isValid()) {
          this.$emit('input', '');
        }
        this.$emit('blur');
      }
    },
    resetDefaultDate() {
      if (this.selectedDate === null) {
        this.setPageDate();
        return;
      }
      this.setPageDate(this.selectedDate);
    },
    /**
     * Effectively a toggle to show/hide the calendar
     * @return {mixed} [description]
     */
    showCalendar() {
      if (this.disabledPicker || this.isInline) {
        return false;
      }
      if (this.isOpen) {
        return this.close(true);
      }
      this.setInitialView();
      if (!this.isInline) {
        this.$emit('opened');
      }
      return false;
    },
    setInitialView() {
      const initialView = this.computedInitialView;

      if (!this.allowedToShowView(initialView)) {
        throw new Error(`initialView '${this.initialView}' cannot be rendered based on minimum '${this.minimumView}' and maximum '${this.maximumView}'`);
      }

      switch (initialView) {
        case 'year':
          this.showYearCalendar();
          break;
        case 'month':
          this.showMonthCalendar();
          break;
        default:
          this.showDayCalendar();
          break;
      }
    },
    allowedToShowView(view) {
      const views = ['day', 'month', 'year'];
      const minimumViewIndex = views.indexOf(this.minimumView);
      const maximumViewIndex = views.indexOf(this.maximumView);
      const viewIndex = views.indexOf(view);

      return viewIndex >= minimumViewIndex && viewIndex <= maximumViewIndex;
    },
    showDayCalendar() {
      if (!this.allowedToShowView('day')) return false;

      this.close();
      this.showDayView = true;
      this.addOutsideClickListener();

      return false;
    },
    showMonthCalendar() {
      if (!this.allowedToShowView('month')) return false;

      this.close();
      this.showMonthView = true;
      this.addOutsideClickListener();

      return false;
    },
    showYearCalendar() {
      if (!this.allowedToShowView('year')) return false;

      this.close();
      this.showYearView = true;
      this.addOutsideClickListener();

      return false;
    },
    addOutsideClickListener() {
      if (!this.isInline) {
        setTimeout(() => {
          document.addEventListener('click', this.clickOutside, false);
        }, 100);
      }
    },
    setDate(timestamp) {
      let date = new Date(timestamp);
      date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
      this.selectedDate = new Date(date);
      this.setPageDate(date);
      this.$emit('selected', new Date(date));
      this.$emit('input', new Date(date));
    },
    clearDate() {
      this.selectedDate = null;
      this.$emit('selected', null);
      this.$emit('input', null);
      this.$emit('cleared');
    },
    /**
     * @param {Object} day
     */
    selectDate(day) {
      this.tempInput = null;
      if (day.isDisabled) {
        this.$emit('selectedDisabled', day);
        return false;
      }
      this.setDate(day.timestamp);
      if (!this.isInline) {
        this.close(true);
      }

      return false;
    },
    /**
     * @param {Object} month
     */
    selectMonth(month) {
      if (month.isDisabled) {
        return false;
      }

      const date = new Date(month.timestamp);
      if (this.allowedToShowView('day')) {
        this.setPageDate(date);
        this.$emit('changedMonth', month);
        this.showDayCalendar();
      } else {
        this.setDate(date);
        if (!this.isInline) {
          this.close(true);
        }
      }
      return false;
    },
    /**
     * @param {Object} year
     */
    selectYear(year) {
      if (year.isDisabled) {
        return false;
      }

      const date = new Date(year.timestamp);
      if (this.allowedToShowView('month')) {
        this.setPageDate(date);
        this.$emit('changedYear', year);
        this.showMonthCalendar();
      } else {
        this.setDate(date);
        if (!this.isInline) {
          this.close(true);
        }
      }
      return false;
    },
    /**
     * @return {Number}
     */
    getPageDate() {
      return this.pageDate.getDate();
    },
    /**
     * @return {Number}
     */
    getPageMonth() {
      return this.pageDate.getMonth();
    },
    /**
     * @return {Number}
     */
    getPageYear() {
      return this.pageDate.getFullYear();
    },
    /**
     * @return {String}
     */
    getPageDecade() {
      const decadeStart = Math.floor(this.pageDate.getFullYear() / 10) * 10;
      const decadeEnd = decadeStart + 9;
      return `${decadeStart} - ${decadeEnd}`;
    },
    changeMonth(incrementBy) {
      const date = this.pageDate;
      date.setMonth(date.getMonth() + incrementBy);
      this.setPageDate(date);
      this.$emit('changedMonth', date);
    },
    previousMonth() {
      if (!this.previousMonthDisabled()) {
        this.changeMonth(-1);
      }
    },
    previousMonthDisabled() {
      if (!this.disabled || !this.disabled.to) {
        return false;
      }
      const d = this.pageDate;
      return this.disabled.to.getMonth() >= d.getMonth() &&
        this.disabled.to.getFullYear() >= d.getFullYear();
    },
    nextMonth() {
      if (!this.nextMonthDisabled()) {
        this.changeMonth(+1);
      }
    },
    nextMonthDisabled() {
      if (!this.disabled || !this.disabled.from) {
        return false;
      }
      const d = this.pageDate;
      return this.disabled.from.getMonth() <= d.getMonth() &&
        this.disabled.from.getFullYear() <= d.getFullYear();
    },
    changeYear(incrementBy, emit = 'changedYear') {
      const date = this.pageDate;
      date.setYear(date.getFullYear() + incrementBy);
      this.setPageDate(date);
      this.$emit(emit, date);
    },
    previousYear() {
      if (!this.previousYearDisabled()) {
        this.changeYear(-1);
      }
    },
    previousYearDisabled() {
      if (!this.disabled || !this.disabled.to) {
        return false;
      }
      return this.disabled.to.getFullYear() >= this.pageDate.getFullYear();
    },
    nextYear() {
      if (!this.nextYearDisabled()) {
        this.changeYear(1);
      }
    },
    nextYearDisabled() {
      if (!this.disabled || !this.disabled.from) {
        return false;
      }
      return this.disabled.from.getFullYear() <= this.pageDate.getFullYear();
    },
    previousDecade() {
      if (!this.previousDecadeDisabled()) {
        this.changeYear(-10, 'changeDecade');
      }
    },
    previousDecadeDisabled() {
      if (!this.disabled || !this.disabled.to) {
        return false;
      }
      return Math.floor(this.disabled.to.getFullYear() / 10) * 10 >=
        Math.floor(this.pageDate.getFullYear() / 10) * 10;
    },
    nextDecade() {
      if (!this.nextDecadeDisabled()) {
        this.changeYear(10, 'changeDecade');
      }
    },
    nextDecadeDisabled() {
      if (!this.disabled || !this.disabled.from) {
        return false;
      }
      return Math.ceil(this.disabled.from.getFullYear() / 10) * 10 <=
        Math.ceil(this.pageDate.getFullYear() / 10) * 10;
    },
    /**
     * Whether a day is selected
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedDate(dObj) {
      return this.selectedDate && this.selectedDate.toDateString() === dObj.toDateString();
    },
    /**
     * Whether a day is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledDate(date) {
      let disabled = false;

      if (typeof this.disabled === 'undefined') {
        return false;
      }

      if (typeof this.disabled.dates !== 'undefined') {
        this.disabled.dates.forEach((d) => {
          if (date.toDateString() === d.toDateString()) {
            disabled = true;
            return true;
          }
          return false;
        });
      }
      if (typeof this.disabled.to !== 'undefined' && this.disabled.to && date < this.disabled.to) {
        disabled = true;
      }
      if (typeof this.disabled.from !== 'undefined' && this.disabled.from && date > this.disabled.from) {
        disabled = true;
      }
      if (typeof this.disabled.ranges !== 'undefined') {
        this.disabled.ranges.forEach((range) => {
          if (typeof range.from !== 'undefined' && range.from && typeof range.to !== 'undefined' && range.to) {
            if (date < range.to && date > range.from) {
              disabled = true;
              return true;
            }
          }
          return false;
        });
      }
      if (typeof this.disabled.days !== 'undefined' && this.disabled.days.indexOf(date.getDay()) !== -1) {
        disabled = true;
      }
      if (typeof this.disabled.daysOfMonth !==
        'undefined' && this.disabled.daysOfMonth.indexOf(date.getDate()) !== -1) {
        disabled = true;
      }
      if (typeof this.disabled.customPredictor === 'function' && this.disabled.customPredictor(date)) {
        disabled = true;
      }
      return disabled;
    },
    /**
     * Whether a day is highlighted (only if it is not disabled already)
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightedDate(date) {
      if (this.isDisabledDate(date)) {
        return false;
      }

      let highlighted = false;

      if (typeof this.highlighted === 'undefined') {
        return false;
      }

      if (typeof this.highlighted.dates !== 'undefined') {
        this.highlighted.dates.forEach((d) => {
          if (date.toDateString() === d.toDateString()) {
            highlighted = true;
            return true;
          }

          return false;
        });
        return false;
      }

      if (this.isDefined(this.highlighted.from) && this.isDefined(this.highlighted.to)) {
        highlighted = date >= this.highlighted.from && date <= this.highlighted.to;
      }

      if (typeof this.highlighted.days !== 'undefined' && this.highlighted.days.indexOf(date.getDay()) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.daysOfMonth !==
          'undefined' && this.highlighted.daysOfMonth.indexOf(date.getDate()) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.customPredictor === 'function' && this.highlighted.customPredictor(date)) {
        highlighted = true;
      }

      return highlighted;
    },
    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightStart(date) {
      return this.isHighlightedDate(date) &&
        (this.highlighted.from instanceof Date) &&
        (this.highlighted.from.getFullYear() === date.getFullYear()) &&
        (this.highlighted.from.getMonth() === date.getMonth()) &&
        (this.highlighted.from.getDate() === date.getDate());
    },
    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightEnd(date) {
      return this.isHighlightedDate(date) &&
        (this.highlighted.to instanceof Date) &&
        (this.highlighted.to.getFullYear() === date.getFullYear()) &&
        (this.highlighted.to.getMonth() === date.getMonth()) &&
        (this.highlighted.to.getDate() === date.getDate());
    },
    /**
     * Helper
     * @param  {mixed}  prop
     * @return {Boolean}
     */
    isDefined(prop) {
      return typeof prop !== 'undefined' && prop;
    },
    /**
     * Whether the selected date is in this month
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedMonth(date) {
      return (this.selectedDate &&
        this.selectedDate.getFullYear() === date.getFullYear() &&
        this.selectedDate.getMonth() === date.getMonth());
    },
    /**
     * Whether a month is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledMonth(date) {
      let disabled = false;

      if (typeof this.disabled === 'undefined') {
        return false;
      }

      if (typeof this.disabled.to !== 'undefined' && this.disabled.to) {
        if (
          (date.getMonth() < this.disabled.to.getMonth() && date.getFullYear() <=
            this.disabled.to.getFullYear()) ||
            date.getFullYear() < this.disabled.to.getFullYear()
        ) {
          disabled = true;
        }
      }
      if (typeof this.disabled.from !== 'undefined' && this.disabled.from) {
        // eslint-disable-next-line
        if (this.disabled.from && (date.getMonth() > this.disabled.from.getMonth() && date.getFullYear() >= this.disabled.from.getFullYear()) || date.getFullYear() > this.disabled.from.getFullYear()) {
          disabled = true;
        }
      }
      return disabled;
    },
    /**
     * Whether the selected date is in this year
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedYear(date) {
      return this.selectedDate && this.selectedDate.getFullYear() === date.getFullYear();
    },
    /**
     * Whether a year is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledYear(date) {
      let disabled = false;
      if (typeof this.disabled === 'undefined' || !this.disabled) {
        return false;
      }

      if (typeof this.disabled.to !== 'undefined' && this.disabled.to) {
        if (date.getFullYear() < this.disabled.to.getFullYear()) {
          disabled = true;
        }
      }
      if (typeof this.disabled.from !== 'undefined' && this.disabled.from) {
        if (date.getFullYear() > this.disabled.from.getFullYear()) {
          disabled = true;
        }
      }

      return disabled;
    },
    /**
     * Set the datepicker value
     * @param {Date|String|Number|null} date
     */
    setValue(d) {
      let date = d;
      if (typeof date === 'string' || typeof date === 'number') {
        const parsed = new Date(date);
        date = isNaN(parsed.valueOf()) ? null : parsed;
      }
      if (!date) {
        this.setPageDate();
        this.selectedDate = null;
        return;
      }
      this.selectedDate = date;
      this.setPageDate(date);
    },
    setPageDate(d) {
      let date = d;
      if (!date) {
        if (this.openDate) {
          date = new Date(this.openDate);
        } else {
          date = new Date();
        }
      }
      this.pageTimestamp = (new Date(date)).setDate(1);
    },
    /**
     * Close the calendar if clicked outside the datepicker
     * @param {Event} event
     */
    clickOutside(event) {
      if (this.$el && !this.$el.contains(event.target)) {
        if (this.isInline) {
          return this.showDayCalendar();
        }
        this.resetDefaultDate();
        this.close(true);
        document.removeEventListener('click', this.clickOutside, false);
      }
      return false;
    },
    dayClasses(day) {
      return {
        selected: day.isSelected,
        disabled: day.isDisabled,
        highlighted: day.isHighlighted,
        today: day.isToday,
        weekend: day.isWeekend,
        sat: day.isSaturday,
        sun: day.isSunday,
        'highlight-start': day.isHighlightStart,
        'highlight-end': day.isHighlightEnd,
      };
    },
    init() {
      if (this.value) {
        this.setValue(this.value);
      }
      if (this.isInline) {
        this.setInitialView();
      }
    },
    directInput(e) {
      let val = this.$refs[this.refName].value;
      // запоминаем положение курсора
      let cursor = this.$refs[this.refName].selectionStart;
      /*
      * Очищаем дату при вводе в поле
      */
      this.selectedDate = null;

      // убираем точки из даты
      val = val.replace(/\.|\D/g, '');
      const arr = val.split('');
      if (arr.length > 8) {
        arr.splice(-1, 1);
      }

      // добавляем точки
      if (e.inputType === 'deleteContentBackward') {
        if (val.length >= 3) {
          arr.splice(2, 0, '.');
        }
        if (val.length >= 5) {
          arr.splice(5, 0, '.');
        }
      } else {
        if (val.length >= 2) {
          arr.splice(2, 0, '.');
        }
        if (val.length >= 4) {
          arr.splice(5, 0, '.');
        }
      }
      val = arr.join('');
      // this.$refs[this.refName].value = val;
      this.tempInput = val;
      this.$refs[this.refName].value = val;
      // set selected date
      this.getClosestDate(val);
      // устанавливаем положение курсора
      if (cursor > 0) {
        if (val[cursor] === '.' &&
          e.inputType !== 'deleteContentBackward' &&
          e.inputType !== 'deleteContentForward') {
          cursor += 1;
        }
      }
      this.$refs[this.refName].setSelectionRange(cursor, cursor);
    },
    // получает ближайшую доступную дату
    getClosestDate(date) {
      if (!date) {
        return {
          error: true,
          message: 'Нет даты',
        };
      }
      if (moment(date, 'DD.MM.YYYY').isValid()) {
        this.setDate(moment(date, 'DD.MM.YYYY').toDate());
        return {
          success: true,
        };
      }
      const dateArr = date.match(/^(\d{1,2})\.?(\d{1,2})?\.?(\d{4})?/);

      // составляем дату для показа в пикере
      const d = new Date();
      if (this.disabled !== undefined && this.disabled.to !== undefined) {
        d.setTime(this.disabled.to.getTime());
      }
      // получаем день в текущем диапазоне
      if (d.getDate() > dateArr[1]) {
        d.setMonth(d.getMonth() + 1);
        d.setDate(dateArr[1]);
      } else {
        d.setDate(dateArr[1]);
      }

      // ставим месяц
      if (dateArr[2] !== undefined) {
        d.setMonth(+dateArr[2] - 1); // исчисление месяцев в методе с 0
      }
      if (dateArr[3] !== undefined) {
        d.setYear(dateArr[3]);
      }
      this.setDate(d.getTime());

      return {
        success: true,
      };
    },
  },
  mounted() {
    this.init();
    Object.keys(this.events).map((key) => {
      this.$refs[this.refName].addEventListener(key, this.events[key]);
      return true;
    });
  },
};
