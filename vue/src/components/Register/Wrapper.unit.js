import Wrapper from './Wrapper';

describe('Registration Wrapper', () => {
  let vm = {};
  let params = { step: 0 };
  beforeEach(() => {
    vm = shallowMount(Wrapper, createComponentMocks({
      mocks: {
        $route: {
          params: {
            code: 'dsd',
          },
        },
      },
      store: {
        main: {
          state: {
            step: 1,
            currentUser: {
              name: 'My Name',
            },
          },
          getters: {
            step: () => params.step,
          },
        },
        lead: {
          getters: {
            clientFirstName: () => 'Иван',
            clientPatronymic: () => 'Иванович',
            clientLastName: () => 'Иванов',
            clientMobilePhone: () => '9000000000'
          }
        },
      },
    }));
  });
  it('renders right content', () => {
    expect(vm.element.innerHTML).toBeDefined();
  });
  // it('v-for and v-if works', () => {
  //   params.step = 1;
  //   expect(vm.element.innerHTML).toBeDefined();
  // })
});
