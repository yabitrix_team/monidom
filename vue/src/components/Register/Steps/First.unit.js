import First from './First';
import Validate from '@/plugins/validate';

describe('Регистрация с лидом', () => {
  let wrapper = {};
  let params = { step: 0 };
  let mockObj = {
    mocks: {
      $route: {
        params: {
          code: 'dsd12345',
        },
      },
    },
    store: {
      main: {
        state: {
          step: 1,
          login: '(999) 999 99-99',
          lead: {
            clientFirstName: 'Иван',
            clientPatronymic: '9000000000',
            clientLastName: 'Иван',
            clientMobilePhone: '9000000000',
          },
        },
        getters: {
          step: () => params.step,
          firstName: state => state.lead.clientFirstName,
          mobilePhone: state => state.login,
          isLead: () => true,
        },
        mutations: {
          set(s) {
            console.log('s');
            s.login = '(999) 999 99-99';
          },
        }
      },
    },
  };
  const localVue = createLocalVue();
  localVue.directive('mask', () => {});
  localVue.use(Validate);

  beforeEach(() => {
    wrapper = mount(First,
      Object.assign({}, {
        ...createComponentMocks(mockObj, localVue),
        attachToDocument: true
      }));
  });

  it('Показывает правильное приветствие если есть лид', () => {
    // arrange and act
    expect(wrapper.html()).toContain('Иван');
    expect(wrapper.html()).toContain('Здравствуйте');
    // mockObj.store.lead.state.clientFirstName = '';
    // assert
    expect(wrapper.text()).toContain('Иван');
    expect(wrapper.text()).toContain('Здравствуйте');
  });

  it('Ввод телефона заблокирован', () => {
    expect(wrapper.find('#register-input-login').html()).toContain('disabled="disabled"');
  });

  it('Если все поля заполнены, вызывается мутация', () => {
    // arrange
    wrapper.vm.login = '999 999 99 99'
    wrapper.vm.password = '^7yy=Ufsf';
    wrapper.vm.passwordRepeat = '^7yy=Ufsf';
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).not.toContain('Поле должно быть заполнено');
  });


});


describe('Регистрация без лида', () => {
  let wrapper = {};
  let params = { step: 0 };
  let mockObj = {
    store: {
      main: {
        state: {
          step: 1,
          login: '',
          lead: {
            clientFirstName: '',
            clientPatronymic: '',
            clientLastName: '',
            clientMobilePhone: '',
          },
        },
        getters: {
          step: () => '',
          firstName: () => '',
          mobilePhone: () => '',
          isLead: () => true,
        },
        mutations: {
          set(s) {
            console.log('s');
            s.login = '(999) 999 99-99';
          },
        }
      },
    },
  };

  const localVue = createLocalVue();
  localVue.directive('mask', () => {});
  localVue.use(Validate);


  beforeEach(() => {
    wrapper = mount(First,
      Object.assign({}, {
        ...createComponentMocks(mockObj, localVue),
        attachToDocument: true
      }));
  });

  // it('phone input UNlocked if we doesn\'t have the lead', () => {
  //   // arrange and act
  //   // mockObj.store.lead.state.clientMobilePhone = '';
  //   wrapper = mount(First,
  //   {
  //     ...createComponentMocks(mockObj, localVue),
  //     attachToDocument: true
  //   });
  //   // assert
  //   expect(wrapper.find('#register-input-login').html()).not.toContain('disabled="disabled"');
  // });
  // it('Validation fires', () => {
  //   console.log('this', wrapper.wm);
  //   // arrange
  //   wrapper.vm.login = '';
  //   wrapper.vm.password = '';
  //   wrapper.vm.passwordRepeat = '';
  //   // act
  //   wrapper.vm.validate();
  //   wrapper.vm.$forceUpdate();
  //   // assert
  //   expect(wrapper.text()).toContain('Поле должно быть заполнено');
  // });

  // it('Ввод телефона заблокирован', () => {
  //   expect(wrapper.find('#register-input-login').html()).toContain('disabled="disabled"');
  // });
});
