import First from './Second';
import Validate from '@/plugins/validate';

describe('Registration Second', () => {
  let wrapper = {};
  let params = { step: 0 };

  let mockObj = {
    mocks: {
      $route: {
        params: {
          code: 'dsd',
        },
      },
    },
    store: {
      main: {
        state: {
          lead: {
            clientFirstName: 'Иван',
            clientPatronymic: 'Иванович',
            clientLastName: 'Иванов',
            clientMobilePhone: '9000000000',
          },
        },
        getters: {
          step: () => params.step,
          firstName: state => state.lead.clientFirstName || '',
          patronymic: state => state.lead.clientPatronymic || '',
          lastName: state => state.lead.clientLastName || '',
          mobilePhone: state => state.lead.clientMobilePhone || '',
        },
      },
    },
  };
  const localVue = createLocalVue();
  localVue.directive('mask', () => {});
  localVue.use(Validate);

  beforeEach(() => {
    wrapper = mount(First,
      Object.assign({}, {
        ...createComponentMocks(mockObj, localVue),
        attachToDocument: true
      }));
  });

  it('Блокирует кнопку, если не поставлена галочка', () => {
    expect(wrapper.find('.button').html()).toContain('disabled-button"');
  });

  it('Разблокирует кнопку, если поставлена галочка', () => {
    wrapper.vm.signature = true;
    expect(wrapper.find('.button').html()).not.toContain('disabled-button"');
  });

  it('Показывает валидацию если не заполнены поля', () => {
    // arrange
    wrapper.vm.signature = true;
    wrapper.vm.lastName = '';
    wrapper.vm.firstName = '';
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).toContain('Поле должно быть заполнено');
  });

  it('Не оказывает валидацию если заполнены поля имени и фамилии', () => {
    // arrange
    wrapper.vm.signature = true;
    wrapper.vm.firstName = 'Иван';
    wrapper.vm.lastName = 'Иванов';
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).not.toContain('Поле должно быть заполнено');
  });

  it('Имя, фамилия и отчество автозаполняются с лида', () => {
    // arrange
    wrapper.vm.signature = true;
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.vm.firstName).toContain('Иван');
    expect(wrapper.vm.patronymic).toContain('Иванович');
    expect(wrapper.vm.lastName).toContain('Иванов');
  });
});
