import First from './Third';
import Validate from '@/plugins/validate';

describe('Registration Third', () => {
  let wrapper = {};

  let mockObj = {
    store: {
      main: {
        getters: {
          timerMin: () => 0,
          timerSec: () => 0,
          mobilePhone: () => '(999) 999 99-99',
        },
        mutations: {
          UPDATE_TIMER: () => '',
        },
        actions: {
          sendCode: () => '',
        }
      },
    },
  };
  const localVue = createLocalVue();
  localVue.directive('mask', () => {});
  localVue.use(Validate);

  beforeEach(() => {
    wrapper = mount(First,
      Object.assign({}, {
        ...createComponentMocks(mockObj, localVue),
        attachToDocument: true
      }));
  });

  it('Показывает валидацию если не заполнены поля', () => {
    // arrange
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).toContain('Поле должно быть заполнено');
  });

  it('Не оказывает валидацию если заполнено поле', () => {
    // arrange
    wrapper.vm.smsCode = '1234';
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).not.toContain('Поле должно быть заполнено');
  });

  it('Правильно отображает мобильный телефон', () => {
    // arrange
    // act
    wrapper.vm.validate();
    wrapper.vm.$forceUpdate();
    // assert
    expect(wrapper.text()).toContain('Код отправлен на +7 (999) 999 99-99');
  });
});
