import First from './Fourth';


describe('Registration Fourth', () => {
  let wrapper = {};
  let params = { step: 0 };

  let mockObj = {
    mocks: {
      $route: {
        params: {
          code: 'dsd',
        },
      },
    },
    store: {
      main: {
        state: {
          lead: {
            clientFirstName: 'Иван',
            clientPatronymic: 'Иванович',
            clientLastName: 'Иванов',
            clientMobilePhone: '9000000000',
          },
        },
        getters: {
          step: () => params.step,
          firstName: () => 'Иван',
        },
        mutations: {
          UPDATE_TIMER: () => '',
        },
        actions: {
          sendCode: () => '',
        }
      },
    },
  };


  beforeEach(() => {
    wrapper = mount(First, {
        ...createComponentMocks(mockObj),
        attachToDocument: true
    });
  });

  it('Правильно отображает имя пользователя', () => {
    // assert
    expect(wrapper.text()).toContain('Иван');
  });
});
