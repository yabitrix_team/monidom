import First from './RegistrationDocs';
import Validate from '@/plugins/validate';

const RouterLink = {
  name: 'router-link',
  render: function (h) {
    return h('div', this.$slots.default)
  },
  props: ['to']
}
describe('Registration RegistrationDocs', () => {
  let wrapper = {};
  let mockObj = {
    stubs: {
      RouterLink,
    },
    mocks: {
      $route: {
        params: {
          meta: {},
        },
      },
    },
    store: {
      main: {
        getters: {
          docs: () => [
            {
              code: 'soglashenie_o_elektronnom_vzaimodejstvii',
              label: 'Текстовый документ',
              html: '<h1>Пример документа</h1>',
            },
          ],
        },
        actions: {
          docs: () => '',
          loadDocs: () => '',
        }
      },
    },
  };

  beforeEach(() => {
    wrapper = shallowMount(First, {
          ...createComponentMocks(mockObj),
          attachToDocument: true
      });
  });

  it('Отображает список документов', () => {
    // assert
    expect(wrapper.text()).toContain('Текстовый документ');
  });

});
