import moment from 'moment';

const years = [];
// eslint-disable-next-line
for (let i = moment().year(); i >= 1919; i--) {
  years.push(
    {
      id: i,
      name: String(i),
    },
  );
}
export default {
  autoYears: years,
  addCredits: [
    {
      label: 'Нет',
      value: '1',
    },
    {
      label: 'Докредитование под текущий залог',
      value: '2',
    },
    {
      label: 'Параллельный заем',
      value: '3',
    },
  ],
};
