import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Manager/Index';
import Graph from '@/components/Manager/Graph';
import $store from '../store/manager';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
    },
    {
      path: '/graph',
      name: 'Graph',
      component: Graph,
      beforeEnter(to, from, next) {
        if (Object.keys($store.getters.graph).length < 1) {
          next('/');
        } else {
          next();
        }
      },
    },
  ],
  // eslint-disable-next-line
  // scrollBehavior(to, from, savedPosition) {
  //   return { x: 0, y: 0 };
  // },
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }
});

export { router as default };
