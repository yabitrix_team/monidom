import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Partner/Index';
import Instructions from '@/components/Global/Instructions';
import PMpage from '@/components/Partner/PMpage';
import Client from '@/components/Partner/Clients';
import Income from '@/components/Partner/Income';
import About from '@/components/Partner/About';
import History from '@/components/Partner/History';
import PageNotFound from '../PageNotFound';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/listclients',
    },
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: { auth: true },
    },
    {
      path: '/history/',
      name: 'History',
      component: History,
      meta: { auth: true },
      children: [{ path: '/history/:page', component: History }],
    },
    {
      path: '/instructions',
      name: 'Instructions',
      component: Instructions,
      meta: { auth: true },
    },
    {
      path: '/pm',
      name: 'PMpage',
      component: PMpage,
      meta: { auth: true },
    },
    {
      path: '/listclients',
      name: 'Client',
      component: Client,
      meta: {
        auth: true,
        admin: true,
      },
    },
    {
      path: '/listclients/:type',
      name: 'Client',
      component: Client,
      meta: {
        auth: true,
        admin: true,
      },
    },
    {
      path: '/listclients/:type/:page',
      name: 'Client',
      component: Client,
      meta: {
        auth: true,
        admin: true,
      },
    },
    {
      path: '/listclients/:type/:dateFrom/:dateTo/:page',
      name: 'Client',
      component: Client,
      meta: {
        auth: true,
        admin: true,
      },
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: { auth: true },
    },
    {
      path: '/income',
      name: 'Income',
      component: Income,
      meta: { auth: true },
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }
});

export { router as default };
