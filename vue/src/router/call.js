import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';

import Index from '../components/Call/Index';
import Edit from '../components/Call/Edit';
import PageNotFound from '../PageNotFound';
import Api from '../api/call';
import Url from '../utils/UrlHelper';
import $store from '../store/call';


Vue.use(Router);
// eslint-disable-next-line
function beforeEnterCallback(to, from, next) {
  const userRole = $store.state.user_info.role;
  if (userRole !== 'admin') {
    next('/auth');
  } else {
    next();
  }
}

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/items/1',
    },
    {
      path: '/items',
      redirect: '/items/1',
    },
    {
      path: '/items/:page',
      component: Index,
      meta: { auth: false },
    },
    {
      path: '/items/item/:code',
      component: Edit,
      meta: { auth: false },
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },

  ],
  // eslint-disable-next-line
  // scrollBehavior(to, from, savedPosition) {
  //   return { x: 0, y: 0 };
  // },
});


router.afterEach((to) => {
  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch(() => {
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});


export { router as default };
