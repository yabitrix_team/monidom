import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import Index from '@/components/Partner/Index';
import Instructions from '@/components/Global/Instructions';
import PMpage from '@/components/Partner/PMpage';
import ClientWrap from '@/components/Partner/ClientsWrap';
import Client from '@/components/Partner/Clients';
import Income from '@/components/Partner/Income';
import About from '@/components/Partner/About';
import History from '@/components/Partner/History';
import VerificationList from '@/components/VerificationList';
import DocumentList from '@/components/DocumentList';
import DocView from '@/components/DocView';
import StepPreRequest from '@/components/Partner/StepPreRequest';
import Request from '@/components/Request';
import Graph from '@/components/Graph';
import Edit from '@/components/Partner/Edit';
import $store from '../store/partner';
import Api from '../api/partner';
import Url from '../utils/UrlHelper';
import stepPosition from '../utils/StepPosition';
import PageNotFound from '../PageNotFound';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: stepPosition() || 0 };
  },
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: { auth: true },
    },
    {
      path: '/graph',
      name: 'Graph',
      component: Graph,
      meta: { auth: true },
      beforeEnter(to, from, next) {
        if ($store.state.calculator.summ) {
          next();
        } else {
          next('/');
        }
      },
    },
    {
      path: '/history/',
      name: 'History',
      component: History,
      meta: { auth: true },
      children: [{ path: '/history/:page', component: History }],
    },
    {
      path: '/instructions',
      name: 'Instructions',
      component: Instructions,
      meta: { auth: true },
    },
    {
      path: '/rules',
      name: 'Documents',
      component: DocumentList,
      meta: { auth: true, documentList: 'treaty_general,information,regulations_individual,information_bulk,soglashenie_o_chastote_vzaimodejstviya_lkp' },
    },
    {
      path: '/rules/:document',
      name: 'SingleDocument',
      meta: { ...this.meta },
      component: DocView,
    },
    {
      path: '/pm',
      name: 'PMpage',
      component: PMpage,
      meta: { auth: true },
    },
    {
      path: '/listclients',
      name: 'Client',
      component: ClientWrap,
      children: [
        {
          path: ':type',
          name: 'ClientByType',
          component: Client,
          meta: { auth: true },
        },
        {
          path: ':type/:page',
          name: 'ClientByTypeAndPage',
          component: Client,
          meta: { auth: true },
        },
        {
          path: ':type/:dateFrom/:dateTo/:page',
          name: 'ClientByTypePageAndDate',
          component: Client,
          meta: { auth: true },
        },
      ],
      meta: { auth: true },
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: { auth: true },
    },
    {
      path: '/income',
      name: 'Income',
      component: Income,
      meta: { auth: true },
    },
    {
      path: '/vlist',
      name: 'VerificationList',
      component: VerificationList,
      meta: { auth: true },
    },
    {
      path: '/request',
      component: Request,
      children: [
        {
          path: '/',
          name: 'Create',
          beforeEnter(to, from, next) {
            const step = $store.state.request.step;
            const summ = $store.state.request.summ;
            if ((step && +step === 2) || !summ) {
              next('/');
            } else {
              next();
            }
          },
          component: StepPreRequest,
          meta: { auth: true },
        },
        {
          path: ':code',
          name: 'Edit',
          beforeEnter(to, from, next) {
            $store.dispatch('getRequest', { code: to.params.code, next });
          },
          component: Edit,
          meta: {
            auth: true,
            fixed: true,
          },
        },
      ],
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }

  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
