import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login/Login';
import LostPassword from '@/components/Login/LostPassword/LostPassword';
import NewPassword from '@/components/Login/NewPassword';
import PageNotFound from '../PageNotFound';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
    },
    {
      path: '/new/:code',
      name: 'NewPassword',
      component: NewPassword,
    },
    {
      path: '/forget',
      name: 'LostPassword',
      component: LostPassword,
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }
});

export { router as default };
