import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import Index from '@/components/Agent/Index';
import Edit from '@/components/Agent/Edit';
import Instructions from '@/components/Agent/InstructionsWrapper';
import VerificationListWrap from '@/components/Agent/VerificationListWrap';
import DocumentListWrapper from '@/components/Agent/DocumentListWrapper';
import DocumentViewWrapper from '@/components/Agent/DocumentViewWrapper';
import Api from '../api/agent';
import Url from '../utils/UrlHelper';
import stepPosition from '../utils/StepPosition';
import PageNotFound from '../PageNotFound';
import $store from '../store/agent';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: stepPosition() || 0 };
  },
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: { auth: true },
    },
    {
      path: '/request/:code',
      name: 'Edit',
      component: Edit,
      beforeEnter(to, from, next) {
        $store.dispatch('getRequest', { code: to.params.code, next });
      },
      meta: {
        auth: true,
        fixed: true,
      },
    },
    {
      path: '/vlist',
      name: 'VerificationList',
      component: VerificationListWrap,
      meta: { auth: true },
    },
    {
      path: '/instructions',
      name: 'Instructions',
      component: Instructions,
      meta: { auth: true },
    },
    {
      path: '/rules',
      name: 'Documents',
      component: DocumentListWrapper,
      meta: { auth: true, documentList: 'treaty_general,information,regulations_individual,information_bulk,soglashenie_o_chastote_vzaimodejstviya_lkp' },
    },
    {
      path: '/rules/:document',
      name: 'SingleDocument',
      meta: { ...this.meta },
      component: DocumentViewWrapper,
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }

  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
