import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
// import Registration from '@/components/Register/Registration';
import RegistrationWrapper from '@/components/Register/Wrapper';
import RegistrationDocs from '@/components/Register/RegistrationDocs';
import Api from '@/api/register';
import Url from '@/utils/UrlHelper';
import PageNotFound from '@/PageNotFound';
import config from '@/config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/documents/',
      name: 'RegistrationDocs',
      component: RegistrationDocs,
    },
    {
      path: '/documents/:code',
      name: 'RegistrationDoc',
      component: RegistrationDocs,
    },
    {
      path: '/:code',
      name: 'Registration',
      component: RegistrationWrapper,
    },
    {
      path: '/',
      name: 'Registration',
      component: RegistrationWrapper,
    },
    // {
    //   path: '/test/:code',
    //   name: 'RegistrationTest',
    //   component: Registration,
    // },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
  // eslint-disable-next-line
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }

  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
