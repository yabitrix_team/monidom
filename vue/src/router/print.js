import Vue from 'vue';
import Router from 'vue-router';
import Print from '@/components/Print/Print';
import PrintClear from '@/components/Print/PrintClear';
import PrintImage from '@/components/Print/PrintImage';
import PageNotFound from '../PageNotFound';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/:code',
      name: 'Print',
      component: Print,
    },

    {
      path: '/print/:code',
      name: 'PrintImage',
      component: PrintImage,
    },
    {
      path: '/',
      name: 'PrintClear',
      component: PrintClear,
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }
});

export { router as default };
