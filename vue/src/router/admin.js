import Vue from 'vue';
import moment from 'moment';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import Index from '@/components/Partner/Index';
import Instructions from '@/components/Global/Instructions';
import PMpage from '@/components/Partner/PMpage';
import Client from '@/components/Partner/Clients';
import Income from '@/components/Partner/Income';
import ClientWrap from '@/components/Partner/ClientsWrap';
import About from '@/components/Partner/About';
import History from '@/components/Partner/History';
import Api from '../api/admin';
import Url from '../utils/UrlHelper';
import PageNotFound from '../PageNotFound';
import config from '../config';

Vue.use(Router);

const meta = {
  auth: true,
  admin: true,
};

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: `/listclients/work/${moment().subtract(1, 'months').format('DD.MM.YYYY')}/${moment()
        .format('DD.MM.YYYY')}/1`,
    },
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta,
    },
    {
      path: '/history/',
      name: 'History',
      component: History,
      meta,
      children: [{ path: '/history/:page', component: History }],
    },
    {
      path: '/instructions',
      name: 'Instructions',
      component: Instructions,
      meta,
    },
    {
      path: '/pm',
      name: 'PMpage',
      component: PMpage,
      meta,
    },
    {
      path: '/listclients',
      name: 'Client',
      component: ClientWrap,
      children: [
        {
          path: ':type',
          name: 'ClientByType',
          component: Client,
          meta,
        },
        {
          path: ':type/:page',
          name: 'ClientByTypeAndPage',
          component: Client,
          meta,
        },
        {
          path: ':type/:dateFrom/:dateTo/:page',
          name: 'ClientByTypePageAndDate',
          component: Client,
          meta,
        },
      ],
      meta,
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta,
    },
    {
      path: '/income',
      name: 'Income',
      component: Income,
      meta,
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta,
    },


  ],
  scrollBehavior(to) {
    if (to.hash) {
      return {
        selector: to.hash,
      };
    }
    return false;
  },
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }

  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
