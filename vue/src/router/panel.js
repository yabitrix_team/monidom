import Vue from 'vue';
import Router from 'vue-router';
import Users from '@/components/Panel/Users';
import Leads from '@/components/Panel/Leads';
import RequestInfo from '@/components/Panel/Request/RequestInfo';
import Auth from '@/components/Panel/Auth';
import Docs from '@/components/Panel/Docs';
import Cookies from 'js-cookie';
import Api from '../api/panel';
import Url from '../utils/UrlHelper';
import PageNotFound from '../PageNotFound';
import $store from '../store/panel';
import { USER_INFO } from '../store/panel/action_types';

Vue.use(Router);
// eslint-disable-next-line
function beforeEnterCallback(to, from, next) {
  const userRole = $store.state.user_info.role;
  if (userRole !== 'admin') {
    next('/auth');
  } else {
    next();
  }
}

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/auth',
      meta: { auth: true },
    },
    {
      path: '/users',
      redirect: '/users/1',
      meta: { auth: true },
    },
    {
      path: '/users/:page',
      name: 'Users',
      component: Users,
      meta: { auth: true },
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      meta: { auth: true },
    },
    {
      path: '/docs',
      name: 'Docs',
      component: Docs,
      meta: { auth: true },
    },
    {
      path: '/orders/:page',
      name: 'Leads',
      component: Leads,
      meta: { auth: true },
    },
    {
      path: '/order_info',
      name: 'Lead',
      component: RequestInfo,
      meta: { auth: true },
      query: {
        num: '',
        guid: '',
        num_1c: '',
      },
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: true },
    },

  ],
  // eslint-disable-next-line
  // scrollBehavior(to, from, savedPosition) {
  //   return { x: 0, y: 0 };
  // },
});


router.afterEach((to) => {
  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        } else {
          $store.dispatch(USER_INFO);
        }
      })
      .catch(() => {
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});


export { router as default };
