import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import Index from '@/components/Lead/Index';
import DockView from '@/components/DocView';
import Calc from '@/components/Lead/Calc';
import Graph from '@/components/Lead/Graph';
import OfferInstructions from '@/components/Lead/OfferInstructions';
import Api from '../api/lead';
import Url from '../utils/UrlHelper';
import stepPosition from '../utils/StepPosition';


Vue.use(Router);

const router = new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: stepPosition() || 0 };
  },
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        auth: true,
      },
    },
    {
      path: '/condition_info',
      name: 'ConditionInfo',
      component: DockView,
      meta: {
        auth: true,
        documentList: 'information',
      },
    },
    {
      path: '/calc',
      name: 'Calc',
      component: Calc,
      meta: {
        auth: true,
      },
    },
    {
      path: '/graph',
      name: 'Graph',
      component: Graph,
      // beforeEnter(to, from, next) {
      //   if (Object.keys($store.getters.graph).length < 1) {
      //     next('/');
      //   } else {
      //     next();
      //   }
      // },
      meta: {
        auth: true,
      },
    },
    {
      path: '/offer_instruction',
      name: 'OfferInstructions',
      component: OfferInstructions,
    },
    // {
    //   path: '/status/:page',
    //   name: 'Status',
    //   component: Status,
    //   meta: {
    //     auth: true,
    //   },
    // },
  ],
});

router.afterEach((to) => {
  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
