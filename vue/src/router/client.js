import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import Index from '@/components/Client/Index';
import Request from '@/components/Request';
import Create from '@/components/Client/Create';
import Graph from '@/components/Client/Graph';
import Edit from '@/components/Client/Edit';
import VerificationListWrap from '@/components/Client/VerificationListWrap';
import GraphRequest from '@/components/Client/GraphWrap';
import Api from '../api/client';
import Url from '../utils/UrlHelper';
import stepPosition from '../utils/StepPosition';
import PageNotFound from '../PageNotFound';
import $store from '../store/client';
import config from '../config';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: stepPosition() || 0 };
  },
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: { auth: true },
    },
    {
      path: '/graph',
      name: 'GraphRequest',
      component: GraphRequest,
      meta: { auth: true },
    },
    {
      path: '/graph/:code',
      name: 'Graph',
      component: Graph,
      meta: { auth: true },
    },
    {
      path: '/vlist',
      name: 'VerificationList',
      component: VerificationListWrap,
      meta: { auth: true },
    },
    {
      path: '/request',
      component: Request,
      meta: { auth: true },
      children: [
        {
          path: '/',
          name: 'Create',
          component: Create,
          meta: { auth: true },
        },
        {
          path: ':code',
          name: 'Edit',
          beforeEnter(to, from, next) {
            $store.dispatch('getRequest', { code: to.params.code, next });
          },
          component: Edit,
          meta: {
            auth: true,
            fixed: true,
          },
        },
      ],
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
  // eslint-disable-next-line
  // scrollBehavior(to, from, savedPosition) {
  //   return { x: 0, y: 0 };
  // },
});

router.afterEach((to) => {
  const yaCounter = config.yaConfig.yaCounter;
  if (yaCounter && window[`yaCounter${yaCounter}`]) {
    window[`yaCounter${yaCounter}`].hit(to.path);
  }

  if (to.matched.some(record => record.meta.auth)) {
    const api = new Api();
    api.check()
      .then((res) => {
        if (!res.data.authorized || res.data.authorized === 'false') {
          Cookies.remove('PHPSESSID');
          Cookies.remove('_identity');
          Cookies.remove('code');
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        } else {
          $store.dispatch('getAgreementsActive');
          $store.dispatch('getAgreementsClosed');
        }
      })
      .catch((error) => {
        if (error.response.status) {
          console.error(error.response.status);
        }
        Cookies.remove('PHPSESSID');
        Cookies.remove('_identity');
        Cookies.remove('code');
        const url = Url.getSubdomainUrl('login');
        window.location = `//${url}/`;
      });
  }
});

export { router as default };
