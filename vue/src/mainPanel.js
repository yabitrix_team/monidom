import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import Cleave from 'vue-cleave';
import VueClipboard from 'vue-clipboard2';
import VueMask from 'v-mask';

import ToggleButton from 'vue-js-toggle-button';
import wysiwyg from 'vue-wysiwyg';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import AppPanel from './AppPanel';
import './assets/scss/index.scss';
import store from './store/panel/';
import router from './router/panel';
import myFormatPlugin from './plugins/moneyformat';
import ToPlugin from './plugins/to';
import formatterPlugin from './plugins/formatter';
import Api from './plugins/apiPanel';
import Validate from './plugins/validate';
import ValidateRu from './plugins/validateRu';

Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(myFormatPlugin);
Vue.use(formatterPlugin);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(ToggleButton);
Vue.use(VueClipboard);
Vue.use(VueMask);
Vue.use(Validate);
Vue.use(ValidateRu);
Vue.use(wysiwyg, {
  hideModules: {
    code: true,
    removeFormat: true,
    separator: true,
  },
});

Vue.component('v-select', vSelect);
Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);
Vue.component('font-awesome', FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { AppPanel },
  template: '<AppPanel/>',
});
