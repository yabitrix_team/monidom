import _ from 'lodash';
/*
* Например: 1000 преобразуется в 1 000
*           123213 перобразуется в 123 213
* ----------
*  const str = 1000;
*  const str2 = this.$moneyFormat(str);
*  console.log(str, str2) // выдаст -> 1000, 1 000
*/

export default function $moneyFormat(str) {
  return _.round(str, 2)
    .toFixed(2)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
    .replace('.', ',')
    .replace(',00', '');
}
