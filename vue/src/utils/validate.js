import luhn from 'luhn-alg';
/**
 * Функция выдаст true если не найдет символы не являющиеся русскими или пробел, иначе выдаст false.
 *
 * @param {String} str
 */
export const validateRu = function validateRu(str) {
  return !/[^А-яЁё ]/.test(str);
};

/**
 * Функция выдаст true если не найдет символы не являющиеся русскими или пробел, иначе выдаст false.
 *
 * @param {String} str
 */
export const validateEng = function validateEng(str) {
  return !/[^A-z]/.test(str);
};

/**
 * Функция выдаст true если не найдет символы не являющиеся русскими,
 *  цифрами или знаками "," "." "/", иначе выдаст false.
 *
 * @param {String} str
 */
export const validateRuAddress = function validateRuAddress(str) {
  return /^([А-яёЁ0-9\s,./-]+|\d)$/.test(str);
};

export const validateCardHolderName = function validateCardHolderName(str) {
  return /^[A-z\s]+ +[A-z\s]+$/.test(str);
};
/**
 * Выдаст true, если длина номера карты 16,18 или 19 и он валидируется по лунному алгоритму
 *
 * @param {String} str
 */
export const validateCardNumber = function validateCardNumber(str) {
  if (str && str.replace) {
    const value = str.replace(/ /g, '');
    if (value.length === 16 || value.length === 18 || value.length === 19) {
      return luhn(value);
    }
  }
  return false;
};

export const validateLastName = function validateLastName(str) {
  return /^[А-яЁё\- ]+$/.test(str);
};

export const validateEmail = function validateEmail(str) {
  // eslint-disable-next-line
  return /^\w+([(\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,10})+$/.test(str);
};

export const validateLogin = function validateLogin(str) {
  return /^([A-z][\w-]+|9[0-9]{9})$/.test(str);
};

export const getAges = function getAges(direction) {
  let infoAge = direction === 'age_min' ? 21 : 68;

  if (this && this.productInfo) {
    const maxLoan = +this.productInfo.loan_period_max &&
      direction !== 'age_min' ?
      +this.productInfo.loan_period_max / 12 : 0;

    infoAge = +this.productInfo[direction] ?
      +this.productInfo[direction] - maxLoan : infoAge;
  }
  return infoAge;
};

export default validateRu;
