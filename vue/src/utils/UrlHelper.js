export default {
  getSubdomainUrl(subdomain) {
    let port = '';
    let domain = window.location.hostname;

    if (window.location.port) {
      port = `:${window.location.port}`;
    }
    const toReplace = domain.split('.')[0];
    domain = domain.replace(`${toReplace}.`, '');
    return `${subdomain}.${domain}${port}`;
  },
};

