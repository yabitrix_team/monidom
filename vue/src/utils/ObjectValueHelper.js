export function InnerValueFromObj(keys, obj) {
  let innerStep = obj;
  keys.forEach((el) => {
    if (innerStep[el] !== undefined) {
      innerStep = innerStep[el];
    } else {
      innerStep = '';
    }
  });
  return innerStep;
}

export function convertPropFromObject(keys, obj) {
  const propsArr = keys.split(',');
  const innerArr = keys.split('.');
  let response = '';
  if (propsArr.length > 1) {
    propsArr.forEach((el) => { response += obj[el] ? ` ${obj[el]}` : ''; });
  } else if (innerArr.length > 1) {
    response = InnerValueFromObj(innerArr, obj);
  } else {
    response = obj[keys] || '';
  }
  return String(response).trim();
}
