function getParam(el) {
  if (Object.prototype.toString.call(el) === '[object Object]') {
    return JSON.stringify(el);
  }
  return el;
}

export default function (url, params, checkParams) {
  let str = url;
  let ind = 0;
  Object.keys(params).forEach((el, index) => {
    if (checkParams) {
      if (params[el]) {
        str += ind === 0 ? `?${el}=${getParam(params[el])}` : `&${el}=${getParam(params[el])}`;
        ind += 1;
      }
    } else {
      str += index === 0 ? `?${el}=${getParam(params[el])}` : `&${el}=${getParam(params[el])}`;
    }
  });
  return str;
}
