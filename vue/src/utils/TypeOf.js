export default function (el) {
  if (typeof el === 'string') {
    return 'string';
  } else if (typeof el === 'number') {
    return 'number';
  } else if (typeof el === 'undefined') {
    return 'undefined';
  } else if (typeof el === 'function') {
    return 'function';
  } else if (Array.isArray(el)) {
    return 'array';
  } else if (Object.prototype.toString.call(el) === '[object Object]') {
    return 'object';
    // eslint-disable-next-line
  } else if (el !== el) {
    return 'nan';
  }
  return '';
}
