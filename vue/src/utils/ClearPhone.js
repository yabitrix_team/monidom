export function isPhone(str) {
  return !/[A-z]/.test(str) && str.length > 5;
}

export function clearPhone(login) {
  if (!login) {
    return undefined;
  }
  let res = login.replace(/\W/g, '');
  if (isPhone(res)) {
    res = res.replace(/^7|^8/, '');
  }
  return res;
}
export function isCellular(str) {
  return isPhone(str) && /^9\d/.test(str);
}
export default { clearPhone, isPhone, isCellular };
