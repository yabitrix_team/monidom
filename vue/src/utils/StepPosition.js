export function topBarHeight() {
  const topBlock = document.querySelector('.wrap_fixed');
  return topBlock ? topBlock.offsetHeight + topBlock.getBoundingClientRect().top : 0;
}

export default function stepPosition() {
  const node = document.querySelector('[name=start]');

  if (node) {
    if (window.innerWidth < 820) {
      return window.scrollY + node.getBoundingClientRect().y;
    }
    const stepPositionOffset = node.getBoundingClientRect().top;
    return (window.pageYOffset + (stepPositionOffset - topBarHeight()));
  }
  return false;
}
