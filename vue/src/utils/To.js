import to from 'to-case';

export default {
  camel(val) {
    return this.convert(val, 'camel');
  },
  snake(val) {
    return this.convert(val, 'snake');
  },
  firstLetterUp(word) {
    if (typeof word === 'string') return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    return word;
  },
  toUpper(word) {
    if (typeof word === 'string') return word.toUpperCase();
    return word;
  },
  convert(val, converter) {
    if (typeof val === 'object') {
      if (Array.isArray(val)) {
        const obj = [];
        val.forEach((item, num) => {
          obj[num] = {};
          Object.keys(val[num]).forEach((i) => {
            obj[num][to[converter](i)] = val[num][i];
          });
        });
        return obj;
      }
      const obj = {};
      Object.keys(val).forEach((i) => {
        obj[to[converter](i)] = val[i];
      });
      return obj;
    }

    return to[converter](val);
  },
  phonePure(phone) {
    let login = phone.replace(/[\W]/g, '');
    if (!/[A-z]/.test(login) && login.length > 5) {
      login = login.replace(/^7|^8/, '');
    }
    return login;
  },
  firstUpper(lower) {
    return (lower[0].toUpperCase() + lower.slice(1).toLowerCase());
  },
};
