export default class ProductsFilter {
  constructor() {
    this.cp = [];
    this.guid = '';
    this.money = null;
    this.date = null;
    this.error = false;
    this.errorMessages = [];
    this.id = -1;
  }

  /**
   * Получаем даты
   */
  getDates() {
    const cp = this.cp;
    const endArr = cp.map(i => i.loanPeriodMax);
    const startArr = cp.map(i => i.loanPeriodMin);
    const startMonthCount = Math.min(...startArr);
    const endMonthCount = Math.max(...endArr);
    const dateStart = new Date();
    const dateEnd = new Date();
    dateStart.setHours(-3, 0);
    dateEnd.setHours(0, 0);

    if (!this.error) {
      // начало периода
      if (startMonthCount !== undefined) {
        dateStart.setMonth(dateStart.getMonth() + parseInt(startMonthCount, 10));
        dateStart.setDate(dateStart.getDate());
      }
      // конец периода
      if (endMonthCount !== undefined) {
        dateEnd.setMonth(dateEnd.getMonth() + parseInt(endMonthCount, 10));
        dateEnd.setDate(dateEnd.getDate() + 1);
      }
    }

    return {
      dateEnd,
      dateStart,
    };
  }

  setGuid(guid) {
    this.guid = guid;
    this.id = this.cp.find(i => i.guid === guid).id;
  }
  // записываем справочник продуктов
  setCp(obj) {
    this.cp = obj;
  }
  addError(message) {
    this.error = true;
    this.errorMessages.push(message);
  }
}
