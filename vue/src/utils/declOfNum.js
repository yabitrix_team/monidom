export default function (number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[
    (number % 100 > 4 && number % 100 < 20) ? 2 :
      cases[(number % 10 < 5) ? number % 10 : 5]
  ];
}

export const dayArr = ['день', 'дня', 'дней'];
export const hourArr = ['час', 'часа', 'часов'];
export const secArr = ['секунда', 'секунды', 'секунд'];
export const minArr = ['минута', 'минуты', 'минут'];
