import axios from 'axios';
import config from '../config';

import { response as responseFilter, errors as errorsFilter } from '../api/axiosFilter';

export default class Api {
  constructor() {
    this.web = axios.create({
      baseURL: config.api.pathGlobal,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * Проверить СМС код
   */
  // smsCheck(code, phone) {
  //   return new Promise((resolve, reject) => {
  //     this.fake.post(`service/sms/${code}`, {
  //       login: phone,
  //     })
  //       .then((response) => {
  //         if (response.data.error.length === 0) {
  //           resolve(response.data.data);
  //         } else {
  //           reject(response.data.error);
  //         }
  //       })
  //       .catch((err) => {
  //         console.error(err);
  //         reject(err);
  //       });
  //   });
  // }
  // /**
  //  * @param string
  //  */
  // sendSms(obj) {
  //   return new Promise((resolve, reject) => {
  //     this.web.post('payment/payment/sms', obj)
  //       .then((response) => {
  //         if (response.data.error.length === 0) {
  //           resolve(response.data.data);
  //         } else {
  //           reject(response.data.error);
  //         }
  //       })
  //       .catch((err) => {
  //         console.error(err);
  //         reject(err);
  //       });
  //   });
  // }
  // check(obj) {
  //   return new Promise((resolve, reject) => {
  //     this.web.post('payment/payment/check', obj)
  //       .then((response) => {
  //         if (response.data.error.length === 0) {
  //           resolve(response.data.data);
  //         } else {
  //           reject(response.data.error);
  //         }
  //       })
  //       .catch((err) => {
  //         console.error(err);
  //         reject(err);
  //       });
  //   });
  // }

  // cloud(obj) {
  //   return new Promise((resolve, reject) => {
  //     this.web.get('payment/payment/cloud', obj)
  //       .then((response) => {
  //         if (response.data.error.length === 0) {
  //           resolve(response.data.data);
  //         } else {
  //           reject(response.data.error);
  //         }
  //       })
  //       .catch((err) => {
  //         console.error(err);
  //         reject(err);
  //       });
  //   });
  // }
  // отпарвка смс партнерам
  sendSms = obj => this.web.post('payment/payment/sms', obj);
  // проверка кода из смс
  check = obj => this.web.post('payment/payment/check', obj);
  // устаревшее(?) апи для получения параметров cloudpayments
  chooseWay = obj => this.web.get('payment/payment/cloud', obj);
  // получение параметров платежных систем
  params = obj => this.web.get('/payment/payment/params', obj);
  // получение методов оплаты для wallet Оne
  methods = obj => this.web.post('/payment/payment/methods', obj);
  // Возвращает форму оплаты
  form = obj => this.web.post('/payment/payment/form', obj);
}
