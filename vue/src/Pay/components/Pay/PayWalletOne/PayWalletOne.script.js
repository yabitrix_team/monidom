import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import { mapGetters } from 'vuex';
import InputWrap from '@/components/InputWrap';
import Checkbox from '@/components/Checkbox';

export default {
  name: 'pay-wallet-one',
  components: {
    // Password,
    InputWrap,
    vSelect,
    Checkbox,
    ClipLoader,
  },
  data() {
    return {
      pay: '',
      form: false,
      smsCode: '',
      lastName: '',
      contract: '',
      cabinets: [],
      step: 3,
      code: '',
      phone: '',
      waiting: false,
      payMethodsId: false,
      summ: '',
      oferta: [],
      recommendSumm: '',
      invoice: '',
      submitClass: {
        button: true,
        'disabled-button': true,
      },
      error: false,
      type: 'cloudPayments',
      validation: {
        payMethodsId: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (!vue[e] || (typeof vue[e] === 'string' && vue[e].length < 1)) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
            }
          },
        },
        summ: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (typeof vue[e] === 'string' && vue[e].length < 1) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
            }
          },
        },
        oferta: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (vue[e].length < 1) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
            }
          },
        },
      },
      money: {
        decimal: ',',
        thousands: ' ',
        prefix: '',
        suffix: '',
        precision: 2,
      },
    };
  },
  methods: {
    submitThird() {
      if (!this.validate()) {
        return;
      }
      this.error = false;
      if (!this.waiting) {
        this.waiting = false;
        this.step = 4;
        this.$store.dispatch('form', {
          vendor: 2,
          sum: this.summToPay,
          comission: this.comPercent,
          pay_method: this.payMethodsId.id,
          invoice: this.params.invoice,
          dog: this.params.contract,
        }).then((data) => {
          this.form = data.form;
          this.$nextTick(() => {
            document.querySelector('.form form').submit();
          });
        }).catch((err) => {
          console.log(err);
        });
      }
    },
    submitLast() {
      this.step = 1;
      location.reload(true);
    },
  },
  computed: {
    ...mapGetters(['payMethods', 'params']),
    summToPay() {
      let r;
      const val = parseFloat((this.summ.toString()).replace(' ', ''));
      if (val > 194) { // если больше 194 рублей
        r = parseFloat((val / ((100 - this.comPercent) / 100)).toFixed(2));
      } else {
        r = parseFloat((val + +this.comFixed).toFixed(2));
      }
      return r;
    },
    comPercent() {
      return (this.payMethodsId ? this.payMethodsId.rnko_client : undefined);
    },
    comFixed() {
      return (this.payMethodsId ? this.payMethodsId.rnko_client_min : undefined);
    },
  },
  mounted() {
    this.$store.dispatch('payMethods')
      .then(() => {
      });
    const watchArr = ['payMethodsId', 'summ', 'oferta'];
    watchArr.forEach((item) => {
      this.$watch(item, () => this.validate(item));
    });
    this.summ = this.params.summ;
  },
};
