import { mapGetters } from 'vuex';
import vSelect from 'vue-select';
import InputWrap from '@/components/InputWrap';
import PayCloud from './PayCloud/PayCloud.component';
import PayWalletOne from './PayWalletOne/PayWalletOne.component';

export default {
  name: 'pay',
  components: {
    // Password,
    InputWrap,
    vSelect,
    PayCloud,
    PayWalletOne,
  },
  data() {
    return {
      pay: '',
      smsCode: '',
      lastName: '',
      contract: '',
      cabinets: [],
      code: '',
      waiting: false,
      summ: '',
      recommendSumm: '',
      summError: {
        invalid: false,
        message: 'Сумма должна быть больше 0',
      },
      invoice: '',
      submitClass: {
        button: true,
        'disabled-button': true,
      },
      error: false,
      errorMessage: '',
      comPercent: 2,
      comFixed: '3.90',
      type: 'cloudPayments',
      validation: {
        lastName: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (!vue[e] || (typeof vue[e] === 'string' && vue[e].length < 1)) {
              this.invalid = true;
              this.message = 'Заполните все поля';
            } else {
              this.invalid = false;
              this.message = '';
              if (String(vue[e]).length > 0 && !vue.$validateLastName(vue[e])) {
                this.invalid = true;
                this.message = 'Разрешены только русские символы';
              }
            }
          },
        },
        contract: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (!vue[e] || (typeof vue[e] === 'string' && vue[e].length < 1)) {
              this.invalid = true;
              this.message = 'Заполните все поля';
            } else {
              this.invalid = false;
              this.message = '';
              if (/\D/.test(vue[e])) {
                this.invalid = true;
                this.message = 'Номер договора должен содержать только цифры';
              }
            }
          },
        },
        code: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (!vue[e] || (typeof vue[e] === 'string' && vue[e].length < 1)) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
            }
          },
        },
      },
    };
  },
  methods: {
    checkUrlParams() {
      const url = new URL(window.location.href);
      const user = url.searchParams.get('payuser');
      const account = url.searchParams.get('payaccount');
      if (account && user) {
        this.lastName = user;
        this.contract = account;
      }
    },
    submitFirst() {
      this.error = false;
      if (!this.waiting && this.validate('lastName') && this.validate('contract')) {
        this.waiting = true;
        this.$store.dispatch('sendSms', {
          lastName: this.lastName,
          contract: this.contract,
        })
          .then(() => {
            this.waiting = false;
          })
          .catch((data) => {
            console.log('err', data);
            this.error = true;
            this.waiting = false;
          });
      }
    },
    submitSecond() {
      if (!this.waiting) {
        this.waiting = true;
        this.error = false;
        this.errorMessage = '';
        this.$store.dispatch('check', {
          code: this.code,
          contract: this.contract,
        })
          .then(() => {
            this.waiting = false;
          })
          .catch((data) => {
            this.validation.code.invalid = true;
            console.log('err', data);
            if (data.message) {
              this.errorMessage = data.message;
            }
            this.error = true;
            this.waiting = false;
          });
      }
    },
  },
  computed: {
    ...mapGetters(['params', 'step', 'phone']),
    summToPay() {
      let r;
      const val = parseFloat((this.summ.toString()).replace(' ', ''));
      if (val > 194) { // если больше 194 рублей
        r = parseFloat((val / ((100 - this.comPercent) / 100)).toFixed(2));
      } else {
        r = parseFloat((val + +this.comFixed).toFixed(2));
      }
      return r;
    },
  },
  mounted() {
    this.$store.dispatch('init');
    this.checkUrlParams();
  },
};
