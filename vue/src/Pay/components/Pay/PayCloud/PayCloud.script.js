import { mapGetters } from 'vuex';
import vSelect from 'vue-select';
import InputWrap from '@/components/InputWrap';


export default {
  name: 'pay-cloud',
  components: {
    // Password,
    InputWrap,
    vSelect,
  },
  data() {
    return {
      pay: '',
      smsCode: '',
      lastName: '',
      contract: '',
      cabinets: [],
      waiting: false,
      summ: '',
      summError: {
        invalid: false,
        message: 'Сумма должна быть больше 0',
      },
      submitClass: {
        button: true,
        'disabled-button': true,
      },
      error: false,
      comPercent: 2,
      type: 'cloudPayments',
      validation: {
        summ: {
          invalid: false,
          message: '',
          func(vue, e) {
            if (!vue[e] || (typeof vue[e] === 'string' && vue[e].length < 1)) {
              this.invalid = true;
              this.message = 'Поле должно быть заполнено';
            } else {
              this.invalid = false;
              this.message = '';
              if (!/^[\d ]+(?:[.,]\d+)?$/.test(vue[e])) {
                this.invalid = true;
                this.message = 'Некорректное значение';
              }
            }
          },
        },
      },
    };
  },
  methods: {
    submitThird() {
      if (this.summToPay === 0) {
        this.summError.invalid = true;
        return;
      }
      this.error = false;
      if (!this.waiting && this.validate()) {
        this.waiting = false;
        this.$store.commit('step', 4);
        this.$nextTick((() => {
          // eslint-disable-next-line
          const widget = new cp.CloudPayments();
          const paramsObj = {
            publicId: this.params.publicId,
            description: this.params.description,
            amount: +this.summToPay, // сумма
            currency: this.params.currency,
            invoiceId: +this.params.invoice, // номер заказа
            accountId: this.params.contract,
            data: {
              sum: parseFloat((this.summ.toString()).replace(/ /g, '').replace(',', '.')),
            },
          };
          console.log(paramsObj);
          widget.charge(
            paramsObj,
            () => { // success
              this.$store.commit('step', 5);
            },
            () => { // fail
              this.$store.commit('step', 6);
            });
        }));
      }
    },
    submitLast() {
      this.$store.commit('step', 1);
      location.reload(true);
    },
  },
  computed: {
    ...mapGetters(['params', 'mode', 'step', 'phone', 'invoice', 'recommendSumm']),
    summToPay() {
      const val = parseFloat((this.summ.toString()).replace(/ /g, '').replace(',', '.'));
      return parseFloat((val / ((100 - this.comPercent) / 100)).toFixed(2));
    },
  },
  mounted() {
    this.summ = this.params.summ;
    (function insertCloudPayments(document, tag) {
      // create a script tag
      const scriptTag = document.createElement(tag);
      // find the first script tag in the document
      const firstScriptTag = document.getElementsByTagName(tag)[0];
      // set the source of the script to your script
      scriptTag.src = 'https://widget.cloudpayments.ru/bundles/cloudpayments';
      // append the script to the DOM
      firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag);
    }(document, 'script'));
  },
  directives: {
    summ: {
      update(el, binding, e) {
        const cursor = el.selectionStart;
        let summ = e.context.summ;

        summ = summ.replace(/[^0-9,.]/g, '').replace('.', ',');

        const arr = summ.match(/([\d])+(?:,?(\d?\d?))?/g, '');

        summ = arr ? arr[0] : '';

        e.context.summ = summ.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        el.setSelectionRange(cursor, cursor);
      },
    },
  },
};
