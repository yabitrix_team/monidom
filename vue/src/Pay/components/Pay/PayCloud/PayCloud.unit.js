import Vuex from 'vuex';
import Component from './PayCloud.component.vue';
import {shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

let store = new Vuex.Store({
  state: {
    code: '',
    methods:[],
    params: {
      code: 'CloudPayments',
      contract: '18051423580001',
      currency: 'RUB',
      description: 'Оплата в carmoney.ru',
      file_oferta: '',
      id: '1',
      invoice: '9753147662415',
      name: 'Cloud Payments',
      publicId: 'pk_dd99e190825e9b3822845e1dcfcda',
      recommendSumm: 1000,
      summ: '1000',
    },
    phone: '+ 7(903)***-**-97',
  },
  mutations: {},
  actions: {
    hasSecondPack(context, data) {
    },
  },
  getters: {
    params: state => state.params,
    step: state => state.step,
    phone: state => state.phone,
    invoice: state => state.params.invoice || 0,
    recommendSumm: state => state.params.recommendSumm || 0,
    payMethods: state => state.methods,
  },
})


describe('StepFinishAfterApprove', () => {
  const routes = [
    // {
    //   path: '/request/:code',
    //   component: Component,
    // },
    {
      path: '/' ,
      redirect: '/request/string',
    }
  ];

  const router = new VueRouter({
    routes
  });
  Component.mounted = function(){};
  const wrapper = shallowMount(Component, {
    store,
    // router,
    localVue,
  });

  it('renders right content', () => {
    expect(wrapper.vm.invoice).toBe('9753147662415');


    // wrapper.vm.$store.state.request.docPack2.forEach(element => {
    //   wrapper.vm.checkedDocuments.push(element.code);
    // });
    // wrapper.vm.submit();
    // expect(wrapper.vm.blockPassword).toBe(true);
  });
  it('Возвращает правильное значение для оплаты', () => {
    expect((wrapper.vm.summ = '98', wrapper.vm.summToPay)).toBe(100);
    expect((wrapper.vm.summ = '980', wrapper.vm.summToPay)).toBe(1000);
    expect((wrapper.vm.summ = '9 800', wrapper.vm.summToPay)).toBe(10000);
    expect((wrapper.vm.summ = '98 000', wrapper.vm.summToPay)).toBe(100000);
    expect((wrapper.vm.summ = '980 000', wrapper.vm.summToPay)).toBe(1000000);
    expect((wrapper.vm.summ = '9 800 000', wrapper.vm.summToPay)).toBe(10000000);
    expect((wrapper.vm.summ = '98 000 000', wrapper.vm.summToPay)).toBe(100000000);

  });
});
