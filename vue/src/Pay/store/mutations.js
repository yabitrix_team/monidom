export default {
  INITIAL_PARAMS(state, payload) {
    state.params = payload;
  },
  SET_PARAMS(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.params[key] = payload[key];
    });
  },
  step(state, payload) {
    state.step = payload;
  },
  SET_PHONE(state, payload) {
    state.phone = payload;
  },
  SET_PAYMETHODS(state, payload) {
    state.methods = payload;
  },
};
