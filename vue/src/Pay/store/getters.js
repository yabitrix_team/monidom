export default {
  params: state => state.params,
  step: state => state.step,
  phone: state => state.phone,
  invoice: state => state.params.invoice || 0,
  recommendSumm: state => state.params.recommendSumm || 0,
  payMethods: state => state.methods,
};
