
import Api from '../api';

const api = new Api();

export default {
  init(context) {
    api.params()
      .then((res) => {
        context.commit('INITIAL_PARAMS', res.data.data[0]);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  sendSms(context, { lastName, contract }) {
    context.commit('SET_PARAMS', {
      contract,
    });
    return new Promise((resolve, reject) => {
      api.sendSms({
        lastName,
        contract,
      })
        .then((data) => {
          if (data.data.error && data.data.error.message) {
            return reject(data.data.error);
          }
          context.commit('SET_PHONE', data.data.data.phone);

          context.commit('step', 2);
          return resolve(data.data.data);
        })
        .catch(data => reject(data));
    });
  },
  check(context, { code, contract }) {
    return new Promise((resolve, reject) => {
      api.check({
        code,
        contract,
      })
        .then((data) => {
          if (data.data.error && data.data.error.message) {
            return reject(data.data.error);
          }
          context.commit('step', 3);
          context.commit('SET_PARAMS', {
            summ: data.data.data.sum.toString(),
            recommendSumm: data.data.data.sum,
            invoice: data.data.data.invoice,
            contract,
          });

          return resolve(data.data.data);
        })
        .catch(error => reject(error));
    });
  },
  payMethods(context) {
    api.methods({
      vendor: 2,
    })
      .then((data) => {
        context.commit('SET_PAYMETHODS', data.data.data);
        return Promise.resolve(data.data.data);
      })
      .catch(data => Promise.reject(data));
  },
  form(context, obj) {
    return new Promise((resolve, reject) => {
      api.form(obj)
        .then((data) => {
          context.commit('SET_PARAMS', data.data.data);
          resolve(data.data.data);
        })
        .catch(data => reject(data));
    });
  },
};
