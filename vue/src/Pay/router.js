import Vue from 'vue';
import Router from 'vue-router';
import Fail from '@/Pay/components/Fail';
import Success from '@/Pay/components/Success';
import Pay from './components/Pay/Pay.component';
import PageNotFound from '../PageNotFound';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Pay',
      component: Pay,
      meta: { auth: false },
    },
    {
      path: '/w1/fail',
      name: 'Fail',
      component: Fail,
      meta: { auth: false },
    },
    {
      path: '/w1/success',
      name: 'Success',
      component: Success,
      meta: { auth: false },
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound,
      meta: { auth: false },
    },
  ],
});

export { router as default };
