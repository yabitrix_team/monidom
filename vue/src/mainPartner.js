// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import vSelect from 'vue-select';
import ClipLoader from 'vue-spinner/src/ClipLoader';
import Vuebar from 'vuebar';
import VueQrcode from '@xkeshi/vue-qrcode';
import VueMask from 'v-mask';
import Cookies from 'js-cookie';
import ToggleButton from 'vue-js-toggle-button';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import 'blueimp-canvas-to-blob';

import Cleave from './components/Cleave';
import AppPartner from './AppPartner';
import router from './router/partner';
import './assets/scss/index.scss';
import store from './store/partner';
import myFormatPlugin from './plugins/moneyformat';
import formatterPlugin from './plugins/formatter';
import ToPlugin from './plugins/to';
import config from './config';
import Api from './plugins/apiPartner';
import Validate from './plugins/validate';
import Datepicker from './components/Global/Datepicker';
import moneyFormat from './utils/MoneyFormat';
import error from './plugins/error';
import ValidateRu from './plugins/validateRu';
import VueNativeSock from './plugins/sockets/index';

Vue.use(error);
Vue.use(Vuebar);
Vue.use(VueAxios, axios);
Vue.use(myFormatPlugin);
Vue.use(formatterPlugin);
Vue.use(ToPlugin);
Vue.use(Api);
Vue.use(
  VueNativeSock,
  `${config.ws.protocol}://${config.ws.server}?code=${Cookies.get('code')}`,
  {
    store,
    format: 'json',
    reconnection: true,
  },
);
Vue.use(ToggleButton);
Vue.use(VueMask);
Vue.use(Validate);
Vue.use(ValidateRu);


Vue.component('v-select', vSelect);
Vue.component('datepicker', Datepicker);
Vue.component('clip-loader', ClipLoader);
Vue.component('cleave', Cleave);
Vue.component('qrcode', VueQrcode);
Vue.component('font-awesome', FontAwesomeIcon);

Vue.config.productionTip = false;
Vue.axios.defaults.withCredentials = true;
Vue.axios.defaults.header = { 'Content-type': 'application/json' };

Vue.filter('moneyFormat', moneyFormat);
Vue.filter('formatDate', (value) => {
  if (!value) return '';
  const a = moment(value);
  return a.format('DD.MM.YYYY');
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { AppPartner },
  template: '<AppPartner/>',
});
