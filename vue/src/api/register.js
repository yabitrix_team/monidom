import axios from 'axios';
import to from '@/utils/To';
import config from '../config';
import error from './errorStore';
import global from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';

export default class Api extends global {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathClient,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * @param string
   */
  checkSms(phone, code) {
    return new Promise((resolve, reject) => {
      this.web.post(`service/sms/${code}`, {
        login: phone,
      }).then((response) => {
        if (response.data.error.length === 0) {
          resolve(response.data.data);
        } else {
          reject(response.data.error);
        }
      }).catch((err) => {
        error.add(err);
        reject(err);
      });
    });
  }
  /**
   * Зарегистрировать клиента
   */
  register(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user/register', obj)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        }).catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получить лид
   */
  lead(code) {
    return new Promise((resolve, reject) => {
      this.web.get(`lead/${code}`)
        .then(({ data }) => {
          if (data.error.length === 0) {
            resolve({ data: to.camel(data.data) });
          } else {
            reject(data.error);
          }
        }).catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Проверить СМС код
   */
  smsCheck(code, phone) {
    return new Promise((resolve, reject) => {
      this.fake.post(`service/sms/${code}`, {
        login: phone,
      }).then((response) => {
        if (response.data.error.length === 0) {
          resolve(response.data.data);
        } else {
          reject(response.data.error);
        }
      }).catch((err) => {
        error.add(err);
        reject(err);
      });
    });
  }
  /**
   * @param string
   */
  sendSms(phone) {
    return new Promise((resolve, reject) => {
      this.web.post('service/sms', {
        login: phone,
      }).then((response) => {
        if (response.data.error.length === 0) {
          resolve(response.data.data);
        } else {
          reject(response.data.error);
        }
      }).catch((err) => {
        error.add(err);
        reject(err);
      });
    });
  }

  /**
   * @param string
   */
  doc(code) {
    return new Promise((resolve, reject) => {
      this.web.get('/document', { params: { code } })
        .then(({ data }) => {
          if (data.error && data.error.message) {
            reject(data.error.message);
          } else {
            resolve(data);
          }
        }).catch((e) => {
          reject(e);
        });
    });
  }
}
