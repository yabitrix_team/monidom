import errorStore from './errorStore';

export function errors(err) {
  // Do something with response error
  console.error('AxiosFilter:', err);
  errorStore.add(err);
  return Promise.reject(err);
}

export function response(res) {
  if (res.data.error && res.data.error.length > 0) {
    errors(res);
  }
  return res;
}


export default { response, errors };
