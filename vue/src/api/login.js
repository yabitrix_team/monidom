import axios from 'axios';
import config from '../config';
import to from '../utils/To';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';

export default class Api {
  constructor() {
    this.web = axios.create({
      baseURL: config.api.pathLogin,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  setPassword(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user/set', obj).then((response) => {
        if (response.data.error.length === 0) {
          resolve(to.camel(response.data.data));
        } else {
          reject(response.data.error);
        }
      }).catch((err) => {
        console.error(err);
        reject(err);
      });
    });
  }
  checkSms(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user/reset', obj).then((response) => {
        if (response.data.error.length === 0) {
          resolve(to.camel(response.data.data));
        } else {
          reject(response.data.error);
        }
      }).catch((err) => {
        console.error(err);
        reject(err);
      });
    });
  }
  checkLogin(login) {
    return new Promise((resolve, reject) => {
      this.web.get(`user/reset/${login}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  login(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user/auth', obj)
        .then(({ data: { data, error: err } }) => {
          if (err.length === 0) {
            resolve(to.camel(data));
          } else {
            reject(to.camel(err));
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  getCabinets() {
    return new Promise((resolve, reject) => {
      this.web.get('info/cabinet')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
}

