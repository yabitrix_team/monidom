import axios from 'axios';
import to from '../utils/To';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';
import config from '../config';

export default class GlobalApi {
  constructor() {
    this.web = axios.create();
    this.fake = axios.create({
      baseURL: 'http://localhost:8080/fake/v1',
    });
    const yaCounter = config.yaConfig.yaCounter;
    this.clientId = yaCounter && window[`yaCounter${yaCounter}`] ? window[`yaCounter${yaCounter}`].getClientID() : null;
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * Получение профиля
   */
  profile() {
    return new Promise((resolve, reject) => {
      this.web.get('user')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получение заявки
   */
  getRequest({ code, next }) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
            if (next) {
              next();
            }
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Создание заявки
   */
  createRequest(params) {
    const yaCounter = config.yaConfig.yaCounter;
    const clientId = yaCounter && window[`yaCounter${yaCounter}`] ? window[`yaCounter${yaCounter}`].getClientID() : null;
    const request = clientId ?
      to.snake(Object.assign(params, { yaClientId: clientId })) :
      to.snake(params);
    return new Promise((resolve, reject) => {
      this.web.post('request', request)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Обновление заявки
   */
  updateRequest(code, params) {
    const yaCounter = config.yaConfig.yaCounter;
    const clientId = yaCounter && window[`yaCounter${yaCounter}`] ? window[`yaCounter${yaCounter}`].getClientID() : null;

    const request = clientId ?
      to.snake(Object.assign(params, { yaClientId: clientId })) :
      to.snake(params);
    return new Promise((resolve, reject) => {
      this.web.post(`request/${code}`, request)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Добавление события к заявке
   */
  insertEvents(code, event, additionalInfo = {}) {
    return new Promise((resolve, reject) => {
      this.web.post('request/event', to.snake({
        code,
        event,
        ...additionalInfo,
      }))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник занятости
   */
  infoEmployments() {
    return new Promise((resolve, reject) => {
      this.web.get('info/employment')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Справочник периодов работы
   */
  infoWorkplacePeriods() {
    return new Promise((resolve, reject) => {
      this.web.get('info/workplace-period')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник связей с заемщиком
   */
  infoPeopleRelations() {
    return new Promise((resolve, reject) => {
      this.web.get('info/people-relation')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник кредитных продуктов
   */
  infoCp() {
    return new Promise((resolve, reject) => {
      this.web.get('info/product')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник методов выдачи займа
   */
  infoMethodsOfIssuance() {
    return new Promise((resolve, reject) => {
      this.web.get('info/method-issuance')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник статусов
   */
  infoStatus() {
    return new Promise((resolve, reject) => {
      this.web.get('info/status')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник брендов авто
   */
  infoAutoBrands() {
    return new Promise((resolve, reject) => {
      this.web.get('info/auto-brand')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник моделей авто на основе бренда
   * @param {*} e
   */
  infoAutoModels(e) {
    return new Promise((resolve, reject) => {
      this.web.get(`info/auto-model/${e}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник регионов
   */
  infoRegions() {
    return new Promise((resolve, reject) => {
      this.web.get('info/region')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Калькуляция займа и графика платежей
   * @param {*} obj
   */
  calculate(obj) {
    return new Promise((resolve, reject) => {
      this.web.get('service/schedule-payment', {
        params: to.snake(obj),
      })
        .then((response) => {
          if (response.data && response.data.error && response.data.error.message) {
            reject(response.data.error.message);
          } else {
            resolve(response.data.data);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * @param {*} obj
   */
  sendFile(obj) {
    const formData = new FormData();
    Object.keys(obj).forEach((key) => {
      if (key !== 'url' && key !== 'file') {
        formData.append(key, obj[key]);
      }
      // если это файл, то добавляем имя файла
      if (key === 'file') {
        // всегда сохраняется в jpeg, поэтому меняем расширение
        formData.append(key, obj[key], obj.name.replace('.png', '.jpg'));
      }
    });
    return new Promise((resolve, reject) => {
      this.web.post('request/file', formData)
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * @param {*} obj
   */
  deleteFile(obj) {
    return new Promise((resolve, reject) => {
      this.web.delete(`request/file/${obj}`)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * @param {*} obj
   */
  saveFiles(obj) {
    return new Promise((resolve, reject) => {
      this.fake.post('save-files', obj)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * @param string
   */
  sendSms(phone) {
    return new Promise((resolve, reject) => {
      this.web.post('service/sms', {
        login: phone,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * @param string
   */
  checkSms(phone, code) {
    return new Promise((resolve, reject) => {
      this.web.post(`service/sms/${code}`, {
        login: phone,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  check() {
    return new Promise((resolve, reject) => {
      this.web.get('user/auth')
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  logout() {
    return new Promise((resolve, reject) => {
      this.web.delete('user/auth')
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  support(params) {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(params).forEach((key) => {
        formData.append(key, params[key]);
      });
      this.web.post('service/support', formData)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * получить  Cписок сообщений по заявке
   */
  getVerificationList() {
    return new Promise((resolve, reject) => {
      // resolve(mock);
      this.web.get('/service/verification/list')
        .then((response) => {
          if (response.data.length !== 0) {
            resolve(response.data);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * отправить сообщение верификатору
   */
  sendVerificationMessage(params) {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(params).forEach((key) => {
        if (key !== 'file') {
          formData.append(key, params[key]);
        }
      });
      if (params.file.length > 0) {
        params.file.forEach((obj) => {
          formData.append('file[]', obj.file, obj.name.replace('.png', '.jpg'));
        });
      }
      this.web.post('/service/verification', formData)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Получить информацию FAQ
   */
  getFaq() {
    return new Promise((resolve, reject) => {
      this.web.get('info/faq')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * Получить все уведомления
   */

  oldNotifications() {
    return new Promise((resolve, reject) => {
      this.web.get('notification').then((response) => {
        if (response.data.length !== 0) {
          resolve(response.data);
        }
      }).catch((err) => {
        error.add(err);
        reject(err);
      });
    });
  }
}

