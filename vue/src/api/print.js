import axios from 'axios';
import config from '../config';
import globalApi from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathAgent,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }

  /**
     * Получить doc-по :code
     */
  printLink(code) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/print_link/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
}

