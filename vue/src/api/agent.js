import axios from 'axios';
import config from '../config';
import globalApi from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';
import toParams from '../utils/urlParams';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathAgent,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * очистка заявки у агента
  */
  clean(code) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/clean/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * Получить 2 пакет документов
   */
  secondDocuments(num) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/second-document/${num}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            if (!response.data.data.stay) {
              resolve(response.data.data);
            }
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получить список заявок
   */
  requests() {
    return new Promise((resolve, reject) => {
      this.web.get('request')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }


  /**
   * Проверка пароля
   */
  verify(password, code) {
    return new Promise((resolve, reject) => {
      this.web.post('user/verify', {
        password,
        code,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Проверка sms в конце заявки
   */
  verifySms(sms, code) {
    return new Promise((resolve, reject) => {
      this.web.post('user/verifysms', {
        code,
        sms,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Загрузка документов на просмотр
   */
  docs(query) {
    return new Promise((resolve, reject) => {
      this.web.get(query ? toParams('document', query) : 'document').then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  /**
   * Проверка фотографий
   */
  checkPhotosDate(code) {
    return new Promise((resolve, reject) => {
      this.web.post(`request/check-and-delete-old-files/${code}`).then((response) => {
        if (response.data.error.length === 0
          && response.data.error.message) {
          reject(response.data.data);
        } else {
          resolve(response.data.data);
        }
      }).catch((e) => {
        console.error(e);
        reject(e);
      });
    });
  }
}

