import axios from 'axios';
import config from '@/config';
import globalApi from '@/api/global';

import { response as responseFilter, errors as errorsFilter } from '@/api/axiosFilter';
import error from '@/api/errorStore';
import to from '@/utils/To';
import toParams from '@/utils/urlParams';


export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathCall,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * Получение информации по коду лида
   */
  getLeadInfo(code) {
    return new Promise((resolve, reject) => {
      this.web.get(`lead/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Создание лида
   */
  createLead(params) {
    return new Promise((resolve, reject) => {
      this.web.post('/lead/lead/new', to.snake(params))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Редактирование лида
   */
  editLead(params) {
    return new Promise((resolve, reject) => {
      this.web.post('lead', to.snake(params))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Получение списка лидов
   */
  getLeads(obj) {
    return new Promise((resolve, reject) => {
      this.web.get(toParams('lead', obj))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Справочник кредитных продуктов
   */
  infoCp() {
    return new Promise((resolve, reject) => {
      this.web.get('info/product')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Калькуляция займа и графика платежей
   * @param {*} obj
   */
  calculate(obj) {
    return new Promise((resolve, reject) => {
      this.web.get('service/schedule-payment', {
        params: to.snake(obj),
      })
        .then((response) => {
          if (response.data && response.data.error && response.data.error.message) {
            reject(response.data.error.message);
          } else {
            resolve(response.data.data);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник регионов
   */
  infoRegions() {
    return new Promise((resolve, reject) => {
      this.web.get('info/region')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник брендов авто
   */
  infoAutoBrands() {
    return new Promise((resolve, reject) => {
      this.web.get('info/auto-brand')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник моделей авто на основе бренда
   * @param {*} e
   */
  infoAutoModels(e) {
    return new Promise((resolve, reject) => {
      this.web.get(`info/auto-model/${e}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  logout() {
    return new Promise((resolve, reject) => {
      this.web.delete('user/auth')
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  list({ page, filter }) {
    return new Promise((resolve, reject) => {
      this.web.get(toParams('lead', { page, filter: filter || '' }))
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник регионов
   */
  infoPoints() {
    return new Promise((resolve, reject) => {
      this.web.get('info/point')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Удалить лид
   */
  cancel(code) {
    return new Promise((resolve, reject) => {
      this.web.delete(`lead/lead/${code}`)
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Восстановить лид
   */
  recover(code) {
    return new Promise((resolve, reject) => {
      this.web.post('lead/lead/recover', to.snake({ code }))
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Перезвонить
   */
  callback(code, payload) {
    return new Promise((resolve, reject) => {
      this.web.post(`lead/${code}`, to.snake(payload))
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
}
