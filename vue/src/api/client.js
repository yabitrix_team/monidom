import axios from 'axios';
import config from '../config';
import globalApi from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathClient,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * Получить список заявок
   */
  requests() {
    return new Promise((resolve, reject) => {
      this.web.get('request')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получить лид
   */
  lead(code) {
    return new Promise((resolve, reject) => {
      this.web.get(`lead/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получить лид
   */
  leadClient() {
    return new Promise((resolve, reject) => {
      this.web.get('lead')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Подтверждение карты
   */
  approveCard(req) {
    return new Promise((resolve, reject) => {
      this.web.post('service/card-verifier/create', req).then(({ data }) => {
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  /**
   * Подтверждение карты
   */
  checkCardApprove(code) {
    return new Promise((resolve, reject) => {
      this.web.post('service/card-verifier/event', {
        code,
      }).then((res) => {
        resolve(res);
      }).catch(e => reject(e));
    });
  }

  /**
   * Удалить лид
   */
  deleteLead(code) {
    return new Promise((resolve, reject) => {
      this.web.delete(`lead/${code}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Получить информацию по активным договорам
   */
  agreementsActive() {
    return new Promise((resolve, reject) => {
      this.web.get('agreements/active')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Получить информацию по закрытым договорам
   */
  agreementsClosed() {
    return new Promise((resolve, reject) => {
      this.web.get('agreements/closed')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
}
