import axios from 'axios';
import globalApi from './global';
import config from '../config';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';
import to from '../utils/To';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathManager,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }

  /**
   * Калькуляция займа и графика платежей
   * @param {*} obj
   */
  calculate(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('service/schedule-payment', {
        ...to.snake(obj),
      })
        .then((response) => {
          if (response.data.data.error && response.data.data.error.length) {
            reject(response.data.data.error.message);
          } else if (response.data.error.message) {
            reject(response.data.error.message);
          } else {
            resolve(response.data.data);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err.message);
        });
    });
  }

  /**
   * Генерация pdf
   * @param {*} obj
   */
  pdf(obj) {
    return new Promise(() => {
      this.web.post('service/generate/pdf', {
        ...to.snake(obj),
      }, {
        responseType: 'arraybuffer',
      }).then((data) => {
        const disposition = data.headers['content-disposition'];
        const matches = /"([^"]*)"/.exec(disposition);
        const filename = (matches != null && matches[1] ? matches[1] : 'PaymentGraph');
        const blob = new Blob([data.data], { type: 'application/base64' });
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;
        link.click();
      });
    });
  }
}
