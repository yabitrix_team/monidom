import axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import config from '../config';

const web = axios.create({
  baseURL: config.api.pathLogin,
});
const store = {
  state: {
    errors: [],
  },
  add(message) {
    console.log('console.log(message)', message);
    this.state.errors.push({
      id: this.state.errors.length + 1,
      message,
    });
  },
  close(id) {
    const rid = this.state.errors.findIndex(i => i.id === id);
    this.state.errors.splice(rid, 1);
  },
};

export default {
  add(error) {
    const id = `${moment().format('DDMMYYYY')}_${_.random(100, 5000)}`;
    const params = {
      error: true,
      message: JSON.stringify(`${error}_${id}`),
    };
    const formData = new FormData();
    Object.keys(params).forEach((key) => {
      formData.append(key, params[key]);
    });

    web.post('service/support', formData)
      .then(() => {
        store.add(`Произошла ошибка. Она уже отправлена нам с кодом: ${id}`);
      })
      .catch(() => {
        store.add('add', 'Невозможно отправить ошибку. Проверьте подключение и обновите страницу.');
      });
  },
  close(id) {
    store.close(id);
  },
  data() {
    return store.state.errors;
  },
};

