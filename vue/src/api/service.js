import axios from 'axios';
import config from '../config';

import { response as responseFilter, errors as errorsFilter } from './axiosFilter';

export default class Api {
  constructor() {
    this.web = axios.create({
      baseURL: config.api.pathGlobal,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * доступен ли сервис
   */
  getClosed() {
    return new Promise((resolve, reject) => {
      this.web.get('/service/verification/closed')
        .then(({ data }) => {
          if (data && data.length !== 0) {
            resolve(data);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * получить  Cписок сообщений по заявке
   */
  getVerificationList() {
    return new Promise((resolve, reject) => {
      this.web.get('/service/verification/list')
        .then((response) => {
          if (response.data.length !== 0) {
            resolve(response.data);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * отправить сообщение верификатору
   */
  sendVerificationMessage(params) {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(params).forEach((key) => {
        formData.append(key, params[key]);
      });
      this.web.post('/service/verification', formData)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
}
