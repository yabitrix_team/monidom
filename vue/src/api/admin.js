import axios from 'axios';
import config from '../config';
import globalApi from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathAdmin,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }
  /**
   * получить  клиентов разных типов с пагинацией
   */
  clientListAdmin(type, params) {
    return new Promise((resolve, reject) => {
      let strpost;
      switch (type) {
        case 'work':
          strpost = 'client';
          break;
        case 'active':
          strpost = 'client/active';
          break;
        case 'closed':
          strpost = 'client/closed';
          break;
        case 'wait':
          strpost = 'client/wait';
          break;
        default:
          strpost = 'client';
      }
      this.web.post(strpost, params)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }


  /**
   * Получить информацию FAQ
   */
  getFaq() {
    return new Promise((resolve, reject) => {
      this.web.get('info/faq')
        .then(({ data }) => {
          const res = data.data;
          res[0].gray = true;
          return resolve(res);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
}
