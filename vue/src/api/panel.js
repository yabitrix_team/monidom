import axios from 'axios';
import config from '../config';
import globalApi from './global';
import params from '../utils/urlParams';

import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';
import to from '../utils/To';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathPanel,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }

  group() {
    return new Promise((resolve) => {
      this.web.get('group').then((res) => {
        resolve(res);
      });
    });
  }
  check() {
    return new Promise((resolve, reject) => {
      this.web.get('user/auth')
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  request(req) {
    return new Promise((resolve, reject) => {
      this.web.get(params('request', req, true))
        // eslint-disable-next-line
        .then(({ data: { data: { request, events }, error } }) => {
          if (error && error.message) {
            reject(error.message);
          }
          resolve(
            { request: to.camel(request),
              events: to.camel(events) },
          );
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  resendRequestEvent(req) {
    return new Promise((resolve, reject) => {
      this.web.post('request/event', req)
        .then(({ data: { data: { success } } }) => {
          if (success) {
            resolve({
              success,
              req,
            });
          }
          reject('Не удалось отправить евент по заявке');
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  requestFiles(req) {
    return new Promise((resolve, reject) => {
      this.web.get(params('request/file', req))
      // eslint-disable-next-line
        .then(({ data: { data: { files, types, access }, error } }) => {
          if (error && error.message) {
            reject(error.message);
          }
          resolve(
            { files: to.camel(files),
              types: to.camel(types),
              access,
            },
          );
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  deleteFile(req) {
    return new Promise((resolve, reject) => {
      this.web.delete('request/file', { data: req })
        // eslint-disable-next-line
        .then(({ data: { data: {deleted}, error } }) => {
          if (error && (error.length || error.message)) {
            reject(error.length ? error[0] : error.message);
          }
          resolve(deleted);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      this.web.delete('user/auth')
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  profile() {
    return new Promise((resolve, reject) => {
      this.web.get('user')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  user(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user', obj).then(({ data }) => {
        if (data.error && (data.error.message || data.error.validate)) {
          reject(data.error.message ? data.error.message : data.error.validate);
        }
        resolve(data);
      });
    });
  }

  leads(obj) {
    return new Promise((resolve, reject) => {
      this.web.get(params('lead', obj)).then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        }
        resolve(data);
      });
    });
  }

  users(obj) {
    return new Promise((resolve) => {
      this.web.get(params('user/user/list', obj)).then(({ data }) => {
        resolve(data);
      });
    });
  }

  smsStatus(id) {
    return new Promise((resolve, reject) => {
      this.web.get(`lead/${id}`).then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  link(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('user/auth/link', obj).then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  docs(query) {
    return new Promise((resolve, reject) => {
      this.web.get(query ? params('document', query) : 'document').then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }

  doc(obj) {
    return new Promise((resolve, reject) => {
      this.web.post('document', obj).then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }
}
