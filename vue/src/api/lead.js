import axios from 'axios';
import config from '../config';
import globalApi from './global';

import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import error from './errorStore';
import to from '../utils/To';
import toParams from '../utils/urlParams';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathLead,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }


  /**
   * Получение профиля
   */
  profile() {
    return new Promise((resolve, reject) => {
      this.web.get('user')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Создание лида
   */
  createLead(params) {
    return new Promise((resolve, reject) => {
      this.web.post('lead', to.snake(params))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Создание лида
   */
  getLeads(obj) {
    return new Promise((resolve, reject) => {
      this.web.get(toParams('lead', obj))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(to.camel(response.data.data));
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Справочник кредитных продуктов
   */
  infoCp() {
    return new Promise((resolve, reject) => {
      this.web.get('info/product')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Калькуляция займа и графика платежей
   * @param {*} obj
   */
  calculate(obj) {
    return new Promise((resolve, reject) => {
      this.web.get('service/schedule-payment', {
        params: to.snake(obj),
      })
        .then((response) => {
          if (response.data && response.data.error && response.data.error.message) {
            reject(response.data.error.message);
          } else {
            resolve(response.data.data);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник регионов
   */
  infoRegions() {
    return new Promise((resolve, reject) => {
      this.web.get('info/region')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник брендов авто
   */
  infoAutoBrands() {
    return new Promise((resolve, reject) => {
      this.web.get('info/auto-brand')
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  /**
   * Справочник моделей авто на основе бренда
   * @param {*} e
   */
  infoAutoModels(e) {
    return new Promise((resolve, reject) => {
      this.web.get(`info/auto-model/${e}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
  logout() {
    return new Promise((resolve, reject) => {
      this.web.delete('user/auth')
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }

  /**
   * Генерация pdf
   * @param {*} obj
   */
  pdf(obj) {
    return new Promise((resolve, reject) => {
      console.log(this.web);
      this.web.get('service/generate/pdf', { params: {
        ...to.snake(obj),
      } }).then((data) => {
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  /**
   * Загрузка документов на просмотр
   */

  docs(query) {
    return new Promise((resolve, reject) => {
      this.web.get(query ? toParams('document', query) : 'document').then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }
}
