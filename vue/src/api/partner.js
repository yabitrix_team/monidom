import axios from 'axios';
import config from '../config';
import to from '../utils/To';
import globalApi from './global';
import { response as responseFilter, errors as errorsFilter } from './axiosFilter';
import toParams from '../utils/urlParams';

export default class Api extends globalApi {
  constructor() {
    super();
    this.web = axios.create({
      baseURL: config.api.pathPartner,
    });
    this.web.interceptors.response.use(responseFilter, errorsFilter);
  }

  /**
   * Обновить mode заявки
   */
  updateRequestMode(params) {
    return new Promise((resolve, reject) => {
      this.web.post('request/mode', to.snake(params))
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Проверка пароля
   */
  verify(password, code) {
    return new Promise((resolve, reject) => {
      this.web.post('user/verify', {
        password,
        code,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Проверка sms в конце заявки
   */
  verifySms(sms, code) {
    return new Promise((resolve, reject) => {
      this.web.post('user/verifysms', {
        code,
        sms,
      })
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Подтверждение карты
   */
  approveCard(req) {
    return new Promise((resolve, reject) => {
      this.web.post('service/card-verifier/create', req).then(({ data }) => {
        resolve(data);
      }).catch(e => reject(e));
    });
  }

  /**
   * Подтверждение карты
   */
  checkCardApprove(code) {
    return new Promise((resolve, reject) => {
      this.web.post('service/card-verifier/event', {
        code,
      }).then((res) => {
        resolve(res);
      }).catch(e => reject(e));
    });
  }

  /**
   * Получить 1 пакет документов
   */
  firstDocuments(num) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/first-document/${num}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Получить 2 пакет документов
   */
  secondDocuments(num) {
    return new Promise((resolve, reject) => {
      this.web.get(`request/second-document/${num}`)
        .then((response) => {
          if (response.data.error.length === 0) {
            if (!response.data.data.stay) {
              resolve(response.data.data);
            }
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Получить информацию "Дополнительный доход"
   */
  getIncome() {
    return new Promise((resolve, reject) => {
      this.web.get('info/income')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
   * Получить информацию "о компании"
   */
  getAbout() {
    return new Promise((resolve, reject) => {
      this.web.get('info/about-company')
        .then((response) => {
          resolve(response.data.data);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * получить  История заявок с пагинацией
   */
  history(params) {
    return new Promise((resolve, reject) => {
      this.web.post('/history', params)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * получить  клиентов разных типов с пагинацией
   */
  clientList(type, params) {
    return new Promise((resolve, reject) => {
      let strpost;
      switch (type) {
        case 'work':
          strpost = 'client';
          break;
        case 'active':
          strpost = 'client/active';
          break;
        case 'closed':
          strpost = 'client/closed';
          break;
        case 'wait':
          strpost = 'client/wait';
          break;
        default:
          strpost = 'client';
      }
      this.web.post(strpost, params)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }
  /**
   * получить  клиентов разных типов с пагинацией
   */
  clientListAdmin(type, params) {
    return new Promise((resolve, reject) => {
      let strpost;
      switch (type) {
        case 'work':
          strpost = 'client';
          break;
        case 'active':
          strpost = 'client/active';
          break;
        case 'closed':
          strpost = 'client/closed';
          break;
        case 'wait':
          strpost = 'client/wait';
          break;
        default:
          strpost = 'client';
      }
      this.web.post(strpost, params)
        .then((response) => {
          if (response.data.error.length === 0) {
            resolve(response.data.data);
          } else {
            reject(response.data.error);
          }
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    });
  }

  /**
  * Загрузка документов на просмотр
  */

  docs(query) {
    return new Promise((resolve, reject) => {
      this.web.get(query ? toParams('document', query) : 'document').then(({ data }) => {
        if (data.error && data.error.message) {
          reject(data.error.message);
        } else {
          resolve(data);
        }
      }).catch((e) => {
        reject(e);
      });
    });
  }
}
