import { requestModel } from './model';

export default {
  socket: {
    isConnected: true,
    message: '',
    reconnectError: false,
  },
  regions: [],
  sms: {
    reset: '',
  },
  user: {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    secondName: '',
    isAccreditated: '',
    role: '',
  },
  approvedCardState: {
    card_number: '',
    card_token: null,
    event: null,
  },
  lead: {
    step: 1,
    num: '',
    code: '',
    summ: '',
    months: '',
    clientMobilePhone: '',
    clientFirstName: '',
    clientLastName: '',
    clientPatronymic: '',
    clientPassportSerialNumber: '',
    clientPassportNumber: '',
    vin: '',
  },
  /* договоры клиента */
  agreements: {
    active: [],
    closed: [],
    noActive: 0,
    noClosed: 0,
  },
  requests: '',
  request: { ...requestModel },
  notify: {},
  statuses: [],
  vermess: {
    show: false,
    message: '',
    code: '',
    files: [],
  },
  notificationsShow: false,
  notifyCount: 0,
  support: {
    show: false,
  },
  graph: {},
  calculator: {},
  menuItems: [
    {
      id: 0,
      link: '/',
      icon: '/static/i/home.svg',
      label: 'Главная',
    },
    {
      id: 1,
      link: '/vlist',
      icon: '/static/i/message.svg',
      label: 'Сообщения',
    },
  ],
  loading: false,
};
