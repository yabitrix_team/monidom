export default {
  notificationsShow: state => state.notificationsShow,
  supportShow: state => state.support.show,
  notifyCount: state => state.notifyCount,
  graph: state => state.graph,
  graphEarly: state => state.graph.early && state.graph.early.schedule,
  graphFull: state => state.graph.full && state.graph.full.schedule,
  calculator: state => state.calculator,
  loading: state => state.loading,
  user: state => state.user,
  lead: state => state.lead,
  // теперь приходит массив информации по продуктам
  // в геттере будет описана логика какой элемент массива брать
  productInfo: state => (state.graph.product_info ? state.graph.product_info[0] : null),
  activeAgreements: state => state.agreements.active,
  closedAgreements: state => state.agreements.closed,
  approvedCardState: state => state.approvedCardState,
};
