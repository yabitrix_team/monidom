import { requestModel } from './model';

export default {
  CLEAN_REQUEST(state) {
    state.request = requestModel;
  },
  UPDATE_LEAD(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.lead[key] = payload[key];
    });
  },
  UPDATE_AGREEMENTS_ACTIVE(state, payload) {
    if (payload.noActiveContracts) {
      state.agreements.noActive = 1;
    } else {
      state.agreements.active = payload;
      state.agreements.noActive = 0;
    }
  },
  UPDATE_APPROVED_STATUS(state, data) {
    state.approvedCardState = data;
  },
  UPDATE_AGREEMENTS_CLOSED(state, payload) {
    if (payload.noClosedContracts) {
      state.agreements.noClosed = 1;
    } else {
      state.agreements.closed = payload;
      state.agreements.noClosed = 0;
    }
  },
  UPDATE_SMS(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.sms[key] = payload[key];
    });
  },
  UPDATE_REGIONS(state, payload) {
    state.regions = payload;
  },
  UPDATE_GRAPH(state, data = {}) {
    state.graph = data;
  },
  LKK_BLOCK(state, data) {
    if (data && data.num === state.request.num) {
      state.request.step = 12;
    }
  },
};
