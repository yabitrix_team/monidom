import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import _ from 'lodash';
import Url from '../../utils/UrlHelper';
import Api from '../../api/client';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
import to from '../../utils/To';

Vue.use(Vuex);

const api = new Api();
Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    changeFieldsRequest(context, data) {
      if (parseInt(data.data.num, 10) === parseInt(context.state.request.num, 10)) {
        console.log('меняем сумму в открытой заявке');
        context.commit('UPDATE_REQUEST', {
          summ: data.data.fields.summ,
        });
      }
    },
    insertNotify(context, data) {
      context.commit('UPDATE_NOTIFY', data);
    },
    verMessage(context, data) {
      context.commit('VERMESS_OPEN', data);
    },
    // eslint-disable-next-line
    logout(context) {
      api.logout()
        .then(() => {
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        })
        .catch(() => {
          console.error('Logout error');
        });
    },
    sendCode(context, phone) {
      return new Promise((resolve) => {
        api.sendSms(phone)
          .then((data) => {
            if (to.camel(data).sendCode === true) {
              const date = new Date();
              context.commit('UPDATE_SMS', {
                reset: (Date.parse(date) + 300000),
              });
              context.commit('UPDATE_LEAD', {
                step: 3,
              });
              resolve();
            }
          });
      });
    },
    checkCode(context, payload) {
      return new Promise((resolve, reject) => {
        api.checkSms(context.state.lead.clientMobilePhone, payload)
          .then((data) => {
            if (to.camel(data).checkCode === true) {
              api.register({
                login: context.state.lead.clientMobilePhone,
                password: context.state.lead.password,
                name: context.state.lead.clientFirstName,
                last_name: context.state.lead.clientLastName,
                second_name: context.state.lead.clientPatronymic,
              }).then((regdata) => {
                if (regdata.authorized === true) {
                  api.deleteLead(context.state.lead.code).then((leaddata) => {
                    if (leaddata === 1) {
                      context.commit('UPDATE_LEAD', {
                        password: '',
                        step: '',
                      });
                    }
                  });
                  resolve();
                }
              }).catch((e) => {
                reject(e);
              });
            }
          });
      });
    },
    getRequest(context, code) {
      api.getRequest(code)
        .then((data) => {
          context.commit('UPDATE_REQUEST', to.camel(data));
        });
    },
    saveFile(context, { saveTo, file }) {
      const id = _.uniqueId('code_');
      const fileObj = {
        ...file,
        ...{
          code: undefined,
          id,
        },
      };

      context.commit('FILES_ADD', { fileObj, saveTo });

      api.sendFile(file)
        .then((data) => {
          context.commit('FILES_SET_CODE', { code: data.code, saveTo, id });
        });
    },
    removeFile(context, { saveTo, key }) {
      const elem = context.state.request[to.camel(saveTo)][key];
      api.deleteFile(elem.code)
        .then(() => {
          context.commit('FILES_DELETE', { key, saveTo });
        });
    },
    getAgreementsActive(context, { force, code } = {}) {
      if (!this.state.agreements.active.length || force) {
        api.agreementsActive(code)
          .then((data) => {
            context.commit('UPDATE_AGREEMENTS_ACTIVE', data);
          });
      }
    },
    getAgreementsClosed(context, { force, code } = {}) {
      if (!this.state.agreements.closed.length || force) {
        api.agreementsClosed(code)
          .then((data) => {
            context.commit('UPDATE_AGREEMENTS_CLOSED', data);
          });
      }
    },
    lkkBlock(context, { data }) {
      context.commit('LKK_BLOCK', data);
    },
    checkCardStatus({ commit }, code) {
      return new Promise((resolve, reject) => {
        api.checkCardApprove(code).then(({ data: { data } }) => {
          commit('UPDATE_APPROVED_STATUS', data);
          resolve(data);
        }).catch(e => reject(e));
      });
    },
    cardVerifier(context, { data }) {
      context.commit('UPDATE_APPROVED_STATUS', data);
    },
  },
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['request', 'lead', 'sms', 'regions'],
    storage: window.sessionStorage,
  })],
});
