import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import state from './main/state';
import getters from './main/getters';

import mutations from './main/mutations';
import actions from './main/actions';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    main: {
      namespaced: true,
      state,
      mutations,
      actions,
      getters,
    },
  },
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['main/lead', 'main/sms', 'main/timer', 'main/step', 'main/login'],
    storage: window.sessionStorage,
  })],
});
