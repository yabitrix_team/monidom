import Cookies from 'js-cookie';
import Api from '@/api/register';
import to from '@/utils/To';
import config from '@/config';
import { clearPhone } from '@/utils/ClearPhone';

const api = new Api();

export default {
  sendCode(context) {
    return new Promise((resolve, reject) => {
      api.sendSms(clearPhone(context.state.login))
        .then((data) => {
          if (to.camel(data).sendCode === true) {
            const date = new Date();
            context.commit('UPDATE_SMS', {
              reset: (Date.parse(date) + 300000),
            });
            context.commit('UPDATE_LEAD', {
              step: 3,
            });
            resolve();
          }
        }).catch((e) => {
          reject(e);
        });
    });
  },
  checkCode(context, payload) {
    return new Promise((resolve, reject) => {
      const info = {
        login: clearPhone(context.state.login),
        password: context.state.password,
        name: context.state.lead.clientFirstName,
        last_name: context.state.lead.clientLastName,
        second_name: context.state.lead.clientPatronymic,
      };
      api.checkSms(info.login, payload)
        .then((data) => {
          if (to.camel(data).checkCode === true) {
            api.register(info).then((regdata) => {
              if (regdata.userExists) {
                context.commit('UPDATE', {
                  step: 4,
                });
                reject();
              } else if (regdata.authorized === true) {
                context.commit('UPDATE_LEAD', {
                  password: '',
                  step: '',
                });
                resolve(regdata);
              }
            });
          }
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  docs(context, payload) {
    api.doc(payload.join(','))
      .then(({ data }) => {
        context.commit('SET_DOCS', data);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  getLead(context, code) {
    return new Promise((resolve) => {
      api.lead(code)
        .then(({ data }) => {
          if (data.userExists) {
            context.commit('UPDATE', {
              step: 4,
            });
            context.commit('UPDATE_LEAD', {
              clientFirstName: data.firstName,
            });
            Cookies.set('phone', data.phone, { domain: `.${config.domain}`, expires: 30 });
            resolve(true);
          } else {
            context.commit('UPDATE', {
              step: 1,
              lead: data,
            });
            resolve(true);
          }
        })
        .catch(() => {
          resolve(false);
        });
    });
  },
};
