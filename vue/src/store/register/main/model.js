export default {
  step: 1,
  sms: {
    reset: '',
  },

  login: '',
  password: '',

  timer: {
    sec: 0,
    min: 0,
  },

  lead: {
    clientBirthday: null,
    clientFirstName: '',
    clientLastName: '',
    clientMobilePhone: '',
    clientPassportNumber: null,
    clientPassportSerialNumber: null,
    clientPatronymic: '',
    code: '',
    months: null,
    summ: null,
    vin: '',
  },
  docs: [],
};
