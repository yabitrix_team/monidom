export default {
  step: state => +state.step || 1,
  code: state => state.code || '',
  docs: state => state.docs,
  login: context => context.login,
  password: context => context.password,
  firstName: context => context.lead.clientFirstName,
  patronymic: context => context.lead.clientPatronymic,
  lastName: context => context.lead.clientLastName,
  mobilePhone: context => context.lead.clientMobilePhone || context.login,
  // будет true если пришел лид
  isLead: context => !!(context.lead.code && context.lead.code.length > 0),
  timerMin: context => context.timer.min || 0,
  timerSec: context => context.timer.sec || 0,
};
