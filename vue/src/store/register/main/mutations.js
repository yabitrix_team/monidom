import model from './model';

export default {
  UPDATE(state, payload) {
    Object.keys(payload).forEach((key) => {
      state[key] = payload[key];
    });
  },
  UPDATE_LEAD(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.lead[key] = payload[key];
    });
  },
  CLEAN_STATE() {
    this.replaceState(model);
  },
  UPDATE_SMS(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.sms[key] = payload[key];
    });
  },
  UPDATE_TIMER(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.timer[key] = payload[key];
    });
  },
  SET_DOCS(state, payload) {
    state.docs = [...payload];
  },
  SET(state, { name, value }) {
    state[name] = value;
  },
};
