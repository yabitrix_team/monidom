import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import editUser from '@/store/modules/editUser';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import globalMutations from '../mutations';

Vue.use(Vuex);

Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  plugins: [
    createPersistedState({
      key: 'storage',
      paths: ['groups', 'user_info'],
      storage: window.sessionStorage,
    }),
  ],
  modules: {
    editUser,
  },
});
