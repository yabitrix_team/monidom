import to from '@/utils/To';
import Api from '@/api/call';


const api = new Api();

export default {
  LEADS({ commit }, obj) {
    commit('LOADING', true);
    api.leads(to.snake(obj)).then(({ data }) => {
      commit('LOADING', false);
      commit('_LEADS', data);
    }).catch((e) => {
      commit('LOADING', false);
      commit('_LEADS_ERROR', e);
    });
  },

  CALLS({ commit }, { page, next, filter }) {
    api.list({ page, filter }).then((data) => {
      commit('_CALLS', data);
      if (next) {
        next();
      }
    });
  },
  CANCEL({ commit }, code) {
    return new Promise((resolve, reject) => {
      commit('_CANCEL', code);
      api.cancel(code)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  RECOVER({ commit }, code) {
    return new Promise((resolve, reject) => {
      commit('_RECOVER', code);
      api.recover(code)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};
