import to from '@/utils/To';

export default {
  _USERS(state, data) {
    state.users = to.camel(data.items);
    state.usersPageCount = data.count;
  },
  _LEADS(state, data) {
    const cameled = to.camel(data.items);
    state.leads = cameled.map((e) => { e.smsStatus = ''; e.loading = false; return e; });
    state.leadsPageCount = data.count;
  },
  _SMS_STATUS_LOAD(state, { val, row }) {
    (state.leads.find(el => el.id === row.id)).loading = val;
  },
  _GROUPS(state, { data }) {
    state.groups = data;
  },
  _CREATE_LINK(state, link) {
    state.link = link;
  },
  _CREATE_LINK_ERROR(state, e) {
    state.linkError = e;
  },
  _USER_INFO(state, data) {
    state.user_info = data;
  },
  _CREATE_USER(state, data) {
    state.user = data;

    setTimeout(() => {
      state.user = '';
    }, 4500);
  },
  _CREATE_USER_ERROR(state, error) {
    state.userError = error;
  },
  _SMS_STATUS(state, data) {
    (state.leads.find(el => el.id === data.row.id)).smsStatus = data.status;
  },
  LOADING(state, bool) {
    state.loading = bool;
  },

  // DOCS
  _LOAD_DOCS(state, data) {
    state.documents = data;
  },

  _CALLS(state, data) {
    state.items = data.data.items.map(el => Object.assign(el, { active: true }));
    state.headers = data.meta.headers;
    state.serverTime = data.meta.server_time;
    state.pageCount = data.data.count;
  },
  _CANCEL(state, code) {
    const index = state.items.findIndex(item => item.code === code);
    state.items[index].active = false;
  },
  _RECOVER(state, code) {
    const index = state.items.findIndex(item => item.code === code);
    state.items[index].active = true;
  },
};
