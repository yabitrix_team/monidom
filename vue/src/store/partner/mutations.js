import { requestModel } from './model';

export default {
  CLEAN_REQUEST(state) {
    state.request = { ...requestModel };
  },
  UPDATE_FIRST_DOCUMENTS(state, message) {
    const docs = [];
    message.docPack1.forEach((item) => {
      docs.push({
        url: item.url,
        name: item.name,
        description: item.description,
        code: item.code,
      });
    });
    state.request.docPack1 = docs;
  },
  LOAD_DOCS(state, data) {
    state.documents = data;
  },
  UPDATE_APPROVED_STATUS(state, data) {
    state.approvedCardState = data;
  },
  UPDATE_SECOND_DOCUMENTS(state, message) {
    const docs = [];
    message.docPack2.forEach((item) => {
      docs.push({
        url: item.url,
        name: item.name,
        description: item.description,
        code: item.code,
      });
    });
    state.request.docPack2 = docs;
  },
  UPDATE_GRAPH(state, data = {}) {
    state.graph = data;
  },
  UPDATE_REGIONS(state, payload) {
    state.regions = payload;
  },
  UPDATE_CLIENTS(state, payload) {
    state.clients = payload;
  },
};
