// eslint-disable-next-line
export const requestModel = {
  /* технические параметры заявки */
  showFoldedStep: '',
  bar: {
    progress: '',
    color: '',
    percent: '',
    left: '',
  },
  code: '',
  step: '',
  mode: '',
  num: '',
  /* калькулятор */
  summ: '',
  dateReturn: '',
  autoBrandId: '',
  autoModelId: '',
  autoYear: '',
  autoPrice: '',
  addCredit: '',
  /* предварительная заявка */
  cardApproved: false,
  clientFirstName: '',
  clientLastName: '',
  clientPatronymic: '',
  clientPassportNumber: '',
  clientPassportSerialNumber: '',
  clientBirthday: '',
  clientRegionId: '',
  clientRegionName: '',
  commentUser: '',
  /* полная заявка */
  clientHomePhone: '',
  clientEmail: '',
  clientTotalMonthlyIncome: '',
  clientTotalMonthlyOutcome: '',
  clientEmploymentId: '',
  clientWorkplaceName: '',
  clientWorkplaceAddress: '',
  clientWorkplaceExperience: '',
  clientWorkplacePeriodId: '',
  clientWorkplacePhone: '',
  clientGuarantorName: '',
  clientGuarantorRelationId: '',
  clientGuarantorPhone: '',
  placeOfStay: '',
  creditProductId: '',
  commentPartner: '',
  /* способы оплаты */
  methodOfIssuanceId: '',
  cardNumber: '',
  cardHolder: '',
  bankBik: '',
  checkingAccount: '',
  /* toggle баттоны в полной заявке */
  haveWork: true,
  haveGuarantor: true,
  /* документы по заявке */
  docPack1: [],
  docPack2: [],
  /* файлы по заявке */
  fotoPts: [],
  fotoSts: [],
  fotoPassport: [],
  fotoClient: [],
  fotoAuto: [],
  fotoCard: [],
  /* аккредитация */
  accreditationNum: '',
  /* предварительно одобрен */
  preApproved: '',
  /* принудительное подписание первого пакета */
  reFirstPack: false,
};
