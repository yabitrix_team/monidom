import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
import createPersistedState from 'vuex-persistedstate';
import _ from 'lodash';
import Url from '../../utils/UrlHelper';
import Api from '../../api/partner';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
import to from '../../utils/To';

Vue.use(Vuex);
const api = new Api();
Object.assign(mutations, globalMutations);
export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    lkkBlock() {},
    changeFieldsRequest(context, { data }) {
      if (Number(data.num) === Number(context.state.request.num)) {
        console.log('меняем сумму в открытой заявке');
        context.commit('UPDATE_REQUEST', to.camel(data.fields));
      }
    },
    hasFirstPack(context, data) {
      api.firstDocuments(data.data)
        .then((res) => {
          if (!res.stay) {
            context.commit('UPDATE_FIRST_DOCUMENTS', res);
          }
        });
    },
    hasSecondPack(context, data) {
      api.secondDocuments(data.data)
        .then((res) => {
          if (!res.stay) {
            context.commit('UPDATE_SECOND_DOCUMENTS', res);
          }
        });
    },
    reFirstPack(context, { data }) {
      if (context.state.request.num === data.num) {
        context.commit('UPDATE_REQUEST', {
          reFirstPack: true,
        });
      }
    },
    getClients(context, { dateFrom, dateTo, pagin, type, admin }) {
      return new Promise((resolve, reject) => {
        api[admin ? 'clientListAdmin' : 'clientList'](type,
          {
            pagin,
            dateFrom: moment(dateFrom).format('DD.MM.YYYY'),
            dateTo: moment(dateTo).format('DD.MM.YYYY'),
          }).then((data) => {
          const response = {};
          if (data.requests.length !== 0) {
            response.data = to.camel(data.requests);
            response.pageCount = Math.ceil(data['total count'] / data['count in page']);
            context.commit('UPDATE_CLIENTS', response);
            resolve(response);
          } else {
            response.data = [];
            response.pageCount = 0;
            context.commit('UPDATE_CLIENTS', response);
            reject('По запросу заявки отсутствуют');
          }
        }).catch(e => reject(e));
      });
    },
    insertNotify(context, data) {
      context.commit('UPDATE_NOTIFY', data);
    },
    cardApproved(context, { data }) {
      context.commit('UPDATE_CARD_APPROVED', data);
    },
    verMessage(context, data) {
      context.commit('VERMESS_OPEN', data);
    },
    // eslint-disable-next-line
    logout(context) {
      api.logout()
        .then(() => {
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        })
        .catch(() => {
          console.error('Logout error');
        });
    },
    getRequest(context, code) {
      api.getRequest(code)
        .then((data) => {
          context.commit('UPDATE_REQUEST', to.camel(data));
        });
    },
    saveFile(context, { saveTo, file }) {
      const id = _.uniqueId('code_');
      const fileObj = {
        ...file,
        ...{
          code: undefined,
          id,
        },
      };
      context.commit('FILES_ADD', { fileObj, saveTo });
      api.sendFile(file)
        .then((data) => {
          context.commit('FILES_SET_CODE', { code: data.code, saveTo, id });
        });
    },
    loadDocs({ commit }, query) {
      api.docs(query).then(({ data }) => commit('LOAD_DOCS', data));
    },
    loadSingleDoc(scope, query) {
      return new Promise(resolve => api.docs(query).then(({ data }) => resolve(data)));
    },
    removeFile(context, { saveTo, key }) {
      const elem = context.state.request[to.camel(saveTo)][key];
      api.deleteFile(elem.code)
        .then(() => {
          context.commit('FILES_DELETE', { key, saveTo });
        });
    },
    checkCardStatus({ commit }, code) {
      return new Promise((resolve, reject) => {
        api.checkCardApprove(code).then(({ data: { data } }) => {
          commit('UPDATE_APPROVED_STATUS', data);
          resolve(data);
        }).catch(e => reject(e));
      });
    },
    cardVerifier(context, { data }) {
      context.commit('UPDATE_APPROVED_STATUS', data);
    },
  },
  plugins: [
    createPersistedState({
      key: 'storage',
      paths: ['request', 'user', 'graph', 'documents'],
      storage: window.sessionStorage,
    }),
  ],
});
