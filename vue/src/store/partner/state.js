import moment from 'moment';
import { requestModel } from './model';

export default {
  socket: {
    isConnected: true,
    message: '',
    reconnectError: false,
  },
  regions: [],
  documents: [],
  user: {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    secondName: '',
    isAccreditated: null,
    pointName: '',
    partnerName: '',
  },
  request: { ...requestModel },
  notify: {},
  statuses: [],
  clients: {},
  vermess: {
    show: false,
    message: '',
    code: '',
    files: [],
  },
  approvedCardState: {
    card_number: '',
    card_token: null,
    event: null,
  },
  notificationsShow: false,
  notifyCount: 0,
  support: {
    show: false,
  },
  graph: {},
  calculator: {},
  menuItems: [
    {
      id: 0,
      link: '/',
      icon: '/static/i/home.svg',
      label: 'Главная',
    },
    {
      id: 1,
      link: `/listclients/work/${moment().subtract(1, 'months').format('DD.MM.YYYY')}/${moment()
        .format('DD.MM.YYYY')}/1`,
      icon: '/static/i/clients.svg',
      label: 'Клиенты',
    },
    {
      id: 2,
      link: '/instructions',
      icon: '/static/i/instruction.svg',
      label: 'Инструкции',
    },
    {
      id: 3,
      link: '/vlist',
      icon: '/static/i/message.svg',
      label: 'Сообщения',
    },
    {
      id: 4,
      link: '/rules',
      icon: '/static/i/docs.svg',
      label: 'Документы',
    },
  ],
  loading: false,
};
