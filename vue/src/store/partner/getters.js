export default {
  notificationsShow: state => state.notificationsShow,
  supportShow: state => state.support.show,
  notifyCount: state => state.notifyCount,
  graph: state => state.graph,
  graphEarly: state => state.graph.early && state.graph.early.schedule,
  graphFull: state => state.graph.full && state.graph.full.schedule,
  clients: state => state.clients.data,
  clientsCount: state => state.clients.pageCount,
  statuses: state => state.statuses,
  documents: state => state.documents,
  calculator: state => state.calculator,
  loading: state => state.loading,
  isLoading: (state) => {
    const check = ['fotoAuto', 'fotoCard', 'fotoClient', 'fotoPassport', 'fotoPts', 'fotoSts'];
    let loading = false;
    check.forEach((item) => {
      if (state.request[item] &&
          state.request[item] !== '' &&
          state.request[item].filter(i => i.code === undefined).length > 0) {
        loading = true;
      }
    });
    return loading;
  },
  // теперь приходит массив информации по продуктам
  // в геттере будет описана логика какой элемент массива брать
  productInfo: state => (state.graph.product_info ? state.graph.product_info[0] : null),
  request: state => state.request,
  approvedCardState: state => state.approvedCardState,
  docPack1Length: state => state.request.docPack1.length,
};
