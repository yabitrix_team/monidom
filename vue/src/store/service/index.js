import Vue from 'vue';
import Vuex from 'vuex';
// import _ from 'lodash';
// import Url from '../../utils/UrlHelper';
// import Api from '../../api/client';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
// import to from '../../utils/To';

Vue.use(Vuex);

// const api = new Api();
Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    changeFieldsRequest(context, data) {
      if (parseInt(data.data.num, 10) === parseInt(context.state.request.num, 10)) {
        context.commit('UPDATE_REQUEST', {
          summ: data.data.fields.summ,
        });
      }
    },
    verMessage(context, data) {
      context.commit('VERMESS_OPEN', data);
    },
  },
});
