export default {
  VERMESS_NEW_OPEN(state) {
    state.vermessNew.show = true;
  },
  VERMESS_NEW_CLOSE(state) {
    state.vermessNew.show = false;
  },
};
