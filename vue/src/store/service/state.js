export default {
  socket: {
    isConnected: true,
    message: '',
    reconnectError: false,
  },
  vermess: {
    show: false,
    files: [],
    message: '',
  },
  vermessNew: {
    show: false,
    files: [],
    message: '',
  },
};
