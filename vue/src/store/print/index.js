import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import Api from '../../api/print';
import state from './state';
import mutations from './mutations';
import globalMutations from '../mutations';
import to from '../../utils/To';

Vue.use(Vuex);
const api = new Api();
Object.assign(mutations, globalMutations);
export default new Vuex.Store({
  state,
  mutations,
  actions: {
    printLink(context, code) {
      return new Promise((resolve, reject) => {
        api.printLink(code)
          .then((data) => {
            if (!data.data) {
              reject(new Error('Файлов нет'));
            } else if (data.error.length === 0) {
              context.commit('UPDATE_PRINTLINK', to.camel(data));
              resolve(true);
            } else {
              reject(data.error.message);
            }
          })
          .catch((e) => {
            reject(e);
          });
      });
    },
  },
  plugins: [
    createPersistedState({
      key: 'storage',
      paths: ['request'],
      storage: window.sessionStorage,
    }),
  ],
});
