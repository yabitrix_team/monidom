import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import _ from 'lodash';
import Url from '../../utils/UrlHelper';
import Api from '../../api/agent';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
import to from '../../utils/To';

Vue.use(Vuex);

const api = new Api();
Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    changeFieldsRequest(context, data) {
      if (parseInt(data.data.num, 10) === parseInt(context.state.request.num, 10)) {
        console.log('меняем сумму в заявке агента', data.data);
        api.getRequest({
          code: data.data.code,
        })
          .then((res) => {
            context.commit('UPDATE_REQUEST', to.camel(res));
          });
        // api.clean(data.data.code)
        //   .then((result) => {
        //     if (result.success) {
        //       api.getRequest(data.data.code)
        //         .then((data) => {
        //           context.commit('UPDATE_REQUEST', to.camel(data));
        //         });
        //     }
        //   });
        // context.commit('UPDATE_REQUEST', {
        //   summ: data.data.fields.summ,
        // });
      }
    },
    lkkBlock() {},
    hasSecondPack(context, data) {
      api.secondDocuments(data.data)
        .then((res) => {
          context.commit('UPDATE_SECOND_DOCUMENTS', res);
        });
    },
    // eslint-disable-next-line
    logout(context) {
      api.logout()
        .then(() => {
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        })
        .catch(() => {
          console.error('Logout error');
        });
    },
    insertNotify(context, data) {
      context.commit('UPDATE_NOTIFY', data);
    },
    verMessage(context, data) {
      context.commit('VERMESS_OPEN', data);
    },
    getRequest(context, code) {
      api.getRequest(code)
        .then((data) => {
          context.commit('UPDATE_REQUEST', to.camel(data));
        });
    },
    loadDocs({ commit }, query) {
      api.docs(query).then(({ data }) => commit('LOAD_DOCS', data));
    },
    saveFile(context, { saveTo, file }) {
      const id = _.uniqueId('code_');
      const fileObj = {
        ...file,
        ...{
          code: undefined,
          id,
        },
      };

      context.commit('FILES_ADD', { fileObj, saveTo });

      api.sendFile(file)
        .then((data) => {
          context.commit('FILES_SET_CODE', { code: data.code, saveTo, id });
        });
    },
    loadSingleDoc(scope, query) {
      return new Promise(resolve => api.docs(query).then(({ data }) => resolve(data)));
    },
    removeFile(context, { saveTo, key }) {
      // console.log(context.state.request);
      const elem = context.state.request[to.camel(saveTo)][key];
      api.deleteFile(elem.code)
        .then(() => {
          context.commit('FILES_DELETE', { key, saveTo });
        });
    },
    canSignature(context, { data }) {
      context.commit('CAN_SIGNATURE', data);
    },
  },
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['requests', 'request', 'user', 'documents'],
    storage: window.sessionStorage,
  })],
});
