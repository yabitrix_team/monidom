export default {
  notificationsShow: state => state.notificationsShow,
  supportShow: state => state.support.show,
  notifyCount: state => state.notifyCount,
  loading: state => state.loading,
  documents: state => state.documents,
};
