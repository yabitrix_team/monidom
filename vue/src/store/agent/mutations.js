export default {
  /*
  * Получение списка заявок
   */
  LOAD_DOCS(state, data) {
    state.documents = data;
  },
  UPDATE_REQUESTS(state, payload) {
    state.requests = payload;
  },
  UPDATE_SECOND_DOCUMENTS(state, message) {
    const docs = [];
    message.docPack2.forEach((item) => {
      docs.push({
        url: item.url,
        name: item.name,
        description: item.description,
        code: item.code,
      });
    });
    state.request.docPack2 = docs;
  },
  CAN_SIGNATURE(state, data) {
    if (data && data.num === state.request.num) {
      state.request.canSignature = true;
    }
  },
};
