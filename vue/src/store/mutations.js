import to from '../utils/To';


const cabinet = window.location.hostname.split('.')[0];
const STATUS_PRE_APPROVED = 'cbb42353-51e6-4a88-b2ef-575729da0343';
const STATUS_SUCCESS = 'abfa1724-c3fd-4b7f-9631-7572288a165b';
const STATUS_SUCCESS2 = '74ed5b21-af9b-44f3-bda5-85fde6847b76';
const STATUS_SUCCESS_LKK = '1b61eb3a-c293-4fca-8ee6-f672c43302ef';
const STATUS_REJECT = 'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e';
const STATUS_CANCELED = 'c37fec4e-28b3-4239-8c2f-6812821b1b32';
const STATUS_CANCELED2 = 'e01a9627-195b-4422-860e-91ba96897840';

export default {
  // обновление уведомлений в сайдбаре
  UPDATE_NOTIFY(state, message) {
    const newNotify = {};
    const oldNotify = state.notify;

    Object.keys(message.data).forEach((key) => {
      const status = message.data[key].status || 1;
      // красим прогресс-бар
      // todo: перенести окрашивание на бек
      if (state.request.num === key) {
        state.request.bar.color = state.statuses[status].color;
        state.request.bar.progress = state.statuses[status].progress;
        state.request.bar.percent =
          Math.round((parseInt(state.statuses[status].progress, 10) * 100) / 100);
      }

      if (state.statuses[status] && state.request.num === key) {
        switch (state.statuses[status].guid) {
          // делаем чтобы в ЛКП юзер не мог пройти далее
          // делаем чтобы в ЛКК юзер не мог пройти далее
          case STATUS_PRE_APPROVED:
            if (cabinet === 'partner' ||
                cabinet === 'client') {
              state.request.preApproved = 1;
            }
            break;

          // переводим заявку в успех в ЛКП
          case STATUS_SUCCESS:
          case STATUS_SUCCESS2:

            if (cabinet === 'partner') {
              console.log('перевести заявку в успех ЛКП', parseInt((message.data[key].status
              ), 10), state.request.num, key, state.request.step);
              state.request.step = 7;
            }
            // переводим заявку в успех в ЛКВМ
            if (state.request.step === 5 &&
              cabinet === 'agent') {
              state.request.step = 5;
            }
            break;
          // переводим заявку на успех в ЛКК
          case STATUS_SUCCESS_LKK:
            if ((state.request.num === key && state.request.step === 4) &&
              (cabinet === 'client')
            ) {
              console.log('перевести заявку в успех ЛКК', parseInt((message.data[key].status
              ), 10), state.request.num, key, state.request.step);
              state.request.step = 5;
            }
            break;
          // переводим заявку в отказано во всех кабинетах
          case STATUS_REJECT:
            console.log('перевести заявку в отказано', parseInt((message.data[key].status
            ), 10), state.request.num, key, state.request.step);
            state.request.step = 10;
            break;

          // переводим заявку в отозвано во всех кабинетах
          case STATUS_CANCELED:
          case STATUS_CANCELED2:
            console.log('перевести заявку в отозвано', parseInt((message.data[key].status
            ), 10), state.request.num, key, state.request.step);
            state.request.step = 11;
            break;
          default:
            // console.log('');
        }
      }

      if (oldNotify[`req${key}`]) {
        if (oldNotify[`req${key}`].statusId === message.data[key].status) {
          let newAlertsLength = 0;
          Object.keys(message.data[key].alerts).forEach((alertKey) => {
            if (
              message.data[key].alerts[alertKey].message !== null &&
              parseInt(message.data[key].alerts[alertKey].type_id, 10) !== 10 &&
              parseInt(message.data[key].alerts[alertKey].type_id, 10) !== 5
            ) {
              newAlertsLength += 1;
            }
          });
          if (newAlertsLength === Object.keys(oldNotify[`req${key}`].alerts).length) {
            console.log('Заявку не надо обновлять, уже выдан последний статус, но изменилось количество алертов',
              Object.keys(oldNotify[`req${key}`].alerts).length,
              newAlertsLength);
            return;
          }
        }
        delete oldNotify[`req${key}`];
      }
    });
    state.notify = oldNotify;
    Object.keys(message.data).forEach((key) => {
      const status = message.data[key].status || 1;
      newNotify[`req${key}`] = {
        rand: Math.floor(Math.random() * 1000000),
        id: message.data[key].request_num,
        name: `${to.toUpper(message.data[key].client_last_name)} ${to.toUpper(message.data[key].client_first_name)}`,
        sum: message.data[key].summ,
        num: `CM-${message.data[key].request_num}`,
        time: message.data[key].date,
        code: message.data[key].request_code,
        progress: 0,
      };

      newNotify[`req${key}`].statusId = status;
      newNotify[`req${key}`].status = state.statuses[status].name;
      newNotify[`req${key}`].color = state.statuses[status].color;
      newNotify[`req${key}`].progress = state.statuses[status].progress;

      newNotify[`req${key}`].alerts = {};
      Object.keys(message.data[key].alerts).forEach((subkey) => {
        if (
          typeof (message.data[key].alerts[subkey].message === 'string') &&
          message.data[key].alerts[subkey].message !== null &&
          parseInt(message.data[key].alerts[subkey].type_id, 10) !== 10 &&
          parseInt(message.data[key].alerts[subkey].type_id, 10) !== 5
        ) {
          newNotify[`req${key}`].alerts[`a${message.data[key].alerts[subkey].id}`] = {
            id: message.data[key].alerts[subkey].id,
            name: message.data[key].alerts[subkey].message,
            createdAt: message.data[key].alerts[subkey].created_at,
            icon: 'info',
          };
        }
      });
    });

    state.notify = Object.assign({}, newNotify, oldNotify);
    state.notifyCount = Object.keys(state.notify).length;
  },
  // удаление всех уведомлений в сайдбаре
  CLOSE_ALL(state) {
    state.notify = {};
    state.notifyCount = 0;
  },
  // справочник статусов в store/localStorage
  UPDATE_STATUSES(state, payload) {
    if (typeof payload === 'object' && Object.keys(payload).length > 0) {
      state.statuses = payload;
    }
  },
  // обновление профиля пользователя
  UPDATE_PROFILE(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.user[key] = payload[key];
    });
  },
  // обновление заявки в store из пришедшего объекта (любого количества полей)
  UPDATE_REQUEST(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.request[key] = payload[key];
    });
  },
  // сохранение поля в store/localStorage
  UPDATE_INPUT_TEXT(state, payload) {
    state.request[payload.key] = payload.value;
  },
  // закрытие уведомления по заявке
  CLOSE_REQUEST(state, payload) {
    const notify = {};
    Object.keys(state.notify).forEach((key) => {
      if (key !== `req${parseInt(payload, 10)}`) {
        notify[key] = state.notify[key];
      }
    });
    let digit = state.notifyCount;
    digit -= 1;
    state.notify = notify;
    state.notifyCount = digit;
  },
  // закрытие алертов по заявке
  CLOSE_ALERT(state, payload) {
    const alerts = {};
    Object.keys(state.notify[`req${parseInt(payload.num, 10)}`].alerts).forEach((key) => {
      if (key !== `a${parseInt(payload.id, 10)}`) {
        alerts[key] = state.notify[`req${parseInt(payload.num, 10)}`].alerts[key];
      }
    });
    state.notify[`req${parseInt(payload.num, 10)}`].alerts = alerts;
  },
  FILES_ADD(state, { fileObj, saveTo }) {
    if (state.request[to.camel(saveTo)] === undefined || state.request[to.camel(saveTo)] === '') {
      state.request[to.camel(saveTo)] = [];
    }
    try {
      state.request[to.camel(saveTo)].push(fileObj);
    } catch (e) {
      console.log(state, e);
    }
  },
  FILES_SET_CODE(state, { code, saveTo, id }) {
    const key = state.request[to.camel(saveTo)].findIndex(i => i.id === id);
    state.request[to.camel(saveTo)][key].code = code;
    state.request[to.camel(saveTo)][key].file = undefined;
  },
  FILES_DELETE(state, { key, saveTo }) {
    state.request[to.camel(saveTo)].splice(key, 1);
  },

  VERMESS_FILES_ADD(state, file) {
    if (state.vermess.files === undefined || state.vermess.files === '') {
      state.vermess.files = [];
    }
    try {
      state.vermess.files.push(file);
    } catch (e) {
      console.log(state, e);
    }
  },
  VERMESS_FILES_DELETE(state, key) {
    state.vermess.files.splice(key, 1);
  },
  SOCKET_ONOPEN(state) {
    state.socket.isConnected = true;
  },
  SOCKET_ONCLOSE(state) {
    state.socket.isConnected = false;
  },
  SOCKET_ONERROR(state) {
    state.socket.isConnected = false;
    // console.error('socket_error', state, event);
  },
  // mutations for reconnect methods
  SOCKET_RECONNECT() {
    // console.info(state, count)
  },
  SOCKET_RECONNECT_ERROR() {
    // state.socket.reconnectError = true;
  },
  SOCKET_ONMESSAGE(state, message) {
    state.socket.message = message;
  },
  NOTIFICATIONS_HIDE(state) {
    state.notificationsShow = false;
  },
  NOTIFICATIONS_SHOW(state) {
    state.notificationsShow = true;
  },
  SUPPORT_HIDE(state) {
    state.support.show = false;
  },
  SUPPORT_SHOW(state) {
    state.support.show = true;
  },
  SET_PRODUCT_ID(state, id) {
    state.request.creditProductId = id;
  },
  VERMESS_OPEN(state, data) {
    state.vermess.show = true;
    state.vermess.id = data.data.id || data.data.question_id || data.data.questionId;
    state.vermess.message = data.data.message;
    state.vermess.code = data.data.code;
  },
  VERMESS_CLOSE(state) {
    state.vermess.show = false;
    state.vermess.id = '';
    state.vermess.message = '';
    state.vermess.code = '';
    state.vermess.files = [];
  },
  SET_CALCULATOR(state, obj) {
    state.calculator = obj;
  },
  SET_GRAPH(state, payload) {
    const graph = payload;
    graph.early.schedule = Array.isArray(graph.early.schedule) ?
      graph.early.schedule : [graph.early.schedule];
    state.graph = graph;
  },
  ADD_ERROR(state, message) {
    state.error.push({
      id: state.error.length + 1,
      message,
    });
  },
  CLOSE_ERROR(state, id) {
    const rid = state.error.findIndex(i => i.id === id);
    state.error.splice(rid, 1);
  },
  LOADING(state, payload) {
    state.loading = payload;
  },
};
