import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import state from './state';
import mutations from './mutations';
import globalMutations from '../mutations';

Vue.use(Vuex);
Object.assign(mutations, globalMutations);
export default new Vuex.Store({
  state,
  mutations,
  // plugins: [
  //   createPersistedState({
  //     key: 'storage',
  //     paths: ['request'],
  //     storage: window.sessionStorage,
  //   }),
  // ],
  actions: {
    CLEAN_LOST({ commit }) {
      commit('UPDATE_LOST', {
        login: '',
        code: '',
        smsCode: '',
        step: 1,
      });
      commit('UPDATE_TIMER', {
        min: null,
        sec: null,
      });
    },
  },
  getters: {
    getSec(s) {
      return s.timer.sec;
    },
    getMin(s) {
      return s.timer.min;
    },
    step(s) {
      return s.lost.step;
    },
  },
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['lost', 'timer'],
    storage: window.sessionStorage,
  })],
});
