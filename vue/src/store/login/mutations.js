export default {
  UPDATE_LOST(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.lost[key] = payload[key];
    });
  },
  UPDATE_TIMER(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.timer[key] = payload[key];
    });
  },
};
