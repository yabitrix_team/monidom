export default {
  groups: [],
  users: [],
  usersPageCount: [],
  leads: [],
  leadsPageCount: [],
  documents: [],
  user_info: {
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    secondName: '',
    isAccreditated: '',
    role: '',
  }, // TODO переписать на user, а user имеющийся на user_create
  user: '',
  link: '',
  linkError: '',
  userError: '',
  loading: false,
  menuItems: [
    {
      id: 1,
      link: '/users',
      icon: '/static/i/users.svg',
      label: 'Пользователи',
    },
    {
      id: 2,
      link: '/auth',
      icon: '/static/i/auth.svg',
      label: 'Авторизация',
    },
    {
      id: 3,
      link: '/docs',
      icon: '/static/i/docs.svg',
      label: 'Документы',
    },
    {
      id: 4,
      link: '/orders/1',
      icon: '/static/i/leads.svg',
      label: 'Заявки',
    },
    {
      id: 5,
      link: '/order_info',
      icon: '/static/i/doc-que.svg',
      label: 'Статус заявки',
    },
  ],
};
