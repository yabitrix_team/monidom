import {
  CREATE_USER,
  GROUPS,
  USERS,
  CREATE_LINK,
  USER_INFO,
  LEADS,
  SMS_STATUS,
  CREATE_DOC,
  LOAD_DOCS,
  DELETE_FILE,
  SEARCH_REQUEST,
  SEND_REQUEST_EVENT,
  FILE_BY_REQUEST,
} from './action_types';
import to from '../../utils/To';
import Api from '../../api/panel';

const api = new Api();
export default {
  [USERS]({ commit }, params) {
    api.users(params).then(({ data }) => {
      commit(`_${USERS}`, data);
    });
  },
  [SEARCH_REQUEST](store, params) {
    return new Promise((resolve, reject) => {
      api.request(params).then((data) => {
        if (Array.isArray(data) && !data.length) {
          reject(null);
        }
        resolve(data);
      }).catch((e) => {
        reject(e);
      });
    });
  },
  [SEND_REQUEST_EVENT](store, req) {
    return new Promise((resolve, reject) => {
      api.resendRequestEvent(req).then((res) => {
        resolve(res);
      }).catch((e) => {
        reject(e);
      });
    });
  },
  [FILE_BY_REQUEST](store, params) {
    return new Promise((resolve, reject) => {
      api.requestFiles(params).then((data) => {
        const preparedFiles = {};
        Object.keys(data.types).forEach((typeKey) => {
          preparedFiles[typeKey] = [];
          data.types[typeKey].forEach((inner) => {
            if (data.files[to.camel(inner)]) {
              preparedFiles[typeKey] = [...preparedFiles[typeKey], ...data.files[to.camel(inner)]];
            }
          });
        });
        resolve({ data: preparedFiles, access: data.access });
      }).catch(e => reject(e));
    });
  },
  [DELETE_FILE](store, params) {
    return new Promise((resolve, reject) => {
      api.deleteFile(params).then(() => {
        resolve(true);
      }).catch(e => reject(e));
    });
  },
  [USER_INFO]({ commit }) {
    api.profile().then((data) => {
      commit(`_${USER_INFO}`, data);
    });
  },
  [CREATE_LINK]({ commit }, params) {
    commit(`_${CREATE_LINK}_ERROR`, '');
    api.link(params).then(({ data }) => {
      commit(`_${CREATE_LINK}`, data);
      commit(`_${CREATE_LINK}_ERROR`, '');
    }).catch((e) => {
      commit(`_${CREATE_LINK}`, '');
      commit(`_${CREATE_LINK}_ERROR`, e);
    });
  },
  [GROUPS]({ commit, state }) {
    if (!state.groups.length) {
      api.group().then(({ data }) => {
        commit(`_${GROUPS}`, data);
      });
    }
  },
  [CREATE_USER]({ commit }, obj) {
    commit('LOADING', true);
    commit(`_${CREATE_USER}_ERROR`, '');
    api.user(to.snake(obj)).then(({ data }) => {
      commit('LOADING', false);
      commit(`_${CREATE_USER}`, data);
    }).catch((e) => {
      commit('LOADING', false);
      if (Object.prototype.toString.call(e) === '[object Object]') {
        const errors = [];
        // eslint-disable-next-line
        for (const i in e) {
          errors.push(e[i]);
        }
        commit(`_${CREATE_USER}_ERROR`, errors.join(', '));
      } else {
        commit(`_${CREATE_USER}_ERROR`, e);
      }
    });
  },
  [LEADS]({ commit }, obj) {
    commit('LOADING', true);
    api.leads(to.snake(obj)).then(({ data }) => {
      commit('LOADING', false);
      commit(`_${LEADS}`, data);
    }).catch((e) => {
      commit('LOADING', false);
      commit(`_${LEADS}_ERROR`, e);
    });
  },
  [SMS_STATUS]({ commit }, obj) {
    if (obj) {
      commit(`_${SMS_STATUS}_LOAD`, { val: true, row: obj.row });
      api.smsStatus(obj.id).then(({ data }) => {
        commit('LOADING', false);
        commit(`_${SMS_STATUS}`, { status: data.status, row: obj.row });
      });
    }
  },
  // DOCUMENTS
  [CREATE_DOC](state, req) {
    api.doc(req).then(({ data }) => {
      if (data) {
        this.dispatch(LOAD_DOCS);
      }
    });
  },
  [LOAD_DOCS]({ commit }, req) {
    api.docs(req).then(({ data }) => {
      commit(`_${LOAD_DOCS}`, data);
    });
  },
};
