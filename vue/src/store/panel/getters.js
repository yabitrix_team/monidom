export default {
  groups: state => state.groups,
  user: state => state.user,
  user_info: state => state.user_info,
  documents: state => state.documents,
  users: state => state.users,
  link: state => state.link,
  linkError: state => state.linkError,
  usersPages: state => state.usersPageCount,
  userError: state => state.userError,
  leads: state => state.leads,
  leadsPageCount: state => state.leadsPageCount,
  loading: state => state.loading,
};
