import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import editUser from '../modules/editUser';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex);


export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  plugins: [
    createPersistedState({
      key: 'storage',
      paths: ['groups', 'user_info'],
      storage: window.sessionStorage,
    }),
  ],
  modules: {
    editUser,
  },
});
