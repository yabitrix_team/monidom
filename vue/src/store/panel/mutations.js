import { GROUPS, CREATE_USER, USERS, CREATE_LINK, USER_INFO, LEADS, SMS_STATUS, LOAD_DOCS } from './action_types';
import to from '../../utils/To';

export default {
  [`_${USERS}`](state, data) {
    state.users = to.camel(data.items);
    state.usersPageCount = data.count;
  },
  [`_${LEADS}`](state, data) {
    const cameled = to.camel(data.items);
    state.leads = cameled.map((e) => { e.smsStatus = ''; e.loading = false; return e; });
    state.leadsPageCount = data.count;
  },
  [`_${SMS_STATUS}_LOAD`](state, { val, row }) {
    (state.leads.find(el => el.id === row.id)).loading = val;
  },
  [`_${GROUPS}`](state, { data }) {
    state.groups = data;
  },
  [`_${CREATE_LINK}`](state, link) {
    state.link = link;
  },
  [`_${CREATE_LINK}_ERROR`](state, e) {
    state.linkError = e;
  },
  [`_${USER_INFO}`](state, data) {
    state.user_info = data;
  },
  [`_${CREATE_USER}`](state, data) {
    state.user = data;

    setTimeout(() => {
      state.user = '';
    }, 4500);
  },
  [`_${CREATE_USER}_ERROR`](state, error) {
    state.userError = error;
  },
  [`_${CREATE_USER}_ERROR`](state, error) {
    state.userError = error;
  },
  [`_${SMS_STATUS}`](state, data) {
    (state.leads.find(el => el.id === data.row.id)).smsStatus = data.status;
  },
  LOADING(state, bool) {
    state.loading = bool;
  },

  // DOCS
  [`_${LOAD_DOCS}`](state, data) {
    state.documents = data;
  },
};
