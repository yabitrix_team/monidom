export default {
  notificationsShow: state => state.notificationsShow,
  supportShow: state => state.support.show,
  notifyCount: state => state.notifyCount,
  graph: state => state.graph,
  calculator: state => state.calculator,
  isLoading: (state) => {
    if (state.request.fotoAuto.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    if (state.request.fotoCard.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    if (state.request.fotoClient.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    if (state.request.fotoPassport.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    if (state.request.fotoPts.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    if (state.request.fotoSts.filter(i => i.code === undefined).length > 0) {
      return true;
    }
    return false;
  },
};
