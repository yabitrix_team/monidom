import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import _ from 'lodash';
import Url from '../../utils/UrlHelper';
import Api from '../../api/admin';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
import to from '../../utils/To';

Vue.use(Vuex);

const api = new Api();
Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    hasFirstPack(context, data) {
      api.firstDocuments(data.data)
        .then((res) => {
          if (!res.stay) {
            context.commit('UPDATE_FIRST_DOCUMENTS', res);
          }
        });
    },
    hasSecondPack(context, data) {
      api.secondDocuments(data.data)
        .then((res) => {
          if (!res.stay) {
            context.commit('UPDATE_SECOND_DOCUMENTS', res);
          }
        });
    },
    insertNotify(context, data) {
      context.commit('UPDATE_NOTIFY', data);
    },
    verMessage(context, data) {
      context.commit('VERMESS_OPEN', data);
      console.log(data, 123);
    },
    // eslint-disable-next-line
    logout(context) {
      api.logout()
        .then(() => {
          const url = Url.getSubdomainUrl('login');
          window.location = `//${url}/`;
        })
        .catch(() => {
          console.error('Logout error');
        });
    },
    getRequest(context, code) {
      api.getRequest(code)
        .then((data) => {
          context.commit('UPDATE_REQUEST', to.camel(data));
        });
    },
    saveFile(context, { saveTo, file }) {
      const id = _.uniqueId('code_');
      const fileObj = Object.assign({}, file, { code: undefined, id });

      context.commit('FILES_ADD', { fileObj, saveTo });

      api.sendFile(file)
        .then((data) => {
          context.commit('FILES_SET_CODE', { code: data.code, saveTo, id });
        })
        .catch(() => {

        });
    },
    removeFile(context, { saveTo, key }) {
      const elem = context.state.request[to.camel(saveTo)][key];
      console.log(key, elem);
      api.deleteFile(elem.code)
        .then(() => {
          context.commit('FILES_DELETE', { key, saveTo });
        });
    },

  },

  plugins: [
    createPersistedState({
      key: 'storage',
      paths: ['request', 'user'],
      storage: window.sessionStorage,
    }),
  ],

});
