import moment from 'moment';

export default {
  socket: {
    isConnected: true,
    message: '',
    reconnectError: false,
  },
  user: {
    comission: '',
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    secondName: '',
    isAccreditated: null,
    pointName: '',
    partnerName: '',
  },
  notify: {},
  statuses: [],
  notificationsShow: false,
  notifyCount: 0,
  support: {
    show: false,
  },
  request: {
    num: '',
  },
  menuItems: [
    {
      id: 0,
      link: '/',
      icon: '/static/i/home.svg',
      label: 'Главная',
    },
    {
      id: 1,
      link: `/listclients/work/${moment().subtract(1, 'months').format('DD.MM.YYYY')}/${moment()
        .format('DD.MM.YYYY')}/1`,
      icon: '/static/i/clients.svg',
      label: 'Клиенты',
    },
    {
      id: 2,
      link: '/instructions',
      icon: '/static/i/instruction.svg',
      label: 'Инструкции',
    },
  ],
};
