export default {
  UPDATE_FIRST_DOCUMENTS(state, message) {
    const docs = [];
    message.docPack1.forEach((item) => {
      console.log(item);
      docs.push({
        url: item.url,
        name: item.name,
        description: item.description,
        code: item.code,
      });
    });
    state.request.docPack1 = docs;
  },
  UPDATE_SECOND_DOCUMENTS(state, message) {
    const docs = [];
    message.docPack2.forEach((item) => {
      console.log(item);
      docs.push({
        url: item.url,
        name: item.name,
        description: item.description,
        code: item.code,
      });
    });
    state.request.docPack2 = docs;
  },
};
