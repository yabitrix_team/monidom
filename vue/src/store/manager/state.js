export default {
  socket: {
    isConnected: true,
    message: '',
    reconnectError: false,
  },
  requests: {
    active: {},
    closed: {},
  },
  request: {
    canSignature: false,
    bar: {
      progress: '',
      color: '',
      percent: '',
      left: '',
    },
    code: '',
    step: '',
    num: '',
    summ: '',
    fotoPassport: [],
    fotoClient: [],
    fotoSts: [],
    fotoPts: [],
    fotoAuto: [],
    printLink: '',
    docPack1: '',
    docPack2: '',
  },
  vermess: {
    show: false,
    message: '',
    code: '',
    files: [],
  },
  graph: {},
  calculator: {
    dateReturn: '',
    summ: '',
    product: '',
  },
  menuItems: [
    {
      id: 0,
      link: '/',
      icon: '/static/i/home.svg',
      label: 'Главная',
    },
  ],
};
