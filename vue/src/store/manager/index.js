import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';

Vue.use(Vuex);

Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['request'],
    storage: window.sessionStorage,
  })],
});
