export default {
  graph(state) {
    return state.graph;
  },
  notificationsShow: state => state.notificationsShow,
  supportShow: state => state.support.show,
  notifyCount: state => state.notifyCount,
  leads: state => state.leads,
  leadsPages: state => state.leadsPages,
};
