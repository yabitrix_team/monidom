import { LEADS } from '../panel/action_types';
import to from '../../utils/To';

export default {
  [`_${LEADS}`](state, payload) {
    state.leads = to.camel(payload.items);
    state.leadsPages = payload.count;
  },
};
