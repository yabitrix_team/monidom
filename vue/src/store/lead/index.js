import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import state from './state';
import Api from '../../api/lead';
import getters from './getters';
import mutations from './mutations';
import globalMutations from '../mutations';
import { LEADS } from '../panel/action_types';

const api = new Api();

Vue.use(Vuex);

Object.assign(mutations, globalMutations);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions: {
    [LEADS]({ commit }, data) {
      api.getLeads(data).then(res => commit(`_${LEADS}`, res));
    },
    loadSingleDoc(scope, query) {
      return new Promise(resolve => api.docs(query).then(({ data }) => resolve(data)));
    },
  },
  plugins: [createPersistedState({
    key: 'storage',
    paths: ['request'],
    storage: window.sessionStorage,
  })],
});
