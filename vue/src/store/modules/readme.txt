Инструкция по созданию модулей:

1) Модуль это обычный js объект который потом расширит наше хранилище
2) В модуле есть файлы actions, getters, mutations, state все они ипортятся в index и экспосортируются в хранилище кабинета
3) В модуле обязательно указывать namespaced: true, так как не указывая данный параметр могут быть конфликты при одинаковых именах функций
4) Созданные модули подключаются к нужному кабинету в зависимости от потребности
5) В модулях не нужен файл констант так как, мы используем namespaced и не будет ситуации когда нам нужно будет поменять направление dispatch или чего-то такого
6) Импортируемый модуль в стор называется по названию папки где он находится чтобы не было путаницы
