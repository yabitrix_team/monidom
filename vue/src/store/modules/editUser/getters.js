export default {
  user(state) {
    return state.user ? state.user : {};
  },
  errorUpdate(state) {
    return state.errorMessage;
  },
  successUpdate(state) {
    return state.successMessage;
  },
};
