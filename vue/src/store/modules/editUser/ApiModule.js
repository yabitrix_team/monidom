import axios from 'axios';
import error from '../../../api/errorStore';
import globalApi from '../../../api/global';
import config from '../../../config';

export default class editUserApi extends globalApi {
  constructor() {
    super();
    const cabinetName = this.cabinetName;
    this.cabinet = axios.create({
      // TODO придумать как подключить кабинет в котором находится пользлватель
      baseURL: cabinetName || config.api.pathPanel,
    });
  }

  /*
  /  @data object принимает username изменяемого пользователя
  /  и newUsername на что будем менять
     {
      username: '123',
      newUsername: '312'
     }
  */
  updateUser(obj) {
    return new Promise((resolve, reject) => {
      this.cabinet.post('user/user/update', obj)
        .then(({ data }) => {
          if (data.error && data.error.message) reject(data.error.message || data.error);
          else resolve(data);
        })
        .catch((err) => {
          error.add(err);
          reject(err);
        });
    });
  }
}
