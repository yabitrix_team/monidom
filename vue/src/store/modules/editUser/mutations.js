export default {
  USER(state, payload) {
    state.user = payload;
  },
  UPDATE_USER(state, payload) {
    state.successMessage = 'Пользователь успешно обновлен';
    state.user.username = payload;
  },
  UPDATE_USER_ERROR(state, message) {
    state.errorMessage = message;
  },
  UPDATE_USER_MESSAGE(state, message) {
    state.successMessage = message;
  },
};
