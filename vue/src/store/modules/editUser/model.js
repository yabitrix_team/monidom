export default {
  user: {
    email: '',
    firstName: '',
    id: '',
    lastName: '',
    role: '',
    secondName: '',
    username: '',
  },
  successMessage: '',
  errorMessage: '',
};
