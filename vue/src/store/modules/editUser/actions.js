import Api from './ApiModule';
import To from '../../../utils/To';

const api = new Api();


export default {
  setUser({ commit }, user) {
    commit('USER', user);
  },
  updateUser({ commit }, user) {
    commit('UPDATE_USER_ERROR', '');
    commit('UPDATE_USER_MESSAGE', '');
    api.updateUser(To.snake(user)).then(() => {
      commit('UPDATE_USER', user.newUsername);
    }).catch((e) => {
      commit('UPDATE_USER_ERROR', e);
    });
  },
};
