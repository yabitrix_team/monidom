var fs = require('fs-extra');
var rimraf = require('rimraf');
const cabinet = require('../config/cabinet');

var source = './dist/temp/static/';
var dest = `./dist/static/${cabinet.name.toLowerCase()}/`;


rimraf(dest, function () {
  console.log('Сleared ' + dest);
  fs.copy(source, dest, function (err) {
    if (err)
    {
        return console.error(err);
    }
    console.log('Copied to ' + dest);
  });
});
