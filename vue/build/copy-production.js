var fs = require('fs-extra');
var rimraf = require('rimraf');
var source = './dist/static/';
var dest = '../../static';

rimraf(dest, function () {
  console.log('cleared');
  fs.copy(source, dest, function (err) {
    if (err)
    {
        return console.error(err);
    }
    console.log('Copied to ' + dest);
  });
});


