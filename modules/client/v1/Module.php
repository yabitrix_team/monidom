<?php

namespace app\modules\client\v1;
/**
 * Class Module - модуль для работы с ЛКК
 *
 * @package app\modules\client\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\client\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
