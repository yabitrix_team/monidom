<?php
namespace app\modules\client\v1\classes\exceptions;

/**
 * Исключение по заявке
 *
 * @package app\modules\call\v1\classes\exceptions
 */
class RequestException extends \Exception implements \Throwable
{
}