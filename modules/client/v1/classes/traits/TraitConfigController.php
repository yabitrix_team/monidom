<?php

namespace app\modules\client\v1\classes\traits;

use app\modules\app\v1\classes\traits\TraitConfigController as TraitConfigControllerApp;
use app\modules\client\v1\models\Requests;

/**
 * Trait TraitSomeController - трейт содержащий методы-настройки для контроллеров,
 * в данном трейте необходимо подлкючить трейт AbstractTraitSomeController
 * для приведения к однотипности трейтов-настройки
 *
 * @package app\modules\app\v1\classes\traits
 */
trait TraitConfigController {
	/**
	 * Подключаем трейт с абстрактными методами, которые нужно реализовать,
	 * приводим текущий трейт к однотипности трейтов-настройки
	 */
	use TraitConfigControllerApp;

	/**
	 * Метод возвращающий namespace для модели Requests
	 * при необходимости переопределяем данный метод и указываем
	 * необходимый namespace в новом подключаемом trait
	 *
	 * @return string|Requests - вернет строку содержащую namespace модели Requests
	 */
	protected function getModelRequest() {

		return Requests::class;
	}
}