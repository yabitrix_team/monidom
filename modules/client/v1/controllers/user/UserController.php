<?php

namespace app\modules\client\v1\controllers\user;

use app\modules\app\v1\controllers\user\UserController as UserControllerApp;

class UserController extends UserControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'update',
							'verify',
						],
						'roles'   => [ 'role_client', 'role_mobile_application'  ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
