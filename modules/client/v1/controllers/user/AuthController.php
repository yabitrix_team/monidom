<?php
namespace app\modules\client\v1\controllers\user;

use app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use yii\filters\AccessControl;

class AuthController extends AuthControllerApp
{
    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['create'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['index', 'delete'],
                        'roles'   => ['role_client', 'role_mobile_application'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
