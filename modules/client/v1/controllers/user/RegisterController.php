<?php
namespace app\modules\client\v1\controllers\user;

use app\modules\app\v1\controllers\user\RegisterController as RegisterControllerApp;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\LeadsConsumer;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use Yii;
use yii\web\JsonParser;

class RegisterController extends RegisterControllerApp
{
    /**
     * Экшен создания пользователя в рамках ЛКК
     *
     * @return array
     * @throws \Exception
     */
    public function actionCreate()
    {
        $result = parent::actionCreate();
        if (!empty($result['data'])) {
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            //если при регистрации у юзера есть лид, то отправляем события в CRM
            $lead = (new Leads())->getByPhone($reqBody["login"], ['guid'], LeadsConsumer::CLIENT);
            if (!empty($lead['guid'])) {
                //отправка события в CRM "Регистрация в ЛКК"
                EventsLoad::sendEventLoadBit(110, $lead['guid'], $reqBody["login"]);
                //отправка события в CRM "Подпись ПЭП ЛКК"
                EventsLoad::sendEventLoadBit(210, $lead['guid'], $reqBody["login"]);
            }
        }

        return $result;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                            'new',
                        ],
                        'roles'   => ['?', '@'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
