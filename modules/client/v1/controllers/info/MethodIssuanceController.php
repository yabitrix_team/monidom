<?php
namespace app\modules\client\v1\controllers\info;

use app\modules\app\v1\controllers\info\MethodIssuanceController as MethodIssuanceControllerApp;
use Yii;

class MethodIssuanceController extends MethodIssuanceControllerApp
{
    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     */
    public function actionIndex()
    {

        $data = Yii::$app->db
            ->createCommand(
                "SELECT  
  `m`.`id` as `id`,
  `m`.`name` as `name`,
  `m`.`guid` as `guid`,
  `m`.`description` as `description`
FROM `methods_issuance` AS `m`
  LEFT JOIN `cabinets_methods_issuance` AS `cm` ON `cm`.`method_id` = `m`.`id`
  LEFT JOIN `cabinets` AS `c` ON `c`.`id` = `cm`.`cabinet_id`
WHERE `c`.`id` = 2"
            )
            ->queryAll();

        return $this->getClassAppResponse()::get($data);
    }

    public function behaviors()
    {

        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                        ],
                        'roles'   => ['role_client', 'role_mobile_application'],
                    ],
                ],
                'denyCallback' => function () {

                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
