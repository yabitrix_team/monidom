<?php

namespace app\modules\client\v1\controllers\agreements;

use app\modules\app\v1\controllers\agreements\ClosedController as ClosedControllerApp;
use app\modules\client\v1\classes\traits\TraitConfigController;

/**
 * Class ClosedController - контроллер для работы с закрытыми договорами клиента
 *
 * @package app\modules\app\v1\controllers
 */
class ClosedController extends ClosedControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен для получения активных договоров авторизованного пользователя
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
	public function actionIndex() {

		$result = parent::actionIndex();
		if ( empty( $result['data'] ) && empty( $result['error'] ) ) {
			$result['data']['noClosedContracts'] = 1;
		}

		return $result;
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::class,
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_client', 'role_mobile_application' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}