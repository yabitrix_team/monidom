<?php

namespace app\modules\client\v1\controllers\lead;

use app\modules\app\v1\controllers\lead\LeadController as LeadControllerApp;

class LeadController extends LeadControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'by-code',
						],
						'roles'   => [ '?', '@' ],
					],
					[
						'allow'   => true,
						'actions' => [
							'index',
						],
						'roles'   => [ 'role_client', 'role_mobile_application'  ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
