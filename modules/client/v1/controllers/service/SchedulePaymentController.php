<?php

namespace app\modules\client\v1\controllers\service;

use app\modules\app\v1\controllers\service\SchedulePaymentController as SchedulePaymentControllerApp;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class SchedulePaymentController extends SchedulePaymentControllerApp {
	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::class,
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
						],
						'roles'   => [ 'role_client', 'role_mobile_application' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
