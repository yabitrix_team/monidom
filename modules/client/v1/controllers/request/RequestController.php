<?php
namespace app\modules\client\v1\controllers\request;

use app\modules\app\v1\controllers\request\RequestController as RequestControllerApp;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use app\modules\app\v1\models\RequestsMetrika;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\client\v1\classes\exceptions\RequestException;
use app\modules\client\v1\classes\traits\TraitConfigController;
use Yii;
use yii\web\JsonParser;

/**
 * Class RequestController - контроллер для работы с заявкой в ЛКК
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends RequestControllerApp
{
    use TraitConfigController;

    /**
     * Экшен для создания заявки
     *
     * @return array|bool|string
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            //из трейта подтягиваем namespace модели Request
            $modelRequest = $this->getModelRequest();
            $model        = new $modelRequest(['scenario' => $modelRequest::SCENARIO_CREATE]);
            $reqBody      = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $data         = [];
            if ($reqBody['ya_client_id']) {
                $ya_client_id = $reqBody['ya_client_id'];
                unset($reqBody['ya_client_id']);
            };
            // ищем лид, чтобы взять guid заявки
            $lead = (new Leads())->getAllByFilter(
                ['client_mobile_phone' => Yii::$app->user->identity->username],
                ['created_at' => 'desc']
            )[0];
            if (!$model->load($reqBody, '')) {
                throw new RequestException(
                    'Не переданы данные для '.(empty($lead)
                        ? ' создания заявки' : ' обновления заявки по лиду')
                );
            }
            if (!empty($lead) && ($request = (new $modelRequest())->getByGuid($lead['guid']))) {
                $reqBody['client_id'] = Yii::$app->user->id;
                $reqBody['user_id']   = Yii::$app->params['client']['settings']['user_id'];
                $reqBody['is_pep']    = 1;
                $result               = $model->updateByCode($request['code'], $reqBody);
                if (is_a($result, \Exception::class)) {
                    throw $result;
                }
                $data = [
                    'code'    => $request['code'],
                    'updated' => 1,
                ];
            }
            if ($model->getClientRequestsCountInProcess() > 4) {
                throw new RequestException(
                    'Уважаемый клиент, в работе находятся 5 заявок, создание новых заявок заблокировано.'
                );
            }
            if (empty($data)) {
                $code = $model->create();
                if (is_a($code, \Exception::class)) {
                    throw new RequestException($code->getMessage(), $code->getCode(), $code);
                }
                if (is_a($code, $modelRequest)) {
                    return $this->getClassAppResponse()::get(false, $code->firstErrors);
                }
                $data      = [
                    'code'    => $code,
                    'created' => 1,
                ];
                $requestId = Yii::$app->db->lastInsertID;
                //получение lcrm_id с последующим добавлением в заявку
                $lcrmId = Yii::$app->lcrmLKK->leadsRegister(
                    Yii::$app->user->identity->username,
                    $requestId,
                    Yii::$app->user->id,
                    $reqBody['client_first_name'].' '.$reqBody['client_last_name'], //согласовано c Перовым
                    Yii::$app->user->identity->crib_client_id,
                    Yii::$app->user->identity->created_at
                );
                if (!empty($lcrmId)) {
                    $model->updateOne($requestId, ['lcrm_id' => $lcrmId]);
                }
                //добавление статуса МП "Создана" к заявке
                $resSetStatus = (new StatusesMPJournal)->setStatus($requestId, StatusesMP::getStatusCreated());
                if (is_a($resSetStatus, \Exception::class)) {
                    return $resSetStatus;
                }
                // добавление Ya.Metrika сессии, если она передана
                if (isset($ya_client_id)) {
                    $addData           = [
                        'request_id'   => $requestId,
                        'metrika_id'   => $ya_client_id,
                        'user_id'      => Yii::$app->user->identity->id / 1,
                        'metrika_type' => 'ya',
                    ];
                    $objRequestMetrika = (new RequestsMetrika($addData))->addData();
                    if (is_a($objRequestMetrika, RequestsMetrika::class)) {
                        return $this->getClassAppResponse()::get(false, $objRequestMetrika->firstErrors);
                    }
                    if (is_a($objRequestMetrika, \Exception::class)) {
                        return $this->getClassAppResponse()::get(false, false, $objRequestMetrika->getMessage());
                    }
                }
                // /запись id метрики к заявке
            }
            (new Leads)->deleteByPhone(Yii::$app->user->identity->username);

            return $this->getClassAppResponse()::get($data);
        } catch (RequestException $e) {
            if (isset($requestId) && isset($model)) {
                $model->deleteOne($requestId);
            }

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (\Exception $e) {
            if (isset($requestId) && isset($model)) {
                $model->deleteOne($requestId);
            }
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Экшен получения всех заявок
     *
     * @return array
     */
    public function actionIndex()
    {
        try {
            $result = parent::actionIndex();
            if (!empty($result['data'])) {
                if ($result['data']['auto_brand_id']) {
                    $brandArray = [
                        'id'   => $result['data']['auto_brand_id'],
                        'name' => $result['data']['auto_brand_name'],
                    ];
                    unset($result['data']['auto_brand_id'], $result['data']['auto_brand_name']);
                    $result['data']['auto_brand_id'] = $brandArray;
                }
                if ($result['data']['auto_model_id']) {
                    $modelArray = [
                        'id'   => $result['data']['auto_model_id'],
                        'name' => $result['data']['auto_model_name'],
                    ];
                    unset($result['data']['auto_model_id'], $result['data']['auto_model_name']);
                    $result['data']['auto_model_id'] = $modelArray;
                }
                //Добавляем пустые значения для файлов которых нет в БД
                $arFileBindEmpty = array_map(
                    function ($v) {
                        return [];
                    },
                    array_flip($this->arFileBind)
                );
                $items           = [];
                foreach ($result['data'] as $item) {
                    //если событий по заявке нет, то заявку не отображаем(учет заявок МП)
                    if ((new RequestsEvents())->getByRequestId($item['id'], ['id'])) {
                        $item           = array_map(
                            function ($v) {
                                return (is_null($v) || $v == '0.00' || $v == '0') ? '' : $v;
                            },
                            $item
                        );
                        $arDiffFileBind = array_diff_key($arFileBindEmpty, $item);
                        $items[]        = array_merge($item, $arDiffFileBind);
                    }
                }
                $result['data'] = $items;
            }

            return $result;
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                'При получении списка заявок произошла ошибка, обратитесь к администратору'
            );
        }
    }

    /**
     * Экшен получения заявки по коду
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        $result = parent::actionByCode($code);
        try {
            if (!empty($result['data'])) {
                if ($result['data']['auto_brand_id']) {
                    $brandArray = [
                        'id'   => $result['data']['auto_brand_id'],
                        'name' => $result['data']['auto_brand_name'],
                    ];
                    unset($result['data']['auto_brand_id'], $result['data']['auto_brand_name']);
                    $result['data']['auto_brand_id'] = $brandArray;
                }
                if ($result['data']['auto_model_id']) {
                    $modelArray = [
                        'id'   => $result['data']['auto_model_id'],
                        'name' => $result['data']['auto_model_name'],
                    ];
                    unset($result['data']['auto_model_id'], $result['data']['auto_model_name']);
                    $result['data']['auto_model_id'] = $modelArray;
                }
                $result['data'] = array_map(
                    function ($v) {
                        return (is_null($v) || $v == '0.00' || $v == '0') ? '' : $v;
                    },
                    $result['data']
                );
                //Добавляем пустые значения для файлов которых нет в БД
                $arFileBindFlip  = array_flip($this->arFileBind);
                $arFileBindEmpty = array_map(
                    function ($v) {
                        return [];
                    },
                    $arFileBindFlip
                );
                $arDiffFileBind  = array_diff_key($arFileBindEmpty, $result['data']);
                $result['data']  = array_merge($result['data'], $arDiffFileBind);
                //Остальные параметры
                $events                            =
                    array_column((new Events())->getByRequestId($result['data']['id']), 'code');
                $requestStatus                     =
                    (new StatusesJournal())->getCurrentByRequestID($result['data']['id']);
                $currentStatus                     = (new Statuses())->getOne($requestStatus['status_id']);
                $result['data']['step']            = $this->getStep($events, $currentStatus, $result['data']);
                $result['data']['bar']['progress'] = $currentStatus['progress'];
                $result['data']['bar']['color']    = $currentStatus['color'];
                $result['data']['bar']['percent']  =
                    round($currentStatus['progress'] * 100 / 100); // последний 100 ставить другой если надо пересчитать
                $result['data']['bar']['left']     = '15 минут';
                $result['data']['preApproved']     = 0;
                if ($currentStatus['guid'] == 'cbb42353-51e6-4a88-b2ef-575729da0343') { // предварительно одобрено
                    $result['data']['preApproved'] = 1;
                };
                unset($result['data']['id']);
            }

            return $result;
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                'При получении заявки по коду произошла ошибка, обратитесь к администратору'
            );
        }
    }

    /**
     * Экшен просчета шага заявки
     *
     * @param array $events - Массив с эвентами по заявке
     *
     * @return int - Вернет цифру статуса
     */
    private function getStep($events, $currentStatus, $request)
    {
        if (in_array(701, $events) && !in_array(702, $events)) { // заявка создана, но больше ничего не сделано
            $result = 2;
        } elseif (in_array(702, $events) && !in_array(711, $events)) {  // отправлена полная
            $result = 3; // отправлены доки
        } elseif (in_array(711, $events)) { //
            $result = 4; // ожидание решения
        }
        if (
        in_array(
            $currentStatus['guid'],
            [
                '623bbcc6-ac4c-4e51-bdcf-f7c5a12b9abd',
                '1b61eb3a-c293-4fca-8ee6-f672c43302ef',
            ]
        )
        ) {
            // todo[burovyuri]: это статус на финальном, сделать чтобы все что после него, тоже реагировало
            $result = 5;
        } elseif (
        in_array(
            $currentStatus['guid'],
            [
                'abfa1724-c3fd-4b7f-9631-7572288a165b', // Одобрено
                '74ed5b21-af9b-44f3-bda5-85fde6847b76', // Ожидает подписания договора
                'ff126df5-e381-4312-b4f3-8863314038b2', // Клиент получает деньги
                '6818753f-7d4c-45e6-ad29-5f98db749508', // Деньги выданы
            ]
        )
        ) {
            $result = 6;
        } elseif ($currentStatus['guid'] == 'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e') { // статус "отказано"
            $result = 10;
        } elseif ($currentStatus['guid'] == 'c37fec4e-28b3-4239-8c2f-6812821b1b32') { // статус "отозвано"
            $result = 11;
        };
        if (
            $request['client_id'] && !in_array(
                $request['point_id'],
                [
                    Yii::$app->params['client']['settings']['point_id'],
                    Yii::$app->params['mp']['settings']['point_id'],
                ]
            )
        ) {
            $result = 12;
        }

        return $result;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                            'by-code',
                            'notice',
                            'create',
                            'update',
                        ],
                        'roles'   => ['role_client', 'role_mobile_application'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
