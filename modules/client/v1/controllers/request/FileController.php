<?php

namespace app\modules\client\v1\controllers\request;

use app\modules\app\v1\controllers\request\FileController as FileControllerApp;
use app\modules\client\v1\models\Requests;
use Yii;

/**
 * Class FileController - контроллер для работы с файлами
 *
 * @package app\modules\app\v1\controllers
 */
class FileController extends FileControllerApp {
	/**
	 * Экшен добавления файлов
	 *
	 * @return array - вернет массив содержащий код заявки или ошибку
	 */
	public function actionCreate() {

		$result = parent::actionCreate();
		if ( ! empty( $result['data'] ) ) {
			//обновление точки и подписанта при добавлении файлов
			$code = Yii::$app->request->post( "code" );
			( new Requests() )->updateByCode( $code );
		}

		return $result;
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::class,
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'by-code',
							'preview-by-code',
							'doc-by-code',
							'zip-by-code',
							'accreditation-by-code',
							'delete',
							'create',
							'extra',
						],
						'roles'   => [ 'role_client', 'role_mobile_application' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
