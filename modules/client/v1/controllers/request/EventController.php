<?php
namespace app\modules\client\v1\controllers\request;

use app\modules\app\v1\controllers\request\EventController as EventControllerApp;
use app\modules\app\v1\models\RequestMP;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\client\v1\models\Requests;
use Yii;
use yii\web\JsonParser;

/**
 * Class EventController - контроллер для работы событиями заявки
 *
 * @package app\modules\client\v1\controllers\request
 */
class EventController extends EventControllerApp
{
    /**
     * Экшен создания события прикрепленного к заявке
     *
     * @return array - Вернет сформированный массив
     */
    public function actionCreate()
    {
        try {
            $reqBody            = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $codeEventSendFiles = 711; //код события Отправка сканов документов
            $codeEventSendFoto  = 712; //код события Отправлены файлы фотографии авто
            //на последнем шаге проверка события на отправку заявки на финальную верификацию из МП
            if (
                $reqBody['event'] == $codeEventSendFiles
                && (new RequestsEvents())->issetEventForRequest($codeEventSendFoto, $reqBody['code'])
            ) {
                return $this->getClassAppResponse()::get(['success' => 1]);
            }
            $result = parent::actionCreate();
            if (!empty($result['data'])) {
                if ($reqBody['event'] == $codeEventSendFiles && !empty($this->request)) {
                    //обновляем точку и подписанта на последнем шаге
                    (new Requests())->updateByCode($reqBody['code']);
                    //на последнем шаге отправки заявки, указываем блокировку заявки для МП
                    $model = new RequestMP();
                    $model->load(
                        [
                            'request_id' => $this->request['id'],
                            'blocked_by' => RequestMP::BLOCKED_BY_CLIENT,
                        ],
                        ''
                    );
                    $model->save();
                }
            }

            return $result;
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                        ],
                        'roles'   => ['role_client', 'role_mobile_application'],
                    ],
                ],
                'denyCallback' => function () {

                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
