<?php
namespace app\modules\client\v1\models;

use app\modules\app\v1\classes\logs\Log;
use DateTime;
use Yii;
use yii\base\Model;

/**
 * Попытки авторизации клиента
 *
 * @package app\modules\client\v1\models
 */
class AuthAttempt extends Model
{
    /**
     * Константа сценария создания
     */
    const SCENARIO_CREATE = 'create';
    /**
     * Константа сценария обновления
     */
    const SCENARIO_UPDATE = 'update';
    /**
     * @var int ID
     */
    public $id;
    /**
     * @var string ID пользователя
     */
    public $user_id;
    /**
     * @var int кол-во неуспешных попыток авторизации
     */
    public $failed;
    /**
     * @var string дата первой попытки авторизации
     */
    public $created_at;
    /**
     * @var string дата последней попытки авторизации
     */
    public $updated_at;
    /**
     * @var array поля сценария создания
     */
    protected $scenarioCreateFields = [
        'user_id',
        'failed',
    ];
    /**
     * @var array поля сценария обновления
     */
    protected $scenarioUpdateFields = [
        'failed',
        '!updated_at',
    ];

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                        = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = $this->scenarioCreateFields;
        $scenarios[self::SCENARIO_UPDATE] = $this->scenarioUpdateFields;

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'failed'], 'required', 'message' => 'Не все обязательные поля заполнены'],
        ];
    }

    /**
     * Получение инфы попыток авторизации по пользователю
     *
     * @param $user_id int ID пользователя
     *
     * @return array|false
     * @throws \yii\db\Exception
     */
    public function getByUserId($user_id)
    {
        $info = Yii::$app->db->createCommand(
            "select ".implode(", ", $this->getFieldsNames())."
            from ".$this->getTableName()."
            where [[user_id]]=:user_id",
            [":user_id" => $user_id]
        )->queryOne();
        if (is_array($info) && strlen($info["updated_at"])) {
            $timeNow = (new DateTime())->getTimestamp();
            $timeLast = (new DateTime($info["updated_at"]))->getTimestamp();
            $info["rest"] = 15 * 60 - ($timeNow - $timeLast);
            $info["rest"] = $info["rest"] >= 0 ? $info["rest"] : 0;
//            $minutes = intval($info['rest'] / 60);
//            $seconds = ($info["rest"] - intval($info['rest'] / 60) * 60);
//            $seconds = strlen($seconds) == 1 ? "0".$seconds : $seconds;
//            $info["rest_formatted"] = $minutes.":".$seconds;
        }
        return $info;
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function create()
    {
        if (Yii::$app->db->createCommand()->insert($this->getTableName(), array_filter($this->attributes))->execute()) {
            return Yii::$app->db->getLastInsertID();
        }
        throw new \Exception("Ошибка создания попытки авторизации");
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function update($id = 0)
    {
        if (
        Yii::$app->db->createCommand()->update(
            $this->getTableName(),
            array_filter($this->attributes, function($val) {
                return !is_null($val);
            }),
            "id=".intval($id)
        )->execute()
        ) {
            return true;
        }
        throw new \Exception("Ошибка обновления попытки авторизации");
    }

    /**
     * @param $user
     */
    public function logValidateError($user)
    {
        Log::error(
            [
                "text"       => "Ошибки валидации: ".
                                print_r($this->firstErrors, 1).
                                "\nАттрибуты модели: ".
                                print_r($this->attributes, 1),
                "user_id"    => $user["id"],
                "user_login" => $user["username"],
            ],
            "user.login.attempt"
        );
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{auth_attempts}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'user_id',
            'failed',
            'created_at',
            'updated_at',
        ];
    }
}