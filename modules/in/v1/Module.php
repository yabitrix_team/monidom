<?php

namespace app\modules\in\v1;
/**
 * app_v1 module definition class
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\in\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
