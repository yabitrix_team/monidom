<?php
namespace app\modules\in\v1\controllers\lead;

use app\modules\app\v1\controllers\lead\LeadController as LeadControllerApp;

/**
 * Class LeadController - контроллер для работы с лидами
 *
 * @package app\modules\in\v1\controllers\lead
 */
class LeadController extends LeadControllerApp
{
}
