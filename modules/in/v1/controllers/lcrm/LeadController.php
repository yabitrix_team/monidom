<?php
namespace app\modules\in\v1\controllers\lcrm;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\LcrmLead;
use Yii;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\JsonParser;
use yii\web\Response;

class LeadController extends Controller
{
    use TraitConfigController;

    private function checkAuth($headers = [])
    {
        if (!$headers["content-hmac"]) {
            return false;
        }
        $hmac = hash_hmac('sha256', Yii::$app->request->getRawBody(), yii::$app->params['lcrm']['secret']);

        return $headers["content-hmac"] == base64_encode($hmac);
    }

    public function actionCreate()
    {
        try {
            if (!(defined('YII_ENV') && YII_ENV == 'dev') && !$this->checkAuth(Yii::$app->request->getHeaders())) {
                throw new \yii\web\HttpException(401, 'Ошибка авторизации со стороны LCRM');
            }
            $post             = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $lead             = new LcrmLead();
            $lead->scenario   = LcrmLead::SCENARIO_CREATE;
            $lead->attributes = $post;
            $lead->code       = md5($lead->lcrm_id);
            if (!$lead->validate()) {
                Log::error(
                    ["text" => "Ошибки валидации: ".print_r($lead->firstErrors, 1), "lcrm_id" => $post["lcrm_id"]],
                    "in.lcrm"
                );

                return $this->getClassAppResponse()::get(false, $lead->firstErrors);
            }
            $idLead = $lead->create();
            Log::info(
                ["text"    => "Лид c id=".$idLead." создан, поля: ".print_r($lead->attributes, 1),
                 "lcrm_id" => $post["lcrm_id"],
                ],
                "in.lcrm"
            );

            return $this->getClassAppResponse()::get(['id' => $idLead]);
        } catch (\Exception $e) {
            Log::error(
                ["text" => $e->getMessage(), "lcrm_id" => $post["lcrm_id"]],
                "in.lcrm"
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}