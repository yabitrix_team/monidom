<?php
namespace app\modules\in\v1\controllers\partner_lead;

use app\modules\app\v1\classes\logs\Log;
use app\modules\in\v1\classes\exceptions\PartnerLeadException;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\lead\v1\models\PartnerLead;
use app\modules\lead\v1\models\LeadStatuses;
use Yii;
use yii\web\JsonParser;
use yii\rest\Controller;

class UpdateController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @param $guid
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate($guid)
    {
        try {
            $arPost = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (empty($arPost['status_guid']) && empty($arPost['summ'])) {
                throw new PartnerLeadException('Не указан статус заявки или сумма займа');
            } else {
                $model           = new PartnerLead();
                $fieldsCanUpdate = ['status_guid', 'summ'];
                $fieldsForUpdate = [];
                foreach ($fieldsCanUpdate as $field) {
                    if (array_key_exists($field, $arPost) && $arPost[$field] !== '') {
                        $fieldsForUpdate[$field] = $arPost[$field];
                    }
                }
                if (!empty($fieldsForUpdate['status_guid'])) {
                    if (!LeadStatuses::instance()->getByGuid($fieldsForUpdate['status_guid'])) {
                        throw new PartnerLeadException('Статуса с таким guid в базе не найдено');
                    }
                }
                if (
                    is_numeric($fieldsForUpdate['summ']) &&
                    ($fieldsForUpdate['summ'] < 75000 | $fieldsForUpdate['summ'] > 1000000)
                ) {
                    throw new PartnerLeadException('Сумма должна быть от 75000 до 1000000');
                }
                $updated = $model->updateByFields(['guid' => $guid], $fieldsForUpdate);
                if ($updated === 1) {
                    Log::info(["text" => "Лид $guid обновлен, тело запроса:\n".print_r($arPost, true)], "in.partner_lead");

                    return $this->getClassAppResponse()::get(['success' => true]);
                } else {
                    throw new PartnerLeadException('Заявки с таким guid не найдено');
                }
            }
        } catch (PartnerLeadException $e) {
            Log::error(["text" => $e->getMessage()], "in.partner_lead");

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }
}