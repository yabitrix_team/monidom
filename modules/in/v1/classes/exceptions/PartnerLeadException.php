<?php
namespace app\modules\in\v1\classes\exceptions;

/**
 * Исключение по лиду
 *
 * @package app\modules\in\v1\classes\exceptions
 */
class PartnerLeadException extends \Exception implements \Throwable
{

}