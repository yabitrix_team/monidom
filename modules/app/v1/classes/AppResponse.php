<?php

namespace app\modules\app\v1\classes;
/**
 * Class AppResponse - класс формирования ответов веб-сервисами
 *
 * @package app\modules\app\v1\classes
 */
class AppResponse {
	/**
	 * @var bool - параметр включения логирования вывода ответа веб-сервисов
	 */
	protected static $log = false;

	/**
	 * Метод формирования ответа веб-сервисами
	 *
	 * @param bool $data      - Данные для передачи
	 * @param bool $validates - Ошибки валидации полей, пример:
	 *                        ['summ'=>'Поле указанно некорректно']
	 * @param bool $msg       - Сообщения исключений (накопительный массив), пример:
	 *                        ['Не переданы параметры', 'Параметры подключения указаны некорректно']
	 * @param bool $code      - Коды (накопительный массив), пример:
	 *                        [1345, 2065]
	 * @param bool $version   - Версия справочника, по необходимости
	 *
	 * @return array - Вернет сформированный массив, пример
	 *               [
	 *                  error=>[
	 *                      validate => [
	 *                          field_name => error message
	 *                      ]
	 *                      messages => [],
	 *                      code => []
	 *                  ],
	 *                  version =>0,
	 *                  data=>[]
	 *              ]
	 *
	 */
	public static function get( $data = false, $validates = false, $msg = false, $code = false, $version = false ) {

		$result          = [];
		$result['error'] = [];
		empty( $validates ) ?: $result['error']['validate'] = $validates;
		empty( $msg ) ?: $result['error']['message'] = $msg;
		empty( $code ) ?: $result['error']['code'] = $code;
		empty( $version ) ?: $result['version'] = $version;
		$result['data'] = ! empty( $data ) ? $data : [];

		return $result;
	}
}