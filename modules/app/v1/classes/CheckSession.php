<?php
//todo[hdcy]: 20.04.2018 Правильно настроить калбэк для вызова из поведения
namespace app\modules\app\v1\classes;

use app\modules\app\v1\models\SessionVerify;
use Yii;

/*
 *  Класс проверки сессии на повторную попытку проведения операции
 */

class CheckSession {
	/*
	 * функция провеки сессии
	 *
	 * @return true - успех|array - ошибки валидации|Строка исключения
	 */
	public static function check() {

		try {
			//-------------------------------------------------------------------------------------------------------------------
			//-------------------Защита от частых попыток авторизации по PHPSESSID
			//-------------------------------------------------------------------------------------------------------------------
			if (
				! ( Yii::$app->params['web']['session']['attempt'] )
				|| ! is_numeric( Yii::$app->params['web']['session']['attempt'] )
				|| ! ( Yii::$app->params['web']['session']['lock_expired_at'] )
				|| ! is_numeric( Yii::$app->params['web']['session']['lock_expired_at'] )
				|| ! ( Yii::$app->params['web']['session']['ttl'] )
				|| ! is_numeric( Yii::$app->params['web']['session']['ttl'] )
			) {
				throw new \Exception( 'Ошибка на стороне сервера, не указаны или указаны некорректно параметры для проверки сессиий клиента: [web]-[session]' );
			}
			if ( ! Yii::$app->session->isActive ) {
				Yii::$app->session->open();
			}
			//проверяем если у клиента уже есть сессия sessions_verify
			$sessionVerifyCurrent = ( new SessionVerify() )->getOneByFilter( [ 'session_id' => Yii::$app->session->getId() ] );
			//Берем текущее время из БД
			$curTime = new \yii\db\Expression( 'NOW()' );
			$curTime = ( new \yii\db\Query )->select( $curTime )->scalar();
			if ( empty( $sessionVerifyCurrent ) //Если информации о ограничениях сессии нет (нет записей в БД)
			     ||
			     //Время в БД больше текущего времени (то есть время блокировки истекло), то сбрасываем
			     ( new \DateTime( $sessionVerifyCurrent['lock_expired_at'] ) ) < ( new \DateTime( $curTime ) ) ) {

				//создаем или сбрасываем ограничения на сессию
				$sessionVerify = ( new SessionVerify( [ 'scenario' => SessionVerify::SCENARIO_CREATE ] ) );
				if ( ! $sessionVerify->load( [ 'session_id' => Yii::$app->session->getId() ], '' ) ) {
					throw new \Exception( 'Не удалось определить и загрузить PHPSESSID. Повторите запрос или обратитесь к администратору.' );
				}
				if ( ! $sessionVerify->validate() ) {
					return $sessionVerify->errors;
				}
				$res = $sessionVerify->save();
				if ( $res !== 1 && $res != 2 ) {
					return $res;
				}
			} else {

				//Если пользователь слишком часто направляет запросы, то выводим ему ошибку
				if ( empty( $sessionVerifyCurrent['updated_at'] ) ) {
					if ( date_add( new \DateTime( $sessionVerifyCurrent['created_at'] ), new \DateInterval( 'PT' . Yii::$app->params['web']['session']['ttl'] . 'S' ) ) > new \DateTime( $curTime ) ) {
						return 'Превышена частота запросов, разрешено не чаще чем раз в ' . Yii::$app->params['web']['session']['ttl'] . ' секунд';
					}
				} elseif ( date_add( new \DateTime( $sessionVerifyCurrent['updated_at'] ), new \DateInterval( 'PT' . Yii::$app->params['web']['session']['ttl'] . 'S' ) ) > new \DateTime( $curTime ) ) {
					return 'Превышена частота запросов, разрешено не чаще чем раз в ' . Yii::$app->params['web']['session']['ttl'] . ' секунд';
				}
				//Если количество попыток превышено, направляем отказ пользователю
				if ( $sessionVerifyCurrent['attempt'] + 1 >= Yii::$app->params['web']['session']['attempt'] ) { //+1 так как нумерация с 0
					return 'Доступ заблокирован до ' . $sessionVerifyCurrent['lock_expired_at'] . ' в связи с превышением ' . Yii::$app->params['web']['session']['attempt'] . ' попыток ввода';
				}
				//Увеличиваем текущую попытку
				$sessionVerify = ( new SessionVerify( [ 'scenario' => SessionVerify::SCENARIO_INC_ATTEMPT ] ) );
				if ( ! $sessionVerify->load( [
					                             'session_id' => isset( $sessionVerifyCurrent['session_id'] ) ?
						                             $sessionVerifyCurrent['session_id'] : Yii::$app->session->getId(),
					                             'attempt'    => isset( $sessionVerifyCurrent['attempt'] ) ?
						                             $sessionVerifyCurrent['attempt'] : 0,
				                             ], '' ) ) {
					throw new \Exception( 'Не удалось увеличить количество проведенных попыток. Повторите запрос или обратитесь к администратору.' );
				}
				if ( ! $sessionVerify->validate() ) {
					return $sessionVerify->errors;
				}
				$res = $sessionVerify->incAttempt();
				if ( ! $res ) {
					throw new \Exception( $res );
				}

				return true;
			}
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}