<?php

namespace app\modules\app\v1\classes;

use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\Requests;
use Yii;
use yii\helpers\FileHelper;

/**
 * Class Tools - класс содержащий дополнительные методы
 *
 * @package app\modules\app\v1\classes
 */
class Tools {
	/**
	 * Метод формирования и получения строки файлов подключаемых к html странице
	 * может работать с файлами содержащими хэш и не содержащими
	 *
	 * @param        $dir       - относительный путь к папке, содержащей файлы для подключения
	 *                          сейчас доступны файлы js и сыы
	 * @param array  $sort      - сортировка с последующим ограничением выбора
	 *                          пример: $sort = ['app','vendor']; где app и vendor название файла без учета хэша
	 *
	 * @param bool   $newPath   - новый путь до файла, если указанный в dir не должен быть отображен
	 * @param string $delimiter - разделитель в названии файла, разделяет чистое название от хэша
	 *
	 * @return string - вернет строку содержащую скрипты или стили подключения
	 * @throws \Exception - если будет некорректно указана директория содержащая файлы, то выбросит исключение
	 */
	public static function getIncludedFilesInDir( $dir, array $sort = [], $newPath = false, $delimiter = '_' ) {

		$assocExt = [
			'css' => '<link href="%s" rel="stylesheet">',
			'js'  => '<script type="text/javascript" src="%s"></script>',
		];
		$strFiles = '';
		$root     = rtrim( Yii::getAlias( '@webroot' ), '/' );
		$dir      = '/' . trim( $dir, '/' ) . '/';
		$dirRoot  = $root . $dir;
		if ( ! file_exists( $dirRoot ) ) {
			throw new \Exception( 'Некорреткно указан путь до директории с подключаемыми файлами к html странице' );
		}
		$files = \yii\helpers\FileHelper::findFiles( $dirRoot );
		if ( ! empty( $files ) && is_array( $files ) ) {
			$sortFiles = ! empty( $sort ) ? array_flip( $sort ) : $sort;
			foreach ( $files as $k => $file ) {
				$pathParts = pathinfo( $file );
				$fileName  = $pathParts['basename'];
				$ext       = $pathParts['extension'];
				$clearName = strpos( $fileName, $delimiter ) !== false
					? strstr( $fileName, $delimiter, true ) : $fileName;
				if ( ! empty( $sort ) && ! in_array( $clearName, $sort ) ) {
					continue;
				}
				if ( key_exists( $ext, $assocExt ) ) {
					$path                    = empty( $newPath )
						? $dir . $fileName : '/' . trim( $newPath, '/' ) . '/' . $fileName;
					$sortFiles[ $clearName ] = sprintf( $assocExt[ $ext ], $path );
				}
			}
			$arFiles  = array_diff( $sortFiles, array_flip( $sort ) );
			$strFiles = ! empty( $arFiles ) ? implode( '', $arFiles ) : $strFiles;
		}

		return $strFiles;
	}

	/**
	 * Метод для очистки полей и файлов заявки
	 *
	 * @param int    $reqId   - идентификатор заявки
	 * @param string $reqCode - код заявки
	 * @param array  $fields  - поля для очистки, принимает поля заявки
	 *                        и параметры привязки к файлам, такие как foto_sts, foto_pts и пр.
	 *                        если оставить пустым, подтянет поля из параметров
	 *
	 * @return bool|string - вернет истину в результате успеха, иначе строку с исключением
	 */
	public static function clearRequest( int $reqId, string $reqCode, array $fields = [] ) {

		try {

			$fields = ! empty( $fields ) ? $fields : Yii::$app->params['core']['requests']['cleaningFields'];
			if ( empty( $fields ) ) {
				throw new \Exception( 'Не удалось произвести очистку полей заявки, так как не были указаны поля для очистки' );
			}
			$requests     = new Requests();
			$requestFile  = new RequestFile();
			$accessFields = array_merge( $requests->getListFields(), $requestFile->getListFileBind() );
			$errorFields  = array_diff( $fields, $accessFields );
			if ( ! empty( $errorFields ) ) {
				throw new \Exception( 'Невозможно произвести очитку полей заявки, так как название следующих полей указано некорректно: ' . implode( ', ', $errorFields ) );
			}
			$arFileBind = array_intersect( $fields, $requestFile->getListFileBind() );
			if ( ! empty( $arFileBind ) ) {
				//удаление физических файлов заявки
				$pathSaveFile = Yii::$app->params['core']['requests']['pathSaveFile'];
				if ( empty( $pathSaveFile ) ) {
					throw new \Exception( 'Невозможно очистить файлы заявки, так как в настройках некорректно указан путь сохранения файлов, обратитесь к администратору' );
				}
				$webRoot    = rtrim( Yii::getAlias( '@webroot' ), '/' );
				$dirRequest = $webRoot . '/' . trim( $pathSaveFile, '/' ) . '/' . $reqCode . '/';
				if ( file_exists( $dirRequest ) ) {
					foreach ( $arFileBind as $fileBind ) {
						FileHelper::removeDirectory( $dirRequest . $fileBind );
					}
					$findFiles = FileHelper::findFiles( $dirRequest );
					if ( empty( $findFiles ) ) {
						FileHelper::removeDirectory( $dirRequest );
					}
				}
				//удаление записей в таблице file относительно привязок файлов, связи в request_file автоудалятся
				$resDelFile = ( new File() )->deleteByRequestIdAndFileBind( $reqId, $arFileBind );
				if ( is_string( $resDelFile ) ) {
					throw new \Exception( $resDelFile );
				}
				//подготовка полей без учета полей привязок файлов к заявке
				$fields = array_diff( $fields, $arFileBind );
			}
			//очистка полей в таблице requests
			if ( ! empty( $fields ) ) {
				$fields = array_flip( $fields );
				array_walk( $fields, function( &$v ) {

					$v = null;
				} );
				$resUpdareReq = ( new Requests() )->updateOne( $reqId, $fields, false );
				if ( is_string( $resUpdareReq ) ) {
					throw new \Exception( 'Не удалось произвести очистку полей заявки, так как произошел сбой при обновлении полей заявки' );
				}
			}

			return true;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	public static function info( $message = [], $category = null ) {

		self::logElastic( 'info', $message, $category );
	}

	public static function error( $message = [], $category = null ) {

		self::logElastic( 'error', $message, $category );
	}

	public static function warning( $message = [], $category = null ) {

		self::logElastic( 'warning', $message, $category );
	}

	private static function logElastic( $method = null, array $message, $category ) {

		if ( ! $method ) {
			return;
		}
		try {
			$idUser  = Yii::$app->user->getId();
			$sess    = Yii::$app->request->cookies->getValue( "sess" );
			$browser = Yii::$app->getRequest()->getUserAgent();
		} catch ( \Exception $e ) {
			$idUser  = 0;
			$sess    = "";
			$browser = "";
		}
		$ip = strlen( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ?
			$_SERVER["HTTP_X_FORWARDED_FOR"] :
			"0.0.0.0";
		switch ( reset( explode( ".", $category ) ) ) {
			case "user":
				$sMessage = "sess[" . $sess . "] user_id[" . intval( $message["user_id"] ) . "] user_login[" . $message["user_login"] . "] user_ip[" . ( strlen( $message["user_ip"] ) ?
						$message["user_ip"] :
						"0.0.0.0" ) . "] user_browser[" . $message["user_browser"] . "]" . ( strlen( $message["text"] ) ?
						" text[" . base64_encode( $message["text"] ) . "]" : "" );
				break;
			case "pay":
				$sMessage = "sess[" . $sess . "] user_id[" . intval( $idUser ) . "] user_ip[" . $ip . "] user_browser[" . $browser . "] contract[" . $message["contract"] . "]" . ( strlen( $message["text"] ) ?
						" text[" . base64_encode( $message["text"] ) . "]" : "" );
				break;
			case "sms":
				$sMessage = "sess[" . $sess . "] user_login[" . $message["user_login"] . "]" . ( strlen( $message["text"] ) ?
						" text[" . base64_encode( $message["text"] ) . "]" : "" );
				break;
			default:
				if ( ! strlen( $message["text"] ) ) {
					return;
				}
				$sMessage = "sess[" . $sess . "] text[" . base64_encode( $message["text"] ) . "]";
		}
		Yii::$method( $sMessage, $category );
	}
}