<?php
namespace app\modules\app\v1\classes\logs;
use Exception;
use Yii;
/**
 * Класс логирования сущностей
 * Пример использования:
 * <code>
 * use app\modules\app\v1\classes\logs\Log;
 * Log::info(["user_ip" => ($_SERVER["HTTP_X_FORWARDED_FOR"] ?: "0.0.0.0"), "text" => "Произвольный текст"]);
 * </code>
 *
 * @link    https://wiki.carmoney.ru/pages/viewpage.action?pageId=3511138
 * @package app\modules\app\v1\classes\logs
 */
class Log
{
    /**
     * Информационное сообщение
     *
     * @param array  $message  массив полей
     * @param string $category категория лога Yii
     */
    public static function info(array $message = [], string $category)
    {
        self::execute(__FUNCTION__, $message, $category);
    }
    /**
     * Предупреждение
     *
     * @param array  $message  массив полей
     * @param string $category категория лога Yii
     */
    public static function warning(array $message = [], string $category)
    {
        self::execute(__FUNCTION__, $message, $category);
    }
    /**
     * Ошибка
     *
     * @param array  $message  массив полей
     * @param string $category категория лога Yii
     */
    public static function error(array $message = [], string $category)
    {
        self::execute(__FUNCTION__, $message, $category);
    }
    /**
     * Отладочная функция формирования сообщения
     *
     * @param array $message массив полей
     *
     * @return string
     */
    public static function show(array $message = []): string
    {
        return self::getMessage($message);
    }
    /**
     * Обертка логирования Yii
     *
     * @param string $method   метод логирования
     * @param array  $message  массив полей
     * @param string $category категория лога Yii
     */
    private static function execute(string $method, array $message, string $category)
    {
        Yii::$method(self::getMessage($message), $category);
    }
    /**
     * Сборка строки лога
     *
     * @param array $message массив полей
     *
     * @return string строка лога
     * @link https://wiki.carmoney.ru/pages/viewpage.action?pageId=3511138#id-%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%D1%82%D0%B8%D0%BF%D0%BE%D0%B2%D0%BB%D0%BE%D0%B3%D0%BE%D0%B2-%D0%9F%D0%BE%D0%BB%D1%8F
     */
    private static function getMessage(array $message): string
    {
        // в CLI-режиме выборосится исключение при попытке считывания куки
        try {
            $message["sess"] = Yii::$app->request->cookies->getValue("sess");
        } catch (Exception $e) {
            $message["sess"] = "";
        }
        // отладочная информация
        $trace                 = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
        $message["debug_file"] = end($trace)['file'];
        $message["debug_line"] = end($trace)['line'];
        // обработка значений, преобразование произвольного текста сообщения text в base64 для передачи в logstash
        $message = array_map(
            function ($key, $value) {
                $key = trim($key);
                if ($key == "text") {
                    $value = base64_encode($value);
                }
                $value = str_replace(["[", "]"], ["(", ")"], $value);
                return $key."[".$value."]";
            },
            array_keys($message),
            $message
        );
        return implode(" ", $message);
    }
}