<?php

namespace app\modules\app\v1\classes\rules;

use app\modules\app\v1\models\Requests;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Проверяем client_id на соответствие с пользователем, переданным через параметры
 */
class RequestRule extends Rule {
	public $name = 'isAuthor';

	/**
	 * @param string|int $user_id
	 * @param Item       $item   the role or permission that this rule is associated width.
	 * @param array      $params parameters passed to ManagerInterface::checkAccess().
	 *
	 * @return bool a value indicating whether the rule permits the role or permission it is associated with.
	 * @throws \yii\db\Exception
	 */
	public function execute( $user_id, $item, $params ) {

		$request = ( new Requests() )->getOneByFilter( [ 'code' => $params['$code'] ] );

		return isset( $request['client_id'] ) ? $request['client_id'] == $user_id : false;
	}
}