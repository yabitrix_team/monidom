<?php

namespace app\modules\app\v1\classes\rules;

use app\modules\app\v1\models\Groups;
use app\modules\app\v1\models\UserGroup;
use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule {
	public $name = 'userGroup';

	/**
	 * @param string|int $user_id
	 * @param Item       $item   the role or permission that this rule is associated width.
	 * @param array      $params parameters passed to ManagerInterface::checkAccess().
	 *
	 * @return bool a value indicating whether the rule permits the role or permission it is associated with.
	 * @throws \yii\db\Exception
	 */
	public function execute( $user, $item, $params ) {


		if ( ! Yii::$app->user->isGuest ) {

			$userGroup = ( new UserGroup() )->getOneByFilter( [ 'user_id' => Yii::$app->user->getId() ] );
			$group     = ( new Groups() )->getOne( $userGroup['group_id'] );
			//todo[hdcy]: 19.04.2018 Вынести группу в identityUser
			//$group     = Yii::$app->user->identity->group;
			if ( $item->name === 'role_admin' ) {
				return $group['identifier'] == 'admin';
			} elseif ( $item->name === 'role_partner_admin' ) {
				return $group['identifier'] == 'partner_admin';
			} elseif ( $item->name === 'role_agent' ) {
				return $group['identifier'] == 'agent';
			} elseif ( $item->name === 'role_client' ) {
				return $group['identifier'] == 'client';
			} elseif ( $item->name === 'role_invest' ) {
				return $group['identifier'] == 'investor';
			} elseif ( $item->name === 'role_user_partner' ) {
				return $group['identifier'] == 'partner_user';
			} elseif ( $item->name === 'role_mobile_application' ) {
				return $group['identifier'] == 'mobile_user';
			} elseif ( $item->name === 'role_support' ) {
				return $group['identifier'] == 'support';
			} elseif ( $item->name === 'role_lead_agent' ) {
				return $group['identifier'] == 'lead_agent';
            } elseif ( $item->name === 'role_call' ) {
                return $group['identifier'] == 'call';
			}
		}

		return false;
	}
}