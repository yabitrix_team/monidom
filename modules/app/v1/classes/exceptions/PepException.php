<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Исключение по ПЭП
 *
 * @package app\modules\app\v1\classes\exceptions
 */
class PepException extends \Exception implements \Throwable
{

}