<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Class InvalidParamException - класс формирования исключения при неуказании параметров
 *
 * @package app\api\common\v1\classes\exceptions
 */
class InvalidParamException extends ResponseException
{
    /**
     * Константа кода параметры в конфигцрациях
     */
    public const CODE_VALIDATION = 1020;

    /**
     * InvalidParamException constructor.
     *
     * @param string          $param    - название параметра
     * @param \Throwable|null $previous - предыдущее исключение
     */
    public function __construct(string $param, \Throwable $previous = null)
    {
        parent::__construct(self::CODE_VALIDATION, $param, $previous);
    }
}