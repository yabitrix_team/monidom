<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Исключение по сбросу пароля
 *
 * @package app\modules\app\v1\classes\exceptions
 */
class ResetException extends \Exception implements \Throwable
{

}