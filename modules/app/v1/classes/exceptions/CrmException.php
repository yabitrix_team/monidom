<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Исключение по CRM
 *
 * @package app\modules\app\v1\classes\exceptions
 */
class CrmException extends \Exception implements \Throwable
{

}