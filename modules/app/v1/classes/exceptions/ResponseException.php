<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Class ResponseException - класс формирования исключения на выдачу по коду связанному с текстом ошибки
 *
 * @package app\api\common\v1\classes\exceptions
 */
class ResponseException extends \Exception
{
    /**
     * @var array - массив связей кодов ошибок с текстами ошибок,
     *            диапазон кодов начинается с 1000, все что меньше это зарезервированные коды
     */
    public static $codeMsg = [
        //зарезервированные ошибки соответствуют http кодам
        401  => 'Вы не авторизованы в системе',
        403  => 'У вас нет прав на выполнение операции',
        //ошибки общего назначения, так же могут использоваться в наследниках
        1000 => 'Произошла неизвестная ошибка, обратитесь к администратору',
        1020 => 'В конфигурациях некорректно указан параметр «%s», обратитесь к администратору',
    ];

    /**
     * ResponseException constructor
     *
     * @param int               $code     - код связи для получения текста ошибки
     * @param null|string|array $param    - параметры для подстановки, если в тексте заданы спец.символы
     * @param \Throwable|null   $previous - предшествующее исключение
     */
    public function __construct(int $code, $param = null, \Throwable $previous = null)
    {
        $codeEx = 1000;
        $msgEx  = self::$codeMsg[$codeEx];
        if (array_key_exists($code, self::$codeMsg)) {
            $codeEx = $code;
            if (!empty($param)) {
                !is_string($param) ?: self::$codeMsg[$code] = sprintf(self::$codeMsg[$code], $param);
                !is_array($param) ?: array_unshift($param, self::$codeMsg[$code]);
                !is_array($param) ?: self::$codeMsg[$code] = call_user_func_array('sprintf', $param);
            }
            $msgEx = self::$codeMsg[$code];
        }
        parent::__construct($msgEx, $codeEx, $previous);
    }
}