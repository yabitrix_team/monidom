<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Исключение по авторизации
 *
 * @package app\modules\app\v1\classes\exceptions
 */
class AuthException extends \Exception implements \Throwable
{

}