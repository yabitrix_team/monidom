<?php
namespace app\modules\app\v1\classes\exceptions;

/**
 * Исключение по регистрации
 *
 * @package app\modules\app\v1\classes\exceptions
 */
class RegisterException extends \Exception implements \Throwable
{

}