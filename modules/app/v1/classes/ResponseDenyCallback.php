<?php

//todo[hdcy]: 20.04.2018 Правильно настроить калбэк для вызова из поведения
namespace app\modules\app\v1\classes;
class ResponseDenyCallback {
	function get( $isGuest ) {

		if ( $isGuest ) {
			throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
		} else {
			throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
		}
	}
}