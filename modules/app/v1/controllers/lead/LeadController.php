<?php
namespace app\modules\app\v1\controllers\lead;

use app\modules\app\v1\classes\exceptions\CrmException;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\AutoBrands;
use app\modules\app\v1\models\AutoModels;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\Regions;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\classes\logs\Log;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class LeadController - контроллер для работы с лидами
 *
 * @package app\modules\app\v1\controllers
 */
class LeadController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод экшен для поиска лида для конкретного юзера
     *
     * @return array|string- вернет результирующий массив или строку с исключением
     * @throws \Exception
     */
    public function actionIndex()
    {
        $data = (new Leads())->getAllByFilter(
            ['client_mobile_phone' => Yii::$app->user->identity->username],
            ['created_at' => 'desc']
        )[0];
        if ($data['auto_model_guid']) {
            $autoModel                 = (new AutoModels())->getByGuid($data['auto_model_guid']);
            $autoBrand                 = (new AutoBrands())->getOne($autoModel['auto_brand_id']);
            $region                    = (new Regions())->getOneByFilter(['name' => $data['region']]);
            $data['client_first_name'] = Yii::$app->user->identity->first_name;
            $data['client_last_name']  = Yii::$app->user->identity->last_name;
            $data['client_patronymic'] = Yii::$app->user->identity->second_name;
            $data['client_region_id']  = $region;
            $data['auto_brand_id']     = $autoBrand;
            $data['auto_model_id']     = $autoModel;
            $data['auto_year']         = [
                'id'   => $data['auto_year'],
                'name' => $data['auto_year'],
            ];
            unset($data['auto_model_guid']);
            unset($data['guid']);
            unset($data['region']);
        };
        if (empty($data)) {
            throw new \Exception('notfound');
        }
        if (is_string($data)) {
            throw new \Exception($data);
        }

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен получения лида по коду
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        $data = (new Leads())->getByCode($code);
        if (empty($data)) {
            throw new \Exception('Лид не найден');
        }
        if (is_string($data)) {
            throw new \Exception($data);
        }
        $user = (new Users())->getOneByFilter(['username' => $data['client_mobile_phone']]);
        if ($user) {
            $data = [
                'userExists' => 1,
                'firstName'  => $user['first_name'],
                'phone'      => $user['username'],
            ];
        };

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод экшен для создания лида
     *
     * @return array|string - вернет результирующий массив или строку с исключением
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            $reqBody    = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $modelLeads = new Leads();
            if (!$modelLeads->load($reqBody, '')) {
                throw new CrmException('Не переданы параметры для создания лида');
            }
            //сохранение лида
            $result = $modelLeads->create();
            if (is_a($result, Leads::class)) {
                return $this->getClassAppResponse()::get(false, $result->firstErrors);
            }
            if (is_a($result, \Exception::class)) {
                throw new CrmException($result->getMessage(), $result->getCode(), $result);
            }
            $leadId       = Yii::$app->db->lastInsertID;
            $dataConsumer = $modelLeads->getDataConsumer();
            //сохранение предварительной заявки при сущестовании пользователя
            $resUser = Users::getByLogin($result['client_mobile_phone']);
            if (is_string($resUser)) {
                throw new CrmException($resUser);
            }
            if (!empty($resUser)) {
                $data = $modelLeads->prepareRequest(
                    $dataConsumer['identifier'],
                    $dataConsumer['requests_origin_id'],
                    $resUser['id'],
                    $result
                );
                if (is_a($data, \Exception::class)) {
                    throw new CrmException($data->getMessage(), $data->getCode(), $data);
                }
                $modelRequest = new Requests();
                $requestSave  = $modelRequest->save($data);
                if (empty($requestSave) || is_a($requestSave, \Exception::class)) {
                    throw new CrmException(
                        'Не удалось сохранить предваритульную заявку по лиду, обратитесь к администратору'
                    );
                }
                //добавление статуса заявки для МП
                if ($requestSave == 1) {
                    $resSetStatus = (new StatusesMPJournal)->setStatus(
                        Yii::$app->db->lastInsertID,
                        StatusesMP::getStatusCreated()
                    );
                    if (is_a($resSetStatus, \Exception::class)) {
                        throw new CrmException($resSetStatus->getMessage(), $resSetStatus->getCode(), $resSetStatus);
                    }
                }
            }
            //подготовка и отправка СМС сообщения
            $smsText  = $result['client_first_name'].', ';
            $smsText  .= $dataConsumer['identifier'] == $modelLeads->getConsumerDefault()
                ? sprintf(
                    $dataConsumer['sms_text'],
                    $_SERVER['REQUEST_SCHEME'].'://register.'.Yii::$app->params['core']['domain'].'/'.$result['code']
                )
                : $dataConsumer['sms_text'];
            $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
            $modelSms->load(
                [
                    'phone'   => $result['client_mobile_phone'],
                    'message' => $smsText,
                ],
                ''
            );
            $smsId = $modelSms->setLogCategory('sms.lead.client')->sendData();
            //обновление лида после отправки СМС сообщения
            $resUpdate = $modelLeads->updateOne(
                $leadId,
                [
                    'sms_id' => $smsId,
                ]
            );
            if (is_a($resUpdate, \Exception::class)) {
                throw new CrmException($resUpdate->getMessage(), $resUpdate->getCode(), $resUpdate);
            }
            //todo[echmaster]: 20.11.2018 ГУИД всегда приходит (комментарий Хайрова)
            //отправка события "Отправка СМС с ссылкой клиенту" в CRM при успешной записи лида
            if (!empty($result['guid'])) {
                EventsLoad::sendEventLoadBit(410, $result['guid'], $result['client_mobile_phone']);
            }
            $data = [
                'code'  => $result['code'],
                'smsid' => $smsId,
            ];
            Log::info(
                ["text" => "Тело запроса:\n".print_r($reqBody, true)],
                "in.service.lead.create"
            );
            return $this->getClassAppResponse()::get($data);
        } catch (CrmException $e) {
            Log::error(
                ["text" => "Текст ошибки: ".$e->getMessage()."\nТело запроса:\n".print_r($reqBody, true)],
                "in.service.lead.create"
            );
            //удаление лида в случае не удачной отправки сообщения или обновления лида
            if (isset($modelLeads) && isset($leadId)) {
                $modelLeads->deleteOne($leadId);
            }
            //удаление созданной заявки
            if (isset($modelRequest) && isset($requestId)) {
                $modelRequest->deleteOne($requestId);
            }
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (\Exception $e) {
            //удаление лида в случае не удачной отправки сообщения или обновления лида
            if (isset($modelLeads) && isset($leadId)) {
                $modelLeads->deleteOne($leadId);
            }
            //удаление созданной заявки
            if (isset($modelRequest) && isset($requestId)) {
                $modelRequest->deleteOne($requestId);
            }
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Экшен удаления лида по коду
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     */
    public function actionDelete($code)
    {
        $data = (new Leads)->deleteByCode($code);

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
