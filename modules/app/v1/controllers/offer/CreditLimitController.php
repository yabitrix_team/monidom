<?php
namespace app\modules\app\v1\controllers\offer;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\CreditLimit;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

/**
 * Class CreditLimit - контроллер взаимодействия с кредитными лимитам
 *
 * @package app\modules\app\v1\controllers
 */
class CreditLimitController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения кредитных лимитов авторизованным пользователем
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionIndex()
    {
        if (!$creditLimitsOfferType = Yii::$app->params['mp']['settings']['creditLimitsOfferType']) {
            throw new \RuntimeException(
                'В параметрах не указан тип предложения для кредитного лимита, обратитесь к администратору'
            );
        }
        $result = (new CreditLimit())->getByPhoneAndOfferType(
            Yii::$app->user->identity->username,
            $creditLimitsOfferType,
            ['code', 'amount', 'period']
        );
        $data   = empty($result)
            ? []
            : [
                [
                    'code'   => $result['code'],
                    'amount' => (int) $result['amount'],
                    'period' => (int) $result['period'],
                ],
            ];

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors(): array
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}