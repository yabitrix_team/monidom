<?php
namespace app\modules\app\v1\controllers\notification;

use app\modules\app\v1\classes\traits\TraitConfigController;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class NotificationController - контроллер взаимодействия с уведомлениями
 *
 * @package app\controllers\app\v1\notification
 */
class NotificationController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @return array
     */
    public function actionIndex()
    {
        return $this->getClassAppResponse()::get(['get_all_notification' => \Yii::$app->user->id]);
    }

    /**
     * @return array
     */
    public function actionCreate()
    {

        /*$reqBody            = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (is_a($result, FCM::class)) {
            return $this->getClassAppResponse()::get(false, $result->firstErrors);
        }*/
        return $this->getClassAppResponse()::get(['create' => true]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}