<?php
namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\RequestsOldFiles;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\app\v1\models\RequestsMetrika;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class RequestController - контроллер для работы с заявкой
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив файлов, которые выводим вместе с заявкой
     */
    protected $arFileBind = [
        'foto_sts',
        'foto_pts',
        'foto_auto',
        'foto_passport',
        'foto_client',
        'foto_card',
        'doc_pack_1',
        'doc_pack_2',
        'doc_pack_3',
    ];
    /**
     * @var array - массив полей файлов, выводимые в заявке
     */
    protected $arFileFields = [
        'code',
        'url',
        'name',
        'description',
        'created_at',
    ];
    /**
     * @var bool - отвечает за добавление файлов в контракт
     *             при получени всех или одной заявки в родительком контроллере
     */
    protected $addFilesInContract = true;
    /**
     * @var array - массив данных заявки, предназначени для снижения запросов к БД
     *            при наследовании
     */
    protected $request;

    /**
     * Экшен получения всех заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */
    public function actionIndex()
    {
        $classModelRequest = $this->getModelRequest();
        $modelRequest      = new $classModelRequest;
        $data              = $modelRequest->getAll();
        if (!empty($data) && !empty($this->addFilesInContract)) {
            foreach ($data as &$item) {
                $item = array_merge(
                    $item,
                    $modelRequest->getFiles(
                        $item['id'],
                        $this->arFileFields,
                        $this->arFileBind
                    )
                );
            }
        }

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен получения заявки по коду
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        //из трейта подтягиваем namespace модели Request
        $classModelRequest = $this->getModelRequest();
        $modelRequest      = new $classModelRequest;
        $data              = $modelRequest->getByCode($code);
        if (empty($data) || is_string($data)) {
            $msg = empty($data)
                ? 'Не удалось найти заявку по коду для текущего пользователя'
                : $data;
            throw new \Exception($msg);
        }
        if (!empty($this->addFilesInContract)) {
            $data = array_merge($data, $modelRequest->getFiles($data['id'], $this->arFileFields, $this->arFileBind));
        }

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * @param null $code
     * @param null $id
     *
     * @return array
     * @throws \Exception
     */
    public function actionNotice($code = null, $id = null)
    {
        $data = Yii::$app->request->getIsGet()
            ? [$code, $id]
            : (Yii::$app->request->getIsDelete()
                ? [$id, $code]
                : []);

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен для создания заявки
     *
     * @return array|bool|string
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            //из трейта подтягиваем namespace модели Request
            $modelRequest = $this->getModelRequest();
            $model        = new $modelRequest(['scenario' => $modelRequest::SCENARIO_CREATE]);
            $reqBody      = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (!$model->load($reqBody, '')) {
                throw new \Exception('Не переданы данные для создания заявки');
            }
            $result = $model->create();
            if (is_a($result, \Exception::class)) {
                throw new \Exception($result->getMessage());
            }
            if (is_a($result, $modelRequest)) {
                return $this->getClassAppResponse()::get(false, $result->firstErrors);
            }
            //добавление статуса МП "Создана" к заявке для всех кабинетов
            $this->request['id'] = $requestId = Yii::$app->db->lastInsertID;
            $resSetStatus        = (new StatusesMPJournal)->setStatus($requestId, StatusesMP::getStatusCreated());
            if (is_a($resSetStatus, \Exception::class)) {
                return $resSetStatus;
            }
            // добавление Ya.Metrika сессии, если она передана
            if (array_key_exists('ya_client_id', $reqBody)) {
                $addData           = [
                    'request_id'   => $requestId,
                    'metrika_id'   => $reqBody['ya_client_id'],
                    'user_id'      => Yii::$app->user->identity->id / 1,
                    'metrika_type' => (array_key_exists('metrika_type', $reqBody)) ?
                        $reqBody['metrika_type'] : 'ya',
                ];
                $objRequestMetrika = (new RequestsMetrika($addData))->addData();
                if (is_a($objRequestMetrika, RequestsMetrika::class)) {
                    return $this->getClassAppResponse()::get(false, $objRequestMetrika->firstErrors);
                }
                if (is_a($objRequestMetrika, \Exception::class)) {
                    return $this->getClassAppResponse()::get(false, false, $objRequestMetrika->getMessage());
                }
            }

            // /запись id метрики к заявке
            return $this->getClassAppResponse()::get(
                [
                    'code' => $result,
                ]
            );
        } catch (\Exception $e) {
            if (isset($requestId) && isset($model)) {
                $model->deleteOne($requestId);
            }
            throw $e;
        }
    }

    /**
     * Экшен для обновления заявки
     *
     * @param $code - Код(хэш) заявки
     *
     * @return array
     * @throws \Exception
     */
    public function actionUpdate($code)
    {
        $modelRequest = $this->getModelRequest();
        $model        = new $modelRequest(['scenario' => $modelRequest::SCENARIO_UPDATE]);
        //поиск заявки по коду
        $this->request = $model->getByCode($code);
        if (empty($this->request) && !is_array($this->request)) {
            $msg = empty($this->request)
                ? 'При попытке обновления заявки не уадлось найти заявку, обратитесь к администратору'
                : 'При попытке обновления заявки произошла неизвестная ошибка, обратитесь к администратору';
            throw new \Exception($msg);
        }
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (!$model->load($reqBody, '')) {
            throw new \Exception('Не переданы данные для обновления заявки');
        }
        if (!$model->validate()) {
            return $this->getClassAppResponse()::get(false, $model->firstErrors);
        }
        $result = $model->updateByCode($code);
        if (is_a($result, \Exception::class)) {
            throw $result;
        }

        return $this->getClassAppResponse()::get(
            [
                'update_request' => !empty($result),
            ]
        );
    }

    /**
     * контроллер удаляет файлы, привязанные к разделу.
     *
     * @param $code
     *
     * @return array
     * @throws \Exception
     */
    public function actionDeleteOldFiles($code)
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $request = new Requests(['scenario' => Requests::SCENARIO_IMG_CHEK_TIME]);
        if (
        !$request->load(
            [
                'imgCheckTime' => $reqBody["img_check_time"],
            ],
            ''
        )
        ) {
            throw new Exception('Не удалось загрузить данные данные заяувки '.__FUNCTION__);
        }
        $ret = $request->deleteOldFiles($code);
        if (is_a($ret, Requests::class) || is_a($ret, \Exception::class)) {
            throw new \Exception('Ошибка: '.print_r($ret->firstErrors, 1));
        }

        return $this->getClassAppResponse()::get($ret);
    }

    /**
     * ф-я проверяет файлы на "свежесть",
     * сколько прошло со времени загрузки файла.
     * бракуются все файлы, если есть хоть один старый.
     *
     * @param $code
     *
     * @return array
     * @throws \Exception
     */
    public function actionIsOldFiles($code)
    {
        $reqBody         = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $requestOldFiles = new RequestsOldFiles(['scenario' => RequestsOldFiles::SCENARIO_IMG_CHEK_TIME]);
        if (
        !$requestOldFiles->load(
            [
                'imgCheckTime' => $reqBody["img_check_time"],
            ],
            ''
        )
        ) {
            throw new \Exception('Не удалось загрузить данные заяувки '.__FUNCTION__);
        }
        $ret = $requestOldFiles->isOldFiles($code);
        if (is_a($ret, Requests::class) || is_a($ret, \Exception::class)) {
            throw new \Exception('Ошибка: '.print_r($ret->firstErrors, 1));
        }

        return $this->getClassAppResponse()::get(["is_old_files" => $ret]);
    }

    /**
     * ф-я проверяет наличие и удаляет файлы, привязанные к разделу.
     *
     * @param $code
     *
     * @return array
     * @throws \Exception
     */
    public function actionCheckAndDeleteOldFiles($code)
    {
        $reqBody         = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $requestOldFiles = new RequestsOldFiles(['scenario' => RequestsOldFiles::SCENARIO_IMG_CHEK_TIME]);
        if (
        !$requestOldFiles->load(
            [
                'imgCheckTime' => $reqBody["img_check_time"],
            ],
            ''
        )
        ) {
            throw new \Exception('Не удалось загрузить данные данные заяувки '.__FUNCTION__);
        }
        $ret = $requestOldFiles->isOldFiles($code);
        if (is_a($ret, Requests::class) || is_a($ret, \Exception::class)) {
            throw new \Exception('Ошибка: '.print_r($ret->firstErrors, 1));
        }
        if ($ret) {
            $ret = $requestOldFiles->deleteOldFiles($code);
        }

        return $this->getClassAppResponse()::get($ret);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}
