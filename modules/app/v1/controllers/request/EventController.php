<?php
namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\RMQ;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class EventController - контроллер для работы событиями заявки
 *
 * @package app\modules\app\v1\controllers
 */
class EventController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив данных заявки, предназначени для снижения запросов к БД
     *            при наследовании
     */
    protected $request;

    /**
     * Экшен создания события прикрепленного к заявке
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */
    public function actionCreate()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        //ассоциация кодов события заявки с кодами событий CRM
        $assocEvents = [
            702 => 310,//полная завявка в ЛКК
            711 => 320,//отправлены файлы сканы документов
        ];
        if (!isset($reqBody['code']) || strlen($reqBody['code']) != 32) {
            throw new \Exception('Некорректно указан код заявки');
        }
        if (!isset($reqBody['event']) || !is_numeric($reqBody['event'])) {
            throw new \Exception('Некорректно указан код события для заявки');
        }
        if (!$this->request = (new Requests())->getOneByFilter(['code' => $reqBody['code']])) {
            throw new \Exception('Указанная заявка отсутствует в системе');
        }
        if (!$event = (new Events())->getByCode($reqBody['event'])) {
            throw new \Exception('Указанный код события отсутствует в системе');
        }
        //отправка события в CRM: Заполнение данных ЛКК или Заполнение фото ЛКК
        if (key_exists($reqBody['event'], $assocEvents)) {
            EventsLoad::sendEventLoadBit(
                $assocEvents[$reqBody['event']],
                $this->request['guid'],
                Yii::$app->user->identity->username
            );
        }
        $modelRequestsEvents = new RequestsEvents();
        $modelRequestsEvents->load(
            [
                'request_id' => $this->request['id'],
                'event_id'   => $event['id'],
            ],
            ''
        );
        if (!$modelRequestsEvents->validate()) {

            return $this->getClassAppResponse()::get(false, $modelRequestsEvents->firstErrors);
        }
        $result = $modelRequestsEvents->create();
        if (!is_numeric($result)) {
            throw new \Exception('Произошла ошибка при попытке прикрепления события к заявке');
        }

        $eventsData = Events::instance()->getDataByRequestId( $this->request['id'] );
        $reFirstPack = false;
        foreach ($eventsData as $reqEvent) {
            if ((int)$reqEvent["code"] == 830 && !(int)$reqEvent["is_sended"]) {
                $reFirstPack = true;
                break;
            }
        }
        if ((int)$reqBody['event'] == 101 && $reFirstPack) {
            ( new RequestsEvents() )->update( "request_id=".$this->request['id']." and event_id = " . $reqEvent['event_id'], [ "is_sended" => 1 ] );
        }

        Log::info(
            [
                "text" => $this->request['id'].' / '.Yii::$app->user->identity->username.
                          ' : сохранен event через EventController, id='.$event['id'],
            ],
            "exchange.event.202"
        );

        return $this->getClassAppResponse()::get(['success' => 1]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}
