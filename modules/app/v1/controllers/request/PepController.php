<?php
namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Contracts;
use app\modules\app\v1\models\RequestPep;
use app\modules\app\v1\models\SmsConfirm;
use app\modules\app\v1\models\AuthToken;
use app\modules\mp\v1\models\Users;
use Yii;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class PepController - контроллер для работы с подтверждением заявки по ПЭП
 *
 * @package app\modules\app\v1\controllers
 */
class PepController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен проверки подписания ПЭП в заявке
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     */
    public function actionByCode($code)
    {
        try {
            $isPep             = false;
            $userId            = Yii::$app->user->id;
            $classModelRequest = $this->getModelRequest();
            $modelRequest      = new $classModelRequest;
            $requests          = $modelRequest->getByClientId(
                $userId,
                [
                    'id',
                    'code',
                    'num_1c',
                    'client_passport_serial_number',
                    'client_passport_number',
                    'created_at',
                    'is_pep',
                ]
            );
            if (empty($requests)) {
                throw new \Exception(
                    'Произошла ошибка при попытке проверки подписания простой электронной подписи в заявке, у пользователя нет активных заявок, обратитесь к администратору'
                );
            }
            $passports         = [];
            $pepByPassportDate = [];
            $curRequest        = [];
            $date              = date('Y-m-d');
            foreach ($requests as $item) {
                $passport     = trim($item['client_passport_serial_number']).trim($item['client_passport_number']);
                $passportDate = $passport.substr(trim($item['created_at']), 0, 10);
                $val          = [
                    'id'           => $item['id'],
                    'passport'     => $passport,
                    'passportDate' => $passport.$date,
                    'is_pep'       => $item['is_pep'],
                ];
                if ($item['code'] == $code) {
                    $curRequest = $val;
                } else {
                    $pepByPassportDate[$passportDate][$item['id']] = $val['is_pep'];
                    if (!empty($item['num_1c'])) {
                        $passports[$val['passport']][] = $item['num_1c'];
                    }
                }
            }
            if (empty($curRequest)) {
                throw new \Exception(
                    'Произошла ошибка проверки подписания простой электронной подписи в заявке, не удалось найти проверяемую заявку, обратитесь к администратору'
                );
            }
            //проверка на подписание ПЭП в текущей заявке
            $isPep = !empty($curRequest['is_pep']);
            if (empty($isPep)) {
                //получение id заявки той же даты, с тем же паспортом и с признаком проставленного ПЭП
                $requestRelation = is_array($pepByPassportDate[$curRequest['passportDate']]) ? array_search(
                    1,
                    $pepByPassportDate[$curRequest['passportDate']]
                ) : false;
                //при наличии активного договора с тем же паспортом
                if (empty($requestRelation) && is_array($passports[$curRequest['passport']])) {
                    $agreements       = $passports[$curRequest['passport']];
                    $agreementsActive = (new Contracts())->getActiveCodeByUserId($userId);
                    if (is_a($agreementsActive, \Exception::class)) {
                        throw new \Exception(
                            'Произошла ошибка при попытке проверки подписания простой электронной подписи в заявке, обратитесь к администратору'
                        );
                    }
                    $agreementRelation = reset(array_intersect($agreements, $agreementsActive));
                }
                //проставление ПЭП при одном из условий
                if (!empty($requestRelation) || !empty($agreementRelation)) {
                    $resUpdate = $modelRequest->updateByCode($code, ['is_pep' => 1]);
                    if (is_a($resUpdate, \Exception::class)) {
                        throw $resUpdate;
                    }
                    $isPep = true;
                    //подготовка и запись подтверждения ПЭП
                    $modelRequestPep = new RequestPep();
                    $relation        = !empty($requestRelation)
                        ? ['relation_request' => $requestRelation]
                        : ['relation_agreement' => $agreementRelation];
                    $modelRequestPep->load(
                        array_merge(
                            [
                                'request_id'   => $curRequest['id'],
                                'confirm_type' => !empty($requestRelation)
                                    ? RequestPep::CONFIRM_TYPE_REQUEST
                                    : RequestPep::CONFIRM_TYPE_AGREEMENT,
                            ],
                            $relation
                        ),
                        ''
                    );
                    $modelRequestPep->save();
                }
            }

            return $this->getClassAppResponse()::get(['is_pep' => $isPep]);
        } catch (Exception $e) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                'Произошел сбой при работе с базой данных, обратитесь к администратору'
            );
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Экшен простановки ПЭП при подтверждением через СМС
     *
     * @param $code - код заявки
     *
     * @return array
     */
    public function actionUpdate($code)
    {
        try {
            $reqBody           = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $classModelRequest = $this->getModelRequest();
            $modelRequest      = new $classModelRequest;
            //проверка на существование заявки и проверка на проставленную ПЭП
            $request = $modelRequest->getBycode($code);
            if (empty($request) || is_string($request) || is_a($request, \Exception::class)) {
                throw new \Exception(
                    'При попытке подписания простой электронной подписи не удалось получить данные по заявке, обратитесь к администратору'
                );
            }
            if (empty($request['is_pep'])) {
                $modelSmsConfirm = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CHECK]);
                $modelSmsConfirm->load(
                    [
                        'login' => !empty($reqBody['login'])
                            ? $reqBody['login']
                            : Users::findIdentityByAccessToken(AuthToken::getTokenInHeader())['username'],
                        'code'  => empty($reqBody['sms_code']) ? false : $reqBody['sms_code'],
                    ],
                    ''
                );
                $modelSmsConfirm->verificationCode();
                $resSmsId = $modelSmsConfirm->getByLogin(null, ['sms_id']);
                if (empty($resSmsId) || is_string($resSmsId)) {
                    throw new \Exception(
                        'Не удалось получить идентификатор СМС-сообщения, обратитесь к администратору'
                    );
                }
                //подписание ПЭП в заявке
                $resUpdate = $modelRequest->updateByCode($code, ['is_pep' => 1]);
                if (empty($resUpdate) || is_a($resUpdate, \Exception::class)) {
                    $msg = empty($resUpdate)
                        ? 'Указанная заявка отсутствует в системе, обратитесь к администратору'
                        : 'Не удалось подписать простую электронную подпись по данной заявки, обратитесь к администратору';
                    throw $resUpdate;
                }
                //подготовка и запись подтверждения ПЭП
                $modelRequestPep = new RequestPep();
                $modelRequestPep->load(
                    [
                        'request_id'   => $request['id'],
                        'confirm_type' => RequestPep::CONFIRM_TYPE_SMS,
                        'sms_code'     => $reqBody['sms_code'],
                        'sms_id'       => $resSmsId['sms_id'],
                        'sms_provider' => RequestPep::SMS_PROVIDER_REDSMS,
                        'user_agent'   => $reqBody['user_agent'],
                    ]
                    ,
                    ''
                );
                $modelRequestPep->save();
            }

            return $this->getClassAppResponse()::get(['is_pep' => true]);
        } catch (Exception $e) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                'Произошел сбой при работе с базой данных, обратитесь к администратору'
            );
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}
