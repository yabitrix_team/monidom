<?php

namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\File;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class FirstDocumentController - контроллер для работы с документами 1 пакета заявки из веб-сервисов
 *
 * @package app\modules\app\v1\controllers
 */
class FirstDocumentController extends Controller {
	/**
	 * @var string - корень сайта
	 */
	private $webRoot;
	/**
	 * @var - название сервера
	 */
	private $serverName;

	/**
	 * Инициализация компонента, происходит после загрузки конфигурации
	 */
	public function init() {

		parent::init();
		$this->webRoot    = rtrim( Yii::getAlias( '@webroot' ), '/' );
		$this->serverName = Yii::$app->getRequest()->serverName;
	}

	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения документов первого пакета
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */
	public function actionByCode( $code ) {

        $classModelRequest = $this->getModelRequest();
        $modelRequest      = new $classModelRequest;
        $request           = $modelRequest->getByCode( $code );
        if ( ! $request ) {
            throw new \Exception( 'Указанная заявка отсутствует в системе' );
        };
        //Проверяем существуют ли уже доки и если да, то отправляем их клиенту
        $requestDocs = ( new File() )->getDocByRequestCode( $code, 1 );
        if ( ! empty( $requestDocs ) ) {

            if ( empty( $request['user_id'] ) ) {
                return $this->getClassAppResponse()::get( false, false, "В заявке не указан подписант, обратитесь к администратору" );
            }
            if ( Yii::$app->user->id == $request['user_id'] ) {

                foreach ( $requestDocs as $doc ) {
                    $docs['docPack1'][] = [
                        "url"         => '/doc/' . $doc['code'],
                        "name"        => reset( explode( '.', $doc['name'] ) ),
                        "description" => $doc['description'] ? $doc['description'] : $doc['name'],
                        "code"        => $doc['code'],
                    ];
                }

                return $this->getClassAppResponse()::get( $docs ); //Отправляем существующие доки клиенту
            }
            // подписант другой, меняем
            if ( ! $event = ( new Events() )->getByCode( 201 ) ) {
                throw new \Exception( 'Указанный код события отсутствует в системе' );
            }
            $res = ( new Requests() )->updateByCode( $code, [ 'user_id' => Yii::$app->user->getId() ] );
            if ( $res != 1 ) {
                if ( is_a( $res, \Exception::class ) ) {
                    throw $res;
                } else {
                    throw new \Exception( "Произошла неизвестная ошибка при обновлении подписанта. Обратиесь к администратору" );
                }
            } else {
                // успешно сохранили подписанта
                $modelRequestsEvents = new RequestsEvents();
                if ( $modelRequestsEvents->load( [
                                                     'request_id' => $request['id'],
                                                     'event_id'   => $event['id'],
                                                     'is_sended'  => 0,
                                                 ], '' ) ) {
                    if ( $modelRequestsEvents->validate() ) {
                        $result = $modelRequestsEvents->create();
                        //Подписант сменился, удаляем старые документы
                        foreach ( $requestDocs as $doc ) {
                            $result = Yii::$app->file->delete( $doc['code'] );
                            if ( is_string( $result ) ) {
                                throw new \Exception( $result );
                            }
                        }
                        if ( is_numeric( $result ) ) {
                            return $this->getClassAppResponse()::get( [ 'docPack1' => [] ] );
                        }
                        throw new \Exception( 'Произошла ошибка при попытке прикрепления события к заявке' );
                    } else {
                        return $this->getClassAppResponse()::get( false, $modelRequestsEvents->firstErrors );
                    }
                } else {
                    throw new \Exception( 'При добавлении события к заявке произошла ошибка загрузки данных в модель' );
                }
            }
        } else {
            return $this->getClassAppResponse()::get( [ 'stay' => true ] );
        }
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}

	protected function dataEncode( $data ) {

		$code = false;
		if ( ! empty( $data ) ) {
			$converter = [ '+' => '-', '/' => '_', '=' => '' ];
			$code      = strtr( base64_encode( serialize( $data ) ), $converter );
			//если параметр в URL длинее 255 символов и нет слэша, то закрывает доступ к странице
			if ( strlen( $code ) > 250 ) {
				$code = substr_replace( $code, "/", 250, 0 );
			}
		}

		return $code;
	}
}
