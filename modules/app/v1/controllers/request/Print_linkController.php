<?php

namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\Requests;
use Imagick;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class FileController - контроллер для работы с файлами
 *
 * @package app\modules\app\v1\controllers
 */
class Print_linkController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	/**
	 * @var array - типы регистрируемых док-в
	 */
	private $docFileType = [
		'doc_pack_1',
		'doc_pack_2',
        'doc_pack_3',
	];

    /**
     * Метод экшен получения фотографий по открытой ссылке
     *
     * @param $code - код print_link
     *
     * @return array
     * @throws \Exception
     */
	public function actionByCode( $code ) {

        if ( ! isset( $code ) ) {
            throw new \Exception( 'Указан некорректный код' );
        }
        $request = ( new Requests() )->getByPrint_link( $code );
        if ( empty( $request ) ) {
            throw new \Exception( 'Не найдено заявки по коду' );
        }
        if ( ! ( $print_link_expired = Yii::$app->params['agent']['print_link_expired_at'] )
             || ! is_numeric( $print_link_expired ) ) {
            throw new \Exception( 'Ошибка на стороне сервера, не указаны или указаны некорректно параметры для проверки времени жизни ссылки' );
        }
        //Проверяем время жизни ссылки
        if ( $request['print_link_expired'] == '0000-00-00 00:00:00' ||
             strtotime( 'now' ) - strtotime( $request['print_link_expired'] ) > $print_link_expired ) {
            throw new \Exception( 'Время ссылки истекло' );
        }

        //загружаем файлы из БД
        $File = new File();
        $file = $File->getByCodeRequestAndType($request['code'], $this->docFileType[1]);
        //для агента код аккредитации
        Yii::$app->accreditationFile->accreditationByCode($request['code']);
        $file = array_merge($file, $File->getByCodeRequestAndType($request['code'], $this->docFileType[2]));

        if ( empty( $file ) ) {
            throw new \Exception( 'Второй комплект документов ещё не сформирован' );
        }
        //Создаем каталог под файлы
        if ( ! ( $print_link_dir = Yii::$app->params['agent']['print_link_dir'] )
             || ! preg_match( '/^\/.*\/$/', $print_link_dir )
        ) {
            throw new \Exception( 'Ошибка на стороне сервера, не указаны или указаны некорректно параметры каталога для хранения изображений' );
        }
        $fileDir = Yii::getAlias( '@webroot' ) . $print_link_dir . $request['code'];
        if ( empty( FileHelper::createDirectory( $fileDir ) ) ) {
            throw new \Exception( "Не удалось создать каталог на сервере для файлов" );
        };
        //$existedImages = FileHelper::findFiles( $fileDir, [ 'only' => [ '*.jpg' ] ] );
        //каталог для img - $fileDir
        $filesJpg = [];
        foreach ( $file as $f ) {

            $fileJpg = [];
            $namepdf = Yii::getAlias( '@webroot' ) . '/' . $f['path'];
            $i       = 0;
            $imagick = new Imagick();
            $name    = '';
            while ( $i < 100 ) {
                try {
                    $imagick->setResolution( 300, 300 );
                    $imagick->readImage( $namepdf . "[$i]" );
                    $name     = basename( $namepdf ) . "$i.jpg";
                    $imgArr[] = $name;
                    $imagick->writeImage( $fileDir . '/' . $name );
                } catch ( \Exception $e ) {
                    break;
                }
                array_push( $filesJpg, [
                    'printLinkExpired' => strtotime( $request['print_link_expired'] ),
                    'fileName'         => $name,
                    'filePath'         => $print_link_dir . $request['code'] . '/' . $name,
                ] );
                $i = $i + 1;
            }
        }

        //todo[hdcy]: 11.04.2018 Добавить очистку файлов из каталога print_link по заявке если повторно загружен второй пакет документов.
        return $this->getClassAppResponse()::get( $filesJpg );
	}

	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}
}
