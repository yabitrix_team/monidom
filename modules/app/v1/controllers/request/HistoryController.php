<?php

namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\History;
use app\modules\app\v1\models\Message;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use app\modules\app\v1\models\Users;
use app\modules\notification\models\MessageType;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class HistoryController - контроллер для работы с историей заявок
 *
 * @package app\modules\app\v1\controllers
 */
class HistoryController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения истории всех заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
	public function actionIndex() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        //Получаем ID всех заявок по точке пользователя
        $request = new Requests();
        $request->setDefaultSelectFields( [ 'id' ] );
        $requestIdList = $request->getAllByFilter( [
                                                       'point_id' => ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'],
                                                   ],
                                                   [ 'created_at' => 'DESC' ]
        );
        //Получаем статусы по всем заявкам
        $statusIdList = [];
        foreach ( $requestIdList as $id ) {
            $status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
            array_push( $statusIdList, empty( $status ) ? [] : $status );
        }
        //Оставляем только нужные нам статусы
        $statuses       = Statuses::instance()->getAll( true );
        $statusesActual = [];
        foreach ( $statuses as $item ) {
            array_push( $statusesActual, $item['id'] );
        }
        //Оставляем все заявки со статусом и с пустым статусом (его отсутствием)
        //результирующий список заявок
        $requestIdActual = [];
        //результьтирующий список статусов
        $statusesIdActual = [];
        for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
            //тут указываем какие статусы из всех и если нужно указываем что нужны заявки без статуса
            if ( empty( $statusIdList[ $i ] ) || in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
                array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
                array_push( $statusesIdActual, $statusIdList[ $i ] );
            }
        }
        //Обогащаем заявки
        $history = new History();
        if ( ! $history->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
        } elseif ( ! $history->validate() ) {
            return $this->getClassAppResponse()::get( false, $history->firstErrors );
        }
        //Получаем заявки по ID с учетом пагинации
        $result = $history->getRequestWithPagination( $requestIdActual );
        //Чистим массив статусов нужно убрать пустые значения
        $statusesIdForMerg = [];
        foreach ( $statusesIdActual as $item ) {
            if ( $item != [] ) {
                array_push( $statusesIdForMerg, $item );
            }
        }
        //Обогащаем заявки статусами
        $result = array_map( function( $v ) use ( $statusesIdForMerg ) {

            $column = array_column( $statusesIdForMerg, 'request_id' );
            $key    = array_search( $v['id'], $column );
            if ( $key === false ) {
                $v['statuses_id'] = '';
            } else {
                $v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];
            }
            if ( ! empty( $v['updated_at'] ) ) {
                $v['updated_at'] = Yii::$app->formatter->asDatetime( $v['updated_at'] );
            }

            return $v;
        }, $result );
        //Обогащаем заявки message
        $result = array_map( function( $v ) {

            $messages = Message::instance()->getByRequestID( $v['id'] );
            if ( ! empty( $messages ) ) {
                $arr           = [];
                $v['messages'] = [];
                /**
                 * в массив добавляются типы сообщений, которые не надо выводить в истории и Message.php line 264
                 */
                foreach(Yii::$app->params['core']['notification']['exclude_types'] as $key=>$value){
                    $MessageTypeId[] = $key;
                }

                foreach ( $messages as $item ) {
                    if ( !in_array($item['type_id'], $MessageTypeId)) {
                        $arr['message_id']         = $item['id'];
                        $arr['message_code']       = $item['code'];
                        $arr['message']            = $item['message'];
                        $arr['type_id']            = $item['type_id'];
                        $arr['status_id']          = $item['status_id'];
                        $arr['message_created_at'] = Yii::$app->formatter->asDatetime( $item['created_at'] );
                        array_push( $v['messages'], $arr );
                    }
                }
                if ( count( $v['messages'] ) == 0 ) {
                    unset( $v['messages'] );
                };
            }

            return $v;
        }, $result );

        return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $requestIdActual ),
                                                  ] );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}
}
