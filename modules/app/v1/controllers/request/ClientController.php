<?php

namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Clients;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use app\modules\app\v1\models\Users;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class ClientController - контроллер для работы со списками заявок
 *
 * @package app\modules\app\v1\controllers
 */
class ClientController extends Controller {
	protected $requestInProgressGUID = [
		'7d06fa22-3be6-4066-a5c7-6a0c6e7bd055',
		'abfa1724-c3fd-4b7f-9631-7572288a165b',
		'623bbcc6-ac4c-4e51-bdcf-f7c5a12b9abd',
		'74ed5b21-af9b-44f3-bda5-85fde6847b76',
		'cbb42353-51e6-4a88-b2ef-575729da0343',
		'1b61eb3a-c293-4fca-8ee6-f672c43302ef',
		'e12788f0-7765-42bc-8ed3-828dab3fdf97',
	];
	protected $requestActiveGUID     = [
		'6818753f-7d4c-45e6-ad29-5f98db749508',
		'ff126df5-e381-4312-b4f3-8863314038b2',
	];
	protected $requestCloseGUID      = [
		'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e',
		'2da7f37a-0226-11e6-80f9-00155d01bf07',
		'c37fec4e-28b3-4239-8c2f-6812821b1b32',
		'e01a9627-195b-4422-860e-91ba96897840',
	];
	protected $requestWaitGUID       = [
		'769bd86a-cb49-490e-bbad-60db89c21c2a',
	];
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения всех заявок в работе
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionIndex() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        //Сразу делаем валидацию для проверки входных данных
        $clients = new Clients();
        if ( ! $clients->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
        } elseif ( ! $clients->validate() ) {
            return $this->getClassAppResponse()::get( false, $clients->firstErrors );
        }
        //Получаем ID всех заявок по точке пользователя
        $request = new Requests();
        $request->setDefaultSelectFields( [ 'id' ] );
        $requestIdList = $request->getAllByFilter( [
                                                       'point_id'     => ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'],
                                                       '>=created_at' => $clients->dateFrom . ' 00:00:00',
                                                       '<created_at'  => $clients->dateTo . '23:59:59',
                                                   ],
                                                   [ 'created_at' => 'DESC' ]
        );
        //Получаем статусы по всем заявкам
        $statusIdList = [];
        foreach ( $requestIdList as $id ) {
            $status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
            array_push( $statusIdList, empty( $status ) ? [] : $status );
        }
        //Оставляем только нужные нам статусы
        $statuses       = Statuses::instance()->getAllByFilter( [ 'guid' => $this->requestInProgressGUID ] );
        $statusesActual = array_column( $statuses, 'id' );
        //Оставляем заявки только со статусом Actual и с пустым статусом
        //результирующий список заявок
        $requestIdActual = [];
        //результьтирующий список статусов
        $statusesIdActual = [];
        for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
            //тут указываем какие статусы из актуальных и если нужно указываем что нужны заявки без статуса
            if ( empty( $statusIdList[ $i ] ) || in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
                array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
                array_push( $statusesIdActual, $statusIdList[ $i ] );
            }
        }
        if ( count( $requestIdActual ) == 0 ) {
            return $this->getClassAppResponse()::get( [
                                                          'requests'      => [],
                                                          'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                          'total count'   => 0,
                                                      ] );
        }
        //Обогащаем заявки
        //Получаем заявки по ID с учетом пагинации
        $result = $clients->getRequestWithPagination( $requestIdActual );
        //Чистим массив статусов нужно убрать пустые значения
        $statusesIdForMerg = [];
        foreach ( $statusesIdActual as $item ) {
            if ( $item != [] ) {
                array_push( $statusesIdForMerg, $item );
            }
        }
        //Обогащаем заявки статусами
        $result = array_map( function( $v ) use ( $statusesIdForMerg ) {

            $column = array_column( $statusesIdForMerg, 'request_id' );
            $key    = array_search( $v['id'], $column );
            if ( $key === false ) {
                $v['statuses_id'] = '';
            } else {
                $v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];
            }

            return $v;
        }, $result );

        return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $requestIdActual ),
                                                  ] );
	}

    /**
     * Экшен получения всех активных заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
	public function actionActive() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        //Сразу делаем валидацию для проверки входных данных
        $clients = new Clients();
        if ( ! $clients->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
        } elseif ( ! $clients->validate() ) {
            return $this->getClassAppResponse()::get( false, $clients->firstErrors );
        }
        //Получаем ID всех заявок по точке пользователя
        $request = new Requests();
        $request->setDefaultSelectFields( [ 'id' ] );
        $requestIdList = $request->getAllByFilter( [
                                                       'point_id'     => ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'],
                                                       '>=created_at' => $clients->dateFrom . ' 00:00:00',
                                                       '<created_at'  => $clients->dateTo . '23:59:59',
                                                   ],
                                                   [ 'created_at' => 'DESC' ]
        );
        //Получаем статусы по всем заявкам
        $statusIdList = [];
        foreach ( $requestIdList as $id ) {
            $status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
            array_push( $statusIdList, empty( $status ) ? [] : $status );
        }
        //Оставляем только нужные нам статусы
        $statuses       = Statuses::instance()->getAllByFilter( [ 'guid' => $this->requestActiveGUID ] );
        $statusesActual = array_column( $statuses, 'id' );
        //Оставляем заявки только со статусом Actual и с пустым статусом
        //результирующий список заявок
        $requestIdActual = [];
        //результьтирующий список статусов
        $statusesIdActual = [];
        for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
            //тут указываем какие статусы из актуальных и если нужно указываем что нужны заявки без статуса
            if ( in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
                array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
                array_push( $statusesIdActual, $statusIdList[ $i ] );
            }
        }
        if ( count( $requestIdActual ) == 0 ) {
            return $this->getClassAppResponse()::get( [
                                                          'requests'      => [],
                                                          'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                          'total count'   => 0,
                                                      ] );
        }
        //Обогащаем заявки
        //Получаем заявки по ID с учетом пагинации
        $result = $clients->getRequestWithPagination( $requestIdActual );
        //Чистим массив статусов нужно убрать пустые значения
        $statusesIdForMerg = [];
        foreach ( $statusesIdActual as $item ) {
            if ( $item != [] ) {
                array_push( $statusesIdForMerg, $item );
            }
        }
        //Обогащаем заявки статусами
        $result = array_map( function( $v ) use ( $statusesIdForMerg ) {

            $column           = array_column( $statusesIdForMerg, 'request_id' );
            $key              = array_search( $v['id'], $column );
            $v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];

            return $v;
        }, $result );

        return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $requestIdActual ),
                                                  ] );
	}

    /**
     * Экшен получения всех закрытых заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionClosed() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        //Сразу делаем валидацию для проверки входных данных
        $clients = new Clients();
        if ( ! $clients->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
        } elseif ( ! $clients->validate() ) {
            return $this->getClassAppResponse()::get( false, $clients->firstErrors );
        }
        //Получаем ID всех заявок по точке пользователя
        $request = new Requests();
        $request->setDefaultSelectFields( [ 'id' ] );
        $requestIdList = $request->getAllByFilter( [
                                                       'point_id'     => ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'],
                                                       '>=created_at' => $clients->dateFrom . ' 00:00:00',
                                                       '<created_at'  => $clients->dateTo . '23:59:59',
                                                   ],
                                                   [ 'created_at' => 'DESC' ]
        );
        //Получаем статусы по всем заявкам
        $statusIdList = [];
        foreach ( $requestIdList as $id ) {
            $status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
            array_push( $statusIdList, empty( $status ) ? [] : $status );
        }
        //Оставляем только нужные нам статусы
        $statuses       = Statuses::instance()->getAllByFilter( [ 'guid' => $this->requestCloseGUID ] );
        $statusesActual = array_column( $statuses, 'id' );
        //Оставляем заявки только со статусом Actual и с пустым статусом
        //результирующий список заявок
        $requestIdActual = [];
        //результьтирующий список статусов
        $statusesIdActual = [];
        for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
            //тут указываем какие статусы из актуальных и если нужно указываем что нужны заявки без статуса
            if ( in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
                array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
                array_push( $statusesIdActual, $statusIdList[ $i ] );
            }
        }
        if ( count( $requestIdActual ) == 0 ) {
            return $this->getClassAppResponse()::get( [
                                                          'requests'      => [],
                                                          'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                          'total count'   => 0,
                                                      ] );
        }
        //Обогащаем заявки
        //Получаем заявки по ID с учетом пагинации
        $result = $clients->getRequestWithPagination( $requestIdActual );
        //Чистим массив статусов нужно убрать пустые значения
        $statusesIdForMerg = [];
        foreach ( $statusesIdActual as $item ) {
            if ( $item != [] ) {
                array_push( $statusesIdForMerg, $item );
            }
        }
        //Обогащаем заявки статусами
        $result = array_map( function( $v ) use ( $statusesIdForMerg ) {

            $column           = array_column( $statusesIdForMerg, 'request_id' );
            $key              = array_search( $v['id'], $column );
            $v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];

            return $v;
        }, $result );

        return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $requestIdActual ),
                                                  ] );
	}

    /**
     * Экшен получения всех подвисших заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionWait() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        //Сразу делаем валидацию для проверки входных данных
        $clients = new Clients();
        if ( ! $clients->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
        } elseif ( ! $clients->validate() ) {
            return $this->getClassAppResponse()::get( false, $clients->firstErrors );
        }
        //Получаем ID всех заявок по точке пользователя
        $request = new Requests();
        $request->setDefaultSelectFields( [ 'id' ] );
        $requestIdList = $request->getAllByFilter( [
                                                       'point_id'     => ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'],
                                                       '>=created_at' => $clients->dateFrom . ' 00:00:00',
                                                       '<created_at'  => $clients->dateTo . '23:59:59',
                                                   ],
                                                   [ 'created_at' => 'DESC' ]
        );
        //Получаем статусы по всем заявкам
        $statusIdList = [];
        foreach ( $requestIdList as $id ) {
            $status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
            array_push( $statusIdList, empty( $status ) ? [] : $status );
        }
        //Оставляем только нужные нам статусы
        $statuses       = Statuses::instance()->getAllByFilter( [ 'guid' => $this->requestWaitGUID ] );
        $statusesActual = array_column( $statuses, 'id' );
        //Оставляем заявки только со статусом Actual и с пустым статусом
        //результирующий список заявок
        $requestIdActual = [];
        //результьтирующий список статусов
        $statusesIdActual = [];
        for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
            //тут указываем какие статусы из актуальных и если нужно указываем что нужны заявки без статуса
            if ( in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
                array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
                array_push( $statusesIdActual, $statusIdList[ $i ] );
            }
        }
        if ( count( $requestIdActual ) == 0 ) {
            return $this->getClassAppResponse()::get( [
                                                          'requests'      => [],
                                                          'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                          'total count'   => 0,
                                                      ] );
        }
        //Обогащаем заявки
        //Получаем заявки по ID с учетом пагинации
        $result = $clients->getRequestWithPagination( $requestIdActual );
        //Чистим массив статусов нужно убрать пустые значения
        $statusesIdForMerg = [];
        foreach ( $statusesIdActual as $item ) {
            if ( $item != [] ) {
                array_push( $statusesIdForMerg, $item );
            }
        }
        //Обогащаем заявки статусами
        $result = array_map( function( $v ) use ( $statusesIdForMerg ) {

            $column           = array_column( $statusesIdForMerg, 'request_id' );
            $key              = array_search( $v['id'], $column );
            $v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];

            return $v;
        }, $result );

        return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $requestIdActual ),
                                                  ] );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}
}
