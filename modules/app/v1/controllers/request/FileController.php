<?php
namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\Requests;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class FileController - контроллер для работы с файлами
 *
 * @package app\modules\app\v1\controllers
 */
class FileController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив данных заявки, предназначени для снижения запросов к БД
     *            при наследовании
     */
    protected $request;
    /**
     * @var - префикс имени файла
     */
    protected $prefixFileName;
    /**
     * @var - идентификатор добавленного файла
     */
    protected $lastInsertIdFile;
    /**
     * @var - корневая директория web
     */
    protected $webRoot;

    /**
     * Метод инициалиации компонента
     */
    public function init()
    {
        parent::init();
        $this->webRoot = rtrim(Yii::getAlias('@webroot'), '/');
    }

    /**
     * Метод экшен получения файла по коду файла
     *
     * @param $code - код файла
     *
     * @return array - вернет массив содержащий путь к файлу или ошибку
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        $path    = Yii::$app->file->get($code);
        $allPath = rtrim(Yii::getAlias('@webroot'), '/').'/'.ltrim($path, '/');
        if (!file_exists($allPath)) {
            throw new \Exception($path);
        }

        return $this->getClassAppResponse()::get(['url' => $path]);
    }

    /**
     * Метод экшен получения превью фотографии по коду файла
     *
     * @param $code - код файла
     *
     * @return array - вернет массив содержащий путь к превью или ошибку
     * @throws \Exception
     */
    public function actionPreviewByCode($code)
    {
        $pathPreview    = Yii::$app->file->getPreview($code);
        $allPathPreview = rtrim(Yii::getAlias('@webroot'), '/').'/'.ltrim($pathPreview, '/');
        if (!file_exists($allPathPreview)) {
            throw new \Exception($pathPreview);
        }

        return $this->getClassAppResponse()::get(['url' => $pathPreview]);
    }

    /**
     * Метод экшен для скачивания файлов(документов)
     *
     * @param $code - код файла
     *
     * @return Response|array - вернет бинарный файл или строку с исключением
     * @throws \Exception
     */
    public function actionDocByCode($code)
    {
        $file = (new File())->getByCode($code);
        if (empty($file)) {
            throw new \Exception('Указанный файл не зарегистрирован в системе');
        }
        $filePath = rtrim(Yii::getAlias('@webroot'), '/').'/'.ltrim($file['path'], '/');
        if (!file_exists($filePath)) {
            throw new \Exception('Физический файл отсутствует в системе');
        }

        return Yii::$app->response->sendFile($filePath, $file['name']);
    }

    /**
     * Метод экшен для получения архива документов по коду заявки
     *
     * @param $num  - номер пакета документов(варианты: 1 или 2)
     * @param $code - код заявки
     *
     * @return Response|array - вернет файл в бинарном виде или массив с ошибкой
     * @throws \Exception
     */
    public function actionZipByCode($num, $code)
    {
        // для агента
        if (substr($_SERVER['HTTP_X_FORWARDED_SERVER'], 0, 5) == 'agent') {
            $file = new File();
            $doc  = $file->getDocByRequestCode($code, $num);
            Yii::$app->accreditationFile->accreditationByCode($code);
            $doc = array_merge($doc, $file->getDocByRequestCode($code, 3));
        } else {
            $doc = (new File)->getDocByRequestCode($code, $num);
        }
        if (is_string($doc)) {
            throw new \Exception('При попытке получения архива документов возникла ошибка');
        }
        if (empty($doc)) {
            throw new \Exception('Невозможно получить архив документов, так как документы еще не сформирваны');
        }
        // Исключаем Анкета.pdf для Партнера из doc_pack_2
        $this->request = (new Requests())->getOneByFilter(['code' => $code]);
        if ($num == 2 && empty($this->request['is_pep'])) {
            foreach ($doc as $subKey => $subArray) {
                if ($subArray['name'] == 'Анкета.pdf') {
                    unset($doc[$subKey]);
                }
            }
        }
        $pathSaveFile = Yii::$app->params['core']['requests']['pathSaveFile'];
        if (empty($pathSaveFile)) {
            throw new \Exception(
                'Невозможно получить архив документов, так как в настройках некорректно указан путь сохранения файлов, обратитесь к администратору'
            );
        }
        $webRoot = rtrim(Yii::getAlias('@webroot'), '/');
        $zipPath = $webRoot.'/'.trim($pathSaveFile, '/').'/'.$code.'/doc_pack_'.$num.'/doc_pack_'.$num.'.zip';
        $zip     = new \ZipArchive();
        if ($zip->open($zipPath, \ZipArchive::CREATE) !== true) {
            throw new \Exception('Произошла ошибка при попытке создания архива, обратитесь к администратору');
        }
        foreach ($doc as $item) {
            $filePath = $webRoot.'/'.ltrim($item['path'], '/');
            if (file_exists($filePath)) {
                $zip->addFile($webRoot.'/'.ltrim($item['path'], '/'), $item['name']);
            } else {
                $zip->close();
                unlink($zipPath);
                throw new \Exception('При попытке создания архива не удалось найти физический файл '.$item['name']);
            }
        }
        $zip->close();
        if (!file_exists($zipPath)) {
            throw new \Exception('Произошла ошибка при попытке получения архива, обратитесь к администратору');
        }
        $result = Yii::$app->response->sendFile(Yii::getAlias($zipPath));
        unlink($zipPath);

        return $result;
    }

    /**
     * @param $code
     */
    public function actionAccreditationByCode($code)
    {
        //если файл уже записан - отправляем
        if (($doc = (new File)->getDocByRequestCode($code, 3))) {
            return Yii::$app->response->sendFile($this->webRoot.'/'.reset($doc)['path']);
        }
        $filePath = Yii::$app->accreditationFile->accreditationByCode($code);

        return Yii::$app->response->sendFile($filePath);
    }

    /**
     * Метод экшен удаления файла и превью по коду файла
     *
     * @param $code - код файла
     *
     * @return array - вернет массив содержащий результат удаления или ошибку
     * @throws \Exception
     */
    public function actionDelete($code)
    {
        $result = Yii::$app->file->delete($code);
        if (is_string($result)) {
            throw new \Exception($result);
        }

        return $this->getClassAppResponse()::get(['delete_file' => true]);
    }

    /**
     * Метод экшен для добавления файла
     * обрабатвает multipart/form-data, в котором должен содержаться файл
     * и два параметра code - код заявки, bind - привязка файла к заявке
     * допустимые значения в $this->arFileBind
     * По умолчанию для всех привязок выставляется active=1,
     * для привязки foto_extra выставляем active=0, необходимо для контроля
     * отображения не отправленных доп. фото в МП
     *
     * @return array - вернет массив содержащий код заявки или ошибку
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $bind    = Yii::$app->request->post("bind");
        $code    = Yii::$app->request->post("code");
        $modelRF = new RequestFile(['file_bind' => $bind]);
        if (empty($bind) || empty($modelRF->validate('file_bind'))) {
            throw new \Exception(reset($modelRF->firstErrors));//некорректная привязка файла
        }
        $modelRequestClass = $this->getModelRequest();
        $this->request     = (new $modelRequestClass)->getByCode($code);
        if (empty($this->request) || is_string($this->request)) {
            $msg = empty($this->request)
                ? 'Произошла ошибка добавления файла к заявке, некорректно указана код заявки либо заявка не принадлежит текущему пользователю, обратитесь к администратору'
                : $this->request;
            throw new \Exception($msg);
        }
        if (empty($_FILES)) {
            throw new \Exception('Не передан файл для его регистрации');
        }
        //получение данных файла
        $file = reset($_FILES);
        if ($error = $this->checkErrorFile($file)) {
            throw new \Exception($error);
        }
        $pathSaveFile = Yii::$app->params['core']['requests']['pathSaveFile'];
        if (empty($pathSaveFile)) {
            throw new \Exception(
                'Невозможно добавить файл к заявке, так как в настройках некорректно указан путь сохранения файлов, обратитесь к администратору'
            );
        }
        $path = trim($pathSaveFile, '/').'/'.$this->request['code'].'/'.$bind;
        //если привязка foto_extra, то выставляем active = 0 для файла
        $active                 = in_array($bind, [RequestFile::BIND_FOTO_EXTRA, RequestFile::BIND_FOTO_NOTIFICATION])
            ? 0 : 1;
        $resFileSave            = Yii::$app->file->save(
            $path,
            $file['name'],
            $file['tmp_name'],
            null,
            $this->prefixFileName,
            $active
        );
        $this->lastInsertIdFile = $resFileSave['id'];//для наследования
        //подготовка данных для записи в таблицу request_file
        $dataRequestFile  = [
            'request_id' => $this->request['id'],
            'file_id'    => $this->lastInsertIdFile,
            'file_bind'  => $bind,
        ];
        $modelRequestFile = new RequestFile();
        //связь заявки с файлами
        $modelRequestFile->load($dataRequestFile, '');
        if (!$modelRequestFile->validate()) {
            throw new \Exception('Произошла ошибка валидации данных при попытке привязать файл к заявке');
        }
        $modelRequestFile->create();

        return $this->getClassAppResponse()::get(['code' => $resFileSave['code']]);
    }

    /**
     * Метод проверки загрузки файла на наличие ошибки
     *
     * @param $file - массив содержащий данные файла
     *
     * @return bool|string - вернет ложь если без ошибок или вернет тест ошибки
     * @throws \Exception
     */
    protected function checkErrorFile($file)
    {
        if ($file['error'] > 0) {
            switch ($file['error']) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new \Exception(
                        'Размер загружаемого файла '.$file['name'].' превысил максимально допустимый размер'
                    );
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \Exception(
                        'Размер загружаемого файла '.$file['name'].' превысил максимально допустимое значение'
                    );
                case UPLOAD_ERR_PARTIAL:
                    throw new \Exception('Загружаемый файл '.$file['name'].' был получен только частично');
                case UPLOAD_ERR_NO_FILE:
                    throw new \Exception('Файл '.$file['name'].' не был загружен');
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new \Exception('Отсутствует временная папка для сохранения файла');
                case UPLOAD_ERR_CANT_WRITE:
                    throw new \Exception('Не удалось записать файл '.$file['name'].' на диск');
                case UPLOAD_ERR_EXTENSION:
                    throw new \Exception('PHP-расширение остановило загрузку файла '.$file['name']);
            }
        }

        return false;
    }

    /**
     * Метод экшен для отправки дополнительный фотографий по заявке
     *
     * @return array - вернет результат отправки или строку с исключением
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionExtra()
    {
        $comment = Yii::$app->request->post("comment");
        $code    = Yii::$app->request->post("code");
        if (strlen($code) != 32) {
            throw new \Exception(
                'При отправке дополнительной фотографии по заявке был некорректно указан код заявки'
            );
        }
        $modelRequestClass = $this->getModelRequest();
        $this->request     = (new $modelRequestClass)->getByCode($code);
        if (empty($this->request) || is_string($this->request)) {
            $msg = empty($this->request)
                ? 'Произошла ошибка отправки дополнительной фотографии по заявке, некорректно указан код заявки, либо заявка не принадлежит текущему пользователю, обратитесь к администратору'
                : $this->request;
            throw new \Exception($msg);
        }
        if (empty($_FILES)) {
            throw new \Exception('Не передан файл дополнительной фотографии');
        }
        //получение данных файла
        $file = reset($_FILES);
        $this->checkErrorFile($file);
        $pathSaveFile = Yii::$app->params['core']['requests']['pathSaveFile'];
        if (empty($pathSaveFile)) {
            throw new \Exception(
                'Невозможно отправить дополнительные фотографии, так как в настройках некорректно указан путь сохранения файлов, обратитесь к администратору'
            );
        }
        $path        = trim($pathSaveFile, '/').'/'.$this->request['code'].'/extra';
        $resFileSave = Yii::$app->file->save($path, $file['name'], $file['tmp_name']);
        //подготовка данных для записи в таблицу request_file
        $dataRequestFile  = [
            'request_id' => $this->request['id'],
            'file_id'    => $resFileSave['id'],
            'file_bind'  => RequestFile::BIND_FOTO_EXTRA,
        ];
        $modelRequestFile = new RequestFile();
        $modelRequestFile->load($dataRequestFile, '');
        if (!$modelRequestFile->validate()) {
            throw new \Exception('Произошла ошибка валидации данных при попытке привязать файл к заявке');
        }
        $modelRequestFile->create();
        //получение полного пути добавленного файла
        $filePath = rtrim(Yii::getAlias('@webroot'), '/').'/'.trim($path, '/').'/'.$resFileSave['fileName'];
        if (!file_exists($filePath)) {
            throw new \Exception('Не удалось сохранить переданный файл');
        }
        //подготовка формата даты
        list($y, $m, $d) = explode('-', strstr($this->request['created_at'], 'T', true));
        $sentMail = Yii::$app->mailer->compose(
            'photo',
            [
                'surname'    => $this->request['client_last_name'],
                'name'       => $this->request['client_first_name'],
                'patronymic' => $this->request['client_patronymic'],
                'amount'     => number_format($this->request['summ'], 0, ',', ' '),
                'date'       => $d.'.'.$m.'.'.$y,
                'comment'    => $comment,
            ]
        )
            ->setFrom(Yii::$app->params['core']['email']['from'])
            ->setTo(Yii::$app->params['core']['email']['extraPhoto'])
            ->setSubject('Дополнительные фото')
            ->attach($filePath)
            ->send();
        if (!$sentMail) {
            //todo[echmaster]: 12.04.2018 в дальнейшем можно добавить логирование с указанием передаваемых данных
            throw new \Exception('Произошла ошибка при отправке дополнительных фотографий');
        }

        return $this->getClassAppResponse()::get(['sent' => true]);
    }

    /**
     * Метод оптравки дополнительных фотографий
     *
     * @return array|string
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionSend()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (empty($reqBody['code'])) {
            throw new \Exception(
                'Не указан код заявки при отправке дополнительных фотографий, обратитесь к администратору'
            );
        }
        $result = (new File())->getByCodeRequestAndType(
            $reqBody['code'],
            RequestFile::BIND_FOTO_EXTRA,
            [
                'file_id' => 'id',
                'path',
                'active',
            ],
            [
                'summ',
                'client_last_name',
                'client_first_name',
                'client_patronymic',
                'created_at',
            ]
        );
        //если результат не пустой, то проверяем и исключаем активные файлы
        empty($result)
            ?: $result = array_filter(
            array_map(
                function ($v) {
                    empty($v['active']) ?: $v = [];

                    return $v;
                },
                $result
            )
        );
        if (empty($result)) {
            throw new \Exception('Не удалось получить файлы для отправки по коду заявки, обратитесь к администратору');
        }
        $data = reset($result);
        //подготовка данных для отправки на почту
        $mail  = Yii::$app->mailer->compose(
            'photo',
            [
                'surname'    => $data['client_last_name'],
                'name'       => $data['client_first_name'],
                'patronymic' => $data['client_patronymic'],
                'amount'     => number_format($data['summ'], 0, ',', ' '),
                'date'       => (new \DateTime($data['created_at']))->format('d.m.Y H:i:s'),
                'comment'    => !empty($reqBody['comment']) ? $reqBody['comment'] : '',
            ]
        )
            ->setFrom(Yii::$app->params['core']['email']['from'])
            ->setTo(Yii::$app->params['core']['email']['extraPhoto'])
            ->setSubject('Дополнительные фото');
        $files = array_column($result, 'path');
        foreach ($files as $item) {
            $filePath = rtrim(Yii::getAlias('@webroot'), '/').'/'.trim($item, '/');
            if (!file_exists($filePath)) {
                throw new \Exception(
                    'Не удалось отправить файлы, ошибка получения файла для оптравки, обратитесь к администратору'
                );
            }
            $mail->attach($filePath);
        }
        $sentMail = $mail->send();
        if (!$sentMail) {
            throw new \Exception(
                'Произошла ошибка при отправке дополнительных фотографий, обратитесь к администратору'
            );
        }
        //проставляем активность записей после отправки
        $fileId = array_column($result, 'file_id');
        (new File())->updateActiveByListId($fileId);

        return $this->getClassAppResponse()::get(['sent' => true]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
