<?php

namespace app\modules\app\v1\controllers\request;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\RequestsEvents;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class CleanController - контроллер для очистки заявки для ЛКВМ
 *
 * @package app\modules\app\v1\controllers
 */
class CleanController extends Controller {
	/**
	 * @var string - корень сайта
	 */
	private $webRoot;
	/**
	 * @var - название сервера
	 */
	private $serverName;

	/**
	 * Инициализация компонента, происходит после загрузки конфигурации
	 */
	public function init() {

		parent::init();
		$this->webRoot    = rtrim( Yii::getAlias( '@webroot' ), '/' );
		$this->serverName = Yii::$app->getRequest()->serverName;
	}

	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения документов первого пакета
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */

	public function actionByCode( $code ) {

        $request = ( new Requests() )->getByCode( $code );
        if ( ! $request ) {
            throw new \Exception( 'Указанная заявка отсутствует в системе' );
        };

        return $this->getClassAppResponse()::get( [ 'success' => true ] );
	}
	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}

	protected function dataEncode( $data ) {

		$code = false;
		if ( ! empty( $data ) ) {
			$converter = [ '+' => '-', '/' => '_', '=' => '' ];
			$code      = strtr( base64_encode( serialize( $data ) ), $converter );
			//если параметр в URL длинее 255 символов и нет слэша, то закрывает доступ к странице
			if ( strlen( $code ) > 250 ) {
				$code = substr_replace( $code, "/", 250, 0 );
			}
		}

		return $code;
	}
}
