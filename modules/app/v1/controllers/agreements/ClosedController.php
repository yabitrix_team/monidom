<?php

namespace app\modules\app\v1\controllers\agreements;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Contracts;
use app\modules\app\v1\models\soap\cmr\AgreementsData;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class ClosedController - контроллер для работы с закрытыми договорами клиента
 *
 * @package app\modules\app\v1\controllers
 */
class ClosedController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	/**
	 * @var array - ассоциация полей верхнего уровня полученных от сервиса через SOAP с полями контракта
	 */
	protected $assocFieldsMain = [
		'Number'    => 'number',
		'Date'      => 'date',
		'Summ'      => 'summ',
		'CloseDate' => 'closeDate',
		'Chart'     => 'chart',
	];
	/**
	 * @var array - ассоциация полей нижнего уровня(график) полученных от сервиса через SOAP с полями контракта
	 */
	protected $assocFieldsChart = [
		'Date' => 'date',
		'Summ' => 'summ',
	];
	/**
	 * @var string - формат даты для метода asDate
	 *             варанты: "short", "medium", "long", "full"
	 */
	protected $dateFormat = 'long';

    /**
     * Экшен для получения закрытых договоров авторизованного пользователя
     * проверка на авторизацию выставляется в дочерних контроллерах в силу того,
     * что для разных модулей разная реализация авторизации
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
	public function actionIndex() {

        Yii::$app->formatter->locale = 'ru-RU';
        $data                        = [];
        $codeContracts               = ( new Contracts() )->getClosedCodeByUserId();
        if ( is_string( $codeContracts ) ) {
            throw new \Exception( 'Не удалось получить закрытые договора пользователя, обратитесь к администратору' );
        }
        if ( ! empty( $codeContracts ) ) {
            $model = new AgreementsData( [ 'scenario' => AgreementsData::SCENARIO_CLOSED_AGREEMENTS_INFO ] );
            $model->load( [ 'code' => $codeContracts ], '' );
            $res = $model->closedAgreementsInfo();
            if ( is_a( $res, AgreementsData::class ) ) {
                return $this->getClassAppResponse()::get( false, $res->firstErrors );
            }
            if ( is_a( $res, \Exception::class ) ) {
                throw $res;
            }
            if ( ! empty( $res ) && ! empty( $this->assocFieldsMain ) ) {
                $agreements = isset( $res->Number ) ? [ $res ] : $res;
                foreach ( $agreements as $key => $item ) {
                    $chart = [];
                    if ( ! empty( $item->PaymentsList ) && ! empty( $this->assocFieldsChart ) ) {

                        $payments = isset( $item->PaymentsList->Date ) ? [ $item->PaymentsList ] :
                            $item->PaymentsList;
                        foreach ( $payments as $k => $v ) {

                            ! isset( $this->assocFieldsChart['Date'] ) ?:
                                $chart[ $k ][ $this->assocFieldsChart['Date'] ] = $v->Date;
                            ! isset( $this->assocFieldsChart['Summ'] ) ?:
                                $chart[ $k ][ $this->assocFieldsChart['Summ'] ] = floatval( $v->Summ );
                        }
                    } else {
                        $chart = $item->PaymentsList;
                    }
                    ! isset( $this->assocFieldsMain['Number'] ) ?:
                        $data[ $key ][ $this->assocFieldsMain['Number'] ] = $item->Number;
                    ! isset( $this->assocFieldsMain['Date'] ) ?:
                        $data[ $key ][ $this->assocFieldsMain['Date'] ] = Yii::$app->formatter->asDate( $item->Date, $this->dateFormat );
                    ! isset( $this->assocFieldsMain['Summ'] ) ?:
                        $data[ $key ][ $this->assocFieldsMain['Summ'] ] = intval( $item->Summ );
                    ! isset( $this->assocFieldsMain['CloseDate'] ) ?:
                        $data[ $key ][ $this->assocFieldsMain['CloseDate'] ] = Yii::$app->formatter->asDate( $item->CloseDate, $this->dateFormat );
                    ! isset( $this->assocFieldsMain['Chart'] ) ?:
                        $data[ $key ][ $this->assocFieldsMain['Chart'] ] = $chart;
                }
            } else {
                $data = $res;
            }
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}
