<?php
namespace app\modules\app\v1\controllers\agreements;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Contracts;
use app\modules\app\v1\models\soap\cmr\AgreementsData;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class ActiveController - контроллер для работы с активными договорами клиента
 *
 * @package app\modules\app\v1\controllers
 */
class ActiveController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - ассоциация верхнеуровневых полей полученных от сервиса через SOAP с полями контракта
     */
    protected $assocFieldsMain = [
        'Number'            => 'number',
        'Date'              => 'date',
        'Summ'              => 'summ',
        'MobilePaymentSumm' => 'currentSumm',
        'NextPaymentDate'   => 'currentDate',
        'FullPaymentSumm'   => 'remainSumm',
        'PSK'               => 'psk',
        'Chart'             => 'chart',
    ];
    /**
     * @var array - ассоциация полей нижнего уровня(график) полученных от сервиса через SOAP с полями контракта
     */
    protected $assocFieldsChart = [
        'Date'        => 'date',
        'PaymentSumm' => 'summ',
        'Debt'        => 'debt',
        'Percents'    => 'percents',
        'DebtRest'    => 'amount',
    ];
    /**
     * @var string - формат даты для метода asDate
     *             варанты: "short", "medium", "long", "full"
     *             так же можно указать формат вида: "dd.MM.yyyy"
     */
    protected $dateFormat = 'long';
    /**
     * @var bool - изменять ли дату в графике платежей
     */
    protected $fixDateChart = false;

    /**
     * Экшен получения активных договоров авторизованного пользователя
     * проверка на авторизацию выставляется в дочерних контроллерах в силу того,
     * что для разных модулей разная реализация авторизации
     *
     * @return array - вернет результирующий массив
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        $data                        = [];
        $codeContracts               = (new Contracts())->getActiveCodeByUserId();
        if (is_a($codeContracts, \Exception::class)) {
            throw new \Exception('Не удалось получить активные договора пользователя, обратитесь к администратору');
        }
        if (!empty($codeContracts)) {
            $model = new AgreementsData(['scenario' => AgreementsData::SCENARIO_ACTIVE_AGREEMENTS_INFO]);
            $model->load(['code' => $codeContracts], '');
            $res = $model->activeAgreementsInfov4();
            if (is_a($res, AgreementsData::class)) {
                return $this->getClassAppResponse()::get(false, $res->firstErrors);
            }
            if (is_a($res, \Exception::class)) {
                throw $res;
            }
            if (!empty($res) && !empty($this->assocFieldsMain)) {
                $agreements = isset($res->Number) ? [$res] : $res;
                $data       = $this->prepareResult($agreements);
            } else {
                $data = $res;
            }
        }

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен получения активного договора по его номеру(коду)
     *
     * @param $code - номер(код) договора
     *
     * @return array - вернет результирующий массив
     * @throws \yii\base\InvalidConfigException
     */
    public function actionByCode($code)
    {
        Yii::$app->formatter->locale = 'ru-RU';
        $data                        = [];
        $model                       =
            new AgreementsData(['scenario' => AgreementsData::SCENARIO_ACTIVE_AGREEMENTS_INFO]);
        $model->load(['code' => $code], '');
        $res = $model->activeAgreementsInfov4();
        if (is_a($res, AgreementsData::class)) {
            return $this->getClassAppResponse()::get(false, $res->firstErrors);
        }
        if (is_a($res, \Exception::class)) {
            throw $res;
        }
        if (!empty($res) && !empty($this->assocFieldsMain)) {
            $agreements = isset($res->Number) ? [$res] : $res;
            $data       = $this->prepareResult($agreements);
        } else {
            $data = $res;
        }

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод подготовки результирующих данных для веб-сервиса
     *
     * @param array $agreements - массив контрактов полученных из AgreementsData
     *
     * @return array|string - вернет результирующий массив в соответствии с контрактом
     *                      или строку в случае исключения
     * @throws \yii\base\InvalidConfigException
     */
    protected function prepareResult(array $agreements)
    {
        $data = [];
        foreach ($agreements as $key => $item) {
            $chart = [];
            if (!empty($item->Chart) && !empty($this->assocFieldsChart)) {
                foreach ($item->Chart as $k => $v) {
                    !isset($this->assocFieldsChart['Date'])
                        ?:
                        $chart[$k][$this->assocFieldsChart['Date']] = empty($this->fixDateChart)
                            ? $v->Date
                            : Yii::$app->formatter->asDate($v->Date, $this->dateFormat);
                    !isset($this->assocFieldsChart['PaymentSumm'])
                        ?: $chart[$k][$this->assocFieldsChart['PaymentSumm']] = (float) $v->PaymentSumm;
                    !isset($this->assocFieldsChart['Debt'])
                        ?: $chart[$k][$this->assocFieldsChart['Debt']] = (float) $v->Debt;
                    !isset($this->assocFieldsChart['Percents'])
                        ?: $chart[$k][$this->assocFieldsChart['Percents']] = (float) $v->Percents;
                    !isset($this->assocFieldsChart['DebtRest'])
                        ?: $chart[$k][$this->assocFieldsChart['DebtRest']] = (float) $v->DebtRest;
                }
            } else {
                $chart = $item->Chart;
            }
            !isset($this->assocFieldsMain['Number'])
                ?: $data[$key][$this->assocFieldsMain['Number']] = $item->Number;
            !isset($this->assocFieldsMain['Date'])
                ?: $data[$key][$this->assocFieldsMain['Date']] =
                Yii::$app->formatter->asDate($item->Date, $this->dateFormat);
            !isset($this->assocFieldsMain['Summ'])
                ?: $data[$key][$this->assocFieldsMain['Summ']] = (int) $item->Summ;
            !isset($this->assocFieldsMain['MobilePaymentSumm'])
                ?: $data[$key][$this->assocFieldsMain['MobilePaymentSumm']] = (float) $item->MobilePaymentSumm;
            !isset($this->assocFieldsMain['NextPaymentSumm'])
                ?: $data[$key][$this->assocFieldsMain['NextPaymentSumm']] = (float) $item->NextPaymentSumm;
            !isset($this->assocFieldsMain['NextPaymentDate'])
                ?: $data[$key][$this->assocFieldsMain['NextPaymentDate']] =
                Yii::$app->formatter->asDate($item->NextPaymentDate, $this->dateFormat);
            !isset($this->assocFieldsMain['FullPaymentSumm'])
                ?: $data[$key][$this->assocFieldsMain['FullPaymentSumm']] = (float) $item->FullPaymentSumm;
            !isset($this->assocFieldsMain['Annuity'])
                ?: $data[$key][$this->assocFieldsMain['Annuity']] = (float) $item->Annuity;
            !isset($this->assocFieldsMain['Percent'])
                ?: $data[$key][$this->assocFieldsMain['Percent']] = (float) $item->Percent;
            !isset($this->assocFieldsMain['PSK'])
                ?: $data[$key][$this->assocFieldsMain['PSK']] = (float) $item->PSK;
            !isset($this->assocFieldsMain['Chart'])
                ?: $data[$key][$this->assocFieldsMain['Chart']] = $chart;
        }

        return $data;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
