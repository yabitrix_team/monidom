<?php
namespace app\modules\app\v1\controllers\user;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\SmsConfirm;
use Exception;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class ResetController - для сброса пароля пользователя
 *
 * @package app\modules\app\v1\controllers
 */
class ResetController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }

    /**
     * Запрос на сброс пароля пользователя
     *
     * @return array
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            $reqBody  = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $smsModel = (new SmsConfirm())->getOneByFilter(
                [
                    'login' => $reqBody['login'],
                    'code'  => $reqBody['code'],
                ]
            );
            if ($smsModel) {
                if (!$reqBody['password']) { // чекнули, но пароль еще не вводили
                    return $this->getClassAppResponse()::get(['exist' => true]);
                } else { // а здесь уже чекаем и меняем пароль
                    //Проверяем пользователя по логину, должен быть Активным
                    $userCheck = Users::instance()->getStaticByLogin($reqBody['login']);
                    if (empty($userCheck['id'])) {
                        throw new Exception("Учетная запись деактивирована. Обратитесь к администратору");
                    }
                    $model = new Users();
                    $model->hashPassword($reqBody['password']);
                    if (
                    (new Users())->updateByUsername(
                        $reqBody['login'],
                        [
                            'password_hash' => $model->password_hash,
                        ]
                    )
                    ) {
                        if (!(new SmsConfirm())->deleteByLogin($reqBody['login'])) {
                            throw new Exception('Произошла ошибка, попробуйте позже');
                        };
                        Log::info(
                            [
                                "user_id"      => Yii::$app->user->getId(),
                                "user_login"   => $reqBody["login"],
                                "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                                "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                                "text"         => "Пароль успешно изменен",
                            ],
                            "user.password"
                        );

                        return $this->getClassAppResponse()::get(['success' => true]);
                    } else {
                        Log::error(
                            [
                                "user_id"      => Yii::$app->user->getId(),
                                "user_login"   => $reqBody["login"],
                                "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                                "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                                "text"         => "Произошла ошибка при смене пароля, обратитесь к администратору",
                            ],
                            "user.password"
                        );

                        return $this->getClassAppResponse()::get(
                            false,
                            "Произошла ошибка при смене пароля, обратитесь к администратору"
                        );
                    }
                }
            } else {
                Log::error(
                    [
                        "user_id"      => Yii::$app->user->getId(),
                        "user_login"   => $reqBody["login"],
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => "Мы не смогли проверить код, обратитесь к администратору",
                    ],
                    "user.password"
                );

                return $this->getClassAppResponse()::get(
                    false,
                    "Мы не смогли проверить код, обратитесь к администратору"
                );
            }
        } catch (Exception $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.password"
            );
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Экшен проверки существования логина (номер телефона) пользователя в базе данных
     *
     * @param $login
     *
     * @return array
     * @throws \Exception
     */
    public function actionByLogin($login)
    {
        try {
            $result = Users::getByLogin($login);
            if (empty($result) || $result == false) {
                usleep(rand(164, 260)); // вовино зелье от черных хакеров
                Log::error(
                    [
                        "user_id"      => Yii::$app->user->getId(),
                        "user_login"   => $login,
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => "Пользователь не существует",
                    ],
                    "user.password"
                );

                return $this->getClassAppResponse()::get(['exist' => true]);
            } elseif (is_array($result)) {
                $model = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CREATE]);
                $model->load(['login' => $login], '');
                if ($model->validate()) {
                    //проверка блокировки отправки смс
                    $model->checkLockByLogin();
                    $model->generateSmsCode();
                    $result = $model->save();
                    if (is_numeric($result)) {
                        $smsText  = 'Код подтверждения для восстановления пароля: '.$model->code;
                        $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
                        $modelSms->load(
                            [
                                'phone'   => $model->login,
                                'message' => $smsText,
                            ],
                            ''
                        );
                        $modelSms->setLogCategory('sms.confirm.reset')->sendData();
                        Log::info(
                            [
                                "user_id"      => Yii::$app->user->getId(),
                                "user_login"   => $model->login,
                                "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                                "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                                "text"         => "Пользователь существует",
                            ],
                            "user.password"
                        );

                        return $this->getClassAppResponse()::get(['exist' => true]);
                    }
                }
            } elseif (is_string($result)) {
                Log::error(
                    [
                        "user_id"      => Yii::$app->user->getId(),
                        "user_login"   => $login,
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => $result,
                    ],
                    "user.password"
                );

                return $this->getClassAppResponse()::get(false, false, $result);
            }
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $login,
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => "Неизвестная ошибка: {$result}. Обратитесь к администратору",
                ],
                "user.password"
            );

            return $this->getClassAppResponse()::get(
                false,
                false,
                "Неизвестная ошибка: {$result}. Обратитесь к администратору"
            );
        } catch (\Exception $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $login,
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.password"
            );
            throw $e;
        }
    }
}
