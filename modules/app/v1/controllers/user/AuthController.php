<?php
namespace app\modules\app\v1\controllers\user;

use app\modules\app\v1\classes\exceptions\AuthException;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Cabinets;
use app\modules\app\v1\models\Groups;
use app\modules\app\v1\models\UserGroup;
use app\modules\app\v1\models\Users;
use app\modules\client\v1\models\AuthAttempt;
use app\modules\panel\v1\models\AuthLink;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;
use app\modules\app\v1\classes\logs\Log;
use Exception;

/**
 * Class AuthController - контроллер для работы со справочником аутентификаций
 *
 * @package app\modules\app\v1\controllers
 */
class AuthController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Аутентификация пользователя
     *
     * @return array|mixed|\yii\web\User
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            //-------------------------------------------------------------------------------------------------------------------
            //-------------------Авторизация по COOKIE
            //-------------------------------------------------------------------------------------------------------------------
            $reqBody         = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $requestUsername = $reqBody["login"];
            $requestPassword = $reqBody["password"];
            if (empty($requestUsername) || empty($requestPassword)) {
                throw new AuthException("Имя пользователя или пароль не заданы");
            }
            $user = (new Users)->getStaticByLogin($requestUsername);
            if ($user['username'] == false) {
                throw new AuthException("Имя пользователя или пароль указаны не верно");
            }
            $result      = Yii::$app->security->validatePassword($requestPassword, $user['password_hash']);
            $authAttempt = new AuthAttempt();
            $attemptInfo = $authAttempt->getByUserId($user['id']);
            if ($attemptInfo['failed'] >= 5 && (int)$attemptInfo['rest'] > 0) {
                return [
                    "error" => [
                        "message" => "Ваша учетная запись была заблокирована на 15 минут",
                        "time" => $attemptInfo['rest']
                    ],
                    "data" => [],
                ];
            }
            if ($result == false) {
                if ($attemptInfo) {
                    $authAttempt->scenario   = AuthAttempt::SCENARIO_UPDATE;
                    $authAttempt->failed     = ++$attemptInfo['failed'];
                    $authAttempt->updated_at = date("Y-m-d H:i:s");
                    if (!$authAttempt->validate()) {
                        $authAttempt->logValidateError($user);
                    }
                    $authAttempt->update($attemptInfo['id']);
                    if ($attemptInfo['failed'] >= 5 ) {
                        Log::error(
                            [
                                "text"       => "Превышено количество попыток авторизации: ".
                                                print_r($attemptInfo, 1),
                                "user_id"    => $user["id"],
                                "user_login" => $user["username"],
                            ],
                            "user.login.attempt"
                        );
                        return [
                            "error" => [
                                "message" => "Ваша учетная запись была заблокирована на 15 минут",
                                "time" => 15 * 60
                            ],
                            "data" => [],
                        ];
                    }
                } else {
                    $authAttempt->scenario = AuthAttempt::SCENARIO_CREATE;
                    $authAttempt->user_id  = $user['id'];
                    $authAttempt->failed   = 1;
                    if (!$authAttempt->validate()) {
                        $authAttempt->logValidateError($user);
                    }
                    $authAttempt->create();
                }
                throw new AuthException("Имя пользователя или пароль указаны не верно");
            }
            if ($attemptInfo) {
                $authAttempt->scenario   = AuthAttempt::SCENARIO_UPDATE;
                $authAttempt->failed     = 0;
                $authAttempt->updated_at = date("Y-m-d H:i:s");
                if (!$authAttempt->validate()) {
                    $authAttempt->logValidateError($user);
                }
                $authAttempt->update($attemptInfo['id']);
            } else {
                $authAttempt->scenario = AuthAttempt::SCENARIO_CREATE;
                $authAttempt->user_id  = $user['id'];
                $authAttempt->failed   = 0;
                if (!$authAttempt->validate()) {
                    $authAttempt->logValidateError($user);
                }
                $authAttempt->create();
            }
            $resultPriorCabinetId =
                (new Groups())->getGroupByUserId($user['id']); //Получили группу пользователя из таблицы user_group
            if (empty($resultPriorCabinetId)) {
                throw new AuthException("У пользователя не установлена группа");
            }
            $cabinetCode = ((new Cabinets())->getOne($resultPriorCabinetId[0]['prior_cabinet_id']))['code'];
            if (empty($cabinetCode)) {
                throw new AuthException(
                    "У группы пользователя не установлена ссылка на личный кабинет. Обратитесь к администратору"
                );
            }
            $resultYiiLogin = Yii::$app->user->login($user, 60 * 60 * 24 * 30); //Результат логина в YII
            if (!$resultYiiLogin) {
                throw new AuthException("Не удалось авторизовать пользователя. Обратитесь к администратору");
            }
            $result = [
                'authorized'   => $resultYiiLogin,
                'cabinet_code' => $cabinetCode,
            ];
            /**
             * регистрируем авторизацию для возможности поднять сокет соединение
             */
            /** @var array $ticket */
            $ticket         = Yii::$app->notify->createTicket($user['id']);
            $result["code"] = $ticket['code'];
            Log::info(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                ],
                "user.login"
            );

            return $this->getClassAppResponse()::get($result);
        } catch (AuthException $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.login"
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (Exception $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.login"
            );
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Проверка аутинтефикации пользователя
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $userState = Yii::$app->user->getIsGuest();
        if ($userState == true) {
            return $this->getClassAppResponse()::get(['authorized' => !$userState]);
        }
        $user = Users::instance()->getOneByFilter(
            [
                'id'     => Yii::$app->user->getId(),
                'active' => 1,
            ]
        );
        if ($user == false) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                "Учетная запись деактивированна. Обратитесь к администратору."
            );
        }

        return $this->getClassAppResponse()::get(['authorized' => true]);
    }

    /**
     * Деаутинтефикация пользователя
     *
     * @return array
     * @throws \Throwable
     */
    public function actionDelete()
    {
        try {
            $idUser          = Yii::$app->user->getId();
            $login           = Yii::$app->user->getIdentity()->getUsername();
            $resultYiiLogout = Yii::$app->user->logout(true);
            if ($resultYiiLogout) {
                Log::info(
                    [
                        "user_id"      => $idUser,
                        "user_login"   => $login,
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    ],
                    "user.logout"
                );

                return $this->getClassAppResponse()::get(['authorized' => 'false']);
            }
            throw new AuthException ('Не удалось деаутентифицировать пользователя');
        } catch (AuthException $e) {
            Log::error(
                [
                    "user_id"      => $idUser,
                    "user_login"   => $login,
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.logout"
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (Exception $e) {
            Log::error(
                [
                    "user_id"      => $idUser,
                    "user_login"   => $login,
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => $e->getMessage(),
                ],
                "user.logout"
            );
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param $code
     *
     * @return array
     * @throws \Throwable
     */
    public function actionByCode($code)
    {
        try {
            if (
            !$arAuthLink = AuthLink::instance()->getOneByFilter(
                [
                    "hash"        => $code,
                    "active"      => 1,
                    ">created_at" => (new \DateTime())->modify('-5 min')
                        ->format('Y-m-d H:i:s'),
                ]
            )
            ) {
                throw new AuthException("Ссылка авторизации не найдена, уже использована или просрочена");
            }
            if (!AuthLink::instance()->update($arAuthLink["id"], ["active" => 0])) {
                throw new AuthException("Ошибка обновления статуса ссылки авторизации");
            }
            $user = new Users();
            $user->setAttributes(Users::instance()->getOne($arAuthLink["user_id"]));
            if (!Yii::$app->user->login($user, 60 * 60 * 24 * 30)) {
                throw new AuthException("Не удалось авторизовать пользователя");
            }
            $ticket = Yii::$app->notify->createTicket($arAuthLink['user_id']);
            if (!$ticket || !$ticket["code"]) {
                throw new AuthException("Не удалось установить сокет");
            }
            setcookie(
                'code',
                $ticket["code"],
                time() + 86400 * 30,
                '/',
                '.'.Yii::$app->params['core']['domain'],
                false,
                false
            );
            if (!$arUserGroup = UserGroup::instance()->getOneByFilter(["user_id" => $arAuthLink['user_id']])) {
                throw new AuthException("Не удалось получить роль пользователя");
            }
            if (!$arGroup = Groups::instance()->getOneByFilter(["id" => $arUserGroup['group_id']])) {
                throw new AuthException("Не удалось получить группу пользователя");
            }
            if (!$arCabinet = Cabinets::instance()->getOneByFilter(["id" => $arGroup['prior_cabinet_id']])) {
                throw new AuthException("Не удалось определить кабинет пользователя");
            }
            Log::info(["text" => "hash: ".$code], "user.link.auth");
            Yii::$app->response->redirect(
                'http'.($_SERVER['HTTPS']
                    ? 's'
                    :
                    '').'://'.$arCabinet["subdomain"].'.'.Yii::$app->params['core']['domain'].'/'
            )
                ->send();

            return $this->getClassAppResponse()::get([$arAuthLink, $ticket]);
        } catch (AuthException $e) {
            Log::error(["text" => "hash: ".$code."\n".$e->getMessage()], "user.link.auth");

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (\Exception $e) {
            Log::error(["text" => "hash: ".$code."\n".$e->getMessage()], "user.link.auth");
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}
