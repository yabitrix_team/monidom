<?php
namespace app\modules\app\v1\controllers\user;

use app\modules\app\v1\classes\exceptions\RegisterException;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Register;
use app\modules\app\v1\models\Users;
use Exception;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class RegisterController - контроллер регистрации пользователя
 *
 * @package app\modules\app\v1\controllers
 */
class RegisterController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшн создания пользователя
     *
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if ($user = (new Users())->getOneByFilter(['username' => $reqBody["login"]])) {
                Log::info(
                    [
                        "user_login"   => $reqBody["login"],
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => "Пользователь уже зарегистрирован",
                    ],
                    "user.register.client"
                );

                return $this->getClassAppResponse()::get(
                    [
                        'userExists' => 1,
                        'firstName'  => $user['first_name'],
                        'phone'      => $user['username'],
                    ]
                );
            };
            $model = new Register();
            if (!$model->load($reqBody, '')) {
                throw new RegisterException("Не переданы данные для создания пользователя");
            }
            $error = false;
            if (!$model->validate()) {
                $error = $model->firstErrors;
            }
            if (empty($error)) {
                $result = $model->addUser();
                if (is_array($result)) {
                    $error = $result;
                } else {
                    $user    = (new Users)->getStaticByLogin($reqBody["login"]);
                    $resAuth = Yii::$app->user->login($user, 60 * 60 * 24 * 30);//аутентифицируем пользователя
                    if (!$resAuth) {
                        throw new RegisterException("Ошибка авторизации пользователя");
                    }
                }
            }
            if (empty($error) && empty($result)) {
                $error = "Неизвестная ошибка при созданиия пользователя. Обратитесь к администратору";
            }
            if (!empty($error)) {
                Log::error(
                    [
                        "user_id"      => Yii::$app->user->getId(),
                        "user_login"   => $reqBody["login"],
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => print_r($error, 1),
                    ],
                    "user.register.client"
                );

                return $this->getClassAppResponse()::get(false, $error);
            }
            if (!empty($result)) {
                /**
                 * регистрируем авторизацию для возможности поднять сокет соединение
                 */
                $ticket = Yii::$app->notify->createTicket(Yii::$app->user->getId());
                $result = [
                    'authorized' => $result,
                    'code'       => $ticket['code'],
                ];
                Log::info(
                    [
                        "user_id"      => Yii::$app->user->getId(),
                        "user_login"   => $reqBody["login"],
                        "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                        "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                        "text"         => "Тело запроса:\n".print_r($reqBody, true),
                    ],
                    "user.register.client"
                );
                //регистрация юзера в системе CRIB с последующим записью полученного crib_client_id нашему юзеру
                $cid          = strlen($_COOKIE[Yii::$app->params['crib']['cid_cookie']])
                    ? $_COOKIE[Yii::$app->params['crib']['cid_cookie']] : '';
                $cribClientID = Yii::$app->cribLKK->registryPush($reqBody['login'], $cid);
                if (!empty($cribClientID)) {
                    (new Users)->updateByUsername(
                        Yii::$app->user->identity->username,
                        ['crib_client_id' => $cribClientID]
                    );
                }

                return $this->getClassAppResponse()::get($result);
            }

            return $this->getClassAppResponse()::get(false, false, $result);
        } catch (RegisterException $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => "Текст ошибки: ".$e->getMessage()."\nТело запроса:\n".print_r($reqBody, true),
                ],
                "user.register.client"
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (Exception $e) {
            Log::error(
                [
                    "user_id"      => Yii::$app->user->getId(),
                    "user_login"   => $reqBody["login"],
                    "user_ip"      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    "user_browser" => Yii::$app->getRequest()->getUserAgent(),
                    "text"         => "Текст ошибки: ".$e->getMessage()."\nТело запроса:\n".print_r($reqBody, true),
                ],
                "user.register.client"
            );
            throw $e;
        }
    }

    /**
     * Экшн установки пароля пользователя
     *
     * @throws \Exception
     */
    public function actionNew()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (empty($reqBody['username']) && empty($reqBody['password_reset_token'])) {

            throw new \Exception("Не верно заданы параметры. Уточните запрос.");
        }
        //Проверяем что логин и токен существуют в БД
        $user = (new Users(['scenario' => Users::SCENARIO_GET_USER_BY_USERNAME_AND_TOKEN]));
        if (!$user->load($reqBody, '')) {
            throw new \Exception("Не удалось загрузить данные из входящего запроса. Обратитесь к администратору.");
        }
        if (!$user->validate()) {
            return $this->getClassAppResponse()::get(false, $user->firstErrors);
        }
        $result = $user->getByLoginAndToken();
        if (empty($result) || $result == false) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                ['Пользователь отсутствует в базе. Проверьте входные данные.']
            );
        } elseif (is_string($result)) {
            return $this->getClassAppResponse()::get(false, false, $result);
        } elseif (is_array($result)) {
            //Пользователь существует, если пароля нет, возвращаем что пользователь существует, иначе устанавливаем пароль.
            if (empty($reqBody['password'])) {
                return $this->getClassAppResponse()::get(['exist' => true]);
            } else {
                //Загружаем и обновляем пароль
                $user = (new Users(['scenario' => Users::SCENARIO_SET_PASSWORD]));
                //Создаем пользователя для сохранения в БД
                if (
                !$user->load(
                    [
                        'username' => $reqBody['username'],
                        'password' => $reqBody['password'],
                    ],
                    ''
                )
                ) {
                    throw new \Exception('Не удалось загрузить данные для сброса пароля');
                }
                $user->hashPassword($reqBody['password']); //Хэшируем пароль
                //сохраняем данные пользователя
                if (!$user->validate()) {
                    return $this->getClassAppResponse()::get(false, $user->firstErrors);
                }
                //Сбрасываем пароль
                $result = $user->resetPassword(null, null);
                if (intval($result) == 0) {
                    throw new \Exception("Не удалось найти пользователя для сброса пароля");
                } elseif (intval($result) > 1) {
                    throw new \Exception("Задублирован логин пользователя. Свяжитесь с администратором");
                } elseif (is_string($result)) {
                    throw new \Exception($result);
                }

                return $this->getClassAppResponse()::get(['result' => 'Новый пароль установлен']);
            }
        }

        return $this->getClassAppResponse()::get(false, false, 'Неизвестная ошибка. Обратитесь к администратору.');
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
}
