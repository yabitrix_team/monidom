<?php

namespace app\modules\app\v1\controllers\user;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Users;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class SetController - для установки нового пароля пользователя
 *
 * @package app\modules\app\v1\controllers
 *
 */
class SetController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}

    /**
     * Запрос на сброс пароля пользователя
     *
     * @return array
     * @throws \Exception
     */
	public function actionCreate() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $model   = ( new Users() )->getOneByFilter( [ 'password_reset_token' => $reqBody['code'] ] );
        if ( ! $model ) {
            throw new \Exception( 'Не найден пользователь' );
        }
        $modelPass = new Users();
        $modelPass->hashPassword( $reqBody['password'] );
        if ( ( new Users() )->updateByUsername( $model['username'], [
            'password_hash'        => $modelPass->password_hash,
            'password_reset_token' => '',
        ] ) ) {
            return $this->getClassAppResponse()::get( [ 'success' => true ] );
        } else {
            return $this->getClassAppResponse()::get( false, "Произошла ошибка при смене пароля, обратитесь к администратору" );
        }
	}
}
