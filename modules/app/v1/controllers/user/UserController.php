<?php
namespace app\modules\app\v1\controllers\user;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Groups;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\SmsConfirm;
use app\modules\app\v1\classes\logs\Log;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class UserController - контроллер для работы со справочником пользователи
 *
 * @package app\modules\app\v1\controllers
 */
class UserController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }

    /**
     * Получение информации о текущем зарегистрированно пользователе
     *
     * @return array|mixed|\yii\web\User
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {
            return $this->getClassAppResponse()::get(
                false,
                false,
                "Пользователь не авторизован, авторизуйтесь для получения информации"
            );
        }
        $user = Users::findIdentity(Yii::$app->user->getId());
        if (!$user['id']) {
            Yii::$app->user->logout(true);

            return $this->getClassAppResponse()::get(
                false,
                false,
                "Не удалось найти пользователя по данным аутентификации. Вы деаутентифицированы."
            );
        }
        $group = Groups::getGroupByUserId($user['id']);
        $group = is_array($group) ? reset($group) : [];
        if (!strlen($group['identifier'])) {
            Yii::$app->user->logout(true);

            return $this->getClassAppResponse()::get(
                false,
                false,
                "Не удалось найти роль пользователя по данным аутентификации. Вы деаутентифицированы."
            );
        }
        // Подготавливаем нужные данные для ответа
        $arrResult = [
            'point_id'        => $user['point_id'],
            'username'        => $user['username'],
            'email'           => $user['email'],
            'first_name'      => $user['first_name'],
            'last_name'       => $user['last_name'],
            'second_name'     => $user['second_name'],
            'is_accreditated' => $user['is_accreditated'],
            'role'            => $group['identifier'],
        ];
        $user->build($user['id']);
        if ($user->isPartnerAdmin()) {
            $arrResult['isAdmin'] = $user->isPartnerAdmin();
        };

        return $this->getClassAppResponse()::get($arrResult);
    }

    /**
     * Экшен для обновления пользователя, замаплено только поле "request_mode"
     *
     * @param $username - логин пользователя
     *
     * @return array
     * @throws \Exception
     */
    public function actionUpdate($username)
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (empty($reqBody['request_mode'])) {
            throw new \Exception('Не задан request_mode');
        }
        $user = Users::instance()->getOneByFilter(
            [
                'username' => $username,
                'active'   => 1,
            ]
        );
        if (empty($user)) {
            throw new \Exception('Не найден пользователь');
        }
        $user['request_mode'] = $reqBody['request_mode'];
        $model                = new Users(['scenario' => Users::SCENARIO_UPDATE]);
        if (!$model->load($user, '')) {
            throw new \Exception('Не удалось загрузить данные. Обратитесь к администратору');
        }
        if (!$model->validate()) {
            return $this->getClassAppResponse()::get(false, $model->firstErrors);
        }
        $result = $model->updateByUsername($username);
        if (is_string($result)) {
            return $this->getClassAppResponse()::get(false, false, $result);
        } elseif (intval($result) == 1) {
            return $this->getClassAppResponse()::get(['updated' => true,]);
        } elseif (intval($result) > 1) {
            //todo[hdcy]: 18.04.2018 Добавить оповещение на ошибку
            return $this->getClassAppResponse()::get(
                false,
                false,
                "По входным данным обновлено более одной записи. Дубликация данных. Обратитесь к администратору."
            );
        }

        return $this->getClassAppResponse()::get(
            false,
            false,
            "При обновлении произошла неизвестная ошибка. Обратитесь к администратору."
        );
    }

    /**
     * Экш валидации СМС клиенту при оформлении займа
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionVerifysms()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (empty($reqBody['sms'])) {
            throw new \Exception('Не задан проверочный код');
        }
        if (!isset($reqBody['code']) || strlen($reqBody['code']) != 32) {
            throw new \Exception('Некорректно указан код заявки');
        }
        if (!$request = (new Requests())->getOneByFilter(['code' => $reqBody['code']])) {
            throw new \Exception('Указанная заявка отсутствует в системе');
        }
        if (
        !(new SmsConfirm)->getOneByFilter(
            [
                'login' => $request['client_mobile_phone'],
                'code'  => $reqBody['sms'],
            ]
        )
        ) {
            return $this->getClassAppResponse()::get(['verified' => false]);
        };

        return $this->getClassAppResponse()::get(['verified' => true]);
    }

    /**
     * Экш валидации пароля пользователя
     *
     * @throws \Exception
     */
    public function actionVerify()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if (empty($reqBody['password'])) {
            throw new \Exception('Не задан пароль пользователя');
        }
        $verified = false;
        $user     = Users::findIdentity(Yii::$app->user->getId());
        if (Yii::$app->security->validatePassword($reqBody['password'], $user['password_hash'])) {
            if (!isset($reqBody['code']) || strlen($reqBody['code']) != 32) {
                throw new \Exception('Некорректно указан код заявки');
            }
            if (!$request = (new Requests())->getOneByFilter(['code' => $reqBody['code']])) {
                throw new \Exception('Указанная заявка отсутствует в системе');
            }
            if (!$event = (new Events())->getByCode(102)) {
                throw new \Exception('Указанный код события отсутствует в системе');
            }
            $model = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CREATE]);
            //проверка блокировки отправки смс
            $model->login = $request['client_mobile_phone'];
            $model->checkLockByLogin();
            $model->generateSmsCode();
            if (!$model->save()) {
                throw new \Exception('При попытке генерации СМС кода произошла ошибка, попробуйте позже');
            }
            $smsText  = $model->getTextSmsConfirm();
            $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
            $modelSms->load(
                [
                    'phone'   => $model->login,
                    'message' => $smsText,
                ],
                ''
            );
            $modelSms->setLogCategory('sms.confirm.request')->sendData();
        }

        return $this->getClassAppResponse()::get(['verified' => true]);
    }
}
