<?php
namespace app\modules\app\v1\controllers\info;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Pages;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class InstructionsController - контроллер для получения статического контента Инструкции
 *
 * @package app\modules\app\v1\controllers
 */
class InstructionsController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        // Получаем идентификатор контроллера Instructions из info/instructions
        $controllerId = end(explode('/', $this->id));
        $data         = (new Pages())->getPage($controllerId, $this->getConsumer(), true);
        $data['text'] = htmlspecialchars_decode($data['text']);

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
}
