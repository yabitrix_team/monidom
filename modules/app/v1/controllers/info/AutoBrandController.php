<?php
namespace app\modules\app\v1\controllers\info;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\AutoBrands;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class AutoBrandController - контроллер для работы со справочником Марки авто
 *
 * @package app\modules\app\v1\controllers
 */
class AutoBrandController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $data = (new AutoBrands())->getAllByFilter(['active' => '1'], ['name' => 'asc']);

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
