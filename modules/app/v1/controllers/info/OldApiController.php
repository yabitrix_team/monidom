<?php

namespace app\modules\app\v1\controllers\info;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class OldApiController - контроллер для поддержки переезда МП со старого АПИ на новое
 * todo[echmaster]: временное решение для переезда, после перехода всех пользователей, можем удалить
 *
 * @package app\controllers\app\v1\service
 */
class OldApiController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	/**
	 * Экшен получения всех версий справочников
	 *
	 * @return array - вернет массив с версиями справочников
	 */
	public function actionIndex() {

        //todo[echmaster]: Перед выгрузкой на прод уточнить версии для МП
        $data = [
            'version' => [
                'cur' => '1.3.7',
                'min' => '1.3.7',
            ],
            'mp'      => [
                'ios' => [
                    'version_cur' => '1.1.0',
                    'version_min' => '1.1.0',
                ],
            ],
        ];

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}