<?php

namespace app\modules\app\v1\controllers\info;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\VersionsDictionaries;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class InfoController - контроллер для работы с версиями справочников
 *
 * @package app\controllers\app\v1\service
 */
class InfoController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	/**
	 * Экшен получения всех версий справочников
	 *
	 * @return array - вернет массив с версиями справочников
	 */
	public function actionIndex() {

        $data = ( new VersionsDictionaries() )->getAll();
        if ( is_string( $data ) ) {
            throw new \Exception( 'При попытке получения версий всех справочников произошла ошибка выборки из таблицы БД, обратитесь к администратору' );
        }
        if ( empty( $data ) ) {
            throw new \Exception( 'При попытке получения версий всех справочников вернулась пустая выборка, обратитесь к администратору' );
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}
}