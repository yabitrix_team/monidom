<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 03.05.2018
 * Time: 17:02
 */

namespace app\modules\app\v1\controllers\info;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\PagesFaq;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class FaqController - контроллер для работы с faq_page
 *
 * @package app\controllers\app\v1\service
 */
class FaqController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения всех pages_faq
     *
     * @return array - вернет массив с версиями справочников
     * @throws \Exception
     */
	public function actionIndex() {

        $data = ( new PagesFaq() )->getAllByConsumer( true, $this->getConsumer() );
        foreach ( $data as &$item ) {
            $item['text'] = htmlspecialchars_decode( $item['text'] );
        }
        if ( is_string( $data ) ) {
            throw new \Exception( 'При попытке получения версий всех pages_faq произошла ошибка выборки из таблицы БД, обратитесь к администратору' );
        }
        if ( empty( $data ) ) {
            throw new \Exception( 'При попытке получения версий всех pages_faq вернулась пустая выборка, обратитесь к администратору' );
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}
}