<?php

namespace app\modules\app\v1\controllers\document;

use Yii;
use app\modules\app\v1\models\Document;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\JsonParser;
use Exception;
use app\modules\web\v1\classes\traits\TraitConfigController;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;


class DocumentController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreate()
    {
        $arRequest = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        if( !empty($arRequest['code']) && !empty($arRequest['label'])) {
            $doc = new Document();
            $updateDoc = $doc->getByCode($arRequest['code']);
            if (!empty($updateDoc)) {
                $updated = $doc->update(['code' => $arRequest['code']], [
                    'label' => $arRequest['label'],
                    'html' => $arRequest['html'],
                ]);
                return $this->getClassAppResponse()::get(['updated' => $updated ? true : false]);
            } else {
                if ($doc->load($arRequest, '')) {
                    if ($doc->validate()) {
                        $id = $doc->create();
                        if (is_numeric($id)) {
                            return $this->getClassAppResponse()::get([
                                "id" => $id
                            ]);
                        } else {
                            throw new Exception("Не удалось добавить запись " . $id);
                        }
                    } else {
                        return $this->getClassAppResponse()::get(false, $doc->firstErrors);
                    }
                } else {
                    throw new Exception("Не удалось загрузить данные в модель");
                }
            }
        } else {
            throw new Exception( "Ошибка создания или обновления документа" );
        }
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionIndex() {

        $code = (Yii::$app->request->getQueryParams())['code'];
        if ($code) {
            $explode_code = explode(',', $code);
            $doc = new Document();
            $res = $doc->getAllByFilter([
                'code' => $explode_code
            ]);
            if ($res) {
                return $this->getClassAppResponse()::get($res);
            } else {
                throw new Exception('По указаным параметрам документов не найдено');
            }
        } else {
            return $this->getClassAppResponse()::get(Document::instance()->getAll());
        }
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                    ],
                ],
            ],
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],

        ];
    }
}
