<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\CardVerifier;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageType;
use Exception;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\JsonParser;

/**
 * Class CardVerifierController - контроллер верификации карты клиента
 *
 * @package app\modules\app\v1\controllers\service
 */
class CardVerifierController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var - ассоциации событий code=>id
     *      [
     *          810=>25 - отправка на проверку
     *          811=>26 - проверка прошла успешно
     *          812=>27 - проверка завершилась неудачей
     *      ]
     */
    protected $events;

    public function __construct(string $id, \yii\base\Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $events = (new Events())->getAllByFilter(['code' => [810, 811, 812]]);
        foreach ($events as $key => $val) {
            $this->events[$val['code']] = $val['id'];
        }
    }

    /**
     * ф-я запроса адреса фрейма/страницы для проверки карты в системе W1
     * Запрос отправляет фронт,
     * {
     *  'code' : 'de236a51c95272d01a00cc58d1c2ad90' - код заявки
     * }
     *
     * @return array
     */
    public function actionCreate()
    {
        try {
            $reqBody  = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $Requests = (new Requests());
            if (!$request = $Requests->getOneByFilter(['code' => $reqBody['code']])) {
                throw new \Exception(
                    'Указанная заявка отсутствует в системе, обратитесь к администратору'
                );
            }
            $chain_id         = md5(time().random_int(1000, 9999));
            $externalId       = Yii::$app->cardVerifier->generateExternalId()->getExternalId();
            $event_id         = $this->events[810];
            $cardVerifier     = new CardVerifier();
            $cardVerifierData = [
                'chain_id'    => $chain_id,
                'request_id'  => $request['id'],
                'external_id' => $externalId,
                'event_id'    => $event_id,
            ];
            $cardVerifier->load($cardVerifierData, '');
            if (!$cardVerifier->validate()) {
                throw new \Exception($cardVerifier->firstErrors);
            }
            $result = $cardVerifier->add($cardVerifierData);
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при записи CardVerifier');
            }
            $Requests->deleteCVByCode($reqBody['code']);
            $modelRequestsEvents = new RequestsEvents();
            $modelRequestsEvents->load(
                [
                    'request_id' => $request['id'],
                    'event_id'   => $event_id,
                ],
                ''
            );
            if (!$modelRequestsEvents->validate()) {
                Log::error(
                    ['text' => print_r(['actionCreate()', $modelRequestsEvents->getMessage()], 1)],
                    'card.verifier.log'
                );
                throw new \Exception('Произошла ошибка валидации данных, обратитесь к администратору.');
            }
            $result = $modelRequestsEvents->create();
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при попытке прикрепления события к заявке');
            }
            $response = Yii::$app->cardVerifier
                ->setRequestCode($reqBody['code'])
                ->setChainId($chain_id)
                ->getCreate('link');
            if (is_a($response, \Exception::class)) {
                throw new \Exception($response->getMessage());
            }
            $cardVerifier     = new CardVerifier();
            $operationId      = $response->operationId;
            $url              = $response->url;
            $cardVerifierData = [
                'chain_id'     => $chain_id,
                'request_id'   => $request['id'],
                'external_id'  => $externalId,
                'operation_id' => $operationId,
                'amount'       => Yii::$app->cardVerifier->getAmount(),
                'url'          => $url,
                'event_id'     => $event_id,
            ];
            $cardVerifier->load($cardVerifierData, '');
            if (!$cardVerifier->validate()) {
                throw new \Exception($cardVerifier->firstErrors);
            }
            $result = $cardVerifier->add($cardVerifierData);
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при записи CardVerifier');
            }
            Log::info(['text' => print_r(['actionCreate()', $reqBody, $cardVerifierData], 1)], 'card.verifier.log');
            $response = json_decode(json_encode($response), true);

            return $this->getClassAppResponse()::get(['url' => $response['url']]);
        } catch (\Exception $e) {
            Log::error(['text' => print_r(['actionCreate()', $reqBody, $cardVerifierData], 1)], 'card.verifier.log');

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * обработка колбэков
     * 1. обработка оплаты
     * 2. обработка отмены оплаты
     *
     * @return array
     */
    public function actionCallback()
    {
        try {
            $reqBody      = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $cardVerifier = new CardVerifier();
            if ($cardVerifier->getOneByFilter(['operation_id' => $reqBody['id'], '?token' => 'payoutToken'])) {
                \Yii::$app->getResponse()->setStatusCode(200);

                return $this->getClassAppResponse()::get($reqBody);
            }
            if (!$request = (new Requests())->getOneByFilter(['code' => $reqBody['merchantParameters']['param2']])) {
                throw new \Exception(
                    'Указанная заявка отсутствует в системе. Обратитесь к администратору.'
                );
            }
            $request_code = $reqBody['merchantParameters']['param2'];
            switch ($reqBody['status']) {
                // если запрос выполнен успешно
                case 'SUCCEEDED':
                    $chain_id        = $reqBody['merchantParameters']['param3'];
                    $request_id      = $request['id'];
                    $operationId     = $reqBody['id'];
                    $externalId      = $reqBody['externalId'];
                    $amount          = $reqBody['amount']['amount'];
                    $url             = '';
                    $card            = $reqBody['payer']['paymentMethod']['entity']['pan'];
                    $token_recurring = $reqBody['registeredMethods'][0]['entity']['id'];
                    $token           = $reqBody['registeredMethods'][1]['entity']['id'];
                    $event_id        = $this->events[811];
                    // если это была оплата (получение токена), то делаем отмену платежа
                    if ($reqBody['type'] == 'PURCHASE') {
                        //if (Yii::$app->cardVerifier->getCabinet() != 'client') {
                        $messageText = "Карта {$card} подтверждена";
                        Yii::$app->notify->addMessage(
                            [
                                "type_id"  => MessageType::instance()->sys,
                                "order_id" => $request['id'],
                                "message"  => $messageText,
                            ],
                            true
                        );
                        //}
                        Yii::$app->cardVerifier
                            ->generateExternalId()
                            ->setOperationId($operationId)
                            ->setRequestCode($reqBody['merchantParameters']['param2'])
                            ->setChainId($reqBody['merchantParameters']['param3'])
                            ->setAmount($reqBody['amount']['amount'] / 1)
                            ->getCreate('refund');
                    }
                    // записываем токен и masked_card в заявку
                    //из трейта подтягиваем namespace модели Request
                    $model        = new Requests(['scenario' => Requests::SCENARIO_CARD_VERIFIER]);
                    //поиск заявки по коду
                    $this->request = $model->getByCode($request_code);
                    if (empty($this->request) && !is_array($this->request)) {
                        $msg = empty($this->request)
                            ? 'При попытке обновления заявки не уадлось найти заявку, обратитесь к администратору'
                            : 'При попытке обновления заявки произошла неизвестная ошибка, обратитесь к администратору';
                        throw new \Exception($msg);
                    }
                    $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
                    if (!$model->load(['card_number' => $card, 'card_token' => $token], '')) {
                        throw new \Exception('Не переданы данные для обновления заявки');
                    }
                    if (!$model->validate()) {
                        Log::error(
                            ['text' => print_r(['actionCallback()', $model->getMessage()], 1)],
                            'card.verifier.log'
                        );
                        throw new \Exception('Произошла ошибка валидации данных, обратитесь к администратору.');
                    }
                    $result = $model->updateByCode($request_code);
                    if (is_a($result, \Exception::class)) {
                        throw $result;
                    }
                    break;
                case 'DECLINED':
                    $chain_id        = $reqBody['merchantParameters']['param3'];
                    $request_id      = $request['id'];
                    $operationId     = $reqBody['id'];
                    $externalId      = $reqBody['externalId'];
                    $amount          = $reqBody['amount']['amount'];
                    $url             = '';
                    $card            = $reqBody['payer']['paymentMethod']['entity']['pan'];
                    $token_recurring = '';
                    $token           = '';
                    $event_id        = $this->events[812];
                    //if (Yii::$app->cardVerifier->getCabinet() != 'client') {
                    $messageText = "Карта {$card} не подтверждена";
                    Yii::$app->notify->addMessage(
                        [
                            "type_id"  => MessageType::instance()->sys,
                            "order_id" => $request['id'],
                            "message"  => $messageText,
                        ],
                        true
                    );
                    //}
                    break;
                case 'EXPIRED':
                    $chain_id        = $reqBody['merchantParameters']['param3'];
                    $request_id      = $request['id'];
                    $operationId     = $reqBody['id'];
                    $externalId      = $reqBody['externalId'];
                    $amount          = $reqBody['amount']['amount'];
                    $url             = '';
                    $card            = $reqBody['payer']['paymentMethod']['entity']['pan'];
                    $token_recurring = '';
                    $token           = '';
                    $event_id        = $this->events[812];
                    //if (Yii::$app->cardVerifier->getCabinet() != 'client') {
                    $messageText = "Срок действия карты {$card} истек";
                    Yii::$app->notify->addMessage(
                        [
                            "type_id"  => MessageType::instance()->sys,
                            "order_id" => $request['id'],
                            "message"  => $messageText,
                        ],
                        true
                    );
                    //}
                    break;
            }
            $cardVerifierData = [
                'chain_id'     => $chain_id,
                'request_id'   => $request_id,
                'external_id'  => $externalId,
                'operation_id' => $operationId,
                'amount'       => $amount,
                'url'          => $url,
                'card'         => $card,
                'token'        => $token,
                'event_id'     => $event_id,
            ];
            $cardVerifier->load($cardVerifierData, '');
            if (!$cardVerifier->validate()) {
                throw new \Exception(print_r($cardVerifier->firstErrors));
            }
            $result = $cardVerifier->add($cardVerifierData);
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при записи CardVerifier');
            }
            $modelRequestsEvents = new RequestsEvents();
            $modelRequestsEvents->load(
                [
                    'request_id' => $request['id'],
                    'event_id'   => $event_id,
                ],
                ''
            );
            if (!$modelRequestsEvents->validate()) {
                return $this->getClassAppResponse()::get(false, $modelRequestsEvents->firstErrors);
            }
            $result = $modelRequestsEvents->create();
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при попытке прикрепления события к заявке');
            }
            if ($reqBody['type'] == 'PURCHASE') {
                $event = $this->getEvent($request['code']);
                if (Yii::$app->cardVerifier->getCabinet() != 'client') {
                    $this->sendMessage($event);
                }
            }
            Log::info(
                [
                    'text' => print_r(
                        [
                            'actionCallback()',
                            '$reqBody'          => $reqBody,
                            '$cardVerifierData' => $cardVerifierData,
                            '$event'            => $event,
                        ],
                        1
                    ),
                ],
                'card.verifier.log'
            );
            \Yii::$app->getResponse()->setStatusCode(200);

            return $this->getClassAppResponse()::get(['OK']);
        } catch (\Exception $e) {
            Log::error(
                ['text' => print_r(['actionCallback()', $e->getMessage(), $reqBody, $cardVerifierData], 1)],
                'card.verifier.log'
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Redirect - для открытия кароткой ссылки
     *
     * @return array|\yii\web\Response
     */
    public function actionRedirect()
    {
        $reqBody = Yii::$app->request->get();
        print_r($reqBody);
        try {
            $cardVerifier =
                (new CardVerifier())->getOneByFilter(['chain_id' => $reqBody['code'], '!url' => ['null', '']]);
            if (is_a($cardVerifier, \Exception::class)) {
                throw new \Exception('URL для редиректа не найден.Обратитесь к администратору.');
            }

            return $this->redirect($cardVerifier['url'], 301);
        } catch (\Exception $e) {
            Log::error(
                ['text' => print_r(['actionRedirect()', $e->getMessage(), $reqBody, $cardVerifier], 1)],
                'card.verifier.log'
            );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Вызывается при успешном подтверждении карты
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionSuccess()
    {
        try {
            $reqBody = Yii::$app->request->get();
            Log::info(['text' => print_r(['actionSuccess()', $reqBody], 1)], 'card.verifier.log');

            return $this->redirect(Yii::$app->cardVerifier->setRequestCode($reqBody['code'])->getRequestUrl(), 301);
        } catch (Exception $e) {
            Log::error(
                ['text' => print_r(['actionSuccess()', $e->getMessage(), $reqBody], 1)],
                'card.verifier.log'
            );
            throw $e;
        }
    }

    /**
     * Вызывается со стороны W1 при неуспешном подтверждении карты
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDecline()
    {

        try {
            $reqBody = Yii::$app->request->get();
            Log::info(['text' => print_r(['actionDecline()', $reqBody], 1)], 'card.verifier.log');

            return $this->redirect(Yii::$app->cardVerifier->setRequestCode($reqBody['code'])->getRequestUrl(), 301);
        } catch (Exception $e) {
            Log::error(
                ['text' => print_r(['actionDecline()', $e->getMessage(), $reqBody], 1)],
                'card.verifier.log'
            );
            throw $e;
        }
    }

    /**
     * Веренет последний статус по заявке
     * [
     *  'event',
     *  'card_number'
     * ]
     *
     * @return array
     * @throws \Exception
     */
    public function actionEvent()
    {
        try {
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $event   = $this->getEvent($reqBody['code']);
            $event   = array_intersect_key($event, ['event' => 'event', 'card_number' => 'card_number']);

            return $this->getClassAppResponse()::get($event);
        } catch (\Exception $e) {
            Log::error(
                ['text' => print_r(['actionEvent()', $e->getMessage(), $event], 1)],
                'card.verifier.log'
            );
            throw $e;
        }
    }

    /**
     * Получение последнего события по подтверждению карты
     *
     * @return array
     * @throws \Exception
     */
    protected function getEvent(string $code)
    {
        try {
            if (strlen($code) != 32) {
                throw new Exception('Не указан код заявки');
            }
            $Events         = (new Events())->getAllByFilter(['code' => array_flip($this->events)]);
            $Request        = (new Requests())->getOneByFilter(['code' => $code]);
            $RequestsEvents = ((new RequestsEvents())->getAllByFilter(
                ['request_id' => $Request['id'], 'event_id' => array_column($Events, 'id')],
                ['created_at' => 'DESC']
            ))[0];

            return [
                "event"       => (array_flip($this->events))[$RequestsEvents['event_id']],
                "card_number" => $Request['card_number'],
                'request_id'  => $Request['id'],
                /*'card_token'  => $Request['card_token'],*/
            ];
        } catch (\Exception $e) {
            Log::error(
                ['text' => print_r(['getEvent()', $e->getMessage(), "request_code" => $code], 1)],
                'card.verifier.log'
            );
            throw $e;
        }
    }

    /**
     * Отправка сообщения
     *
     * @param array $request - заявка
     * @param array $message - сообщение
     */
    protected function sendMessage($message)
    {


        $mess = [
            "type_id"  => MessageType::instance()->cardVerifier,
            "order_id" => $message['request_id'],
            "message"  => json_encode(
                array_intersect_key($message, ['event' => 'event', 'card_number' => 'card_number']),
                JSON_FORCE_OBJECT
            ),
        ];
        if (!Yii::$app->notify->addMessage($mess, true)) {
            Log::error(
                [
                    'text' => print_r(
                        [
                            'Не удается отправить сообщение в сокет о статусе подтверждения карты : sendMessage($event)',
                            $message,
                        ],
                        1
                    ),
                ],
                'card.verifier.log'
            );
            throw new Exception(
                "Не удается отправить сообщение в сокет о статусе подтверждения карты"
            );
        }
    }

    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application / json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => \yii\filters\VerbFilter::class,
                'actions' => [
                    'callback' => ['POST'],
                    'create'   => ['POST'],
                    'success'  => ['GET'],
                    'decline'  => ['GET'],
                    'redirect' => ['GET'],
                ],
            ],
        ];
    }
}