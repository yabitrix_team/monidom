<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\cmr\Annuity;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class CalcAnnuityController - контроллер для расчета аннуитета
 *
 * @package app\controllers\app\v1\service
 */
class TestpController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	/**
	 * @var array - ассоциация полей полученных от сервиса через SOAP с полями контракта
	 */

	/**
	 * Экшен - расчет аннуитета
	 */
	public function actionIndex() {

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('http://in.pratsun.lk/in/v1/service/testp/success/12634162394619273948711111111111')
            ->setData(['name' => 'John Doe', 'email' => 'johndoe@example.com'])
            ->send();
        if ($response->isOk) {
            $newUserId = $response->data['id'];
        }

	}

    public function actionSuccess(){
        return 'actionSuccess()';
    }

    public function actionDecline(){
        return 'actionDecline()';
    }

    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index'  => ['GET'],
                    'callback' => ['GET'],
                    'success'   => ['POST'],
                    'decline' => ['POST'],
                ],
            ],

        ];
    }

}