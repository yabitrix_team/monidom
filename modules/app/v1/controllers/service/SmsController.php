<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\AuthToken;
use app\modules\app\v1\models\SmsConfirm;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\mp\v1\models\Users;
use app\modules\app\v1\classes\logs\Log;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;
use app\modules\app\v1\models\RegisterLog;
use app\modules\app\v1\models\Leads;

/**
 * Class SMSController - контроллер взаимодействия с SMS-сервисом
 *
 * @package app\modules\app\v1\controllers\service
 */
class SmsController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var string - категория лога для клиента
     */
    protected $logCategory = 'sms.confirm.client';

    /**
     * Экшен для создания и отправки смс сообщения с кодом подтверждения
     *
     * @return array - вернет результат отправки кода подтверждения или ошибку
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            $reqBody                 = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $reqBody['login']        = isset($reqBody['login'])
                ? $reqBody['login']
                : Users::findIdentityByAccessToken(AuthToken::getTokenInHeader())['username'];
            $arLead                  = (new Leads())->getAllByFilter(
                ['client_mobile_phone' => $reqBody['login']],
                ['created_at' => 'desc']
            )[0];
            $obRegisterLog           = new RegisterLog(
                [
                    'login'           => $reqBody['login'],
                    'code'            => $arLead['code'],
                    'first_name'      => $arLead['client_first_name'],
                    'last_name'       => $arLead['client_last_name'],
                    'patronymic'      => $arLead['client_patronymic'],
                    'passport_serial' => $arLead['client_passport_serial_number'],
                    'passport_number' => $arLead['client_passport_number'],
                    'sms_vendor'      => 'RedSMS',
                    'user_agent'      => Yii::$app->request->getHeaders()['user-agent'],
                    'document_type'   => 'СМС',
                ]
            );
            $obRegisterLog->key_sent = 0;
            $model                   = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CREATE]);
            if (!$model->load($reqBody, '')) {
                throw new \Exception('Не переданы параметры для отправки кода подтверждения через СМС-сообщение');
            }
            if (!$model->validate()) {
                $obRegisterLog->add($obRegisterLog->getAttributes());

                return $this->getClassAppResponse()::get(false, $model->firstErrors);
            }
            //проверка блокировки отправки смс
            $model->checkLockByLogin();
            $obRegisterLog->key_code = $model->generateSmsCode();
            $model->save();
            $smsText  = $model->getTextSmsConfirm($reqBody['hash_code']);
            $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
            $modelSms->load(
                [
                    'phone'   => $model->login,
                    'message' => $smsText,
                ],
                ''
            );
            $smsId = $modelSms->setLogCategory('sms.confirm.client')->sendData();
            $model->updateByLogin($model->login, ['sms_id' => $smsId]);
            $obRegisterLog->key_sent = 1;
            $obRegisterLog->add($obRegisterLog->getAttributes());

            return $this->getClassAppResponse()::get(
                [
                    'send_code' => true,
                    'lock_time' => Yii::$app->params['core']['sms']['lock_expired_at'] * 60,
                ]
            );
        } catch (\Exception $e) {
            $obRegisterLog->add($obRegisterLog->getAttributes());
            throw $e;
        }
    }

    /**
     * Экшен для проверки кода подтверждения операции
     *
     * @param $code - код подтверждения операции
     *
     * @return mixed - вернет результат проверки кода подтверждения или ошибку
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionCheck($code)
    {
        try {
            $reqBody          = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            $reqBody['login'] = isset($reqBody['login'])
                ? $reqBody['login']
                : Users::findIdentityByAccessToken(AuthToken::getTokenInHeader())['username'];
            $reqBody['code']  = $code;
            if (
            $arRegisterLog = RegisterLog::instance()
                ->getAllByFilter(['login' => $reqBody['login']], ['created_at' => 'desc'])[0]
            ) {
                $idRegisterLog = $arRegisterLog["id"];
            }
            $model = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CHECK]);
            $model->load($reqBody, '');
            if (!$model->validate()) {
                empty($idRegisterLog)
                    ?: RegisterLog::instance()->update($idRegisterLog, ['key_accepted' => 0, 'key_signed' => 0]);

                return $this->getClassAppResponse()::get(false, $model->firstErrors);
            }
            $model->verificationCode();
            if ($idRegisterLog) {
                RegisterLog::instance()->update(
                    $idRegisterLog,
                    [
                        'key_accepted'  => 1,
                        'key_signed'    => 1,
                        'key_delivered' => 1,
                    ]
                );
            }

            return $this->getClassAppResponse()::get(['check_code' => true]);
        } catch (\Exception $e) {
            if ($idRegisterLog) {
                RegisterLog::instance()->update($idRegisterLog, ['key_accepted' => 0, 'key_signed' => 0]);
            }
            throw $e;
        }
    }

    /**
     * Метод получения информации смс-сообщения(кол-во попыток ввода и время блокировки)
     *
     * @param $login - номер телефона, по которому необходимо получить информацию
     *
     * @return array
     * @throws \Exception
     */
    public function actionByLogin($login)
    {
        $model  = new SmsConfirm();
        $result = $model->getByLogin($login, ['attempt', 'lock_expired_at']);
        if (empty($result)) {
            throw new \Exception('Для указанного номера телефона в системе отсутствует информация смс-сообщения');
        }

        return $this->getClassAppResponse()::get(
            [
                'attempt'   => intval($result['attempt']),
                'lock_time' => intval($model->getLock($result['lock_expired_at'], true)),
            ]
        );
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}