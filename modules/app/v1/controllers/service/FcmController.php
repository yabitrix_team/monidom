<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\FCM;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class FcmController - контроллер взаимодействия с FCM-токенами и сервисом Firebase Cloud Messaging
 *
 * @package app\controllers\app\v1\service
 */
class FcmController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод экшен - запись данных по fcm-токену
     *
     * @throws \Exception
     */
    public function actionCreate()
    {

        $reqBody            = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $reqBody['os_type'] = strtolower($reqBody['os_type']);
        $model              = new FCM(['scenario' => FCM::SCENARIO_SAVE]);
        if (!$model->load($reqBody, '')) {
            throw new \Exception('Не переданы параметры для записи FCM токена');
        }
        $result = $model->save();
        if (is_a($result, \Exception::class)) {
            throw new \Exception('Прозошла ошибка при записи FCM токена');
        }
        if (is_a($result, FCM::class)) {
            return $this->getClassAppResponse()::get(false, $result->firstErrors);
        }

        return $this->getClassAppResponse()::get(['fcm_save' => true]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}