<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class Notification - контроллер для работы с нотификациями
 *
 * @package app\modules\app\v1\controllers
 *
 */
class NotificationController extends Controller {
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    public function actionIndex() {

        try {
            $data = [ "notification" ];

            return $this->getClassAppResponse()::get( $data );
        } catch ( \Exception $e ) {
            return $this->getClassAppResponse()::get( false, false, $e->getMessage() );
        }
    }

    public function behaviors() {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::className(),
                'actions' => $this->verbs(),
            ],
        ];
    }

    protected function verbs() {

        return [
        ];
    }
}