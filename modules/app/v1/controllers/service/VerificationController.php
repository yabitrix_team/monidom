<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\mp\v1\models\MessageType as MessageTypeMP;
use app\modules\rmq\classes\mfo\RmqMfoVerificatorsRequest;
use app\modules\notification\models\MessageVerification;
use app\modules\notification\models\MessageAttacheFile;
use app\modules\notification\models\MessageReceiver;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\notification\models\MessageStatus;
use app\modules\notification\models\MessageType;
use app\modules\notification\models\Message;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\FCM;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

class VerificationController extends Controller
{
    use TraitConfigController;

    /**
     * Экшен для работы с верификаторами
     *
     * @return array
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $data      = Yii::$app->getRequest()->post();
        $modelM    = new Message();
        $is_answer = false;
        if ($data['message_id']) {
            $mess       = [
                'type_id'  => MessageType::instance()->ver,
                'parentId' => $data['message_id'],
                'message'  => $data['message'],
            ];
            $is_answer  = true;
            $idAnswered = MessageStatus::instance()->answered;
            if (
                (!$messData = $modelM->getOne($data['message_id']))
                || $messData['status_id'] == $idAnswered
            ) {
                $msg = empty($messData)
                    ? 'Неверно указано уведомление запроса, обратитесь к администратору'
                    : 'Ответ на указанное уведомление уже получен';
                throw new \Exception($msg);
            }
            $mess['order_id'] = $messData['order_id'];
        } else {
            $mess = [
                'type_id' => MessageType::instance()->ver,
                'message' => $data['message'],
            ];
            if (false !== ($uid = Yii::$app->user->getId())) {
                $mess['user_id_from'] = $uid;
            }
            if (isset($data['code']) && ($request = (new Requests())->getByNum($data['code']))) {
                $mess['order_id'] = $request['id'];
            } else {
                return $this->getClassAppResponse()::get(false, false, 'empty');
            }
            Yii::$app->notify->addMessage(
                [
                    'type_id'  => MessageType::instance()->sys,
                    'order_id' => $request['id'],
                    'message'  => "Запрос данных: {$data['message']}",
                ]
            );
            if (
                (int) $request["client_id"]
                && (int) $request["point_id"] == (int) Yii::$app->params['client']['settings']['point_id']
                && strlen($request["client_mobile_phone"])
            ) {
                $smsText  = 'Уважаемый клиент, направьте недостающие данные через ЛК '.
                            $_SERVER['REQUEST_SCHEME'].
                            '://client.'.
                            Yii::$app->params['core']['domain'].
                            '. 88043339433 звонок бесплатный';
                $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
                $modelSms->load(
                    [
                        'phone'   => $request["client_mobile_phone"],
                        'message' => $smsText,
                    ],
                    ''
                );
                $modelSms->setLogCategory('sms.varification.service')->sendData();
            }
        }
        $id = Yii::$app->notify->addMessage($mess);
        if (empty($is_answer)) {
            //отправка пуш-уведомления в МП
            $pushTitle = Yii::$app->params['mp']['settings']['pushTitle'];
            !empty($request) ?: $request = (new Requests())->getByNum($data['code']);
            $message  = $modelM->getById($id, ['code']);
            $modelFCM = new FCM(['scenario' => FCM::SCENARIO_PUSH_NOTIFICATION]);
            $modelFCM->load(
                [
                    'user_id'      => $request['client_id'],
                    'title'        => !empty($pushTitle) ? $pushTitle : 'CarMoney',
                    'message'      => $mess['message'],
                    'request_code' => $request['code'],
                    'nt_code'      => $message['code'],
                    'nt_type'      => MessageTypeMP::$assocType[MessageType::VERIFIER],
                    'nt_title'     => '',
                    'nt_text'      => $mess['message'],
                    'nt_time'      => time(),
                    'nt_status'    => MessageStatus::NEW,
                ],
                ''
            );
            $modelFCM->send();
        }
        $listFilePath = [];
        $fileList     = [];
        if ($id && !empty($_FILES)) {
            $file = reset($_FILES);
            if (is_array($file['tmp_name'])) {
                foreach ($file['tmp_name'] as $i => $tmp_file) {
                    $path        = "/path/messages/$id/";
                    $resFileSave = Yii::$app->file->save($path, $file['name'][$i], $file['tmp_name'][$i]);
                    $mf          = new MessageAttacheFile(
                        [
                            'file_id'    => $resFileSave['id'],
                            'message_id' => $id,
                        ]
                    );
                    if ($mf->validate()) {
                        if ($mf->create()) {
                            $listFilePath[] = $path.$resFileSave['fileName'];
                            $fileList[]     = $resFileSave['id'];
                        }
                    }
                }
            }
        }
        //todo[echmaster]: 17.01.2019 ПРИ РАБОТЕ ЧЕРЕЗ RMQ НЕ ПРОСТАВЛЯЕТСЯ СТАТУС ОТВЕЧЕНО ДЛЯ РОДИТЕЛЬКОГО УВЕДОМЛЕНИЯ
        //todo[echmaster]: 21.01.2019 ПЕРЕЙТИ В RmqMfoVerificatorsRequest И ИСПРАВИТЬ ТАМ ЛОГИКУ КАК И ЗДЕСЬ
        if (Yii::$app->params['core']['rmq']['rmq201']) {
            /**
             * в классе-обработчике сообщений должна быть определена и описана функция prepareResponse()
             * в которую будет передан array $params вызываемой функции response()
             * ключи массива $params будут проверены на соответствие ключам, описанным в responseRequiredFields() (если пустой массив - проверки нет)
             * далее сформированный в функции prepareResponse() пакет данных будет отправлен посредством компонента RmqBase $component,
             * переданного первым параметром функции response()
             * далее следует обработать исключения Exception
             */
            $RmqMfoVerificatorsRequest = (new RmqMfoVerificatorsRequest())->response(
                Yii::$app->queueMFOSend,
                [
                    'message_id' => $mess['parentId'],
                    'message'    => $mess['message'],
                    'file_list'  => empty($fileList) ? [] : $fileList,
                ]
            );
            if (is_a($RmqMfoVerificatorsRequest, \Exception::class)) {
                throw new \Exception($RmqMfoVerificatorsRequest->getMessage());
            }
            if ($is_answer) {
                $modelM->update(
                    $data['message_id'],
                    ['status_id' => $idAnswered ?? MessageStatus::instance()->answered]
                );
            }

            return $this->getClassAppResponse()::get($id ? 'OK' : 'FALSE');
        }
        // старая версия ниже
        if ($is_answer) {
            !empty($idAnswered) ?: $idAnswered = MessageStatus::instance()->answered;
            $modelM->update($data['message_id'], ['status_id' => $idAnswered]);
            MessageReceiver::instance()->updateStatusByIdOrCodeMessage(
                $data['message_id'],
                $idAnswered,
                Yii::$app->user->id
            );
            $modelM->sendMailVer($data['message'], $data['message_id'], $listFilePath);
        }

        return $this->getClassAppResponse()::get($id ? 'OK' : 'FALSE');
    }

    /**
     * Включен/Выключен алгоритм работы через RMQ
     *
     * @return array
     */
    public function actionClosed()
    {
        return $this->getClassAppResponse()::get(["closed" => Yii::$app->params['core']['rmq']['rmq201']]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }

    protected function verbs()
    {

        return [
            'create' => ['POST'],
            'list'   => ['GET'],
            'is-rmq' => ['GET'],
        ];
    }

    /**
     * @return array|bool
     * @throws \yii\db\Exception
     */
    public function actionList()
    {

        if (false !== ($uid = Yii::$app->user->getId())) {
            return $this->getClassAppResponse()::get(
                array_values(
                    MessageVerification::instance()
                        ->getByUserID($uid)
                )
            );
        }
    }
}
