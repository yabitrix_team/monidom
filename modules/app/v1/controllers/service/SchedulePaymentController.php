<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Products;
use app\modules\app\v1\models\soap\cmr\CMRService;
use DateTime;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class SchedulePaymentController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения графика платежей относительно функции SmartPaymentChart сервиса CMRService
     *
     * @return mixed|string
     * @throws \Exception
     */
    public function actionIndex()
    {

        $request = Yii::$app->request->get();
        if (
            empty($request)
            || !isset($request['request_sum'])
            || !isset($request['return_date'])
        ) {
            throw new \Exception('Некорректно указаны параметры для получения графика платежей');
        }
        $model = new CMRService(['scenario' => CMRService::SCENARIO_SMART_PAYMENT_CHART]);
        $model->load(
            [
                'amount' => $request['request_sum'],
                'date'   => $request['return_date'],
            ],
            ''
        );
        //--- От даты срока займа отнимаем 1 день для учета отдаленных регионов и разницы по времени
        if (
            $model->validate('date')
            && preg_match('/^\d{4}-\d{2}-\d{2}/', $model->date)
        ) {
            $format = 'd.m.Y';
            $d      = new DateTime($model->date);
            $d->modify('-1 day');
            $date = $d->format($format);
            if ($date > date($format)) {
                $model->date = $date;
            }
        }
        $res = $model->smartPaymentChart();
        if (is_a($res, CMRService::class)) {
            return $this->getClassAppResponse()::get(false, $res->firstErrors);
        }
        if (is_string($res)) {
            throw new \Exception($res);
        }
        //доп. инфа о кредитном продукте
        $product = (new Products)->getDataByGuid(
            $res->product,
            [
                "guid",
                "name",
                "is_additional_loan",
                "percent_per_day",
                "month_num",
                "summ_min",
                "summ_max",
                "age_min",
                "age_max",
                "loan_period_min",
                "loan_period_max",
            ]
        );
        if (empty($product) || is_a($product, \Exception::class)) {
            throw new \Exception(
                'При попытке получения графика платежей не удалось получить данные кредитного продукта, обратитесь к администратору'
            );
        }
        $res->product_info = $product;

        return $this->getClassAppResponse()::get($res);
    }

    /**
     * Экшен получения графика платежей относительно функции GetPaymentChartv2 сервиса CMRService
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
    public function actionCreate()
    {

        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $model   = new CMRService(['scenario' => CMRService::SCENARIO_GET_PAYMENT_CHART_V2]);
        if (!$model->load($reqBody, '')) {
            throw new \Exception('Не были указаны параметры для получения графика платежей');
        }
        $res = $model->getPaymentChartV2();
        if (is_a($res, CMRService::class)) {
            return $this->getClassAppResponse()::get(false, $res->firstErrors);
        }
        if (is_string($res)) {
            throw new \Exception($res);
        }

        return $this->getClassAppResponse()::get($res);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
