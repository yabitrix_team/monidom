<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\cmr\Annuity;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class CalcAnnuityController - контроллер для расчета аннуитета
 *
 * @package app\controllers\app\v1\service
 */
class CalcAnnuityController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	/**
	 * @var array - ассоциация полей полученных от сервиса через SOAP с полями контракта
	 */
	protected $assocFields = [
		'Annuity'         => 'annuity',
		'GUID'            => 'guid',
		'CreditProductID' => 'credit_product_1c',
	];

    /**
     * Экшен - расчет аннуитета
     *
     * @throws \Exception
     */
	public function actionCreate() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $model   = new Annuity( [ 'scenario' => Annuity::SCENARIO_SMART_ANNUITY ] );
        if ( ! $model->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не были указаны параметры для расчета калькулятора' );
        }
        $res = $model->getSmartAnnuity();
        if ( is_a( $res, Annuity::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }
        if ( is_string( $res ) ) {
            throw new \Exception( $res );
        }
        $data = [];
        if ( ! empty( $res ) && ! empty( $this->assocFields ) ) {
            ! isset( $this->assocFields['Annuity'] ) ?: $data[ $this->assocFields['Annuity'] ] = $res->Annuity;
            ! isset( $this->assocFields['GUID'] ) ?: $data[ $this->assocFields['GUID'] ] = $res->GUID;
            ! isset( $this->assocFields['CreditProductID'] ) ?:
                $data[ $this->assocFields['CreditProductID'] ] = $res->CreditProductID;
        } else {
            $data = (array) $res;
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}