<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\exceptions\CrmException;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Crm;
use app\modules\notification\models\MessageType;

/**
 * Class CrmController - контроллер для работы с уведомлениями от системы CRM
 *
 * @package app\modules\app\v1\controllers
 */
class CrmController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {

        try {
            $reqBody              = Yii::$app->request->post();
            $data['guid']         = trim($reqBody['request_xml_id']);
            $data['message']      = trim($reqBody['message_text']);
            $data['message_date'] = $reqBody['message_date'];
            if (time() - strtotime($data['message_date']) < 2800 || empty($data['message_date'])) {
                $data['message_date'] = date("d.m.Y H:i:s", time());
            }
            $model = new Crm();
            if ($model->load($data, '')) {
                if (!$model->validate()) {
                    return $this->getClassAppResponse()::get(false, $model->firstErrors);
                }
            } else {
                throw new CrmException('Не удалось загрузить данные запроса '.__FILE__."(".__LINE__.")");
            }
            if (!$request = (new Requests())->getOneByFilter(['guid' => $data['guid']])) {
                throw new CrmException('Указанная заявка отсутствует в системе');
            }
            if (
            $id = Yii::$app->notify->addMessage(
                [
                    "type_id"  => MessageType::instance()->sys,
                    "order_id" => $request["id"],
                    "message"  => $data['message'],
                ],
                true
            )
            ) {
                $data["send"]       = true;
                $data["message_id"] = $id;
            } else {
                $data["send"] = false;
            }
            Log::info(["text" => "Тело запроса:\n".print_r($reqBody, true)], "in.service.crm");

            return $this->getClassAppResponse()::get($data);
        } catch (CrmException $e) {
            Log::error(
                ["text" => "Текст ошибки: ".$e->getMessage()."\nТело запроса:\n".print_r($reqBody, true)],
                "in.service.crm"
            );
        }
    }

    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}