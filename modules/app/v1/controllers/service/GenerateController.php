<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\PDF;
use app\modules\app\v1\models\Products;
use app\modules\app\v1\models\soap\cmr\CMRService;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class GenerateController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	/**
	 * Генерирует PDF из параметров
	 *
	 * @param $params - ассоциативный массив содержащий параметры для генерации
	 *
	 * @return array|mixed
	 * @throws \Exception
	 */
	private function generate($params) {
		$model = new PDF();
		if ( ! $model->load( $params, '' ) ) {
			throw new \Exception( 'Не переданы параметры для генерации PDF' );
		}

		$res = $model->generate();

		if ( is_a( $res, PDF::class ) ) {
			$msg = is_string( $res ) ? 'Ошибка при генерации PDF, обратитесь к администратору' : $res->firstErrors;
			throw new \Exception( $msg );
		}

		return $res;
	}
	/*
	* Экшен генерации pdf из переданного контента, включая HTML + CSS
	*
	* @return array - вернет результирующий массив
	*/
    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function actionPdfFile() {
        $request = Yii::$app->request->get();

        if (
            empty( $request )
            || ! isset( $request['request_sum'] )
            || ! isset( $request['return_date'] )
            || ! isset( $request['get_year_credit'])
        ) {
            throw new \Exception( 'Некорректно указаны параметры для получения файла графика платежей' );
        }
        $model = new CMRService( [ 'scenario' => CMRService::SCENARIO_SMART_PAYMENT_CHART ] );
        $model->load( [
                          'amount' => $request['request_sum'],
                          'date'   => $request['return_date']
                      ], '' );
        $res = $model->smartPaymentChart();

        if ( is_a( $res, CMRService::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }
        if ( is_string( $res ) ) {
            throw new \Exception( $res );
        }
        //доп. инфа о кредитном продукте
        $product = ( new Products )->getDataByGuid( $res->product, [
            "guid",
            "name",
            "is_additional_loan",
            "percent_per_day",
            "month_num",
            "summ_min",
            "summ_max",
            "age_min",
            "age_max",
            "loan_period_min",
            "loan_period_max",
        ] );
        if ( empty( $product ) || is_a( $product, \Exception::class ) ) {
            throw new \Exception( 'При попытке получения графика платежей не удалось получить данные кредитного продукта, обратитесь к администратору' );
        }
        $res->product_info = $product;


        $text = '';
        $i = 1;
        foreach ( $res->full->schedule as $item ) {
            $text .= '<tr>
                    <th scope="row">'.$i++.'</th> 
                    <td>'.date_format(date_create($item->date),"d.m.Y").'</td>
                    <td>
                     '.str_replace('.', ',', $item->total).'
                      <span class="rub">руб.</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="table__gray">
                            '.str_replace('.', ',', $item->debt).'
                            <span class="rub">руб.</span>
                        </span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="table__gray">
                            '.str_replace('.', ',', $item->percent).'
                            <span class="rub">руб.</span>
                        </span>
                    </td>
                    <td>
                     '.str_replace('.', ',', $item->rest).'
                        <span class="rub">руб.</span>
                    </td>
                </tr>';
        }

        $reqBody = array (
            'content' => '
        <div>
        <p class="graph-notice">Вы можете погасить свой заем без дополнительной
    комиссии в любое удобное время.
    Даже через день после получения денег. Тем не менее, мы даем Вам возможность
    пользоваться нашими деньгами в течение '.$request['get_year_credit'].'.</p>
           <div class="table-wrap">
            <table class="table">
            <thead><tr><th scope="col">№</th> <th scope="col">Дата оплаты</th> <th scope="col">Сумма платежа</th> <th scope="col" class="d-none d-sm-table-cell">Основной долг</th> <th scope="col" class="d-none d-sm-table-cell">Процент</th> <th scope="col">Сумма задолженности</th></tr></thead> 
            <tbody>'.
                         $text
                         .'
            </tbody>
            </table>
            </div> 
            <p>
                Общая сумма выплат по окончании пользования средствами:
                '.str_replace('.', ',', $res->full->totalAmount).'
                руб.
            </p>
  </div>',
            'title' => 'Graph_'.$request['request_sum'].'_'.date_format(date_create(),"d.m.Y").'-'.date_format(date_create($item->date),"d.m.Y"),
        );

        Yii::$app->response->format = Response::FORMAT_RAW;
        return $this->generate($reqBody);

	}

	/*
	* Экшен генерации pdf из переданного контента, включая HTML + CSS
	*
	* @return array - вернет результирующий массив
	*/
    /**
     * @return array|mixed
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionPdf() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        if ( array_key_exists( 'content', $reqBody ) && !empty( $reqBody['content'] ) && strpos( $reqBody['content'], '>q<' ) !== false ) {
            $reqBody['content'] = str_ireplace( '>q<', '>руб.<', $reqBody['content'] );
        }

        return $this->generate($reqBody);
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}
