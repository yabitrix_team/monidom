<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Agents;
use app\modules\app\v1\models\Partners;
use app\modules\app\v1\models\Points;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Users;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class SupportController - контроллер взаимодействия с формой обращения в ТП
 *
 * @package app\controllers\app\v1\service
 */
class SupportController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	public $listMimeTypeImage = [
		//картинки
		'png'  => 'image/png',
		'jpg'  => 'image/jpeg',
		'jpe'  => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'gif'  => 'image/gif',
		'bmp'  => 'image/bmp',
		'bmp1' => 'image/x-ms-bmp',
	];
	/**
	 * @var array - массив разрешенных MIME-тип, при регистрации компонента можно переопределить
	 */
	public $listMimeType = [
		//картинки
		'png'   => 'image/png',
		'jpg'   => 'image/jpeg',
		'jpe'   => 'image/jpeg',
		'jpeg'  => 'image/jpeg',
		'gif'   => 'image/gif',
		'tiff'  => 'image/tiff',
		'tif'   => 'image/tiff',
		'bmp'   => 'image/bmp',
		'bmp1'  => 'image/x-ms-bmp',
		//документы
		'pdf'   => 'application/pdf',
		'doc'   => 'application/msword',
		'docx'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'xls'   => 'application/vnd.ms-excel',
		'xls1'  => 'application/vnd.ms-office',
		'xlsx'  => 'application/octet-stream',
		'xlsx1' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'ppt'   => 'application/vnd.ms-powerpoint',
		'pptx'  => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'odt'   => 'application/vnd.oasis.opendocument.text',
		'txt'   => 'text/plain',
	];

    /**
     * Метод экшен - для отправки сообщения в ТП или разработчикам (метод реализован для канала web)
     *
     * @return
     * @throws \Exception
     */
	public function actionCreate() {

        //todo[hdcy]: 13.04.2018 Добавить прием информации из SessionStorage и LocalStorage
        $reqMessage     = Yii::$app->request->post( 'message' );
        $reqEmail       = Yii::$app->request->post( 'email' );
        $reqUrl         = Yii::$app->request->post( 'code' ); // код заявки
        $reqBrowser     = Yii::$app->request->post( 'browser' );
        $reqOs          = Yii::$app->request->post( 'os' );
        $reqSizeBrowser = Yii::$app->request->post( 'sizeBrowser' );
        $reqSizeScreen  = Yii::$app->request->post( 'sizeScreen' );
        $reqErrorForDev = Yii::$app->request->post( 'error' );
        if ( empty( $reqMessage ) && count( $_FILES ) == 0 ) {
            throw new \Exception( "Приложите файлы с ошибкой или опишите проблему" );
        }
        $arrData = [];
        if ( ! empty( $reqUrl ) ) {
            $requestData = ( new Requests() )->getByCode( $reqUrl );
            $arrData     = $arrData + [
                    'code'   => $reqUrl,
                    'num'    => $requestData['num'],
                    'client' => implode( ' ', [
                        //Здесь добавляем в письмо поля из заявки
                        $requestData['client_first_name'],
                        $requestData['client_last_name'],
                        $requestData['client_patronymic'],
                    ] ),
                ];
        }
        if ( ! empty( $_FILES ) ) {
            foreach ( $_FILES as $file ) {
                if ( $error = $this->checkErrorFile( $file ) ) {
                    throw new \Exception( $error );
                }
            }
            $arrData = $arrData + [ 'files' => $_FILES ];
        }
        if ( ! empty( $reqEmail ) ) {
            $arrData = $arrData + [ 'email' => $reqEmail ];
        }
        if ( ! empty( $reqMessage ) ) {
            $arrData = $arrData + [ 'message' => $reqMessage ];
        }
        if ( ! empty( $reqBrowser ) ) {
            $arrData = $arrData + [ 'browser' => $reqBrowser ];
        }
        if ( ! empty( $reqOs ) ) {
            $arrData = $arrData + [ 'os' => $reqOs ];
        }
        if ( ! empty( $reqSizeBrowser ) ) {
            $arrData = $arrData + [ 'sizeBrowser' => $reqSizeBrowser ];
        }
        if ( ! empty( $reqSizeScreen ) ) {
            $arrData = $arrData + [ 'sizeScreen' => $reqSizeScreen ];
        }
        if ( ! empty( $reqErrorForDev ) && $reqErrorForDev == 'true' ) {
            $arrData = $arrData + [ 'error' => true ];
        }
        //Добавляем данные если пользователь авторизован
        if ( ! Yii::$app->user->isGuest ) {
            $user    = Users::findIdentity( Yii::$app->user->getId() );
            $arrData = $arrData + [
                    'user_phone' => $user['username'],
                    'user_fio'   => implode( ' ', [
                        //Здесь добавляем в письмо поля из заявки
                        $user['first_name'],
                        $user['last_name'],
                        $user['second_name'],
                    ] ),
                    'user_email' => $user['email'],
                ];
            //Добавляем информацию по точке и партнера по пользователю
            if ( ! empty( $user['point_id'] ) ) {
                $point = ( new Points() )->getOne( $user['point_id'] );
                if ( ! empty( $point ) ) {
                    $partnerByPoint = ( new Partners() )->getOne( $point['partner_id'] );
                }
                if ( $point ) {
                    $arrData = $arrData + [ 'point' => $point['name'] ];
                    if ( $partnerByPoint ) {
                        $arrData = $arrData + [ 'partner_point' => $partnerByPoint['name'] ];
                    }
                }
            }
            //Добавляем агента и партнера агента пользователя
            if ( ! empty( $user['agent_id'] ) ) {
                $agent = ( new Agents() )->getOne( $user['agent_id'] );
                if ( ! empty( $agent ) ) {
                    $partnerByAgent = ( new Partners() )->getOne( $agent['partner_id'] );
                }
                if ( $agent && $partnerByAgent ) {
                    $arrData = $arrData + [ 'partner_agent' => $partnerByAgent['name'] ];
                }
            }
        }
        $user_agent = [ 'user_agent' => Yii::$app->request->userAgent ];
        $arrData    = $arrData + $user_agent;
        //первое изображение добавляем в тело письма, остальные файлы в аттач
        $isFirstImageLoaded = false;
        $arrayFileToAttach  = []; //файлы для добавления в аттач
        $imageFileToEmbed   = ''; //файл изображения для добавления в тело письма
        if ( isset( $arrData['files'] ) ) {
            foreach ( $arrData['files'] as $file ) {

                //Если файлы отправлены не были, то массив пустой или размер файла в массиве нулевой, тогда файлы не добавляем к сообщению
                if ( $file['size'] > 0 ) {

                    if ( ! is_uploaded_file( $file['tmp_name'] ) ) {
                        throw new \Exception( 'Не удалось загрузить файл "' . $file['name'] . '" на сервер' );
                    }
                    //проверка на допустимые MIME-типы
                    $mimeType = mime_content_type( $file['tmp_name'] );
                    if ( ! in_array( $mimeType, $this->listMimeType ) ) {
                        throw new \Exception( 'Попытка загрузки файла с недопустимым расширением' );
                    }
                    //проверка имени файла на наличие расширения и формирование нового имени
                    $extArray = explode( '.', $file['name'] );
                    $ext      = end( $extArray );
                    //проверка имени файла на наличие расширения и формирование нового имени
                    $dirTempPath = sys_get_temp_dir() . '/';
                    if ( array_key_exists( $ext, $this->listMimeType ) ) {

                        $fileNameNotExt = explode( '.', $file['name'] );
                        $fileNameNotExt = reset( $fileNameNotExt );
                        $fileName       = $this->generateFileName( $dirTempPath, $fileNameNotExt, $ext );
                    } else {
                        $ext      = array_search( $mimeType, $this->listMimeType );
                        $fileName = $this->generateFileName( $dirTempPath, $file['name'], $ext );
                    }
                    $filePath = $dirTempPath . $fileName;
                    if ( ! move_uploaded_file( $file['tmp_name'], $filePath ) ) {
                        throw new \Exception( "Не удалось загрузить файл по временному пути" );
                    }
                    //Если файл это первая картинка добавляем в тело сообщения, иначе в аттач
                    if ( ! $isFirstImageLoaded && array_key_exists( $ext, $this->listMimeTypeImage ) ) {
                        $isFirstImageLoaded = true;
                        $imageFileToEmbed   = $filePath;
                    } else {
                        array_push( $arrayFileToAttach, [ $fileName => $filePath ] );
                    }
                }
            }
        }
        //Подготавливаем тему письма
        if ( $arrData['error'] ) {
            $subject = "Сообщение об ошибке :";
            // Отключено по просьбе Юрия Бурова 18.10.2018
            return $this->getClassAppResponse()::get( [ 'send' => true ] );
        } elseif ( empty( $arrData['num'] ) ) {
            $subject = 'Сообщение для ТП.';
        } else {
            $subject = 'Сообщение для ТП. Заявка №' . $arrData['num'];
        }
        //Получатель
        $emailTo = $arrData['error'] ?
            Yii::$app->params['core']['email']['error'] :
            Yii::$app->params['core']['email']['supportPhoto'];
        if ( empty( $emailTo ) ) {
            return $this->getClassAppResponse()::get( false, false, 'В настройках сервера не задан E-mail ТП или Dev. Обратитесь к администратору' );
        }
        //готовим сообщение
        $sendMail = Yii::$app->mailer->compose( 'support', [
            'code'             => empty( $arrData['code'] ) ? null : $arrData['code'],
            'num'              => empty( $arrData['num'] ) ? null : $arrData['num'],
            'client'           => empty( $arrData['client'] ) ? null : $arrData['client'],
            'email'            => empty( $arrData['email'] ) ? null : $arrData['email'],
            'messageText'      => empty( $arrData['message'] ) ? null : $arrData['message'],
            'browser'          => empty( $arrData['browser'] ) ? null : $arrData['browser'],
            'os'               => empty( $arrData['os'] ) ? null : $arrData['os'],
            'sizeBrowser'      => empty( $arrData['sizeBrowser'] ) ? null : $arrData['sizeBrowser'],
            'sizeScreen'       => empty( $arrData['sizeScreen'] ) ? null : $arrData['sizeScreen'],
            'user_phone'       => empty( $arrData['user_phone'] ) ? null : $arrData['user_phone'],
            'user_fio'         => empty( $arrData['user_fio'] ) ? null : $arrData['user_fio'],
            'user_email'       => empty( $arrData['user_email'] ) ? null : $arrData['user_email'],
            'point'            => empty( $arrData['point'] ) ? null : $arrData['point'],
            'partner_point'    => empty( $arrData['partner_point'] ) ? null : $arrData['partner_point'],
            'partner_agent'    => empty( $arrData['partner_agent'] ) ? null : $arrData['partner_agent'],
            'user_agent'       => $arrData['user_agent'],
            'imageFileToEmbed' => $imageFileToEmbed,
        ] )
                                     ->setFrom( $arrData['user_email'] ?:
                                                    Yii::$app->params['core']['email']['from'] )
                                     ->setTo( $emailTo )
                                     ->setSubject( $subject );
        //добавляем файлы в аттач если есть
        foreach ( $arrayFileToAttach as $file ) {
            $sendMail->attach( reset( $file ), [ 'fileName' => key( $file ) ] );
        }
        $sendMail->send();
        if ( $sendMail ) {
            //todo[hdcy]: 17.04.2018 Выводить сообщение в лог если файлы не удалось удалить или в БД (согласовать)
            foreach ( $arrayFileToAttach as $file ) {
                @unlink( reset( $file ) );
            }
            if ( $imageFileToEmbed ) {
                @unlink( $imageFileToEmbed );
            }

            return $this->getClassAppResponse()::get( [ 'send' => true ] );
        }

        return $this->getClassAppResponse()::get( false, false, "Неизвестная ошибка при отправке сообщения. Обратитесь к администратору" );
	}
	/*
	 * @var array - массив типов изображений, первый файл этого типа будет добавлен в тело письма, а не в аттач.
	 */
	/**
	 * Метод проверки загрузки файла на наличие ошибки
	 *
	 * @param $file - массив содержащий данные файла
	 *
	 * @return bool|string - вернет ложь если без ошибок или вернет тест ошибки
	 */
	protected function checkErrorFile( $file ) {

		try {
			if ( $file['error'] > 0 ) {
				switch ( $file['error'] ) {
					case UPLOAD_ERR_INI_SIZE:
						throw new \Exception( 'Размер загружаемого файла ' . $file['name'] . ' превысил максимально допустимый размер' );
					case UPLOAD_ERR_FORM_SIZE:
						throw new \Exception( 'Размер загружаемого файла ' . $file['name'] . ' превысил максимально допустимое значение' );
					case UPLOAD_ERR_PARTIAL:
						throw new \Exception( 'Загружаемый файл ' . $file['name'] . ' был получен только частично' );
					case UPLOAD_ERR_NO_TMP_DIR:
						throw new \Exception( 'Отсутствует временная папка для сохранения файла' );
					case UPLOAD_ERR_CANT_WRITE:
						throw new \Exception( 'Не удалось записать файл ' . $file['name'] . ' на диск' );
					case UPLOAD_ERR_EXTENSION:
						throw new \Exception( 'PHP-расширение остановило загрузку файла ' . $file['name'] );
				}
			}

			return false;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод генерации именя файла при наличии такового имени в системе
	 *
	 * @param     $dirPath        - путь до директории сохранения файла
	 * @param     $fileNameNotExt - название файла без расширения
	 * @param     $ext            - расширение файла
	 * @param int $i              - числовой номер файла, для формирования имени
	 *
	 * @return string - вернет строку названия файла с расширением
	 */
	protected function generateFileName( $dirPath, $fileNameNotExt, $ext, $i = 0 ) {

		if ( file_exists( $dirPath . $fileNameNotExt . '.' . $ext ) ) {
			if ( $pos = mb_strrpos( $fileNameNotExt, '_' . $i ) ) {
				$fileNameNotExt = mb_substr( $fileNameNotExt, 0, $pos );
			}
			$fnNew = $fileNameNotExt . '_' . ++ $i;

			return $this->generateFileName( $dirPath, $fnNew, $ext, $i );
		}

		return $fileNameNotExt . '.' . $ext;
	}

	/**
	 * Метод поведения для настройки заголовков
	 *
	 * @return array - вернет массив, в котором указаны настройки для заголовков
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}

	protected function verbs() {

		return [];
	}
}