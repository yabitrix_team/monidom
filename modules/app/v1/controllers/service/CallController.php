<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Call;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class CallController  - контроллер взаимодействия с формой обратного звонка
 *
 * @package app\controllers\app\v1\service
 */
class CallController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Метод экшен - для отправки сообщения формой обратного звонка
     *
     * @return
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionCreate() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        if ( empty( $reqBody['login'] ) ) {
            throw new \Exception( 'Не задан логин (телефон) пользователя' );
        }
        $call = new Call();
        if ( ! $call->load( [ 'login' => $reqBody['login'] ], '' ) ) {
            throw new \Exception( 'Не удалось загрузить логин пользователя. Обратитесь к администратору' );
        }
        if ( ! $call->validate() ) {
            return $this->getClassAppResponse()::get( false, $call->firstErrors );
        }
        $res = $call->send();
        if ( $res ) {
            return $this->getClassAppResponse()::get( [ 'send' => true ] );
        } elseif ( $res == false ) {
            $res = "Неизвестная ошибка при отправке сообщения. Обратитесь к админисратору";
        }

        return $this->getClassAppResponse()::get( false, false, $res );
	}

	/**
	 * Метод поведения для настройки заголовков
	 *
	 * @return array - вернет массив, в котором указаны настройки для заголовков
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}

	protected function verbs() {

		return [
		];
	}
}