<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\cmr\CMRService;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class CalcOptionsController - контроллер получения параметров расчета
 *                              для калькулятора, относительно кредитных продуктов
 *
 * @package app\controllers\app\v1\service
 */
class CalcOptionsController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	/**
	 * @var array - ассоциация полей полученных от сервиса через SOAP с полями контракта
	 */
	protected $assocFields = [
		'Periods' => 'period',
		'MinSum'  => 'min_sum',
		'MaxSum'  => 'max_sum',
		'Step'    => 'step',
	];

    /**
     * Экшен - получения параметров расчета
     *
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionCreate() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $reqBody = ! empty( $reqBody ) ? $reqBody : [ 'client' => 0, 'params' => 0 ];
        $model   = new CMRService( [ 'scenario' => CMRService::SCENARIO_CALC_OPTIONS ] );
        if ( ! $model->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не переданы данные для получения параметров расчета калькулятора' );
        }
        $res = $model->getCalcOptions();
        if ( is_a( $res, CMRService::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }
        if ( is_string( $res ) ) {
            throw new \Exception( $res );
        }
        $options = [];
        if ( ! empty( $res ) && ! empty( $this->assocFields ) ) {
            foreach ( $res as $k => $item ) {
                ! isset( $this->assocFields['Periods'] ) ?:
                    $options[ $k ][ $this->assocFields['Periods'] ] = $item->Periods;
                ! isset( $this->assocFields['MinSum'] ) ?:
                    $options[ $k ][ $this->assocFields['MinSum'] ] = $item->MinSum;
                ! isset( $this->assocFields['MaxSum'] ) ?:
                    $options[ $k ][ $this->assocFields['MaxSum'] ] = $item->MaxSum;
                ! isset( $this->assocFields['Step'] ) ?: $options[ $k ][ $this->assocFields['Step'] ] = $item->Step;
            }
        } else {
            $options = $res;
        }

        return $this->getClassAppResponse()::get( [ 'options' => $options ] );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}