<?php
namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\models\UserGroup;
use app\modules\app\v1\models\Users;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class RegisterController - контроллер для регистрации юзера из других систем
 *
 * @package app\modules\in\v1\controllers\service
 */
class RegisterController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен проверки регистрации пользователя для внешней системы
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreate()
    {

        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $canSend = true; //разрешена ли отправка SMS сообщения
        $user    = Users::getByLogin($reqBody['phone']);
        if (is_string($user)) {
            throw new \Exception('Произошла ошибка получения данных пользователя, обратитесь к администратору');
        }
        if (!empty($user) && isset($user['id'])) {
            //проверяем если пользователь МП, то СМС не отправляем
            $build = Users::instance()->build($user['id']);
            if (!empty($build) && is_object($build) && $build->isMobileUser()) {
                $canSend = false;
            }
            //Отправляем СМС
            $smsText = 'Здравствуйте '.
                       $user['first_name'].
                       ', войти в свой личный кабинет вы можете по адресу '.
                       $_SERVER['REQUEST_SCHEME'].
                       '://client.'.
                       Yii::$app->params['core']['domain'];
            $message = [
                'phone'   => $user['username'],
                'message' => $smsText,
            ];
            if (!empty($canSend)) {
                $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
                $modelSms->load($message, '');
                $modelSms->setLogCategory('sms.enterlink.exists')->sendData();
            }

            return $this->getClassAppResponse()::get(['exists' => true]);
        }
        $guid  = Uuid::uuid4();
        $token = Uuid::uuid4();
        $user  = (new Users)->create(
            [
                'username'             => $reqBody['phone'],
                'first_name'           => $reqBody['firstName'],
                'last_name'            => $reqBody['lastName'],
                'second_name'          => $reqBody['patronymic'],
                'password_reset_token' => $token,
                'guid'                 => $guid,
            ]
        );
        if (empty($user) || is_string($user)) {
            Log::error(
                [
                    "user_login" => $reqBody['phone'],
                    "text"       => 'Ошибка: '.$user,
                ],
                "user.register.partner"
            );
            throw new \Exception('Не удалось создать пользователя.');
        }
        $userGroup = (new UserGroup)->create(
            [
                'user_id'  => $user,
                'group_id' => Yii::$app->params['console_params']['params']['exchange']['user_groups']['clients'],
            ]
        );
        if (empty($userGroup) || is_string($userGroup)) {
            throw new \Exception('Не удалось создать группу.');
        }
        Log::info(
            [
                "user_id"    => $user,
                "user_login" => $reqBody['phone'],
                "text"       => 'Имя: '.
                                $reqBody['firstName'].
                                ' | Фамилия: '.
                                $reqBody['lastName'].
                                '| Отчество: '.
                                $reqBody['patronymic'],
            ],
            "user.register.partner"
        );
        $smsText  = 'Здравствуйте '.
                    $reqBody['firstName'].
                    ', ссылка для первого входа в личный кабинет '.
                    $_SERVER['REQUEST_SCHEME'].
                    '://login.'.
                    Yii::$app->params['core']['domain'].
                    '/new/'.
                    $token;
        $message  = [
            'phone'   => $reqBody['phone'],
            'message' => $smsText,
        ];
        $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
        $modelSms->load($message, '');
        $modelSms->setLogCategory('sms.enterlink.new')->sendData();
        return $this->getClassAppResponse()::get(
            [
                'created' => true,
                'guid'    => $guid,
            ]
        );
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
        ];
    }
}
