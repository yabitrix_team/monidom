<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\ComOffer;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class ComOfferController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return mixed|string
     * @throws \Exception
     */
	public function actionIndex() {

        $model = new ComOffer();
        $data  = $model->getCodeName();
        if ( is_string( $data ) ) {
            throw new \Exception( $data );
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}
