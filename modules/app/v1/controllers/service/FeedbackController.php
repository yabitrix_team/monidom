<?php

namespace app\modules\app\v1\controllers\service;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Feedback;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\JsonParser;
use yii\web\Response;

/**
 * Class FeedbackController  - контроллер взаимодействия с формой обратной связи
 *
 * @package app\controllers\app\v1\service
 */
class FeedbackController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Метод экшен - для отправки сообщения формы обратной связи
     *
     * @return
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
	public function actionCreate() {

	    try {
            $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
            if ( empty( $reqBody['name'] ) || empty( $reqBody['email'] ) || empty( $reqBody['message'] ) ) {
                throw new \Exception( 'Не заданые входные параметры' );
            }
            $feedback = new Feedback();
            if ( ! $feedback->load( $reqBody, '' ) ) {
                throw new \Exception( "Не удалось загрузить параметр из входящего запроса. Обратитесь к администратору" );
            }
            if ( ! $feedback->validate() ) {
                return $this->getClassAppResponse()::get( false, $feedback->firstErrors );
            }
            $res = $feedback->send();
            if ( $res ) {
                return $this->getClassAppResponse()::get( [ 'send' => true ] );
            } elseif ( $res == false ) {
                $res = "Неизвестная ошибка при отправке сообщения. Обратитесь к администратору";
            }

            return $this->getClassAppResponse()::get( false, false, $res );
        } catch (\yii\web\BadRequestHttpException $e) {
            return $this->getClassAppResponse()::get( false, false, $e->getMessage(), $e->getCode() );
        }
	}

	/**
	 * Метод поведения для настройки заголовков
	 *
	 * @return array - вернет массив, в котором указаны настройки для заголовков
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::className(),
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::className(),
				'actions' => $this->verbs(),
			],
		];
	}

	protected function verbs() {

		return [
		];
	}
}