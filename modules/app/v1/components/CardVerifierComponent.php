<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\Requests;
use Yii;
use yii\base\Component;
use yii\web\JsonParser;

/**
 * Class SoapClientComponent - компонент для подключения через SOAP клиент
 *
 * @package app\modules\app\v1\components\soap
 */
class CardVerifierComponent extends Component
{
    /**
     * @var string - url сервиса
     */
    public $url = 'https://payment.hashconnect.eu';
    /**
     * @var string - приватный ключ
     */
    public $key_private = '';
    /**
     * @var string - приватный ключ
     */
    public $key_public = '';
    /**
     * @var string - набор uri для проведения запросов
     */
    public $uri = [
        'operations' => '/card/{siteId}/operations/{operationId}',
        'link'       => '/paymentpageapi/{siteId}/create_payment_link',
        'purchase'   => '/card/{siteId}/operations/purchase',
        'refund'     => '/card/{siteId}/operations/{operationId}/refund',
    ];
    /**
     * @var string - id в системе W1
     */
    public $site_id = '1-1';
    /**
     * @var bool - дебаг
     */
    public $debug = false;
    /**
     * @var bool - https : true | false
     */
    public $https = false;
    /**
     * @var array|null данные для отправки
     */
    protected $sendData = null;
    /**
     * @var null - ответ от сервиса W1
     */
    protected $response = null;
    /**
     * @var string|null - code в таблице bank_card
     */
    protected $externalId = null;
    /**
     * @var string|null - код операции в W1
     */
    protected $operationId = null;
    /**
     * @var null - цепочка для связи событий
     */
    protected $chain_id = null;
    /**
     * @var string|null - code операции
     */
    protected $request_code = null;
    /**
     * @var string|null - хост, с которого делается запрос
     */
    protected $host = null;
    /**
     * @var string|null - кабинет, поддомен
     */
    protected $cabinet      = null;
    protected $cabinetAssoc = [
        'partner' => 'web',
        'client'  => 'client',
    ];
    /**
     * @var null - сумма платежа
     */
    protected $amount = null;
    /**
     * @var array  Операция => функция
     */
    protected $operationAction = [
        'link'   => 'prepareRequestLink',
        'refund' => 'prepareRequestRefund',
    ];

    /**
     * Метод инициализации
     *
     * @throws \Exception
     */
    public function init()
    {

        parent::init();
        $this->generateExternalId();
        $this->host    = ($this->https
                ? "https://"
                :
                "http://").(!empty($_SERVER['HTTP_X_FORWARDED_SERVER'])
                ? $_SERVER['HTTP_X_FORWARDED_SERVER']
                :
                $_SERVER['SERVER_NAME']);
        $this->cabinet = explode(
            ".",
            (!empty($_SERVER['HTTP_X_FORWARDED_SERVER']) ? $_SERVER['HTTP_X_FORWARDED_SERVER']
                : $_SERVER['SERVER_NAME'])
        )[0];

        $this->cabinetAssoc = !empty(Yii::$app->params['core']['cabinetAssoc']) ? Yii::$app->params['core']['cabinetAssoc'] :  $this->cabinetAssoc ;
    }

    /**
     * ф-я запроса адреса фрейма/страницы для проверки карты в системе W1
     * Запрос отправляет фронт,
     * {
     *  'code' : 'de236a51c95272d01a00cc58d1c2ad90' - код заявки
     * }
     *
     * @return \Exception|mixed
     */
    public function getCreate(string $operationName)
    {

        try {

            if (empty($this->getExternalId()) or empty($this->getRequestCode())) {
                Log::error(
                    ['text' => print_r(['getCreate()', 'Не указан externalId или request_code'], 1)],
                    'card.verifier.log'
                );
                throw new \Exception('Не указан externalId или request_code. Обратитесь к администратору.');
            }
            $function = $this->operationAction[$operationName];
            $this->$function();
            $this->response = $this->signSend($this->sendData, 'POST', $operationName);
            if (is_a($this->response, \Exception::class)) {

            }
            // todo: обновить таблицу
            Log::info(
                ['text' => print_r(['getCreate()', 'запрос' => $this->sendData, 'ответ' => $this->response], 1)],
                'card.verifier.log'
            );

            return $this->response;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return array
     * @throws \yii\web\BadRequestHttpException
     */
    public function getCallback()
    {
        return true;
    }

    /**
     * урл заявки по коду
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->getHost().'/request/'.$this->getRequestCode();
    }

    public function getSuccess()
    {
        return true;
    }

    public function getDecline()
    {
        return true;
    }

    /**
     * подписать и отправить запрос
     *
     * @param array|null  $sendData - массив данных
     * @param string      $method   - метод запроса GET|POST
     * @param string|null $uri_name - uri запроса
     * @param string|null $key      - приватный ключ (config/key-private.php)
     *
     * @return \Exception|mixed - вернет массив с ответом
     */
    protected function signSend(
        array $sendData = null,
        string $method = 'POST',
        string $uri_name = null,
        string $key = null
    ) {

        try {
            if (empty($method) || empty($uri_name)) {
                throw new \Exception('Данные, метод или uri - не указаны.');
            }
            $method = $method == 'POST' ? $method : 'GET';
            if (empty($this->key_private) && empty($key)) {
                throw new \Exception('Цифровая подпись не указана.');
            }
            $key = !empty($this->key_private) ? $this->key_private : $key;
            if (empty($sendData)) {
                $sendData = '';
            } else {
                $sendData = json_encode($sendData, JSON_UNESCAPED_UNICODE);
            }
            $privateEncryptString = $this->sign($method, $this->getUrl($uri_name), $sendData, $key);
            Log::info(
                [
                    'text' => print_r(
                        [
                            'signSend '.__LINE__,
                            $method,
                            $this->getUrl($uri_name),
                            $this->getUrl('purchase', 1),
                            $sendData,
                            $key,
                        ],
                        1
                    ),
                ],
                'card.verifier.log'
            );
            if ($this->debug) {

                Log::info(
                    ['text' => [$method, $this->getUrl($uri_name), $this->getUrl($uri_name, 1), $sendData, $key]],
                    'card.check.debug'
                );
                print_r([__FILE__.":".__LINE__, $sendData]);
            }
            $url = $this->getUrl($uri_name, 1);
            $ch  = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $sendData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                [
                    'W1-Signature: '.$privateEncryptString,
                    'Content-Type: application/json',
                    'Content-Length: '.strlen($sendData),
                ]
            );
            $result = curl_exec($ch);
            if ($this->debug) {
                print_r([curl_getinfo($ch)]);
            }
            curl_close($ch);
            $arData = json_decode($result);

            return $arData;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * получить uri | url
     *
     * @param string $uri_name   - получиьт uri по псевдониму
     * @param bool   $return_url - получить полный урл true | false
     *
     * @return \Exception|mixed|string
     */
    protected function getUrl(string $uri_name, bool $return_url = false)
    {

        try {
            $uri = $this->uri[$uri_name];
            if (empty($uri)) {
                throw new \Exception('Не верно указано навзвание операции');
            }
            $url = str_replace('{siteId}', $this->site_id, $uri);
            $url = str_replace('{operationId}', $this->getOperationId(), $url);
            if ($this->debug) {
                print_r([__FILE__.":".__LINE__, $uri, $url]);
            }
            if ($return_url) {
                $url = $this->url.$url;
            }

            return $url;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return null|string
     */
    public function getCabinet(): ?string
    {
        return $this->cabinet;
    }

    /**
     * @return null
     */
    public function getChainId()
    {
        return $this->chain_id;
    }

    /**
     * @param null $chain_id
     */
    public function setChainId($chain_id)
    {
        $this->chain_id = $chain_id;

        return $this;
    }

    protected function prepareRequestLink()
    {
        //$this->setAmount(rand(101, 999) / 100);
        $this->setAmount(1);
        $this->sendData = [
            'action'             => 'purchase',
            'amount'             => [
                'amount'   => $this->getAmount(),
                'currency' => 'RUB',
            ],
            'externalId'         => $this->getExternalId(),
            'merchantParameters' => [
                'param1' => $this->getExternalId(), // id операции
                'param2' => $this->getRequestCode(), // code заявки
                'param3' => $this->getChainId(), // id цепи запросов
            ],
            'notification'       => [
                'url' => $this->getHost().'/'.$this->cabinetAssoc[$this->cabinet].'/v1/service/card-verifier/callback',
            ],
            'pageParameters'     => [
                'description'  => 'Проверка карты',
                'urlOnDecline' => $this->host.
                                  '/'.
                                  $this->cabinetAssoc[$this->cabinet].
                                  '/v1/service/card-verifier/decline/'.
                                  $this->getRequestCode(),
                'urlOnSuccess' => $this->host.
                                  '/'.
                                  $this->cabinetAssoc[$this->cabinet].
                                  '/v1/service/card-verifier/success/'.
                                  $this->getRequestCode(),
            ],
            'registerRecurring'  => true,
        ];

        return $this;
    }

    protected function prepareRequestRefund()
    {

        $this->sendData = [
            'action'             => 'refund',
            'amount'             => [
                'amount'   => $this->getAmount(),
                'currency' => 'RUB',
            ],
            'externalId'         => $this->getExternalId(),
            'merchantParameters' => [
                'param1' => $this->getExternalId(),
                'param2' => $this->getRequestCode(),
                'param3' => $this->getChainId(),
            ],
            'notification'       => [
                'url' => $this->getHost().'/'.$this->cabinetAssoc[$this->cabinet].'/v1/service/card-verifier/callback',
            ],
        ];

        return $this;
    }

    /**
     * алгоритм подписи
     *
     * @param string $method          - метод
     * @param string $uri             - uri
     * @param string $data            - данные JSON
     * @param string $private_key_pem - приватный ключ
     *
     * @return string
     */
    protected function sign(string $method, string $uri, string $data, string $private_key_pem): string
    {
        openssl_sign("{$method}\n{$uri}\n{$data}", $signature, $private_key_pem, OPENSSL_ALGO_SHA256);

        return base64_encode($signature);
    }

    /**
     * @return null|string
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @return null|string
     */
    public function getPayHost(): ?string
    {

        $proto = ($this->https ? "https://" : "http://");
        $host  = explode(
            ".",
            (!empty($_SERVER['HTTP_X_FORWARDED_SERVER']) ? $_SERVER['HTTP_X_FORWARDED_SERVER']
                : $_SERVER['SERVER_NAME'])
        );

        return $proto.'pay.'.$host[1].'.'.$host[2];
    }

    /**
     * @param null|string $host
     */
    public function setHost(?string $host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * генерация кода операции
     *
     * @return $this
     */
    public function generateExternalId()
    {
        $this->externalId = md5(time().rand(1000, 9990));

        return $this;
    }

    /**
     * @return null|string
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param null|string $externalId
     */
    public function setExternalId(?string $externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getRequestCode(): ?string
    {
        return $this->request_code;
    }

    /**
     * @param null|string $request_code
     */
    public function setRequestCode(?string $request_code)
    {
        $this->request_code = $request_code;

        return $this;
    }

    /**
     * @return null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param null $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getOperationId(): ?string
    {
        return $this->operationId;
    }

    /**
     * @param null|string $operationId
     */
    public function setOperationId(?string $operationId)
    {
        $this->operationId = $operationId;

        return $this;
    }
}