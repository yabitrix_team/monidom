<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\exceptions\InvalidParamException;
use app\modules\app\v1\classes\logs\Log;
use yii\httpclient\Client;
use yii\base\BaseObject;
use Yii;

/**
 * Class LCRMComponent - компонент взаимодействия с сервисом LCRM
 *
 * @link    https://carmoney.ru/api/docs/#api-Leads-GetApiV1PrivateLeadsRegister
 * @package app\modules\app\v1\components
 */
class LCRMComponent extends BaseObject
{
    /**
     * Константы целей авторизации
     */
    public const AUTH_TARGET_REGISTRY_LKK    = 'registry_lkk'; //регистрация в ЛКК
    public const AUTH_TARGET_REGISTRY_MOBILE = 'registry_mobile_app'; //регистрация в МП
    /**
     * @var string - основная часть url-адреса сервиса LCRM,
     *             далее в рамках методов подставляется часть необходимого пути
     */
    public $url;
    /**
     * @var - цель атворизации, на данный момент предусмотрено две цели:
     *      registry_lkk - регистрация в ЛКК и registry_mobile_app - регистрация в МП
     */
    public $target;
    /**
     * @var string - токен авторизации
     */
    public $token;
    /**
     * @var string - название категории лога, если не указать, то логировать не будет
     */
    public $logCategory;
    /**
     * @var int - максимальное кол-во секунд, в течении которых может быть выполнен запрос
     */
    public $timeout = 5;
    /**
     * @var yii\httpclient\Request - экземпляр запроса httpclient
     */
    protected $request;

    /**
     * Метод инициализации компонента
     *
     * @throws
     */
    public function init(): void
    {
        parent::init();
        $isAllowedLog = false;
        try {
            if (!empty($this->logCategory)) {
                $category = array_column(Yii::$app->log->targets, 'categories');
                array_walk(
                    $category,
                    function ($v) use (&$isAllowedLog) {
                        !in_array($this->logCategory, $v, true) ?: $isAllowedLog = true;
                    }
                );
                if (empty($isAllowedLog)) {
                    throw new InvalidParamException('Категория логирования LCRM');
                }
            }
            if (!filter_var($this->url, FILTER_VALIDATE_URL)) {
                throw new InvalidParamException('URL-адрес LCRM');
            }
            if (
                empty($this->target) ||
                !in_array($this->target, [self::AUTH_TARGET_REGISTRY_LKK, self::AUTH_TARGET_REGISTRY_MOBILE], true)
            ) {
                throw new InvalidParamException('Цель авторизации LCRM');
            }
            if (empty($this->token)) {
                throw new InvalidParamException('Токен авторизации LCRM');
            }
            if (empty($this->timeout)) {
                throw new InvalidParamException('Время выполнения запроса');
            }
            $this->url     = rtrim($this->url, '/');
            $this->request = (new Client(['baseUrl' => $this->url]))
                ->createRequest()
                ->setOptions(
                    [
                        'timeout'       => $this->timeout,
                        'sslVerifyPeer' => defined('YII_ENV') && YII_ENV === 'prod',
                    ]
                )
                ->setFormat(Client::FORMAT_JSON)
                ->addHeaders(['x-auth-token' => $this->token, 'x-auth-target' => $this->target]);
        } catch (\Exception $e) {
            if (!empty($isAllowedLog)) {
                $params = [
                    'url'     => $this->url,
                    'target'  => $this->target,
                    'token'   => substr($this->token, 0, 5).'...',
                    'timeout' => $this->timeout,
                ];
                Log::error(
                    [
                        'text' => '**** НЕ УДАЛОСЬ ИНИЦИАЛИЗИРОВАТЬ КОМПОНЕНТ ВЗАИМОДЕЙСТВИЯ С СИСТЕМОЙ LCRM ****
Параметры подключения: '.print_r($params, true).'
Ошибка: '.$e->getMessage(),
                    ],
                    $this->logCategory
                );
            }
            throw new \RuntimeException('Ошибка интеграции к системе LCRM, обратитесь к администратору');
        }
    }

    /**
     * Метод предварительной регистрация лида в LCRM
     *
     * @param             $phone              - номер телефона
     * @param             $leadId             - идентификатор заявки в нашей системе
     * @param             $creatorId          - идентификатор пользователя/лидогенератора
     * @param string      $creatorName        - ФИО пользователя/лидогенератора
     * @param string      $cribClientId       - идентификатор из сервиса CRIB
     * @param string      $clientRegisteredAt - дата-время регистрации пользователя
     *
     * @return bool|int - вернет идентификатор заявки системы LCRM иначе ложь
     * @throws \Exception
     */
    public function leadsRegister(
        $phone,
        $leadId,
        $creatorId,
        string $creatorName,
        string $cribClientId = null,
        string $clientRegisteredAt = null
    ) {
        $data = [
            'phone'        => $phone,
            'lead_id'      => $leadId,
            'creator_id'   => $creatorId,
            'creator_name' => $creatorName,
        ];
        empty($cribClientId) ?: $data['crib_client_id'] = $cribClientId;
        empty($clientRegisteredAt) ?: $data['client_registered_at'] = $this->getDateTime($clientRegisteredAt);
        $partUrl = 'v1/private/leads/register';
        $params  = print_r(array_merge($data, ['url' => $this->url.'/'.$partUrl]), true);
        try {
            $response = $this->request
                ->setMethod('POST')
                ->setUrl($partUrl)
                ->setData($data)
                ->send();
            $result   = $response->getData();
            if (!empty($result['error']) || !isset($result['id'])) {
                $msg = !empty($result['error'])
                    ? $result['message']
                    : 'При интеграции с сервисом LCRM нарушен контракт взаимодействия, сервис не вернул id';
                throw new \RuntimeException($msg);
            }
            Log::error(
                [
                    'text' => '**** ПРЕДВАРИТЕЛЬНАЯ РЕГИСТРАЦИЮ ЛИДА В СИСТЕМЕ LCRM ****
Запрос: '.$params.'
Ответ: '.print_r($result, true),
                ],
                $this->logCategory
            );

            return $result['id'];
        } catch (\Exception $e) {
            if (!empty($this->logCategory)) {
                Log::error(
                    [
                        'text' => '**** НЕ УДАЛОСЬ ПРОИЗВЕСТИ ПРЕДВАРИТЕЛЬНУЮ РЕГИСТРАЦИЮ ЛИДА В СИСТЕМЕ LCRM ****
Запрос: '.$params.'
Ошибка: '.$e->getMessage(),
                    ],
                    $this->logCategory
                );
            }

            return false;
        }
    }

    /**
     * Метод провеорки состояния АПИ
     *
     * @return mixed - вернет результат ответа
     */
    public function ping()
    {
        $response = $this->request
            ->setMethod('GET')
            ->setUrl('v1/public/ping')
            ->send();

        return $response->getData();
    }

    /**
     * Метод получения даты в определенном формате
     *
     * @param string $time - дата в формате 2019-01-25 10:23:28
     *
     * @return string - вернет отфоратированную дату в формате Y-m-d\TH:i:sP
     * @throws \Exception
     */
    protected function getDateTime(string $time = 'now'): string
    {
        return (new \DateTime($time))->format("Y-m-d\TH:i:sP");
    }
}