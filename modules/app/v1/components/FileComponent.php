<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\File;
use Imagine\Image\Box;
use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * Class FileComponent - компонент для регистрации файлов в системе
 *
 * @package app\modules\app\v1\components
 */
class FileComponent extends Component
{
    /**
     * @var array - массив разрешенных MIME-тип, при регистрации компонента можно переопределить
     */
    public $listMimeType = [
        //картинки
        'png'  => 'image/png',
        'jpg'  => 'image/jpeg',
        'jpe'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'gif'  => 'image/gif',
        'bmp'  => 'image/bmp',
        'tiff' => 'image/tiff',
        'tif'  => 'image/tiff',
        //документы
        'pdf'  => 'application/pdf',
        'doc'  => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xls'  => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    ];
    /**
     * @var int - ширина превью, при регистрации компонента можно переопределить
     */
    public $previewWidth = 208;
    /**
     * @var int - высота превью, при регистрации компонента можно переопределить
     */
    public $previewHeight = 208;
    /**
     * @var int - качество сжимаемого превью, ри регистрации компонента можно переопределить
     */
    public $previewQuality = 90;
    /**
     * @var string - директория содержащая превью фотографий
     */
    protected $dirPreview = 'preview';
    /**
     * @var string - корень сайта
     */
    protected $webRoot;

    /**
     * Инициализация компонента, происходит после загрузки конфигурации
     */
    public function init()
    {
        parent::init();
        $this->webRoot = rtrim(Yii::getAlias('@webroot'), '/');
    }

    /**
     * Метод регистрации файла, включает сохранение файла физически и запись в таблицу БД file
     *
     * @param      $path     - путь относительно корня сайта до места сохранения,
     *                       если указанного пути нет, то создаст его
     * @param      $nameFile - название файла, допускается с указанием и без указания расширения
     * @param      $tmpFile  - путь до загруженного файла во временную папку или в другое место на сервере
     * @param null $desc     - описание файла, для записи в таблице file
     * @param null $prefix   - некий префикс идентификации символьного кода при формировании,
     *                       если указать то в начале символьного кода файла будет этот префикс
     *                       не должен превышать 12 символов, параметр необязателен
     * @param int  $active   - активность элемента, принимает 1 или 0
     * @param bool $isLocal  - активность элемента, принимает 1 или 0
     *
     * @return array|string - массив содержащий код, идентификатор файла и имя файла или строку с исключением
     *                      [
     *                          'id'=>'38',
     *                          'code'=>'224353e6d58ff3f4ccab277fbd24c6f6',
     *                          'fileName'=>'trey_5.jpg',
     *                      ]
     * @throws \Throwable
     */
    public function save($path, $nameFile, $tmpFile, $desc = null, $prefix = null, $active = 1, $isLocal = false)
    {
        if (!$isLocal && !is_uploaded_file($tmpFile)) {
            throw new \Exception('Не удалось загрузить файл "'.$nameFile.'" на сервер');
        }
        //проверка на допустимые MIME-типы
        $mimeType = mime_content_type($tmpFile);
        if (!in_array($mimeType, $this->listMimeType)) {
            throw new \Exception('Попытка регистрации файла с недопустимым расширением');
        }
        //создание директории хранения файлов
        $dirPath = $this->webRoot.'/'.trim($path, '/').'/';
        if (!file_exists($dirPath)) {
            if (empty(FileHelper::createDirectory($dirPath))) {
                throw new \Exception('Не удалось создать директорию для загрузки файла');
            }
        }
        //проверка имени файла на наличие расширения и формирование нового имени
        $extArray = explode('.', $nameFile);
        $ext      = end($extArray);
        if (array_key_exists($ext, $this->listMimeType)) {
            $fileNameNotExt = explode('.', $nameFile);
            $fileNameNotExt = reset($fileNameNotExt);
            $fileName       = $this->generateFileName($dirPath, $fileNameNotExt, $ext);
        } else {
            $ext      = array_search($mimeType, $this->listMimeType);
            $fileName = $this->generateFileName($dirPath, $nameFile, $ext);
        }
        //загрузка файлов
        $filePath = $dirPath.$fileName;
        if (!$isLocal) {
            if (!move_uploaded_file($tmpFile, $filePath)) {
                throw new \Exception("Не удалось загрузить файл по временному пути: '{$tmpFile} : {$filePath}'");
            }
        } else {
            if (!copy($tmpFile, $filePath)) {
                throw new \Exception("Не удалось загрузить файл по временному пути: '{$tmpFile} : {$filePath}'");
            }
        }
        //подготовка данных для записи в таблицу file
        $dataImg    = getimagesize($filePath);
        $fileParams = [
            'name'        => $fileName,
            'path'        => trim($path, '/').'/'.$fileName,
            'width'       => !empty($dataImg) ? $dataImg[0] : 0,
            'height'      => !empty($dataImg) ? $dataImg[1] : 0,
            'type'        => $mimeType,
            'size'        => filesize($filePath),
            'description' => $desc,
            'prefix'      => $prefix,
            'active'      => (int) $active,
        ];
        $modelFile  = new File();
        $modelFile->load($fileParams, '');
        if (!$modelFile->validate()) {
            throw new \Exception(
                'Ошибка валидации данных при регистрации файла '.$fileName
            );
        }
        $result = $modelFile->create();
        if (is_string($result)) {
            throw new \Exception($result);
        }
        //так же возвращаем новое имя файла
        $result['fileName'] = $fileName;
        Log::info(
            [
                "user_id"      => Yii::$app->user->getId(),
                "user_login"   => Yii::$app->user->getIdentity()->getUsername(),
                "user_browser" => Yii::$app->request->userAgent,
                "text"         => 'id: '.$result['id'].', url: '.Yii::$app->request->url,
            ],
            'photos.upload'
        );

        return $result;
    }

    /**
     * Метод генерации именя файла при наличии такового имени в системе
     *
     * @param     $dirPath        - путь до директории сохранения файла
     * @param     $fileNameNotExt - название файла без расширения
     * @param     $ext            - расширение файла
     * @param int $i              - числовой номер файла, для формирования имени
     *
     * @return string - вернет строку названия файла с расширением
     */
    protected function generateFileName($dirPath, $fileNameNotExt, $ext, $i = 0)
    {
        if (file_exists($dirPath.$fileNameNotExt.'.'.$ext)) {
            if ($pos = mb_strrpos($fileNameNotExt, '_'.$i)) {
                $fileNameNotExt = mb_substr($fileNameNotExt, 0, $pos);
            }
            $fnNew = $fileNameNotExt.'_'.++$i;

            return $this->generateFileName($dirPath, $fnNew, $ext, $i);
        }

        return $fileNameNotExt.'.'.$ext;
    }

    /**
     * Метод получения файла по коду
     *
     * @param      $code    - код файла
     * @param bool $getLink - параметр отвечает в каком виде будет ответ
     *                      true - вернет ссылку на файл
     *                      false - вернет превью в бинарном виде
     *
     * @return string - вернет ссылку на файл или объект Response для вывода файла в бинарном виде
     *                или вернет строку содержащую исключение
     */
    public function get($code, $getLink = true)
    {
        try {
            $file = (new File())->getByCode($code);
            if (empty($file)) {
                throw new \Exception('В системе не зарегистрирован указанный файл');
            }
            if (is_string($file)) {
                throw new \Exception('Произошел сбой при получении файла, попробуйте позже');
            }
            $pathOriginFile = $this->webRoot.'/'.trim($file['path'], '/');
            if (!file_exists($pathOriginFile)) {
                throw new \Exception('Физический файл отсутствует в системе');
            }
            if (!empty($getLink)) {
                return trim($file['path'], '/');
            }

            return Yii::$app->response->sendFile($pathOriginFile, $file['name']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод генерации и получения превью фотографии
     *
     * @param      $code    - код файла
     * @param bool $getLink - параметр отвечает в каком виде будет ответ
     *                      true - вернет ссылку на превью
     *                      false - вернет превью в бинарном виде
     *
     * @return string - вернет ссылку на фото или объект Response для вывода файла в бинарном виде
     *                или вернет строку содержащую исключение
     */
    public function getPreview($code, $getLink = true)
    {
        try {
            $file = (new File())->getByCode($code);
            if (empty($file)) {
                throw new \Exception('В системе не зарегистрирован указанный файл');
            }
            if (is_string($file)) {
                throw new \Exception('Произошел сбой при получении первью фотографии, попробуйте позже');
            }
            $pathOriginFile = $this->webRoot.'/'.trim($file['path'], '/');
            if (!file_exists($pathOriginFile)) {
                throw new \Exception('Не удалось получить превью в связи отсутствия оригинальной фотографии');
            }
            //проверка на то, что файл является изображением
            if (empty(getimagesize($pathOriginFile))) {
                throw new \Exception('Попытка получения превью файла не являющегося фотографией');
            }
            //формирование директории превью и проверка на существование таковой
            $dirPathPart = '/'.rtrim($file['path'], $file['name']).trim($this->dirPreview, '/').'/';
            $dirPath     = $this->webRoot.'/'.ltrim($dirPathPart, '/');
            if (!file_exists($dirPath)) {
                if (empty(FileHelper::createDirectory($dirPath))) {
                    throw new \Exception('В системе отсутствует директория для превью фотографий');
                }
            }
            //формируем название превью фото с последующей генерацией
            $filePreviewName = $this->getPreviewName($file['name']);
            $filePreviewPath = $dirPath.$filePreviewName;
            if (!file_exists($filePreviewPath)) {
                Image::getImagine()->open($pathOriginFile)
                    ->thumbnail(new Box($this->previewWidth, $this->previewHeight))
                    ->save($filePreviewPath, ['quality' => $this->previewQuality]);
                if (!file_exists($filePreviewPath)) {
                    throw new \Exception('Произошла ошибка генерации превью фотографии');
                }
            }
            if (!empty($getLink)) {
                return $dirPathPart.$filePreviewName;
            }

            return Yii::$app->response->sendFile($filePreviewPath, $filePreviewName);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод формирования названия превью
     *
     * @param $name - имя оригинального файла
     *
     * @return string - вернет название превью
     */
    protected function getPreviewName($name)
    {
        $extArray              = explode('.', $name);
        $ext                   = end($extArray);
        $fileNamePreviewNotExt =
            rtrim($name, '.'.$ext).'_'.$this->previewWidth.'_'.$this->previewHeight.'_'.$this->previewQuality;

        return $fileNamePreviewNotExt.'.'.$ext;
    }

    /**
     * Метод удаления физического файла, превью и записи о регистрации файла
     *
     * @param $code - код файла
     *
     * @return int|string - вернет кол-во удаленных записей в таблице или выбросит иселючение
     */
    public function delete($code)
    {
        try {
            $model = new File();
            $file  = $model->getByCode($code);
            if (empty($file)) {
                throw new \Exception('В системе не зарегистрирован указанный файл');
            }
            if (is_string($file)) {
                throw new \Exception('Произошел сбой при удалении файла, попробуйте позже');
            }
            //формирование физического пути оригинального файла с последующим удалением
            $pathOriginFile = $this->webRoot.'/'.trim($file['path'], '/');
            //если файл изображение, то при наличии превью удаляем таковое
            if (!empty(getimagesize($pathOriginFile))) {
                $dirPathPart = rtrim($file['path'], $file['name']).trim($this->dirPreview, '/').'/';
                $pathPreview = $this->webRoot.'/'.ltrim($dirPathPart, '/').$this->getPreviewName($file['name']);
                if (file_exists($pathPreview) && empty(FileHelper::unlink($pathPreview))) {
                    //todo[echmaster]: 11.04.2018 при не удачном удалении превью, не выбрасываем исключение, лучше логируем
                    $msg = 'При удалении файла произошла ошибка удаления превью, путь до превью: '.$pathPreview;
                }
            }
            //удаляем оригинальный файл
            if (file_exists($pathOriginFile) && empty(FileHelper::unlink($pathOriginFile))) {
                throw new \Exception('Произошла ошибка при удалении физического файла, попробуйте позже');
            }

            return $model->deleteOne($file['id']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
