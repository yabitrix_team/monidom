<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\RequestFile;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\FileHelper;

/**
 * Class AccreditationFileComponent - компонент для регистрации файлов в системе
 *
 * @package app\modules\app\v1\components
 */
class AccreditationFileComponent extends FileComponent
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод экшен для получения архива документов по коду заявки
     *
     * @param $code   - код заявки
     *
     * @return string - вернет путь к файлу
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function accreditationByCode($code)
    {
        /**
         * сохранение pdf в таблицу file и добавление в doc_pack_3
         */
        $path     = Yii::$app->params['core']['requests']['pathSaveFile'].$code.'/';
        $path     = $this->webRoot.'/'.trim($path, '/').'/';
        $fileName = 'Печать данных номера бланка.pdf';
        $bind     = 'doc_pack_3';
        if (!file_exists($path.$bind.'/'.$fileName)) {

            $modelRequestClass = $this->getModelRequest();
            $request     = (new $modelRequestClass)->getByCode($code);
            if (empty($request)) {
                throw new \Exception(
                    'Произошла ошибка получения заявки, некорректно указан код заявки, либо заявка не принадлежит текущему пользователю, обратитесь к администратору.'
                );
            }
            if (is_string($request)) {
                throw new \Exception($request);
            }
            if (empty ($request['accreditation_num'])) {
                throw new \Exception(
                    "У заявки не определен код аккредитации, повторите запрос позже или обратитесь к администратору."
                );
            }
            // get your HTML raw content without any layouts or scripts
            $content = '<p id="kv-heading-1"> '.$request['accreditation_num'].'</p>
<br>
<br>
<br>
<br>
<br>
<p id="kv-heading-2">'.$request['accreditation_num'].'</p>';
            // setup kartik\mpdf\Pdf component
            $pdf = new \kartik\mpdf\Pdf(
                [
                    // set to use core fonts only
                    'mode'        => Pdf::MODE_CORE,
                    // A4 paper format
                    'format'      => Pdf::FORMAT_A4,
                    // portrait orientation
                    'orientation' => Pdf::ORIENT_LANDSCAPE,
                    // stream to browser inline
                    'destination' => Pdf::DEST_FILE,
                    // your html content input
                    'content'     => $content,
                    // format content from your own css file if needed or use the
                    // enhanced bootstrap css built by Krajee for mPDF formatting
                    //'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                    // any css to be embedded if required
                    'cssInline'   => '
				                #kv-heading-1{font-size:32px; color:black; font-family:Times New Roman; font-weight: bold;  text-align: center;} 
				                #kv-heading-2{font-size:180px; color:black; font-family:Times New Roman; font-weight: bold;  text-align: left;}
				                ',
                    // set mPDF properties on the fly
                    'options'     => ['title' => 'Печать данных номера бланка'],
                    // call mPDF methods on the fly
                    'methods'     => [
                        //    'SetHeader' => [ 'Report Header' ],
                        //    'SetFooter' => [ '{PAGENO}' ],
                    ],
                ]
            );
            /**
             * сохранение pdf в таблицу file и добавление в doc_pack_1 и doc_pack_2
             */
            $path     = Yii::$app->params['core']['requests']['pathSaveFile'].$code.'/';
            $path     = $this->webRoot.'/'.trim($path, '/').'/';
            $fileName = 'Печать данных номера бланка.pdf';
            if (!file_exists($path)) {
                if (empty(FileHelper::createDirectory($path))) {
                    Log::error(
                        ['text' => "Не удалось создать директорию для загрузки файла: {$path}"],
                        'photos.upload'
                    );
                }
            }
            $pdf->output($content, $path.$fileName, 'F');
            $this->createLocalFile($path.$fileName, $code, $bind, 'Печать данных номера бланка', null);
            unlink($path.$fileName);
        }
        return $path.$bind.'/'.$fileName;
    }

    /**
     * ф-я для записи локально сгенерированных
     *
     * @param string|null $filePath - путь к файлу
     * @param string      $code     - код заявки
     * @param string      $bind     - бинд файла
     * @param string      $desc     - описание файла, для записи в таблице file
     * @param string      $prefix   - некий префикс идентификации символьного кода при формировании,
     *                              если указать то в начале символьного кода файла будет этот префикс
     *                              не должен превышать 12 символов, параметр необязателен
     */
    public function createLocalFile(
        string $filePath,
        string $code,
        string $bind,
        string $desc = null,
        string $prefix = null
    ) {
        try {
            if (!file_exists($filePath)) {
                Log::error(['text'=>'Файл не существует: {$filePath}.'], 'photos.upload');
                throw new \Exception('Файл не существует.');
            }
            //проверяем корректность введенного значения в bind
            $checkBind = new RequestFile();
            $checkBind->load(['file_bind' => $bind], '');
            if (empty($bind) || empty($checkBind->validate('file_bind'))) {
                throw new \Exception('Некорректно указана привязка файла к заявке');
            }
            if (strlen($code) != 32) {
                throw new \Exception('При попытке регистрации файла был некорректно указан код заявки');
            }
            $modelRequestClass = $this->getModelRequest();
            $request     = (new $modelRequestClass)->getByCode($code);
            if (empty($request) || is_string($request)) {
                $msg = empty($request)
                    ? 'Произошла ошибка добавления файла к заявке, некорректно указана код заявки либо заявка не принадлежит текущему пользователю, обратитесь к администратору'
                    : $request;
                throw new \Exception($msg);
            }
            $pathSaveFile = Yii::$app->params['core']['requests']['pathSaveFile'];
            if (empty($pathSaveFile)) {
                throw new \Exception(
                    'Невозможно добавить файл к заявке, так как в настройках некорректно указан путь сохранения файлов, обратитесь к администратору'
                );
            }
            $file_info   = parse_url($filePath);
            $path        = trim($pathSaveFile, '/').'/'.$request['code'].'/'.$bind;
            $resFileSave = $this->save($path, basename($file_info['path']), $filePath, $desc, $prefix, 1, true);
            //подготовка данных для записи в таблицу request_file
            $dataRequestFile  = [
                'request_id' => $request['id'],
                'file_id'    => $resFileSave['id'],
                'file_bind'  => $bind,
            ];
            $modelRequestFile = new RequestFile();
            //связь заявки с файлами
            $modelRequestFile->load($dataRequestFile, '');
            if (!$modelRequestFile->validate()) {
                //todo[echmaster]: 10.04.2018 в дальнейшем добавить логирование валидируемых данных
                $modelRequestFile->errors;
                throw new \Exception('Произошла ошибка валидации данных при попытке привязать файл к заявке');
            }
            $result = $modelRequestFile->create();
            if (is_string($result)) {
                throw new \Exception(
                    'При попытке привязать файл к заявке произошла ошибка вставки данных в таблицу'
                );
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}