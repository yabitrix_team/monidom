<?php

namespace app\modules\app\v1\components;

use yii\base\Component;
use Yii;

/**
 * Class MsgComponent - компонент для работы с сообщениями
 *
 * @package app\modules\app\v1\components
 */
class MsgComponent extends Component {
	/**
	 * @var - путь до файла относительно корня(пример /message/error.php),
	 * содержащего массив сообщений в виде "201" => "Сообщение", где 201 - это код сообщения
	 */
	public $path;

	/**
	 * Метод получения текста сообщения по коду
	 *
	 * @param $code - код сообщения
	 *
	 * @return mixed|string - вернет пустую строку или сообщение по коду
	 */
	public function get( $code ) {

		$msg = '';
		try {

			if ( ! empty( $this->path ) ) {
				$file = rtrim( Yii::getAlias( '@webroot' ), '/web' ) . '/' . ltrim( $this->path, '/' );
				if ( file_exists( $file ) && is_file( $file ) ) {
					//todo[echmaster]: добавить кэширование к подключаемому файлу, после реализация кэша
					$arMsg = include $file;
					if ( is_array( $arMsg ) && key_exists( $code, $arMsg ) ) {
						$msg = $arMsg[ $code ];
					} else {
						$msg = 'Указан неизвестный код сообщения, обратитесь к администратору';
					}
				} else {
					throw new \Exception( 'В настройках компонента некорректно указан путь до файла сообщений' );
				}
			} else {
				throw new \Exception( 'В настройках компонента не указан путь до файла сообщений' );
			}
		} catch ( \Exception $e ) {
			if ( function_exists( 'carLog' ) ) {
				carLog( $e->getMessage(), __FILE__, __LINE__ );
			}
		}

		return $msg;
	}
}