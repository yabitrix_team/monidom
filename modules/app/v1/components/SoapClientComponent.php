<?php

namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\logs\Log;
use SoapClient;
use Yii;
use yii\base\Component;

/**
 * Class SoapClientComponent - компонент для подключения через SOAP клиент
 *
 * @package app\modules\app\v1\components\soap
 */
class SoapClientComponent extends Component {
	/**
	 * @var string - общий адресс подключения к сервисам, без указания конкрентного wsdl
	 */
	public $url;
	/**
	 * @var string - общий адресс подключения к имитационным сервисам, без указания конкрентного wsdl
	 */
	public $urlMock;
	/**
	 * @var string - логин
	 */
	public $login;
	/**
	 * @var string - пароль
	 */
	public $password;
	/**
	 * @var bool - логирование действий компонента, true включит логировани,
	 *           но при этом обязательно нужно указать $logCategory
	 *           предварительно настроив категорию в компоненте log
	 */
	public $log = false;
	/**
	 * @var - категория логов, указывается в настройках компонента log
	 */
	public $logCategory;
	/**
	 * @var bool - будут ли SOAP-ошибки бросать исключения типа SoapFault
	 */
	public $exceptions = true;
	/**
	 * @var bool - отслеживание запроса и в случае ошибки можно получить обратную трассировку
	 */
	public $trace = true;
	/**
	 * @var int - значение тайм-аута  в секундах по умолчанию для потоков, использующих сокеты
	 */
	public $defaultSocketTimeout = 60;
	/**
	 * @var int - значение тайм-аута  в секундах для потоков, использующих сокеты,
	 *          в классе modules/notification/classes/WSRedis.php:34 выставлятеся
	 *          значение -1 для корректной работы Редис, необходимо для SOAP
	 *          переопределять значение на дефолтное и потом возвращать на то, что
	 *          было указано до вызова SOAP
	 */
	protected $socketTimeout;
	/**
	 * @var soapClient - экземпляр для SOAP клиента
	 */
	protected $client;
	/**
	 * @var - url сервиса, формируется в getInstance
	 */
	protected $urlService;

	/**
	 * Метод инициализации
	 *
	 * @throws \Exception
	 */
	public function init() {

		parent::init();
		ini_set( "soap.wsdl_cache", "0" );
		ini_set( "soap.wsdl_cache_enabled", "0" );
	}

	/**
	 * Метод получения инстанса покдлючения по SOAP
	 *
	 * @param string $name - название сервиса без указания wsdl,
	 *                     пример: Annuity?wsdl, указываем Annuity
	 *
	 * @return $this|\Exception - вернет объкет текущего компонента
	 *                        либо объект исключения содержащий описание ошибки
	 */
	public function getInstance( string $name ) {

		try {
			if ( empty( $this->url ) || ! filter_var( $this->url, FILTER_VALIDATE_URL ) ) {
				throw new \Exception( 'Не удалось подключиться по SOAP протоколу, так как в параметрах некорректно указан URL' );
			}
			if (
				! empty( Yii::$app->params['core']['isMock'] )
				&& ( empty( $this->urlMock ) || ! filter_var( $this->urlMock, FILTER_VALIDATE_URL ) )
			) {
				throw new \Exception( 'Не удалось подключиться по SOAP протоколу к имитационному сервису, так как в параметрах некорректно указан URL имитационного сервиса' );
			}
			if ( empty( $this->login ) ) {
				throw new \Exception( 'Не удалось подключиться по SOAP протоколу, так как в параметрах не указан логин' );
			}
			if ( empty( $this->password ) ) {
				throw new \Exception( 'Не удалось подключиться по SOAP протоколу, так как в параметрах не указан пароль' );
			}
			if ( ! empty( $this->log ) ) {
				if ( empty( $this->logCategory ) ) {
					throw new \Exception( 'Не удалось подключиться по SOAP протоколу, так как при включенном логировании в параметрах не указана категория логирования' );
				}
				//получение категорий логирования, проверка настроены ли логи для данного компонента
				$categoriesLog = array_column( Yii::$app->log->targets, 'categories' );
				array_walk( $categoriesLog, function( &$v ) {

					$v = ! empty( $v ) ? reset( $v ) : '';
				} );
				$categoriesLog = array_unique( array_filter( $categoriesLog ) );
				if ( ! in_array( $this->logCategory, $categoriesLog ) ) {
					throw new \Exception( 'Не удалось подключиться по SOAP протоколу, так как при включенном логировании в параметрах некорректно указана категория логирования, возможно не настроены параметры логирования' );
				}
			}
			//для имитационных сервисов подставляем свой URL
			$url = empty( Yii::$app->params['core']['isMock'] ) ? $this->url : $this->urlMock;
			//перед названием файла ?wsdl указываем слеш, рекомендации Васильева, если имитируем сервис, то ставим точку
			$ext              = empty( Yii::$app->params['core']['isMock'] ) ? '/?wsdl' : '.wsdl';
			$this->urlService = rtrim( $url, '/' ) . '/' . $name . $ext;
			$params           = [
				'login'      => $this->login,
				'password'   => $this->password,
				'exceptions' => $this->exceptions,
				'trace'      => $this->trace,
				'cache_wsdl' => WSDL_CACHE_NONE,
			];
			//----получение тайм-аута для потоков использующих сокеты
			$this->socketTimeout = ini_get( 'default_socket_timeout' );
			if ( substr( $this->urlService, 0, 5 ) == 'https' ) {
				//----установка значения тайм-аута для потоков использующих сокеты только в случае https
				ini_set( 'default_socket_timeout', $this->defaultSocketTimeout );
				$params['stream_context'] = stream_context_create( [
					                                                   'ssl' => [
						                                                   'verify_peer'       => false,
						                                                   'verify_peer_name'  => false,
						                                                   'allow_self_signed' => true,
					                                                   ],
				                                                   ] );
			}
			$this->client = new SoapClient( $this->urlService, $params );

			return $this;
		} catch ( \Exception $e ) {
			$this->logError( $e, 'Инстанс - ' . $name, get_object_vars( $this ) );

			return $e;
		}
	}

	/**
	 * Метод отправки запроса к указанному сервису по протоколу SOAP
	 *
	 * @param string $service - название сервиса
	 * @param array  $params  - массив параметров, передаваемый сервису
	 *
	 * @return mixed|\Exception - вернет результат запроса
	 *                           или объект исключения содержащий описание ошибки
	 */
	public function send( string $service, $params = [] ) {

		try {
			if ( empty( $this->client ) ) {
				throw new \Exception( 'Не получен инстанс для подключения по SOAP протоколу' );
			}
			//при работе с имитационными сервисами не оборачиваем параметры в массив
			$params = empty( Yii::$app->params['core']['isMock'] ) ? [ $params ] : $params;
			$result = $this->client->__soapCall( $service, $params );
			$result = isset( $result->return ) ? $result->return : $result;
			$this->logSuccess( $service, $this->urlService, $params, $result );

			return $result;
		} catch ( \Exception $e ) {
			$this->logError( $e, $service, [ 'url' => $this->urlService ], $params );

			return $e;
		}
		finally {
			//после отправки запроса по SOAP устанавливаем значение, если были изменения
			if ( $this->socketTimeout != ini_get( 'default_socket_timeout' ) ) {
				ini_set( 'default_socket_timeout', $this->socketTimeout );
			}
		}
	}

	/**
	 * Метод логирования успешных операций по данному функицоналу
	 * лог лежит по адресу: runtime/logs/soap_success.log
	 *
	 * @param $service    - наименование вызываемого сервиса
	 * @param $serviceUrl - адрес удаленного сервиса
	 * @param $request    - данные передаваемые на удаленный сервис
	 * @param $response   - данные получаемые с удаленного сервиса
	 */
	protected function logSuccess( $service, $serviceUrl, $request, $response ) {

		$req  = print_r( $request, true );
		$res  = print_r( $response, true );
		$info = '';
		if ( ! is_a( Yii::$app, 'yii\console\Application' ) ) {
			$info .= "UserAgent: " . Yii::$app->request->userAgent . "\n";
			$info .= "URL бэка: " . Yii::$app->request->url;
		}
		$log = <<<TXT

*** Сервис: $service *** 
$info
URL сервиса: $serviceUrl
Данные передачи: $req Ответ от сервиса: $res
TXT;
		Log::info( ["text" => $log], $this->logCategory );
	}

	/**
	 * Метод логирования ошибок по данному функицоналу
	 * лог лежит по адресу: runtime/logs/soap_error.log
	 *
	 * @param \Exception $e             - объект исключения
	 * @param            $service       - наименование вызываемого сервиса
	 * @param            $serviceParams - параметры вызываемого сервиса
	 * @param            $data          - данные передаваемые на удаленный сервис
	 */
	protected function logError( \Exception $e, $service, $serviceParams = [], $data = [] ) {

		$params = ! empty( $serviceParams )
			? 'Параметры подключения к сервису: ' . print_r( $serviceParams, true ) : '';
		$data   = ! empty( $data ) ? ' Данные передачи: ' . print_r( $data, true ) : '';
		$info   = '';
		if ( ! is_a( Yii::$app, 'yii\console\Application' ) ) {
			$info .= "UserAgent: " . Yii::$app->request->userAgent . PHP_EOL;
			$info .= "URL бэка: " . Yii::$app->request->url;
		}
		$log = <<<TXT

*** Сервис: $service *** 
Текст ошибки: {$e->getMessage()}
Http-код: {$e->getCode()}
$info
 $params $data
TXT;
		Log::error( ["text" => $log], $this->logCategory );
	}
}