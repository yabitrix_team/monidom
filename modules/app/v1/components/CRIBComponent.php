<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\exceptions\InvalidParamException;
use app\modules\app\v1\classes\logs\Log;
use yii\httpclient\Client;
use yii\base\BaseObject;
use Yii;

/**
 * Class CRIBComponent - компонент взаимодействия с сервисом CRIB
 *
 * @package app\modules\app\v1\components
 */
class CRIBComponent extends BaseObject
{
    /**
     * Константы целей авторизации
     */
    public const AUTH_TARGET_LK         = 'lk'; //для ЛКК
    public const AUTH_TARGET_MOBILE_APP = 'mobile_app'; //для МП
    /**
     * @var string - основная часть url-адреса сервиса CRIB,
     *             далее в рамках методов подставляется часть необходимого пути
     */
    public $url;
    /**
     * @var - цель атворизации, на данный момент предусмотрено две цели:
     *      lk - для ЛКК и mobile_app - для МП
     */
    public $target;
    /**
     * @var string - токен авторизации
     */
    public $token;
    /**
     * @var string - название категории лога, если не указать, то логировать не будет
     */
    public $logCategory;
    /**
     * @var int - максимальное кол-во секунд, в течении которых может быть выполнен запрос
     */
    public $timeout = 5;
    /**
     * @var yii\httpclient\Request - экземпляр запроса httpclient
     */
    protected $request;

    /**
     * Метод инициализации компонента
     *
     * @throws
     */
    public function init(): void
    {
        parent::init();
        $isAllowedLog = false;
        try {
            if (!empty($this->logCategory)) {
                $category = array_column(Yii::$app->log->targets, 'categories');
                array_walk(
                    $category,
                    function ($v) use (&$isAllowedLog) {
                        !in_array($this->logCategory, $v, true) ?: $isAllowedLog = true;
                    }
                );
                if (empty($isAllowedLog)) {
                    throw new InvalidParamException('Категория логирования CRIB');
                }
            }
            if (!filter_var($this->url, FILTER_VALIDATE_URL)) {
                throw new InvalidParamException('URL-адрес CRIB');
            }
            if (
                empty($this->target) ||
                !in_array($this->target, [self::AUTH_TARGET_LK, self::AUTH_TARGET_MOBILE_APP], true)
            ) {
                throw new InvalidParamException('Цель авторизации CRIB');
            }
            if (empty($this->token)) {
                throw new InvalidParamException('Токен авторизации CRIB');
            }
            if (empty($this->timeout)) {
                throw new InvalidParamException('Время выполнения запроса');
            }
            $this->url     = rtrim($this->url, '/');
            $this->request = (new Client(['baseUrl' => $this->url]))
                ->createRequest()
                ->setOptions(
                    [
                        'timeout'       => $this->timeout,
                        'sslVerifyPeer' => defined('YII_ENV') && YII_ENV === 'prod',
                    ]
                )
                ->setFormat(Client::FORMAT_JSON)
                ->addHeaders(['x-auth-token' => $this->token, 'x-auth-target' => $this->target]);
        } catch (\Exception $e) {
            if (!empty($isAllowedLog)) {
                $params = [
                    'url'     => $this->url,
                    'token'   => substr($this->token, 0, 5).'...',
                    'timeout' => $this->timeout,
                ];
                Log::error(
                    [
                        'text' => '**** НЕ УДАЛОСЬ ИНИЦИАЛИЗИРОВАТЬ КОМПОНЕНТ ВЗАИМОДЕЙСТВИЯ С СИСТЕМОЙ CRIB ****
Параметры подключения: '.print_r($params, true).'
Ошибка: '.$e->getMessage(),
                    ],
                    $this->logCategory
                );
            }
            throw new \RuntimeException('Ошибка интеграции к системе CRIB, обратитесь к администратору');
        }
    }

    /**
     * Метод получения crib_client_id
     *
     * @param        $phone        - номер телефона пользователя
     * @param string $cribClientId - идентификатор из сервиса CRIB, полученный ранее
     * @param string $yandexId     - яндекс идентификатор
     * @param string $googleId     - гугл идентификатор
     *
     * @return bool|string - вернет идентификатор CRIB в виде строки или ложь
     * @throws \Exception
     */
    public function registryPush($phone, string $cribClientId = null, string $yandexId = null, string $googleId = null)
    {
        $data = ['event_dt' => $this->getDateTime()];
        empty($this->getIP()) ?: $data['ipd'] = $this->getIP();
        empty($phone) ?: $data['phone'] = $phone;
        empty($cribClientId) ?: $data['cid'] = $cribClientId;
        empty($yandexId) ?: $data['yandex_id'] = $yandexId;
        empty($googleId) ?: $data['google_id'] = $googleId;
        $partUrl = 'v1/registry/push/';
        $params  = print_r(array_merge($data, ['url' => $this->url.'/'.$partUrl]), true);
        try {
            $response = $this->request
                ->setMethod('POST')
                ->setUrl($partUrl)
                ->setData($data)
                ->send();
            $result   = $response->getData();
            if (!empty($result['error']) || !isset($result['id'])) {
                $msg = !empty($result['error'])
                    ? $result['error_message']
                    : 'При интеграции с сервисом CRIB нарушен контракт взаимодействия, сервис не вернул id';
                throw new \RuntimeException($msg);
            }
            Log::info(
                [
                    'text' => '**** ПОЛУЧЕНИЕ CRIB_CLIENT_ID ИЗ СИСТЕМЫ CRIB ****
Запрос: '.$params.'
Ответ: '.print_r($result, true),
                ],
                $this->logCategory
            );

            return $result['id'];
        } catch (\Exception $e) {
            if (!empty($this->logCategory)) {
                Log::error(
                    [
                        'text' => '**** НЕ УДАЛОСЬ ПОЛУЧИТЬ CRIB_CLIENT_ID ИЗ СИСТЕМЫ CRIB ****
Запрос: '.$params.'
Ошибка: '.$e->getMessage(),
                    ],
                    $this->logCategory
                );
            }

            return $e->getMessage();
        }
    }

    /**
     * Метод получения даты в определенном формате
     *
     * @param string $time - дата в формате 2019-01-25 10:23:28
     *
     * @return string - вернет отфоратированную дату в формате Y-m-d\TH:i:sP
     * @throws \Exception
     */
    protected function getDateTime(string $time = 'now'): string
    {
        return (new \DateTime($time))->format("Y-m-d\TH:i:sP");
    }

    /**
     * Метод получения ip адреса
     *
     * @return string|false - вернет строку с ip адресом или ложь
     */
    protected function getIP()
    {
        return preg_match('/^(192|127|0)/', $_SERVER['HTTP_X_FORWARDED_FOR'])
            ? false : $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
}