<?php
namespace app\modules\app\v1\components;

use app\modules\app\v1\classes\logs\Log;
use yii;
use yii\base\ErrorException;
use yii\helpers\VarDumper;
use yii\web\ErrorHandler;
use app\modules\app\v1\classes\AppResponse;
use yii\web\HttpException;

class ErrorComponent extends ErrorHandler
{
    protected $_memoryReserve;

    public function handleException($exception)
    {
        $this->exception = $exception;
        // disable error capturing to avoid recursive errors while handling exceptions
        $this->unregister();
        // set preventive HTTP status code to 500 in case error handling somehow fails and headers are sent
        // HTTP exceptions will override this value in renderException()
        if (PHP_SAPI !== 'cli') {
            http_response_code(500);
        }
        try {
            $text = $exception->getMessage().PHP_EOL.PHP_EOL.
                    "Исключение: ".get_class($exception).PHP_EOL.
                    "Файл: ".$exception->getFile().PHP_EOL.
                    "Строка: ".$exception->getLine().PHP_EOL.
                    "Код: ".$exception->getCode();
            if (
                !in_array(get_class($exception), ["yii\web\UnauthorizedHttpException"]) &&
                !in_array(intval($exception->getCode()), [401, 403])
            ) {
                Log::error(["text" => $text], "debug.exception");
            }
            if ($this->discardExistingOutput) {
                $this->clearOutput();
            }
            $this->renderException($exception);
            if (!YII_ENV_TEST) {
                yii::getLogger()->flush(true);
                exit(1);
            }
        } catch (\Exception $e) {
            // an other exception could be thrown while displaying the exception
            $this->handleFallbackExceptionMessage($e, $exception);
        } catch (\Throwable $e) {
            // additional check for \Throwable introduced in PHP 7
            $this->handleFallbackExceptionMessage($e, $exception);
        }
        $this->exception = null;
    }

    public function handleFatalError()
    {
        unset($this->_memoryReserve);
        // load ErrorException manually here because autoloading them will not work
        // when error occurs while autoloading a class
        if (!class_exists('yii\\base\\ErrorException', false)) {
            require_once yii::getAlias('@app').'/vendor/yiisoft/yii2/base/ErrorException.php';
        }
        $error = error_get_last();
        if (ErrorException::isFatalError($error)) {
            $exception =
                new ErrorException($error['message'], $error['type'], $error['type'], $error['file'], $error['line']);
            $this->exception = $exception;
            Log::error(["text" => $exception], "debug.fatal");
            if ($this->discardExistingOutput) {
                $this->clearOutput();
            }
            $this->renderException($exception);
            // need to explicitly flush logs because exit() next will terminate the app immediately
            yii::getLogger()->flush(true);
            exit(1);
        }
    }

    protected function handleFallbackExceptionMessage($exception, $previousException)
    {
        $msg = "Возникла ошибка при обработке исключения:\n";
        $msg .= (string) $exception;
        $msg .= "\nПредыдущее исключение:\n";
        $msg .= (string) $previousException;
        if (YII_DEBUG) {
            if (PHP_SAPI === 'cli') {
                echo $msg."\n";
            } else {
                echo '<pre>'.htmlspecialchars($msg, ENT_QUOTES, Yii::$app->charset).'</pre>';
            }
        } else {
            echo 'Произошла системная ошибка.';
        }
        $msg .= "\n\$_SERVER = ".VarDumper::export($_SERVER);
        Log::error(["text" => $msg], "debug.fallback");
        exit(1);
    }

    protected function convertExceptionToArray($exception)
    {
        return AppResponse::get(
            false,
            false,
            (defined('YII_ENV') && YII_ENV == "dev" ? $exception->getMessage() : "Произошла системная ошибка.")
        );
    }

    protected function renderException($exception)
    {
        if (!isset($exception->statusCode)) {
            $exception = new HttpException(200, $exception->getMessage(), $exception->getCode(), $exception);
        }
        parent::renderException($exception);
    }
}
