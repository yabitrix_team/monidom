<?php
namespace app\modules\app\v1\helpers;

/**
 * Class DBHelper - класс помощник, содерждащий статические методы для вспомогательной работы с БД
 *
 * @package app\modules\app\v1\helpers
 */
class DBHelper
{
    /**
     * Метод получения подготовленных полей для работы через join,
     * подставляет префиксы полям и оборачивает в скобки
     *
     * @param array  $fields          - поля БД для подготовки
     * @param string $prefix          - префикс базы для полей
     * @param bool   $setNameByPrefix - устанавливает названия полей с учетом префикса,
     *                                пример: f_id
     *
     * @return string - вернет подготовленную строку, пример:
     *                  "[[f]].[[id]], [[f]].[[path]]"
     */
    public static function getPrepareFieldsForJoin(array $fields, string $prefix, $setNameByPrefix = false)
    {
        array_walk(
            $fields,
            function (&$v, &$k) use ($prefix, $setNameByPrefix) {
                $v = '[['.$prefix.']].'.(is_numeric($k)
                        ? (empty($setNameByPrefix)
                            ? '[['.$v.']]'
                            : '[['.$v.']] AS [['.$prefix.'_'.$v.']]')
                        : '[['.$v.']] AS [['.$k.']]');
            }
        );

        return implode(',', $fields);
    }
}