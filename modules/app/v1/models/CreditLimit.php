<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\validators\PhoneValidator;
use RuntimeException;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class CreditLimit - модель для работы с Кредитными лимитами
 *
 * @package app\modules\app\common\models
 */
class CreditLimit extends Model
{
    /**
     * Подключение трейтов
     */
    use ReadableTrait;
    use WritableTrait;
    /**
     * Поля модели
     */
    public $phone;
    public $amount;
    public $period;
    public $full_name;
    public $offer_type;

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'code',
            'phone',
            'amount',
            'period',
            'full_name',
            'offer_type',
            'active',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return '{{credit_limit}}';
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules(): array
    {
        return [
            [
                'phone',
                PhoneValidator::class,
            ],
            [
                'amount',
                'number',
                'message' => 'Сумма лимита указана в некорректном формате',
            ],
            [
                'period',
                'number',
                'message' => 'Срок займа указан в некорректном формате',
            ],
            [
                'full_name',
                'string',
                'message' => 'Имя указано в некорректном формате',
            ],
            //todo[echmaster]: 02.02.2019 после принятия формата типов предложений скорректировать валидатор
            [
                'offer_type',
                'safe',
            ],
        ];
    }

    /**
     * Метод получения данных по номер телефона и типу предложения
     *
     * @param       $phone     - номер телефона
     * @param       $offerType - тип предложения
     * @param array $fields    - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \RuntimeException
     */
    public function getByPhoneAndOfferType($phone, $offerType, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode(']],[[', $fields).']]';

            return Yii::$app->db->createCommand(
                'SELECT '.$fields.' 
		            FROM '.$this->getTableName().' 
		            WHERE [[active]] = 1 AND [[phone]] = :phone AND [[offer_type]] = :offerType',
                [':phone' => $phone, ':offerType' => $offerType]
            )
                ->queryOne();
        } catch (Exception $e) {
            throw new RuntimeException(
                'Ошибка получения кредитных лимитов в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод сохранения записи в таблице
     *
     * @return int - вернет идентификатор вставленной или обновленной записи
     */
    public function save(): int
    {
        try {
            if (!$this->validate()) {
                throw new RuntimeException(reset($this->firstErrors));
            }
            $data                 = $this->getAttributes();
            $data['active']       = (int) !empty($this->amount);
            $data['code']         = md5($this->phone.$this->offer_type);
            $insert               = $update = $data;
            $update['updated_at'] = new Expression('NOW()');
            Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)
                ->execute();

            return (int) Yii::$app->db->lastInsertID;
        } catch (Exception $e) {
            throw new RuntimeException(
                'Ошибка сохранения кредитного лимита в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод удаления записи по номере телефона и типу предложения
     *
     * @param $phone     - номер телефона
     * @param $offerType - тип предложения
     *
     * @return bool - вернет истину в случае успеха, иначе ложь
     */
    public function deleteByPhoneAndOfferType($phone, $offerType): bool
    {
        try {
            $code   = md5($phone.$offerType);
            $result = Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), '[[code]]=:code', ['code' => $code])
                ->execute();

            return (bool) $result;
        } catch (\Exception $e) {
            throw new RuntimeException(
                'Ошибка удаления кредитного лимита в базе данных, обратитесь к администратору'
            );
        }
    }
}