<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Платеж по договору
 *
 * @package app\modules\app\v1\models\base
 */
class RequestsMetrika extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * поля платежной системы при ответе
	 *
	 * @var
	 */
	public $request_id;
	public $metrika_id;
	public $user_id;
	public $metrika_type;

	public function getFieldsNames(): array {
		return [
			'id',
			'request_id',
			'metrika_id',
			'user_id',
			'metrika_type',
			'created_at',
			'updated_at'
		];
	}

	public function getTableName(): string {
		return "{{requests_metrika}}";
	}

	public function rules() {

		return [
			[
				[
					'request_id',
					'metrika_id',
					'user_id',
					'metrika_type',
				],
				'required',
			],
		];
	}

	public function addData(){

		try{
			if(! $this->validate()){
				return $this;
			}

			$arPrepare = $this->getAttributes();
			$arPrepare['created_at']      = new Expression( 'NOW()' );
			$arPrepare['updated_at']      = new Expression( 'NOW()' );

			$result = Yii::$app->db
				->createCommand()
				->insert( $this->getTableName(), $arPrepare )->execute();

			return !empty($result) ? Yii::$app->db->lastInsertID : $result;

		}catch (\Exception $e){
			return $e;
		}

	}
}