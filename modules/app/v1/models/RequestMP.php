<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Class RequestMP - модель для работы с таблицей содержащей
 * инфу по заявке для мобильного приложения
 *
 * @package app\modules\app\v1\models
 */
class RequestMP extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константа блокировки по клиенту
     */
    CONST BLOCKED_BY_CLIENT = 'client';
    /**
     * Константа блокировки по партнеру
     */
    CONST BLOCKED_BY_PARTNER = 'partner';
    /**
     * @var integer - идентификатор заявки
     */
    public $request_id;
    /**
     * @var string - информация мобильного приложения
     */
    public $service_info;
    /**
     * @var - параметр указывающий на источник блокировки,
     *      на данный момент это client или partner
     */
    public $blocked_by;

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{request_mp}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "request_id",
            "service_info",
            "blocked_by",
            "updated_at",
            "created_at",
        ];
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {
        return [
            [
                'request_id',
                'required',
                'message' => 'Не указан идентификатор заявки',
            ],
            [
                'request_id',
                'number',
                'message' => 'Идентификатор заявки указан в некорректном формате',
            ],
            [
                'service_info',
                'string',
                'message' => 'Информация мобильного приложения указана в некорректном формате',
            ],
            [
                'blocked_by',
                'in',
                'range'   => [self::BLOCKED_BY_CLIENT, self::BLOCKED_BY_PARTNER],
                'message' => 'Некорректно указан параметр блокировки заявки',
            ],
        ];
    }

    /**
     * Метод сохранения записи (создает или обновляет запись) в таблице
     *
     * @return $this|int|string - вернет 1 - при добавлении,
     *                            2 - при обновлении,
     *                            текущий объект, если валидация не прошла,
     *                            объект исключения если была ошибка валидации
     */
    public function save()
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $insert               = $update = $this->prepareDataForExecute();
            $update['updated_at'] = new Expression('NOW()');

            return Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)->execute();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения доп. информации по заявке относительно номера заявки из 1С
     *
     * @param       $num1C  - номер заявки из 1С
     * @param array $fields - поля для выборки, при пустом значении выберет все поля
     *
     * @return array|\Exception|false - вернет значение или ложь если не нашел в БД
     *                                при исключении вернет объект исключения
     */
    public function getByNum1C($num1C, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $method = count($fields) == 1 ? 'queryScalar' : 'queryOne';
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    "SELECT ".$fields." 
FROM ".$this->getTableName()." 
WHERE [[request_id]] = (
SELECT [[id]] FROM {{requests}} WHERE [[num_1c]] = :num1c
)",
                    [':num1c' => $num1C]
                )
                ->$method();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения информации заявки для МП по идентификатору заявки
     *
     * @param       $requestId - идентфикатор заявки
     * @param array $fields    - поля для выборки, при пустом значении выберет все поля
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \yii\db\Exception - исключение БД
     */
    public function getByRequestId($requestId, array $fields = [])
    {
        $fields = !empty($fields) ? $fields : $this->getFieldsNames();
        $fields = '[['.implode("]],[[", $fields).']]';

        return Yii::$app->db->createCommand(
            "SELECT ".$fields." FROM ".$this->getTableName()." WHERE [[request_id]] = :requestId",
            [':requestId' => $requestId]
        )
            ->queryOne();
    }

    /**
     * Метод получения информации заявки для МП по коду заявки
     *
     * @param       $requestCode - код заявки
     * @param array $fields      - поля для выборки, при пустом значении выберет все поля
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \yii\db\Exception - исключение БД
     */
    public function getByRequestCode($requestCode, array $fields = [])
    {
        $fields = !empty($fields) ? $fields : $this->getFieldsNames();
        $fields = '[['.implode("]],[[", $fields).']]';

        return Yii::$app->db->createCommand(
            "SELECT ".$fields." FROM ".$this->getTableName()." 
			WHERE [[request_id]] = (SELECT [[id]] FROM {{requests}} WHERE [[code]]=:requestCode)",
            [':requestCode' => $requestCode]
        )
            ->queryOne();
    }

    /**
     * Метод получения вида блокировки заявки
     *
     * @param $idOrCodeRequest - идентификатор или код заявки
     *
     * @return bool|string - вернет наименовение блокировки или false
     * @throws \Exception
     */
    public function getBlocking($idOrCodeRequest)
    {
        try {
            $method = strlen($idOrCodeRequest) == 32 ? 'getByRequestCode' : 'getByRequestId';
            $res    = $this->$method($idOrCodeRequest, ['blocked_by']);

            return empty($res['blocked_by']) ? false : $res['blocked_by'];
        } catch (\Exception $e) {
            throw new \Exception('Не удалось получить состояние блокировки заявки, обратитесь к администратору');
        }
    }

    /**
     * Метод проверки блокировки заявки
     *
     * @param $idOrCodeRequest - идентификатор или код заявки
     *
     * @return bool - вернет истину если проверка пройдена, иначе выбросит исключение
     * @throws \Exception
     */
    public function checkBlocking($idOrCodeRequest)
    {
        $blocking = $this->getBlocking($idOrCodeRequest);
        if (!empty($blocking)) {
            //массив ассоциации блокировки с кодами ошибок из файла /message/error.php
            $data = [
                self::BLOCKED_BY_CLIENT  => 3420,//доступна только в Личном кабинете
                self::BLOCKED_BY_PARTNER => 3430,//заявка заблокирована партнером
                'default'                => 3410,//заявка заблокирована
            ];
            key_exists($blocking, $data) ?: $blocking = 'default';
            throw new \Exception('', $data[$blocking]);
        }

        return empty($blocking);
    }
}