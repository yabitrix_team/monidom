<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class EventsCrm - модель для работы со справочником Событий CRM
 *
 * @package app\modules\app\v1\models
 */
class ComOffer extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{com_offer}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"code",
			"name",
			"active",
			"sort",
		];
	}

	/**
	 * Метод получения кода и имени
	 *
	 * @param int $active
	 *
	 * @return array|string
	 */
	public function getCodeName( $active = 1 ) {

		try {
			return Yii::$app->db
				->createCommand( 'SELECT [[code]], [[name]]
 FROM ' . self::getTableName() . '
 WHERE [[active]] = :active ORDER BY [[sort]] ASC', [ 'active' => $active ] )
				->queryAll();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}