<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 03.05.2018
 * Time: 15:19
 */

namespace app\modules\app\v1\models;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class PagesFaq - модель для работы с pages_fag
 *
 * @package app\modules\app\v1\models
 */
class PagesFaq extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function getAllByConsumer( $active = true, $consumer = 'client' ) {

		$consumerID = ( new Consumer() )->getOneByFilter( [ 'name' => $consumer ] );
		if ( empty( $consumerID ) ) {
			throw new \Exception( 'Не удалось получить идентификатор потребителя.' );
		}
		$consumer = " WHERE [[consumer_id]] = " . $consumerID['id'] . " ";
		$active   = is_null( $active ) ? "" : ( empty( $active ) ? " AND [[active]] = 0 " : " AND [[active]] = 1 " );

		return Yii::$app->db->createCommand( "SELECT " . $this->prepareFieldsNames() . " FROM " . $this->getTableName() . " "
		                                     . $consumer . $active )
		                    ->queryAll();
	}

	/**
	 * Получение названия таблицы модели в БД.vehzx
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{pages_faq}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{pages_faq}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"name",
			"text",
			"active",
			"sort",
		];
	}
}