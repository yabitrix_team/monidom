<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;

/**
 * Class RequestsOrigin - модель для работы с таблицей связей места создания заявки
 * в данной таблице указаны связи идентификаторов 1С с сайтом для поля место создания заявки
 *
 * @package app\modules\app\v1\models
 */
class RequestsOrigin extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константы символьных идентификаторов места создания заявки
     */
    const IDENTIFIER_MOBILE     = 'MOBILE';
    const IDENTIFIER_PARTNER    = 'PARTNER';
    const IDENTIFIER_CALL       = 'CALL';
    const IDENTIFIER_SIDES_CALL = 'SIDES_CALL';
    const IDENTIFIER_CLIENT     = 'CLIENT';

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{requests_origin}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "name_1c",
            "identifier",
            "created_at",
            "updated_at",
            "active",
            "sort",
        ];
    }

    /**
     * Метод получения связи места создания заявки по идентификаторы из бэка
     *
     * @param $identifier - символьный идентификатор, см. в таблице, допустимые варианты:
     *                    MOBILE, PARTNER, CALL, SIDES_CALL
     *
     * @return array|false|string - вернет массив данных при успехе, ложь если не найден и строку если исключение
     */
    public function getByIdentifier($identifier)
    {
        try {
            return $this->getOneByFilter(['identifier' => strtoupper($identifier)]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения связи места создания заявки по идентификатору из 1С
     *
     * @param $name1C    - название идентификатора 1С, см. в таблице, допустимые варианты:
     *                   МобильноеПриложение, ЛКПартнера, КоллЦентр, СтороннийКЦ
     *
     * @return array|false|string - вернет массив данных при успехе, ложь если не найден и строку если исключение
     */
    public function getByName1C($name1C)
    {
        try {
            return $this->getOneByFilter(['name_1c' => $name1C]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}