<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class Auth extends Model {
	public $username;
	public $email;
	public $password;
	/**
	 * @var array - поля регистрации пользователя для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $registerFieldCreate = [
		'username',
		'email',
		'password',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'username',
				'match',
				'pattern' => '/^9[0-9]{9}$/iu',
				'message' => 'Поле "Phone Number" заполнено некорректно',
			],
			[
				'email',
				'email',
				'message' => 'Поле "Электронная почта" заполнено некорректно',
			],
			[
				'password',
				'match',
				'pattern' => "/^(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,20}$/iu",
				'message' => 'Пароль должен содержать минимум одну заглавную, одну строчную буквы, одну цифру, один спец. символ и быть длиной не меннее 6 символов',
			],
		];
	}
}