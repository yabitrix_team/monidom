<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Events - модель для работы со справочником Событий
 *
 * @package app\modules\app\v1\models
 */
class Events extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{events}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"code",
			"name",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	/**
	 *  Метод получения данных события по коду события
	 *
	 * @param array|int $code - код события или массив кодов событий
	 *
	 * @return array|false|string - вернет массив данных или ложь
	 */
	public function getByCode( $code ) {

		try {
			if ( is_array( $code ) && ! empty( $code ) ) {
				$condition = '';
				$params    = [];
				foreach ( $code as $k => $v ) {
					$or                     = $k == 0 ? '' : ' OR ';
					$condition              .= $or . '[[code]] = :code' . $k;
					$params[ ':code' . $k ] = $v;
				}

				return Yii::$app->db
					->createCommand( "SELECT
[[id]],
[[name]]
FROM " . $this->getTableName() . "
WHERE " . $condition, $params )
					->queryAll();
			}

			return Yii::$app->db
				->createCommand( "SELECT
[[id]],
[[name]]
FROM " . $this->getTableName() . "
WHERE [[code]] = :code", [ ':code' => $code ] )
				->queryOne();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения кодов события по заявке
	 *
	 * @param $id - идентификатор заявки
	 *
	 * @return array|string
	 */
	public function getByRequestId( $id ) {

		try {

			return Yii::$app->db
				->createCommand( "SELECT
[[e]].[[code]] as [[code]]
FROM [[requests_events]] as [[re]]
LEFT JOIN " . $this->getTableName() . " as [[e]] ON [[re]].[[event_id]] = [[e]].[[id]]
WHERE [[re]].[[request_id]] = :id
", [ ':id' => $id ] )
				->queryAll();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

    /**
     * Метод получения данных события по заявке
     *
     * @param $id - идентификатор заявки
     *
     * @return array|string
     */
    public function getDataByRequestId( $id ) {

        try {

            return Yii::$app->db
                ->createCommand( "SELECT
[[e]].[[id]] as [[event_id]],
[[re]].[[id]] as [[id]],
[[e]].[[code]] as [[code]],
[[re]].[[is_sended]]
FROM [[requests_events]] as [[re]]
LEFT JOIN " . $this->getTableName() . " as [[e]] ON [[re]].[[event_id]] = [[e]].[[id]]
WHERE [[re]].[[request_id]] = :id
", [ ':id' => $id ] )
                ->queryAll();
        } catch ( \Exception $e ) {
            return $e->getMessage();
        }
    }
}