<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class StatusesJournal - модель для работы с таблице связей заявки со статусами
 *
 * @package app\modules\app\v1\models
 */
class Message extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * @param null $RID
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getByRequestID( $RID = null ) {

		return Yii::$app->db
			->createCommand( 'SELECT [[' . implode( "]],[[", self::getFieldsNames() ) . ']]
 FROM ' . self::getTableName() . '
 WHERE [[order_id]] = :order_id
 ORDER BY [[created_at]] DESC', [ 'order_id' => $RID ] )
			->queryAll();
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"code",
			"name",
			"user_id_from",
			"user_id_to",
			"order_id",
			"message",
			"type_id",
			"status_id",
			"sys_date",
			"created_at",
			"updated_at",
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{message}}";
	}
}