<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class RegisterLog - журнал событий регистрации
 *
 * @package app\modules\app\v1\models
 */
class RegisterLog extends Model {
	use WritableTrait;
	use ReadableTrait;

	/**
	 * @var int ID записи
	 */
	public $id;
	/**
	 * @var string код лида
	 */
	public $code;
	/**
	 * @var int ID клиента
	 */
	public $user_id;
	/**
	 * @var string номер телефона клиента
	 */
	public $login;
	/**
	 * @var string фамилия
	 */
	public $first_name;
	/**
	 * @var string имя
	 */
	public $last_name;
	/**
	 * @var string отчество
	 */
	public $patronymic;
	/**
	 * @var string серия паспорта
	 */
	public $passport_serial;
	/**
	 * @var string номер паспорта
	 */
	public $passport_number;
	/**
	 * @var string оператор связи
	 */
	public $sms_vendor;
	/**
	 * @var string заголовок браузера
	 */
	public $user_agent;
	/**
	 * @var string тип подписываемого документа
	 */
	public $document_type;
	/**
	 * @var int смс код
	 */
	public $key_code;
	/**
	 * @var int код отправлен
	 */
	public $key_sent;
	/**
	 * @var int код доставлен
	 */
	public $key_delivered;
	/**
	 * @var int код принят
	 */
	public $key_accepted;
	/**
	 * @var int документ подписан
	 */
	public $key_signed;
	/**
	 * @var string дата создания
	 */
	public $created_at;
	/**
	 * @var string дата обновления
	 */
	public $updated_at;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'login', 'code' ], 'required' ],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{register_log}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"code",
			"user_id",
			"login",
			"first_name",
			"last_name",
			"patronymic",
			"passport_serial",
			"passport_number",
			"sms_vendor",
			"user_agent",
			"document_type",
			"key_code",
			"key_sent",
			"key_delivered",
			"key_accepted",
			"key_signed",
			"created_at",
			"updated_at",
		];
	}
}