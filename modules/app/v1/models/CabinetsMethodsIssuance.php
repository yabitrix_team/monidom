<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class CabinetsMethodsIssuance - модель для работы с таблицей связей кабинетов и способов выдачи
 *
 * @package app\modules\app\v1\models
 */
class CabinetsMethodsIssuance extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;
	/**
	 * @var - идентификатор кабинета
	 */
	public $cabinet_id;
	/**
	 * @var - идентификатор метода выдачи
	 */
	public $method_id;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{cabinets_methods_issuance}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"cabinet_id",
			"method_id",
		];
	}

	/**
	 * @return array правила валидации передаваемых данных
	 */
	public function rules() {

		return [
			[
				[ 'method_id', 'cabinet_id' ],
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'method_id',
				'number',
				'message' => 'Идентификатор способа оплаты указан в некорректном формате',
			],
			[
				'cabinet_id',
				'number',
				'message' => 'Идентификатор кабинета указан в некорректном формате',
			],
			[
				'is_sended',
				'default',
				'value' => 0,
			],
		];
	}

}