<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class AutoModels - модель для работы со справочником Модели авто
 *
 * @package app\modules\app\v1\models
 */
class AutoModels extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{auto_models}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"auto_brand_id",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	/**
	 * Метод получения моделей по идентификатору брэнда
	 *
	 * @param      $id     - Идентификатор брэнда авто
	 * @param bool $active - Параметр отвечающий за получение всех моделей авто, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array|bool- Вернет массив данных или ложь <br/>
	 *                   id <br/>
	 *                   guid <br/>
	 *                   name <br/>
	 *                   active <br/>
	 *                   sort <br/>
	 *                   auto_brand_id <br/>
	 * @throws \yii\db\Exception
	 */
	public function getByAutoBrandId( $id, $active = true ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}
		$active = is_null( $active ) ? "" : ( empty( $active ) ? " [[active]] = 0 AND " : " [[active]] = 1 AND " );

		return Yii::$app->db->createCommand( "SELECT" . $this->prepareFieldsNames() . "
FROM " . $this->getTableName() . "
WHERE " . $active . " [[auto_brand_id]] = :id ORDER BY [[name]] asc", [ ':id' => $id ] )
		                    ->queryAll();
	}

	/**
	 * Метод получения данных по ГУИД
	 *
	 * @param string|null $guid   - ГУИД элемента
	 * @param array       $fields - поля выборки
	 *
	 * @return mixed|\Exception - вернет либо значение если в fields одно поле
	 *                          либо ассоциативный массив если fields пустой или несколько значений
	 *                          объект если будет выброшено исключение
	 */
	public function getByGuid( string $guid = null, array $fields = [] ) {

		try {
			$fields = ! empty( $fields ) ? $fields : $this->getFieldsNames();
			$method = count( $fields ) == 1 ? 'queryScalar' : 'queryOne';
			$fields = '[[' . implode( "]],[[", $fields ) . ']]';

			return Yii::$app->db
				->createCommand(
					'SELECT ' . $fields . ' FROM ' . $this->getTableName() . ' WHERE [[guid]] =:guid',
					[ ':guid' => $guid ]
				)->$method();
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}