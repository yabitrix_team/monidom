<?php

namespace app\modules\app\v1\models\traits;

use yii;

/**
 * Возможность чтения из таблиц моделей
 *
 * @package app\modules\app\v1\models\traits
 */
trait ReadableTrait {
	/**
	 * Получение записи модели по внешнему коду
	 *
	 * @param string|null $guid внешний код
	 *
	 * @return array|bool результат запроса
	 * @throws yii\db\Exception
	 */
	public function getByGuid( string $guid = null ) {

		if ( ! strlen( $guid ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . " where [[guid]]=:guid", [ ":guid" => $guid ] )
			->queryOne();
	}

	/**
	 * Экранирование названий полей модели для DAO-запросов
	 *
	 * @return string экранированная строка списка полей
	 */
	private function prepareFieldsNames($fields = null) {

        $fields = is_array($fields) && ! empty( $fields ) ? $fields : $this->getFieldsNames();

		return "[[" . implode( "]],[[", $fields ) . "]]";
	}

	/**
	 * Получения всех записей модели по фильтру
	 *
	 * @param array $filter   фильтр
	 * @param array $sort     сортировка
	 *                        [
	 *                        'column1' => 'acs'
	 *                        'column2' => 'desc'
	 *                        ]
	 * @param integer $offset отступ отвыборки
	 * @param integer $limit количество элементов на возврат
	 * @return array|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getAllByFilter( array $filter = [], array $sort = [], $offset = null, $limit = null ) {

		if ( empty( $filter ) && empty( $sort ) ) {
			return false;
		}

		return $this->getByFilter( $filter, $sort, $offset, $limit )->queryAll();
	}

	/**
	 * Получения количества записей модели по фильтру
	 *
	 * @param array $filter   фильтр
	 *
	 * @return array|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getCountByFilter( array $filter = []) {

		if ( empty( $filter ) ) {
			return false;
		}

		return $this->countByFilter( $filter)->queryOne()["cnt"];
	}

	/**
	 * Подготовка получения записей модели по фильтру
	 *
	 * @param array $filter   фильтр
	 * @param array $sort     сортировка
	 *                        [
	 *                        'column1' => 'acs'
	 *                        'column2' => 'desc'
	 *                        ]
	 * @param integer $offset отступ отвыборки
	 * @param integer $limit количество элементов на возврат
	 *
	 * @return \yii\db\Command результат запроса
	 */
	protected function getByFilter( array $filter = null, array $sort = null, $offset = null, $limit = null) {

		$logic = "and";
		if ( reset( array_keys( $filter ) ) == "or" ) {
			$filter = reset( $filter );
			$logic  = "or";
		}
		$conditions = [];
		foreach ( $filter as $key => $value ) {
			$prefix = "";
			if ( preg_match( '/^[\!\<\>\=\?]+/s', $key, $mKey ) ) {
				$prefix = reset( $mKey );
				$key    = str_replace( $prefix, "", $key );
			}
			$key = "[[" . $key . "]]";
			if ( is_array( $value ) ) {
				// пропускаем пустые значения-массивы
				if ( empty( $value ) ) {
					continue;
				}
				$bNumeric = true;
				foreach ( $value as $val ) {
					if ( ! is_numeric( $val ) ) {
						$bNumeric = false;
						break;
					}
				}
				$conditions[] = $key . ( $prefix == "!" ? " not" : "" ) . " in (" . ( $bNumeric ? "" : "'" ) . implode(
						( $bNumeric ? ", " : "', '" ), $value
					) . ( $bNumeric ? "" : "'" ) . ")";
				continue;
			}
			$value = is_numeric( $value ) ? $value : "'" . $value . "'";
			switch ( $prefix ) {
				case "<":
				case "<=":
				case ">":
				case ">=":
					$conditions[] = $key . $prefix . $value;
					break;
				case "!":
					$conditions[] = $key . " not " . $value;
					break;
				case "?":
					$conditions[] = $key . " like '%" . str_replace('\'', '', $value). "%'";
					break;
				default:
					$conditions[] = $key . "=" . $value;
			}
		}
		//добавляем сортировку
		$stringOrder = '';
		if ( ! empty( $sort ) ) {
			$stringOrder = ' order by';
			foreach ( $sort as $column => $direction ) {
				$stringOrder = $stringOrder . ' [[' . $column . ']] ' . $direction . ',';
			}
			$stringOrder = rtrim( $stringOrder, ',' );
		}
		$limitString = '';
		if( ! empty($limit) ) {
			$limitString = ' limit ' .$offset . ', ' . $limit;
		}
		if ( empty( $filter ) ) {

			if (! empty ($limitString)) {
				return Yii::$app->db
					->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . $stringOrder . $limitString);
			}
			return Yii::$app->db
				->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . $stringOrder );
		}

		if (! empty ($limitString)) {
			return Yii::$app->db
				->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . " where " . implode( " $logic ", $conditions ) . $stringOrder . $limitString);
		}
		return Yii::$app->db
			->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . " where " . implode( " $logic ", $conditions ) . $stringOrder );
	}

	/**
	 * Подготовка получения количества элементов по фильтру
	 * это упрощенная функция getByFilter
	 *
	 * @param array $filter   фильтр*
	 * @return \yii\db\Command результат запроса
	 */
	protected function countByFilter( array $filter = null) {

		$logic = "and";
		if ( reset( array_keys( $filter ) ) == "or" ) {
			$filter = reset( $filter );
			$logic  = "or";
		}
		$conditions = [];
		foreach ( $filter as $key => $value ) {
			$prefix = "";
			if ( preg_match( '/^[\!\<\>\=\?]+/s', $key, $mKey ) ) {
				$prefix = reset( $mKey );
				$key    = str_replace( $prefix, "", $key );
			}
			$key = "[[" . $key . "]]";
			if ( is_array( $value ) ) {
				// пропускаем пустые значения-массивы
				if ( empty( $value ) ) {
					continue;
				}
				$bNumeric = true;
				foreach ( $value as $val ) {
					if ( ! is_numeric( $val ) ) {
						$bNumeric = false;
						break;
					}
				}
				$conditions[] = $key . ( $prefix == "!" ? " not" : "" ) . " in (" . ( $bNumeric ? "" : "'" ) . implode(
						( $bNumeric ? ", " : "', '" ), $value
					) . ( $bNumeric ? "" : "'" ) . ")";
				continue;
			}
			$value = is_numeric( $value ) ? $value : "'" . $value . "'";
			switch ( $prefix ) {
				case "<":
				case "<=":
				case ">":
				case ">=":
					$conditions[] = $key . $prefix . $value;
					break;
				case "!":
					$conditions[] = $key . " not " . $value;
					break;
				case "?":
					$conditions[] = $key . " like '%" . str_replace('\'', '', $value). "%'";
					break;
				default:
					$conditions[] = $key . "=" . $value;
			}
		}

		return Yii::$app->db
			->createCommand( "select count(*) cnt from " . $this->getTableName() . " where " . implode( " $logic ", $conditions ) );
	}

	/**
	 * Получение одной записи модели по фильтру
	 *
	 * @param array $filter фильтр
	 *
	 * @return array|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getOneByFilter( array $filter = [] ) {

		if ( empty( $filter ) ) {
			return false;
		}

		return $this->getByFilter( $filter )->queryOne();
	}

	/**
	 * Получение набора данных модели по фильтру
	 *
	 * @param array $filter фильтр
	 *
	 * @return yii\db\DataReader|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getResByFilter( array $filter = [] ) {

		if ( empty( $filter ) ) {
			return false;
		}

		return $this->getByFilter( $filter )->query();
	}

	/**
	 * Получение всех записей модели
	 *
	 * @param bool $active - фильтрация по активности
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? "" : ( empty( $active ) ? " WHERE [[active]] = 0 " : " WHERE [[active]] = 1 " );

		return Yii::$app->db->createCommand( "SELECT " . $this->prepareFieldsNames() . " FROM " . $this->getTableName() . " " . $active )
		                    ->queryAll();
	}

	/**
	 * Получение записи модели по идентификатору
	 *
	 * @param $id - Идентификатор записи
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id, array $fields = [] ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "SELECT " . $this->prepareFieldsNames($fields) . " FROM " . $this->getTableName() . " WHERE [[id]]=:id", [ ":id" => $id ] )
			->queryOne();
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	abstract protected function getTableName(): string;

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	abstract protected function getFieldsNames(): array;
}
