<?php
namespace app\modules\app\v1\models\traits;

use yii;

/**
 * Возможность записи/обновления в таблицах моделей
 *
 * @package app\modules\app\v1\models\traits
 */
trait WritableTrait
{
    /**
     * Добавление экземпляра модели в БД
     *
     * @param array $fields массив полей
     *
     * @return bool|int результат выполнения
     * @throws yii\db\Exception
     */
    public function add(array $fields = [])
    {
        if (empty($fields)) {
            return false;
        }
        if (empty($fields["created_at"])) {
            $fields["created_at"] = new Yii\db\Expression('NOW()');
        }
        if (
        Yii::$app->db
            ->createCommand()
            ->insert($this->getTableName(), $fields)
            ->execute()
        ) {
            return Yii::$app->db->getLastInsertId();
        }

        return false;
    }

    /**
     * Обновление записи модели по ключу
     *
     * @param int|string $condition первичный ключ|условие запроса
     * @param array      $fields    массив полей
     *
     * @return bool|int результат выполнения
     * @throws yii\db\Exception
     */
    public function update($condition, array $fields = [])
    {
        $params = [];
        if (is_numeric($condition)) {
            $params[":id"] = (int) $condition;
            $condition     = "[[id]]=:id";
        }
        if (empty($condition) || empty($fields)) {
            return false;
        }
        $fields["updated_at"] = new Yii\db\Expression('NOW()');

        return Yii::$app->db
            ->createCommand()
            ->update($this->getTableName(), $fields, $condition, $params)
            ->execute();
    }

    /**
     * Обновление записи модели по нескольким полям
     *
     * @param array $condition [Поле1=>Значение1, Поле2=>Значение2]
     * @param array $fields    массив полей
     *
     * @return bool|int результат выполнения
     * @throws yii\db\Exception
     */
    public function updateByFields($condition, array $fields = [])
    {
        $params = [];
        foreach ($condition as $k => $item) {

            $params[":".$k] = $item;
            $condition      = "[[".$k."]]=:".$k;
        }
        if (empty($condition) || empty($fields)) {
            return false;
        }
        $fields["updated_at"] = new Yii\db\Expression('NOW()');

        return Yii::$app->db
            ->createCommand()
            ->update($this->getTableName(), $fields, $condition, $params)
            ->execute();
    }

    /**
     * Удаление записи модели по ключу
     *
     * @param       int|string $condition первичный ключ|условие запроса
     *
     * @return bool|int результат выполнения
     * @throws yii\db\Exception
     */
    public function delete($condition)
    {
        $params = [];
        if (is_numeric($condition)) {
            $params[":id"] = (int) $condition;
            $condition     = "[[id]]=:id";
        }
        if (empty($condition)) {
            return false;
        }

        return Yii::$app->db
            ->createCommand()
            ->delete($this->getTableName(), $condition, $params)
            ->execute();
    }

    /**
     * Метод подготовки данных перед выполнением,
     * объединяет доп. данные с загруженными через $model->load
     * с последующей очисткой от параметров со значением NULL
     *
     * @param array $params - дополнительные параметры
     *
     * @return array - вернет подготовленный массив
     * @throws \Exception - выбросит иисключение, если вызов метода произведен
     *                    в класса не унаследованном от yii\base\Model
     */
    protected function prepareDataForExecute(array $params = [])
    {
        if (!$this instanceof yii\base\Model) {
            throw new \Exception('Попытка вызова метода подготовки данных в несоответствующей моделе');
        }
        $arPrepare = [];
        $data      = empty($params) ? $this->getAttributes() : array_merge($this->getAttributes(), $params);
        //подготавливаем массив для записи
        foreach ($data as $k => $v) {
            if (!is_null($v)) {
                $arPrepare[$k] = $v;
            }
        }

        return $arPrepare;
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    abstract protected function getTableName(): string;
}