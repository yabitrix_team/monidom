<?php

namespace app\modules\app\v1\models\traits;

use yii;

/**
 * Возможность чтения из таблиц моделей
 *
 * @package app\modules\app\v1\models\traits
 */
trait ReadableTraitEx {
	/**
	 * Получение записи модели по внешнему коду
	 *
	 * @param string|null $guid внешний код
	 *
	 * @return array|bool результат запроса
	 * @throws yii\db\Exception
	 */
	public function getByGuid( string $guid = null, array $select = [] ) {

		if ( ! strlen( $guid ) ) {
			return false;
		}

		return $this->getByFilter( [ 'guid' => $guid ], [], $select )->queryOne();
	}

	/**
	 * Экранирование названий полей модели для DAO-запросов
	 *
	 * @return string экранированная строка списка полей
	 */
	private function prepareFieldsNames() {

		return "[[" . implode( "]],[[", $this->getFieldsNames() ) . "]]";
	}

	/**
	 * Получения всех записей модели по фильтру
	 *
	 * @param array $filter   фильтр
	 * @param array $sort     сортировка
	 *                        [
	 *                        'column1' => 'acs'
	 *                        'column2' => 'desc'
	 *                        ]
	 *
	 * @return array|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getAllByFilter( array $filter = [], array $sort = [], array $select = [] ) {

		if ( empty( $filter ) && empty( $sort ) ) {
			return false;
		}

		return $this->getByFilter( $filter, $sort, $select )->queryAll();
	}

	/**
	 * Подготовка получения записей модели по фильтру
	 *
	 * @param array $filter   фильтр
	 * @param array $sort     сортировка
	 *                        [
	 *                        'column1' => 'acs'
	 *                        'column2' => 'desc'
	 *                        ]
	 *
	 * @return \yii\db\Command результат запроса
	 */
	protected function getByFilter( array $filter = [], array $sort = [], array $select = [] ) {

		$logic = "and";
		if ( reset( array_keys( $filter ) ) == "or" ) {
			$filter = reset( $filter );
			$logic  = "or";
		}
		$conditions = [];
		foreach ( $filter as $key => $value ) {
			$prefix = "";
			if ( preg_match( '/^[\!\<\>\=]+/s', $key, $mKey ) ) {
				$prefix = reset( $mKey );
				$key    = str_replace( $prefix, "", $key );
			}
			$key = "[[" . $key . "]]";
			if ( is_array( $value ) ) {
				// пропускаем пустые значения-массивы
				if ( empty( $value ) ) {
					continue;
				}
				$bNumeric = true;
				foreach ( $value as $val ) {
					if ( ! is_numeric( $val ) ) {
						$bNumeric = false;
						break;
					}
				}
				$conditions[] = $key . ( $prefix == "!" ? " not" : "" ) . " in (" . ( $bNumeric ? "" : "'" ) . implode(
						( $bNumeric ? ", " : "', '" ), $value
					) . ( $bNumeric ? "" : "'" ) . ")";
				continue;
			}
			$value = is_numeric( $value ) ? $value : "'" . $value . "'";
			switch ( $prefix ) {
				case "<":
				case "<=":
				case ">":
				case ">=":
					$conditions[] = $key . $prefix . $value;
					break;
				case "!":
					$conditions[] = $key . " not " . $value;
					break;
				case "?":
					$conditions[] = $key . " like '%" . $value . "%'";
					break;
				default:
					$conditions[] = $key . "=" . $value;
			}
		}
		// добавляем поля для select
		$stringSelect = '';
		if ( ! empty( $select ) ) {
			$stringSelect = '[[' . implode(']], [[', $select) . ']]';
		}
		$stringSelect = ! empty( $stringSelect ) ? $stringSelect : $this->prepareFieldsNames();

		//добавляем сортировку
		$stringOrder = '';
		if ( ! empty( $sort ) ) {
			$stringOrder = ' order by';
			foreach ( $sort as $column => $direction ) {
				$stringOrder = $stringOrder . ' [[' . $column . ']] ' . $direction . ',';
			}
			$stringOrder = rtrim( $stringOrder, ',' );
		}
		if ( empty( $filter ) ) {

			return Yii::$app->db
				->createCommand( "select " . $stringSelect . " from " . $this->getTableName() . $stringOrder );
		}

		return Yii::$app->db
			->createCommand( "select " . $stringSelect . " from " . $this->getTableName() . " where " . implode( " $logic ", $conditions ) . $stringOrder );
	}

	/**
	 * Получение одной записи модели по фильтру
	 *
	 * @param array $filter фильтр
	 *
	 * @return array|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getOneByFilter( array $filter = [], $select = [] ) {

		if ( empty( $filter ) ) {
			return false;
		}

		return $this->getByFilter( $filter, [], $select )->queryOne();
	}

	/**
	 * Получение набора данных модели по фильтру
	 *
	 * @param array $filter фильтр
	 *
	 * @return yii\db\DataReader|false результат выборки
	 * @throws yii\db\Exception
	 */
	public function getResByFilter( array $filter = [], array $select = [] ) {

		if ( empty( $filter ) ) {
			return false;
		}

		return $this->getByFilter( $filter, [], $select )->query();
	}

	/**
	 * Получение всех записей модели
	 *
	 * @param bool $active - фильтрация по активности
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = 1, array $select = [] ) {

		if ( ! is_numeric( $active ) ) {
			return false;
		}

		return $this->getByFilter( [ 'active' => $active ], [], $select )->queryAll();
	}

	/**
	 * Получение записи модели по идентификатору
	 *
	 * @param $id - Идентификатор записи
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id, array $select = [] ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return $this->getByFilter( [ 'id' => $id ], [], $select )->query();
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	abstract protected function getTableName(): string;

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	abstract protected function getFieldsNames(): array;
}
