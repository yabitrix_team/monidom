<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;

class RequestCommission extends Model {
	use ReadableTrait;
	use WritableTrait;

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {
		return [
			"id",
			"request_id",
			"is_null",
			"commission_contract",
			"commission_cash_withdrawal",
			"monthly_fee",
			"received",
			"created_at",
			"updated_at",
			"active",
			"sort"
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {
		return "{{request_commission}}";
	}
}