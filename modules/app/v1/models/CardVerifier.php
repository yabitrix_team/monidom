<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class BankCard - модель для работы с проверкой банковской карты
 *
 * @package app\modules\app\v1\models
 */
class CardVerifier extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

    public $request_id;
    public $external_id;
    public $operation_id;
    public $amount;
    public $url;
    public $card;
    public $token;
    public $event_id;



	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{card_verifier}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
            "chain_id",
			"request_id",
            "external_id",
            "operation_id",
            "amount",
            "url",
			"card",
			"token",
            "event_id",
		];
	}

    /**
     * @return array - правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            [['request_id', 'external_id', 'event_id'], 'required', 'message' => 'Не заполнены обязательные поля'],
            [
                'url',
                'string',
                'length'   => [3],
                'tooShort' => 'Поле "URL" должно содержать минимум 3 символа',
            ],
            [
                'url',
                'string',
                'length'   => [11],
                'tooShort' => 'Поле "URL" должно содержать минимум 11 символа',
            ],
            [
                'card',
                'string',
                'length'   => [16],
                'tooShort' => 'Поле "CARD" должно содержать минимум 16 символа',
            ],
            [
                'token',
                'string',
                'length'   => [7],
                'tooShort' => 'Поле "CARD" должно содержать минимум 7 символа',
            ],
        ];
    }
}