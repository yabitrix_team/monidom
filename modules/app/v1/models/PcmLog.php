<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class PcmLog extends Model {
	/**
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function get() {

		return Yii::$app->db->createCommand( 'SELECT * FROM pcm_log' )
		                    ->queryAll();
	}
}