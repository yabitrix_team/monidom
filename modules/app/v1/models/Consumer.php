<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Consumer - модель для работы с таблицей Потребитель
 * данные из указанной таблицы используются в таблице Pages
 * для указания принадлежности контента
 *
 * @package app\modules\app\v1\models
 */
class Consumer extends Model {
	use ReadableTrait;

	/**
	 * Метод получения всех элементов
	 *
	 * @param bool $active - Параметр отвечающий за получение всех элементов, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' WHERE `active` = 0 ' : ' WHERE `active` = 1 ' );

		return Yii::$app->db->createCommand( 'SELECT
  `id`,
  `name`,
  `active`,
  `sort`
FROM consumer ' . $active )
		                    ->queryAll();
	}

	/**
	 * Метод получения одного элемента
	 *
	 * @param $id - Идентификатор элемента
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( 'SELECT
  `name`,
  `active`
FROM consumer
WHERE `id` = :id', [ ':id' => $id ] )
			->queryOne();
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{consumer}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			'id',
			'name',
			'active',
		];
	}
}