<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class File - модель взаимодействия с таблицей файлов
 *
 * @package app\modules\app\v1\models
 */
class File extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return "{{file}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    public function getFieldsNames(): array
    {

        return [
            "id",
            "code",
            "name",
            "path",
            "height",
            "width",
            "type",
            "size",
            "description",
            "sim_link",
            "created_at",
            "updated_at",
            "active",
            "sort",
        ];
    }

    /**
     * @var - название файла
     */
    public $name;
    /**
     * @var - путь к файлу
     */
    public $path;
    /**
     * @var - высота для изображений
     */
    public $height;
    /**
     * @var - ширина для изображений
     */
    public $width;
    /**
     * @var - тип файла
     */
    public $type;
    /**
     * @var - размер файла
     */
    public $size;
    /**
     * @var - описание файла
     */
    public $description;
    /**
     * @var - необязательный параметр для указания префикса в символьном коде файла
     */
    public $prefix;
    /**
     * @var int - отвечает за активность записи, 1 - активно, 0 - неактивно
     */
    public $active;

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {

        return [
            [
                'name',
                'required',
                'message' => 'Не указано название файла, обратитесь к администратору',
            ],
            [
                'path',
                'required',
                'message' => 'Не указан путь к файлу, обратитесь к администратору',
            ],
            [
                'type',
                'required',
                'message' => 'Не указан тип файла, обратитесь к администратору',
            ],
            [
                'size',
                'required',
                'message' => 'Не указан размер файла, обратитесь к администратору',
            ],
            [
                'name',
                'string',
                'message' => 'Имя файла указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'height',
                'number',
                'message' => 'Значение высоты картинки указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'width',
                'number',
                'message' => 'Значение ширины картинки указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'type',
                'string',
                'message' => 'Значение для типа файла указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'size',
                'number',
                'message' => 'Значение для размера файла указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'description',
                'string',
                'message' => 'Значение для описания файла указано в некорректном формате, обратитесь к администратору',
            ],
            [
                'prefix',
                'string',
                'message' => 'Значение префикса кода файла указано в некорректном формате, обратитесь к администратору',
                'max'     => 12,
                'tooLong' => 'Длина префикса кода файла указана больше чем допустимо, обратитесь к администратору',
            ],
            [
                'active',
                'default',
                'value' => 1,
            ],
        ];
    }

    /**
     * Метод для создания записи о загруженном файле
     *
     * @return array|string - вернет массив содержащий код и идентификатор файла или строку с исключением
     */
    public function create()
    {

        try {
            $insert               = $this->getAttributes(null, ['prefix']);
            $code                 = md5(str_shuffle('file_code'.time()).microtime(true));
            $insert['code']       = empty($this->prefix)
                ? $code
                : substr_replace($code, $this->prefix, 0, strlen($this->prefix));
            $insert['sim_link']   = md5(str_shuffle('file_sim_link'.time()).microtime(true));
            $insert['created_at'] = date('Y-m-d H:i:s', time());
            $result               = Yii::$app->db->createCommand()
                ->insert($this->getTableName(), $insert)
                ->execute();
            if (empty($result)) {
                throw new \Exception('Произошла ошибка при вставке данных о файле');
            }

            return [
                'id'   => Yii::$app->db->getLastInsertID(),
                'code' => $insert['code'],
            ];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод обновления активности записи по списку идентификаторов
     *
     * @param array $listId - список идентификаторов
     * @param int   $active - значение активности, принимает 1 или 0
     *
     * @return bool - вернет истину в случае успешного обновления, иначе ложь
     * @throws \Exception
     */
    public function updateActiveByListId(array $listId, int $active = 1)
    {
        try {
            $result = false;
            if (!empty($listId)) {
                $strId  = implode(',', $listId);
                $result = Yii::$app->db
                    ->createCommand()
                    ->update($this->getTableName(), ['active' => $active], '[[id]] IN ('.$strId.')')
                    ->execute();
            }

            return boolval($result);
        } catch (\Exception $e) {
            throw new \Exception(
                'Произошла ошибка при обновлении активности записи в таблице БД, обратитесь к администратору'
            );
        }
    }

    /**
     * Удаление записи о файле по идентификатору
     * Каскадно удаляет запись из таблицы File и Request_File
     *
     * @param $id - иентификатор записи
     *
     * @return int|string - вернут кол-во удаленных строк или строку с исключением
     */
    public function deleteOne($id)
    {

        try {
            return Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), '[[id]] = :id', [':id' => $id])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления файлов заявки относительно привязок файлов к заявке,
     * т.е удаляет файл заявки по привязкам
     *
     * @param       $reqId    - идентификатор заявки
     * @param array $fileBind - параметры привязки заявки к файлам,
     *                        если передать пустой массив удалит все файлы заявки
     *
     * @return int|string - вернет кол-во удаленных строк или стоку с исключенем
     */
    public function deleteByRequestIdAndFileBind($reqId, array $fileBind = [])
    {

        try {
            $listFileBind = (new RequestFile())->getListFileBind();
            if (!empty($fileBind)) {
                $diffFileBind = array_diff($fileBind, $listFileBind);
                if (!empty($diffFileBind)) {
                    throw new \Exception(
                        'Невозмножно произвести удаление файлов заявки, так как некорректно указаны следующие параметры привязки файлов к заявке: '.
                        implode(', ', $diffFileBind)
                    );
                }
            }
            $fileBind = empty($fileBind) ? $listFileBind : $fileBind;

            return Yii::$app->db
                ->createCommand()
                ->delete(
                    $this->getTableName(),
                    '[[id]] IN (SELECT [[file_id]] FROM {{request_file}} WHERE [[request_id]] = :reqId AND [[file_bind]] IN ("'.
                    implode('","', $fileBind).'") )',
                    [':reqId' => $reqId]
                )
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения записи о файле по идентификатору
     *
     * @param $id - идентификатор записи о файле
     *
     * @return array|false|string - вернет результирующий массив, ложь или строку с исключением
     */
    public function getOne($id)
    {

        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
[[code]],
[[name]],
[[path]],
[[height]],
[[width]],
[[type]],
[[size]],
[[description]],
[[sim_link]],
DATE_FORMAT([[created_at]], '%Y-%m-%dT00:00:00.000Z') AS [[created_at]],
DATE_FORMAT([[updated_at]], '%Y-%m-%dT00:00:00.000Z') AS [[updated_at]],
[[active]],
[[sort]]
FROM ".$this->getTableName()."
WHERE [[id]] = :id",
                    [':id' => $id]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения записи о файле по коду
     *
     * @param $code - код файла
     *
     * @return array|false|string - вернет результирующий массив, ложь или строку с исключением
     */
    public function getByCode($code)
    {

        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
[[id]],
[[name]],
[[path]],
[[height]],
[[width]],
[[type]],
[[size]],
[[description]],
[[sim_link]],
DATE_FORMAT([[created_at]], '%Y-%m-%dT00:00:00.000Z') AS [[created_at]],
DATE_FORMAT([[updated_at]], '%Y-%m-%dT00:00:00.000Z') AS [[updated_at]],
[[active]],
[[sort]]
FROM ".$this->getTableName()."
WHERE [[code]] = :code",
                    [':code' => $code]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных файла и заявки по коду заявки и типу файла
     *
     * @param string $codeRequest       - код заявки
     * @param string $fileBind          - привязка файла
     * @param array  $fieldsFile        - поля файла для выборки, пример
     *                                  [
     *                                  'id',
     *                                  'file_code'=>'code' //ключ массива это псевдонимом значения(code as file_code)
     *                                  ]
     * @param array  $fieldsRequest     - поля заявки для выборки, пример соответствует примеру в fieldsFile
     * @param array  $fieldsRequestFile - поля связи заявки с файлом для выборки, пример соответствует примеру в
     *                                  fieldsFile
     *
     * @return array|string - вернет результирующий массив или строку с исключением
     * @throws \Exception
     */
    public function getByCodeRequestAndType(
        string $codeRequest,
        string $fileBind,
        array $fieldsFile = [],
        array $fieldsRequest = [],
        array $fieldsRequestFile = []
    ) {
        try {
            $selectFields = "
            [[f]].[[id]],
            [[f]].[[code]],
            [[f]].[[name]],
            [[f]].[[path]],
            [[f]].[[height]],
            [[f]].[[width]],
            [[f]].[[type]],
            [[f]].[[size]],
            [[f]].[[description]],
            [[f]].[[sim_link]],
            DATE_FORMAT([[f]].[[created_at]], '%Y-%m-%dT%H:%i:%s.000Z') AS [[created_at]],
            DATE_FORMAT([[f]].[[updated_at]], '%Y-%m-%dT%H:%i:%s.000Z') AS [[updated_at]],
            [[f]].[[active]],
            [[f]].[[sort]],
            [[rf]].[[file_bind]]
            ";
            if (!empty($fieldsFile) || !empty($fieldsRequest) || !empty($fieldsRequestFile)) {
                //переменная с анонимной функцией для подготовки полей запроса в бд
                $strFields    = function (array $fields, $prefix) {
                    if (empty($fields)) {
                        return '';
                    }
                    array_walk(
                        $fields,
                        function (&$v, &$k) use ($prefix) {
                            $v = '[['.$prefix.']].'.(is_numeric($k) ? '[['.$v.']]' : '[['.$v.']] AS [['.$k.']]');
                        }
                    );

                    return implode(',', $fields).',';
                };
                $selectFields = rtrim(
                    $strFields($fieldsFile, 'f').$strFields($fieldsRequest, 'r').$strFields($fieldsRequestFile, 'rf'),
                    ','
                );
            }

            return Yii::$app->db
                ->createCommand(
                    "SELECT ".$selectFields."
FROM file AS [[f]]
LEFT JOIN [[request_file]] AS [[rf]] ON [[f]].[[id]] = [[rf]].[[file_id]]
LEFT JOIN [[requests]] AS [[r]] ON [[rf]].[[request_id]] = [[r]].[[id]]
WHERE [[r]].[[code]] = :codeValue AND [[rf]].[[file_bind]] = :fileBind",
                    [
                        ':codeValue' => $codeRequest,
                        ':fileBind'  => $fileBind,
                    ]
                )
                ->queryAll();
        } catch (Exception $e) {
            throw new \Exception(
                'Произошла ошибка в работе с БД при получении файлов по заявке, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Удаление записи о файле по коду
     *
     * @param $code - код записи о файле
     *
     * @return int|string - вернут кол-во удаленных строк или строку с исключением
     */
    public function deleteByCode($code)
    {

        try {
            return Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), 'code = :code', [':code' => $code])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных файла по идентификатору заявки
     *
     * @param $id - идентификатор заявки
     *
     * @return array|string - вернет массив данных по файлу или строку с искючением
     */
    public function getByRequestId($id)
    {

        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
[[f]].[[id]],
[[f]].[[code]],
[[f]].[[name]],
[[f]].[[path]],
[[f]].[[height]],
[[f]].[[width]],
[[f]].[[type]],
[[f]].[[size]],
[[f]].[[description]],
[[f]].[[sim_link]],
DATE_FORMAT([[f]].[[created_at]], '%Y-%m-%dT%H:%i:%s.000Z') AS [[created_at]],
DATE_FORMAT([[f]].[[updated_at]], '%Y-%m-%dT%H:%i:%s.000Z') AS [[updated_at]],
[[f]].[[active]],
[[f]].[[sort]],
[[rf]].[[file_bind]]
FROM ".$this->getTableName()." AS [[f]]
LEFT JOIN {{request_file}} AS [[rf]] ON [[f]].[[id]] = [[rf]].[[file_id]]
WHERE [[request_id]] = :id ",
                    [':id' => $id]
                )
                ->queryAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения файлов документов по коду заявки
     *
     * @param $code - код заявки
     * @param $num  - номер пакета документов
     *
     * @return array|string - вернет массив данных по файлу или строку с искючением
     */
    public function getDocByRequestCode($code, $num)
    {

        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
[[f]].[[id]],
[[f]].[[code]],
[[f]].[[name]],
[[f]].[[path]],
[[f]].[[height]],
[[f]].[[width]],
[[f]].[[type]],
[[f]].[[size]],
[[f]].[[description]],
[[f]].[[sim_link]],
DATE_FORMAT([[f]].[[created_at]], '%Y-%m-%dT00:00:00.000Z') AS [[created_at]],
DATE_FORMAT([[f]].[[updated_at]], '%Y-%m-%dT00:00:00.000Z') AS [[updated_at]],
[[f]].[[active]],
[[f]].[[sort]]
FROM ".$this->getTableName()." AS [[f]]
LEFT JOIN [[request_file]] AS [[rf]] ON [[f]].[[id]] = [[rf]].[[file_id]]
LEFT JOIN [[requests]] AS [[r]] ON [[rf]].[[request_id]] = [[r]].[[id]]
WHERE [[r]].[[code]] = :code AND [[rf]].[[file_bind]] = :docPack",
                    [
                        ':code'    => $code,
                        ':docPack' => 'doc_pack_'.$num,
                    ]
                )
                ->queryAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения идентификатора заявки по коду файла
     *
     * @param $code - код файла
     *
     * @return false|string|null - вернет идентификатор заявки
     * @throws \Exception
     */
    public function getRequestIdByCode($code)
    {
        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT [[request_id]] 
                    FROM {{request_file}} 
                    WHERE [[file_id]] = 
                    (
                          SELECT [[id]] 
                          FROM ".$this->getTableName()."
                           WHERE [[code]] = :code
                    )",
                    [
                        ':code' => $code,
                    ]
                )
                ->queryScalar();
        } catch (Exception $e) {
            throw new \Exception(
                'При попытке получения идентификатора заявки произошла ошибка базы данных, обратитесь к администратору'
            );
        }
    }
}