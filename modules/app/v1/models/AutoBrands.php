<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;
use yii\base\Model;
use app\modules\app\v1\models\traits\WritableTrait;

/**
 * Class AutoBrands - модель для работы со справочником Брэнды(марки) авто
 *
 * @package app\modules\app\v1\models
 */
class AutoBrands extends Model {
	use WritableTrait;
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{auto_brands}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}