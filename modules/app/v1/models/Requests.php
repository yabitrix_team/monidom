<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\validators\AutoBrandValidator;
use app\modules\app\v1\validators\AutoModelValidator;
use app\modules\app\v1\validators\CreditProductValidator;
use app\modules\app\v1\validators\EmploymentValidator;
use app\modules\app\v1\validators\PeopleRelationValidator;
use app\modules\app\v1\validators\RegionValidator;
use app\modules\app\v1\validators\WorkplacePeriodValidator;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константа сценария создания заявки
     */
    const SCENARIO_CREATE = 'create';
    /**
     * Константа сценария обновления заявки
     */
    const SCENARIO_UPDATE = 'update';
    /**
     * Константа сценария обновления заявки
     */
    const SCENARIO_CARD_VERIFIER = 'card_verifier';
    // поля для создания заявки
    public $summ;
    public $num;
    public $num_1c;
    public $date_return;
    public $auto_brand_id;
    public $auto_model_id;
    public $auto_year;
    public $auto_price;
    public $credit_product_id;
    public $client_first_name;
    public $client_last_name;
    public $client_patronymic;
    public $client_passport_number;
    public $client_passport_serial_number;
    public $client_birthday;
    public $client_region_id;
    public $client_mobile_phone;
    public $comment_client;
    public $pre_crediting;
    //поля для обновления заявки
    public $method_of_issuance_id;
    public $card_number;
    public $card_holder;
    public $bank_bik;
    public $checking_account;
    public $client_home_phone;
    public $client_email;
    public $client_total_monthly_income;
    public $client_total_monthly_outcome;
    public $client_employment_id;
    public $client_workplace_experience;
    public $client_workplace_address;
    public $client_workplace_period_id;
    public $client_workplace_phone;
    public $client_workplace_name;
    public $client_guarantor_name;
    public $client_guarantor_relation_id;
    public $client_guarantor_phone;
    public $comment_partner;
    public $mode;
    public $print_link;
    public $is_pep;
    public $place_of_stay;
    public $lcrm_id;
    public $card_token;
    public $credit_limit_code;
    /**
     * @var array - обязательные поля при создании заявки,
     *            в наследнике переопределить под свои задачи
     */
    protected $reqFieldCreate = [
        'summ',
        'date_return',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'auto_price',
        'credit_product_id',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_passport_number',
        'client_passport_serial_number',
        'client_birthday',
        'client_region_id',
        'client_mobile_phone',
    ];
    /**
     * @var array - поля создания заявки для сценария Создание,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioCreate = [
        'summ',
        'date_return',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'auto_price',
        'credit_product_id',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_passport_number',
        'client_passport_serial_number',
        'client_birthday',
        'client_region_id',
        'client_mobile_phone',
        'comment_client',
        'pre_crediting',
        'credit_limit_code',
    ];
    /**
     * @var array - поля обновления заявки для сценария Обновление,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioUpdate = [
        'client_home_phone',
        'client_email',
        'client_total_monthly_income',
        'client_total_monthly_outcome',
        'client_employment_id',
        'client_workplace_experience',
        'client_workplace_address',
        'client_workplace_period_id',
        'client_workplace_phone',
        'client_workplace_name',
        'client_guarantor_name',
        'client_guarantor_relation_id',
        'client_guarantor_phone',
        'comment_partner',
        'place_of_stay',
        'credit_limit_code',
    ];
    /**
     * @var array поля для сценария подтверждения номера карты клиента
     */
    protected $fieldScenarioCardVerifier = [
        'card_number',
        'card_token',
    ];
    /**
     * @var string - веб-сервис для скачивания документов
     */
    protected $pathDoc = '/web/v1/request/file/doc/';
    /**
     * @var string - место создания заявки, смотреть в таблице requests_origin
     * допустимые варианты: MOBILE, PARTNER, CALL, SIDES_CALL
     */
    protected $requestOrigin = RequestsOrigin::IDENTIFIER_PARTNER;
    /**
     * @var array - список полей заявки для выборки по умолчанию
     */
    private          $defaultSelectFields = [
        'id',
        'guid',
        'num',
        'num_1c',
        'code',
        'mode',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'auto_price',
        'summ',
        'date_return',
        'credit_product_id',
        'point_id',
        'user_id',
        'client_id',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_birthday',
        'client_mobile_phone',
        'client_email',
        'client_passport_serial_number',
        'client_passport_number',
        'client_region_id',
        'client_home_phone',
        'client_employment_id',
        'client_workplace_experience',
        'client_workplace_period_id',
        'client_workplace_address',
        'client_workplace_phone',
        'client_workplace_name',
        'client_total_monthly_income',
        'client_total_monthly_outcome',
        'client_guarantor_name',
        'client_guarantor_relation_id',
        'client_guarantor_phone',
        'method_of_issuance_id',
        'card_number',
        'card_holder',
        'bank_bik',
        'checking_account',
        'pre_crediting',
        'accreditation_num',
        'roystat_id',
        'advert_channel',
        'referal_code',
        'request_source',
        'requests_origin_id',
        'comment_client',
        'comment_partner',
        'ready_to_exchange',
        'print_link',
        'created_at',
        'updated_at',
        'active',
        'sort',
        'is_pep',
        'place_of_stay',
        'lcrm_id',
        'card_token',
        'credit_limit_code',
    ];
    protected static $selectFieldsPseudo  = [
        '`r`.`id`',
        '`r`.`guid`',
        '`r`.`auto_brand_id`',
        '`auto_model_id`',
        '`b`.`name` AS `auto_brand_name`',
        '`m`.`name` AS `auto_model_name`',
        '`auto_year`',
        '`auto_price`',
        '`summ`',
        '`num`',
        '`num_1c`',
        '`code`',
        '`mode`',
        'DATE_FORMAT(`date_return`, "%Y-%m-%dT00:00:00.000Z") AS `date_return`',
        '`credit_product_id`',
        '`point_id`',
        '`user_id`',
        '`client_id`',
        '`client_first_name`',
        '`client_last_name`',
        '`client_patronymic`',
        'DATE_FORMAT(`client_birthday`, "%Y-%m-%dT00:00:00.000Z") AS `client_birthday`',
        '`client_mobile_phone`',
        '`client_email`',
        '`client_passport_serial_number`',
        '`client_passport_number`',
        '`client_region_id`',
        '`client_home_phone`',
        '`client_employment_id`',
        '`client_workplace_experience`',
        '`client_workplace_period_id`',
        '`client_workplace_address`',
        '`client_workplace_phone`',
        '`client_workplace_name`',
        '`client_total_monthly_income`',
        '`client_total_monthly_outcome`',
        '`client_guarantor_name`',
        '`client_guarantor_relation_id`',
        '`client_guarantor_phone`',
        '`method_of_issuance_id`',
        '`card_number`',
        '`bank_bik`',
        '`checking_account`',
        '`pre_crediting`',
        '`accreditation_num`',
        '`comment_client`',
        '`comment_partner`',
        'DATE_FORMAT(`r`.`created_at`, "%Y-%m-%dT00:00:00.000Z") AS `created_at`',
        '`is_pep`',
        '`place_of_stay`',
        '`print_link`',
        '`print_link_expired`',
        '`card_token`',
    ];
    /**
     * @var array - массив полей файлов, выводимые в заявке
     */
    protected $arFileFields = [
        'code',
        'url',
        'name',
        'description',
        'created_at',
    ];

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            [
                $this->reqFieldCreate,
                'required',
                'message' => 'Не все обязательные поля заполнены',
            ],
            [
                /*
                 * параметр указаный для фильтра trim после обработки будет равен пустой строке,  а не null
                 * в результате при обновлении если параметр не будет указан в параметрах обновления
                 * то после trim получит пустую строку и перезапишет ранее введенные данные в таблице
                 * метод prepareDataExecute исключает значения null для записи в БД, но не пустые строки
                */
                [
                    'client_first_name',
                    'client_last_name',
                    'client_patronymic',
                    'comment_client',
                    'client_email',
                    'client_workplace_name',
                    'client_workplace_address',
                    'client_guarantor_name',
                    'comment_partner',
                    'card_holder',
                ],
                'trim',
            ],
            [
                'summ',
                'number',
                'message' => 'Поле "Сумма займа" заполнено некорректно',
            ],
            [
                'date_return',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
                'message' => 'Некорректно указана дата возврата займа',
            ],
            ['auto_brand_id', AutoBrandValidator::class],
            ['auto_model_id', AutoModelValidator::class],
            [
                'auto_year',
                'number',
                'message'  => 'Поле "Год выпуска авто" заполнено некорректно',
                'min'      => '1919',
                'tooSmall' => 'Значение в поле "Год выпуска авто" должно быть не меньше 2000',
                'max'      => date('Y'),
                'tooBig'   => 'Значение в поле "Год выпуска авто" не должно превышать 2018',
            ],
            ['auto_price', 'number', 'message' => 'Поле "Приблизительная стоимость авто" заполнено некорректно'],
            [
                'method_of_issuance_id',
                'number',
                'message' => 'Способ выдачи займа указан в некорректном формате',
            ],
            ['credit_product_id', CreditProductValidator::class],
            [
                'client_first_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Имя" заполнено некорректно',
            ],
            [
                'client_first_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
            ],
            [
                'client_last_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Фамилия" заполнено некорректно',
            ],
            [
                'client_last_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
            ],
            [
                'client_patronymic',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Отчество" заполнено некорректно',
            ],
            [
                'client_patronymic',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
            ],
            [
                'client_passport_serial_number',
                'match',
                'pattern' => '/^\d{4}$/iu',
                'message' => 'Поле "Серия паспора" заполнено некорректно',
            ],
            [
                'client_passport_number',
                'match',
                'pattern' => '/^\d{6}$/iu',
                'message' => 'Поле "Номер паспора" заполнено некорректно',
            ],
            [
                'client_birthday',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
                'message' => 'Поле "Дата рождения" заполнено некорректно',
            ],
            [
                ['date_return', 'client_birthday'],
                'filter',
                'filter' => function ($v) {

                    if (isset($v)) {
                        //для формата  2018-01-22T00:00:00.000Z
                        if (strpos($v, '-') !== false) {
                            return substr($v, 0, 10);
                        }
                        //для формата  22.01.2018
                        if (strpos($v, '.') !== false) {
                            list($d, $m, $y) = explode('.', $v);

                            return $y.'-'.$m.'-'.$d;
                        }
                    }

                    return null;
                },
            ],
            ['client_region_id', RegionValidator::class],
            //			[
            //				[ 'client_mobile_phone', 'client_home_phone', 'client_workplace_phone', 'client_guarantor_phone' ],
            //				'match',
            //				'pattern' => '/^\d{10}$/iu',
            //				'message' => 'Номер телефона заполнен некорректно',
            //			],
            //  TODO: временно закомментил, потому что нужно договариваться о форматах, так же стоит учесть валидатор  modules/app/v1/validators/PhoneValidator.php
            [
                [
                    'comment_client',
                    'comment_partner',
                    'bank_bik',
                    'card_number',
                    'card_holder',
                    'card_token',
                    'checking_account',
                ],
                'string',
            ],
            ['client_email', 'email', 'message' => 'Поле "Электронная почта" заполнено некорректно'],
            ['client_total_monthly_income', 'number', 'message' => 'Сумарный месячный доход'],
            ['client_total_monthly_outcome', 'number', 'message' => 'Платежи по кредитам в месяц'],
            ['client_employment_id', EmploymentValidator::class],
            [
                'client_workplace_experience',
                'number',
                'message' => 'Поле "Стаж на этом месте работы" заполнено некорректно',
            ],
            ['client_workplace_period_id', WorkplacePeriodValidator::class],
            ['client_workplace_period_id', WorkplacePeriodValidator::class],
            [
                'client_workplace_address',
                'string',
                'message' => 'Поле "Место работы" заполнено некорректно',
            ],
            [
                'client_workplace_name',
                'string',
                'message' => 'Поле "Название работы" заполнено некорректно',
            ],
            [
                'client_guarantor_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "ФИО дополнительного контактного лица" заполнено некорректно',
            ],
            ['client_guarantor_relation_id', PeopleRelationValidator::class],
            [
                'mode',
                'string',
                'message' => 'Некорректно указан формат параметра "Выбор партнера"(mode)',
            ],
            [
                'place_of_stay',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Место прибывания" должно содержать минимум 2 символа',
            ],
            [
                'credit_limit_code',
                'string',
                'message' => 'Код кредитного лимита указан в некорректном формате, обратитесь к администратору',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                               = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE]        = $this->fieldScenarioCreate;
        $scenarios[self::SCENARIO_UPDATE]        = $this->fieldScenarioUpdate;
        $scenarios[self::SCENARIO_CARD_VERIFIER] = $this->fieldScenarioCardVerifier;

        return $scenarios;
    }

    /**
     * Метод получения всех Заявок
     *
     * @param bool $active - Параметр отвечающий за получение всех Заявок, в том числе неактивных
     *                     true - по умолчанию, выбираем только активные
     *                     false - выбираем только неактивные
     *                     null - выбираем все и актиные и неактивные
     *
     * @return array - Вернет массив данных или ложь
     * @throws \yii\db\Exception
     */
    public function getAll($active = true)
    {
        $active = is_null($active)
            ? ' '
            : (empty($active) ? ' AND `r`.`active` = 0 ' : ' AND `r`.`active` = 1 ');
        if (empty($this->getPointId())) {
            throw new \Exception('У пользователя не задана точка. Обратитесь к администратору.');
        }

        return Yii::$app->db->createCommand(
            "SELECT
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `num_1c`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `card_token`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `credit_limit_code`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`,
  `is_pep`,
  `place_of_stay`
FROM `requests` AS `r`
LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id` 
WHERE `point_id` = ".$this->getPointId().$active
        )
            ->queryAll();
    }

    /**
     * Метод получения идентификатора точки партнера
     * при необходимости переропределить в дочерней моделе
     *
     * @return bool - вернет идентификатор точки или ложь
     */
    protected function getPointId()
    {
        try {
            $userId = $this->getUserId();
            if (!isset($userId) || !is_numeric($userId)) {
                throw new \Exception('При создании заявки не был указан пользователь');
            }
            $user = (new Users())->getOne($userId);

            return !empty($user) ? $user['point_id'] : false;
        } catch (\Exception $e) {
            //todo[echmaster]: 18.04.2018 добавить логирование
            $e->getMessage();

            return false;
        }
    }

    /**
     * Метод получения количества открытых заявок клиента
     *
     * @return int - вернет число, количество заявок
     * @throws \yii\db\Exception
     */
    public function getClientRequestsCountInProcess()
    {
        $count      = 0;
        $idRequests = Yii::$app->db->createCommand(
            'SELECT [[id]] FROM '.
            $this->getTableName().
            ' WHERE [[client_id]] = "'.
            Yii::$app->user->id.
            '" AND [[active]] = "1"'
        )->queryAll();
        if (!empty($idRequests)) {
            $count            = count($idRequests);
            $requestsStatuses = Yii::$app->db->createCommand(
                'SELECT [[request_id]], [[status_id]]
                            FROM {{statuses_journal}}
                            WHERE [[request_id]] IN ("'.implode('","', array_column($idRequests, 'id')).'")'
            )->queryAll();
            if (!empty($requestsStatuses)) {
                $lastStatusRequests = array_column($requestsStatuses, 'status_id', 'request_id');
                $statuses           = [
                    '7d06fa22-3be6-4066-a5c7-6a0c6e7bd055', //На предварительном одобрении
                    'cbb42353-51e6-4a88-b2ef-575729da0343', //Предварительно одобрено
                    'e12788f0-7765-42bc-8ed3-828dab3fdf97', //Проверка документов
                    '1b61eb3a-c293-4fca-8ee6-f672c43302ef', //Документы проверены
                    '623bbcc6-ac4c-4e51-bdcf-f7c5a12b9abd', //На финальном одобрении
                ];
                $statusesFail       = Yii::$app->db->createCommand(
                    'SELECT [[id]]
                                FROM {{statuses}}
                                WHERE [[guid]] NOT IN ("'.implode('","', $statuses).'")'
                )->queryAll();
                $listStatusesFail   = array_column($statusesFail, 'id');
                foreach ($lastStatusRequests as $item) {
                    if (in_array($item, $listStatusesFail)) {
                        $count--;
                    }
                }
            }
        }

        return $count;
    }

    /**
     * Метод получения идентификатора пользоателя создавшего заявку
     * при необходимости переропределить в дочерней моделе
     *
     * @return int|string - вернет идентификатор пользователя
     */
    protected function getUserId()
    {
        return Yii::$app->user->id;
    }

    /**
     * Метод получения заявки по идентификатору
     *
     * @param int   $id     - идентификатор заявки
     * @param array $fields - поля для выборки
     *
     * @return array|false|mixed - вернет массив, если $fields пустое или в нем указано несколько значений
     *                           вернет одно значение если в $fields указано одно поля для выборки
     *                           вернет ложь если заявка отсутствует в базе
     * @throws \yii\db\Exception
     */
    public function getOne(int $id, array $fields = [])
    {
        if (empty($fields)) {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
  `r`.`id`,
  `r`.`guid`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `num_1c`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `card_token`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `credit_limit_code`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`,
  `is_pep`,
  `place_of_stay`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `r`.`id` = :id",
                    [':id' => $id]
                )
                ->queryOne();
        } else {
            $method = count($fields) == 1 ? 'queryScalar' : 'queryOne';
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[id]] =:id',
                    [':id' => $id]
                )->$method();
        }
    }

    /**
     * Метод получения заявки по коду
     *
     * @param string $code - код заявки
     *
     * @return array|bool|false - вернет результат выборки
     */
    public function getByCode(string $code)
    {
        try {
            if (strlen($code) != 32) {
                throw new \Exception('При попытке получения заявки по коду был некорректно указан код заявки');
            }

            return Yii::$app->db
                ->createCommand(
                    "SELECT
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `num_1c`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `card_token`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `credit_limit_code`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`,
  `is_pep`,
  `place_of_stay`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `code` = :code",
                    [':code' => $code]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения заявки по коду print_link
     *
     * @param string $code - код print_link
     *
     * @return array|bool|false - вернет результат выборки
     * @throws \yii\db\Exception
     */
    public function getByPrint_link($code)
    {
        if (strlen($code) != 8) {
            throw new \Exception('При попытке получения заявки был некорректно указан код print_link');
        }

        return Yii::$app->db
            ->createCommand(
                "SELECT
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `num_1c`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `card_token`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `print_link`,
  `print_link_expired`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`,
  `is_pep`,
  `place_of_stay`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `print_link` = :code",
                [':code' => $code]
            )
            ->queryOne();
    }

    /**
     * Метод получения кода заявки по идентификатору
     *
     * @param int $id - Идентификатор заявки
     *
     * @return array|false - Вернет код заявки или ложь
     * @throws \yii\db\Exception
     */
    public function getCodeByID(int $id)
    {
        $result = Yii::$app->db
            ->createCommand(
                "SELECT `code`
FROM requests
WHERE `id` = :id",
                [':id' => $id]
            )
            ->queryOne();

        return !empty($result) ? $result['code'] : $result;
    }

    /**
     * Метод получения заявок по идентификатору клиента
     *
     * @param null  $clientId - идентификатор клиента
     * @param array $fields   - массив полей, данные которых нужно получить
     * @param bool  $active   - флаг активности заявок:
     *                        true - выбрать активные заявки клиента
     *                        false - выбрать неактивные заявки клиента
     *                        null - выбрать все заявки клиента
     *
     * @return array|false - результирующий массив заявок или ложь в случае отсутствия завок у пользователя
     * @throws \yii\db\Exception
     */
    public function getByClientId($clientId = null, array $fields = [], $active = true)
    {
        $fields    = !empty($fields) ? $fields : $this->getFieldsNames();
        $fields    = '[['.implode("]],[[", $fields).']]';
        $clientId  = $clientId ?? Yii::$app->user->id;
        $condition = is_null($active) ? '' : 'AND [[active]]='.(empty($active) ? 0 : 1);

        return Yii::$app->db
            ->createCommand(
                'SELECT '.$fields.
                ' FROM '.$this->getTableName().
                ' WHERE [[client_id]] =:clientId '.$condition.
                ' ORDER BY [[id]] DESC',
                [':clientId' => $clientId]
            )->queryAll();
    }

    /**
     * Метод создания заявки
     *
     * @param array $params - Параметры для создания заявки,
     *                      можно использовать совместно с  $model->load
     *
     * @return bool|string|$this - Вернет код(хэш) в случае успеха иначе ложь
     *                                        текущий объект, если не пройдена валидация,
     *                                        объект исключения, при выбросе исключения
     * @throws \Exception
     */
    public function create(array $params = [])
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $arPrepare = $this->prepareDataForExecute($params);
            if (empty($arPrepare)) {
                throw new \RuntimeException('Не переданы параметры для создания заявки');
            }
            $arPrepare['code']       = md5(str_shuffle('reuest'.time()).microtime(true));
            $arPrepare['active']     = isset($arPrepare['active']) ? $arPrepare['active'] : '1';
            $arPrepare['sort']       = isset($arPrepare['sort']) ? $arPrepare['sort'] : '500';
            $arPrepare['created_at'] = $arPrepare['updated_at'] = new Expression('NOW()');
            $pointId                 = $this->getPointId();
            if (empty($pointId)) {
                throw new \RuntimeException(
                    'При создании заявки произошла ошибка, не удалось привязать точку партнера к заявке, обратитесь к администратору'
                );
            }
            $arPrepare['point_id']  = $pointId;
            $arPrepare['user_id']   = $this->getUserId();
            $arPrepare['client_id'] = Yii::$app->user->id;
            $generateNum            = $this->generateNum();
            if (is_string($generateNum)) {
                throw new \RuntimeException($generateNum);
            }
            $arPrepare['num'] = $generateNum;
            $requestsOrigin   = (new RequestsOrigin())->getByIdentifier($this->requestOrigin);
            if (is_string($requestsOrigin)) {
                throw new \RuntimeException(
                    'При создании заявки произошла ошибка, не удалось указать место создания заявки, обратитесь к администратору'
                );
            }
            if (!empty($requestsOrigin)) {
                $arPrepare['requests_origin_id'] = $requestsOrigin['id'];
            }
            !empty($this->credit_limit_code) ?: $arPrepare['credit_limit_code'] = null;
            $insert = Yii::$app->db
                ->createCommand()
                ->insert('requests', $arPrepare)->execute();

            return !empty($insert) ? $arPrepare['code'] : false;
        } catch (Exception $e) {
            throw new \RuntimeException('Ошибка создания заявки в базе данных, обратитесь к администратору');
        }
    }

    /**
     * Метод сохранения заявки (добавляет или обновляет данные)
     *
     * @param array $params - параметры для сохранения заявки
     *
     * @return \Exception|int - вернет 1 - при добавлении, 2 - при обновлении
     *                    или вернет объект при исключении
     */
    public function save(array $params = [])
    {
        try {
            if (!empty($params)) {
                $arFlip      = array_flip($this->getFieldsNames());
                $arIntersect = array_intersect_key($params, $arFlip);
                if (count($params) != count($arIntersect)) {
                    throw new \Exception('При сохранении заявки были некорректно указаны поля в параметрах');
                }
            }
            $insert               = $update = empty($params) ? $this->getAttributes() : $params;
            $insert['created_at'] = $update['updated_at'] = new Expression('NOW()');
            $insert['code']       = md5(str_shuffle('requestsave'.time()).microtime(true));
            $num                  = $this->generateNum();
            if (is_string($num)) {
                throw new \Exception(
                    'При поптыке сохранения заявки не удалось присвоить номер заявки, обратитесь к администратору'
                );
            }
            $insert['num'] = $num;

            return Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)->execute();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод генерации псевдо-идентификатора заявки для отображения пользователю
     *
     * @return false|int|null|string - вернет числовой идентификатор или строку с текстом ошибки
     */
    public function generateNum()
    {
        try {
            $maxNumQuery = Yii::$app->db
                ->createCommand("SELECT MAX([[num]]) FROM {{requests}}")
                ->queryScalar();

            return $maxNumQuery + rand(1, 12);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод обновления заявки по идентификатору
     *
     * @param       $id      - Идентификатор заявки
     * @param array $params  - Параметры для обновления заявки
     * @param bool  $prepare - флаг для подготовки данных
     *
     * @return bool|int - Вернет кол-во обновленных строк или выбросит исключение
     */
    public function updateOne($id, array $params = [], $prepare = true)
    {
        try {
            $arPrepare = !empty($prepare) ? $this->prepareDataForExecute($params) : $params;
            if (empty($arPrepare)) {
                throw new \Exception('Не переданы параметры для обновления заявки');
            }
            $arPrepare['updated_at'] = new Expression('NOW()');
            !empty($this->credit_limit_code) ?: $arPrepare['credit_limit_code'] = null;
            if (!is_string($this->credit_limit_code)) {
                unset($arPrepare['credit_limit_code']);
            }

            return Yii::$app->db
                ->createCommand()
                ->update('requests', $arPrepare, ['id' => $id])->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод обновления заявки по коду(хэшу)
     *
     * @param string $code    - Код(хэш) заявки
     * @param array  $params  - Параметры для обновления заявки
     * @param bool   $prepare - флаг для подготовки данных
     *
     * @return bool|int|\Exception - Вернет кол-во обновленных строк или выбросит исключение
     */
    public function updateByCode(string $code, array $params = [], $prepare = true)
    {
        try {
            if (strlen($code) != 32) {
                throw new \Exception('При попытке обновления заявки был некорректно указан код заявки');
            }
            $arPrepare = !empty($prepare) ? $this->prepareDataForExecute($params) : $params;
            if (empty($arPrepare)) {
                throw new \Exception('Не переданы параметры для обновления заявки');
            }
            $arPrepare['updated_at'] = new Expression('NOW()');
            !empty($this->credit_limit_code) ?: $arPrepare['credit_limit_code'] = null;
            if (!is_string($this->credit_limit_code)) {
                unset($arPrepare['credit_limit_code']);
            }

            return Yii::$app->db
                ->createCommand()
                ->update('requests', $arPrepare, ['code' => $code])->execute();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод обновления заявки по ГУИД
     *
     * @param string $guid    - ГУИД заявки
     * @param array  $params  - Параметры для обновления заявки
     * @param bool   $prepare - флаг для подготовки данных
     *
     * @return bool|int - Вернет кол-во обновленных строк или ложь
     */
    public function updateByGUID(string $guid, array $params = [], $prepare = true)
    {
        try {
            if (strlen($guid) != 36) {
                throw new \Exception('При попытке обновления заявки был некорректно указан ГУИД заявки');
            }
            $arPrepare = !empty($prepare) ? $this->prepareDataForExecute($params) : $params;
            if (empty($arPrepare)) {
                throw new \Exception('Не переданы параметры для обновления заявки');
            }
            $arPrepare['updated_at'] = new Expression('NOW()');
            !empty($this->credit_limit_code) ?: $arPrepare['credit_limit_code'] = null;
            if (!is_string($this->credit_limit_code)) {
                unset($arPrepare['credit_limit_code']);
            }

            return Yii::$app->db
                ->createCommand()
                ->update('requests', $arPrepare, ['guid' => $guid])->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления заявки по идентификатору
     *
     * @param int $id - идентификатор заявки
     *
     * @return int|string - вернет кол-во удаленных строк в таблице БД
     */
    public function deleteOne(int $id)
    {
        try {
            return Yii::$app->db->createCommand()->delete('requests', 'id = :id', [':id' => $id])->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления заявки по коду
     *
     * @param string $code - Код заявки
     *
     * @return bool|int - вернет кол-во удаленных строк или ложь,
     *                  если некорректный код заявки
     */
    public function deleteByCode(string $code)
    {
        try {
            if (strlen($code) != 32) {
                throw new \Exception('При попытке удаления заявки был некорректно указан код заявки');
            }

            return Yii::$app->db->createCommand()->delete('requests', 'code = :code', [':code' => $code])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Очистить поля card_number и card_token
     *
     * @param string $code - Код заявки
     *
     * @return bool|int - вернет кол-во удаленных строк или ложь,
     *                  если некорректный код заявки
     */
    public function deleteCVByCode(string $code)
    {
        try {
            if (strlen($code) != 32) {
                throw new \Exception('При попытке удаления заявки был некорректно указан код заявки');
            }

            return Yii::$app->db->createCommand()->update(
                'requests',
                ['card_number' => '', 'card_token' => ''],
                ['code' => $code]
            )
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления заявки по ГУИД
     *
     * @param string $guid - ГУИД заявки
     *
     * @return bool|int - вернет кол-во удаленных строк или ложь,
     *                  если некорректный код заявки
     */
    public function deleteByGUID(string $guid)
    {
        try {
            if (strlen($guid) != 36) {
                throw new \Exception('При попытке удаления заявки был некорректно указан ГУИД заявки');
            }

            return Yii::$app->db->createCommand()->delete('requests', 'guid = :guid', [':guid' => $guid])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения файлов заявки
     *
     * @param       $id             - идентификатор заявки
     * @param array $arFields       - массив полей файла, которые необходимо вывести, если указать пустым,то выведет все
     *                              "id": "1",
     *                              "code": "b49c35bab3c681dfcc8d2f3b254dd3a4",
     *                              "name": "joxi_screenshot_1521802657349.png",
     *                              "path": "/path/1-joxi_screenshot_1521802657349.png",
     *                              "height": "949",
     *                              "width": "1920",
     *                              "type": "image/png",
     *                              "size": "2092708",
     *                              "description": "",
     *                              "sim_link": "fd3a75d95785d147d99c36286cfb9b5e",
     *                              "created_at": "2018-04-05T00:00:00.000Z",
     *                              "updated_at": "2018-04-05T00:00:00.000Z",
     *                              "active": "1",
     *                              "sort": "500",
     *                              "url":  "" - доп. параметр, в таблице файлов нет, если файл картинка, то выведет
     *                              ссылку на превью, если документ, то специально сформированную ссылку для документов
     * @param array $arFileBind     - файлы, которые выводим в заявке, по умолчанию выводит все
     *                              допустимые значения ['foto_sts','foto_pts','foto_auto','foto_passport', 'foto_card'
     *                              'foto_client','foto_extra','doc_pack_1','doc_pack_2','doc_pack_3', 'foto_extra',
     *                              'foto_notification']
     *
     * @return array - веренет массив данных по файлам либо строку с исключением
     *                      "foto_pts": [
     *                              {
     *                              "code": "b49c35bab3c681dfcc8d2f3b254dd3a4",
     *                              ...
     *                              },
     *                      ]
     * @throws \Exception
     */
    public function getFiles($id, array $arFields = [], array $arFileBind = [])
    {
        $files = (new File())->getByRequestId($id);
        if (is_string($files)) {
            throw new \Exception($files);
        }
        $result     = [];
        $flipFields = !empty($arFields) ? array_flip($arFields) : $arFields;
        foreach ($files as $file) {
            $fileBind = $file['file_bind'];
            unset($file['file_bind']);
            $pathFile    = rtrim(Yii::getAlias('@webroot'), '/').'/'.ltrim($file['path'], '/');
            $file['url'] = !file_exists($pathFile)
                ? false
                : (!empty(getimagesize($pathFile))
                    ? Yii::$app->file->getPreview($file['code'])
                    //: '/' . trim( $this->pathDoc, '/' ) . '/' . $file['code'] );
                    : '/doc/'.$file['code']);
            $file        = !empty($flipFields) ? array_intersect_key($file, $flipFields) : $file;
            if (!empty($arFileBind) && !in_array($fileBind, $arFileBind)) {
                continue;
            }
            $result[$fileBind][] = $file;
        }

        return $result;
    }

    /**
     * Метод получения заявки по номеру (псевдо)
     *
     * @param int $num номер заявки
     *
     * @return array|bool|false - вернет результат выборки или строку с иключением
     */
    public function getByNum(int $num)
    {
        try {
            return $this->getOneByFilter(['num' => $num]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения заявки по номеру (псевдо)
     *
     * @param int $num1c - номер заявки 1C
     *
     * @return array|bool|false - вернет результат выборки или строку с иключением
     */
    public function getByNum1c(int $num1c)
    {
        try {
            return $this->getOneByFilter(['num_1c' => $num1c]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения списка полей заявки
     * специально выполнен публичным для получения доступа к полям
     *
     * @return array - вернет массив список названия полей БД
     */
    public function getListFields(): array
    {
        return $this->getFieldsNames();
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    public function getFieldsNames(): array
    {
        return $this->defaultSelectFields;
    }

    /**
     * Установка полей заявки для последующей выборки
     *
     * @param array $fields - поля
     */
    public function setDefaultSelectFields(array $fields = [])
    {
        $this->defaultSelectFields = $fields;
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return '{{requests}}';
    }

    /**
     * @return array
     */
    public static function getSelectFieldsPseudo(): array
    {
        return self::$selectFieldsPseudo;
    }
}