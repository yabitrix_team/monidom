<?php
namespace app\modules\app\v1\models;

use app\modules\mp\v1\models\MessageType as MessageTypeMP;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\notification\models\MessageStatus;
use app\modules\notification\models\MessageType;
use app\modules\app\v1\classes\logs\Log;
use app\modules\mp\v1\models\Users;
use yii\db\Expression;
use yii\db\Exception;
use yii\base\Model;
use Yii;

/**
 * Class FCM - модель для взаимодейтсвия с таблицей fcm-токенов (fcm)
 * todo[echmaster]: 06.02.2019 - перезаложить архитектуру, разделить оптравку пушей с работой базы
 *
 * @package app\modules\app\v1\models
 */
class FCM extends Model
{
    /**
     * Подключение трейтов взаимодействия с базой
     */
    use WritableTrait;
    use ReadableTrait;
    /**
     * Константы сценариев
     */
    public const SCENARIO_SAVE              = 'save';//сохранения записи по FCM токену
    public const SCENARIO_PUSH_STATUS       = 'pushStatus';//отправка пушей по статуса
    public const SCENARIO_PUSH_NOTIFICATION = 'pushNotification';//отправка пушей по уведомлениям
    /**
     * Константы типов ОС
     */
    public const OS_TYPE_ANDROID = 'android';//тип ОС android
    public const OS_TYPE_IOS     = 'ios';//тип ОС ios
    /**
     * Константы типы пушей
     */
    public const PUSH_TYPE_STATUS       = 'status';//тип пушей для статусов
    public const PUSH_TYPE_NOTIFICATION = 'notification';//тип пушей для уведомлений
    /**
     * Поля модели
     */
    public $user_id;//идентификатор пользователя
    public $device_id;//идентификатор девайса
    public $fcm_token;//токен для FCM
    public $os_type;//тип операционной системы
    public $title;//заголовок пуш-уведомления
    public $message;//сообщение пуш-уведомления
    public $request_code;//символьный код заявки
    public $status_mp;//статус заявки для МП
    public $nt_code;//код уведомления
    public $nt_type;//тип уведомления
    public $nt_title;//заголовок уведомления
    public $nt_text;//сообщение уведомления
    public $nt_time;//время
    public $nt_status;//статус уведомления
    /**
     * @var array - массив полей валидации по сценариям
     */
    protected static $fieldsScenarios = [
        self::SCENARIO_SAVE              => [
            'device_id',
            'fcm_token',
            'os_type',
        ],
        self::SCENARIO_PUSH_STATUS       => [
            'user_id',
            'title',
            'message',
            'request_code',
            'status_mp',
        ],
        self::SCENARIO_PUSH_NOTIFICATION => [
            'user_id',
            'title',
            'message',
            'nt_code',
            'nt_type',
            'nt_title',
            'nt_text',
            'nt_time',
            'nt_status',
            'request_code',
        ],
    ];

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return '{{fcm}}';
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'user_id',
            'fcm-token',
            'device_id',
            'os_type',
            'authorized',
            'updated_at',
            'created_at',
        ];
    }

    /**
     * @return array - правила валидации передаваемых данных
     */
    public function rules(): array
    {
        return [
            [
                [
                    'device_id',
                    'fcm_token',
                    'os_type',
                    'user_id',
                    'title',
                    'message',
                    'nt_code',
                    'nt_type',
                    'nt_text',
                    'nt_time',
                    'nt_status',
                ],
                'required',
            ],
            [
                'request_code',
                'required',
                'on' => self::SCENARIO_PUSH_NOTIFICATION,
            ],
            [
                'os_type',
                'in',
                'range' => [self::OS_TYPE_ANDROID, self::OS_TYPE_IOS],
            ],
            [
                ['user_id', 'nt_time'],
                'number',
            ],
            [
                ['request_code', 'nt_code'],
                'string',
                'length' => 32,
            ],
            [
                ['status_mp', 'nt_text'],
                'string',
            ],
            [
                'nt_type',
                'in',
                'range' => [MessageTypeMP::$assocType[MessageType::VERIFIER]],
            ],
            [
                'nt_status',
                'in',
                'range' => [MessageStatus::NEW, MessageStatus::READ, MessageStatus::ANSWERED],
            ],
        ];
    }

    /**
     * Метод именования атрибутов
     *
     * @return array - вернет массив именованных атрибутов
     */
    public function attributeLabels(): array
    {
        return [
            'device_id'    => 'Идентификатор девайса',
            'fcm_token'    => 'FCM токен',
            'os_type'      => 'Тип операционной системы',
            'user_id'      => 'Идентификатор пользователя',
            'title'        => 'Заголовок для пуш-уведомления',
            'message'      => 'Текст для пуш-уведомления',
            'request_code' => 'Код заявки',
            'status_mp'    => 'Статус для мобильного приложения',
            'nt_code'      => 'Код уведомления',
            'nt_type'      => 'Тип уведомления',
            'nt_title'     => 'Заголовк уведомления',
            'nt_text'      => 'Текст уведомления',
            'nt_time'      => 'Время создания уведомления',
            'nt_status'    => 'Статус уведомления',
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios(): array
    {
        $scenarios                                   = parent::scenarios();
        $scenarios[self::SCENARIO_SAVE]              = self::$fieldsScenarios[self::SCENARIO_SAVE];
        $scenarios[self::SCENARIO_PUSH_STATUS]       = self::$fieldsScenarios[self::SCENARIO_PUSH_STATUS];
        $scenarios[self::SCENARIO_PUSH_NOTIFICATION] = self::$fieldsScenarios[self::SCENARIO_PUSH_NOTIFICATION];

        return $scenarios;
    }

    /**
     * Метод сохранения(вставка или обновление) данных FCM - токена
     *
     * @return $this|\Exception|int - возращает:
     *                    0 - не обновил не вставил, данные не изменились
     *                    1 - произвел вставку
     *                    2 - произвел обновление
     *                    объект содержазий выбрашенное исключение
     *                    текущий объект с ошибками валидации
     */
    public function save()
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $insert               = $update = $this->getAttributes(self::$fieldsScenarios[self::SCENARIO_SAVE]);
            $update['updated_at'] = new Expression('NOW()');
            $userId               = Users::findIdentityByAccessToken(AuthToken::getTokenInHeader())['id'];
            $insert['authorized'] = $update['authorized'] = (int) !empty($userId);
            $update['updated_at'] = new Expression('NOW()');
            empty($userId) ?: $insert['user_id'] = $update['user_id'] = $userId;

            return Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)
                ->execute();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод для отправки пуш-уведомления
     * при отдаче мобильному приложению преобразует
     * request_code в code
     * status_mp в status
     * $this|bool|string|\Exception - вернет истину если отправка прошла успешна,
     *                                объект содержазий выбрашенное исключение
     *                                текущий объект с ошибками валидации
     */
    public function send()
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $result = $this->getTokenByUserId($this->user_id);
            if (empty($result)) {
                throw new \Exception(
                    'При попытке отправки пуш-уведомления произошла ошибка получения fcm-токенов по идентификатору пользователя'
                );
            }
            $response = $this->{$this->scenario}($result);
            if (!empty($response)) {
                $arError = [
                    self::OS_TYPE_ANDROID => [
                        'msg'  => 'При отправке пуш-уведомлений на деайсы с OC android произошла неизвестная ошибка',
                        'code' => $response[self::OS_TYPE_ANDROID] ?? 0,
                    ],
                    self::OS_TYPE_IOS     => [
                        'msg'  => 'При отпраке пуш-уведомлений на деайсы с OC ios произошла неизвестная ошибка',
                        'code' => $response[self::OS_TYPE_IOS] ?? 0,
                    ],
                    'default'             => [
                        'msg'  => 'При отпраке пуш-уведомлений произошла неизвестная ошибка',
                        'code' => 1,
                    ],
                ];
                $key     = isset($response[self::OS_TYPE_ANDROID])
                    ? self::OS_TYPE_ANDROID
                    : ($response[self::OS_TYPE_IOS] ? self::OS_TYPE_ANDROID : 'default');
                throw new \Exception($arError[$key]['msg'].' - '.print_r($arError[$key]['code'], 1));
            }
            self::logSuccess($this->getAttributes(self::$fieldsScenarios[$this->scenario]));

            return true;
        } catch (\Exception $e) {
            self::logError(
                $e->getMessage(),
                $e->getCode(),
                $result,
                $this->getAttributes(self::$fieldsScenarios[$this->scenario])
            );

            return $e;
        }
    }

    /**
     * Метод отправки пушей для статусов
     *
     * @param $result - массив FCM-токенов разделенных по типам ОС
     *                [
     *                ios=>[fNz6-S2JCz0:APA91bGwiO...]
     *                android=>[fNz6-S2JCz0:APA91bGwiO...]
     *                ]
     *
     * @return array - вернет массив ответа
     */
    protected function pushStatus($result): array
    {
        $response = [];
        //подготовка пушей для андроида
        if (isset($result[self::OS_TYPE_ANDROID])) {
            $messageAndroid = Yii::$app->fcm->createMessage($result[self::OS_TYPE_ANDROID]);
            $dataAndroid    = [
                'title' => $this->title,
                'body'  => $this->message,
                'type'  => self::PUSH_TYPE_STATUS,
            ];
            empty($this->request_code) ?: $dataAndroid['code'] = $this->request_code;
            empty($this->status_mp) ?: $dataAndroid['status'] = $this->status_mp;
            $messageAndroid->setData($dataAndroid);
            $send = Yii::$app->fcm->send($messageAndroid);
            (int) $send->getStatusCode() === 200 ?: $response[self::OS_TYPE_ANDROID] = $send->getStatusCode();
        }
        //подготовка пушей для IOS
        if (isset($result[self::OS_TYPE_IOS])) {
            $note = Yii::$app->fcm->createNotification($this->title, $this->message);
            $note->setIcon('notification_icon_resource_name')
                ->setColor('#ffcc00')
                ->setBadge(1)
                ->setSound('default');
            $messageIos = Yii::$app->fcm->createMessage($result[self::OS_TYPE_IOS]);
            $messageIos->setNotification($note);
            $dataIos = ['type' => self::PUSH_TYPE_STATUS];
            empty($this->request_code) ?: $dataIos['code'] = $this->request_code;
            empty($this->status_mp) ?: $dataIos['status'] = $this->status_mp;
            $messageIos->setData($dataIos);
            $send = Yii::$app->fcm->send($messageIos);
            (int) $send->getStatusCode() === 200 ?: $response[self::OS_TYPE_IOS] = $send->getStatusCode();
        }

        return $response;
    }

    /**
     * Метод отправки пушей для уведомлений
     *
     * @param $result - массив FCM-токенов разделенных по типам ОС
     *                [
     *                ios=>[fNz6-S2JCz0:APA91bGwiO...]
     *                android=>[fNz6-S2JCz0:APA91bGwiO...]
     *                ]
     *
     * @return array - вернет массив ответа
     */
    protected function pushNotification($result): array
    {
        $response                = [];
        $data                    = $this->getAttributes(self::$fieldsScenarios[self::SCENARIO_PUSH_NOTIFICATION]);
        $data['type']            = self::PUSH_TYPE_NOTIFICATION;
        $data['body']            = $data['message'];
        $data['nt_request_code'] = $data['request_code'];
        unset($data['request_code'], $data['message']);
        //подготовка пушей для андроида
        if (isset($result[self::OS_TYPE_ANDROID])) {
            $messageAndroid = Yii::$app->fcm->createMessage($result[self::OS_TYPE_ANDROID]);
            $messageAndroid->setData($data);
            $send = Yii::$app->fcm->send($messageAndroid);
            (int) $send->getStatusCode() === 200 ?: $response[self::OS_TYPE_ANDROID] = $send->getStatusCode();
        }
        //подготовка пушей для IOS
        if (isset($result[self::OS_TYPE_IOS])) {
            $note = Yii::$app->fcm->createNotification($this->title, $this->message);
            $note->setIcon('notification_icon_resource_name')
                ->setColor('#ffcc00')
                ->setBadge(1)
                ->setSound('default');
            $messageIos = Yii::$app->fcm->createMessage($result[self::OS_TYPE_IOS]);
            unset($data['title'], $data['message']);
            $messageIos->setData($data);
            $messageIos->setNotification($note);
            $send = Yii::$app->fcm->send($messageIos);
            (int) $send->getStatusCode() === 200 ?: $response[self::OS_TYPE_IOS] = $send->getStatusCode();
        }

        return $response;
    }

    /**
     * Метод получения FCM-токена по идентификатору пользователя с разделением на типы OS
     *
     * @param $userId - идентификатор пользователя
     *
     * @return array - результирующий массив вида
     *               [
     *                  android => [
     *                      fNz6-S2JCz0:APA91bGwiO.....
     *                  ]
     *                  ios => [
     *                      d60o82nolN0:APA91bFrzUJ.....
     *                  ]
     *               ]
     * @throws \Exception
     */
    public function getTokenByUserId($userId)
    {
        try {
            $result = [];
            $data   = Yii::$app->db->createCommand(
                'SELECT
[[fcm_token]], [[os_type]]
FROM '.$this->getTableName().'
WHERE [[user_id]] = :userId',
                [':userId' => $userId]
            )
                ->queryAll();
            if (!empty($data)) {
                foreach ($data as $item) {
                    $result[$item['os_type']][] = $item['fcm_token'];
                }
            }

            return $result;
        } catch (Exception $e) {
            throw new \Exception(
                'При попытке получения FCM токенов произошла ошибка в работе с базой данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод логирования ошибок по данному функицоналу
     * лог лежит по адресу: runtime/logs/push_error.log
     * todo[echmaster]: 20.04.2018 : В дальнейшем метод будет удален, в пользу централизованного логирования
     *
     * @param $msg   - сообщение ошибки
     * @param $code  - http код
     * @param $token - fcm токен
     * @param $data  - передаваемые данные
     */
    protected static function logError($msg, $code, $token, $data): void
    {
        $data  = print_r($data, true);
        $token = print_r($token, true);
        $log   = <<<TXT
*** Сбой при отправке пуш-уведомления *** 
Текст ошибки: $msg
Http-код: $code
FCM-токен: $token Данные передачи: $data
TXT;
        Log::error(['text' => $log], 'mobile.push');
    }

    /**
     * Метод логирования успешных операций по данному функицоналу
     * лог лежит по адресу: runtime/logs/push_success.log
     * todo[echmaster]: 20.04.2018 : В дальнейшем метод будет удален, в пользу централизованного логирования
     *
     * @param $data - передаваемые данные
     */
    protected static function logSuccess($data): void
    {
        $res = print_r($data, true);
        $log = <<<TXT
*** Отправка пуш-уведомления *** 
Данные передачи: $res
TXT;
        Log::info(['text' => $log], 'mobile.push');
    }
}