<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class CCmpLoanAgreementFields extends Model {
	/**
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function get() {

		return Yii::$app->db->createCommand( 'SELECT * FROM c_cmp_loan_agreement_fields' )
		                    ->queryAll();
	}
}