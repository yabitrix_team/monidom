<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class EventsCrm - модель для работы со справочником Событий CRM
 *
 * @package app\modules\app\v1\models
 */
class EventsCrm extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * @var integer - код события
     */
    public $event_crm_code;
    /**
     * @var - логин пользователя
     */
    public $username;
    /**
     * @var string - группа пользователей
     */
    protected $userGroup = "client";
    /**
     * @var bool - разрешена ли отправка события
     */
    protected $canSend = true;

    /**
     * @return array - правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            ['event_crm_code', 'required', 'message' => 'Не указан контент документа, обратитесь к администратору'],
            [
                'event_crm_code',
                'validateEventCode',
            ],
            [
                'username',
                'match',
                'pattern' => '/^9[0-9]{9}$/iu',
                'message' => 'Поле "Phone Number" заполнено некорректно',
            ],
        ];
    }

    /**
     * Метод валидация кода события
     *
     * @param $attribute
     * @param $params
     */
    public function validateEventCode($attribute, $params)
    {
        $res = $this->getCodes();
        if (is_string($res)) {
            $this->addError($attribute, 'Ошибка получения кодов события, обратитесь к администратору');
        }
        if (!empty($res) && !in_array($this->event_crm_code, $res)) {
            $this->addError($attribute, 'Код события указан не верно');
        }
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{events_crm}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "code",
            "guid",
            "name",
            "created_at",
            "updated_at",
            "active",
            "sort",
        ];
    }

    public function getCodes($active = 1)
    {
        try {
            return Yii::$app->db
                ->createCommand(
                    'SELECT [[code]]
 FROM '.self::getTableName().'
 WHERE [[active]] = :active ORDER BY [[sort]] ASC',
                    ['active' => $active]
                )
                ->queryColumn();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}