<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

class Groups extends Model {
	use WritableTrait;
	use ReadableTrait;

	public static function getGroupByUserId( $user_id ) {

		return Yii::$app->db->createCommand( "SELECT
  `g`.`id`,
  `identifier`,
  `name`, 
  `prior_cabinet_id`,
  `active`,
  `sort`
FROM `groups` AS `g`
LEFT JOIN `user_group` AS `ug` ON `g`.`id` = `ug`.`group_id`
WHERE `ug`.`user_id` = :user_id", [ ':user_id' => $user_id ] )
		                    ->queryAll();
	}

	public static function getGroupByIdentifier( $identifier ) {

		return Yii::$app->db->createCommand( "SELECT
  `g`.`id`,
  `identifier`,
  `name`, 
  `prior_cabinet_id`,
  `active`,
  `sort`
FROM `groups` AS `g`
WHERE `g`.`identifier` = :identifier", [ ':identifier' => $identifier ] )
		                    ->queryOne();
	}

	/*
	 * Выбор группы пользователя
	 */

	/**
	 * Метод получения всех элементов
	 *
	 * @param bool $active - Параметр отвечающий за получение всех элементов, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' WHERE `active` = 0 ' : ' WHERE `active` = 1 ' );

		return Yii::$app->db->createCommand( 'SELECT
  `id`,
  `identifier`,
  `name`, 
  `prior_cabinet_id`,
  `active`,
  `sort`
FROM `groups` ' . $active )
		                    ->queryAll();
	}

	/*
	 * Получение группы по GUID
	 */

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{groups}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"identifier",
			"name",
			"prior_cabinet_id",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}