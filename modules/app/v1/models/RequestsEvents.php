<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class RequestsEvents - модель для работы с таблице связей событий с заявкой
 *
 * @package app\modules\app\v1\models
 */
class RequestsEvents extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * @var - идентификатор заявки
     */
    public $request_id;
    /**
     * @var - идентификатор события
     */
    public $event_id;
    /**
     * @var - отправка в ТБД
     */
    public $is_sended;
    /**
     * @var - заблокирован  для отправки
     */
    public $is_ignored;

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{requests_events}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "request_id",
            "event_id",
            "is_sended",
            "is_ignored",
            "ignore_reason",
            "created_at",
            "updated_at",
        ];
    }

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            [
                ['request_id', 'event_id'],
                'required',
                'message' => 'Поле обязательно для заполнения',
            ],
            [
                'request_id',
                'number',
                'message' => 'Идентификатор заявки указан в некорректном формате',
            ],
            [
                'event_id',
                'number',
                'message' => 'Идентификатор события по заявке указан в некорректном формате',
            ],
            [
                'is_sended',
                'default',
                'value' => 0,
            ],
            [
                'is_ignored',
                'default',
                'value' => 0,
            ],
        ];
    }

    /**
     * Метод созадния записи связи заявки с событием
     *
     * @return bool|int|string - вернет идентификатор записи
     */
    public function create()
    {
        try {
            $atr = $this->getAttributes();

            return $this->add($atr);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод множественного добавления записей связей заявки с собитиями
     *
     * @param       $requestId - идентификатор заявки
     * @param array $eventId   - массив идентификаторов событий
     *
     * @return int|string - вернет кол-во вставленных строк
     */
    public function batchInsert($requestId, array $eventId)
    {
        try {
            $arInsert = [];
            foreach ($eventId as $v) {
                $arInsert[] = [$requestId, $v];
            }

            return Yii::$app->db
                ->createCommand()
                ->batchInsert($this->getTableName(), ['request_id', 'event_id'], $arInsert)
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных по связям событий заявки
     *
     * @param       $requestId - идентификатор заявки
     * @param array $fields    - поля для выборки, при пустом значении выберет все поля
     *
     * @return array - массив запрошенных данных
     * @throws \yii\db\Exception - исключения БД
     */
    public function getByRequestId($requestId, array $fields = [])
    {
        $fields = !empty($fields) ? $fields : $this->getFieldsNames();
        $fields = '[['.implode("]],[[", $fields).']]';

        return Yii::$app->db
            ->createCommand(
                "SELECT ".$fields." FROM ".$this->getTableName()." WHERE [[request_id]] = :requestId",
                [':requestId' => $requestId]
            )
            ->queryAll();
    }

    /**
     * Метод проверки существования события по заявке
     *
     * @param $idOrCodeEvent   - идентификатор или код события
     * @param $idOrCodeRequest - идентификатор или код заявки
     *
     * @return bool - вернет истину если событие было добавлено по заявке иначе ложь
     * @throws \yii\db\Exception
     */
    public function issetEventForRequest($idOrCodeEvent, $idOrCodeRequest)
    {
        $event   = strlen($idOrCodeEvent) <= 3 ? ':event' : '(SELECT [[id]] FROM {{events}} WHERE [[code]] = :event)';
        $request = strlen($idOrCodeRequest) != 32
            ? ':request'
            : '(SELECT [[id]] FROM {{requests}} WHERE [[code]] = :request)';
        $res     = Yii::$app->db
            ->createCommand(
                "SELECT [[id]] FROM ".$this->getTableName().
                " WHERE [[event_id]] = ".$event.
                " AND [[request_id]] = ".$request,
                [':event' => $idOrCodeEvent, ':request' => $idOrCodeRequest]
            )
            ->queryOne();

        return !empty($res);
    }
}
