<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;

/**
 * Class StatusesMP - модель для работы с таблицей Статусы для МП
 *
 * @package app\modules\app\v1\models
 */
class StatusesMP extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;
	/**
	 * @var string - символьный идентификатор статуса "Создана заявка"
	 */
	protected static $statusCreated = 'CREATED';

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{statuses_mp}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"name",
			"identifier",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	/**
	 * Метод получения символьного идентификатора статуса "Создана заявка"
	 *
	 * @return string
	 */
	public static function getStatusCreated(): string {

		return self::$statusCreated;
	}

	/**
	 * Метод получения статуса заявки по символному идентификатору
	 *
	 * @param $identifier - символьный идентификатор, заведены следующие:
	 *                    CREATED, PREAPPROVED_SENT, PREAPPROVED, APPROVED_SENT, APPROVED, DENIED
	 *
	 * @return array|false|string - вернет массив данных при успехе, ложь если не найден и строку если исключение
	 */
	public function getByIdentifier( $identifier ) {

		try {

			return $this->getOneByFilter( [ 'identifier' => strtoupper( $identifier ) ] );
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}