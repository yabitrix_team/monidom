<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\validators\AgentValidator;
use app\modules\app\v1\validators\PointValidator;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\IdentityInterface;

/**
 * Модель таблицы <b>users</b>
 *
 * @property mixed  $authKey
 * @property bool   $groups
 * @property string $uniqueAuthKey
 */
class Users extends Model implements IdentityInterface
{
    use WritableTrait;
    use ReadableTrait;
    /**
     * Константа сценария создания пользователя
     */
    const SCENARIO_CREATE = 'create';
    /**
     * Константа сценария обновления пользователя
     */
    const SCENARIO_UPDATE = 'update';
    /**
     * Константа сценария сброса пароля по SMS
     */
    const SCENARIO_RESET_PASSWORD = 'resetPassword';
    /**
     * Константа сценария установки пароля
     */
    const SCENARIO_SET_PASSWORD = 'setPassword';
    /**
     * Константа сценария запрос пользователя по логину и токену
     */
    const SCENARIO_GET_USER_BY_USERNAME_AND_TOKEN = 'getByUsernameAndToken';
    public $id;
    public $guid;
    public $username;
    public $email;
    public $auth_key;
    public $password;
    public $password_hash;
    public $password_reset_token;
    public $point_id;
    public $agent_id;
    public $first_name;
    public $last_name;
    public $second_name;
    public $is_accreditated;
    public $inside_support_token;
    public $request_mode;
    public $crib_client_id;
    public $created_at;
    public $updated_at;
    public $active;
    public $sort;
    /**
     * Поля необходимые для создание user
     */
    protected $usersFieldCreate = [
        'username',
        'password_hash',
    ];
    /**
     * @var array - поля создания пользователя для сценария Создание,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioCreate = [
        'username',
        'email',
        'first_name',
        'last_name',
        'second_name',
        'password_hash',
    ];
    /**
     * @var array - поля обновления пользователя для сценария Обновление,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioUpdate = [
        'email',
        'point_id',
        'agent_id',
        'first_name',
        'last_name',
        'second_name',
        'is_accreditated',
        'request_mode',
    ];
    /**
     * @var array - поля обновления пользователя для сценария Восстановление пароля,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioReset = [
        'password_reset_token',
    ];
    /**
     * @var array - поля обновления пользователя для сценария Восстановление пароля,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioResetPassword = [
        'username',
        'password_hash',
        'password',
    ];
    /** @var bool признак принадлежности к группе admin */
    protected $is_admin = false;
    /** @var bool признак принадлежности к группе partner_admin */
    protected $is_partner_admin = false;
    /** @var bool признак принадлежности к группе partner_user */
    protected $is_partner_user = false;
    /** @var bool признак принадлежности к группе clients */
    protected $is_client = false;
    /** @var bool признак принадлежности к группе agents */
    protected $is_agent = false;
    /** @var bool признак принадлежности к группе mobile user */
    protected $is_mobile_user = false;
    /** @var array данные о группах, к которым принадлежит пользователь */
    protected $_groups = [];

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected static function getFieldsNames(): array
    {
        return [
            'id',
            'guid',
            'username',
            'email',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'point_id',
            'agent_id',
            'first_name',
            'last_name',
            'second_name',
            'is_accreditated',
            'request_mode',
            'inside_support_token',
            'created_at',
            'updated_at',
            'active',
            'sort',
            'crib_client_id',
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     * Получение названия таблицы в которой хранятся данные identity (auth_key)
     */
    protected static function getTableName(): string
    {
        return "{{users}}";
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                $this->usersFieldCreate,
                'required',
                'message' => 'Поле обязательно для заполнения',
            ],
            [
                'id',
                'number',
                'message' => 'Поле "Идентификатор" не является числом',
            ],
            [
                'auth_key',
                'string',
                'message' => 'Некорректно задан формат для ключа аутинтефикации',
            ],
            [
                'username',
                'match',
                'pattern' => '/^9[0-9]{9}$/iu',
                'message' => 'Номер телефона указан в некорректном формате',
            ],
            [
                'email',
                'email',
                'message' => 'Поле "Электронная почта" заполнено некорректно',
            ],
            [
                'point_id',
                PointValidator::class,
            ],
            [
                'agent_id',
                AgentValidator::class,
            ],
            [
                'first_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
            ],
            [
                'first_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Имя" заполнено некорректно',
            ],
            [
                'last_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Фамилия" заполнено некорректно',
            ],
            [
                'last_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
            ],
            [
                'second_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Отчество" заполнено некорректно',
            ],
            [
                'second_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
            ],
            [
                'is_accreditated',
                'boolean',
                'trueValue'  => true,
                'falseValue' => false,
                'strict'     => false,
            ],
            [
                'inside_support_token',
                'string',
                'length'   => [2],
                'tooShort' => 'Неверный токен',
            ],
            [
                'password_reset_token',
                'string',
                'length'   => [36],
                'tooShort' => 'Токен сброса пароля неверный',
            ],
            [
                'request_mode',
                'in',
                'range'   => ['photos', 'full', ''],
                'message' => 'Не верно установлен режим сохранения заявки: request_mode',
            ],
            [
                'password',
                'match',
                'pattern' => "/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[\d])\S*$/iu",
                'message' => 'Пароль должен содержать минимум одну строчную буквы, одну цифру и быть длинной не меннее 6 символов',
            ],
            [
                'crib_client_id',
                'string',
                'message' => 'Идентификатор из системы CRIB указан в некорректном формате, обратитесь к администратору',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                                                = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE]                         = $this->fieldScenarioCreate;
        $scenarios[self::SCENARIO_UPDATE]                         = $this->fieldScenarioUpdate;
        $scenarios[self::SCENARIO_RESET_PASSWORD]                 = $this->fieldScenarioResetPassword;
        $scenarios[self::SCENARIO_GET_USER_BY_USERNAME_AND_TOKEN] = ['username', 'password_reset_token'];
        $scenarios[self::SCENARIO_SET_PASSWORD]                   = ['username', 'password'];

        return $scenarios;
    }

    /**
     * Запрос identity по идентификатору пользователя
     *
     * @inheritdoc
     * @throws \yii\db\Exception
     * @return self
     */
    public static function findIdentity($id, $active = true)
    {
        $active    = is_null($active)
            ? ' '
            : (empty($active) ? ' AND [[active]] = 0 ' : ' AND [[active]] = 1 ');
        $arrResult = Yii::$app->db
            ->createCommand(
                "SELECT [[".implode("]],[[", self::getFieldsNames())."]]
FROM ".self::getTableName()."
WHERE [[id]] = :id".$active,
                [':id' => $id]
            )
            ->queryOne();
        $user      = new Users();
        $user->setAttributes($arrResult);

        return new static ($user);
    }

    /**
     * Метод для получение identity по токену
     *
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // Код необходим для дальнешей разработки
        /*	$value = self::getAll();
            foreach ( $value as $user ) {
                if ( $user['accessToken'] === $token ) {
                    return new static( $user );
                }
            }

            return null;*/
    }

    /**
     * Finds static user by username
     *
     * @param string $username
     *
     * @return static|null
     * @throws \yii\db\Exception
     */
    public static function getStaticByLogin($username, $active = true)
    {
        $active    = is_null($active)
            ? ' '
            : (empty($active) ? ' AND [[active]] = 0 ' : ' AND [[active]] = 1 ');
        $arrResult = Yii::$app->db->createCommand(
            'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[username]] = :username'.$active,
            [':username' => $username]
        )
            ->queryOne();
        $user      = new Users();
        $user->setAttributes($arrResult);

        return new static ($user);
    }

    /**
     * Метод получения данных по идентификатору
     *
     * @param       $id     - идентификатор
     * @param array $fields - поля для выборки
     *
     * @return array|\Exception|false|mixed  - вернет результирующий массив при выборе нескольких полей
     *                                       вернет значение при выборе одного поля
     *                                       вернет ложь если не нашел запись
     *                                      при выбросе исключения вернет объект исключения
     */
    public function getOne($id, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : self::getFieldsNames();
            $method = count($fields) == 1 ? 'queryScalar' : 'queryOne';
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.self::getTableName().' WHERE [[id]] =:id',
                    [':id' => $id]
                )->$method();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения пользователя по username(логину)
     *
     * @param $username - логин пользователя
     *
     * @return array|false|string
     */
    public static function getByLogin($username)
    {
        try {
            return Yii::$app->db->createCommand(
                'SELECT 
 	[['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[username]] = :username',
                [':username' => $username]
            )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Finds user by email
     *
     * @param string $email
     *
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function getByEmail($email)
    {
        return Yii::$app->db->createCommand(
            'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[email]] = :email',
            [':email' => $email]
        )
            ->queryOne();
    }

    /**
     * Метод получения пользователя по username(логину) и токену
     *
     * @return array|false|string
     */
    public function getByLoginAndToken()
    {
        try {
            return Yii::$app->db->createCommand(
                'SELECT 
 	[['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[username]] = :username AND [[password_reset_token]] = :password_reset_token',
                [
                    ':username'             => $this->username,
                    ':password_reset_token' => $this->password_reset_token,
                ]
            )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * М-д возвращает список активных пользователей по точке
     *
     * @param null $PID
     *
     * @return array|false
     * @throws \yii\db\Exception
     */
    public function getByPoint($PID = null)
    {
        return Yii::$app->db->createCommand(
            'SELECT 
 	[['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[point_id]] = :point_id',
            [":point_id" => $PID]
        )
            ->queryAll();
    }

    /**
     * геттер username
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * геттер ID
     *
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Метод обновления пользователя по логину
     *
     * @param string $username - логин пользователя
     * @param array  $params   - Параметры для обновления пользователя
     *
     * @return bool|int - Вернет кол-во обновленных строк или выбросит исключение
     * @throws \Exception
     */
    public function updateByUsername($username, array $params = [])
    {
        try {
            $arPrepare = $this->prepareDataForExecute($params);
            if (!empty($arPrepare)) {
                $arPrepare['updated_at'] = date('Y-m-d H:i:s', time());

                return Yii::$app->db
                    ->createCommand()
                    ->update('users', $arPrepare, ['username' => $username])->execute();
            }
            throw new \Exception('Не переданы параметры для обновления пользователя');
        } catch (Exception $e) {
            throw new \Exception('Ошибка обновления пользователя по логину в базе данных, обратитесь к администратору');
        }
    }

    /**
     * Метод обновления пользователя по ID
     *
     * @param string $id     - ID пользователя
     * @param array  $params - Параметры для обновления пользователя
     *
     * @return bool|int - Вернет кол-во обновленных строк или выбросит исключение
     */
    public function updateById($id, array $params = [])
    {
        try {
            $arPrepare = $this->prepareDataForExecute($params);
            if (!empty($arPrepare)) {
                $arPrepare['updated_at'] = date('Y-m-d H:i:s', time());

                return Yii::$app->db
                    ->createCommand()
                    ->update('users', $arPrepare, ['id' => $id])->execute();
            }
            throw new \Exception('Не переданы параметры для обновления пользователя');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Получение ключа аутинтефикации
     *
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Проверка ключа аутинтефикации
     *
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     * @throws \Exception
     */
    public function validatePassword($password)
    {
        try {
            return $this->password_hash === Yii::$app->security->generatePasswordHash($password);
        } catch (\Exception $e) {
            throw  new \Exception($e->getMessage());
        }
    }

    /**
     * Метод для добавления захэшированного пароля
     *
     * @param $password - пароль в чистом виде
     *
     * @throws \yii\base\Exception
     */
    public function hashPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Создание пользователя
     *
     * @param array $params
     *
     * @return int|false|string - идентификатор пользователя, ошибка вставки в БД, ошибка
     */
    public function create(array $params = [])
    {
        try {
            $arPrepare = $this->prepareDataForExecute($params);
            if (!empty($arPrepare)) {

                $arPrepare['active']     = isset($arPrepare['active']) ? $arPrepare['active'] : '1';
                $arPrepare['sort']       = isset($arPrepare['sort']) ? $arPrepare['sort'] : '500';
                $arPrepare['created_at'] = $arPrepare['updated_at'] = date('Y-m-d H:i:s', time());
                $this->auth_key          = $this->getUniqueAuthKey();
                $arPrepare['auth_key']   = $this->auth_key;
                $insert                  = Yii::$app->db
                    ->createCommand()
                    ->insert('users', $arPrepare)->execute();
            }

            return !empty($insert) ? intval(Yii::$app->db->getLastInsertID()) : false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Получение уникального ключа аутинтефикации, которого нет в БД
     *
     * @return string
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function getUniqueAuthKey()
    {
        $authKey = Yii::$app->security->generateRandomString();
        while ($this->checkAuthKey($authKey)) {

            $authKey = Yii::$app->security->generateRandomString();
        }

        return $authKey;
    }

    /**
     * Проверка наличия auth_key в БД
     *
     * @param $authKey - ключ авторизации
     *
     * @return bool //false - если записей нет || true - если записи есть
     * @throws \yii\db\Exception
     */
    public function checkAuthKey($authKey)
    {
        $array = Yii::$app->db->createCommand(
            'SELECT 
 	[[id]] 
 FROM '.self::getTableName().'
 WHERE [[auth_key]]= :authKey',
            [':authKey' => $authKey]
        )
            ->queryAll();
        //  Если нет записей в ответе           => count($array) == 0
        //  Если одна и более запись в ответе   => count($array) >= 1
        return count($array) > 0;
    }

    /**
     * Сброс пароля
     *
     * @param        $login         - логин пользователя, он же номер телефона
     * @param        $password_hash - hash пароля
     *
     * @return int|string - вернет кол-во обновленных строк или текст исключения,
     *                    если при обновлении вернется 0, то пользователя по указанному логину
     *                    нет в системе и сбросить пароль для него нельзя
     */
    public function resetPassword($login, $password_hash)
    {
        try {
            if (empty($login) && empty($this->username)) {
                throw new \Exception('Не указан логин для сброса пароля');
            }
            if (empty($password_hash) && empty($this->password_hash)) {
                throw new \Exception('Не указан пароль для сброса пароля');
            }
            $login         = empty($login) ? $this->username : $login;
            $password_hash = empty($password_hash) ? $this->password_hash : $password_hash;

            return Yii::$app->db
                ->createCommand()
                ->update(
                    self::getTableName(),
                    [
                        'password_hash' => $password_hash,
                        'updated_at'    => date('Y-m-d H:i:s', time()),
                    ],
                    ['username' => $login]
                )->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления пользователя по идентификатору
     *
     * @param $id - идентификатор пользователя
     *
     * @return $this|int|string - при успехе вернет кол-во удаленных строк,
     *                          либо объект, если валидация не прошла, доступ к ошибкам $this->firstErrors
     *                          либо строку, если выброшено исключение
     */
    public function deleteOne($id)
    {
        try {
            if ($this->validate('id')) {
                return Yii::$app->db->createCommand()
                    ->delete(self::getTableName(), '[[id]] = :id', [':id' => $id])
                    ->execute();
            } else {
                return $this;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных пользователей по списку идентификаторов
     *
     * @param array $listId - списко идентификаторов
     * @param array $fields - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \Exception
     */
    public function getByListId(array $listId, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : self::getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.self::getTableName().
                    ' WHERE [[id]] IN ('.implode(',', array_filter($listId)).')'
                )->queryAll();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения пользователей из базы данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Объект "пользователь"
     * <p>
     * М-д создает объект пользователя из строки в таблице <b>users</b> по идентификатору, в котором доступны публичные
     * параметры:
     * <ul>
     * <li>id</li>
     * <li>guid</li>
     * <li>username</li>
     * <li>email</li>
     * <li>auth_key</li>
     * <li>password_hash</li>
     * <li>password_reset_token</li>
     * <li>point_id</li>
     * <li>agent_id</li>
     * <li>first_name</li>
     * <li>last_name</li>
     * <li>second_name</li>
     * <li>is_accreditated</li>
     * <li>request_mode</li>
     * <li>inside_support_token</li>
     * </ul>
     * а так же методы проверки принадлежности
     * к группе:
     * <ul>
     * <li>isAdmin()</li>
     * <li>isPartnerUser()</li>
     * <li>isPartnerAdmin()</li>
     * <li>isAgent()</li>
     * <li>isClient()</li>
     * <li>isMobileUser()</li>
     * </ul>
     * Проверка принадлежности осуществляется по полю "identifier" в таблице <b>groups</b>
     * </p>
     *
     * @param null|int $uid
     *
     * @return Users|bool
     * @throws \yii\db\Exception
     */
    public function build($uid = null)
    {
        if (isset($uid)) {
            if ($user = $this->getOne($uid)) {
                if ($this->_groups = UserGroup::instance()->byUserIDHard($uid)) {
                    $groups                 = array_column($this->_groups, 'identifier');
                    $this->is_admin         = in_array("admin", $groups);
                    $this->is_partner_admin = in_array("partner_admin", $groups);
                    $this->is_partner_user  = in_array("partner_user", $groups);
                    $this->is_client        = in_array("client", $groups);
                    $this->is_agent         = in_array("agent", $groups);
                    $this->is_mobile_user   = in_array("mobile_user", $groups);
                    unset(
                        $user['created_at'],
                        $user['updated_at'],
                        $user['active'],
                        $user['sort']
                    );
                    parent::__construct($user);
                    unset($user);

                    return $this;
                }
            }
        }

        return false;
    }

    public function isAdmin()
    {
        return false !== $this->is_admin;
    }

    public function isPartnerUser()
    {
        return false !== $this->is_partner_user;
    }

    public function isPartnerAdmin()
    {
        return false !== $this->is_partner_admin;
    }

    public function isAgent()
    {
        return false !== $this->is_agent;
    }

    public function isClient()
    {
        return false !== $this->is_client;
    }

    public function isMobileUser()
    {
        return false !== $this->is_mobile_user;
    }

    public function hasGroup()
    {
        return !empty($this->_groups);
    }

    public function getGroups()
    {
        return $this->_groups ?: false;
    }
}