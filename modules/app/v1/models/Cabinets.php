<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Points - модель для работы со справочником Точки
 *
 * @package app\modules\app\v1\models
 */
class Cabinets extends Model {
	use ReadableTrait;

	/**
	 * Метод получения всех элементов
	 *
	 * @param bool $active - Параметр отвечающий за получение всех элементов, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? '' : ( empty( $active ) ? ' WHERE `active` = 0 ' : ' WHERE `active` = 1 ' );

		return Yii::$app->db->createCommand( "SELECT
  `id`,
  `code`,
  `name`, 
  `subdomain`
FROM {$this->getTableName()} {$active}" )
		                    ->queryAll();
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{cabinets}}";
	}

	/**
	 * Метод получения элемента по идентификатору
	 *
	 * @param $id - Идентификатор элемента
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "SELECT
  `code`,
  `name`, 
  `subdomain`
FROM {$this->getTableName()}
WHERE `id` = :id", [ ':id' => $id ] )
			->queryOne();
	}

	/**
	 * @param null|string $subdomain - название поддомена (должно быть зарегестрировано в бд)
	 *
	 * @return array|bool|string
	 * @throws \yii\db\Exception
	 */
	public function bySubDomain( $subdomain = null ) {

		if ( isset( $subdomain ) ) {

			return $this->getByFilter( [ 'subdomain' => $subdomain ] )->execute();
		}

		return false;
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			'id',
			'code',
			'name',
			'subdomain',
		];
	}
}