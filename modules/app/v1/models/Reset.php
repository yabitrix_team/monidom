<?php

namespace app\modules\app\v1\models;

use yii\base\Model;

/*
* Модель для сброса пароля пользователя
*/
class Reset extends Model {
	/*
	 * Логин пользователя, он же номер телефона (в БД username)
	 */
	public $login;
	/*
	 * Пароль пользователя
	 */
	public $password;
	/**
	 * @var array - поля регистрации пользователя для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $registerFieldCreate = [
		'login',
		'password',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'login',
				'match',
				'pattern' => '/^9[0-9]{9}$/iu',
				'message' => 'Логин пользователя (номер телефона) заполнен некорректно',
			],
			[
				'password',
				'match',
				'pattern' => "/^(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,20}$/iu",
				'message' => 'Пароль должен содержать минимум одну заглавную, одну строчную буквы, одну цифру, один спец. символ и быть длиной не меннее 6 символов',
			],
		];
	}

	/*
	 * Метод сброса пароля
	 *
	 * @return true|array|false|string - создан, ошибка валидции, неизвестная ошибка, экспешн
	 */
	public function resetPassword() {

		try {

			//Проверяем статус кода подтверждения
			$statusArray = ( new SmsConfirm() )->getByLogin( $this->login );
			if ( empty( $statusArray ) || $statusArray == false ) {
				throw new \Exception( 'Не найдент код подтверждения' );
			}
			if ( is_string( $statusArray ) ) {
				throw new \Exception( $statusArray );
			}
			if ( $statusArray['status'] == 0 ) {
				throw new \Exception( 'Код подтверждения не подтвержден' );
			}
			//Проверяем пользователя на существование в БД
			$user = Users::getByLogin( $this->login );
			if ( ! $user ) {
				throw new \Exception( 'Пользователь с этим номером телефона не зарегистрирован' );
			}
			$user = ( new Users( [ 'scenario' => Users::SCENARIO_RESET_PASSWORD ] ) );
			//Создаем пользователя для сохранения в БД
			if ( ! $user->load( [
				                    'username' => $this->login,
			                    ], ''
			) ) {
				throw new \Exception( 'Не удалось загрузить данные для сброса пароля' );
			}
			$user->hashPassword( $this->password ); //Хэшируем пароль
			//сохраняем данные пользователя
			if ( ! $user->validate() ) {

				return $user->firstErrors;
			}
			//Сбрасываем пароль
			$result = $user->resetPassword( null, null );
			if ( intval( $result ) == 0 ) {
				throw new \Exception( "Не удалось найти пользователя для сброса пароля" );
			} elseif ( intval( $result ) > 1 ) {
				throw new \Exception( "Задублирован логин пользователя. Свяжитесь с администратором" );
			} elseif ( is_string( $result ) ) {
				throw new \Exception( $result );
			}
			//очистка записи подтверждения
			$resultSmsDelete = ( new SmsConfirm() )->deleteByLogin( $user->username );
			if ( intval( $resultSmsDelete ) == 0 ) {
				throw new \Exception( "Не найдена запись строки подтверждения для удаления" );
			}
			if ( intval( $resultSmsDelete ) != 1 ) {
				throw new \Exception( $resultSmsDelete );
			}

			return $resultSmsDelete;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}