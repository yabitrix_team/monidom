<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\soap\cmr\Annuity;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use DateInterval;
use DateTime;

/**
 * Class Leads - модель по работе с Лидами
 *
 * @package app\modules\app\v1\models
 */
class Leads extends Model
{
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Поля для создания лида
     */
    public $guid;
    public $code;
    public $summ;
    public $months;
    public $client_first_name;
    public $client_last_name;
    public $client_patronymic;
    public $client_mobile_phone;
    public $client_passport_serial_number;
    public $client_passport_number;
    public $client_birthday;
    public $vin;
    public $auto_model_guid;
    public $auto_year;
    public $region;
    public $leads_consumer_id;
    /**
     * @var string - колонка потребитель отсутствует в текущей таблице БД,
     *               необходимо для заполнения колонки leads_consumer_id,
     *              по умолчанию указан потребитель ЛКК
     */
    public $consumer;
    /**
     * @var array - массив данных потребителя лида
     */
    protected $dataConsumer;
    /**
     * @var string - потребитель лида по умолчанию
     */
    protected $consumerDefault = 'client';
    /**
     * @var array - обязательные поля при создании лида
     */
    protected $reqFieldCreate = [
        'client_first_name',
        'client_mobile_phone',
        'region',
    ];

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return "{{leads}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {

        return [
            "id",
            "guid",
            "code",
            "summ",
            "months",
            "client_first_name",
            "client_last_name",
            "client_patronymic",
            "client_mobile_phone",
            "client_passport_serial_number",
            "client_passport_number",
            "client_birthday",
            "auto_model_guid",
            "auto_year",
            "region",
            "vin",
            "leads_consumer_id",
            "sms_id",
            "is_closed",
            "created_at",
        ];
    }

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {

        return [
            [
                $this->reqFieldCreate,
                'required',
                'message' => 'Поле обязательно для заполнения',
            ],
            [
                $this->reqFieldCreate,
                'trim',
            ],
            [
                'guid',
                'string',
                'length'   => [36],
                'tooShort' => 'Поле "GUID" должно содержать 36 символов',
            ],
            [
                'auto_model_guid',
                'string',
            ],
            [
                'auto_year',
                'number',
            ],
            [
                'region',
                'string',
            ],
            [
                'summ',
                'number',
                'message' => 'Поле "Сумма займа" заполнено некорректно',
            ],
            [
                'months',
                'number',
                'message' => 'Поле "Количество месяцев" заполнено некорректно',
            ],
            [
                'client_first_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Имя" заполнено некорректно',
            ],
            [
                'client_first_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
            ],
            [
                'client_last_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Фамилия" заполнено некорректно',
            ],
            [
                'client_last_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
            ],
            [
                'client_patronymic',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Отчество" заполнено некорректно',
            ],
            [
                'client_patronymic',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
            ],
            [
                'client_passport_serial_number',
                'match',
                'pattern' => '/^\d{4}$/iu',
                'message' => 'Поле "Серия паспора" заполнено некорректно',
            ],
            [
                'client_passport_number',
                'match',
                'pattern' => '/^\d{6}$/iu',
                'message' => 'Поле "Номер паспора" заполнено некорректно',
            ],
            [
                'client_birthday',
                'match',
                'pattern' => '/^\d{4}-\d{2}-\d{2}$/i',
                'message' => 'Поле "Дата рождения" заполнено некорректно',
            ],
            [
                ['client_mobile_phone'],
                'match',
                'pattern' => '/^9\d{9}$/iu',
                'message' => 'Номер телефона должен содержать 10 символов и начинаться с 9',
            ],
            [
                'vin',
                'string',
                'length'   => [17],
                'tooShort' => 'Поле "VIN" должно содержать 17 символов',
            ],
            ['consumer', 'default', 'value' => $this->consumerDefault],
            ['consumer', 'validateConsumer'],
        ];
    }

    /**
     * Метод валидации для потребителя
     *
     * @param $attribute - атрибуты
     * @param $params
     */
    public function validateConsumer($attribute, $params)
    {
        $this->dataConsumer = (new LeadsConsumer())->getByIdentifier(
            $this->$attribute,
            [
                'id',
                'identifier',
                'requests_origin_id',
                'sms_text',
            ]
        );
        if (is_a($this->dataConsumer, \Exception::class)) {
            $this->addError($attribute, 'Не удалось проверить значение потребителя лида, обратитесь к администратору');
        }
        if (empty($this->dataConsumer)) {
            $this->addError($attribute, 'Указанное значение потребителя лида отсутствует в системе');
        }
        //подготавливаем значения поля id потребителя лида для БД
        $this->leads_consumer_id = $this->dataConsumer['id'];
        //исключаем поле consumer до вставки в БД
        $this->$attribute = null;
    }

    /**
     * Метод получения лида по коду
     *
     * @param string $code - код лида
     *
     * @return array|bool|false - вернет результат выборки
     */
    public function getByCode(string $code)
    {
        try {
            if (strlen($code) != 8) {
                throw new \Exception('При попытке получения лида по коду был некорректно указан код лида');
            }

            return Yii::$app->db
                ->createCommand(
                    "SELECT 
[[code]],
[[summ]],  
[[months]], 
[[client_first_name]], 
[[client_last_name]], 
[[client_patronymic]],
[[client_mobile_phone]],
[[client_passport_serial_number]], 
[[client_passport_number]],
DATE_FORMAT([[client_birthday]], '%Y-%m-%dT00:00:00.000Z') AS [[client_birthday]], 
[[vin]]
FROM ".$this->getTableName()."
WHERE [[code]]=:code",
                    [':code' => $code]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения лида по номеру телефона
     *
     * @param             $phone    - номер телефона
     * @param array       $fields   - поля для выборки
     * @param string|null $consumer - потребитель лида, указываем константы из модели LeadsConsumer
     * @param bool        $queryAll - способ выборки
     *
     * @return mixed|\Exception - вернет результирующий массив при выборе нескольких полей
     *                                       вернет значение при выборе одного поля
     *                                       вернет ложь если не нашел запись
     *                                      при выбросе исключения врнет объект исключения
     */
    public function getByPhone($phone, array $fields = [], string $consumer = null, $queryAll = false)
    {
        try {
            $fields    = !empty($fields) ? $fields : $this->getFieldsNames();
            $method    = empty($queryAll) ? 'queryOne' : 'queryAll';
            $fields    = '[['.implode("]],[[", $fields).']]';
            $params    = [':phone' => $phone];
            $condition = !empty($consumer)
                ? ' AND leads_consumer_id = (SELECT [[id]] FROM {{leads_consumer}} WHERE [[identifier]] = :consumer) '
                : '';
            empty($consumer) ?: $params = array_merge($params, [':consumer' => $consumer]);

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    ' WHERE [[client_mobile_phone]] =:phone '.$condition.' ORDER BY id DESC',
                    $params
                )->$method();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения данных потребителя
     *
     * @param string|null $consumer - потребитель лида в виде символьного идентификатора
     *
     * @return array|\Exception|false|mixed - массив данных лида
     *                                      либо объект исключения
     */
    public function getDataConsumer(string $consumer = null)
    {
        try {
            if (!empty($this->dataConsumer)) {
                return $this->dataConsumer;
            }

            return (new LeadsConsumer())->getByIdentifier($consumer);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения потребителя по умолчанию
     *
     * @return string - вернет символьный идентфикатор потребителя
     */
    public function getConsumerDefault()
    {

        return $this->consumerDefault;
    }

    /**
     * Метод создания лида
     *
     * @param array $params - Параметры для создания лида,
     *                      можно использовать совместно с  $model->load
     *
     * @return $this|array|bool|\Exception - в случае успеха вернет массив записанных данных по лиду
     *                                      в случае ошикби валидации вернет текущий объект
     *                                      в случае исключения вернет объект Исключения
     */
    public function create(array $params = [])
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $arPrepare = $this->prepareDataForExecute($params);
            if (empty($arPrepare)) {
                throw new \Exception('Не переданы параметры для создания лида');
            }
            $arPrepare['code'] = '';
            $cntTry            = 0;
            // пока не найдем уникальный код пытаемся сгенерить новый
            while (!strlen($arPrepare['code']) || is_array($this->getByCode($arPrepare['code']))) {
                if (++$cntTry > 100) {
                    throw new \Exception('Превышено максимальное число попыток создания лида');
                }
                $arPrepare['code'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 8);
            }
            $arPrepare['created_at'] = new Expression('NOW()');
            $arPrepare['is_closed']  = 0;
            $insert                  = Yii::$app->db
                ->createCommand()
                ->insert($this->getTableName(), $arPrepare)->execute();

            return !empty($insert) ? $arPrepare : false;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод обновления лида по его идентификатору
     *
     * @param       $id      - идентификатор лида
     * @param array $params  - параметры для обновлениялида
     * @param bool  $prepare - флаг для подготовки данных
     *
     * @return $this|\Exception|int - вернет результат обновления заявки
     *                              либо объект Исключения
     *                              либо текущий объект в случае не пройденной валидации
     */
    public function updateOne($id, array $params = [], $prepare = true)
    {
        try {
            $arPrepare = !empty($prepare) ? $this->prepareDataForExecute($params) : $params;
            if (empty($arPrepare)) {
                throw new \Exception('Не переданы параметры для обновления лида');
            }

            return Yii::$app->db
                ->createCommand()
                ->update($this->getTableName(), $arPrepare, ['id' => $id])->execute();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод удаления лида по идентификатору
     *
     * @param $id - идентификатор лида
     *
     * @return int|string - вернет кол-во удаленных строк в таблице БД
     */
    public function deleteOne($id)
    {
        try {
            return Yii::$app->db->createCommand()->delete($this->getTableName(), 'id = :id', [':id' => $id])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления лида по коду
     *
     * @param string $code - код заявки
     *
     * @return int|string - вернет кол-во удаленных строк в таблице БД
     */
    public function deleteByCode(string $code)
    {
        try {
            return Yii::$app->db->createCommand()->delete($this->getTableName(), 'code = :code', [':code' => $code])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления лидов по телефону
     *
     * @param string $phone - код заявки
     *
     * @return int|string - вернет кол-во удаленных строк в таблице БД
     */
    public function deleteByPhone(string $phone)
    {
        try {
            return Yii::$app->db->createCommand()
                ->delete($this->getTableName(), 'client_mobile_phone = :phone', [':phone' => $phone])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод подготовки данных заявки относительно лида
     *
     * @param string  $consumer        - символьный идентификатор потребителя лида
     * @param         $requestOriginId - идентификатор места создания заявки
     * @param         $clientId        - идентификатор клиента
     * @param array   $lead            - массив данных по лиду, необходимый для создания заявки
     *
     * @return array|\Exception - вернет массив подготовленных данных для заявки
     *                          в случае исключения вернет объект
     */
    public function prepareRequest(string $consumer, $requestOriginId, $clientId, array $lead)
    {
        try {
            $userId  = Yii::$app->params['client']['settings']['user_id'];
            $pointId = Yii::$app->params['client']['settings']['point_id'];
            if ($consumer != $this->consumerDefault) {
                $userId  = Yii::$app->params['mp']['settings']['user_id'];
                $pointId = Yii::$app->params['mp']['settings']['point_id'];
            }
            $regionRelation = (new Regions())->getRelation('name', ['id'], true);
            if (is_a($regionRelation, \Exception::class)) {
                throw new \Exception(
                    'При поптыке сохранения предварительной заявки по лиду произошла ошибка получения связей регионов, обратитесь к администратору'
                );
            }
            $data = [
                'requests_origin_id'  => $requestOriginId,
                'point_id'            => $pointId,
                'user_id'             => $userId,
                'client_id'           => $clientId,
                'client_first_name'   => $lead['client_first_name'],
                'client_mobile_phone' => $lead['client_mobile_phone'],
                'client_region_id'    => $regionRelation[$lead['region']],
            ];
            empty($lead['guid']) ?: $data['guid'] = $lead['guid'];
            empty($lead['summ']) ?: $data['summ'] = $lead['summ'];
            empty($lead['client_last_name']) ?: $data['client_last_name'] = $lead['client_last_name'];
            empty($lead['client_patronymic']) ?: $data['client_patronymic'] = $lead['client_patronymic'];
            empty($lead['client_mobile_phone'])
                ?:
                $data['client_mobile_phone'] = $lead['client_mobile_phone'];
            empty($lead['client_passport_serial_number'])
                ?:
                $data['client_passport_serial_number'] = $lead['client_passport_serial_number'];
            empty($lead['client_passport_number'])
                ?:
                $data['client_passport_number'] = $lead['client_passport_number'];
            empty($lead['client_birthday']) ?: $data['client_birthday'] = $lead['client_birthday'];
            empty($lead['auto_year']) ?: $data['auto_year'] = $lead['auto_year'];
            if (!empty($lead['auto_model_guid'])) {
                $auto = (new AutoModels())->getByGuid($lead['auto_model_guid'], ['id', 'auto_brand_id']);
                if (empty($auto) || is_a($auto, \Exception::class)) {
                    throw new \Exception(
                        'При подготовке данных авто клиента для сохранения предварительной заявки по лиду произошла ошибка, обратитесь к администратору'
                    );
                }
                $data['auto_brand_id'] = $auto['auto_brand_id'];
                $data['auto_model_id'] = $auto['id'];
            }
            if (!empty($lead['months'])) {
                $date = new DateTime();
                $date->add(new DateInterval('P'.$lead['months'].'M'));
                $data['date_return'] = $date->format('Y-m-d');
            }
            //получение кредитного продукта для предварительной заявки
            if (!empty($data['date_return']) && !empty($lead['summ'])) {
                $annuity = new Annuity(['scenario' => Annuity::SCENARIO_SMART_ANNUITY]);
                $annuity->load(
                    [
                        'amount' => $lead['summ'],
                        'date'   => $data['date_return'],
                    ],
                    ''
                );
                $res = $annuity->getSmartAnnuity();
                if (is_a($res, Annuity::class) || is_string($res)) {
                    throw new \Exception(
                        'При поптыке сохранения предварительной заявки по лиду произошла ошибка получения кредитного продукта, обратитесь к администратору'
                    );
                }
                $creditProductId = (new Products)->getDataByGuid($res->GUID, ['id']);
                if (is_a($creditProductId, \Exception::class) || empty($creditProductId)) {
                    throw new \Exception(
                        'При попытке сохранения предварительной заявки по лиду не удалось получить кредитный продукт, обратитесь к администратору'
                    );
                }
                $data['credit_product_id'] = $creditProductId;
            }

            return $data;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод проверки на существование лида в системе
     * если не указать лид, то проверит только по телефону
     * наличие лида
     *
     * @param        $phone - номер телефона клиента
     * @param string $guid  - ГУИД заявки, не обязательный параметр
     *
     * @return bool|\Exception - вернет истину если лид существует иначе ложь
     *                         при выбросе ислкючения вернет объект
     */
    public function checkExists($phone, string $guid = '')
    {
        try {
            $params    = [':phone' => $phone];
            $condition = empty($guid) ? $guid : ' AND [[guid]]=:guid  ';
            $params    = empty($guid)
                ? $params
                : array_merge(
                    $params,
                    [
                        ':guid' => $guid,
                    ]
                );
            $res       = Yii::$app->db
                ->createCommand(
                    'SELECT [[id]] FROM '.$this->getTableName().' WHERE [[client_mobile_phone]] =:phone '.$condition,
                    $params
                )->queryAll();

            return !empty($res);
        } catch (\Exception $e) {
            return $e;
        }
    }
}