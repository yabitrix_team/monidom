<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\validators\PhoneValidator;
use DateTime;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class SmsConfirm - модель для работы с подтверждением операций через смс (sms_confirm)
 *
 * @package app\modules\app\v1\models
 */
class SmsConfirm extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константа сценария создания заявки
     */
    const SCENARIO_CREATE = 'create';
    /**
     * Константа сценария обновления заявки
     */
    const SCENARIO_CHECK = 'check';
    /**
     * @var - номер телефона, на который отправляем сообщение, он же логин
     */
    public $login;
    /**
     * @var - код подтверждения операции, отправляемый в СМС-сообщении
     */
    public $code;

    /**
     * @return array - правила валидации передаваемых данных
     */
    public function rules()
    {

        return [
            [
                'login',
                'required',
                'message' => 'Не указан номер телефона',
            ],
            [
                'login',
                PhoneValidator::class,
            ],
            [
                'code',
                'required',
                'message' => 'Не указан код подтверждения',
            ],
            [
                'code',
                'number',
                'message' => 'Код подтверждения указан в некорректном формате',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                        = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['login'];
        $scenarios[self::SCENARIO_CHECK]  = ['login', 'code'];

        return $scenarios;
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{sms_confirm}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "code",
            "status",
            "attempt",
            "lock_expired_at",
            "sms_id",
            "created_at",
            "updated_at",
        ];
    }

    /**
     * Метод для генерации смс кода, вызываем перед методом save или update
     * при необходимости создания или обновлении кода подтверждения,
     *
     * @return int - вернет код подтверждения
     */
    public function generateSmsCode()
    {
        $this->code = mt_rand(1054, 9966);

        return $this->code;
    }

    /**
     * Метод получения текста с кодом подтверждения
     *
     * @param string $hashCode - хэш код для формирования СМС-текста для android
     * @param null   $codeSMS  - код подтверждения СМС
     *
     * @return string - вернет строку содержащую текст
     * @throws \Exception
     */
    public function getTextSmsConfirm(string $hashCode = null, $codeSMS = null)
    {
        !empty($codeSMS) ?: $codeSMS = $this->code;
        $smsText = 'Код подтверждения: '.$codeSMS;
        if (strlen($hashCode) == 11) {
            $smsText = '<#> Код подтверждения: '.$codeSMS.' Служебная информация: '.$hashCode;
        }

        return $smsText;
    }

    /**
     * Метод сохранения записи подтверждения операции через СМС( создает или обновляет )
     * если запись существует, то обновит запись, так как-будто только что созадал,
     *
     * @return int - вернет 1 - при добавлении, 2 - при обновлении и строку при выбросе исключения
     * @throws \Exception
     */
    public function save()
    {
        try {
            if (empty($this->code)) {
                $this->generateSmsCode();
            }
            $params                    = $this->getParams();
            $insert                    = $update = $this->getAttributes();
            $insert['status']          = $update['status'] = 0;
            $insert['attempt']         = $update['attempt'] = $params['attempt'];
            $insert['lock_expired_at'] = $update['lock_expired_at'] = new Expression(
                'NOW() + INTERVAL '.$params['lock_expired_at'].' MINUTE'
            );
            $insert['created_at']      = new Expression('NOW()');

            return Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)->execute();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка при работе с базой данных, не удалось сохранить код подтверждения операции, отправленный через СМС-сообщение, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод для изменения статуса подтверждения операции
     *
     * @param        $login  - логин пользователя, он же номер телефона
     * @param int    $status - статус операции, по умолчанию 1, то есть подтверждено
     *
     * @return int|string - вернет кол-во обновленных строк или текст исключения,
     *                    если при обновлении вернется 0, то запись по указанному логину
     *                    отсутствует в системе, то есть нет операций на подтверждение
     * @throws \Exception
     */
    public function setStatus($login = false, $status = 1)
    {
        try {
            if (empty($this->login) && empty($login)) {
                throw new \Exception('Не указан логин при установке статуса подтверждения операции по коду СМС');
            }
            $login = empty($login) ? $this->login : $login;

            return Yii::$app->db
                ->createCommand()
                ->update(
                    $this->getTableName(),
                    [
                        'status'     => $status,
                        'updated_at' => new Expression('NOW()'),
                    ],
                    ['login' => $login]
                )->execute();
        } catch (Exception $e) {
            throw new \Exception(
                'При попытке подтверждения кода из СМС-сообщения произошла ошибка установки статуса в БД, обратитесь к администратору'
            );
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Метод для обновления данных по логину
     *
     * @param bool  $login  - логин пользователя, он же номер телефона
     * @param array $params - параметры в виде массива для обновления
     *                      [attempt => 2]
     *
     * @return int|string - вернет кол-во обновленных строк или строку исключения
     */
    public function updateByLogin($login, array $params)
    {
        try {
            if (empty($this->login) && empty($login)) {
                throw new \Exception('Не указан логин при обновлении записи подтверждения операции по коду СМС');
            }
            $login = empty($login) ? $this->login : $login;

            return Yii::$app->db
                ->createCommand()
                ->update($this->getTableName(), $params, ['login' => $login])->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод для получения времени блокировки отправки кода смс по дате с временем
     *
     * @param string|null $lockExpiredAt - дата и время блокировки оптравки СМС
     *                                   формат: 2018-10-16 18:20:43
     * @param bool        $isSecond      - флаг, отвечающий за возвращение результата в секундах
     *
     * @return bool|string - вернет время до конца блокировки, либо false,
     *              то есть блокировка отсутствует
     * @throws \Exception
     */
    public function getLock(string $lockExpiredAt = null, $isSecond = false)
    {
        $curTime = date('Y-m-d H:i:s');
        if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/i', $lockExpiredAt) && $lockExpiredAt > $curTime) {
            $dateTimeCur  = new DateTime($curTime);
            $dateTimeLock = new DateTime($lockExpiredAt);
            $interval     = $dateTimeCur->diff($dateTimeLock);
            $lockTime     = $interval->format('%H:%I:%S');
            if (!empty($isSecond)) {
                list($h, $m, $s) = explode(':', $lockTime);
                $lockTime = $h * 3600 + $m * 60 + $s;
            }

            return $lockTime;
        }

        return false;
    }

    /**
     * Метод проверки блокировки отправки кода смс по логину
     *
     * @param      $login    - логин, он же номер телефона,
     * @param bool $isSecond - флаг, отвечающий за возвращение результата в секундах
     *
     * @return bool - вернет true если проверка пройдена, иначе выбросит исключение
     * @throws \Exception
     */
    public function checkLockByLogin($login = null, $isSecond = false)
    {
        $result = $this->getByLogin($login, ['lock_expired_at']);
        if (is_string($result)) {
            throw new \Exception(
                'При попытке проверки кода подтверждения не удалось получить по номеру телефона данные из БД, обратитесь к администратору'
            );
        }
        if (!empty($result) && ($timeLock = $this->getLock($result['lock_expired_at'], $isSecond))) {
            throw new \Exception(
                'Повторный запрос кода возможен через '.$timeLock
            );
        }

        return true;
    }

    /**
     * Метод получения данных по логину
     *
     * @param       $login    - логин, он же номер телефона,
     *                        так же задается через метод $modal->load
     * @param array $fields   - поля для выборки,
     *                        если не указываем, выбирает поля по умолчанию
     *
     * @return array|false|string - вернет массив данных или ложь, или строку исключения
     */
    public function getByLogin($login = null, array $fields = [])
    {
        try {
            $this->login = empty($login) ? $this->login : $login;
            if (!$this->validate(['login'])) {
                throw new \Exception(reset($this->firstErrors));
            }
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[login]] =:login',
                    [':login' => $this->login]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Проверка что код подтверждения существует и подтвержден
     *
     * @param       $login - логин, он же номер телефона
     *
     * @return true|false|string - вернет истину или ложь, или строку исключения
     */
    public function checkStatus($login)
    {
        $result = $this->getByLogin($login);
        if (is_array($result)) {
            return $result['status'] == 1;
        }

        return $result;
    }

    /**
     * Метод для удаления записи операции подтверждения через СМС
     *
     * @param $login - логин пользователя, он же телефон
     *
     * @return int|string - вернет 1 - при успешно удалении,
     *                     0 - если запись не найдена
     *                    строку -  если выброшено исключение
     */
    public function deleteByLogin($login)
    {
        try {
            if (empty($this->login) && empty($login)) {
                throw new \Exception('Не указан логин при удалении записи операции подтверждения по коду');
            }
            $login = empty($login) ? $this->login : $login;

            return Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), '[[login]] = :login', [':login' => $login])->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод верификации кода подтверждения отправленного в СМС-сообщении
     *
     * @param null $code  - код подтверждения, отправленный через СМС
     * @param null $login - логин пользователя, он же номер телефона
     *
     * @return bool - вернет истину если проверка пройдена, иначе ложь
     * @throws \Exception
     */
    public function verificationCode($code = null, $login = null)
    {
        $this->code  = empty($code) ? $this->code : $code;
        $this->login = empty($login) ? $this->login : $login;
        if (!$this->validate()) {
            throw new \Exception(
                'Ошибка валидации переданных параметров при попытке проверки кода'
            );
        }
        $record = $this->getByLogin($this->login, ['code', 'attempt', 'lock_expired_at', 'status']);
        if (empty($record) || is_string($record)) {
            $msg = empty($record)
                ? 'Для пользователя отсутствует операция подтверждения по коду'
                : 'При попытке проверки кода не удалось получить данные по номеру телефона из БД, обратитесь к администратору';
            throw new \Exception($msg);
        }
        //запрос нового кода, если все попытки ввода кода использованы
        if (empty($record['attempt'])) {
            throw new \Exception('Исчерпаны попытки ввода кода, запросите новый код');
        }
        //при указании неверного кода уменьшаем попытки ввода и выбрасываем ошибку
        if ($record['code'] != $this->code) {
            $updateParams['attempt'] = --$record['attempt'];
            if ($updateParams['attempt'] >= 0) {
                $this->updateByLogin(false, $updateParams);
            }
            if ($updateParams['attempt'] == 0) {
                throw new \Exception('Исчерпаны попытки ввода кода, запросите новый код');
            }
            $text = $updateParams['attempt'] == 1 ? 'попытка' : 'попытки';
            throw new \Exception('Введен неверный код. Осталось '.$updateParams['attempt'].' '.$text);
        }
        //запрет на повторную проверку подтвержденного кода
        if ($record['code'] == $this->code && !empty($record['status'])) {
            throw new \Exception('Запросите новый код');
        }
        //установка статуса "Код из СМС подтвержден"
        if ($record['code'] == $this->code) {
            $this->setStatus();
        }

        return true;
    }

    /**
     * Метод получения параметров для работы с СМС-сообщениями
     *
     * @return array - вернет массив параметров для работы с СМС-сообщениями
     *               [
     *                  attempt => 3
     *                  lock_expired_at => 5
     *               ]
     * @throws \InvalidArgumentException
     */
    public function getParams()
    {
        if (
            !($paramAttempt = Yii::$app->params['core']['sms']['attempt'])
            || !($paramLock = Yii::$app->params['core']['sms']['lock_expired_at'])
            || !is_numeric($paramAttempt)
            || !is_numeric($paramLock)
        ) {
            throw new \InvalidArgumentException(
                'Не указаны или указаны некорректно конфигурациии для работы с СМС-сообщениями, обратитесь к администратору'
            );
        }

        return [
            'attempt'         => $paramAttempt,
            'lock_expired_at' => $paramLock,
        ];
    }
}