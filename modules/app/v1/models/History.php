<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class History - модель для работы со справочником История заявок
 *
 * @package app\modules\app\v1\models
 */
class History extends Model {
	use WritableTrait;
	use ReadableTrait;
	/*
	 * Страница пагинации
	 */
	public $pagin;
	/*
	 * Дата начала выборки
	 */
	public $dateFrom = '2000-01-01';
	/*
	 * Дата конца выборки
	 */
	public $dateTo = '2050-01-01';

	public function rules() {

		return [
			[
				'pagin',
				'number',
				'message' => 'Некорректно указана страница для выборки',
			],
		];
	}

	/**
	 * Получение списка заявок по ID с учетом пагинации
	 *
	 * @throws \Exception
	 */
	public function getRequestWithPagination( array $requestsId ) {


		if ( ! $this->validate() ) {
			throw new \Exception( 'Входные данные не проходят валидацию. Повторите запрос с валидными данными или обратитесь к администратору.' );
		}
		if ( ! yii::$app->params['web']['request']['pagination']
		     || ! is_int( yii::$app->params['web']['request']['pagination'] ) ) {
			throw new \Exception( "Не заданы параметры пагинации в настройках сервера. Обратитесь к администратору." );
		}
		if ( $this->pagin < 1 ) {
			throw new \Exception( "Неверно указана страница пагинации, должна начинаться с 1." );
		}
		$this->pagin --;

		return Yii::$app->db->createCommand( '
		SELECT
		[[' . implode( "]],[[", self::getFieldsNames() ) . ']]		
		FROM ' . $this->getTableName() . '
		WHERE [[id]] IN (' . implode( ',', $requestsId ) . ')
		AND [[created_at]] BETWEEN \'' . $this->dateFrom . '\' AND \'' . $this->dateTo . ' 23:59:59\'
		ORDER BY [[created_at]] DESC
 LIMIT ' . yii::$app->params['web']['request']['pagination'] . '
 OFFSET ' . yii::$app->params['web']['request']['pagination'] * ( $this->pagin ) )
		                    ->queryAll();
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"code",
			"created_at",
			"updated_at",
			"num",
			"summ",
			"client_first_name",
			"client_last_name",
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{requests}}";
	}
}