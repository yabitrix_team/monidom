<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Employments - модель для работы со справочником Занятости
 *
 * @package app\modules\app\v1\models
 */
class Employments extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {
		return "{{employments}}";
	}
	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}