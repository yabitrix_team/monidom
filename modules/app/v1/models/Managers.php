<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Managers - модель для работы со справочником Менеджеры
 *
 * @package app\modules\app\v1\models
 */
class Managers extends Model {
	use WritableTrait;
	use ReadableTrait;

	/**
	 * Метод получения всех пунктов Менеджеров
	 *
	 * @param int $pointId - идентификатор точки
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getManagerByPointId( int $pointId, $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' AND `m`.`active` = 0 AND `mp`.`active` = 0': ' AND `m`.`active` = 1 AND `mp`.`active` = 1' );

		return Yii::$app->db->createCommand( 'SELECT 
`m`.`guid`,
`m`.`name`,
`m`.`last_name`,
`m`.`second_name`,
`m`.`phone`,
`m`.`email`,
`m`.`photo_id`,	
`mp`.`point_id`,
`mp`.`type`
FROM 
`managers` AS `m`
LEFT JOIN `managers_points` AS `mp` ON `m`.`id` = `mp`.`manager_id`
WHERE `mp`.`point_id` = :point_id ' . $active, [ ':point_id' => $pointId ] )
		                    ->queryAll();
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{managers}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"last_name",
			"second_name",
			"phone",
			"email",
			"photo_id",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}