<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class PcmLogData extends Model {
	/**
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function get() {

		return Yii::$app->db->createCommand( 'SELECT * FROM pcm_log_data' )
		                    ->queryAll();
	}
}