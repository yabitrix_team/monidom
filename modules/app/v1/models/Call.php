<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class Call extends Model {
	/*
	 * Логин пользователя, он же номер телефона (в БД username)
	 */
	public $login;
	/**
	 * @var array - поля регистрации пользователя для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $registerFieldCreate = [
		'login',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'login',
				'match',
				'pattern' => '/^9[0-9]{9}$/iu',
				'message' => 'Логин пользователя (номер телефона) заполнен некорректно',
			],
		];
	}

	/*
	 * Метод отправки сообщения обратный звонок
	 *
	 * @return true|array|false|string - отправлен|ошибка валидации|неуспех отправки|сообщение об ошибку
	 */
	public function send() {

		try {
			//Подготавливаем сообщение
			$sendMail = Yii::$app->mailer->compose( 'call', [
				'name'  => ( Users::getByLogin( $this->login ) )['first_name'],
				'login' => $this->login,
			] )
			                             ->setFrom( Yii::$app->params['core']['email']['from'] )
			                             ->setTo( Yii::$app->params['core']['email']['callPhoto'] )
			                             ->setSubject( 'Обратный звонок.' );
			$sendMail->send();
			if ( $sendMail ) {
				//todo[hdcy]: 17.04.2018 Выводить сообщение в лог если файлы не удалось удалить
				return true;
			}

			return "Неизвестная ошибка при отправке сообщения. Обратитесь к администратору";
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}