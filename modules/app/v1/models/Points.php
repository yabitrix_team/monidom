<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Points - модель для работы со справочником Точки
 *
 * @package app\modules\app\v1\models
 */
class Points extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {
		return "{{points}}";
	}
	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"partner_id",
			"country",
			"index",
			"region",
			"area",
			"city",
			"locality",
			"street",
			"house",
			"housing",
			"office",
			"brand_name",
			"time_table",
			"coords",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}