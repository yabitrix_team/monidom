<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class StatusesJournal - модель для работы с таблице связей заявки со статусами
 *
 * @package app\modules\app\v1\models
 */
class StatusesJournal extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;

    /**
     * @param null $RID
     *
     * @return array
     * @throws \Exception
     */
    public function getByRequestID($RID = null)
    {
        return Yii::$app->db
            ->createCommand(
                'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[request_id]] = :request_id',
                ['request_id' => $RID]
            )
            ->queryAll();
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "request_id",
            "status_id",
            "created_at",
            "updated_at",
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{statuses_journal}}";
    }

    /**
     * @param null $RID
     * @param bool $getName
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getCurrentByRequestID($RID = null)
    {
        return Yii::$app->db
            ->createCommand(
                'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[request_id]] = :request_id ORDER BY [[created_at]] DESC LIMIT 1',
                ['request_id' => $RID]
            )
            ->queryOne();
    }
}
