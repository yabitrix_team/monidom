<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class RequestFile - модель взаимодействия с таблицей связей заявок с файлами
 *
 * @package app\modules\app\v1\models
 */
class RequestFile extends Model
{
    use WritableTrait;
    use ReadableTrait;
    /**
     * Константы привязки файлов
     */
    CONST BIND_FOTO_STS          = 'foto_sts';
    CONST BIND_FOTO_PTS          = 'foto_pts';
    CONST BIND_FOTO_AUTO         = 'foto_auto';
    CONST BIND_FOTO_PASSPORT     = 'foto_passport';
    CONST BIND_FOTO_CLIENT       = 'foto_client';
    CONST BIND_FOTO_EXTRA        = 'foto_extra';
    CONST BIND_FOTO_CARD         = 'foto_card';
    CONST BIND_FOTO_NOTIFICATION = 'foto_notification';
    CONST BIND_DOC_PACK_1        = 'doc_pack_1';
    CONST BIND_DOC_PACK_2        = 'doc_pack_2';
    CONST BIND_DOC_PACK_3        = 'doc_pack_3';
    /**
     * @var - идентификатор заявки
     */
    public $request_id;
    /**
     * @var - идентификатор файла
     */
    public $file_id;
    /**
     * @var - привязка файла, варианты
     *      фото: 'foto_...'
     *      документы: 'doc_pack_..'
     */
    public $file_bind;
    /**
     * @var array - массив привязки файлов см. в текущей таблице в поле file_bind
     */
    protected $listFileBind = [
        self::BIND_FOTO_STS,
        self::BIND_FOTO_PTS,
        self::BIND_FOTO_AUTO,
        self::BIND_FOTO_PASSPORT,
        self::BIND_FOTO_CLIENT,
        self::BIND_FOTO_EXTRA,
        self::BIND_FOTO_CARD,
        self::BIND_FOTO_NOTIFICATION,
        self::BIND_DOC_PACK_1,
        self::BIND_DOC_PACK_2,
        self::BIND_DOC_PACK_3,
    ];

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{request_file}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "request_id",
            "file_id",
            "file_bind",
            "created_at",
            "updated_at",
            "active",
            "sort",
        ];
    }

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            [
                ['request_id', 'file_id', 'file_bind'],
                'required',
                'message' => 'Поле обязательно для заполнения',
            ],
            [
                'request_id',
                'number',
                'message' => 'Некорректно указан идентификатор заявки',
            ],
            [
                'file_id',
                'number',
                'message' => 'Некорректно указан идентификатор файла',
            ],
            [
                'file_bind',
                'in',
                'range'   => $this->getListFileBind(),
                'message' => 'Некорректно указана привязка файла к заявке',
            ],
        ];
    }

    /**
     * Метод получения списка привязок к файлу
     *
     * @return array - вернет массив список привязок к файлу
     */
    public function getListFileBind(): array
    {
        return $this->listFileBind;
    }

    /**
     * Метод получения данных по идентификатору заявки
     *
     * @param       $requestId - идентификатор заявки
     * @param array $fields    - поля для выборки
     *
     * @return array|\Exception - веренет результрующий массив
     *                          при выбросе исключения вернет объект
     */
    public function getByRequestId($requestId, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[request_id]] =:requestId',
                    [':requestId' => $requestId]
                )->queryAll();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод создания записи связи заявки с файлом
     *
     * @return int|string - вернет кол-во созданных записей или строку с исключением
     * @throws \Exception
     */
    public function create()
    {
        try {
            $insert               = $this->getAttributes();
            $insert['created_at'] = new Expression('NOW()');

            return Yii::$app->db->createCommand()
                ->insert($this->getTableName(), $insert)
                ->execute();
        } catch (Exception $e) {
            throw new \Exception(
                'Произошла ошибка при создании записи связи заявки с файлом, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод удаления записи связи заявки с файлом по идентификатору файла
     *
     * @param $fileId - идентификатор файла
     *
     * @return int|string - вернет кол-во удаленных строк или строку с исключением
     */
    public function deleteByFileId($fileId)
    {
        try {
            return Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), 'file_id = :fileId', [':fileId' => $fileId])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}