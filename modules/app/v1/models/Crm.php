<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

/**
 * Class Crm - модель для работы уведомлениями из CRM
 *
 * @package app\modules\app\v1\models
 */
class Crm extends Model {
	/*
	 *  guid заявки
	 */
	public $guid;
	/*
	 * Поле "сообщение"
	 */
	public $message;

	public function rules() {

		return [
			[
				'guid',
				'required',
				'message' => 'Не указан номер заявки.',
			],
			[
				'guid',
				'match',
				'pattern' => '/^(([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12})$/',
				'message' => 'Указан некорректный GUID заявки',
			],
			[
				'message',
				'string',
				'min'  =>  10 ,
				'tooShort' => 'Слишком коротккое сообщение (<10).',
			],
			[
				'message',
				'string',
				'max'  => 255 ,
				'tooLong' => 'Слишком длинное сообщение (>255).',
			],
		];
	}

}