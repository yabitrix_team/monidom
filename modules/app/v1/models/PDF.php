<?php

namespace app\modules\app\v1\models;

use yii\base\Model;
use kartik\mpdf\Pdf as mPDF;

/**
 * Class PDF - модель для работы с PDF файлам
 *
 * @package app\modules\app\v1\models
 */
class PDF extends Model {
	public $content;
	public $title;
	public $style;
	public $format;
	public $orientation;
	public $destination;

	/**
	 * @return array - правила валидации передаваемых данных
	 */
	public function rules() {

		return [
			[ 'content', 'required', 'message' => 'Не указан контент документа, обратитесь к администратору' ],
			[ 'title', 'required', 'message' => 'Не указан заголовок документа, обратитесь к администратору' ],
			[ 'content', 'string', 'message' => 'Некорретно формат контент документа, обратитесь к администратору' ],
			[
				'title',
				'string',
				'max'     => 255,
				'tooLong' => 'Превышена длина заголовка',
				'message' => 'Некорретно формат заголовок документа, обратитесь к администратору',
			],
			[ 'style', 'string', 'message' => 'Не верный формат CSS, обратитесь к администратору' ],
			[ 'format', 'default', 'value' => 'A4' ],
			[
				'format',
				'in',
				'range'   => [ 'A4', 'A3', 'Letter', 'Legal', 'Folio', 'Ledger-L', 'Tabloid' ],
				'message' => 'Некорректно указан формат страницы, обратитесь к администратору',
			],
			[ 'orientation', 'default', 'value' => 'P' ],
			[
				'orientation',
				'in',
				'range'   => [ 'P', 'L' ],
				'message' => 'Некорректно указана ориентация страницы, обратитесь к администратору',
			],
			[ 'destination', 'default', 'value' => 'D' ],
			[
				'destination',
				'in',
				'range'   => [ 'I', 'D', 'F', 'S' ],
				'message' => 'Некорректно указан поток вывода, обратитесь к администратору',
			],
		];
	}

	/**
	 * метод генерации pdf
	 *
	 * @return $this|mixed|string
	 */
	public function generate() {

		try {
			if ( ! $this->validate() ) {
				return $this;
			}
			// setup kartik\mpdf\Pdf component
			$pdf = new mPDF( [
				                 // set to use core fonts only
				                 'mode'        => mPDF::MODE_BLANK,
				                 // A4 paper format
				                 'format'      => $this->format,
				                 // portrait orientation
				                 'orientation' => $this->orientation,
				                 // stream to browser inline
				                 'destination' => $this->destination,
				                 // your html content input
				                 'content'     => $this->content,
				                 // format content from your own css file if needed or use the
				                 // enhanced bootstrap css built by Krajee for mPDF formatting
				                 //'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
				                 // any css to be embedded if required
				                 'cssInline'   => $this->style,
				                 // set mPDF properties on the fly
				                 'options'     => [ 'title' => $this->title ],
				                 // call mPDF methods on the fly
				                 'methods'     => [
					                 //    'SetHeader' => [ 'Report Header' ],
					                 'SetFooter' => [ '{PAGENO}' ],
				                 ],
			                 ] );

			return $pdf->output( $this->content, $this->title . '.pdf', $this->destination );
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}