<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;
use yii\base\Model;

/**
 * Class MPVersion - модель для работы с таблицей версий МП
 *
 * @package app\modules\app\v1\models
 */
class MPVersion extends Model
{
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;

    /**
     * Получение названия таблицы модели в БД.
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{mp_version}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'os_type',
            'minimal',
            'current',
            'updated_at',
            'created_at',
        ];
    }

    /**
     * Метод получения версий МП
     *
     * @return array - вернет результирующий массив
     * @throws \yii\db\Exception
     */
    public function getVersions()
    {
        $result = [];
        $data   = Yii::$app->db->createCommand(
            'SELECT [[os_type]], [[minimal]], [[current]]
            FROM '.$this->getTableName()
        )->queryAll();
        if (is_array($data)) {
            foreach ($data as $item) {
                $result[$item['os_type']] = [
                    'version_cur' => $item['current'],
                    'version_min' => $item['minimal'],
                ];
            }
        }

        return $result;
    }
}