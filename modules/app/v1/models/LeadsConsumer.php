<?php
namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

/**
 * Class LeadsConsumer - модель по работе с потребителями лидов
 *
 * @package app\modules\app\v1\models
 */
class LeadsConsumer extends Model
{
    /**
     * Константы содержат имена потребителей
     */
    CONST MOBILE = 'mobile';
    CONST CLIENT = 'client';

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{leads_consumer}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "name",
            "identifier",
            "requests_origin_id",
            "sms_text",
            "active",
            "updated_at",
            "created_at",
        ];
    }

    /**
     * Метод получения данных по идентификатору
     *
     * @param       $id     - идентификатор
     * @param array $fields - поля для выборки
     *
     * @return array|\Exception|false|mixed  - вернет результирующий массив при выборе нескольких полей
     *                                       вернет значение при выборе одного поля
     *                                       вернет ложь если не нашел запись
     *                                      при выбросе исключения вернет объект исключения
     */
    public function getOne($id, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $method = count($fields) == 1 ? 'queryScalar' : 'queryOne';
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[id]] =:id',
                    [':id' => $id]
                )->$method();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения данных по символьному идентификатору
     *
     * @param       $identifier - символьный идентификатор
     * @param array $fields     - поля для выборки
     *
     * @return array|\Exception|false|mixed  - вернет результирующий массив при выборе нескольких полей
     *                                       вернет значение при выборе одного поля
     *                                       вернет ложь если не нашел запись
     *                                      при выбросе исключения вернет объект исключения
     */
    public function getByIdentifier($identifier, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $method = count($fields) == 1 ? 'queryScalar' : 'queryOne';
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[identifier]] =:identifier',
                    [':identifier' => $identifier]
                )->$method();
        } catch (\Exception $e) {
            return $e;
        }
    }
}