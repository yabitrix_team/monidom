<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

class Partners extends Model {
	use WritableTrait;
	use ReadableTrait;

	/*
	 * получение партнера по точке
	 */
	public function getByPointId( int $pointId ) {

		if ( empty( $pointId ) ) {
			throw new \Exception( 'Не указан идентификатор точки' );
		}

		return Yii::$app->db
			->createCommand( 'SELECT 
[[po]].[[id]] as point_id, 
[[po]].[[NAME]] as point_name, 
[[pa]].[[id]] as partner_id, 
[[pa]].[[NAME]] as partner_name,
[[pa]].[[comission_percentage]] as comission
FROM [[points]] AS [[po]]
LEFT JOIN [[partners]] AS [[pa]] ON [[po]].[[partner_id]] = [[pa]].[[id]]
WHERE [[po]].[[id]] = :point_id', [ 'point_id' => $pointId ] )
			->queryOne();
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"comission_percentage",
			"comission_summ",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{partners}}";
	}
}