<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class AuthToken - модель для работы с токенами авторизации пользователя
 *
 * @package app\modules\app\v1\models
 */
class AuthToken extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * @var - идентификатор пользователя
     */
    public $user_id;
    /**
     * @var - время жизни токена авторизации
     */
    public $expired_at;
    /**
     * @var string - тег для создания кэштега, по которому можно комплексно удалить кэш для всех пользователей
     *             должен совпадать со значением в моделе Users
     */
    protected static $tag = 'users';

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {

        return [
            [
                'user_id',
                'required',
                'message' => 'Не указан идентификатор пользователя',
            ],
            [
                'user_id',
                'number',
                'message' => 'Идентификатор пользователя указан в некорреткном формате',
            ],
            [
                'expired_at',
                'number',
                'message' => 'Время жизни токена указано в некорреткном формате',
            ],
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return "{{auth_token}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {

        return [
            "id",
            "user_id",
            "token",
            "expired_at",
            "updated_at",
            "created_at",
        ];
    }

    /**
     * Метод сохранения записи токена в таблице
     *
     * @return $this|int|string - в случае успеха вернет токен авторизации,
     *                          либо объект, если валидация не прошла, доступ к ошибкам $this->firstErrors
     *                          либо строку, если выброшено исключение
     */
    public function save()
    {

        try {
            if ($this->validate()) {
                $tokenExpired = Yii::$app->params['mp']['settings']['tokenExpired'];
                if (
                    empty($this->expired_at)
                    && (empty($tokenExpired) || !is_numeric($tokenExpired))
                ) {

                    throw new \Exception(
                        'При попытке записи токена авторизации пользователя произошла ошибка, в настройках не указан параметр врменени жизни токена, обратитесь к администратору'
                    );
                }
                $expired_at           = empty($this->expired_at) ? time() + $tokenExpired : $this->expired_at;
                $token                = Yii::$app->security->generateRandomString();
                $insert               = $update = [
                    'user_id'    => $this->user_id,
                    'token'      => $token,
                    'expired_at' => $expired_at,
                ];
                $update['updated_at'] = new Expression('NOW()');
                $res                  = Yii::$app->db->createCommand()
                    ->upsert($this->getTableName(), $insert, $update)
                    ->execute();

                return is_numeric($res) ? $token : false;
            } else {
                return $this;
            }
        } catch (\Exception $e) {
            //todo[echmaster]: 24.04.2018 добавить логирование
            return $e->getMessage();
        }
    }

    /**
     * Метод удаления записи по идентификатору пользователя
     *
     * @param $userId - идентификатор пользователя
     *
     * @return bool - вернет истину в случае успешного удаления, иначе ложь
     * @throws \Exception
     */
    public function deleteByUserId($userId)
    {
        try {
            $result = Yii::$app->db
                ->createCommand()
                ->delete($this->getTableName(), ['user_id' => $userId])
                ->execute();

            return boolval($result);
        } catch (Exception $e) {
            throw new \Exception('Ошикбка в базе данных при деавторизации пользователя, обратитесь к администратору');
        }
    }

    /**
     * Метод удаления записи о токене из базы и из кэша по токену
     *
     * @param bool $token - токен авторизованного пользователя
     *
     * @return int|string - вернет кол-во удаленных строк или строку с исключением
     */
    public function deleteByToken($token = false)
    {

        try {
            $token    = !empty($token) ? $token : self::getTokenInHeader();
            $cache    = Yii::$app->cache;
            $cacheTag = md5(Yii::getAlias('@app').self::$tag);
            $cacheKey = md5($cacheTag.'_'.$token);
            $cache->delete($cacheKey);

            return Yii::$app->db->createCommand()
                ->delete($this->getTableName(), 'token = :token', [':token' => $token])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения токена авторизованного польователя из заголовков
     *
     * @return mixed - вернет токен для авторизации
     */
    public static function getTokenInHeader()
    {

        $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
        preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches);

        return $matches[1];
    }

    /**
     * Метод получения данных по токену
     *
     * @param       $token  - авторизационный токен пользователя
     * @param array $fields - массив полей для выборки
     *
     * @return array|false|string - вернет массив выбранных полей либо ложь
     *                            либо строку с исключением
     */
    public function getByToken($token, array $fields = [])
    {

        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE [[token]] =:token',
                    [':token' => $token]
                )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения секретного ключа из заголовка
     *
     * @return mixed - вернет секретный ключ
     */
    public static function getAccessKeyInHeader()
    {

        $authHeader = Yii::$app->request->getHeaders()->get('Access-Key');
        preg_match('/^(.*?)$/', $authHeader, $matches);

        return $matches[1];
    }

    /**
     * Метод проверки секретного ключа
     *
     * @return bool - вернет истину в случае успеха
     */
    public static function checkAccessKey()
    {

        $accessKey = Yii::$app->params['mp']['settings']['accessKey'];
        if (empty($accessKey)) {
            throw new \InvalidArgumentException(
                'В конфигурациях для мобильного приложенеия не указана ключ доступа, обратитесь к администратору'
            );
        }
        $keyHeader = self::getAccessKeyInHeader();
        if (empty($keyHeader)) {
            throw new \InvalidArgumentException('В параметрах запроса не указан ключ доступа');
        }
        if ($keyHeader !== $accessKey) {
            throw new \InvalidArgumentException('В параметрах запроса некорректно указан ключ доступа');
        }

        return true;
    }
}