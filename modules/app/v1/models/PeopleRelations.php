<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class PeopleRelations extends Model {
	/**
	 * Метод получения всех пунктов Кем приходится
	 *
	 * @param bool $active - Параметр отвечающий за получение всех пунктов Кем приходится,
	 *                     в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' WHERE `active` = 0 ' : ' WHERE `active` = 1 ' );

		return Yii::$app->db->createCommand( 'SELECT 
`id`, 
`guid`, 
`name`, 
`active`, 
`sort` 
FROM people_relations ' . $active )
		                    ->queryAll();
	}

	/**
	 * Метод получения пункта Кем приходится по идентификатору
	 *
	 * @param $id - Идентификатор пункта Кем приходится
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "SELECT `guid`, `name`, `active` FROM people_relations WHERE `id`=:id", [ ':id' => $id ] )
			->queryOne();
	}
}