<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Products - модель для работы со справочником Кредитные продукты
 *
 * @package app\modules\app\v1\models
 */
class Products extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{products}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"is_additional_loan",
			"percent_per_day",
			"month_num",
			"summ_min",
			"summ_max",
			"age_min",
			"age_max",
			"loan_period_min",
			"loan_period_max",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	/**
	 * Метод получения данных кредитного продукта по его ГУИД
	 *
	 * @param       $guid   - ГУИД кредитного продукта
	 * @param array $fields - массив полей, данные которых нужно получить
	 *
	 * @return \Exception|array|mixed - если указано более одного поля, то вернет массив
	 *                    если указано одно поле, то вернет его значение
	 *                    если возникла ошибка, то вернет объект исключения
	 */
	public function getDataByGuid( $guid, array $fields = [] ) {

		try {
			//подготавливаем поля для получения данных
			$prepareFields = array_intersect( $fields, $this->getFieldsNames() );
			$fields        = ! empty( $prepareFields ) ? $prepareFields : $this->getFieldsNames();
			$strFields     = '[[' . implode( "]],[[", $fields ) . ']]';
			//указываем метод для получения данных в зависимости от кол-ва полей
			$method = count( $fields ) == 1 ? 'queryScalar' : 'queryAll';

			return Yii::$app->db->createCommand(
				'SELECT ' . $strFields . ' FROM ' . $this->getTableName() . ' WHERE [[guid]] = :guid',
				[ ':guid' => $guid ] )
			                    ->$method();
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}