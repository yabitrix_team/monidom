<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Платежная система
 *
 * @package app\modules\payment\v1\models\base
 */
class Document extends Model
{
    use WritableTrait;
    use ReadableTrait;

    /**
     * @var int ID
     */
//    public $id;
    /**
     * @var string уникальный hash для адреса ссылки авторизации
     */
    public $code;
    /**
     * @var string ID пользователя, под которым будет авторизация
     */
    public $label;
    /**
     * @var string html
     */
    public $html;


    public function getAll()
    {


        return Yii::$app->db->createCommand('SELECT
            `id`,
            `code`,
            `label`,
            `html`
        FROM `documents` ')->queryAll();
    }

    public function create()
    {
        $params = $this->getAttributes();
        $insert = Yii::$app->db
            ->createCommand()
            ->insert('documents', $params)->execute();

        return !empty($insert) ? Yii::$app->db->lastInsertID : false;
    }

    public function getByCode( string $code = null ) {

        if ($code === null) {
            $code = $this->getAttributes()['code'];
        }

        if ( ! strlen( $code ) ) {
            return false;
        }

        return Yii::$app->db
            ->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . " where [[code]]=:code", [ ":code" => $code ] )
            ->queryOne();
    }

    public function update( $condition, array $fields = [] ) {

        if ( empty( $condition ) || empty( $fields ) ) {
            return false;
        }

        return Yii::$app->db
            ->createCommand()
            ->update( $this->getTableName(), $fields, $condition )
            ->execute();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['code', 'label'], 'required'],
            [['html'], 'string']
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     *
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return "{{documents}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {

        return [
            "code",
            "label",
            "html",
        ];
    }
}
