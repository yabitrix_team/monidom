<?php
namespace app\modules\app\v1\models\soap\crm;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\EventsCrm;
use app\modules\app\v1\validators\PhoneValidator;
use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class EventsLoad - модель взаимодействия по протоколу SOAP с удаленным сервисом "Обмен событиями по заявкам"
 *
 * @package app\modules\app\v1\models
 */
class EventsLoad extends Model
{
    /**
     * Константа сценария получение события по заявке
     */
    const SCENARIO_EVENTS_LOAD_BIT = 'EVENTS_LOAD_BIT';
    /**
     * Константа сценария получение события по клиенту
     */
    const SCENARIO_EVENTS_LOAD_CLIENT = 'EVENTS_LOAD_CLIENT';
    /**
     * @var - ГУИД события
     */
    public $guid_event;
    /**
     * @var - ГУИД заявки
     */
    public $guid_request;
    /**
     * @var - Логин пользователя в виде номера телефона
     */
    public $username;
    /**
     * @var - дополнительная информация по событию
     */
    public $param;
    /**
     * @var string - название сервиса, к которому обращаемся
     */
    protected $service = 'EventsLoad';
    /**
     * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
     */
    protected $soapCRM;

    /**
     * Метод инициализации модели
     */
    public function init()
    {
        parent::init();
        $this->soapCRM = Yii::$app->soapCRM->getInstance($this->service);
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {
        return [
            [
                'guid_event',
                'required',
                'message' => 'Не указан ГИУД события, обратитесь к администратору',
            ],
            [
                'guid_request',
                'required',
                'message' => 'Не указан ГИУД заявки, обратитесь к администратору',
            ],
            [
                'username',
                'required',
                'message' => 'Не указан логин пользователя в виде номера телефона, обратитесь к администратору',
            ],
            [
                'guid_event',
                'string',
                'message' => 'ГИУД события указан в некорректном формате, обратитесь к администратору',
            ],
            [
                'guid_request',
                'string',
                'message' => 'ГИУД заявки указан в некорректном формате, обратитесь к администратору',
            ],
            [
                'username',
                PhoneValidator::class,
                'message' => 'Логин пользователя в виде номера телефона указан в некорректном формате, обратитесь к администратору',
            ],
            [
                'param',
                'default',
                'value' => '',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                                 = parent::scenarios();
        $scenarios[self::SCENARIO_EVENTS_LOAD_BIT] = $scenarios[self::SCENARIO_EVENTS_LOAD_CLIENT] = [
            'guid_event',
            'guid_request',
            'username',
            'param',
        ];

        return $scenarios;
    }

    /**
     * Метод получения события по заявке
     *
     * @return $this|bool|string - вернет истину в случае успешного получения события,
     *                           вернет текущий объект в случае ошибки валидации
     * @throws \Exception
     */
    public function eventsLoadBit()
    {
        return $this->eventsLoad(__FUNCTION__);
    }

    /**
     * Метод получения события по клиенту
     *
     * @return $this|bool|string - вернет истину в случае успешного получения события,
     *                           вернет текущий объект в случае ошибки валидации
     * @throws \Exception
     */
    public function eventsLoadClient()
    {
        return $this->eventsLoad(__FUNCTION__);
    }

    /**
     * Метод содержащий одинаковую логику для сервисов EventsLoadBit и EventsLoadClient
     *
     * @param string $functionService - название функции в удаленном сервисе
     *                                пример EventsLoadBit
     *
     * @return $this|bool - в случае успеха вернет истину
     *                               в случае ошибки валидации вернет текущий объект
     * @throws \Exception
     */
    protected function eventsLoad(string $functionService)
    {
        if (!$this->validate()) {
            throw new \Exception('Ошибка валидации данных при отправке события в CRM');
        }
        if (is_a($this->soapCRM, \Exception::class)) {
            throw new \Exception('Ошибка подключения к сервису ЦРМ, обратитесь к администратору');
        }
        $functionService   = ucfirst($functionService);
        $soapData['Event'] = [
            'Name'        => $this->guid_event,
            'ID'          => $this->guid_request,
            'PhoneNumber' => $this->username,
            'DateTime'    => date("Y-m-d")."T".date("H:i:s"),
            'Param'       => $this->param,
        ];
        $res               = $this->soapCRM->send($functionService, $soapData);
        if (is_a($res, SoapFault::class)) {
            throw new \Exception(
                'Возникла неизвестная ошибка при передаче события по заявке в сервис '.
                $functionService.
                ', обратитесь к администратору'
            );
        }
        if (is_a($res, \Exception::class)) {
            throw new \Exception(
                'Ошибка передачи события по заявке в сервис '.$functionService.', обратитесь к администратору'
            );
        }
        if (!isset($res->ErrorData) || !isset($res->ErrorData->ID)) {
            throw new \Exception(
                'При попытке передачи события по заявке в сервис '.
                $functionService.
                ' был нарушен контракт, обратитесь к администратору'
            );
        }
        $errors = [
            1         => 'При передачи события по заявке в сервис '.$functionService.' не удалось найти событие',
            2         => 'При передачи события по заявке в сервис '.$functionService.' не удалось найти заявку',
            3         => 'При передачи события по заявке в сервис '.
                         $functionService.' произошла ошибка заяписи события в регистр',
            1000      => 'При передачи события по заявке в сервис '.
                         $functionService.' был некорректно указан номер телефона (менее 10 символов)',
            'default' => 'При передачи события по заявке в сервис '.
                         $functionService.' произошла неизвестная ошибка',
        ];
        if ($res->ErrorData->ID != 0) {
            $key = key_exists($res->ErrorData->ID, $errors) ? $res->ErrorData->ID : 'default';
            throw new \Exception($errors[$key]);
        }

        return true;
    }

    /**
     * Метод отправки события в CRM по коду события с учетом логирования
     *
     * @param        $codeEvent   - код события
     * @param        $guidRequest - ГУИД заявки
     * @param        $username    - номер телефона клиента (логин)
     * @param string $param       - дополнительные параметры
     *
     * @return bool - в случае успешной отправки true иначе false
     */
    public static function sendEventLoadBit($codeEvent, $guidRequest, $username, $param = '')
    {
        $msg = "*** Сервис: EventsLoadBit *** \n";
        try {
            $eventCrm = (new EventsCrm())->getOneByFilter(['code' => $codeEvent]);
            if (empty($eventCrm)) {
                throw new \Exception(
                    'При попытке отправки события в CRM был неверно указан код события, обратитесь к администратору'
                );
            }
            $eventsLoad = new self(['scenario' => EventsLoad::SCENARIO_EVENTS_LOAD_BIT]);
            $eventsLoad->load(
                [
                    'guid_event'   => $eventCrm["guid"],
                    'guid_request' => $guidRequest,
                    'username'     => $username,
                    'param'        => $param,
                ],
                ''
            );
            $eventsLoad->eventsLoadBit();
            $msg .= "Отправка события в CRM прошла успешно! \n";

            return true;
        } catch (\Exception $e) {
            $msg .= "Текст ошибки: {$e->getMessage()} \n";

            return false;
        } finally {
            if (!empty($eventCrm) && is_array($eventCrm)) {
                $msg .= <<<TXT
Название события: {$eventCrm['name']}
ГУИД события: {$eventCrm['guid']} \n
TXT;
            }
            $msg .= <<<TXT
ГУИД заявки: $guidRequest
Номер телефона: $username \n
TXT;
            if (!empty($param)) {
                $msg .= "Параметры: $param";
            }
            Log::info(['text' => $msg, 'user_login' => $username], 'crm.events');
        }
    }
}