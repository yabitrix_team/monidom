<?php

namespace app\modules\app\v1\models\soap\crm;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class LeadsLoad -  модель взаимодействия по протоколу SOAP с удаленным сервисом "Создание лида в CRM"
 *
 * @package app\modules\app\v1\models
 */
class LeadsLoad extends Model {
	/**
	 * Константа сценария загрузки лида
	 */
	const SCENARIO_LOAD_LEAD = 'LOAD_LEAD';
	/**
	 * @var string номер телефона
	 */
	public $phone;
	/**
	 * @var string канал привлечения (ФИО агента лидогенератора)
	 */
	public $channel;
	/**
	 * @var string имя
	 */
	public $first_name;
	/**
	 * @var string фамилия
	 */
	public $last_name;
	/**
	 * @var string отчество
	 */
	public $second_name;
	/**
	 * @var string дата рождения
	 */
	public $birthday;
	/**
	 * @var string регион проживания
	 */
	public $region;
	/**
	 * @var string сумма займа
	 */
	public $sum;
	/**
	 * @var string бренд авто
	 */
	public $auto_brand;
	/**
	 * @var string модель авто
	 */
	public $auto_model;
	/**
	 * @var string паспорт серия
	 */
	public $passport_serial;
	/**
	 * @var string паспорт номер
	 */
	public $passport_number;
	/**
	 * @var string комментарий
	 */
	public $comment;
	/**
	 * @var string - название сервиса, к которому обращаемся
	 */
	protected $service = 'LeadsLoad';
	/**
	 * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
	 */
	protected $soapCRM;

	/**
	 * Метод инициализации модели
	 */
	public function init() {

		parent::init();
		$this->soapCRM = Yii::$app->soapCRM->getInstance( $this->service );
	}

	/**
	 * Правила валидации
	 *
	 * @return array - вернет массив правил валидации
	 */
	public function rules() {

		return [
			[
				'phone',
				'required',
				'message' => 'Не указан номер телефона',
			],
			[
				'channel',
				'required',
				'message' => 'Не указан канал привлечения',
			],
			[
				'birthday',
				'filter',
				'filter' => function( $value ) {

					if ( ! empty( $value ) ) {
						//для формата  2018-01-22T00:00:00.000Z
						if ( preg_match( "/^(\d{4})\-(\d{2})\-(\d{2})/", $value, $mDate ) ) {
							return $mDate[1] . $mDate[2] . $mDate[3];
						}
					}

					return null;
				},
			],
			[
				'sum',
				'filter',
				'filter' => function( $value ) {

					return preg_replace( "/\s+/s", "", $value );
				},
			],
		];
	}

	/**
	 * Метод установки сценария для модели при валидации полей
	 *
	 * @return array - вернет массив сценариев,
	 * по умолчанию установлен в сценарии установлен DEFAULT
	 */
	public function scenarios() {

		$scenarios                             = parent::scenarios();
		$scenarios[ self::SCENARIO_LOAD_LEAD ] = [
			'phone',
			'channel',
			'first_name',
			'last_name',
			'second_name',
			'birthday',
			'region',
			'sum',
			'auto_brand',
			'auto_model',
			'passport_serial',
			'passport_number',
			'comment',
		];

		return $scenarios;
	}

	/**
	 * Метод загрузки лида в CRM
	 *
	 * @return $this|mixed|string - вернет объект(stdClass) полученный при обращении к сервису,
	 *                              {
	 *                              }
	 *                            в случае ошибки валидации вернет текущий объект
	 *                            в случае исключения строку с описанием ошибки
	 */
	public function loadLead() {

		try {
			if ( ! $this->validate() ) {
				return $this;
			}
			if ( is_a( $this->soapCRM, \Exception::class ) ) {
				throw new \Exception( 'Ошибка подключения к сервису создания лида, обратитесь к администратору' );
			}
			$arLead = [
				'LeadData' => [
					'PhoneNumber'          => $this->phone,
					'Chanel'               => $this->channel,
					'Name'                 => $this->first_name,
					'Surname'              => $this->last_name,
					'PatronymicName'       => $this->second_name,
					'DateOfBirth'          => $this->birthday,
					'RegionOfRegistration' => $this->region,
					'LoanSumm'             => $this->sum,
					'CarBrand'             => $this->auto_brand,
					'CarModel'             => $this->auto_model,
					'PassportSeries'       => $this->passport_serial,
					'PassportNumber'       => $this->passport_number,
					'Comment'              => $this->comment,
				],
			];
			Log::info( [ "text" => print_r( $arLead, true ) ], "crm.lead.new" );
			$res = $this->soapCRM->send( 'LoadLead', $arLead );
			if ( is_a( $res, SoapFault::class ) ) {
				throw new \Exception( 'Возникла неизвестная ошибка при создании лида, обратитесь к администратору' );
			}
			if ( is_a( $res, \Exception::class ) ) {
				throw new \Exception( 'Ошибка создания лида, обратитесь к администратору' );
			}
			if ( ! isset( $res->ErrorData ) || ! isset( $res->ErrorData->ID ) ) {
				throw new \Exception( 'При попытке создания лида был нарушен контракт между сервисами, обратитесь к администратору' );
			}
			$errors = [
				1000      => 'Передан короткий номер телефона (меньше 10 символов)',
				2000      => 'Не удалось создать лид в CRM',
				'default' => 'Произошла неизвестная ошибка при создании лида',
			];
			if ( $res->ErrorData->ID != 0 ) {
				$key = key_exists( $res->ErrorData->ID, $errors ) ? $res->ErrorData->ID : 'default';
				throw new \Exception( $errors[ $key ] );
			}
			unset( $res->ErrorData );

			return $res;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}