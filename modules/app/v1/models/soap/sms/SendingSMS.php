<?php
namespace app\modules\app\v1\models\soap\sms;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\validators\PhoneValidator;
use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class SendingSMS - модель взаимодействия по протоколу SOAP с удаленным сервисом "Отправка СМС"
 *
 * @package app\modules\app\v1\models\soap\sms
 */
class SendingSMS extends Model
{
    /**
     * Константа сценария получение статуса СМС
     */
    const SCENARIO_GET_STATUS = 'GET_STATUS';
    /**
     * Константа сценария отправка простой СМС по номеру телефона
     */
    const SCENARIO_SEND_DATA = 'SEND_DATA';
    /**
     * @var string - строковый идентификатор СМС сообщения полученный при отпрваке
     */
    public $sms_id;
    /**
     * @var - номер телефона для отправки СМС сообщения
     */
    public $phone;
    /**
     * @var string - текст СМС сообщения
     */
    public $message;
    /**
     * @var string - название сервиса, к которому обращаемся
     */
    protected $service = 'SendingSMS';
    /**
     * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
     */
    protected $soapSMS;
    /**
     * @var - категория лога
     */
    protected $logCategory;
    /**
     * @var array - массив статусов СМС сообщений, на данный момент указаны статусы сервиса REDSMS
     *            в дальнейшем буду подключены доп. сервисы, если не примут решение контролировать
     *            контракт на уровне удаленного сервиса "Отправка СМС", то необходимо будет расширять
     */
    protected static $statusesSMS = [
        //---- статусы сервиса REDSMS -----
        'send'        => 'Отправлено',
        'delivered'   => 'Доставлено',
        'not_deliver' => 'Не доставлено',
        //---- статусы сервиса REDSMS+viber -----
        'created'     => 'Ожидает отправки',
        'moderation'  => 'На модерации',
        'reject'      => 'Запрещено модерацией',
        'delivered'   => 'Доставлено',
        'read'        => 'Прочитано',
        'reply'       => 'Есть ответ',
        'undelivered' => 'Не доставлено',
        'timeout'     => 'Просрочено',
        'progress'    => 'В процессе',
        'no_money'    => 'Недостаточно средств',
        'doubled'     => 'Дублирование',
        'bad_number'  => 'Неверный номер',
        'stop_list'   => 'Запрещено стоп листом',
        'error'       => 'Ошибка',
    ];

    /**
     * Метод инициализации модели
     */
    public function init()
    {

        parent::init();
        $this->soapSMS = Yii::$app->soapSMS->getInstance($this->service);
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {

        return [
            [
                'sms_id',
                'required',
                'message' => 'Не указана строковый идентификатор СМС сообщения',
            ],
            [
                'phone',
                'required',
                'message' => 'Не указан номер телефона для отправки СМС сообщения',
            ],
            [
                'message',
                'required',
                'message' => 'Не указан текст для СМС сообщения',
            ],
            [
                ['sms_id', 'phone', 'message'],
                'trim',
            ],
            [
                'sms_id',
                'string',
                'message' => 'Строковый идентификатор СМС сообщения указан в некорректном формате',
            ],
            [
                'phone',
                PhoneValidator::class,
                'message' => 'Некорректно указан номер телефона для отправки СМС сообщения',
            ],
            [
                'message',
                'string',
                'message' => 'Текст СМС сообщения указан в некорректном формате',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен сценарий DEFAULT
     */
    public function scenarios()
    {

        $scenarios                            = parent::scenarios();
        $scenarios[self::SCENARIO_GET_STATUS] = ['sms_id'];
        $scenarios[self::SCENARIO_SEND_DATA]  = ['phone', 'message'];

        return $scenarios;
    }

    /**
     * Метод получения статуса СМС сообщения
     *
     * @return $this|string|\Exception - вернет результат отправки в виде строки send|deliver|not_deliver
     *                                 текущий объект если произошла ошибка валидации
     *                                 объект исключения, если оно было выброшено
     */
    public function getStatus()
    {

        try {
            if (!$this->validate()) {
                return $this;
            }
            if (is_a($this->soapSMS, \Exception::class)) {
                throw new \Exception('Ошибка подключения к сервису Отправка СМС, обратитесь к администратору');
            }
            $res = $this->soapSMS->send(
                'GetStatus',
                [
                    'SmsId' => $this->sms_id,
                ]
            );
            if (is_a($res, SoapFault::class)) {
                throw new \Exception(
                    'Возникла неизвестная ошибка при получении статуса СМС сообщения, обратитесь к администратору'
                );
            }
            if (is_a($res, \Exception::class)) {
                throw new \Exception('Ошибка поулчения статуса СМС сообщения, обратитесь к администратору');
            }
            if (!isset($res->ErrorData) || !isset($res->ErrorData->ID) || !isset($res->ProviderCode)) {
                throw new \Exception(
                    'При попытке получения статуса СМС сообщения был нарушен контракт между сервисами, обратитесь к администратору'
                );
            }
            $errors = [
                -1        => 'При получении статуса СМС сообщения возникла неопределенная ошибка, обратитесь к администратору',
                505       => 'При попытке получениия статуса СМС был некорректно указан идентификатор СМС, обратитесь к администратору',
                506       => 'При попытке получениия статуса СМС не удалось найти СМС с указанным идентификатором, обратитесь к администратору',
                'default' => 'Произошла неизвестная ошибка при получении статуса СМС сообщения, обратитесь к администратору',
            ];
            if ($res->ErrorData->ID != 1) {
                $key = key_exists($res->ErrorData->ID, $errors) ? $res->ErrorData->ID : 'default';
                throw new \Exception($errors[$key]);
            }

            return $res->ProviderCode;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод отправки простого СМС сообщения по номеру телефона
     *
     * @return $this|\Exception|string - вернет строку содержащую идентификатор СМС
     *                                 текущий объект если произошла ошибка валидации
     *                                 объект исключения, если оно было выброшено
     */
    public function sendData()
    {
        try {
            if (!$this->validate()) {
                throw new \Exception(
                    'Произошла ошибка валидации данных при отправке СМС-сообщения, обратитесь к администратору.'
                );
            }
            if (is_a($this->soapSMS, \Exception::class)) {
                throw new \Exception('Ошибка подключения к сервису Отправка СМС, обратитесь к администратору');
            }
            $res = $this->soapSMS->send(
                'SendData',
                [
                    'Phone'        => $this->phone,
                    'Message'      => $this->message,
                    'TestLogin'    => '',
                    'TestPassword' => '',
                ]
            );
            if (is_a($res, SoapFault::class)) {
                throw new \Exception(
                    'Возникла неизвестная ошибка при отправке СМС сообщения, обратитесь к администратору'
                );
            }
            if (is_a($res, \Exception::class)) {
                throw new \Exception('Ошибка отправки СМС сообщения, обратитесь к администратору');
            }
            if (!isset($res->ErrorData) || !isset($res->ErrorData->ID) || !isset($res->SmsID)) {
                throw new \Exception(
                    'При попытке отправки СМС сообщения был нарушен контракт между сервисами, обратитесь к администратору'
                );
            }
            $errors = [
                -1        => 'При попытке отправки СМС сообщения возникла неопределенная ошибка, обратитесь к администратору',
                500       => 'Не удалось отправить СМС сообщение, так как номер телефона содержит не только цифры, обратитесь к администратору',
                501       => 'Не удалось отправить СМС сообщение, так как указан короткий номер телефона, обратитесь к администратору',
                502       => 'Не удалось отправить СМС сообщение, так как не указан текст сообщения, обратитесь к администратору',
                'default' => 'Произошла неизвестная ошибка при отправке СМС сообщения, обратитесь к администратору',
            ];
            if ($res->ErrorData->ID != 1) {
                $key = key_exists($res->ErrorData->ID, $errors) ? $res->ErrorData->ID : 'default';
                throw new \Exception($errors[$key]);
            }
            Log::info(
                [
                    'user_login' => $this->phone,
                    'text'       => 'Сообщение: '.$this->message.' | SMSID: '.$res->SmsID,
                ],
                $this->logCategory
            );

            return $res->SmsID;
        } catch (\Exception $e) {
            Log::error(
                [
                    'user_login' => $this->phone,
                    'text'       => 'Сообщение: '.
                                    $this->message.
                                    ' | Ответ веб-сервиса: '.
                                    print_r($this->firstErrors, 1),
                ],
                $this->logCategory
            );
            throw $e;
        }
    }

    /**
     * Метод получения статусов СМС сообщений заведенных в свойстве модели
     *
     * @return array - вернет массив
     */
    public static function getStatusesSMS(): array
    {

        return self::$statusesSMS;
    }

    /**
     * @param mixed $logCategory
     */
    public function setLogCategory($logCategory)
    {
        $this->logCategory = $logCategory;
        return $this;
    }
}