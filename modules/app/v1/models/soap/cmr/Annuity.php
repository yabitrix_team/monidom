<?php
namespace app\modules\app\v1\models\soap\cmr;

use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class Annuity -  модель взаимодействия по протоколу SOAP с удаленным сервисом "Расчет аннуитета"
 *
 * @package app\modules\app\v1\models
 */
class Annuity extends Model
{
    /**
     * Константа сценария получения расчета ануитета
     */
    const SCENARIO_SMART_ANNUITY = 'SMART_ANNUITY';
    /**
     * @var - сумма займа
     */
    public $amount;
    /**
     * @var - срок займа в формате 15.19.2020
     */
    public $date;
    /**
     * @var - идентификатор клиента, в данном случае это номер телефона
     */
    public $client;
    /**
     * @var - дополнительны параметр
     */
    public $params;
    /**
     * @var string - название сервиса, к которому обращаемся
     */
    protected $service = 'Annuity';
    /**
     * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
     */
    protected $soapCMR;

    /**
     * Метод инициализации модели
     */
    public function init()
    {

        parent::init();
        $this->soapCMR = Yii::$app->soapCMR->getInstance($this->service);
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {

        return [
            [
                'amount',
                'required',
                'message' => 'Не указана сумма займа',
            ],
            [
                'date',
                'required',
                'message' => 'Не указан срок займа',
            ],
            [
                ['amount', 'date', 'client'],
                'trim',
            ],
            [
                'amount',
                'number',
                'message'  => 'Некорректно указан формат суммы займа',
                'min'      => 1,
                'tooSmall' => 'Сумма займа не может быть меньше или равна нулю',
            ],
            [
                'date',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
                'message' => 'Некорректно указан формат даты срока займа',
            ],
            //приводим к формату: 2020-09-15
            [
                'date',
                'filter',
                'filter' => function ($v) {

                    if (!empty($v)) {
                        //для формата  2018-01-22T00:00:00.000Z
                        if (strpos($v, '-') !== false) {
                            return substr($v, 0, 10);
                        }
                        //для формата  22.01.2020
                        if (strpos($v, '.') !== false) {
                            list($d, $m, $y) = explode('.', $v);

                            return $y.'-'.$m.'-'.$d;
                        }
                    }

                    return null;
                },
            ],
            [
                'date',
                'validateDate',
            ],
            [
                'client',
                'default',
                'value' => 0,
            ],
            [
                'params',
                'safe',
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {

        $scenarios                               = parent::scenarios();
        $scenarios[self::SCENARIO_SMART_ANNUITY] = ['amount', 'date', 'client', 'params'];

        return $scenarios;
    }

    /**
     * Метод валидации для даты
     *
     * @param $attribute - атрибуты
     * @param $params
     */
    public function validateDate($attribute, $params)
    {

        if ($this->{$attribute} < date('Y-m-d')) {
            $this->addError($attribute, 'Срок займа не может быть меньше текущей даты');
        }
    }

    /**
     * Метод получения расчета ануитета
     *
     * @return $this|mixed|string - вернет объект(stdClass) полученный при обращении к сервису,
     *                              {
     *                                  "Annuity": "90055",
     *                                  "GUID": "aee632cb-5e4b-11e8-8153-00155d01bf07",
     *                                  "CreditProductID": "000000092"
     *                              }
     *                            в случае ошикби валидации вернет текущий объект
     *                            в случае исключения строку с описанием ошибки
     */
    public function getSmartAnnuity()
    {

        try {
            if (!$this->validate()) {
                return $this;
            }
            if (is_a($this->soapCMR, \Exception::class)) {
                throw new \Exception('Ошибка подключения к сервису Аннуитетного расчета, обратитесь к администратору');
            }
            $params = [];
            if (!empty($this->params)) {
                foreach ($this->params as $k => $v) {
                    $params['Parameter'][] = [
                        'Key'   => $k,
                        'Value' => $v,
                    ];
                }
            }
            $res = $this->soapCMR->send(
                'GetSmartAnnuity',
                [
                    'Sum'        => $this->amount,
                    'Date'       => $this->date,
                    'ClientID'   => $this->client,
                    'Parameters' => $params,
                ]
            );
            if (is_a($res, SoapFault::class)) {
                throw new \Exception(
                    'Возникла неизвестная ошибка при получения расчета для калькулятора, обратитесь к администратору'
                );
            }
            if (is_a($res, \Exception::class)) {
                throw new \Exception('Ошибка поулчения расчета для калькулятора, обратитесь к администратору');
            }
            if (!isset($res->ErrorData) || !isset($res->ErrorData->ID)) {
                throw new \Exception(
                    'При попытке получения расчета калькулятора был нарушен контракт между сервисами, обратитесь к администратору'
                );
            }
            $errors = [
                -3        => 'По указанным в калькуляторе параметрам невозможно подобрать кредитный продукт, обратитесь к администратору',
                -4        => 'По указанным в калькуляторе параметрам для подобранного кредитного продукта существует более одного условия, обратитесь к администратору',
                'default' => 'Произошла неизвестная ошибка при расчете калькулятора',
            ];
            if ($res->ErrorData->ID != 1) {
                $key = key_exists($res->ErrorData->ID, $errors) ? $res->ErrorData->ID : 'default';
                throw new \Exception($errors[$key]);
            }
            unset($res->ErrorData);

            return $res;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}