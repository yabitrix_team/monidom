<?php

namespace app\modules\app\v1\models\soap\cmr;

use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class CMRService - модель заимодействия по протоколу SOAP с удаленным сервисом "Сервисы ЦМР"
 *
 * @package app\modules\app\v1\models
 */
class CMRService extends Model {
	/**
	 * Константа сценария получения параметров расчета для калькулятора
	 */
	const SCENARIO_CALC_OPTIONS = 'CALC_OPTIONS';
	/**
	 * Константа сценария получение расчет полного и короткого графиков платежей
	 */
	const SCENARIO_GET_PAYMENT_CHART_V2 = 'GET_PAYMENT_CHART_V2';
	/**
	 * Константа сценария подбор кредитного продукта по параметрам с полным грфиком платежей
	 * и графиком желаемой даты погашения
	 */
	const SCENARIO_SMART_PAYMENT_CHART = 'SMART_PAYMENT_CHART';
	/**
	 * @var - идентификатор клиента, в данном случае это номер телефона
	 */
	public $client;
	/**
	 * @var - дополнительны параметр
	 */
	public $params;
	/**
	 * @var - сумма займа
	 */
	public $amount;
	/**
	 * @var - дата возврата займа
	 */
	public $date;
	/**
	 * @var - идентификатор кредитного продукта из 1С
	 */
	public $credit_product_1c;
	/**
	 * @var bool - признак постоянный клиент
	 */
	public $regular_customer = false;
	/**
	 * @var bool - признак докердитование
	 */
	public $pre_lending = false;
	/**
	 * @var string - название сервиса, к которому обращаемся
	 */
	protected $service = 'CMRService';
	/**
	 * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
	 */
	protected $soapCMR;

	/**
	 * Метод инициализации модели
	 */
	public function init() {

		parent::init();
		$this->soapCMR = Yii::$app->soapCMR->getInstance( $this->service );
	}

	/**
	 * Правила валидации
	 *
	 * @return array - вернет массив правил валидации
	 */
	public function rules() {

		return [
			[
				'client',
				'number',
				'message' => 'Некорректно укзаан формат идентификатора клиента для расчета калькулятора, обратитесь к администратору',
			],
			[
				'params',
				'safe',
			],
			[
				'amount',
				'required',
				'message' => 'Не указана сумма займа',
			],
			[
				'date',
				'required',
				'message' => 'Не указан срок займа',
			],
			[
				'credit_product_1c',
				'required',
				'message' => 'Не указан код кредитного продукта, обратитесь к администратору',
			],
			[
				'amount',
				'number',
				'message'  => 'Некорректно указан формат суммы займа',
				'min'      => 1,
				'tooSmall' => 'Сумма займа не может быть меньше или равна нулю',
			],
			[
				'date',
				'match',
				'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
				'message' => 'Некорректно указан формат даты срока займа',
			],
			//приводим к формату: 2020-09-15
			[
				'date',
				'filter',
				'filter' => function( $v ) {

					if ( ! empty( $v ) ) {
						//для формата  2018-01-22T00:00:00.000Z
						if ( strpos( $v, '-' ) !== false ) {
							return substr( $v, 0, 10 );
						}
						//для формата  22.01.2020
						if ( strpos( $v, '.' ) !== false ) {
							list( $d, $m, $y ) = explode( '.', $v );

							return $y . '-' . $m . '-' . $d;
						}
					}

					return null;
				},
			],
			[
				'date',
				'validateDate',
			],
			[
				'credit_product_1c',
				'string',
				'message' => 'Некорректно указан формат кода кредитного продукта, обратитесь к администратору',
			],
			[
				'regular_customer',
				'boolean',
				'message' => 'Некорректно указан формат признака постоянного клиента, обратитесь к администратору',
			],
			[
				'pre_lending',
				'boolean',
				'message' => 'Некорректно указан формат признака докредитования, обратитесь к администратору',
			],
		];
	}

	/**
	 * Метод установки сценария для модели при валидации полей
	 *
	 * @return array - вернет массив сценариев,
	 * по умолчанию установлен в сценарии установлен DEFAULT
	 */
	public function scenarios() {

		$scenarios                                        = parent::scenarios();
		$scenarios[ self::SCENARIO_CALC_OPTIONS ]         = [ 'client', 'params' ];
		$scenarios[ self::SCENARIO_GET_PAYMENT_CHART_V2 ] = [ 'amount', 'date', 'credit_product_1c' ];
		$scenarios[ self::SCENARIO_SMART_PAYMENT_CHART ]  = [ 'amount', 'date', 'regular_customer', 'pre_lending' ];

		return $scenarios;
	}

	/**
	 * Метод валидации для даты
	 *
	 * @param $attribute - атрибуты
	 * @param $params
	 */
	public function validateDate( $attribute, $params ) {

		if ( $this->{$attribute} < date( 'Y-m-d', time() ) ) {
			$this->addError( $attribute, 'Срок займа не может быть меньше текущей даты' );
		}
	}

	/**
	 * Метод получения параметров расчета для калькулятора
	 *
	 * @return $this|array|string - в случае успеха вернет массив объектов вида:
	 *                            [
	 *                              {
	 *                                  "Periods": 24,
	 *                                  "MinSum": 151000,
	 *                                  "MaxSum": 1000000,
	 *                                  "Step": 1000
	 *                              }
	 *                            ]
	 *                            вернет объект, в случае ошибки валидации
	 *                            врнет строку, в случае виброса исключения
	 */
	public function getCalcOptions() {

		try {
			if ( ! $this->validate() ) {
				return $this;
			}
			if ( is_a( $this->soapCMR, \Exception::class ) ) {
				throw new \Exception( 'Ошибка подключения к сервису ЦМР, обратитесь к администратору' );
			}
			$params = [];
			if ( ! empty( $this->params ) ) {
				foreach ( $this->params as $k => $v ) {
					$params['Parameter'][] = [
						'Key'   => $k,
						'Value' => $v,
					];
				}
			}
			$res = $this->soapCMR->send( 'GetCalcOptions',
			                             [
				                             'ClientID'   => $this->client,
				                             'Parameters' => $params,
			                             ] );
			if ( is_a( $res, SoapFault::class ) ) {
				throw new \Exception( 'Возникла неизвестная ошибка при получении параметров расчета для калькулятора, обратитесь к администратору' );
			}
			if ( is_a( $res, \Exception::class ) ) {
				throw new \Exception( 'Ошибка поулчения параметров расчета для калькулятора, обратитесь к администратору' );
			}
			if ( ! isset( $res->ErrorData ) || ! isset( $res->ErrorData->ID ) || ! isset( $res->CalcOptions ) ) {
				throw new \Exception( 'При попытке получения параметров расчета для калькулятора был нарушен контракт между сервисами, обратитесь к администратору' );
			}
			if ( $res->ErrorData->ID != 1 ) {
				throw new \Exception( 'Произошла неизвестная ошибка при попытке получения параметров расчета для калькулятора, обратитесь к администратору' );
			}

			return $res->CalcOptions;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения расчета полного и короткого графиков платежей
	 *
	 * @return $this|mixed|string - вернет объект(stdClass) полученный при обращении к сервису,
	 *                              либо текущий объект, содержащий ошибки валидации
	 *                              либо стоку с исклюением
	 */
	public function getPaymentChartV2() {

		try {
			if ( ! $this->validate() ) {
				return $this;
			}
			if ( is_a( $this->soapCMR, \Exception::class ) ) {
				throw new \Exception( 'Ошибка подключения к сервису ЦМР, обратитесь к администратору' );
			}
			$res = $this->soapCMR->send( 'GetPaymentChartv2',
			                             [
				                             'CreditProductId' => $this->credit_product_1c,
				                             'Sum'             => $this->amount,
				                             'StartDate'       => date( 'Y-m-d' ),
				                             'ReturnDate'      => $this->date,
			                             ] );
			if ( is_a( $res, SoapFault::class ) ) {
				throw new \Exception( 'Возникла неизвестная ошибка при получении графика платежей, обратитесь к администратору' );
			}
			if ( is_a( $res, \Exception::class ) ) {
				throw new \Exception( 'Ошибка поулчения графика платежей, обратитесь к администратору' );
			}
			if ( ! isset( $res->error ) || ! isset( $res->error->ID ) ) {
				throw new \Exception( 'При попытке получения графика платежей был нарушен контракт между сервисами, обратитесь к администратору' );
			}
			switch ( $res->error->ID ) {
				case 1:
					break;
				case 300:
					//Не найден кредитный продукт
					throw new \Exception( 'При попытке получения графика платежей не удалось найти кредитный продукт, обратитесь к администратору' );
					break;
				default:
					throw new \Exception( 'Произошла неизвестная ошибка при получении графика платежей' );
					break;
			}

			return $res;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод подбора кредитного продукта по параметрам с полным грфиком платежей
	 * и графиком желаемой даты погашения
	 *
	 * @return $this|mixed|string
	 */
	public function smartPaymentChart() {

		try {
			if ( ! $this->validate() ) {
				return $this;
			}
			if ( is_a( $this->soapCMR, \Exception::class ) ) {
				throw new \Exception( 'Ошибка подключения к сервису ЦМР, обратитесь к администратору' );
			}
			$res = $this->soapCMR->send( 'SmartPaymentChart',
			                             [
				                             'Sum'             => $this->amount,
				                             'RegularCustomer' => $this->regular_customer,
				                             'Recredit'        => $this->pre_lending,
				                             'ReturnDate'      => $this->date,
			                             ] );
			if ( is_a( $res, SoapFault::class ) ) {
				throw new \Exception( 'Возникла неизвестная ошибка при получении графика платежей, обратитесь к администратору' );
			}
			if ( is_a( $res, \Exception::class ) ) {
				throw new \Exception( 'Ошибка поулчения графика платежей, обратитесь к администратору' );
			}
			if ( ! isset( $res->error ) || ! isset( $res->error->ID ) ) {
				throw new \Exception( 'При попытке получения графика платежей был нарушен контракт между сервисами, обратитесь к администратору' );
			}
			switch ( $res->error->ID ) {
				case 1:
					break;
				case 300:
					//Невозможно подобрать продукт по заданным параметрам
					throw new \Exception( 'При попытке получения графика платежей не удалось подобрать кредитный продукт по указанным параметрам' );
					break;
				default:
					throw new \Exception( 'Произошла неизвестная ошибка при получении графика платежей' );
					break;
			}

			return $res;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}