<?php
namespace app\modules\app\v1\models\soap\cmr;

use SoapFault;
use Yii;
use yii\base\Model;

/**
 * Class AgreementsData -  модель взаимодействия по протоколу SOAP с удаленным сервисом "Сведения по договорам"
 *
 * @package app\modules\app\v1\models
 */
class AgreementsData extends Model
{
    /**
     * Константа сценария получения активных договоров
     */
    public const SCENARIO_ACTIVE_AGREEMENTS_INFO = 'ACTIVE_AGREEMENTS_INFO';
    /**
     * Константа сценария получения закрытых договоров
     */
    public const SCENARIO_CLOSED_AGREEMENTS_INFO = 'CLOSED_AGREEMENTS_INFO';
    /**
     * @var - коды договоров для получения договоров, принимает строку,
     *        содержащуию код договора или массив кодов договоров
     */
    public $code;
    /**
     * @var string - название сервиса, к которому обращаемся
     */
    protected $service = 'AgreementsData';
    /**
     * @var \app\modules\app\v1\components\SoapClientComponent - объект инициализации подключения по SOAP
     */
    protected $soapCMR;

    /**
     * Метод инициализации модели
     */
    public function init()
    {
        parent::init();
        $this->soapCMR = Yii::$app->soapCMR->getInstance($this->service);
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {
        return [
            [
                'code',
                'required',
                'message' => 'Не указаны коды договоров, обратитесь к администратору',
            ],
            ['code', 'validateCode'],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios(): array
    {
        $scenarios                                        = parent::scenarios();
        $scenarios[self::SCENARIO_ACTIVE_AGREEMENTS_INFO] = ['code'];
        $scenarios[self::SCENARIO_CLOSED_AGREEMENTS_INFO] = ['code'];

        return $scenarios;
    }

    /**
     * Метод валидации для кодов активных договоров
     *
     * @param $attribute - атрибуты
     * @param $params
     */
    public function validateCode($attribute, $params)
    {
        if (!is_string($this->{$attribute}) && !is_array($this->{$attribute})) {
            $this->addError($attribute, 'Некорректно указан формат договоров, обратитесь к администратору');
        }
    }

    /**
     * Метод получения активных договоров (первая версия без PSK)
     *
     * @link https://wiki.carmoney.ru/pages/viewpage.action?pageId=329046
     * @return $this|array|string - в случае успеха вернет результирующий массив объектов
     *                            [
     *                              {
     *                                  "Number": "18031208790003",
     *                                  "Date": "13 марта 2018 г.",
     *                                  "Summ": 750000,
     *                                  "CurrentPaymentSumm": 56678,
     *                                  "CurrentPaymentDate": "14 июля 2018 г.",
     *                                  "Rest": 690103.87,
     *                                  "Chart": [
     *                                      {
     *                                          "Date": "2018-06-14",
     *                                          "PaymentSumm": 56678,
     *                                          "Debt": 44153.11,
     *                                          "Percents": 12524.89,
     *                                          "DebtRest": 696304.56
     *                                      }
     *                                  ]
     *                              }
     *                            ]
     *                              в случае ошикби валидации вернет текущий объект
     *                              в случае исключения строку с описанием ошибки
     *                              если договора не найдены вернет пустой массив
     */
    public function activeAgreementsInfo()
    {
        return $this->agreements(__FUNCTION__, 'ActiveAgreementInfo');
    }

    /**
     * Метод получения активных договоров с учетом ПСК (вторая версия с учетом PSK)
     *
     * @link https://wiki.carmoney.ru/pages/viewpage.action?pageId=3510853
     * @return $this|array|string - в случае успеха вернет результирующий массив объектов
     *                            [
     *                              {
     *                                  "Number": "18031208790003",
     *                                  "Date": "13 марта 2018 г.",
     *                                  "Summ": 750000,
     *                                  "CurrentPaymentSumm": 56678,
     *                                  "NextPaymentSumm": 58940,
     *                                  "NextPaymentDate": "14 июля 2018 г.",
     *                                  "FullPaymentSumm": 690103.87,
     *                                  "Annuity": 56678,
     *                                  "Percent": 95,
     *                                  "PSK": 94.871,
     *                                  "Chart": [
     *                                      {
     *                                          "Date": "2018-06-14",
     *                                          "PaymentSumm": 56678,
     *                                          "Debt": 44153.11,
     *                                          "Percents": 12524.89,
     *                                          "DebtRest": 696304.56
     *                                      }
     *                                  ]
     *                              }
     *                            ]
     *                              в случае ошикби валидации вернет текущий объект
     *                              в случае исключения строку с описанием ошибки
     *                              если договора не найдены вернет пустой массив
     */
    public function activeAgreementsInfov2()
    {
        return $this->agreements(__FUNCTION__, 'ActiveAgreementInfo');
    }

    /**
     * Метод получения активных договоров с учетом суммы к оплате для клиента
     *
     * @link https://wiki.carmoney.ru/pages/viewpage.action?pageId=15761505
     * @return $this|array|string - в случае успеха вернет результирующий массив объектов
     *                            [
     *                              {
     *                                  "Number": "18031208790003",
     *                                  "Date": "13 марта 2018 г.",
     *                                  "Summ": 750000,
     *                                  "MobilePaymentSumm": 56678, //сумма к оплате для клиента
     *                                  "CurrentPaymentSumm": 56678,
     *                                  "NextPaymentSumm": 58940,
     *                                  "NextPaymentDate": "14 июля 2018 г.",
     *                                  "FullPaymentSumm": 690103.87,
     *                                  "Annuity": 56678,
     *                                  "Percent": 95,
     *                                  "PSK": 94.871,
     *                                  "Chart": [
     *                                      {
     *                                          "Date": "2018-06-14",
     *                                          "PaymentSumm": 56678,
     *                                          "Debt": 44153.11,
     *                                          "Percents": 12524.89,
     *                                          "DebtRest": 696304.56
     *                                      }
     *                                  ]
     *                              }
     *                            ]
     *                              в случае ошикби валидации вернет текущий объект
     *                              в случае исключения строку с описанием ошибки
     *                              если договора не найдены вернет пустой массив
     */
    public function activeAgreementsInfov4()
    {
        return $this->agreements(__FUNCTION__, 'ActiveAgreementInfo');
    }

    /**
     * Метод получения закрытых договоров
     *
     * @return $this|array|string - в случае успеха вернет результирующий массив объектов
     *                            [
     *                              {
     *                                  "Number":       "17122514700004",
     *                                  "Date":         "13 марта 2018 г.",
     *                                  "Summ":         345000,
     *                                  "CloseDate":    "13 марта 2018 г.",
     *                                  "Chart": [
     *                                      {
     *                                          "Date": "2018-06-14",
     *                                          "Summ": 56678,
     *                                      }
     *                                  ]
     *                              }
     *                            ]
     *                              в случае ошикби валидации вернет текущий объект
     *                              в случае исключения строку с описанием ошибки
     *                              если договора не найдены вернет пустой массив
     */
    public function closedAgreementsInfo()
    {
        return $this->agreements(__FUNCTION__, 'ClosedAgreementInfo');
    }

    /**
     * Метод обращения к сервису по работе с договрами
     *
     * @param string $method   - название сервиса, данные которого необходимо получить
     * @param string $response - элемент массива результата(см. доку в конфлюинсе по описанию сервиса)
     *
     * @return $this|array|\Exception - в случае успеха вернет результирующий массив объектов
     *                                  в случае ошикби валидации вернет текущий объект
     *                                  в случае исключения строку с описанием ошибки
     *                                  если договора не найдены вернет пустой массив
     */
    protected function agreements(string $method, string $response = '')
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $method        = ucfirst($method);
            $arNameService = [
                'ActiveAgreementsInfo'   => 'Активные договора',
                'ActiveAgreementsInfov2' => 'Активные договора с учетом ПСК',
                'ClosedAgreementsInfo'   => 'Закрытые договора',
                'default'                => 'Договора',
            ];
            $serviceName   = array_key_exists($method, $arNameService)
                ? $arNameService[$method]
                : $arNameService['default'];
            if (is_a($this->soapCMR, \Exception::class)) {
                throw new \Exception('Ошибка подключения к сервису "'.$serviceName.'", обратитесь к администратору');
            }
            $code   = is_string($this->code) ? $this->code : implode(',', $this->code);
            $result = $this->soapCMR->send(
                $method,
                [
                    'AgreementString' => $code,
                ]
            );
            if (is_a($result, SoapFault::class)) {
                throw new \Exception(
                    'Возникла неизвестная ошибка при обращении к сервису "'.
                    $serviceName.
                    '", обратитесь к администратору'
                );
            }
            if (is_a($result, \Exception::class)) {
                throw new \Exception(
                    'Произошла ошибка при отправке запроса к сервису "'.$serviceName.'", обратитесь к администратору'
                );
            }
            if (
                !isset($result->ErrorData) ||
                !isset($result->ErrorData->ID) ||
                !isset($result->ErrorData->Description)
            ) {
                throw new \Exception(
                    'При попытке обращения к сервису "'.
                    $serviceName.
                    '" был нарушен контракт согласованных параметров, обратитесь к администратору'
                );
            }
            switch ($result->ErrorData->ID) {
                case 2:
                case 1:
                    return !empty($response) ? $result->{$response} : $result;
                    break;
                case 3:
                    return [];
                    break;
                default:
                    throw new \Exception(
                        'При попытке обращения к сервису "'.
                        $serviceName.
                        '" произошла неизвестая ошибка, обратитесь к администратору'
                    );
                    break;
            }
        } catch (\Exception $e) {
            return $e;
        }
    }
}