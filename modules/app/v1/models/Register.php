<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;
use app\modules\app\v1\models\RegisterLog;

class Register extends Model {
	/*
	 * Логин пользователя, он же номер телефона (в БД username)
	 */
	public $login;
	/*
	 * email
	 */
	public $email;
	/*
	 * Имя пользователя (в БД first_name)
	 */
	public $name;
	/*
	 * Фамилия пользователя
	 */
	public $last_name;
	/*
	 * Отчество
	 */
	public $second_name;
	/*
	 * Пароль пользователя
	 */
	public $password;
	/**
	 * @var array - поля регистрации пользователя для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $registerFieldCreate = [
		'login',
		'password',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'login',
				'match',
				'pattern' => '/^9[0-9]{9}$/iu',
				'message' => 'Номер телефона указан в некорректном формате',
			],
			[
				'password',
				'match',
				'pattern' => "/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[\d])\S*$/iu",
				'message' => 'Пароль должен содержать минимум одну строчную буквы, одну цифру и быть длинной не меннее 6 символов',
			],
			[
				'email',
				'email',
				'message' => 'Поле "Электронная почта" заполнено некорректно',
			],
			[
				'name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
			],
			[
				'name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Имя" заполнено некорректно',
			],
			[
				'last_name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Фамилия" заполнено некорректно',
			],
			[
				'last_name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
			],
			[
				'second_name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Отчество" заполнено некорректно',
			],
			[
				'second_name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
			],
		];
	}

	/*
	 * Метод регистрации нового пользователя
	 *
	 * @return true|array|false|string - создан и авторизован, ошибка валидции, неизвестная ошибка, экспешн
	 */
	public function addUser() {

		try {
			//Проверяем статус кода подтверждения
			$statusArray = ( new SmsConfirm() )->getByLogin( $this->login );
			if ( empty( $statusArray ) || $statusArray == false ) {
				throw new \Exception( 'Не найден код подтверждения' );
			}
			if ( is_string( $statusArray ) ) {
				throw new \Exception( $statusArray );
			}
			if ( $statusArray['status'] == 0 ) {
				throw new \Exception( 'Код подтверждения не подтвержден' );
			}
			//Проверяем пользователя на существование в БД
			if ( Users::getByLogin( $this->login ) ) {
				throw new \Exception( 'Пользователь с этим номером телефона уже зарегистрирован' );
			}
			//Создаем пользователя для сохранения в БД
			$user = new Users( [ 'scenario' => Users::SCENARIO_CREATE ] );
			if ( ! $user->load( [
				                    'username'    => $this->login,
				                    'email'       => $this->email,
				                    'first_name'  => $this->name,
				                    'last_name'   => $this->last_name,
				                    'second_name' => $this->second_name,
			                    ], ''
			) ) {
				throw new \Exception( 'Не удалось загрузить данные для нового пользователя' );
			}
			$user->hashPassword( $this->password ); //Не забываем загрузить пароль
			//сохраняем данные пользователя
			if ( ! $user->validate() ) {

				return $user->firstErrors;
			}
			$result = $user->create();
			if ( ! intval( $result ) && is_string( $result ) ) {
				throw new \Exception( $result );
			} elseif ( $result == false ) {
				throw new \Exception( "Не удалось создать пользователя. Обратитесь к администратору." );
			}
			if ( $arRegisterLog = RegisterLog::instance()
			                                 ->getAllByFilter( [ 'login' => $user->username ], [ 'created_at' => 'desc' ] )[0] ) {
				RegisterLog::instance()->update( $arRegisterLog["id"], [ 'user_id' => $result ] );
			}
			//todo[hdcy]: 09.04.2018 Отправка сообщение с уведомлением о регистрации нового пользователя
			//очистка записи подтверждения
			$resultSmsDelete = ( new SmsConfirm() )->deleteByLogin( $user->username );
			if ( $resultSmsDelete == 0 ) {
				throw new \Exception( "Не найдена запись строки подтверждения для удаления" );
			}
			if ( $resultSmsDelete != 1 ) {
				throw new \Exception( $resultSmsDelete );
			}
			//Добвляем группу для пользователя
			$userGroup        = new UserGroup();
			$groupGuidDefault = Yii::$app->params['core']['user']['groupGuidDefault'];
			if ( ! $userGroup->load( [
				                         'user_id'  => Users::getByLogin( $user->username )['id'],
				                         'group_id' => Groups::getGroupByIdentifier( $groupGuidDefault )['id'],
			                         ], '' ) ) {
				throw new \Exception( "Не удалось загрузить данные группы нового пользователя" );
			}
			if ( ! $userGroup->validate() ) {

				return $userGroup->firstErrors;
			}
			if (!$userGroup->create()) {
                throw new \Exception( "Не удалось сохранить связку пользователь-группа. Обратитесь к администратору" );
			}

			return true;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}