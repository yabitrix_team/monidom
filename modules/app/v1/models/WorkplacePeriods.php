<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;
use yii\base\Model;

/**
 * Class WorkplacePeriods - модель для работы со справочником  Стаж на этом месте работы
 *
 * @package app\modules\app\v1\models
 */
class WorkplacePeriods extends Model {
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {
		return "{{workplace_periods}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {
		return [
			"id",
			"guid",
			"name",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}
}