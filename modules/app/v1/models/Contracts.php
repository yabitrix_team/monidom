<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Class Contracts - модель по работе с Договорами заявок
 *
 * @package app\modules\app\v1\models
 */
class Contracts extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{contracts}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "code",
            "request_id",
            "user_id",
            "active",
            "created_at",
            "updated_at",
        ];
    }

    /**
     * Метод получения активных кодов договоров по идентификатору пользователя
     *
     * @param int $userId - идентификатор пользователя
     *
     * @return array|\Exception- вернет результирующий массив кодов или объект исключения
     */
    public function getActiveCodeByUserId(int $userId = 0)
    {
        try {
            $userId = empty($userId) ? Yii::$app->user->id : $userId;

            return Yii::$app->db->createCommand(
                'SELECT [[CODE]] FROM '.$this->getTableName().'
				 WHERE [[user_id]] = :userId AND [[active]] = 1',
                [':userId' => $userId]
            )
                ->queryColumn();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод получения закрытых кодов договоров по идентификатору пользователя
     *
     * @param int $userId - идентификатор пользователя
     *
     * @return array|string - вернет результирующий массив кодов или строку с исключением
     */
    public function getClosedCodeByUserId(int $userId = 0)
    {
        try {
            $userId = empty($userId) ? Yii::$app->user->id : $userId;

            return Yii::$app->db->createCommand(
                '
	SELECT [[code]] FROM '.$this->getTableName().'
				 WHERE [[user_id]] = :userId AND [[active]] = 0',
                [':userId' => $userId]
            )
                ->queryColumn();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения связей кодов договоров с кодами заявки
     *
     * @param $code - код или массив кодов, по которым нужно получить
     *              символьные коды заявок
     *
     * @return array|string - вернет результирующий массив вида
     *                      [
     *                      "18042614150002": "acf866403f544d15bf14845d295c8506",
     *                      ]
     *                      либо строку с исключением
     */
    public function relationCode($code)
    {
        try {
            $code = is_array($code) ? implode(',', $code) : $code;
            $res  = Yii::$app->db->createCommand(
                '
SELECT [[c]].[[code]], [[r]].[[code]] AS [[request_code]]
FROM '.$this->getTableName().' AS [[c]]
LEFT JOIN {{requests}} AS [[r]] ON [[c]].[[request_id]] = [[r]].[[id]]
WHERE [[c]].[[code]] IN ('.$code.')
			'
            )->queryAll();

            return !empty($res) && is_array($res) ? array_column($res, 'request_code', 'code') : $res;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод сохранения записи (создает или обновляет запись) в таблице
     *
     * @param array $params - параметры для сохранения
     *
     * @return $this|int - вернет 1 - при добавлении, 2 - при обновленииекущий объект, если валидация не прошла
     * @throws \Exception
     */
    public function save(array $params)
    {
        try {
            $params['active']     = isset($params['active']) ? intval($params['active']) : 1;
            $insert               = $update = $params;
            $insert['updated_at'] = $update['updated_at'] = new Expression('NOW()');

            return Yii::$app->db
                ->createCommand()
                ->upsert($this->getTableName(), $insert, $update)
                ->execute();
        } catch (\Exception $e) {
            throw new \Exception('Ошибка сохранения договоров по заявке в базе данных, обратитесь к администратору');
        }
    }
}
