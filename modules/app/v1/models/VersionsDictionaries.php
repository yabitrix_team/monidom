<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Class StatusesMP - модель для работы с таблицей Версии справочников
 *
 * @package app\modules\app\v1\models
 */
class VersionsDictionaries extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;
	/**
	 * @var - название справочника(аналогично названию идентификатора контроллера)
	 */
	public $dictionary;

	/**
	 * @return array - правила валидации передаваемых данных
	 */
	public function rules() {

		return [
			[ 'dictionary', 'required', 'message' => 'Не указано название справочника' ],
			[ 'dictionary', 'string', 'message' => 'Название справочника указана в некорректном формате' ],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{versions_dictionaries}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"version",
			"dictionary",
			"updated_at",
			"created_at",
		];
	}

	/**
	 * Метод сохранения записи о версиях справочников
	 * создает или обновляет запись
	 *
	 * @return $this|int|string - вернет 1 - при добавлении,
	 *                            2 - при обновлении,
	 *                            объект, если валидация не прошла,
	 *                            строку при выбросе исключения
	 */
	public function save() {

		try {
			if ( $this->validate() ) {
				$insert               = $update = $this->getAttributes();
				$update['version']    = new Expression( 'version + 1' );
				$update['updated_at'] = new Expression( 'NOW()' );

				return Yii::$app->db
					->createCommand()
					->upsert( $this->getTableName(), $insert, $update )->execute();
			} else {
				return $this;
			}
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения версий всех справочников
	 *
	 * @return array|string - вернет результирующий массив или строку с исключением
	 */
	public function getAll() {

		try {
			$data = [];
			$res  = Yii::$app->db->createCommand( "SELECT [[version]], [[dictionary]] 
FROM " . $this->getTableName() )
			                     ->queryAll();
			if ( ! empty( $res ) ) {
				foreach ( $res as $item ) {
					$data[ $item['dictionary'] ] = $item['version'];
				}
			}

			return $data;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения версии справочника
	 *
	 * @param $dictionary - название справочника, он же идентификатор контроллера
	 *                    пример: AboutCompanyController.php = about-company
	 *
	 * @return false|int|string - вернет номер версии, либо ложь, либо строку с исключением
	 */
	public function getByDictionary( $dictionary ) {

		try {
			$res = Yii::$app->db->createCommand( "SELECT [[version]] 
FROM " . $this->getTableName() . " 
WHERE [[dictionary]] = :dictionary", [ 'dictionary' => $dictionary ] )
			                    ->queryScalar();

			return is_numeric( $res ) ? intval( $res ) : $res;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}