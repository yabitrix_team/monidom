<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class StatusesJournal - модель для работы с таблице связей заявки со статусами для МП
 *
 * @package app\modules\app\v1\models
 */
class StatusesMPJournal extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * @var - идентификатор заявки
     */
    public $request_id;
    /**
     * @var - идентификатор статуса заявки для МП
     */
    public $status_mp_id;

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return '{{statuses_mp_journal}}';
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {

        return [
            'id',
            'request_id',
            'status_mp_id',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {

        return [
            [
                'request_id',
                'required',
                'message' => 'Не указан идентификатор заявки',
            ],
            [
                'status_mp_id',
                'required',
                'message' => 'Не указан идентификатор статуса для мобильного приложения',
            ],
            [
                'request_id',
                'number',
                'message' => 'Идентификатор заявки указан в некорректном формате',
            ],
            [
                'status_mp_id',
                'number',
                'message' => 'Идентификатор статуса заявки для мобильного приложения указан в некорректном формате',
            ],
        ];
    }

    /**
     * Метод созадни записи связи заявки со статусом для МП
     *
     * @return $this|bool|\Exception|int - вернет идентификатор записи или ложь,
     *                                   текущий объект, если не пройдена валидация,
     *                                   объект исключения, при выбросе исключения
     */
    public function create()
    {

        try {
            if (!$this->validate()) {
                return $this;
            }
            $insert = $this->getAttributes();

            return $this->add($insert);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод удаления записи по идентификатору
     *
     * @param $id - идентификатор записи
     *
     * @return int|string - вернет кол-во удаленных строк или строку с исключением
     */
    public function deleteOne($id)
    {

        try {
            return Yii::$app->db->createCommand()
                ->delete($this->getTableName(), 'id = :id', [':id' => $id])
                ->execute();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения последнего статуса заявки для МП по коду заявки
     *
     * @param $code - код заявки
     *
     * @return array|false - вернет результрующий массив или ложь
     */
    public function lastStatusByRequestCode($code)
    {
        try {
            return Yii::$app->db
                ->createCommand(
                    "SELECT
[[sm]].[[id]],
[[sm]].[[name]],
[[sm]].[[identifier]],
[[sm]].[[active]],
[[r]].[[id]] as [[request_id]]
FROM {{statuses_mp}} AS [[sm]]
LEFT JOIN ".$this->getTableName()." AS [[smj]] ON [[sm]].[[id]] = [[smj]].[[status_mp_id]]
LEFT JOIN {{requests}} AS [[r]] ON [[smj]].[[request_id]] = [[r]].[[id]]
WHERE [[r]].[[code]] = :code AND [[r]].[[client_id]] = ".Yii::$app->user->id." ORDER BY [[smj]].[[id]] DESC LIMIT 1",
                    [':code' => $code,]
                )
                ->queryOne();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения последнего статуса заявки мобильного приложения из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод добавления статуса МП для заявок
     *
     * @param        $requestId - идентификатор заявки
     * @param string $statusMP  - символьный идентификатор статуса для МП
     *
     * @return bool|\Exception|int - вернет идентификатор записи
     *                             либо ложь если не произойдет вставка
     *                             вернет объект в случае исключения
     */
    public function setStatus($requestId, string $statusMP)
    {

        try {
            $statusMP = (new StatusesMP())->getByIdentifier($statusMP);
            if (empty($statusMP) || is_string($statusMP)) {
                throw new \Exception(
                    'При установке статуса заявки для мобильного приложения не удалось получить идентификатор статуса, обратитесь к администратору'
                );
            }

            return $this->add(
                [
                    'request_id'   => $requestId,
                    'status_mp_id' => $statusMP['id'],
                ]
            );
        } catch (\Exception $e) {
            return $e;
        }
    }
}