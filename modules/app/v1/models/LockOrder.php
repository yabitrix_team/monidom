<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class LockOrder extends Model {
	/**
	 * @return array
	 * @throws \yii\db\Exception
	 */
	public function get() {

		return Yii::$app->db->createCommand( 'SELECT * FROM lock_order' )
		                    ->queryAll();
	}
}