<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class Agents extends Model {
	/**
	 * Метод получения элемента по идентификатору
	 *
	 * @param $id - Идентификатор элемента
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "SELECT
  `id`,
  `guid`,  
  `partner_id`,  
  `active`
FROM agents
WHERE `id` = :id", [ ':id' => $id ] )
			->queryOne();
	}
}