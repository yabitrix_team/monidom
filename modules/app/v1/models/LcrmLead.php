<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\validators\AutoBrandValidator;
use app\modules\app\v1\validators\AutoModelValidator;
use app\modules\app\v1\validators\CreditProductValidator;
use app\modules\app\v1\validators\RegionValidator;
use Yii;
use yii\base\Model;

/**
 * Лид LCRM
 *
 * @package app\modules\call\v1\models\base
 */
class LcrmLead extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константа сценария создания лида
     */
    const SCENARIO_CREATE = 'create';
    /**
     * Константа сценария обновления лида
     */
    const SCENARIO_UPDATE = 'update';
    /**
     * Константа сценария обновления лида в части времени перезвонить и коментария
     */
    const SCENARIO_UPDATE_CALLBACK = 'update_callback';
    /**
     * Константа сценария создания заявки
     */
    const SCENARIO_REQUEST = 'request';
    /**
     * @var int ID
     */
    public $id;
    /**
     * @var string код для url
     */
    public $code;
    /**
     * @var int LCRM ID
     */
    public $lcrm_id;
    /**
     * @var int ID заявки
     */
    public $request_id;
    /**
     * @var int точка партнера
     */
    public $point_id;
    /**
     * @var int пользователь партнера
     */
    public $user_id;
    /**
     * @var string мобильный телефон
     */
    public $client_mobile_phone;
    /**
     * @var int сумма займа
     */
    public $summ;
    /**
     * @var string ID кредитной программы
     */
    public $credit_product_id;
    /**
     * @var int ID модели автомобиля
     */
    public $auto_model_id;
    /**
     * @var int ID бренда автомобиля
     */
    public $auto_brand_id;
    /**
     * @var int год автомобиля
     */
    public $auto_year;
    /**
     * @var string имя
     */
    public $client_first_name;
    /**
     * @var string фамилия
     */
    public $client_last_name;
    /**
     * @var string отчетсво
     */
    public $client_patronymic;
    /**
     * @var string дата рождения
     */
    public $client_birthday;
    /**
     * @var string паспорт
     */
    public $client_passport_number;
    /**
     * @var string серийный номер паспорта
     */
    public $client_passport_serial_number;
    /**
     * @var int регион
     */
    public $client_region_id;
    /**
     * @var string дата возврата долга
     */
    public $date_return;
    /**
     * @var bool флаг активности
     */
    public $active;
    /**
     * @var /date дата создания
     */
    public $created_at;
    /**
     * @var /date дата обновления
     */
    public $updated_at;
    /**
     * @var дата, время созвона
     */
    public $callback_time;
    /**
     * @var коментарий
     */
    public $comment;
    /**
     * @var array поля сценария создания лида
     */
    protected $scenarioCreateFields = [
        '!code',
        'lcrm_id',
        'client_mobile_phone',
    ];
    /**
     * @var array поля сценария обновления лида
     */
    protected $scenarioUpdateFields = [
        'client_mobile_phone',
        'summ',
        'credit_product_id',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_birthday',
        'client_passport_serial_number',
        'client_passport_number',
        'client_region_id',
        'date_return',
        'point_id',
        'user_id',
        '!updated_at',
    ];
    /**
     * @var array поля сценария обновления лида
     */
    protected $scenarioUpdateCallbackFields = [
        'callback_time',
        'comment',
    ];
    /**
     * @var array поля сценария создания заявки и привязки лида
     */
    protected $scenarioRequestFields = [
        '!request_id',
        '!updated_at',
    ];

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     * по умолчанию установлен в сценарии установлен DEFAULT
     */
    public function scenarios()
    {
        $scenarios                                 = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE]          = $this->scenarioCreateFields;
        $scenarios[self::SCENARIO_UPDATE]          = $this->scenarioUpdateFields;
        $scenarios[self::SCENARIO_UPDATE_CALLBACK] = $this->scenarioUpdateCallbackFields;
        $scenarios[self::SCENARIO_REQUEST]         = $this->scenarioRequestFields;

        return $scenarios;
    }

    public function rules()
    {
        return [
            [
                [
                    'code',
                    'lcrm_id',
                    'client_mobile_phone',
                ],
                'required',
                'on'      => self::SCENARIO_CREATE,
                'message' => 'Не все обязательные поля заполнены',
            ],
            [
                [
                    'client_mobile_phone',
                    'summ',
                    'credit_product_id',
                    'auto_brand_id',
                    'auto_model_id',
                    'auto_year',
                    'client_first_name',
                    'client_last_name',
                    'client_birthday',
                    'client_passport_serial_number',
                    'client_passport_number',
                    'client_region_id',
                    'date_return',
                    'point_id',
                    'user_id',
                    'updated_at',
                ],
                'required',
                'on'      => self::SCENARIO_UPDATE,
                'message' => 'Не все обязательные поля заполнены',
            ],
            [
                [
                    'callback_time',
                ],
                'required',
                'on'      => self::SCENARIO_UPDATE_CALLBACK,
                'message' => 'Не все обязательные поля заполнены',
            ],
            [
                [
                    'request_id',
                    'updated_at',
                ],
                'required',
                'on'      => self::SCENARIO_REQUEST,
                'message' => 'Не все обязательные поля заполнены',
            ],
            [
                'summ',
                'number',
                'message' => 'Поле "Сумма займа" заполнено некорректно',
            ],
            ['auto_brand_id', AutoBrandValidator::class],
            ['auto_model_id', AutoModelValidator::class],
            [
                'auto_year',
                'number',
                'message'  => 'Поле "Год выпуска авто" заполнено некорректно',
                'min'      => '1919',
                'tooSmall' => 'Значение в поле "Год выпуска авто" должно быть не меньше 1919',
                'max'      => date('Y'),
                'tooBig'   => 'Значение в поле "Год выпуска авто" не должно превышать '.date('Y'),
            ],
            ['credit_product_id', CreditProductValidator::class],
            [
                'client_first_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Имя" заполнено некорректно',
            ],
            [
                'client_first_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
            ],
            [
                'client_last_name',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Фамилия" заполнено некорректно',
            ],
            [
                'client_last_name',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
            ],
            [
                'client_patronymic',
                'match',
                'pattern' => '/^[а-яё\s-]+$/iu',
                'message' => 'Поле "Отчество" заполнено некорректно',
            ],
            [
                'client_patronymic',
                'string',
                'length'   => [2],
                'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
            ],
            [
                'client_passport_serial_number',
                'match',
                'pattern' => '/^\d{4}$/iu',
                'message' => 'Поле "Серия паспора" заполнено некорректно',
            ],
            [
                'client_passport_number',
                'match',
                'pattern' => '/^\d{6}$/iu',
                'message' => 'Поле "Номер паспора" заполнено некорректно',
            ],
            [
                'client_birthday',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
                'message' => 'Поле "Дата рождения" заполнено некорректно',
            ],
            [
                'date_return',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
                'message' => 'Некорректно указана дата возврата займа',
            ],
            [
                'callback_time',
                'match',
                'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}[T]\d{2}[:]\d{2}[:]\d{2}[\.]\d{3}[Z]/i',
                'message' => 'Некорректно указана дата и время созвона с клиентом',
            ],
            [
                ['client_mobile_phone'],
                'match',
                'pattern' => '/^\d{10}$/iu',
                'message' => 'Номер телефона заполнен некорректно',
            ],
            ['client_region_id', RegionValidator::class],
        ];
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function getAllCnt($filter = "")
    {
        return (int) Yii::$app->db->createCommand(
            "select count(*) cnt from ".
            $this->getTableName().
            " where [[active]]=1 and [[request_id]] is null".
            (strlen($filter) ? " and [[client_mobile_phone]] like :filter" : ""),
            [":filter" => "%".$filter."%"]
        )->queryOne()["cnt"];
    }

    /**
     * @throws \yii\db\Exception
     */
    public function getList($page = 1, $size = 10, $filter = "")
    {
        return Yii::$app->db->createCommand(
            "select ".
            implode(", ", $this->getFieldsNames()).
            "
            from ".
            $this->getTableName().
            "
            where [[active]]=1 and [[request_id]] is null".
            (strlen($filter) ? " and [[client_mobile_phone]] like :filter" : "").
            " order by ISNULL(callback_time), callback_time ASC ".
            " limit ".
            (($page - 1) * $size).
            ", ".
            $size,
            [":filter" => "%".$filter."%"]
        )->queryAll();
    }

    /**
     * @param string $code
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getOne($code = "", $active = true)
    {
        return Yii::$app->db->createCommand(
            "select ".implode(", ", $this->getFieldsNames())."
            from ".$this->getTableName()."
            where".($active ? " [[active]]=1 and " : "")."[[request_id]] is null and [[code]]=:code",
            [":code" => $code]
        )->queryOne();
    }

    /**
     * @throws \Exception
     */
    public function delete($id)
    {
        if (
        Yii::$app->db->createCommand()
            ->update($this->getTableName(), ["active" => 0], "id=:id", [":id" => $id])
            ->execute()
        ) {
            return true;
        }
        throw new \Exception("Ошибка при удалении лида LCRM");
    }

    /**
     * @throws \Exception
     */
    public function recover($id)
    {
        if (
        Yii::$app->db->createCommand()
            ->update($this->getTableName(), ["active" => 1], "id=:id", [":id" => $id])
            ->execute()
        ) {
            return true;
        }
        throw new \Exception("Ошибка при восстановлении лида LCRM");
    }

    /**
     * @throws \Exception
     */
    public function create()
    {
        if (Yii::$app->db->createCommand()->insert($this->getTableName(), array_filter($this->attributes))->execute()) {
            return Yii::$app->db->getLastInsertID();
        }
        throw new \Exception("Ошибка при записи лида LCRM");
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function update($id = 0)
    {

        return Yii::$app->db->createCommand()->update(
            $this->getTableName(),
            $this->getAttributes($this->scenarios()[$this->scenario]),
            "id=".intval($id)
        )->execute();

    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "{{lcrm_leads}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'code',
            'lcrm_id',
            'request_id',
            'point_id',
            'user_id',
            'client_mobile_phone',
            'summ',
            'credit_product_id',
            'auto_brand_id',
            'auto_model_id',
            'auto_year',
            'client_first_name',
            'client_last_name',
            'client_patronymic',
            'client_birthday',
            'client_passport_serial_number',
            'client_passport_number',
            'client_region_id',
            'date_return',
            'created_at',
            'updated_at',
            'callback_time',
            'comment',
        ];
    }
}