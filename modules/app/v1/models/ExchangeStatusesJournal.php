<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;

/**
 * Class StatusesMP - модель для работы с таблицей Версии справочников
 *
 * @package app\modules\app\v1\models
 */
class ExchangeStatusesJournal extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{exchange_statuses_journal}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"ID_1C",
			"DateAdd",
			"Zayavka",
			"Period",
			"Status",
			"Dopolnitelnoe_opisanie",
			"Nomer_zayavki_sayta",
			"created_at",
			"updated_at",
			"try"
		];
	}

}