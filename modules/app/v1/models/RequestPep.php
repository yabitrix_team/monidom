<?php
namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Class RequestPep - модель для работы с таблицей подтверждений ПЭП по заявкам
 *
 * @package app\modules\app\v1\models
 */
class RequestPep extends Model
{
    /**
     * Подключение трейта, отвечающего за запись данных в таблицу
     */
    use WritableTrait;
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * Константа тип подтверждения ПЭП заявки по СМС
     */
    const CONFIRM_TYPE_SMS = 'sms';
    /**
     * Константа тип подтверждения ПЭП заявки по заявке от той же даты
     * с тем же паспортом и с флагом подтверждения ПЭП
     */
    const CONFIRM_TYPE_REQUEST = 'request';
    /**
     * Константа тип подтверждения ПЭП заявки по ативному договору клиента
     * с тем же паспортом заявки, от которой создан договор
     */
    const CONFIRM_TYPE_AGREEMENT = 'agreement';
    /**
     * Константа SMS провайдер от ресурса redsms.ru
     */
    const SMS_PROVIDER_REDSMS = 'RedSMS';
    /**
     * @var integer - идентификатор заявки
     */
    public $request_id;
    /**
     * @var - тип подтверждения ПЭП заявки,
     *      тивы заведены в виде констант CONFIRM_TYPE_*
     */
    public $confirm_type;
    /**
     * @var string - код подтверждения по СМС
     */
    public $sms_code;
    /**
     * @var string - символьный идентификатор СМС
     */
    public $sms_id;
    /**
     * @var string - поставщик услуг СМС
     */
    public $sms_provider;
    /**
     * @var integer - связь с заявкой относительно которой был проставлен ПЭП
     *              в текущей заявке
     */
    public $relation_request;
    /**
     * @var string - связь номером договора, относительно которого был просталвен ПЭП
     *             в текущей заявке
     */
    public $relation_agreement;
    /**
     * @var string - тип устройства или заголовок браузера
     */
    public $user_agent;

    /**
     * Получение названия таблицы модели в БД.
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {

        return "{{request_pep}}";
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {

        return [
            'id',
            'request_id',
            'confirm_type',
            'relation_request',
            'relation_agreement',
            'sms_code',
            'sms_id',
            'sms_provider',
            'user_agent',
            'updated_at',
            'created_at',
        ];
    }

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {
        return [
            [
                'request_id',
                'required',
                'message' => 'Не указан идентификатор заявки',
            ],
            [
                'request_id',
                'number',
                'message' => 'Идентификатор заявки указан в некорректном формате',
            ],
            [
                'confirm_type',
                'in',
                'range'   => [self::CONFIRM_TYPE_SMS, self::CONFIRM_TYPE_REQUEST, self::CONFIRM_TYPE_AGREEMENT],
                'message' => 'В передаваемых параметрах некорректно указан тип подтверждения простой электронной подписи в заявке',
            ],
            [
                'relation_request',
                'number',
                'message' => 'Идентификатор связующей заявки указан в некорректном формате',
            ],
            [
                'relation_agreement',
                'string',
                'message' => 'Идентификатор связующего активного договора указан в некорректном формате',
            ],
            [
                'sms_code',
                'string',
                'message' => 'Код подтверждения СМС указан в некорректно формате',
            ],
            [
                'sms_id',
                'string',
                'message' => 'Символьный идентификатор СМС указан в некорректном формате',
            ],
            [
                'sms_provider',
                'string',
                'message' => 'Название поставщика услуг СМС указано в некорректном формате',
            ],
            [
                'user_agent',
                'string',
                'message' => 'Тип устройства(заголовок браузера) указан в некорректном формате',
            ],
        ];
    }

    /**
     * Метод сохранения записи (создает или обновляет запись) в таблице
     *
     * @return $this|int - вернет 1 - при добавлении, 2 - при обновлении,
     *                            текущий объект, если валидация не прошла
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            throw new \Exception(
                'Ошибка валидаци данных при подтверждении простой электронной подписи, обратитесь к администратору'
            );
        }
        $insert               = $update = $this->prepareDataForExecute();
        $update['updated_at'] = new Expression('NOW()');

        return Yii::$app->db
            ->createCommand()
            ->upsert($this->getTableName(), $insert, $update)->execute();
    }

    /**
     * Метод удаления записи в БД по идентификатору заявки
     *
     * @param $requestId - идентификатор заявки
     *
     * @return int - вернет кол-во удаленных строк
     * @throws \yii\db\Exception
     */
    public function deleteByRequestId($requestId)
    {
        return Yii::$app->db
            ->createCommand()
            ->delete($this->getTableName(), '[[request_id]] = :requestId', ['requestId' => $requestId])
            ->execute();
    }
}