<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

/**
 * Class Pages - модель для работы со справочником Страницы
 *
 * @package app\modules\app\v1\models
 */
class Pages extends Model {
	/**
	 * Метод получения всех элементов
	 *
	 * @param bool $active - Параметр отвечающий за получение всех элементов, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' WHERE `active` = 0 ' : ' WHERE `active` = 1 ' );

		return Yii::$app->db->createCommand( 'SELECT
  `id`,
  `name`,
  `text`,
  `url`,
  `consumer_id`,
  `active`,
  `sort`
FROM pages ' . $active )
		                    ->queryAll();
	}

	/**
	 * Метод получения оного элемента
	 *
	 * @param $id - Идентификатор элемента
	 *
	 * @return array|bool|false - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( $id ) {

		if ( ! is_numeric( $id ) ) {
			return false;
		}

		return Yii::$app->db
			->createCommand( "SELECT
  `name`,
  `text`,
  `url`,
  `consumer_id`
FROM pages
WHERE `id` = :id", [ ':id' => $id ] )
			->queryOne();
	}

	/**
	 * Метод получения определенной страницы для определенного потребителя
	 *
	 * @param $url         - путь страницы
	 * @param $consumer    - потребитель(пример: mp, client, partner)
	 * @param $active      - параметр отвечающий за получение всех элементов, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array|false - вернет результат выборки
	 * @throws \yii\db\Exception
	 */
	public function getPage( $url, $consumer, $active = true ) {

		$active = is_null( $active ) ? ' ' : ( empty( $active ) ? ' AND `active` = 0 ' : ' AND `active` = 1 ' );

		return Yii::$app->db
			->createCommand( "SELECT
  `name`,
  `text`
FROM pages
WHERE `url` = :url " . $active . " AND `consumer_id` = (SELECT `id`
                                        FROM consumer AS c
                                        WHERE `name` = :consumer) ", [
				':url'      => $url,
				':consumer' => $consumer,
			] )
			->queryOne();
	}
}