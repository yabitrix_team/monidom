<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

class Feedback extends Model {
	/*
	 * Имя пользователя
	 */
	public $name;
	/*
	 * Email пользователя
	 */
	public $email;
	/*
	 * Сообщение от пользователя
	 */
	public $message;
	/**
	 * @var array - поля регистрации пользователя для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $registerFieldCreate = [
		'name',
		'email',
		'message',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'email',
				'email',
				'message' => 'E-mail пользователя заполнен не корректно',
			],
		];
	}

	/*
	 * Метод отправки сообщения feedback
	 *
	 * @return true|array|false|string - отправлен|ошибка валидации|неуспех отправки|сообщение об ошибку
	 */
	public function send() {

		try {
			//Подготавливаем сообщение
			$sendMail = Yii::$app->mailer->compose( 'feedback', [
				'name'    => $this->name,
				'email'   => $this->email,
				'message' => $this->message,
			] )
			                             ->setFrom( Yii::$app->params['core']['email']['from'] )
			                             ->setTo( Yii::$app->params['core']['email']['feedbackPhoto'] )
			                             ->setSubject( 'Сообщение обратной связи от пользователя.' );
			$sendMail->send();
			if ( $sendMail ) {
				//todo[hdcy]: 17.04.2018 Выводить сообщение в лог если файлы не удалось удалить
				return true;
			}

			return "Неизвестная ошибка при отправке сообщения. Обратитесь к администратору";
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}