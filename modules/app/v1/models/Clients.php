<?php

namespace app\modules\app\v1\models;

use Yii;
use yii\base\Model;

/**
 * Class Clients - модель для работы со справочником Заявок
 *
 * @package app\modules\app\v1\models
 */
class Clients extends Model {
	/*
	 * Страница пагинации
	 */
	public $pagin;
	/*
	 * Дата начала выборки
	 */
	public $dateFrom;
	/*
	 * Дата конца выборки
	 */
	public $dateTo;

	public function rules() {

		return [
			[
				[ 'pagin', 'dateFrom', 'dateTo' ],
				'required',
				'message' => 'В запросе отсутствуют обязательные поля.',
			],
			[
				'pagin',
				'number',
				'message' => 'Некорректно указана страница для выборки',
			],
			[
				'dateFrom',
				'match',
				'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
				'message' => 'Некорректно указана дата начала выборки',
			],
			[
				'dateTo',
				'match',
				'pattern' => '/^\d{2,4}[-\.]{1}\d{2}[-\.]{1}\d{2,4}/i',
				'message' => 'Некорректно указана дата конца выборки',
			],
			[
				[ 'dateFrom', 'dateTo' ],
				'filter',
				'filter' => function( $v ) {

					if ( isset( $v ) ) {
						//для формата  2018-01-22
						if ( strpos( $v, '-' ) !== false ) {
							return substr( $v, 0, 10 );
						}
						//для формата  22.01.2018
						if ( strpos( $v, '.' ) !== false ) {
							list( $d, $m, $y ) = explode( '.', $v );

							return $y . '-' . $m . '-' . $d;
						}
					}

					return null;
				},
			],
		];
	}

	/**
	 * Получение списка заявок по ID с учетом пагинации
	 */
	public function getRequestWithPagination( array $requestsId ) {


		if ( ! $this->validate() ) {
			throw new \Exception( 'Входные данные не проходят валидацию. Повторите запрос с валидными данными или обратитесь к администратору.' );
		}
		if ( ! yii::$app->params['web']['request']['pagination']
		     || ! is_int( yii::$app->params['web']['request']['pagination'] ) ) {
			throw new \Exception( "Не заданы параметры пагинации в настройках сервера. Обратитесь к администратору." );
		}
		if ( $this->pagin < 1 ) {
			throw new \Exception( "Неверно указана страница пагинации, должна начинаться с 1." );
		}
		$this->pagin --;

		return Yii::$app->db->createCommand( '
		SELECT
			`r`.`id`,
			`r`.`code`,
			`r`.`created_at`,
			`r`.`num`,
			`r`.`summ`,
			`r`.`client_first_name`,
			`r`.`client_last_name`,
			`c`.`is_null`,
			`c`.`commission_cash_withdrawal`,
			`p`.`name` as `point_name`,
			`p`.`city` as `point_city`,
			`u`.`first_name` as `signer_first_name`,
			`u`.`last_name` as `signer_last_name`,
			`u`.`second_name` as `signer_second_name`
		FROM ' . $this->getTableName() . ' as `r`
		left join `request_commission` as `c` ON `r`.`id` = `c`.`request_id`
		left join `points` as `p` ON `r`.`point_id` = `p`.`id`		
		left join `users` as `u` ON `r`.`user_id` = `u`.`id`
			WHERE `r`.`id` IN (' . implode( ',', $requestsId ) . ')
				AND `r`.`created_at` BETWEEN \'' . $this->dateFrom . '\' AND \'' . $this->dateTo . ' 23:59:59\'
			ORDER BY `r`.`created_at` DESC
        LIMIT ' . yii::$app->params['web']['request']['pagination'] . '
        OFFSET ' . yii::$app->params['web']['request']['pagination'] * ( $this->pagin ) )
		                    ->queryAll();
	}



	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{requests}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"code",
			"created_at",
			"num",
			"client_first_name",
			"client_last_name",
			"status_id",
		];
	}
}