<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

/**
 * Class Regions - модель для работы со справочником Регионы регистрации
 *
 * @package app\modules\app\v1\models
 */
class Regions extends Model {
	/**
	 * Подключение трейта, отвечающего за запись данных в таблицу
	 */
	use WritableTrait;
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 */
	use ReadableTrait;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{regions}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"guid",
			"name",
			"kladr",
			"created_at",
			"updated_at",
			"active",
			"sort",
		];
	}

	public function getByName( $name = "" ) {

		if ( ! strlen( $name ) ) {
			return [];
		}

		return Yii::$app->db
			->createCommand( "select " . $this->prepareFieldsNames() . " from " . $this->getTableName() . " where [[name]]=:name", [ ":name" => $name ] )
			->queryOne();
	}

	/**
	 * Метод получения связей данных регионов
	 *
	 * @param string $key    - ключ для массива связей данных регионов
	 * @param array  $fields - поля, являющиеся значением в массиве связей
	 * @param bool   $trim   - флаг, указывающий на доп. обработку очищение от пробелов
	 *
	 * @return array|\Exception - вернет результирующий массив, взависимости от аргументов два вида:
	 *                            первый
	 *                            [
	 *                              '11':'Ингушетия респ'
	 *                            ]
	 *                            второй
	 *                            [
	 *                              '11': [
	 *                                      "name": "Ингушетия респ",
	 *                                      "kladr": "0600000000000"
	 *                                  ]
	 *                            ]
	 *                          при выбросе исключения вернет объект
	 */
	public function getRelation( string $key = '', array $fields = [], $trim = false ) {

		try {

			$key = empty( $key ) ? 'id' : $key;
			if ( ! in_array( $key, $this->getFieldsNames() ) ) {
				throw new \Exception( 'Некорректно указан ключ для массива связей регионов' );
			}
			if ( ! empty( $fields ) ) {
				$arIntersect = array_intersect( $this->getFieldsNames(), $fields );
				if ( count( $fields ) != count( $arIntersect ) ) {
					throw new \Exception( 'Для массива связей регионов некорректно указаны наименования полей для значений массива' );
				}
			}
			$fields = ! empty( $fields ) ? $fields : $this->getFieldsNames();
			if ( ! in_array( $key, $fields ) ) {
				$fields = array_merge( $fields, [ $key ] );
			}
			$strFields = '[[' . implode( "]],[[", $fields ) . ']]';
			$res       = Yii::$app->db->createCommand( 'SELECT ' . $strFields . ' FROM ' . $this->getTableName() )
			                          ->queryAll();
			if ( empty( $res ) ) {
				throw new \Exception( 'При попытке получения связей регионов произошла неизвестная ошибка, обратитесь к администратору' );
			}
			$data = [];
			foreach ( $res as $item ) {
				$k = empty( $trim ) ? $item[ $key ] : trim( $item[ $key ] );
				unset( $item[ $key ] );
				$v          = count( $item ) == 1
					? ( empty( $trim ) ? reset( $item ) : trim( reset( $item ) ) )
					: ( empty( $trim ) ? $item : array_map( function( $val ) {

						return trim( $val );
					}, $item ) );
				$data[ $k ] = $v;
			}

			return $data;
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}