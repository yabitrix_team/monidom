<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

class SessionVerify extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * Константа сценария создания записи о сессии
	 */
	const SCENARIO_CREATE = 'create';
	/**
	 * Константа сценария обновления заявки
	 */
	const SCENARIO_INC_ATTEMPT = 'inc_attempt';
	/*
	 * Идентификатор сессии
	 */
	public $session_id;
	/*
	 * Попытки отправки запроса
	 */
	public $attempt;
	/*
	 * Время оставшееся до окончания блокировки
	 */
	public $lock_expired_at;
	/*
	 * Время обновления записи
	 */
	public $updated_at;

	/**
	 * Метод установки сценария для модели при валидации полей
	 *
	 * @return array - вернет массив сценариев,
	 * по умолчанию установлен в сценарии установлен DEFAULT
	 */
	public function scenarios() {

		$scenarios                               = parent::scenarios();
		$scenarios[ self::SCENARIO_CREATE ]      = [ 'session_id' ];
		$scenarios[ self::SCENARIO_INC_ATTEMPT ] = [ 'session_id', 'attempt' ];

		return $scenarios;
	}

	public function rules() {

		return [
			[
				'session_id',
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'attempt',
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'session_id',
				'string',
				'message' => 'PHPSESSID заполнен не корректно',
			],
			[
				'attempt',
				'number',
				'message' => 'Поле заполнено не корректно',
			],
		];
	}

	/**
	 * Метод сохраняет запись с сессие операции ( создает или обновляет )
	 * если запись существует, то обновит запись, так как-будто только что созадал,
	 * то есть attempt = 0, lock_expired_at = NOW() + Interval
	 * нужно для заброшенных операций, то есть аналог create
	 *
	 * @return int|string - вернет 1 - при добавлении, 2 - при обновлении и строку при выбросе исключения
	 */
	public function save() {

		if ( isset( Yii::$app->params['web']['session']['lock_expired_at'] ) && is_int( Yii::$app->params['web']['session']['lock_expired_at'] ) ) {
			try {
				$insert                    = $update = $this->getAttributes();
				$insert['created_at']      = date( 'Y-m-d H:i:s', time() );
				$insert['attempt']         = $update['attempt'] = 0;
				$insert['lock_expired_at'] = $update['lock_expired_at'] =
					new \yii\db\Expression(
						'DATE_ADD(NOW(),INTERVAL \'00:0'
						. Yii::$app->params['web']['session']['lock_expired_at']
						. '\' HOUR_MINUTE)' );

				return Yii::$app->db
					->createCommand()
					->upsert( $this->getTableName(), $insert, $update )->execute();
			} catch ( \Exception $e ) {
				return $e->getMessage();
			}
		} else {
			return "Ошибка конфигурации сервера. Не задан период ограничения web-session-lock_expired_at. Обратитесь к администратору.";
		}
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{session_verify}}";
	}

	/**
	 * Метод увеличения количество попыток обращения к сервису по сессии за заданное время
	 *
	 * @params загружаем через Load
	 *
	 * @return int|array|string - количество обновленных строк|массив с ошибками валидации|строка исключения
	 */
	public function incAttempt() {

		try {
			$this->attempt = $this->attempt + 1;

			return $this->updateByFields( [ 'session_id' => $this->session_id ], [ 'attempt' => $this->attempt ] );
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected
	function getFieldsNames(): array {

		return [
			"id",
			"session_id",
			"attempt",
			"lock_expired_at",
			'created_at',
			'updated_at',
		];
	}
}