<?php

namespace app\modules\app\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;

class UserGroup extends Model {
	use WritableTrait;
	use ReadableTrait;
	public    $user_id;
	public    $group_id;
	protected $registerFieldCreate = [
		'user_id',
		'group_id',
	];

	public function rules() {

		return [
			[
				$this->registerFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'user_id',
				'number',
				'message' => 'Идентификатор пользователя указан некорректно',
			],
			[
				'group_id',
				'number',
				'message' => 'Идентификатор группы указан некорректно',
			],
		];
	}

	/**
	 * Метод создания связи пользователь - группа
	 *
	 * @param array $params
	 *
	 * @return int|false|string - id записи, ошибка, ошибка
	 */
	public function create( array $params = [] ) {

		try {
			$arPrepare = $this->prepareDataForExecute( $params );
			if ( ! empty( $arPrepare ) ) {
				$insert = Yii::$app->db
					->createCommand()
					->insert( 'user_group', $arPrepare )->execute();

				return ! empty( $insert ) ? intval( Yii::$app->db->getLastInsertID() ) : false;
			}
			throw new \Exception( 'Не переданы параметры для создания записи файла' );
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * М-д возвращает список групп пользователя
	 *
	 * @param null $uid
	 *
	 * @return bool|array - список ID групп пользователя
	 * @throws \yii\db\Exception
	 */
	public function byUserID( $uid = null ) {

		if ( isset( $uid ) ) {
			/* для возможности использовать код в процессах используется постоянное закрываемое подключение */
			$pdb = Yii::$app->pdb;
			$pdb->open();
			$result = $pdb->createCommand( "SELECT g.identifier
FROM user_group
  join groups g on user_group.group_id = g.id
WHERE user_id = :user_id", [ ':user_id' => $uid ] )
			              ->queryAll();
			$pdb->close();

			return $result;
		}

		return false;
	}

	/**
	 * М-д возвращает список групп пользователя (ПРЕДЫДУЩИЙ МЕТОД НЕ РАБОТАЕТ ИЗ_ЗА PDB)
	 *
	 * @param null $uid
	 *
	 * @return bool|array - список ID групп пользователя
	 * @throws \yii\db\Exception
	 */
	public function byUserIDHard( $uid = null ) {

		if ( isset( $uid ) ) {
			/* для возможности использовать код в процессах используется постоянное закрываемое подключение */
			$pdb = Yii::$app->db;
			//$pdb->open();
			$result = $pdb->createCommand( "SELECT g.identifier
FROM user_group
  join groups g on user_group.group_id = g.id
WHERE user_id = :user_id", [ ':user_id' => $uid ] )
			              ->queryAll();
			$pdb->close();

			return $result;
		}

		return false;
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{user_group}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"user_id",
			"group_id",
			"created_at",
		];
	}
}