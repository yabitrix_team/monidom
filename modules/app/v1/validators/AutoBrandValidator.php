<?php

namespace app\modules\app\v1\validators;

use app\modules\app\v1\models\AutoBrands;
use yii\validators\Validator;

/**
 * Class AutoBrandValidator - класс валидатор для справочника Марки авто,
 * проверяем на наличие в справочнике по идентификтору
 *
 * @package app\modules\app\v1\validators
 */
class AutoBrandValidator extends Validator {
	/**
	 * @var string - общее сообщение об ошибке
	 */
	public $message = 'Некорректно указано значение в поле "Марка автомобиля"';
	/**
	 * @var string - частное сообщение об ошибке
	 */
	public $msgNotEl = 'Указанное значение в поле "Марка автомобиля" отсутствует в системе';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function validateAttribute( $model, $attribute ) {

		$value = $model->$attribute;
		if ( ! is_numeric( $value ) ) {
			$this->addError( $model, $attribute, $this->message );

			return;
		}
		if ( empty( ( new AutoBrands )->getOne( $value ) ) ) {
			$this->addError( $model, $attribute, $this->msgNotEl );
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue( $value ) {

		if ( ! is_numeric( $value ) ) {
			return $this->message;
		}
		if ( empty( ( new AutoBrands )->getOne( $value ) ) ) {
			return [ $this->msgNotEl, [] ];
		}

		return null;
	}
}