<?php

namespace app\modules\app\v1\validators;

use app\modules\app\v1\models\Regions;
use yii\validators\Validator;

/**
 * Class RegionValidator - класс валидатор для справочника Регион регистрации,
 * проверяем на наличие в справочнике по идентификтору
 *
 * @package app\modules\app\v1\validators
 */
class RegionValidator extends Validator {
	/**
	 * @var string - общее сообщение об ошибке
	 */
	public $message = 'Некорректно указано значение в поле "Регион регистрации"';
	/**
	 * @var string - частное сообщение об ошибке
	 */
	public $msgNotEl = 'Указанное значение в поле "Регион регистрации" отсутствует в системе';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function validateAttribute( $model, $attribute ) {

		$value = $model->$attribute;
		if ( ! is_numeric( $value ) ) {
			$this->addError( $model, $attribute, $this->message );

			return;
		}
		if ( empty( ( new Regions )->getOne( $value ) ) ) {
			$this->addError( $model, $attribute, $this->msgNotEl );
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue( $value ) {

		if ( ! is_numeric( $value ) ) {
			return $this->message;
		}
		if ( empty( ( new Regions )->getOne( $value ) ) ) {
			return [ $this->msgNotEl, [] ];
		}

		return null;
	}
}