<?php
namespace app\modules\app\v1\validators;

use yii\validators\Validator;

/**
 * Class PhoneValidator - класс валидатор для формата телефона(он же логин)
 * задаем в одном месте централизовано
 *
 * @package app\modules\app\v1\validators
 */
class PhoneValidator extends Validator
{
    /**
     * @var string - общее сообщение об ошибке
     */
    public $message = 'Номер телефона указан в некорректном формате';
    /**
     * @var int  - шаблон для номера телефона
     */
    public $pattern = '/^9\d{9}$/iu';

    /**
     * @inheritdoc
     */
    public function init()
    {

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {

        $value = $model->$attribute;
        if (!is_numeric($value) || !preg_match($this->pattern, $value)) {
            $this->addError($model, $attribute, $this->message);

            return;
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {

        if (!is_numeric($value) || !preg_match($this->pattern, $value)) {
            return $this->message;
        }

        return null;
    }
}