<?php

namespace app\modules\app\v1\validators;

use app\modules\app\v1\models\Points;
use yii\validators\Validator;

/**
 * Class PointValidator - класс валидатор для справочника Марки авто,
 * проверяем на наличие в справочнике по идентификтору
 *
 * @package app\modules\app\v1\validators
 */
class PointValidator extends Validator {
	/**
	 * @var string - общее сообщение об ошибке
	 */
	public $message = 'Некорректно выбрано значение в поле "Точка"';
	/**
	 * @var string - частное сообщение об ошибке
	 */
	public $msgNotEl = 'Указанное значение в поле "Точка" отсутствует в системе';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function validateAttribute( $model, $attribute ) {

		$value = $model->$attribute;
		if ( ! is_numeric( $value ) && ! method_exists( $value, '__toString' ) ) {
			$this->addError( $model, $attribute, $this->message );

			return;
		}
		if ( empty( ( new Points() )->getOne( $value ) ) ) {
			$this->addError( $model, $attribute, $this->msgNotEl );
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue( $value ) {

		if ( ! is_numeric( $value ) ) {
			return $this->message;
		}
		if ( empty( ( new Points() )->getOne( $value ) ) ) {
			return [ $this->msgNotEl, [] ];
		}

		return null;
	}
}