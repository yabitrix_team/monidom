<?php

namespace app\modules\app\v1\validators;

use app\modules\app\v1\models\Products;
use yii\validators\Validator;

/**
 * Class CreditProductValidator - класс валидатор для справочника Кредитные продукты,
 * проверяем на наличие в справочнике по идентификтору
 *
 * @package app\modules\app\v1\validators
 */
class CreditProductValidator extends Validator {
	/**
	 * @var string - общее сообщение об ошибке
	 */
	public $message = 'Некорректно выбран кредитный продукт';
	/**
	 * @var string - частное сообщение об ошибке
	 */
	public $msgNotEl = 'Указанный кредитный продукт отсутствует в системе';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}

	/**
	 * @inheritdoc
	 */
	public function validateAttribute( $model, $attribute ) {

		$value = $model->$attribute;
		if ( ! is_numeric( $value ) ) {
			$this->addError( $model, $attribute, $this->message );

			return;
		}
		if ( empty( ( new Products )->getOne( $value ) ) ) {
			$this->addError( $model, $attribute, $this->msgNotEl );
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue( $value ) {

		if ( ! is_numeric( $value ) ) {
			return $this->message;
		}
		if ( empty( ( new Products )->getOne( $value ) ) ) {
			return [ $this->msgNotEl, [] ];
		}

		return null;
	}
}