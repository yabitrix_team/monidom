<?php

namespace app\modules\app\v1\commands;

use app\modules\app\v1\models\Users;
use yii\base\Exception;
use yii\console\Controller;

/**
 * Набор методов по работе с системой
 */
class UserController extends Controller {
	/**
	 * Изменение пароля для пользователя. Пример: php yii app/user/index [login] [new_password]
	 *
	 * @param $login
	 * @param $pass
	 */
	public function actionIndex( $login, $pass ): void {

		$model = new Users();
		try {
			$model->hashPassword( $pass );
			if ( 0 < $model->updateByUsername( $login, [
					'password_hash' => $model->password_hash,
				] ) ) {
				echo "\ncomplete\nfor login: '$login' set password '$pass'\n";
			} else {
				echo "\nnot set password\n";
			}
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
}
