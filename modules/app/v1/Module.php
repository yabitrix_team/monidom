<?php

namespace app\modules\app\v1;

use yii\base\BootstrapInterface;
use yii\console\Application;

/**
 * app_v1 module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\app\v1\controllers';

	public function bootstrap( $app ) {

		if ( $app instanceof Application ) {
			$this->controllerNamespace = 'app\modules\app\v1\commands';
		}
	}
}