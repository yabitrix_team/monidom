<?php
/**
 * Created by PhpStorm.
 * User: daglob
 * Date: 13.05.2018
 * Time: 15:56
 */

namespace app\modules\yii2;

use app\modules\app\v1\classes\logs\Log;

/**
 * @property \yii\db\Connection $db  The database connection. This property is read-only.
 * @property \yii\db\Connection $pdb The database persistent connection. This property is read-only.
 */
class Application extends \yii\console\Application {
    /**
     * @return null|object|\yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */

    protected $connectionVar = [
        'db',
        'pdb'
    ];

    public function getDb() {

        /** @var \yii\db\Connection $connection */
        $connection = $this->get( 'db' );
        try {
            $connection->createCommand( "DO 1;" )->execute();
            /*
            Log::error(
                ["text" => print_r(["DB : DO 1; - 1"], 1)],
                'rmq.201'
            );
            */
        } catch ( \yii\db\Exception $e ) {
            $connection->close();
            $connection->open();
            /*
            Log::error(
                ["text" => print_r(["DB : DO 1; - 2"], 1)],
                'rmq.201'
            );
            */
            echo "\nNew YII2 app getDB() -> reconnected\n" . PHP_EOL;
        }

        return $connection;
    }


    /**
     * @return \yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function getPdb() {

        /** @var \yii\db\Connection $connection */
        $connection = $this->get( 'pdb' );
        try {
            $connection->createCommand( "DO 1;" )->execute();
        } catch ( \yii\db\Exception $e ) {
            $connection->close();
            $connection->open();
            echo "\nNew YII2 app getPDB()->  reconnected\n" . PHP_EOL;
        }

        return $connection;
    }

    /**
     * @return \yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function getTdb() {

        /** @var \yii\db\Connection $connection */
        $connection = $this->get( 'tdb' );
        try {
            $connection->createCommand( "DO 1;" )->execute();
        } catch ( \yii\db\Exception $e ) {
            $connection->close();
            $connection->open();
            echo "\nNew YII2 app getTdb()->  reconnected\n" . PHP_EOL;
        }

        return $connection;
    }
}