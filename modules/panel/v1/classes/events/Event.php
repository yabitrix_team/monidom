<?php
namespace app\modules\panel\v1\classes\events;

abstract class Event
{
    protected $requestEvents;

    public function __construct(array $requestEvents)
    {
        $this->requestEvents = $requestEvents;
    }

    protected function inProgress(){
        return !empty($this->requestEvents) && end($this->requestEvents)["status_code"] == "wait";
    }

    protected function hasSent(){
        return !empty($this->requestEvents);
    }

    public function __call($name, $arguments )
    {
        return false;
    }

    public function onEmpty()
    {
        return false;
    }

    public function onNotFull()
    {
        return false;
    }

    public function onPreApproving()
    {
        return false;
    }

    public function onPreApproved()
    {
        return false;
    }

    public function onDocsChecking()
    {
        return false;
    }

    public function onDocsChecked()
    {
        return false;
    }

    public function onFinalApproving()
    {
        return false;
    }

    public function onFinalApproved()
    {
        return false;
    }

    public function onContractSigning()
    {
        return false;
    }

    public function onMoneyTake()
    {
        return false;
    }

    public function onMoneyIssued()
    {
        return false;
    }

    public function onContractCompleted()
    {
        return false;
    }

    public function onDenied()
    {
        return false;
    }

    public function onRequestCancelled()
    {
        return false;
    }

    public function onContractCancelled()
    {
        return false;
    }
}