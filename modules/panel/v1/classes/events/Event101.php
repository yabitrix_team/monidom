<?php
namespace app\modules\panel\v1\classes\events;

class Event101 extends Event
{
    public function onFinalApproving()
    {
        return !$this->hasSent() && !$this->inProgress();
    }

    public function onDocsChecked()
    {
        return !$this->hasSent() && !$this->inProgress();
    }
}