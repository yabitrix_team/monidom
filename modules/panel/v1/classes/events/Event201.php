<?php
namespace app\modules\panel\v1\classes\events;

class Event201 extends Event
{
    private function hasSent101()
    {
        return true;
    }

    public function onFinalApproving()
    {
        return $this->hasSent101() && !$this->inProgress();
    }
}