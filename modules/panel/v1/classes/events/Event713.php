<?php
namespace app\modules\panel\v1\classes\events;

class Event713 extends Event
{
    public function onPreApproved()
    {
        return !$this->inProgress();
    }

    public function onDocsChecking()
    {
        return !$this->inProgress();
    }
}