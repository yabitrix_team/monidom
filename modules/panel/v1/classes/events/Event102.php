<?php
namespace app\modules\panel\v1\classes\events;

class Event102 extends Event
{
    public function onFinalApproved()
    {
        return !$this->hasSent() && !$this->inProgress();
    }

    public function onContractSigning()
    {
        return !$this->hasSent() && !$this->inProgress();
    }
}