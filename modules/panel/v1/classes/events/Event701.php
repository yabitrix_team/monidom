<?php
namespace app\modules\panel\v1\classes\events;

class Event701 extends Event
{
    public function onEmpty()
    {
        return !$this->inProgress();
    }
}