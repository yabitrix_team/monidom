<?php
namespace app\modules\panel\v1\classes\events;

class Event702 extends Event
{
    public function onPreApproving()
    {
        return !$this->inProgress();
    }
}