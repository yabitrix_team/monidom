<?php
namespace app\modules\panel\v1\classes\events;

use Yii;

class EventAccess
{
    private $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    private function getStatusCode($guid)
    {
        return str_replace(
            " ",
            "",
            ucfirst(str_replace("_", " ", array_flip(Yii::$app->params["core"]["statuses"])[$guid]))
        );
    }

    public function get($guid = "")
    {
        return $this->event->{"on".$this->getStatusCode((string) $guid)}();
    }
}