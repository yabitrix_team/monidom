<?php
namespace app\modules\panel\v1\classes\events;

class Event202 extends Event
{
    private function hasSent102()
    {
        return true;
    }

    public function onFinalApproved()
    {
        return $this->hasSent102() && !$this->inProgress();
    }

    public function onContractSigning()
    {
        return $this->hasSent102() && !$this->inProgress();
    }
}