<?php
namespace app\modules\panel\v1\classes\events;

class Event712 extends Event
{
    public function onDocsChecked()
    {
        return !$this->inProgress();
    }

    public function onFinalApproving()
    {
        return !$this->inProgress();
    }

}