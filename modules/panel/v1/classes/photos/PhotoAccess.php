<?php
namespace app\modules\panel\v1\classes\photos;

use Yii;

class PhotoAccess
{
    private function getStatusCode($guid)
    {
        return str_replace(
            " ",
            "",
            ucfirst(str_replace("_", " ", array_flip(Yii::$app->params["core"]["statuses"])[$guid]))
        );
    }

    public function get($guid = "")
    {
        return $this->{"on".$this->getStatusCode((string) $guid)}();
    }

    public function __call($name, $arguments )
    {
        return false;
    }

    public function onPreApproved()
    {
        return true;
    }

    public function onDocsChecking()
    {
        return true;
    }

    public function onPreApproving()
    {
        return true;
    }

    public function onDocsChecked()
    {
        return true;
    }

    public function onFinalApproving()
    {
        return true;
    }
}
