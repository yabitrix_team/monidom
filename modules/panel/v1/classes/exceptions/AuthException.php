<?php
namespace app\modules\panel\v1\classes\exceptions;

/**
 * Исключение по авторизации
 *
 * @package app\modules\panel\v1\classes\exceptions
 */
class AuthException extends \Exception implements \Throwable
{
}