<?php
namespace app\modules\panel\v1\classes\exceptions;

/**
 * Исключение по пользователю
 *
 * @package app\modules\panel\v1\classes\exceptions
 */
class UserException extends \Exception implements \Throwable
{
}