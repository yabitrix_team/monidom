<?php

namespace app\modules\panel\v1;
/**
 * Class Module - модуль для работы с Панелью управления
 *
 * @package app\modules\panel\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\panel\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
