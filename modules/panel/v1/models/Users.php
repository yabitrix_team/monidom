<?php

namespace app\modules\panel\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\models\Users as UsersApp;
use Yii;

class Users extends UsersApp {
	use ReadableTrait;
	use WritableTrait;

	public function rules() {

		return [
			[
				$this->usersFieldCreate,
				'required',
				'message' => 'Поле обязательно для заполнения',
			],
			[
				'id',
				'number',
				'message' => 'Поле "Идентификатор" не является числом',
			],
			[
				'auth_key',
				'string',
				'message' => 'Некорректно задан формат для ключа аутинтефикации',
			],
			[
				'username',
				'match',
				'pattern' => '/^([a-z][\w\-]+|9[0-9]{9})$/iu',
				'message' => 'Логин указан в некорректном формате',
			],
			[
				'email',
				'email',
				'message' => 'Поле "Электронная почта" заполнено некорректно',
			],
			[
				'first_name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Имя" должно содержать минимум 2 символа',
			],
			[
				'first_name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Имя" заполнено некорректно',
			],
			[
				'last_name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Фамилия" заполнено некорректно',
			],
			[
				'last_name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Фамилия" должно содержать минимум 2 символа',
			],
			[
				'second_name',
				'match',
				'pattern' => '/^[а-яё\s-]+$/iu',
				'message' => 'Поле "Отчество" заполнено некорректно',
			],
			[
				'second_name',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Поле "Отчество" должно содержать минимум 2 символа',
			],
			[
				'is_accreditated',
				'boolean',
				'trueValue'  => true,
				'falseValue' => false,
				'strict'     => false,
			],
			[
				'inside_support_token',
				'string',
				'length'   => [ 2 ],
				'tooShort' => 'Неверный токен',
			],
			[
				'password_reset_token',
				'string',
				'length'   => [ 36 ],
				'tooShort' => 'Токен сброса пароля неверный',
			],
			[
				'request_mode',
				'in',
				'range'   => [ 'photos', 'full', '' ],
				'message' => 'Не верно установлен режим сохранения заявки: request_mode',
			],
			[
				'password',
				'match',
				'pattern' => "/^(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,20}$/iu",
				'message' => 'Пароль должен содержать минимум одну заглавную, одну строчную буквы, одну цифру, один спец. символ и быть длиной не меннее 6 символов',
			],
		];
	}

	public function updateUsername( $username, $newUsername ) {
		$isAlreadyReg = Yii::$app->db->createCommand( 'SELECT `id` FROM ' . $this->getTableName() . ' WHERE [[username]]= :username', [ ':username' => $newUsername] )->queryAll();
		if(! empty($isAlreadyReg)) {
			throw new \Exception('Пользователь с таким username уже зарегистрирован');
		} else {
			$valid = $this->validate(['username']);
			if ($valid) {
				return $this->updateByUsername($username, ['username' => $newUsername]);
			}
			else {
				throw new \Exception('Ошибка валидации');
			}
		}
	}
}