<?php
namespace app\modules\panel\v1\models;

use app\modules\app\v1\models\Events as AppEvents;
use Yii;

class Events extends AppEvents
{
    /**
     * @param bool $active
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getSendable()
    {
        return Yii::$app->db->createCommand(
            "select [[".
            implode("]],[[", $this->getFieldsNames()).
            "]] from ".
            $this->getTableName().
            " where [[active]] = 1 and code not like '8%' order by [[sort]] asc"
        )
            ->queryAll();
    }
}