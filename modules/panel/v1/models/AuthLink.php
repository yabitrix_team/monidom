<?php

namespace app\modules\panel\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;

class AuthLink extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * @var int ID
	 */
	public $id;
	/**
	 * @var string уникальный hash для адреса ссылки авторизации
	 */
	public $hash;
	/**
	 * @var string ID пользователя, под которым будет авторизация
	 */
	public $user_id;
	/**
	 * @var bool активность
	 */
	public $active;
	/**
	 * @var string дата создания
	 */
	public $created_at;
	/**
	 * @var string дата обновления
	 */
	public $updated_at;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'hash', 'user_id' ], 'required' ],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{auth_link}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"hash",
			"user_id",
			"active",
			"created_at",
			"updated_at",
		];
	}
}