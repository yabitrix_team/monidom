<?php

namespace app\modules\panel\v1\models;

use app\modules\client\v1\models\Requests as Requests;
use yii\filters\AccessControl;
use yii\web\JsonParser;
use Yii;

class Lead extends Requests {

    public function getAllWithPagination($page = 1, $size = 20)
    {
        return Yii::$app->db->createCommand("SELECT * FROM `leads` ORDER BY [[created_at]] DESC limit " .(($page - 1) * $size).", ".$size)->queryAll();
    }

    public function getCount()
    {
        return Yii::$app->db->createCommand("SELECT count(*) cnt from `leads`")->queryOne()['cnt'];
    }
}