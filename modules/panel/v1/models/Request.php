<?php
namespace app\modules\panel\v1\models;

use app\modules\panel\v1\classes\events\EventAccess;
use app\modules\app\v1\models\Requests as RequestsApp;
use app\modules\app\v1\models\RequestsEvents;
use ReflectionClass;
use Yii;

class Request extends RequestsApp
{
    private $eventsStatusNames = [
        "wait"  => "Ожидается отправка",
        "sent"  => "Отправлено",
        "error" => "Ошибка отправки",
    ];

    public function get($num, $guid, $num_1c)
    {
        if ($num) {
            $where = "r.num=:num";
            $vars  = [":num" => $num];
        } elseif ($guid) {
            $where = "r.guid=:guid";
            $vars  = [":guid" => $guid];
        } elseif ($num_1c) {
            $where = "r.num_1c=:num_1c";
            $vars  = [":num_1c" => $num_1c];
        }
        try {
            return Yii::$app->db->createCommand(
                "select r.id, r.created_at, r.num, r.guid, r.id, r.point_id, r.client_id, r.requests_origin_id, r.is_pep, r.user_id, r.summ, r.num_1c, r.updated_at, r.client_first_name, r.client_mobile_phone, r.client_last_name, r.client_patronymic,
e.id status_id,
client.username customer_username, client.first_name customer_first_name, client.last_name customer_last_name, client.second_name customer_patronymic,
partner.username partner_username, partner.first_name partner_first_name, partner.last_name partner_last_name, partner.second_name partner_patronymic,
point.name point_name,
origin.name_1c origin_name1c
from requests r
left join requests_events e on r.id=e.request_id
left join users client on r.client_id=client.id
left join users partner on r.user_id=partner.id
left join points point on r.point_id=point.id
left join requests_origin origin on r.requests_origin_id=origin.id
where ".$where,
                $vars
            )->queryOne();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getRequestEvents($id, $status)
    {
        $requestEvents = RequestsEvents::instance()->getByRequestId($id);
        $allEvents     = array_map(
            function ($arEvent) use ($requestEvents, $status) {
                $list = [];
                foreach ($requestEvents as $requestEvent) {
                    if ($arEvent["id"] == $requestEvent["event_id"]) {
                        $statusCode = $requestEvent["is_sended"]
                            ? "sent"
                            : ($requestEvent["is_ignored"]
                                ? "error" : "wait");
                        $list[]     = [
                            "id"          => $requestEvent["id"],
                            "created_at"  => $requestEvent["created_at"],
                            "updated_at"  => $requestEvent["updated_at"],
                            "status_code" => $statusCode,
                            "status"      => $this->eventsStatusNames[$statusCode],
                            "error"       => (string) $requestEvent["ignore_reason"],
                        ];
                    }
                }
                try {
                    $bAccess = (new EventAccess(
                        (new ReflectionClass(
                            'app\modules\panel\v1\classes\events\Event'.$arEvent["code"]
                        ))->newInstance($list)
                    ))->get($status);
                } catch (\Exception $e) {
                    $bAccess = false;
                }

                return [
                    "id"     => $arEvent["id"],
                    "code"   => $arEvent["code"],
                    "name"   => $arEvent["name"],
                    "list"   => $list,
                    "access" => $bAccess,
                ];
            },
            Events::instance()->getSendable()
        );

        return $allEvents;
    }
}
