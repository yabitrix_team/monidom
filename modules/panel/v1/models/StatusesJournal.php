<?php
namespace app\modules\panel\v1\models;

use app\modules\app\v1\models\StatusesJournal as StatusesJournalApp;
use Yii;

/**
 * Class StatusesJournal - модель для работы с таблице связей заявки со статусами
 *
 * @package app\modules\app\v1\models
 */
class StatusesJournal extends StatusesJournalApp
{
    /**
     * @param null $RID
     * @param bool $getName
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getCurrentByRequestID($RID = null)
    {
        return Yii::$app->db
            ->createCommand(
                'select j.created_at, j.updated_at, j.id, s.name, s.color, s.guid
 from '.self::getTableName().' j
 left join statuses s on j.status_id=s.id
 WHERE [[request_id]] = :request_id ORDER BY [[created_at]] DESC LIMIT 1',
                ['request_id' => $RID]
            )
            ->queryOne();
    }
}
