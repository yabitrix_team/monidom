<?php
namespace app\modules\panel\v1\controllers\user;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use app\modules\panel\v1\classes\exceptions\AuthException;
use app\modules\panel\v1\models\AuthLink;
use Exception;
use Yii;
use yii\filters\AccessControl;
use app\modules\app\v1\models\Users;
use yii\web\JsonParser;

class AuthController extends AuthControllerApp
{
    /**
     * @return array
     * @throws \Throwable
     */
    public function actionLink()
    {
        try {
            $arRequest = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (
                !strlen($arRequest["username"]) || (!$arUser = Users::instance()
                    ->getOneByFilter(["username" => $arRequest["username"]]))
            ) {
                throw new AuthException("Пользователь не найден");
            }
            $hash     = md5(Yii::$app->security->generateRandomString().$arUser["id"].time().microtime());
            $authLink = new AuthLink(
                ["hash" => $hash, "user_id" => $arUser["id"], "active" => 1]
            );
            if (!$authLink->validate()) {
                return $this->getClassAppResponse()::get(false, $authLink->getErrors());
            }
            if (!$idAuthLink = $authLink->add($authLink->getAttributes())) {
                throw new AuthException("Ошибка создания ссылки авторизации");
            }
            Log::info(
                [
                    "text"       => "id: ".$arUser["id"].", username: ".$arRequest["username"],
                    "user_login" => Yii::$app->user->getIdentity()->getUserName(),
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "user.link.get"
            );

            return $this->getClassAppResponse()::get(
                "http".($_SERVER["HTTPS"]
                    ? 's'
                    :
                    '')."://login.".Yii::$app->params['core']['domain']."/web/v1/user/auth/$hash"
            );
        } catch (AuthException $e) {
            Log::error(
                [
                    "text"       => "id: ".$arUser["id"].", username: ".$arRequest["username"]."\n".$e->getMessage(),
                    "user_login" => Yii::$app->user->getIdentity()->getUserName(),
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "user.link.get"
            );
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        } catch (Exception $e) {
            Log::error(
                [
                    "text"       => "id: ".$arUser["id"].", username: ".$arRequest["username"]."\n".$e->getMessage(),
                    "user_login" => Yii::$app->user->getIdentity()->getUserName(),
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "user.link.get"
            );
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['create'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['index', 'delete', 'link'],
                        'roles'   => ['role_support', 'role_admin'],
                    ],
                ],
                'denyCallback' => function () {

                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
