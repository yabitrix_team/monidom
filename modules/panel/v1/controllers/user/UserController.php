<?php

namespace app\modules\panel\v1\controllers\user;

use app\modules\app\v1\controllers\user\UserController as UserControllerApp;
use app\modules\app\v1\models\Groups;
use app\modules\panel\v1\classes\exceptions\UserException;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\panel\v1\models\Users;
use app\modules\app\v1\models\UserGroup;
use Exception;
use Yii;
use yii\web\JsonParser;
use app\modules\app\v1\classes\logs\Log;

class UserController extends UserControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
	public function actionCreate() {

		try {
			$transaction = Yii::$app->db->beginTransaction();
			$arRequest   = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
			if ( Users::getByLogin( $arRequest['username'] ) ) {
				throw new UserException( 'Пользователь с этим номером телефона уже зарегистрирован' );
			}
			if ( ! $arGroup = Groups::getGroupByIdentifier( $arRequest['role'] ) ) {
				throw new UserException( 'Группа с указанным идентификатором не найдена' );
			}
			//Создаем пользователя для сохранения в БД
			$user = new Users( [ 'scenario' => Users::SCENARIO_CREATE ] );
			if ( ! $user->load( [
				                    'username'    => $arRequest['username'],
				                    'email'       => $arRequest['email'],
				                    'first_name'  => $arRequest['first_name'],
				                    'last_name'   => $arRequest['last_name'],
				                    'second_name' => $arRequest['second_name'],
			                    ], ''
			) ) {
				throw new UserException( 'Не удалось загрузить данные для нового пользователя' );
			}
			$user->hashPassword( $arRequest['password'] );
			if ( ! $user->validate() ) {
				return $this->getClassAppResponse()::get( false, $user->firstErrors );
			}
			$result = $user->create();
			if ( ! intval( $result ) && is_string( $result ) ) {
				throw new UserException( $result );
			} elseif ( $result == false ) {
				throw new UserException( "Не удалось создать пользователя" );
			}
			$userGroup = new UserGroup();
			if ( ! $userGroup->load( [
				                         'user_id'  => $result,
				                         'group_id' => $arGroup['id'],
			                         ], '' ) ) {
				throw new UserException( "Не удалось загрузить данные группы нового пользователя" );
			}
			if ( ! $userGroup->validate() ) {
				return $this->getClassAppResponse()::get( false, $userGroup->firstErrors );
			}
			$groupID = $userGroup->create();
			if ( ! intval( $groupID ) ) {
				if ( $groupID == false ) {
					throw new UserException( "Не удалось сохранить связку пользователь-группа. Обратитесь к администратору" );
				}
				throw new UserException( $groupID );
			}
			$transaction->commit();

			return $this->getClassAppResponse()::get( $result );
        } catch ( UserException $e ) {
            $transaction->rollBack();
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
		} catch ( Exception $e ) {
			$transaction->rollBack();
            throw new Exception($e->getMessage(), $e->getCode(), $e);
		}
	}

    /**
     * @param string $username
     *
     * @return array
     * @throws \Exception
     */
    public function actionUpdate($username = '') {
		try {
			$arRequest   = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
			if (empty($arRequest['username']) || empty($arRequest['new_username'])) {
				throw new UserException('Не указан username или new_username');
			}
			if ($arRequest['username'] === $arRequest['new_username']) {
				throw new UserException('Переданные значения равны');
			}
			$model = new Users(['username' => $arRequest['new_username']]);
			if ($model->updateUsername($arRequest['username'], $arRequest['new_username'])) {
                Log::info( [
                    "user_id"    => Yii::$app->user->getId(),
                    "user_login" => $arRequest['new_username'],
                    "user_login_old" => $arRequest['username'],
                ], "user.change.login" );

                return $this->getClassAppResponse()::get( ['update' => true] );
			} else {
                throw new UserException('Пользователя с таким username не найдено');
			}
        } catch (UserException $e) {
            Log::error( [
                "user_id"    => Yii::$app->user->getId(),
                "user_login" => $arRequest['new_username'],
                "user_login_old" => $arRequest['username'],
                "text" => $e->getMessage(),
            ], "user.change.login" );

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
		} catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
		}
	}

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionList() {

        $page = (int)Yii::$app->request->get("page") ?: 1;
        $size = (int)Yii::$app->request->get("size") ?: 20;
        $filters = ( new JsonParser() )->parse(Yii::$app->request->get('filters'), 'json') ?: [];
        $sort = ( new JsonParser() )->parse(Yii::$app->request->get('sort'), 'json') ?: [];
        $cntAll  = (int) Yii::$app->db->createCommand( "select count(*) cnt from users" )->queryOne()["cnt"];
        $userModel = new Users();

        $users = [];
        if(empty($filters) && empty($sort)) {
            $users = Yii::$app->db->createCommand("
              select u.`id`, `username`, `email`, `first_name`, u.`active`, `last_name`, `second_name`, g.`identifier` role
                from `users` u
                left join `user_group` ug
                    on u.`id` = ug.`user_id`
                left join `groups` g
                    on ug.`group_id` = g.`id`
              limit ".(($page - 1) * $size).", ".$size
            )->queryAll();
        } else {
            $users = $userModel->getAllByFilter($filters, $sort, ($page - 1) * $size, $size);
            if (!empty($filters)) {
                $cntAll = $userModel->getCountByFilter($filters);
            }
        }

        return $this->getClassAppResponse()::get( [
                                                      "page" => $page,
                                                      "size" => $size,
                                                      "count" => ceil($cntAll / $size),
                                                      "items" => $users,
                                                  ] );
	}

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'create',
						],
						'roles'   => [ 'role_admin', 'role_support' ],
					],
					[
						'allow'   => true,
						'actions' => [
							'index', 'list', 'update'
						],
						'roles'   => [ 'role_admin', 'role_support' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}
}
