<?php
namespace app\modules\panel\v1\controllers\request;

use app\modules\app\v1\models\File;
use app\modules\panel\v1\classes\photos\PhotoAccess;
use app\modules\panel\v1\models\StatusesJournal;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\rest\Controller;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\panel\v1\models\Request;
use yii\web\JsonParser;

class FileController extends Controller
{
    use TraitConfigController;

    protected function getBinds()
    {

        return [
            "foto_sts"      => "Изображения СТС",
            "foto_pts"      => "Изображения ПТС",
            "foto_passport" => "Изображения паспорта",
            "foto_card"     => "Изображения банковской карты",
            "foto_auto"     => "Изображения автомобиля",
            "foto_client"   => "Изображения клиента",
        ];
    }

    public function actionIndex(int $id)
    {
        try {
            $requestModel = new Request();
            $files        = $requestModel->getFiles(
                $id,
                [],
                ['foto_sts', 'foto_pts', 'foto_auto', 'foto_passport', 'foto_card', 'foto_client', 'foto_extra']
            );
            if ($files) {
                $clientPhotoTypes = ["foto_auto", "foto_client", "foto_card"];
                $docPhotoTypes    = ["foto_sts", "foto_pts", "foto_passport", "foto_card"];
                foreach ($files as $fileType => $photos) {
                    foreach ($photos as $i => $photo) {
                        $files[$fileType][$i]["group"] = $this->getBinds()[$fileType];
                    }
                }
                $requestStatus = StatusesJournal::instance()->getCurrentByRequestID($id);

                return $this->getClassAppResponse()::get(
                    [
                        'files'  => $files,
                        'types'  => [
                            'client' => $clientPhotoTypes,
                            'doc'    => $docPhotoTypes,
                        ],
                        'access' => (is_array($requestStatus) && (new PhotoAccess())->get($requestStatus["guid"])),
                    ]
                );
            } else {
                throw new \Exception('Файлы по заявке не найдены');
            }
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * @return array
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDelete()
    {
        $code = (new JsonParser())->parse(\Yii::$app->request->getRawBody(), 'json')['code'];
        try {
//            $deleted = \Yii::$app->file->delete($code);
            $model = new File();
            $file  = $model->getByCode($code);
            $deleted = $model->deleteOne($file['id']);
            if (is_numeric($deleted)) {
                return $this->getClassAppResponse()::get(
                    [
                        'deleted' => boolval($deleted),
                    ]
                );
            }
            throw new \Exception($deleted);
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class'        => AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                            'delete',
                        ],
                        'roles'   => ['role_admin', 'role_support'],
                    ],
                ],
                'denyCallback' => function () {

                    if (Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнение операции.');
                    }
                },
            ],
        ];
    }
}
