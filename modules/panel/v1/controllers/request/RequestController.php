<?php
namespace app\modules\panel\v1\controllers\request;

use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use app\modules\panel\v1\models\Request;
use app\modules\panel\v1\models\StatusesJournal;
use app\modules\web\v1\classes\traits\TraitConfigController;

class RequestController extends Controller
{
    use TraitConfigController;

    public function actionIndex($num = null, $guid = null, $num_1c = null)
    {
        try {
            $model         = new Request();
            $statusesModel = new StatusesJournal();
            if ($request = $model->get($num, $guid, $num_1c)) {
                $request['last_status'] = $statusesModel->getCurrentByRequestID($request["id"]);

                return $this->getClassAppResponse()::get(
                    [
                        'request' => $request,
                        'events'  => $model->getRequestEvents($request['id'], $request['last_status']['guid']),
                    ]
                );
            }
            throw new \Exception('Заявка не найдена');
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors() {

        return [
            'access' => [
                'class'        => AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                        ],
                        'roles'   => [ 'role_admin', 'role_support' ],
                    ],
                ],
                'denyCallback' => function() {

                    if ( Yii::$app->user->isGuest ) {
                        throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
                    } else {
                        throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
                    }
                },
            ],
        ];
    }
}
