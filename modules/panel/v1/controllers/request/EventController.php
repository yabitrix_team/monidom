<?php
namespace app\modules\panel\v1\controllers\request;

use app\modules\app\v1\controllers\request\EventController as EventControllerApp;
use app\modules\app\v1\models\RequestsEvents;
use Yii;
use yii\filters\AccessControl;
use yii\web\JsonParser;

/**
 * Class EventController - контроллер для работы событиями заявки
 *
 * @package app\modules\client\v1\controllers\request
 */
class EventController extends EventControllerApp
{
    /**
     * Экшен создания события прикрепленного к заявке
     *
     * @return array - Вернет сформированный массив
     */
    public function actionCreate()
    {
        try {
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (!$reqBody['request_id']) {
                throw new \Exception('Не указана заявка');
            }
            if (!$reqBody['event_id']) {
                throw new \Exception('Не указано событие');
            }
            $modelRequestsEvents = new RequestsEvents(
                [
                    'request_id' => (int) $reqBody['request_id'],
                    'event_id'   => (int) $reqBody['event_id'],
                    'is_ignored' => 0,
                ]
            );
            if (!$modelRequestsEvents->validate()) {
                return $this->getClassAppResponse()::get(false, $modelRequestsEvents->firstErrors);
            }
            $result = $modelRequestsEvents->create();
            if (!is_numeric($result)) {
                throw new \Exception('Произошла ошибка при попытке прикрепления события к заявке '.$result);
            }
            return $this->getClassAppResponse()::get(["success" => true]);
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class'        => AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                        ],
                        'roles'   => ['role_admin', 'role_support'],
                    ],
                ],
                'denyCallback' => function () {

                    if (Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнение операции.');
                    }
                },
            ],
        ];
    }
}
