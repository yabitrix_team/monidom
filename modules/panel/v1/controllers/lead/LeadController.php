<?php

namespace app\modules\panel\v1\controllers\lead;

use \app\modules\app\v1\controllers\lead\LeadController as LeadControllerApp;
use Exception;
use Yii;
use yii\filters\AccessControl;
use app\modules\panel\v1\models\Lead;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\classes\traits\TraitConfigController;

/**
 * Class LeadController - контроллер по работе с лидами в панеле
 *
 * @package app\modules\panel\v1\controllers\lead
 */
class LeadController extends LeadControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	/**
	 * Получение списка лидов
	 *
	 * @return array
	 */
	public function actionIndex() {

        $page   = (int) Yii::$app->request->get( "page" ) ?: 1;
        $size   = (int) Yii::$app->request->get( "size" ) ?: 20;
        $model  = new Lead();
        $cntAll = $model->getCount();
        $leads  = $model->getAllWithPagination( $page, $size );

        return $this->getClassAppResponse()::get( [
                                                      "page"  => $page,
                                                      "size"  => $size,
                                                      "count" => ceil( $cntAll / $size ),
                                                      "items" => $leads,
                                                  ] );
	}

    /**
     * Получение статуса отправленного СМС для лида
     *
     * @param $guid - строковый идентификатор смс сообщения
     *
     * @return array
     * @throws \Exception
     */
	public function actionByGuid( $guid ) {

        $model = new SendingSMS( [ 'scenario' => SendingSMS::SCENARIO_GET_STATUS ] );
        $model->load( [
                          'sms_id' => $guid,
                      ], '' );
        $res = $model->getStatus();
        if ( is_a( $res, SendingSMS::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }
        if ( is_a( $res, \Exception::class ) ) {
            throw $res;
        }
        $status = key_exists( $res, SendingSMS::getStatusesSMS() ) ? SendingSMS::getStatusesSMS()[ $res ] :
            'Статус не определен в системе';

        return $this->getClassAppResponse()::get( [
                                                      'status' => $status,
                                                  ] );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'access' => [
				'class'        => AccessControl::class,
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'by-guid',
						],
						'roles'   => [ 'role_admin', 'role_support' ],
					],
				],
				'denyCallback' => function() {

					if ( Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}
}
