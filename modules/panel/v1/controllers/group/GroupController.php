<?php

namespace app\modules\panel\v1\controllers\group;

use app\modules\app\v1\models\Groups;
use Exception;
use yii\rest\Controller;
use app\modules\web\v1\classes\traits\TraitConfigController;

class GroupController extends Controller {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionIndex() {

        if ( ! $arGroups = Groups::instance()->getAll() ) {
            throw new Exception( "Ошибка получения списка групп пользователей" );
        }

        return $this->getClassAppResponse()::get( array_map(function($arGroup) {
            return [
                "id" => $arGroup["id"],
                "identifier" => $arGroup["identifier"],
                "name" => $arGroup["name"],
            ];
        }, $arGroups) );
	}

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_admin', 'role_support' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}
}
