<?php

namespace app\modules\payment\v1;

use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;
use yii\console\Application;

/**
 * Exchange module definition class
 */
class Module extends BaseModule implements BootstrapInterface {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\payment\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
		// custom initialization code goes here
	}

	public function bootstrap( $app ) {

		if ( $app instanceof Application ) {

			$this->controllerNamespace = 'app\modules\v1\payment\commands';
		}
	}
}
