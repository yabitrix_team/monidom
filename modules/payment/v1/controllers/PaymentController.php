<?php

namespace app\modules\payment\v1\controllers;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Currency;
use app\modules\payment\v1\models\base\PayFactory;
use app\modules\payment\v1\models\base\PayMethods;
use app\modules\payment\v1\models\base\Vendor;
use app\modules\payment\v1\models\interfaces\PayActionsInterfaces;
use Exception;
use SoapClient;
use yii\base\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\JsonParser;
use yii\web\Response;
use yii;

/**
 * Обработчики платежей
 *
 * @package app\modules\payment\v1\controllers
 */
class PaymentController extends Controller {
	/**
	 * @inheritdoc
	 */
	use TraitConfigController;
	/**
	 * @var SoapClient экземпляр SOAP клиента
	 */
	protected $soapClient;
	/**
	 * @var int - значение тайм-аута  в секундах по умолчанию для потоков, использующих сокеты
	 */
	public $defaultSocketTimeout = 60;
	/**
	 * @var int - значение тайм-аута  в секундах для потоков, использующих сокеты,
	 *          в классе modules/notification/classes/WSRedis.php:34 выставлятеся
	 *          значение -1 для корректной работы Редис, необходимо для SOAP
	 *          переопределять значение на дефолтное и потом возвращать на то, что
	 *          было указано до вызова SOAP
	 */
	protected $socketTimeout;

	/**
	 * @inheritdoc
	 */
	public function init() {

		$params              = [
			'login'      => yii::$app->params['payment']['mfo_service']['login'],
			'password'   => yii::$app->params['payment']['mfo_service']['password'],
			'exceptions' => 1,
			'trace'      => 1,
			'cache_wsdl' => WSDL_CACHE_NONE,
		];
		$this->socketTimeout = ini_get( 'default_socket_timeout' );
		if ( substr( yii::$app->params['payment']['mfo_service']['url'], 0, 5 ) == 'https' ) {
			ini_set( 'default_socket_timeout', $this->defaultSocketTimeout );
			$params['stream_context'] = stream_context_create( [
				                                                   'ssl' => [
					                                                   'verify_peer'       => false,
					                                                   'verify_peer_name'  => false,
					                                                   'allow_self_signed' => true,
				                                                   ],
			                                                   ] );
		}
		$this->soapClient = new SoapClient( yii::$app->params['payment']['mfo_service']['url'], $params );
		parent::init();
	}

	/**
	 * Преобразование номера телефона из МФО в строку
	 *
	 * @param $phone null|array массив с телефоном
	 *
	 * @return string форматированный номер телефона
	 */
	protected function phoneFormat( $phone ) {

		if ( ! is_null( $phone ) ) {
			return "+ 7(" . $phone[0] . $phone[1] . $phone[2] . ")***-**-" . $phone[8] . $phone[9];
		}

		return "";
	}

	/**
	 * Отправка смс с проверочным кодом через МФО
	 *
	 * @return array|string успешный результат или ошибка
	 */
	public function actionSms() {

		try {
			$arPost = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
			if ( ! $this->soapClient ) {
				throw new Exception( "Некорректные настройки подключения к МФО-сервису" );
			}
			$sReturn  = $this->soapClient->__soapCall( "CheckIncoming", [
				[
					"LastName" => $arPost["lastName"],
					"Nom"      => $arPost["contract"],
				],
			] )->return;
			$arReturn = json_decode( $sReturn, true );
			if ( $arReturn["ReturnCode"] !== 0 ) {
				throw new Exception( "Указаны неверные данные" );
			}
			Log::info( [
				             "contract" => $arPost["contract"],
				             "text"     => "Запрос: " . print_r( $arPost, true ) . "\nОтвет МФО: " . print_r( $arReturn, true ),
			             ], "pay.mfo.sms" );

			return $this->getClassAppResponse()::get( [
				                                          "status" => "ok",
				                                          "phone"  => $this->phoneFormat( $arReturn["Phone"] ),
			                                          ] );
		} catch ( Exception $e ) {
			Log::error( [
				              "contract" => $arPost["contract"],
				              "text"     => "Ошибка: " . $e->getMessage() . "\nЗапрос: " . print_r( $arPost, true ) . "\nОтвет МФО: " . print_r( $arReturn, true ),
			              ], "pay.mfo.sms" );

			return $this->getClassAppResponse()::get( false, false, $e->getMessage() );
		}
		finally {
			if ( $this->socketTimeout != ini_get( 'default_socket_timeout' ) ) {
				ini_set( 'default_socket_timeout', $this->socketTimeout );
			}
		}
	}

	/**
	 * Проверка проверочного кода из смс в МФО
	 *
	 * @return array|string успешный результат или ошибка
	 */
	public function actionCheck() {

		try {
			$arPost = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
			if ( ! $this->soapClient ) {
				throw new Exception( "Некорректные настройки подключения к МФО-сервису" );
			}
			$sReturn  = $this->soapClient->__soapCall( "GetOperData", [
				[
					"AccessCode" => $arPost["code"],
					"Nom"        => $arPost["contract"],
				],
			] )->return;
			$arReturn = json_decode( $sReturn, true );
			if ( ! $arReturn["НомерДоговора"] ) {
				throw new Exception( "Проверочный код не совпадает" );
			} elseif ( preg_replace( "/[^\d]/s", "", $arReturn["НомерДоговора"] ) !== preg_replace( "/[^\d]/s", "", $arPost["contract"] ) ) {
				throw new Exception( "Произошла системная ошибка" );
			}
			Log::info( [
				             "contract" => $arPost["contract"],
				             "text"     => "Запрос: " . print_r( $arPost, true ) . "\nОтвет МФО: " . print_r( $arReturn, true ),
			             ], "pay.mfo.check" );

			return $this->getClassAppResponse()::get( [
				                                          "status"  => "ok",
				                                          "sum"     => (float) $arReturn['РекомендуемаяСумма'],
				                                          "invoice" => trim( $arReturn['ИдентификаторПлатежа'] ),
			                                          ] );
		} catch ( Exception $e ) {
//			return $this->getClassAppResponse()::get( [
//				                                          "status"  => "ok",
//				                                          "sum"     => 1000,
//				                                          "invoice" => rand( 10000000000, 9999999999999 ),
//			                                          ] );
			Log::error( [
				              "contract" => $arPost["contract"],
				              "text"     => "Ошибка: " . $e->getMessage() . "\nЗапрос: " . print_r( $arPost, true ) . "\nОтвет МФО: " . print_r( $arReturn, true ),
			              ], "pay.mfo.check" );

			return $this->getClassAppResponse()::get( false, false, $e->getMessage() );
		}
		finally {
			if ( $this->socketTimeout != ini_get( 'default_socket_timeout' ) ) {
				ini_set( 'default_socket_timeout', $this->socketTimeout );
			}
		}
	}

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCloud() {

        if ( ! yii::$app->params['payment']['cloud']['publicId'] ) {
            throw new Exception( "Отсутствует ключ подключения publicId к сервису CloudPayments" );
        }

        return $this->getClassAppResponse()::get( [
                                                      "id"          => yii::$app->params['payment']['cloud']['publicId'],
                                                      "description" => "Оплата в carmoney.ru",
                                                      "currency"    => "RUB",
                                                  ] );
	}

    /**
     * Экшн возвращает массив активных платежных систем,
     *
     * @return array
     * @throws \Exception
     */
	public function actionParams() {

        if ( ! array_key_exists( 'multiplicity', yii::$app->params['payment'] ) ) {
            throw new Exception( "Отсутствует параметр множественности платежных систем" );
        }
        $currencyCode = ( new Currency() )->getOne( 643, [ 'code' ] );
        if ( Yii::$app->params['payment']['multiplicity'] === false ) {
            if ( ! $arVendor = ( new Vendor() )->getOneByFilter( [ "active" => 1 ], [], [
                'id',
                'name',
                'code',
                'publicId',
                'description',
                'currency_id',
            ] ) ) {
                throw new Exception( "Не задана акстивная платежная система" );
            }
        } else {
            if ( ! $arVendor = ( new Vendor() )->getAllByFilter( [ "active" => 1 ], [], [
                'id',
                'name',
                'code',
                'publicId',
                'description',
                'currency_id',
                'file_oferta',
            ] ) ) {
                throw new Exception( "Не задано ни одной акстивной платежной системы" );
            }
        }
        foreach ( $arVendor as $val ) {

            $val['currency'] = ( new Currency() )->getAllByFilter( [ 'id' => $val['currency_id'] ] )[0]['code'];
            unset( $val['currency_id'] );
            $vendor[] = $val;
        }
        $arVendor = $vendor;

        return $this->getClassAppResponse()::get( $arVendor );
	}

    /**
     * Экшн возвращает массив активных платежных систем,
     *
     * @return array
     * @throws \Exception
     */
	public function actionMethods() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $model   = ( new PayMethods() );
        if ( ! $model->load( $reqBody, '' ) ) {
            throw new Exception( 'Не были указаны параметры для получения списка способов платежей' );
        }
        $res  = $model->getAllByFilter( [
                                            'vendor_id' => $model->vendor,
                                            'active'    => 1,
                                        ], [
                                            'sort' => "ASC",
                                        ], [
                                            'id',
                                            'name',
                                            'rnko_client',
                                            'rnko_client_min',
                                        ] );
        $data = $res;

        /*
        foreach ( $res as $val ) {
            $data[] = [
                'id'              => $val['id'],
                'name'            => $val['name'],
                'rnko_client'     => $val['rnko_client'],
                'rnko_client_min' => $val['rnko_client_min'],
            ];
        }*/

        return $this->getClassAppResponse()::get( $data );
	}

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionForm() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $Vendor  = new Vendor();
        if ( ( $arVendor = $Vendor->getOneByFilter( [ 'id' => $reqBody['vendor'] ] ) ) instanceof Exception ) {
            throw new Exception( $arVendor->getMessage() );
        }
        $classPayActions = '\app\modules\payment\v1\models\\' . $arVendor['code'] . '\PayActions';
        $obPayActions    = ( new $classPayActions( [ 'scenario' => $classPayActions::SCENARIO_CREATE ] ) );
        if ( ! $obPayActions instanceof PayActionsInterfaces ) {
            throw new Exception( "Не верно определен типа объекта $classPayActions " . __LINE__ );
        }
        if ( ! $obPayActions->load( $reqBody, '' ) ) {
            throw new \Exception( 'Не были указаны параметры для получения формы' );
        }
        if ( $obPayActions->validate() ) {
            $data['form'] = $obPayActions->generateForm();
            /**
             * new addon
             */
            $arPost = $data['check'] = $obPayActions->writeCheckData();
            $loads  = [
                'vendor' => $Vendor,
                'data'   => $arPost,
            ];
            if ( ! $PayFactory = new PayFactory() ) {
                throw new \Exception( 'Не возможно создать фабрику Оплаты' );
            }
            if ( ! $PayFactory->load( $loads, '' ) ) {
                throw new \Exception( 'Не были указаны параметры для получения формы' );
            }
            $PayFactory->setIsNeedCheck( false ); // отмена проверки данных
            $res = $PayFactory->mainProcess();
            if ( is_a( $res, 'PayFactory' ) ) {
                return $this->getClassAppResponse()::get( false, $res->firstErrors );
            }
            if ( is_a( $res, 'Exception' ) ) {
                return $this->getClassAppResponse()::get( false, $res->getMessage() );
            }
            /**
             * end new addon
             */
        } else {
            return $this->getClassAppResponse()::get( false, $obPayActions->firstErrors );
        }

        return $this->getClassAppResponse()::get( $data );
	}

	/**
	 * для создания тестовой формы
	 *
	 * @return array|string
	 */
	public function actionForm1() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $data                        = "<html>
<head>
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
</head>
<body>
<div id=\"body\"></div>
<script>
function sendQuery() {
    $.ajax(
        {
        url: 'http://pay.carmoney55.ru/web/v1/payment/payment/form',
        dataType : 'json',
        type : 'post',
        contentType : 'application/json',
        data               : JSON.stringify({
            'vendor'    : 2,
            'sum'       : 10,
            'comission' : 0.1,
            'pay_method': 1,
            'invoice'   : '" . rand( 10000000000, 9999999999999 ) . "',
            'dog'       : '11111111111'
        }),
        processData : false,
        success : function (data, textStatus, jQxhr) {
           
            console.log('data',data, 'form', data.form, 'data2', data['data']['form']);
            $('#body').append(data['data']['form']);
            
            $('#body>form').append('<input type=submit>');
            
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}
sendQuery();

</script>
</body>
</html>
        ";

        return $data;
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}
}