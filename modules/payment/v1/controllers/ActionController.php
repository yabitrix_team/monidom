<?php

namespace app\modules\payment\v1\controllers;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Currency;
use app\modules\payment\v1\models\base\PayFactory;
use app\modules\payment\v1\models\base\Payment;
use app\modules\payment\v1\models\base\Vendor;
use app\modules\payment\v1\models\cloud\Action;
use Exception;
use yii;
use yii\base\Controller;
use yii\db\Transaction;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Обработчики действий по платежам
 *
 * @package app\modules\payment\v1\controllers
 */
class ActionController extends Controller {
	/**
	 * @inheritdoc
	 */
	use TraitConfigController;

	private function checkAuth( $headers = [] ) {

		if ( ! $headers["content-hmac"] ) {
			return false;
		}
		$hmac = hash_hmac( 'sha256', Yii::$app->request->getRawBody(), yii::$app->params['payment']['cloud']['secret'], true );

		return $headers["content-hmac"] == base64_encode( $hmac );
	}

	/**
	 * Проверка на проведение платежа со стороны CloudPayments
	 *
	 * @return array код ответа платежной системе
	 * @throws yii\db\Exception
	 */
	public function actionCheckCloud() {

		try {
			if ( ! yii::$app->params['payment']['cloud']['publicId'] ) {
				throw new Exception( "Отсутствует ключ подключения publicId к сервису CloudPayments" );
			}
			if ( ! yii::$app->params['payment']['cloud']['secret'] ) {
				throw new Exception( "Отсутствует ключ API Secret к сервису CloudPayments" );
			}
			if ( ! $this->checkAuth( Yii::$app->request->getHeaders() ) ) {
				throw new \yii\web\HttpException( 401, 'Ошибка авторизации со стороны CloudPayments' );
			}
			$arPost = Yii::$app->request->getBodyParams();
			if ( ! $arVendor = ( new Vendor() )->getOneByFilter( [ "code" => "CloudPayments", "active" => 1 ] ) ) {
				throw new Exception( "Не найдена платежная система \"code\"=\"cloudpayments\"" );
			}
			if ( ! $arCurrency = ( new Currency() )->getOneByFilter( [
				                                                         "code" => $arPost["Currency"],
			                                                         ] ) ) {
				throw new Exception( "Не найдена валюта \"code\"=" . $arPost["Currency"] );
			}
			$arData = json_decode( $arPost["Data"], true );
			if ( ! is_array( $arData ) || ! strlen( $arData["sum"] ) ) {
				throw new Exception( "Не пришла сумма для МФО" );
			}
			$baseSum = floatval( str_replace( [ ",", " " ], [ ".", "" ], $arData["sum"] ) );
			$commSum = floatval( round( $baseSum / ( ( 100 - 2 ) / 100 ), 2 ) );
			if ( $commSum !== (float) $arPost["Amount"] ) {
				throw new Exception( "Суммы не сошлись: " . $commSum . " !== " . $arPost["Amount"] );
			}
			$transaction = yii::$app->db->beginTransaction();
			$obPayment   = new Payment( [
				                            "vendor"      => $arVendor["id"],
				                            "invoice"     => $arPost["InvoiceId"],
				                            "contract"    => $arPost["AccountId"],
				                            "transaction" => $arPost["TransactionId"],
				                            "sum"         => $arPost["Amount"],
				                            "currency"    => $arCurrency["id"],
				                            "description" => $arPost["Description"],
			                            ] );
			if ( ! $obPayment->validate() ) {
				throw new Exception( print_r( $obPayment->getErrors(), true ) );
			}
			if ( ! $idPayment = $obPayment->add( $obPayment->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа: " . print_r( $obPayment->getAttributes(), true ) );
			}
			$obAction = new Action( [
				                        'payment'           => $idPayment,
				                        'action'            => 'check',
				                        // время передается в UTC. добавлям чсов по таймзоне, что бы получить соответствие.
				                        'date'              => date( "Y-m-d H:i:s", strtotime( $arPost["DateTime"] ) + date( "Z" ) ),
				                        'card_number_first' => $arPost["CardFirstSix"],
				                        'card_number_last'  => $arPost["CardLastFour"],
				                        'card_type'         => $arPost["CardType"],
				                        'card_exp'          => $arPost["CardExpDate"],
				                        'status'            => $arPost["Status"],
				                        'type'              => $arPost["OperationType"],
				                        'name'              => $arPost["Name"],
				                        'email'             => $arPost["Email"],
				                        'ip'                => $arPost["IpAddress"],
				                        'country'           => $arPost["IpCountry"],
				                        'city'              => $arPost["IpCity"],
				                        'region'            => $arPost["IpRegion"],
				                        'district'          => $arPost["IpDistrict"],
				                        'bank_name'         => $arPost["Issuer"],
				                        'bank_country'      => $arPost["IssuerBankCountry"],
				                        'token_payee'       => $arPost["TokenRecipient"],
				                        "data"              => $arPost["Data"],
			                        ] );
			if ( ! $obAction->validate() ) {
				throw new Exception( print_r( $obAction->getErrors(), true ) );
			}
			if ( ! $obAction->add( $obAction->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа cloudpayments: " . print_r( $obAction->getAttributes(), true ) );
			}
			$transaction->commit();
			Log::info( [
				             "contract" => $arPost["AccountId"],
			             ], "pay.cp.check" );

			return [ "code" => 0 ];
		} catch ( Exception $e ) {
			if ( $transaction instanceof Transaction ) {
				$transaction->rollBack();
			}
			Log::error( [
				              "contract" => $arPost["AccountId"],
				              "text"     => "Ошибка: " . $e->getMessage() . "\nЗапрос: " . print_r( $arPost, true ),
			              ], "pay.cp.check" );

			return [ "code" => 13 ];
		}
	}

	/**
	 * Проведение платежа со стороны CloudPayments
	 *
	 * @return array
	 */
	public function actionPayCloud() {

		try {
			if ( ! yii::$app->params['payment']['cloud']['publicId'] ) {
				throw new Exception( "Отсутствует ключ подключения publicId к сервису CloudPayments" );
			}
			if ( ! yii::$app->params['payment']['cloud']['secret'] ) {
				throw new Exception( "Отсутствует ключ API Secret к сервису CloudPayments" );
			}
			if ( ! $this->checkAuth( Yii::$app->request->getHeaders() ) ) {
				throw new \yii\web\HttpException( 401, 'Ошибка авторизации со стороны CloudPayments' );
			}
			$arPost = Yii::$app->request->getBodyParams();
			if ( ! $arVendor = ( new Vendor() )->getOneByFilter( [ "code" => "CloudPayments", "active" => 1 ] ) ) {
				throw new Exception( "Не найдена платежная система \"code\"=\"cloudpayments\"" );
			}
			if ( ! $arCurrency = ( new Currency() )->getOneByFilter( [
				                                                         "code" => $arPost["Currency"],
			                                                         ] ) ) {
				throw new Exception( "Не найдена валюта \"code\"=" . $arPost["Currency"] );
			}
			if ( ! $idPayment = ( new Payment() )->getOneByFilter( [ "transaction" => $arPost["TransactionId"] ] )["id"] ) {
				throw new Exception( "Не найден платеж по транзакции \"transaction\"=" . $arPost["TransactionId"] );
			}
			$obAction = new Action( [
				                        'payment'           => $idPayment,
				                        'action'            => 'pay',
				                        // время передается в UTC. добавлям чсов по таймзоне, что бы получить соответствие.
				                        'date'              => date( "Y-m-d H:i:s", strtotime( $arPost["DateTime"] ) + date( "Z" ) ),
				                        'card_number_first' => $arPost["CardFirstSix"],
				                        'card_number_last'  => $arPost["CardLastFour"],
				                        'card_type'         => $arPost["CardType"],
				                        'card_exp'          => $arPost["CardExpDate"],
				                        'status'            => $arPost["Status"],
				                        'type'              => $arPost["OperationType"],
				                        'name'              => $arPost["Name"],
				                        'email'             => $arPost["Email"],
				                        'ip'                => $arPost["IpAddress"],
				                        'country'           => $arPost["IpCountry"],
				                        'city'              => $arPost["IpCity"],
				                        'region'            => $arPost["IpRegion"],
				                        'district'          => $arPost["IpDistrict"],
				                        'bank_name'         => $arPost["Issuer"],
				                        'bank_country'      => $arPost["IssuerBankCountry"],
				                        'token_card'        => $arPost["Token"],
				                        "data"              => $arPost["Data"],
			                        ] );
			if ( ! $obAction->validate() ) {
				throw new Exception( print_r( $obAction->getErrors(), true ) );
			}
			if ( ! $obAction->add( $obAction->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа cloudpayments: " . print_r( $obAction->getAttributes(), true ) );
			}
			Log::info( [
				             "contract" => $arPost["AccountId"],
			             ], "pay.cp.pay" );

			return [ "code" => 0 ];
		} catch ( Exception $e ) {
			Log::error( [
				              "contract" => $arPost["AccountId"],
				              "text"     => "Ошибка: " . $e->getMessage() . "\nЗапрос: " . print_r( $arPost, true ),
			              ], "pay.cp.pay" );

			return [ "code" => 13 ];
		}
	}

	public function actionFailCloud() {

		try {
			if ( ! yii::$app->params['payment']['cloud']['publicId'] ) {
				throw new Exception( "Отсутствует ключ подключения publicId к сервису CloudPayments" );
			}
			if ( ! yii::$app->params['payment']['cloud']['secret'] ) {
				throw new Exception( "Отсутствует ключ API Secret к сервису CloudPayments" );
			}
			if ( ! $this->checkAuth( Yii::$app->request->getHeaders() ) ) {
				throw new \yii\web\HttpException( 401, 'Ошибка авторизации со стороны CloudPayments' );
			}
			$arPost = Yii::$app->request->getBodyParams();
			if ( ! $arVendor = ( new Vendor() )->getOneByFilter( [ "code" => "CloudPayments", "active" => 1 ] ) ) {
				throw new Exception( "Не найдена платежная система \"code\"=\"cloudpayments\"" );
			}
			if ( ! $arCurrency = ( new Currency() )->getOneByFilter( [
				                                                         "code" => $arPost["Currency"],
			                                                         ] ) ) {
				throw new Exception( "Не найдена валюта \"code\"=" . $arPost["Currency"] );
			}
			if ( ! $idPayment = ( new Payment() )->getOneByFilter( [ "transaction" => $arPost["TransactionId"] ] )["id"] ) {
				throw new Exception( "Не найден платеж по транзакции \"transaction\"=" . $arPost["TransactionId"] );
			}
			$obAction = new Action( [
				                        'payment'           => $idPayment,
				                        'action'            => 'fail',
				                        // время передается в UTC. добавлям чсов по таймзоне, что бы получить соответствие.
				                        'date'              => date( "Y-m-d H:i:s", strtotime( $arPost["DateTime"] ) + date( "Z" ) ),
				                        'card_number_first' => $arPost["CardFirstSix"],
				                        'card_number_last'  => $arPost["CardLastFour"],
				                        'card_type'         => $arPost["CardType"],
				                        'card_exp'          => $arPost["CardExpDate"],
				                        'type'              => $arPost["OperationType"],
				                        'name'              => $arPost["Name"],
				                        'email'             => $arPost["Email"],
				                        'ip'                => $arPost["IpAddress"],
				                        'country'           => $arPost["IpCountry"],
				                        'city'              => $arPost["IpCity"],
				                        'region'            => $arPost["IpRegion"],
				                        'district'          => $arPost["IpDistrict"],
				                        'bank_name'         => $arPost["Issuer"],
				                        'bank_country'      => $arPost["IssuerBankCountry"],
				                        'error'             => $arPost["Reason"],
				                        'error_code'        => $arPost["ReasonCode"],
				                        "data"              => $arPost["Data"],
			                        ] );
			if ( ! $obAction->validate() ) {
				throw new Exception( print_r( $obAction->getErrors(), true ) );
			}
			if ( ! $obAction->add( $obAction->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа cloudpayments: " . print_r( $obAction->getAttributes(), true ) );
			}
			Log::info( [
				             "contract" => $arPost["AccountId"],
			             ], "pay.cp.fail" );

			return [ "code" => 0 ];
		} catch ( Exception $e ) {
			Log::error( [
				              "contract" => $arPost["AccountId"],
				              "text"     => "Ошибка: " . $e->getMessage() . "\nЗапрос: " . print_r( $arPost, true ),
			              ], "pay.cp.fail" );

			return [ "code" => 13 ];
		}
	}

    /**
     * новая функция process
     *
     * @return array
     * @throws \Exception
     */
	function actionProcess() {

        $arPost = Yii::$app->request->getBodyParams();
        $loads  = [
            'vendor' => ( new Vendor() ),
            'data'   => $arPost,
        ];
        if ( ! $PayFactory = new PayFactory() ) {
            throw new \Exception( 'Не возможно создать фабрику Оплаты' );
        }
        if ( ! $PayFactory->load( $loads, '' ) ) {
            throw new \Exception( 'Не были указаны параметры для получения формы' );
        }
        $res = $PayFactory->mainProcess();
        if ( is_a( $res, PayFactory::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }
        if ( is_a( $res, \Exception::class ) ) {
            return $this->getClassAppResponse()::get( false, $res->getMessage() );
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

        return $res;
	}

	/**
	 * старая функция process
	 *
	 * @return string
	 * @throws yii\db\Exception
	 */
	function actionProcess1() {

		try {
			$arPost = Yii::$app->request->getBodyParams();
			if ( ( $arVendor = ( new Vendor() )->testVendor( $arPost ) ) instanceof Exception ) {
				throw new Exception( $arVendor->getMessage() );
			}
			if ( ! $arCurrency = ( new Currency() )->getOneByFilter( [
				                                                         "id" => $arPost["WMI_CURRENCY_ID"],
			                                                         ] ) ) {
				throw new Exception( "Не найдена валюта \"code\"=" . $arPost["WMI_CURRENCY_ID"] );
			}
			$transaction  = yii::$app->db->beginTransaction();
			$classPayment = '\app\modules\payment\v1\models\\' . $arVendor['code'] . '\Payment';
			$obPayment    = new $classPayment();
			if ( ! $obPayment instanceof PaymentInterfaces ) {
				throw new Exception( "Не верно определен типа объекта " . __LINE__ );
			}
			$obPayment->setVendor( $arVendor['id'] );
			$obPayment->setCurrency( $arCurrency['id'] );
			$loadDataPayment = $obPayment->prepareDataToLoad( $arPost );
			if ( $obPayment->load( $loadDataPayment, '' ) ) { // загружаем данные в модель
				if ( ! $obPayment->validate() ) {
					return $this->getClassAppResponse()::get( false, $obPayment->getErrors() );
				}
			}
			if ( ! $idPayment = $obPayment->add( $obPayment->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа: " . print_r( $obPayment->getAttributes(), true ) );
			}
			$classPayActions = '\app\modules\payment\v1\models\\' . $arVendor['code'] . '\PayActions';
			$obPayActions    = new $classPayActions( [ 'scenario' => $classPayActions::SCENARIO_CHECK ] );
			if ( ! $obPayActions instanceof PayActionsInterfaces ) {
				throw new Exception( "Не верно определен типа объекта $classPayActions " . __LINE__ );
			}
			$obPayActions->setPayment( $idPayment ); // устанавливаем id платежа
			$obPayActions->setVendor( $arVendor['id'] ); // устанавливаем id платежа
			$loadData = $obPayActions->prepareDataToLoad( $arPost ); // подготавливаем массив к загрузке в модель
			if ( $obPayActions->load( $loadData, '' ) ) { // загружаем данные в модель
				if ( ! $obPayActions->checkAuth( Yii::$app->request->getHeaders() ) ) {
					throw new \yii\web\HttpException( 401, 'Ошибка авторизации со стороны ' . $arVendor['name'] );
				}
				if ( ! $obPayActions->validate() ) {
					return $this->getClassAppResponse()::get( false, $obPayActions->getErrors() );
				}
				if ( ( $res = $obPayActions->checkResult() ) instanceof \Exception ) {

					throw new Exception( "Ошибка проверки платежа w1: " . $res->getMessage() );
				};
				if ( ! $obPayActions->add( $obPayActions->getAttributes( $obPayActions->getFieldsNames ) ) ) {
					throw new Exception( "Ошибка записи платежа w1: " . print_r( $obPayActions->getAttributes( $obPayActions->getFieldsNames ), true ) );
				}
				$transaction->commit();
				Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

				return $res;
			} else {
				throw new Exception( "Ошибка записи платежа w1: " . print_r( $obPayActions->getAttributes( null, $obPayActions->getAttributes( $obPayActions->checkFieldsNames ) ), true ) );
			}
		} catch ( Exception $e ) {
			if ( $transaction instanceof Transaction ) {
				$transaction->rollBack();
			}
			\Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

			return $e->getMessage();
		}
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => [],
			],
		];
	}
}