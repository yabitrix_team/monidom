<?php

namespace app\modules\payment\v1\models\WalletOne;

use app\modules\payment\v1\models\base\Payment as BasePayment;
use app\modules\payment\v1\models\interfaces\PaymentInterfaces;

/**
 * Платеж по договору
 *
 * @package app\modules\payment\v1\models\base
 */
class Payment extends BasePayment implements PaymentInterfaces {

	public function prepareDataToLoad( array $arr = [] ) {    // загрузка данных в модель
		return [
			"vendor"      => $this->getVendor(),
			"invoice"     => $arr["WMI_PAYMENT_NO"],
			"contract"    => $arr["FD_ACCOUNTID"],
			"transaction" => $arr["WMI_ORDER_ID"],
			"sum"         => $arr["FD_MAIN_AMOUNT"],
			"currency"    => $this->getCurrency(),
			"description" => iconv("windows-1251", "utf-8", $arr["WMI_DESCRIPTION"]),
		];
	}
}