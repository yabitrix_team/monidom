<?php

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Exception;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Платеж по договору
 *
 * @package app\modules\payment\v1\models\base
 */
class Payment extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * @var int ID платежной системы
	 */
	public $vendor;
	/**
	 * @var string внешний ID платежа из МФО
	 */
	public $invoice;
	/**
	 * @var string номер договора
	 */
	public $contract;
	/**
	 * @var string ID платежа из платежной системы
	 */
	public $transaction;
	/**
	 * @var float сумма платежа
	 */
	public $sum;
	/**
	 * @var int ID валюты
	 */
	public $currency;
	/**
	 * @var string назначение платежа
	 */
	public $description;


	const SCENARIO_WRITE_CHECK = 'write_check';

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{pay_payment}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"vendor",
			"invoice",
			"contract",
			"transaction",
			"sum",
			"currency",
			"description",
			"created_at",
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'vendor', 'invoice', 'contract', 'transaction', 'sum', 'currency' ], 'required' ],
		];
	}

	/**
	 * @param mixed $vendor
	 *
	 * @return Vendor
	 */
	public function setVendor( $vendor ) {

		$this->vendor = $vendor;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVendor() {

		return $this->vendor;
	}

	/**
	 * @param mixed $currency_id
	 *
	 * @return Vendor
	 */
	public function setCurrency( $currency ) {

		$this->currency = $currency;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCurrency() {

		return $this->currency;
	}

	public function save(array $fields = []){
		try {
			$insert                    = $update = $fields;
			$insert['created_at']      = new Expression( 'NOW()' );
			$update['updated_at']      = new Expression( 'NOW()' );

			$result = Yii::$app->db
				->createCommand()
				->upsert( $this->getTableName(), $insert, $update )->execute();

			return !empty($result) ? Yii::$app->db->lastInsertID : $result;
		} catch ( Exception $e ) {
			return $e->getMessage();
		}
	}
}