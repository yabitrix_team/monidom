<?php

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\traits\ReadableTraitEx;
use yii\base\Model;

/**
 * Платеж по договору
 *
 * @package app\modules\payment\v1\models\base
 */
class PayMethods extends Model {
	use ReadableTraitEx;
	/**
	 * @var int ID платежной системы
	 */
	public $vendor;

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{pay_methods}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			'id',
			'name',
			'rnko_mfk',
			'rnko_mfk_min',
			'rnko_client',
			'rnko_client_min',
			'vendor_id',
			'pseudonim',
			'active',
			'sort',
			'created_at',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[
				'vendor',
				'required',
				'message' => 'Поле "Поставщик" не указано',
			],
			[
				'vendor',
				'number',
				'message' => 'Поле "Поставщик" заполнено некорректно',
			],
		];
	}


}