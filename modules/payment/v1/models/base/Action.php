<?php
/**
 * Created by PhpStorm.
 * User: greb
 * Date: 08.05.18
 * Time: 14:13
 */

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use Yii;
use yii\base\Model;
use Exception;

/**
 * Действие по платежу
 *
 * @package app\modules\payment\v1\models\base
 */
abstract class Action extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * @var int внутренний ID транзакции по платежу
	 */
	public $id;
	/**
	 * @var int ID платежа
	 */
	public $payment;
	/**
	 * @var string тип действия
	 */
	public $action;
	/**
	 * @var string дата операции из платежной системы
	 */
	public $date;
	/**
	 * @var string дата создания
	 */
	public $created_at;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'payment', 'action' ], 'required' ],
		];
	}
}