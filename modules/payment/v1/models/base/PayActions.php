<?php

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\traits\ReadableTraitEx;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;

/**
 * Функции для платежных систем
 *
 * @package app\modules\payment\v1\models\base
 */
class PayActions extends Model {
	use ReadableTraitEx;
	use WritableTrait;
	/**
	 * @var int ID платежной системы
	 */
	public $vendor;
	/*
	 * @var @var int ID метод системы
	 */
	public $pay_method;
	/*
	 * @var double Сумма платежа
	 */
	public $sum;
	/*
	 * @var double процент
	 */
	public $comission;
	/*
	 * @var string номер счета
	 */
	public $invoice;
	/*
	 * @var string номер договора
	 */
	public $dog;
	/**
	 * Константа сценария создания заявки
	 */
	const SCENARIO_CREATE = 'create';
	/**
	 * Константа сценария обновления заявки
	 */
	const SCENARIO_CHECK = 'check';
	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * @return string название таблицы
	 */
	public $getTableName = "";

	/**
	 * Список названий полей модели
	 *
	 * @return array список полей
	 */
	public $getFieldsNames = [];

	public $checkFieldsNames = [
		'vendor',
		'pay_method',
		'sum',
		'comission',
		'invoice',
		'dog',
	];
	/**
	 * Метод установки сценария для модели при валидации полей
	 *
	 * @return array - вернет массив сценариев,
	 * по умолчанию установлен в сценарии установлен DEFAULT
	 */
	public function scenarios() {

		$scenarios                          = parent::scenarios();
		$scenarios[ self::SCENARIO_CREATE ] = $this->checkFieldsNames;
		$scenarios[ self::SCENARIO_CHECK ] = $this->getFieldsNames;
		return $scenarios;
	}

	/**
	 * @param mixed $vendor
	 *
	 * @return Vendor
	 */
	public function setVendor( $vendor ) {

		$this->vendor = $vendor;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getVendor() {

		return $this->vendor;
	}
	/**
	 * @param mixed $payment
	 *
	 * @return PayActions
	 */
	public function setPayment( $payment ) {

		$this->payment = $payment;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPayment() {

		return $this->payment;
	}
	/**
	 * @inheritdoc
	 *
	 *
	 */
	function getVendorById( int $vendor = 0 ) {

		$vendor = $vendor > 0 ? $vendor : $this->vendor;

		//return print_r( ["vendor"=>$vendor, "this->vendor"=>$this->vendor], true);
		return ( new Vendor() )->getOneByFilter( [ 'id' => $vendor ] );
	}

	function getPayMethodByIdVendorId( int $pay_method = 0, int $vendor = 0 ) {

		$pay_method = $pay_method > 0 ? $pay_method : $this->pay_method;
		$vendor     = $vendor > 0 ? $vendor : $this->vendor;

		return ( new PayMethods() )->getOneByFilter( [ 'id' => $pay_method, 'vendor_id' => $vendor ] );
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return $this->getTableName;
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return $this->getFieldsNames;
	}

	/**
	 * @inheritdoc
	 *
	 *
	 *    public $vendor;
	 * public $pay_method;
	 * public $sum;
	 * public $persent;
	 * public $invoice;
	 * public $dog;
	 *
	 */
	public function rules() {

		return [
			[
				'vendor',
				'required',
				'message' => 'Поле "Поставщик" не указано',
			],
			[
				'vendor',
				'number',
				'message' => 'Поле "Поставщик" заполнено некорректно',
			],
			[
				'pay_method',
				'required',
				'message' => 'Поле "Метод платежа" не указано',
			],
			[
				'pay_method',
				'number',
				'message' => 'Поле "Метод платежа" заполнено некорректно',
			],
			[
				'sum',
				'required',
				'message' => 'Поле "Сумма" не указано',
			],
			[
				'sum',
				'double',
				'message' => 'Поле "Сумма" заполнено некорректно',
			],
			[
				'comission',
				'required',
				'message' => 'Поле "Процент" не указано',
			],
			[
				'comission',
				'double',
				'message' => 'Поле "Процент" заполнено некорректно',
			],
			[
				'invoice',
				'required',
				'message' => 'Поле "Счет" не указано',
			],
			[
				'invoice',
				'match',
				'pattern' => '/^\d{11,15}$/iu',
				'message' => 'Поле "Счет" заполнено некорректно',
			],
			[
				'dog',
				'required',
				'message' => 'Поле "Договор" не указано',
			],
			[
				'dog',
				'match',
				'pattern' => '/^\d{11,15}$/iu',
				'message' => 'Поле "Договор" заполнено некорректно',
			],
		];
	}

	/**
	 * функция дебага
	 * @param array  $arrDebug
	 * @param string $fileName
	 */
	public function dbg( array $arrDebug = [], $fileName = "" ) {
		return false;
		$fileName = empty( $fileName ) ? $_SERVER['DOCUMENT_ROOT'] . '/PayAction.log' :
			$_SERVER['DOCUMENT_ROOT'] . '/' . $fileName;
		file_put_contents( $fileName, print_r( [ date( "d.m.Y H:i:s" ), $arrDebug ], true ), FILE_APPEND );
	}

	/**
	 * функция возвращает ссылку на оферту
	 *
	 * @return mixed
	 */
	public function linkOferta() {

		$vendor = $this->getVendorById();

		return $vendor['url_oferta'];
	}
}