<?php
/**
 * Created by PhpStorm.
 * User: greb
 * Date: 08.05.18
 * Time: 14:10
 */

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\ReadableTraitEx;
use app\modules\app\v1\models\traits\WritableTrait;
use Exception;
use yii\base\Model;

/**
 * Платежная система
 *
 * @package app\modules\payment\v1\models\base
 */
class Vendor extends Model {
	use WritableTrait;

	// todo[ruffnec] 12.07.2018 : переработать ReadableTrait и удалить ReadableTraitEx и переопределения.
	/*
	use ReadableTrait{
		ReadableTrait::getByGuid as pseudo_getByGuid;
		ReadableTrait::prepareFieldsNames as pseudo_prepareFieldsNames;
		ReadableTrait::getAllByFIlter as pseudo_getAllByFIlter;
		ReadableTrait::getByFilter as pseudo_getByFilter;
		ReadableTrait::getOneByFilter as pseudo_getOneByFilter;
		ReadableTrait::getResByFilter as pseudo_getResByFilter;
		ReadableTrait::getAll as pseudo_getAll;
		ReadableTrait::getOne as pseudo_getOne;
	}*/
	use ReadableTraitEx;
	/**
	 * @var string название платежной системы
	 */
	public $name;
	/**
	 * @var string символьный код
	 */
	protected $code;
	/**
	 * @var bool активность платежной системы
	 */
	public $active;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'name', 'code' ], 'required' ],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{pay_vendor}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"name",
			"code",
			"table_name",
			"publicId",
			"secret",
			'merchant_id',
			'url_success',
			'url_fail',
			'secret_test',
			'merchant_id_test',
			'url_success_test',
			'url_fail_test',
			'description',
			'currency_id',
			'file_oferta',
			'test_field1',
			'test_field2',
			"active",
			"created_at",
		];
	}

	public function testVendor( $arr ) {

		try {
			if ( ! $arVendor = ( new Vendor() )->getAllByFilter(['active'=>1]) ) {
				throw new Exception( "Платежная система не найдена 1");
			}
			//throw new Exception( print_r($arVendor, true) );
			foreach ( $arVendor as $key => $vendor ) {
				if ( array_key_exists( $vendor['test_field1'] , $arr) && array_key_exists( $vendor['test_field2'], $arr) ) {
					if($vendor['active']==1){
						return $vendor;
					}else{
						throw new Exception( "Платежная система не активна ". $vendor['name'] );
					}

				} else {
					throw new Exception( "Платежная система не найдена 2");
				}
			}
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}