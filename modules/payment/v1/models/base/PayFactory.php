<?php

namespace app\modules\payment\v1\models\base;

use app\modules\app\v1\models\Currency;
use app\modules\payment\v1\models\interfaces\PayActionsInterfaces;
use app\modules\payment\v1\models\interfaces\PaymentInterfaces;
use Exception;
use yii\base\Model;
use yii;
/**
 * Функции для платежных систем
 *
 * @package app\modules\payment\v1\models\base
 */
class PayFactory extends Model {

	public $vendor;
	public $data;
	protected $isNeedCheck = true;

	public function rules()
	{
		return [
			// встроенный валидатор определяется как модель метода validateCountry()
			[['vendor', 'data'], 'required', 'message'=>'Не все необходимые параметры получены'],

			// встроенный валидатор определяется как анонимная функция
			['vendor', function ($attribute, $params) {
				if ( ! $this->$attribute instanceof Vendor ) {
					$this->addError($attribute, 'Не валидный обект "Vendor"');
				}
			}],
			// встроенный валидатор определяется как анонимная функция
			['data', function ($attribute, $params) {
				if ( ! is_array($this->$attribute) ) {
					$this->addError($attribute, 'Не валидный обект "Data"');
				}
			}],
		];
	}

	public function mainProcess(){
		try{
			//$this->setIsNeedCheck(false); // отмена проверки данных

			if(!$this->validate()){
				return $this;
			}

			$arPost = $this->data;
			$arVendor = ($this->vendor)->testVendor( $arPost );
			if ( is_a($arVendor,  \Exception::class) ) {
				throw new Exception( $arVendor->getMessage() );
			}

			if ( ! $arCurrency = ( new Currency() )->getOneByFilter( [
				                                                         "id" => $arPost["WMI_CURRENCY_ID"],
			                                                         ] ) ) {
				throw new Exception( "Не найдена валюта \"code\"=" . $arPost["WMI_CURRENCY_ID"] );
			}
			$transaction  = yii::$app->db->beginTransaction();
			$classPayment = '\app\modules\payment\v1\models\\' . $arVendor['code'] . '\Payment';

			$obPayment    = new $classPayment();

			if ( ! $obPayment instanceof PaymentInterfaces ) {
				throw new Exception( "Не верно определен типа объекта " . __LINE__ );
			}
			$obPayment->setVendor( $arVendor['id'] );
			$obPayment->setCurrency( $arCurrency['id'] );
			$loadDataPayment = $obPayment->prepareDataToLoad( $arPost );
			//throw new Exception( "Не пройдена проверка данных платежа " . print_r([$loadDataPayment, $arPost], true). __LINE__ );
			if ( $obPayment->load( $loadDataPayment, '' ) ) { // загружаем данные в модель
				if ( ! $obPayment->validate() ) {
					throw new Exception( "Не пройдена проверка данных платежа " . print_r($loadDataPayment, true). __LINE__ );

				}
			}
/*
			if ( ! $idPayment = ( new Payment() )->getOneByFilter( [ "invoice" => $loadDataPayment["invoice"] ] )["id"] ) {
				if ( ! $idPayment = $obPayment->add( $obPayment->getAttributes() ) ) {
					throw new Exception( "Ошибка записи платежа: " . print_r( $obPayment->getAttributes(), true ) );
				}
			}else{
				if ( ! $obPayment->update( [ "invoice" => $loadDataPayment["invoice"] ], $obPayment->getAttributes() ) ) {
					throw new Exception( "Ошибка обновления данных платежа: " . print_r( $obPayment->getAttributes(), true ) );
				}
			}
*/
			if ( ! $idPayment = $obPayment->save( $obPayment->getAttributes() ) ) {
				throw new Exception( "Ошибка записи платежа: " . print_r( $obPayment->getAttributes(), true ) );
			}

			$classPayActions = '\app\modules\payment\v1\models\\' . $arVendor['code'] . '\PayActions';
			$obPayActions    = new $classPayActions( [ 'scenario' => $classPayActions::SCENARIO_CHECK ] );


			if ( ! $obPayActions instanceof PayActionsInterfaces ) {
				throw new Exception( "Не верно определен типа объекта $classPayActions " . __LINE__ );
			}
			$obPayActions->setPayment( $idPayment ); // устанавливаем id платежа
			$obPayActions->setVendor( $arVendor['id'] ); // устанавливаем id платежа
			$loadData = $obPayActions->prepareDataToLoad( $arPost ); // подготавливаем массив к загрузке в модель
			if ( $obPayActions->load( $loadData, '' ) ) { // загружаем данные в модель
				if ( ! $obPayActions->checkAuth( Yii::$app->request->getHeaders() ) ) {
					throw new \yii\web\HttpException( 401, 'Ошибка авторизации со стороны ' . $arVendor['name'] );
				}
				if ( ! $obPayActions->validate() ) {
					throw new Exception( "Не пройдена проверка данных платежа платежной системы " . print_r($obPayActions->firstErrors, true ));
				}
				if($this->isNeedCheck()){
					if ( ( $res = $obPayActions->checkResult() ) instanceof \Exception ) {

						throw new Exception( "Ошибка проверки платежа w1: " . $res->getMessage() );
					};
				}
				if ( ! $obPayActions->add( $obPayActions->getAttributes( $obPayActions->getFieldsNames ) ) ) {
					throw new Exception( "Ошибка записи платежа w1: " . print_r( $obPayActions->getAttributes( $obPayActions->getFieldsNames ), true ) );
				}
				$transaction->commit();

				return $res;
			} else {
				throw new Exception( "Ошибка записи платежа w1: " . print_r( $obPayActions->getAttributes( null, $obPayActions->getAttributes( $obPayActions->checkFieldsNames ) ), true ) );
			}
		}catch (\Exception $e){
			return $e;
		}

	}

	/**
	 * @return bool
	 */
	public function isNeedCheck(): bool {

		return $this->isNeedCheck;
	}

	/**
	 * @param bool $isNeedCheck
	 */
	public function setIsNeedCheck( bool $isNeedCheck ): void {

		$this->isNeedCheck = $isNeedCheck;
	}
}