<?php
/**
 * Created by PhpStorm.
 * User: greb
 * Date: 08.05.18
 * Time: 14:13
 */

namespace app\modules\payment\v1\models\cloud;

use app\modules\payment\v1\models\base\Action as BaseAction;

/**
 * Действие по платежу
 *
 * @package app\modules\payment\v1\models\cloud
 */
class Action extends BaseAction {
	/**
	 * @var string статус платежа
	 */
	public $status;
	/**
	 * @var string тип операции
	 */
	public $type;
	/**
	 * @var string имя держателя карты
	 */
	public $name;
	/**
	 * @var string e-mail плательщика
	 */
	public $email;
	/**
	 * @var string первые 6 цифр номера карты
	 */
	public $card_number_first;
	/**
	 * @var string последние 4 цифры номера карты
	 */
	public $card_number_last;
	/**
	 * @var string платежная система карты: Visa, MasterCard, Maestro или МИР
	 */
	public $card_type;
	/**
	 * @var string срок действия карты в формате MM/YY
	 */
	public $card_exp;
	/**
	 * @var string IP адрес плательщика
	 */
	public $ip;
	/**
	 * @var string двухбуквенный код страны нахождения плательщика по ISO3166-1
	 */
	public $country;
	/**
	 * @var string город нахождения плательщика
	 */
	public $city;
	/**
	 * @var string регион нахождения плательщика
	 */
	public $region;
	/**
	 * @var string округ нахождения плательщика
	 */
	public $district;
	/**
	 * @var string название банка-эмитента карты
	 */
	public $bank_name;
	/**
	 * @var string двухбуквенный код страны эмитента карты по ISO3166-1
	 */
	public $bank_country;
	/**
	 * @var string токен получателя платежа
	 */
	public $token_payee;
	/**
	 * @var string токен карты для повторных платежей без ввода реквизитов
	 */
	public $token_card;
	/**
	 * @var string причина отказа
	 */
	public $error;
	/**
	 * @var int код ошибки
	 */
	public $error_code;
	/**
	 * @var array произвольный набор параметров, переданных в транзакцию
	 */
	public $data;
	/**
	 * @var string дата создания
	 */
	public $created_at;

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[
				[
					'payment',
					'action',
					'date',
					'card_number_first',
					'card_number_last',
					'card_type',
					'card_exp',
					'type',
				],
				'required',
			],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{pay_cloud}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"payment",
			"action",
			"status",
			"type",
			"date",
			"name",
			"email",
			"card_number_first",
			"card_number_last",
			"card_type",
			"card_exp",
			"ip",
			"country",
			"city",
			"region",
			"district",
			"bank_name",
			"bank_country",
			"token_payee",
			"token_card",
			"error",
			"error_code",
			"data",
			"created_at",
		];
	}
}