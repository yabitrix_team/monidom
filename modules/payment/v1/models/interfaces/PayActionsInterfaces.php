<?php
/**
 * Created by PhpStorm.
 * User: Студент 7
 * Date: 12.07.2018
 * Time: 19:33
 */
namespace app\modules\payment\v1\models\interfaces;

interface PayActionsInterfaces {
	public function checkAuth( $headers = [] ); // авторизация по заголовкам
	public function prepareDataToLoad(array $arr = []);     // загрузка данных в модель
	public function generateForm(); // генерация формы для отправки на фронт
	public function writeCheckData(); // генерация формы для отправки на фронт
	public function checkResult();  // проверка результата
}