<?php
/**
 * Created by PhpStorm.
 * User: Студент 7
 * Date: 12.07.2018
 * Time: 19:33
 */
namespace app\modules\payment\v1\models\interfaces;

interface PaymentInterfaces {
	public function prepareDataToLoad(array $arr = []);     // загрузка данных в модель
}