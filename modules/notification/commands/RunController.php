<?php

namespace app\modules\notification\commands;

//use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\Requests;
use app\modules\notification\commands\constructors\reqConstructor;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageType;
use swoole_process;
use swoole_redis;
use swoole_websocket_frame;
use swoole_websocket_server;
use Yii;
use yii\console\Controller;

\define( 'DEBUG', 'off' );

/**
 * Notification module
 *
 *
 * @property swoole_process $processTicketKeyFresher
 * @property swoole_process $processListener
 * @property swoole_process $processConnectMonitor
 */
class RunController extends Controller {
	/**
	 * @var
	 */
	public $swoole_params;
	/** @var swoole_websocket_server */
	private $ws;
	/** @var string swoole_version */
	private $swoole_version;
	/** @var \app\modules\notification\classes\WSTicketRegistration */
	private $wstr;

	/**
	 * Run swoole server
	 */
	public function actionIndex(): void {

		ini_set( 'default_socket_timeout', - 1 );
		ini_set( 'max_execution_time', 0 );
		$this->wstr           = Yii::$app->notify->getTRS();
		$this->swoole_version = swoole_version();
		/** @var swoole_websocket_server $ws */
		$this->ws = new swoole_websocket_server( $this->swoole_params['ip'], $this->swoole_params['port'] );
		$this->ws->set( [
			                'daemonize'  => false,
			                'user'       => 'nginx',
			                'group'      => 'nginx',
			                'worker_num' => 3,
			                /**
			                 * 0 =>DEBUG // all the levels of log will be recorded
			                 * 1 =>TRACE
			                 * 2 =>INFO
			                 * 3 =>NOTICE
			                 * 4 =>WARNING
			                 * 5 =>ERROR
			                 */
			                'log_level'  => 0,
			                'log_file'   => Yii::getAlias( '@runtime/logs/notification.log' ),
		                ] );
		swoole_set_process_name( "lkp_notifying_{$this->swoole_params['port']}" );
		$this->ws->on( 'start', [ $this, 'eventStart' ] );
		$this->ws->on( 'connect', [ $this, 'eventConnect' ] );
		$this->ws->on( 'open', [ $this, 'eventOpen' ] );
		$this->ws->on( 'receive', [ $this, 'eventReceive' ] );
		$this->ws->on( 'message', [ $this, 'eventMessage' ] );
		$this->ws->on( 'close', [ $this, 'eventClose' ] );
		$this->ws->on( 'managerstop', [ $this, 'eventManagerStop' ] );
		$this->ws->on( 'shutdown', [ $this, 'eventShutdown' ] );
		$this->ws->addProcess( $this->getProcessTicketKeyFresher() );
		$this->ws->addProcess( $this->getProcessListener() );
		if ( YII_ENV_DEV || YII_ENV_TEST ) {
			// $this->ws->addProcess( $this->getProcessConnectMonitor() );
		}
		$this->ws->start();
	}

	/**
	 * Процесс, отвечающий за состояние активности тикетов и их продление
	 *
	 * @return swoole_process
	 */
	public function getProcessTicketKeyFresher(): swoole_process {

		return new swoole_process( [ $this, 'processTicketKeyFresher' ] );
	}

	/**
	 * Регистрирует процесс, слушающий RedisQueue
	 *
	 * @return swoole_process
	 */
	public function getProcessListener(): swoole_process {

		return new swoole_process( [ $this, 'processListener' ] );
	}

	/**
	 * Регистрирует процесс, выводящий кол-во активных подключений
	 *
	 * @return swoole_process
	 */
	private function getProcessConnectMonitor(): swoole_process {

		return new swoole_process( [ $this, 'processConnectMonitor' ] );
	}

	/**
	 * М-д автоматические отправляет команду для деавторизации пользователя всем подключенным клиентам
	 *
	 * @param swoole_websocket_server $ws
	 */
	public function eventManagerStop( swoole_websocket_server $ws ): void {

		$connections = $ws->connection_list();
		$cnt         = \count( $connections );
		echo "\nnow will log out {$cnt} users\n";
		foreach ( $ws->connection_list() as $fd ) {
			$this->logout( $fd );
		}
		echo "\nLogout complete...\n";
	}

	/**
	 * @param                              $fd
	 */
	private function logout( $fd ): void {

		$this->ws->push( $fd, json_encode( [
			                                   'action' => 'logout',
			                                   'data'   => [ '...I do not know you, bro' ],
		                                   ], JSON_FORCE_OBJECT ) );
		$this->ws->close( $fd, true );
	}

	/**
	 * М-д, обрабатывающий пулл тикотов, продляющий их ttl
	 *
	 * @param swoole_process $process
	 *
	 * @throws \Exception
	 */
	public function processTicketKeyFresher( swoole_process $process ): void {

		swoole_set_process_name( "lkp_notifying_refresher_{$this->swoole_params['port']}" );
		try {
			$this->wstr->keyRefresher();
		} catch ( \Exception $e ) {
			echo $e->getMessage() . "\n\n" . $e->getTraceAsString();
		}
	}

	/**
	 * М-д-обработчик (callback), который будет повешан на процесс swoole сформированный в getProcessListener
	 *
	 * @param swoole_process $process
	 */
	public function processListener( swoole_process $process ): void {

		swoole_set_process_name( "lkp_notifying_queue_{$this->swoole_params['port']}" );
		$client = new swoole_redis;
		$client->on( 'close', function() {

			var_dump( func_get_args() );
		} );
		$client->on( 'message', function( swoole_redis $client, $result ) {

			// todo[daglob-bug]: 04.04.2018 17:43 проверять тело сообщения, если пришел ID - дернуть из базы
			// todo[daglob-fix]: 04.04.2018 15:12 построить систему на EVENT-ах компонентов - позволит расширять зону связей/обработки сообщений
			/**
			 * тип сообщения в системе Redis!!!
			 */
			$messageType = $result[0];
			$bodyMessage = json_decode( $result[2], true );
			if ( $messageType === 'message' ) {
				if ( $messageData = Message::instance()->getOne( $bodyMessage['id'] ) ) {
					$rsMessage = Yii::$app->notify->buildMessageByID( $bodyMessage['id'] );
					if ( $targets = $rsMessage->getTargets() ) {
						foreach ( $targets as $userID ) {
							try {
								if ( $channels = $this->wstr->getChannels( $userID ) ) {
									$message = $rsMessage->getMessage();
									/**
                                     * добавляет поле 'created_at' к пакету данных
                                     */
                                    $message['created_at'] = $rsMessage->getCreatedAt();
									foreach ( $channels as $fd ) {
										$this->push( $fd, $message );
									}
								}
							} catch ( \Exception $e ) {
								print_r( $e );
							}
						}
					} else {
						echo "\n\nNo targets\n\n";
					}
				};
			}
		} );
		$client->connect(
			Yii::$app->notify->param['ip'],
			Yii::$app->notify->param['port'],
			function( swoole_redis $client, $result ) {

				if ( $result === false ) {
					echo "connect to redis server failed.\n";

					return;
				}
				foreach ( Yii::$app->notify->param['channels'] as $chanel ) {
					echo "connect to $chanel " . ( ! $client->subscribe( Yii::$app->notify->getHashKey() . ':' . $chanel ) ?:
							"ready \n" );
				}
			}
		);
	}

	/**
	 * Приведение сообщения в JSON, проверка состояния канала, и отправка сообщения в канал
	 *
	 * @param $fd
	 * @param $message
	 */
	private function push( $fd, $message ): void {

		if ( $this->ws->connection_info( $fd ) ) {
			$this->ws->push( $fd, json_encode( $message, JSON_FORCE_OBJECT ) );
		}
		// todo[daglob]: 05.04.2018 12:28 возможно повесить обработчик сообщений, если оно не ушло
	}

	/**
	 * Событие на запуск севрера
	 *
	 * @param swoole_websocket_server $ws
	 */
	public function eventStart( swoole_websocket_server $ws ): void {

		echo "\nstart...\n";
		echo "\nStart \"Swoole {$this->swoole_version}\" web socket server:\n";
		echo "\n======= " . date( 'Y-m-d H:i:s' ) . " =======\n";
	}

	/**
	 * Остановка сервера
	 *
	 * @param swoole_websocket_server $ws
	 *
	 * @throws \RedisException
	 */
	public function eventShutdown( swoole_websocket_server $ws ): void {

		$this->wstr->shutdown();
		echo "\nshutdown...\n";
		echo "\n======= " . date( 'Y-m-d H:i:s' ) . " =======\n";
		echo "\nShutdown \"Swoole {$this->swoole_version}\" web socket server:\n";
	}

	/**
	 * Событие открытия канала
	 *
	 * @param swoole_websocket_server $ws
	 * @param                         $request
	 *
	 * @throws \Exception
	 */
	public function eventOpen( swoole_websocket_server $ws, $request ): void {

		try {
			if ( $ticket = $this->wstr->checkTicketByCode( $request->get['code'] ) ) {
				$this->wstr->registerConnect( $ticket['uid'], $request->fd );
			} else {
				$this->wstr->unRegisterConnect( $request->fd );
				$this->logout( $request->fd );
			}
		} catch ( \Exception $e ) {
			print_r( [
				         $e->getMessage(),
				         $e->getTraceAsString(),
			         ] );
		}
	}

	/**
	 * Событие на получение сообщения
	 *
	 * @param swoole_websocket_server $ws
	 * @param swoole_websocket_frame  $frame
	 *
	 * @throws \Exception
	 */
	public function eventMessage( swoole_websocket_server $ws, swoole_websocket_frame $frame ): void {

		try {

			if ( $uid = $this->wstr->getConnectKeyByFID( $frame->fd ) ) {
				$result = false;
				if ( $frame->finish ) {
					$data = json_decode( $frame->data, true );
					switch ( $data['action'] ) {
						case 'closeMessage':
//							Tools::info( [ "text" => "closeMessage: ".$uid." id: ".$data['id'] ], "debug" );
							$result = Yii::$app->notify->setFilterMessage( $uid, $data['id'] );
							break;
						case 'closeRequest':
//							Tools::info( [ "text" => "closeRequest: ".$uid." request: ".$data['id'] ], "debug" );
							if ( $request = ( new Requests() )->getByNum( $data['id'] ) ) {

								$result = Yii::$app->notify->setFilterByRequest( $uid, $request['id'] );
							} else {
								throw new \Exception( "Не найдена заявка с номером {$data['id']}" );
							}
							break;
						case 'closeAll':
							$bClient = Users::instance()->build( $uid )->isClient() || Users::instance()->build( $uid )
							                                                                ->isMobileUser();
							$pointRequest = ( new Requests() )->getOneByFilter( [
								                                                    ( $bClient ? "client_id" :
									                                                    "user_id" ) => $uid,
							                                                    ] );
							$requests     = ( new Requests() )->getAllByFilter( [
								                                                    "or" => [
									                                                    "point_id"      => $pointRequest["point_id"],
									                                                    ( $bClient ? "client_id" :
										                                                    "user_id" ) => $uid,
								                                                    ],
							                                                    ] );
//							Tools::info( [ "text" => "closeAll: ".$uid." count req: ".count($requests) ], "debug" );
							if ( is_array( $requests ) ) {
								$idsMessage =
								$idsRequest = [];
								foreach ( $requests as $request ) {
									$idsRequest[] = $request['id'];
//									if ($messageList = ( new Message() )->getByRequestID( $request['id'] )) {
//										foreach ($messageList as $message) {
//											$idsMessage[] = $message["id"];
//										}
//									}
								}
								Yii::$app->notify->setFilterByRequest( $uid, $idsRequest );
//								Tools::info( [ "text" => "closeAll: req: ".print_r($idsRequest,true) ], "debug" );
//								Yii::$app->notify->setFilterMessage( $uid, $idsMessage );
//								Tools::info( [ "text" => "closeAll: mess: ".print_r($idsMessage,true) ], "debug" );
								$result = true;
							}
							break;
					}
					$this->push( $frame->fd, [
						$data['action'] => ( $result ? "OK" : "NO" ),
					] );
				}
			} else {
				throw new \Exception( "Не найдено соединение с номером $frame->fd" );
			}
		} catch ( \Exception $e ) {
			print_r( [
				         $e->getMessage(),
				         $e->getTraceAsString(),
			         ] );
		}
	}

	/**
	 * @param swoole_websocket_server $ws
	 * @param                         $frame
	 */
	public function eventReceive( swoole_websocket_server $ws, $frame ): void {

	}

	/**
	 * Событие на подключение к серверу
	 *
	 * @param swoole_websocket_server $ws
	 * @param                         $frame
	 */
	public function eventConnect( swoole_websocket_server $ws, $frame ): void {

	}

	/**
	 * Событие на закрытие соединения
	 *
	 * @param swoole_websocket_server $ws
	 * @param                         $fd
	 *
	 * @throws \RedisException
	 */
	public function eventClose( swoole_websocket_server $ws, $fd ): void {

		$this->wstr->disChanel( $fd );
	}

	/**
	 * М-д-обработчик (callback), который будет повешан на процесс swoole сформированный в getProcessConnectMonitor
	 *
	 * @param swoole_process $process
	 *
	 * @throws \RedisException
	 */
	public function processConnectMonitor( swoole_process $process ): void {

		swoole_set_process_name( "lkp_connection_monitor_{$this->swoole_params['port']}" );
		while ( true ) {
			if ( $connectors = $this->wstr->getConnectors() ) {
				print_r( [
					         'connectors'  => count( $connectors ),
					         'connections' => array_map( function( $connector ) {

						         $cnt = \count( $this->wstr->getChannels( $connector ) );

						         return "{$connector}: {$cnt}";
					         }, $connectors ),
				         ] );
			}
			usleep( 1000000 );
		}
	}
}
