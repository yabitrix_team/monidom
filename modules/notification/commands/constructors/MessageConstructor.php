<?php
/**
 * Created by PhpStorm.
 * User: daglob
 * Date: 12.04.2018
 * Time: 19:50
 */

namespace app\modules\notification\commands\constructors;

use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Users;
use app\modules\notification\classes\NotifyException;
use app\modules\notification\models\MessageAttacheFile;
use Exception;
use Yii;

abstract class MessageConstructor implements InterfaceMessageConstructor {
	public const USE_MAP    = 0;
	public const USE_FILTER = 1;
	/** @var array сообщение-инициатор */
	protected $message;
	/** @var null|array список алертов по заявке */
	protected $alerts = null;
	/** @var array|null функция-обрабботчик списка алертов */
	protected $callbackAlertsFilter;
	/**
	 * @var array - данные по заявке
	 */
	protected $request = [];

	/**
	 * MessageConstructor constructor.
	 *
	 * @param array $message
	 */
	public function __construct( array $message ) {

		$this->message = $message;
	}

	/**
	 * @param \Closure $callbackAlertsFilter
	 * @param null     $filter способ обработки исходного списка сообщений (alerts) с помощью callback функции, может
	 *                         иметь значение:<br/> null - сохраняется исходный формат (default)<br/>
	 *                         0 - используется array_map для построения нового массива<br/>
	 *                         1 - используется array_filter для построения нового массива<br/>
	 *                         <b>Внимание!!! обработка исходного массива происходит только при установленном
	 *                         callbackAlertsFilter, методом setCallbackAlertsFilter( \Closure $callbackAlertsFilter
	 *                         )</b>
	 *
	 * @param int|null $flag   [optional] значение использеутся только если параметр <i>$filter</i> равен 1. Флаг,
	 *                         определяющий, какие аргументы передавать в <i>callback</i>:
	 *                         <ul>
	 *                         <li>
	 *                         <b>ARRAY_FILTER_USE_KEY</b> - передавать только ключ массива как аргумент для
	 *                         <i>callback</i> вместо значения
	 *                         </li>
	 *                         <li>
	 *                         <b>ARRAY_FILTER_USE_BOTH</b> - передавать и ключ, и значение в <i>callback</i> вместо
	 *                         только значения
	 *                         </li>
	 *                         </ul>
	 */
	public function setCallbackAlertsFilter( \Closure $callbackAlertsFilter, $filter = null, $flag = null ): void {

		$callBackAdd = [
			'filter'   => $filter,
			'callback' => $callbackAlertsFilter,
		];
		if ( $flag !== null ) {
			$callBackAdd['flag'] = $flag;
		}
		$this->callbackAlertsFilter[] = $callBackAdd;
	}

	public function delCallbackAlertsFilter(): void {

		$this->callbackAlertsFilter = null;
	}

	/**
	 * Метод получения целей, в данном случае идентификаторов пользователей
	 *
	 * @param bool $checkClient - флаг для осуществления проверки клиентских пользователей,
	 *                          к ним относятся пользователи ЛКК и МП
	 *
	 * @return array|bool - вернет массив идентификаторов юзеров, иначе ложь
	 */
	public function getTargets( bool $checkClient = true ) {

		try {
			$targets = [];
			$this->getRequest();
			//массив подписантов, клиентам которых сообщения отправляются только лично
			$defaultUsers = [
				Yii::$app->params['client']['settings']['user_id'],
				Yii::$app->params['mp']['settings']['user_id'],
			];
			$targetUser   = ! empty( $checkClient ) && in_array( $this->request['user_id'], $defaultUsers )
				? $this->request['client_id']
				: $this->request['user_id'];
			//если пользователь онлайн выбираем, иначе проверяем что не клиент и МП и выбираем все юзеров точки
			if ( Yii::$app->notify->getSRS()->isOnline( $targetUser ) ) {
				$targets[] = $targetUser;
			} else {
				if ( ! in_array( $this->request['user_id'], $defaultUsers ) || empty( $checkClient ) ) {
					$users   = ( new Users() )->getByPoint( $this->request['point_id'] );
					$targets = array_merge( $targets, array_column( $users, 'id' ) );
				}
			}

			return $targets ?: false;
		} catch ( \Exception $e ) {
			return false;
		}
	}

	/**
	 * Метод формирования сообщения
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getMessage() {

		return $this->buildMessage();
	}
    /**
     * Получение даты записи в БД
     * используется RunController.php 189 для добавления created_at к пакету данных на уровне
     * ['action', 'data', 'created_at']
     *
     * @return mixed
     */
    public function getCreatedAt(){
        return ($this->message['created_at'] ? (new \DateTime($this->message['created_at']))->format("d.m.Y H:i:s") : "");
    }
	/**
	 * @return array
	 * @throws Exception
	 */
	private function buildMessage(): array {

		return [
			'message' => $this->message,
			'alerts'  => $this->getAlerts(),
		];
	}

	/**
	 * @return bool|array
	 * @throws Exception
	 */
	protected function getAlerts() {

		if ( $this->alerts !== null ) {
			if ( $this->callbackAlertsFilter !== null ) {

				foreach ( $this->callbackAlertsFilter as $callBackFilter ) {
					if ( ! array_key_exists( 'filter', $callBackFilter ) ) {
						throw new Exception( 'Не установлен способ обработки массива сообщений' );
					}
					if ( \in_array( (int) $callBackFilter['filter'], [
						self::USE_MAP,
						self::USE_FILTER,
					], true ) ) {
						/*
						 * check callback
						 */
						if ( ! array_key_exists( 'callback', $callBackFilter ) ) {
							throw new Exception( 'Не установлен параметр "callback"' );
						}
						if ( ! \is_callable( $callBackFilter['callback'] ) ) {
							throw new Exception( 'Значение "callback" переданное в качестве параметра не является is_callable' );
						}
					}
					$O_al = $this->alerts;
					switch ( $callBackFilter['filter'] ) {
						case self::USE_MAP:
							$this->alerts = array_map( $callBackFilter['callback'], $this->alerts );
							break;
						case self::USE_FILTER:
							$this->alerts = array_filter( $this->alerts, $callBackFilter['callback'], $callBackFilter['flag'] );
							break;
						case null:
						default:
					}
				}
			}

			return $this->alerts;
		}

		return false;
	}

	protected function getAttache(): void {

		if (
			$this->message !== null &&
			$attaches = MessageAttacheFile::instance()
		) {

		}
	}

	protected function CabinetsGroups(): void {
	}

	/**
	 * Метод получения данных заявки по идентификатору заявки заложенному в сообщениях
	 *
	 * @return array|false
	 * @throws NotifyException
	 */
	protected function getRequest() {

		try {
			$className   = ( new \ReflectionClass( get_class( $this ) ) )->getShortName(); //пример: sysConstructor
			$messageType = mb_strstr( $className, 'Constructor', true ); //пример: sys
			if ( ! isset( $this->message['order_id'] ) || $this->message['order_id'] === '' ) {
				throw new NotifyException( 'Не указан идентификатор заявки в сообщении типа ' . $messageType );
			}
			$this->request = empty( $this->request )
				? ( new Requests() )->getOne( $this->message['order_id'] )
				: $this->request;
			if ( empty( $this->request ) ) {
				throw new NotifyException( 'Не удалось найти заявку для сообщения типа ' . $messageType );
			}
		} catch ( \ReflectionException $e ) {
			throw new NotifyException( 'При попытке получения заявки не удалось получить тип сообщения', $e->getCode(), $e );
		} catch ( \yii\db\Exception $e ) {
			throw new NotifyException( 'Ошибка получения данных заявки из БД при формировании сообщения типа ' . $messageType, $e->getCode(), $e );
		} catch ( NotifyException $e ) {
			throw $e;
		}

		return $this->request;
	}
}