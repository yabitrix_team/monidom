<?php
/**
 * Created by PhpStorm.
 * User: daglob
 * Date: 12.04.2018
 * Time: 19:47
 */

namespace app\modules\notification\commands\constructors;
interface InterfaceMessageConstructor {
	public function getTargets();
}