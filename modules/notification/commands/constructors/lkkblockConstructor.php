<?php
namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class reqConstructor - класс конструктор для уведомлений по работе с заявкой
 *
 * @package app\modules\notification\commands\constructors
 */
class lkkblockConstructor extends MessageConstructor
{
    /**
     * Метод формирования сообщения
     *
     * @return mixed
     */
    public function getMessage()
    {
        $result['action'] = 'lkkBlock';
        try {
            $this->getRequest();
            $result['data'] = [
                'num' => $this->request['num'],
            ];
        } catch (NotifyException $e) {
            $result['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $result['error'] = 'Произошла неизвестная ошибка при формировании сообщения типа lkkBlock';
        }

        return $result;
    }

    public function getTargets(bool $checkClient = true)
    {
        try {
            $this->getRequest();
            if ($this->request['client_id']) {
                $targets = [$this->request['client_id']];
            }

            return $targets ?: false;
        } catch (\Exception $e) {
            return false;
        }
    }
}