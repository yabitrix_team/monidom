<?php

namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class requpConstructor - класс конструктор для уведомлений при обновлении заявки
 *
 * @package app\modules\notification\commands\constructors
 */
class requpConstructor extends MessageConstructor {
	/**
	 * Метод формирования сообщения
	 *
	 * @return mixed
	 */
	public function getMessage() {

		$result['action'] = 'changeFieldsRequest';
		try {
			$this->getRequest();
			$result['data'] = [
				'num'    => $this->request['num'],
				'code'   => $this->request['code'],
				'fields' => json_decode( $this->message['message'] ),
			];
		} catch ( NotifyException $e ) {
			$result['error'] = $e->getMessage();
		}

		return $result;
	}
}