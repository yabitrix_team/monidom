<?php

namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class firstpackConstructor - класс конструктор для уведомлений при получении первого пакета документов
 *
 * @package app\modules\notification\commands\constructors
 */
class firstpackConstructor extends MessageConstructor {
	/**
	 * Метод формирования сообщения
	 *
	 * @return mixed
	 */
	public function getMessage() {

		$result['action'] = 'hasFirstPack';
		try {
			$this->getRequest();
			$result['data'] = $this->request['code'];
		} catch ( NotifyException $e ) {
			$result['error'] = $e->getMessage();
		}

		return $result;
	}
}