<?php

namespace app\modules\notification\commands\constructors;

use app\modules\app\v1\models\StatusesJournal;
use app\modules\notification\classes\NotifyException;
use app\modules\notification\models\Message;

/**
 * Class statusConstructor
 *
 * @package app\modules\notification\commands\constructors
 */
class statusConstructor extends MessageConstructor {
	/**
	 * Метод формирования сообщения
	 *
	 * @return mixed
	 */
	public function getMessage() {

		$result['action'] = 'insertNotify';
		try {
			$this->getRequest();
			$requestStatus = ( new StatusesJournal() )->getCurrentByRequestID( $this->request['id'] );
			if ( ( $messageList = ( new Message() )->getByRequestID( $this->request['id'] ) ) && \is_array( $messageList ) ) {
				$this->alerts                = array_map( function( $value ) {

					return [
						'type_id' => $value['type_id'],
						'message' => $value['message'],
						'id'      => $value['id'],
                        'created_at' => ($value['created_at'] ? (new \DateTime($value['created_at']))->format("d.m.Y H:i:s") : ""),
					];
				}, $messageList );
				$this->message['created_at'] = ! empty( $this->message['created_at'] ) ? $this->message['created_at'] :
					array_shift( $messageList )['created_at'];
			}
			$result['data'] = [
				$this->request['num'] => [
					'type'              => $this->message['type_id'],
					'request_num'       => $this->request['num'],
					'request_code'      => $this->request['code'],
					'client_first_name' => $this->request['client_first_name'],
					'client_last_name'  => $this->request['client_last_name'],
					'client_patronymic' => $this->request['client_patronymic'],
					'summ'              => $this->request['summ'],
					'date'              => date( 'd.m.y, H:i', strtotime( $this->message['created_at'] ) ),
					'status'            => $requestStatus['status_id'],
					'alerts'            => $this->getAlerts(),
				],
			];
		} catch ( NotifyException $e ) {
			$result['error'] = $e->getMessage();
		} catch ( \Exception $e ) {
			$result['error'] = 'Произошла неизвестная ошибка при формировании сообщения типа status';
		}

		return $result;
	}
}