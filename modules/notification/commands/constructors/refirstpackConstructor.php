<?php
namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class reqConstructor - класс конструктор для уведомлений по работе с заявкой
 *
 * @package app\modules\notification\commands\constructors
 */
class refirstpackConstructor extends MessageConstructor
{
    /**
     * Метод формирования сообщения
     *
     * @return mixed
     */
    public function getMessage()
    {
        $result['action'] = 'reFirstPack';
        try {
            $this->getRequest();
            $result['data'] = [
                'num' => $this->request['num'],
            ];
        } catch (NotifyException $e) {
            $result['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $result['error'] = 'Произошла неизвестная ошибка при формировании сообщения типа reFirstPack';
        }

        return $result;
    }

    public function getTargets(bool $checkClient = true)
    {
        try {
            $this->getRequest();
            if ($this->request['user_id']) {
                $targets = [$this->request['user_id']];
            }

            return $targets ?: false;
        } catch (\Exception $e) {
            return false;
        }
    }
}