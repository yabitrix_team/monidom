<?php
namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class cardVerifierConstructor - класс конструктор для уведомлений по работе с заявкой
 *
 * @package app\modules\notification\commands\constructors
 */
class cardVerifierConstructor extends MessageConstructor
{
    /**
     * Метод формирования сообщения
     *
     * @return mixed
     */
    public function getMessage()
    {
        $result['action'] = 'cardVerifier';
        try {
            $this->getRequest();
            $result['data'] = json_decode($this->message['message']);
        } catch (NotifyException $e) {
            $result['error'] = $e->getMessage();
        } catch (\Exception $e) {
            $result['error'] = 'Произошла неизвестная ошибка при формировании сообщения типа cardVerifier';
        }

        return $result;
    }
}