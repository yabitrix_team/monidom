<?php
/**
 * Created by PhpStorm.
 * User: daglob
 * Date: 13.04.2018
 * Time: 17:58
 */

namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\WSSessionRegistration as WSSR;

class testConstructor extends MessageConstructor {
	private $request;

	public function getTargets() {

		return WSSR::getInstance()->getConnectors();
	}

	/**
	 * @return array|mixed
	 * @throws \Exception
	 */
	public function getMessage() {

		return [
			"test-{$this->request['num']}" => $this->message,
		];
	}
}