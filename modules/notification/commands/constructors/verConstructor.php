<?php

namespace app\modules\notification\commands\constructors;

use app\modules\notification\classes\NotifyException;

/**
 * Class verConstructor - класс конструктор для уведомлений верификации
 *
 * @package app\modules\notification\commands\constructors
 */
class verConstructor extends MessageConstructor {
	/**
	 * Метод формирования сообщения
	 *
	 * @return mixed
	 */
	public function getMessage() {

		$result['action'] = 'verMessage';
		try {
			$this->getRequest();
			$result['data'] = [
				'code'        => $this->request['num'],
				'message'     => 'Запрос данных: ' . $this->message['message'],
				'question_id' => $this->message['id'],
			];
		} catch ( NotifyException $e ) {
			$result['error'] = $e->getMessage();
		}

		return $result;
	}
}