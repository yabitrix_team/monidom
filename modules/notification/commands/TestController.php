<?php

namespace app\modules\notification\commands;

use app\modules\notification\models\MessageType;
use Yii;
use yii\console\Controller;

define( 'DEBUG', 'off' );

/**
 * Notification module
 */
class TestController extends Controller {
	/**
	 * @return array
	 */
	public function actionAdd() {

		try {
		    /*
            $mess = [
                "type_id"  => MessageType::instance()->refirstpack,
                "order_id" => 20489,
                "message"  => json_encode(["num" => 700038], JSON_FORCE_OBJECT),
            ];*/
		    $req["accreditation_num"] = "650190";

            $mess = [
                "type_id"  => MessageType::instance()->requp,
                "order_id" => 67283,
                "message"  => json_encode(
                    ["accreditation_num" => $req["accreditation_num"]],
                    JSON_FORCE_OBJECT
                ),
            ];
            /*
            $req["summ"] = 370000;
            $mess = [
                "type_id"  => MessageType::instance()->requp,
                "order_id" => 67282,
                "message"  => json_encode(
                    ["summ" => $req["summ"]],
                    JSON_FORCE_OBJECT
                ),
            ];
            */

			Yii::$app->notify->addMessage( $mess, true );
			print_r($mess);
		} catch ( \Exception $e ) {
			print_r( $e->getMessage() );
		}
	}
}
