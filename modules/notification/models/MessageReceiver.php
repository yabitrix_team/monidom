<?php
namespace app\modules\notification\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\Requests;
use yii\db\Expression;
use yii\db\Exception;
use yii\base\Model;
use Yii;

/**
 * Class MessageReceiver - модель для работы с таблицей связей сообщения с пользователями
 *
 * @package app\modules\notification\models
 */
class MessageReceiver extends Model
{
    /**
     * Подключение трейта, отвечающего за получение данных из таблицы
     */
    use ReadableTrait;
    /**
     * @var int - идентификатор сообщения
     */
    public $message_id;
    /**
     * @var int - идентификатор пользователя
     */
    public $user_id;
    /**
     * @var int - идентификатор статуса
     */
    public $status_id;

    /**
     * Получение названия таблицы модели в БД
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return '{{message_receiver}}';
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'message_id',
            'user_id',
            'status_id',
            'active',
            'updated_at',
            'created_at',
        ];
    }

    /**
     * @return array - правила валидации передаваемых данных
     */
    public function rules()
    {
        return [
            [
                'message_id',
                'required',
                'message' => 'В передаваемых параметрах не указан идентификатор уведомления',
            ],
            [
                'user_id',
                'required',
                'message' => 'В передаваемых параметрах не указан идентификатор получателя',
            ],
            [
                'status_id',
                'required',
                'message' => 'В передаваемых параметрах не указан идентификатор статуса уведомления',
            ],
            [
                'message_id',
                'number',
                'message' => 'Идентификатор уведомления указан в некорректном формате',
            ],
            [
                'user_id',
                'number',
                'message' => 'Идентификатор получателя указан в некорректном формате',
            ],
            [
                'status_id',
                'number',
                'message' => 'Идентификатор статуса уведомления указан в некорректном формате',
            ],
        ];
    }

    /**
     * Метод получения уведомлений пользователя
     * если не указать тип и статус вернет все увеодмления пользователя
     *
     * @param int              $userId - идентификатор пользователя
     * @param int|string|array $type   - тип увеодмления (принимет id или имя типа, а так же массив id или имен типов)
     * @param int|string|array $status - статус прочитанности (принимет id или имя или массив их)
     *
     * @return array - вернет массив уведомлений
     * @throws \Exception
     */
    public function getUserMessages(int $userId, $type = null, $status = null)
    {
        try {
            $params    = [':userId' => $userId];
            $condition = '';
            if (!empty($type)) {
                $messageType = MessageType::instance();
                $typeId      = is_string($type)
                    ? (is_numeric($type) ? $type : $messageType->{$type})
                    : (is_array($type) ? array_map(
                        function ($v) use ($messageType) {
                            return is_numeric($v) ? $v : $messageType->{$v};
                        },
                        $type
                    ) : false);
                $condition   = !empty($typeId)
                    ? (is_numeric($typeId)
                        ? ' AND [[m]].[[type_id]] = :typeId '
                        : ' AND [[m]].[[type_id]] IN ("'.implode('","', $typeId).'")')
                    : $condition;
                $params      = !empty($typeId) && is_numeric($typeId)
                    ? array_merge($params, [':typeId' => $typeId])
                    : $params;
            }
            if (!empty($status)) {
                $messageStatus = MessageStatus::instance();
                $statusId      = is_string($status)
                    ? (is_numeric($status) ? $status : $messageStatus->{$status})
                    : (is_array($status) ? array_map(
                        function ($v) use ($messageStatus) {
                            return is_numeric($v) ? $v : $messageStatus->{$v};
                        },
                        $status
                    ) : false);
                $condition     = !empty($statusId)
                    ? $condition.(is_numeric($statusId)
                        ? ' AND [[mr]].[[status_id]] = :statusId '
                        : ' AND [[mr]].[[status_id]] IN ("'.implode('","', $statusId).'")')
                    : $condition;
                $params        = !empty($statusId) && is_numeric($statusId)
                    ? array_merge($params, [':statusId' => $statusId])
                    : $params;
            }

            return Yii::$app->db->createCommand(
                $this->getSqlForUserMessages($condition),
                $params
            )
                ->queryAll();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения уведомлений пользователя из базы данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод получения sql запроса для выборки уведомлений пользователя
     * вызывается в методе getUserMessages
     *
     * @param string $condition - дополнительные условия запроса
     *
     * @return string - вернет строку sql-запроса
     */
    protected function getSqlForUserMessages(string $condition = ''): string
    {
        return 'SELECT 
                   [[m]].[[code]], 
                   [[m]].[[message]], 
                   [[m]].[[type_id]], 
                   [[mr]].[[status_id]],
                   [[m]].[[created_at]],
                   [[m]].[[order_id]] as [[request_id]]
            FROM '.$this->getTableName().' as [[mr]]
            JOIN {{message}} AS [[m]] ON [[m]].[[id]] = [[mr]].[[message_id]]
            WHERE [[mr]].[[user_id]] = :userId AND [[mr]].[[active]] = 1 AND [[m]].[[active]] = 1 '.$condition.'
            ORDER BY [[m]].[[created_at]] DESC';
    }

    /**
     * Метод добавления получателя уведомлений
     *
     * @return bool - вернет истину при успехе, иначе ложь
     * @throws \Exception
     */
    public function create()
    {
        try {
            if (!$this->validate()) {
                throw new \Exception('Ошибка валидации данных при утсановке получателя уведмолений');
            }
            $insert = $this->getAttributes();

            return (bool) Yii::$app->db->createCommand()
                ->insert($this->getTableName(), $insert)
                ->execute();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка записи в базу данных получателя уведомлений, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод добавления получателя уведомлений по типу уведомлений
     * для обработки нового типа увеодмлений нужно создать метод setReceiverBy...()
     * который должен добавлять получателя относительно типа увеодмлений
     *
     * @param int $typeId    - идентификатор типа уведомления
     * @param int $messageId - идентификатор уведомления
     * @param int $requestId - идентификатор заявки
     *
     * @return bool
     * @throws \Exception
     */
    public function addByMessageType(int $typeId, int $messageId, int $requestId)
    {
        $types = MessageType::instance()->relationPublicTypes;
        if (!array_key_exists($typeId, $types)) {
            throw new \Exception(
                'Не удалось добавить получателя уведомления, так как был указан тип уведомления отсутствующий в системе, обратитесь к администратору'
            );
        }
        $this->message_id = $messageId;
        $this->status_id  = MessageStatus::instance()->getNew();
        $method           = 'setReceiverBy'.ucfirst($types[$typeId]);
        if (method_exists($this, $method)) {
            return $this->$method($requestId);
        }

        return false;
    }

    /**
     * Метод установки получателя уведомлений по типу уведомлений Ver
     *
     * @param $requestId - идентификатор заявки
     *
     * @return bool
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    protected function setReceiverByVer($requestId)
    {
        $request = (new Requests())->getOne($requestId, ['user_id', 'client_id']);
        if (empty($request)) {
            throw new \Exception(
                'Не удалось установить получателя уведомлений по заявке, обратитесь к администратору'
            );
        }
        //получение идентификатора клиента или партнера в роли получателя уведомления
        $this->user_id = !in_array(
            (int) $request['user_id'],
            [
                (int) Yii::$app->params['client']['settings']['user_id'],
                (int) Yii::$app->params['mp']['settings']['user_id'],
            ],
            true
        ) ? $request['user_id'] : $request['client_id'];

        return $this->create();
    }

    /**
     * Метод обновления статуса по коду уведомления
     *
     * @param            $idOrCode - код увеодмления
     * @param string|int $status   - статус уведомления(принимает id или имя статуса)
     * @param int        $userId   - идентификатор пользователя, если указать null,
     *                             то установит статус всем пользователям уведомления
     *
     * @return bool - вернет истину в случае успешного обновления
     * @throws \Exception
     */
    public function updateStatusByIdOrCodeMessage($idOrCode, $status, int $userId = null)
    {
        try {
            is_numeric($status) ?: $status = MessageStatus::instance()->{$status};
            $condition = strlen($idOrCode) == 32
                ? '[[message_id]] = (SELECT [[id]] FROM {{message}} WHERE code = :code)'
                : '[[message_id]] = :messageId';
            $params    = strlen($idOrCode) == 32
                ? [':code' => $idOrCode]
                : [':messageId' => $idOrCode];
            empty($userId) ?: $condition = $condition.' AND [[user_id]] = :userId ';
            empty($userId) ?: $params[':userId'] = $userId;
            $result = Yii::$app->db->createCommand()->update(
                $this->getTableName(),
                [
                    'status_id'  => $status,
                    'updated_at' => new Expression('NOW()'),
                ],
                $condition,
                $params
            )->execute();

            return (bool) $result;
        } catch (\Exception $e) {
            throw new \Exception(
                'Ошибка обновления статсу уведомления по коду в базе данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод деактивации получателя уведомлений
     *
     * @param string   $code     - код уведомления
     * @param int|null $userId   - идентификатор пользователя, если указать null,
     *                           то деактивировать всех получателей уведомления
     *
     * @return bool - вернет истину в случае успешной деактивации
     * @throws \yii\db\Exception
     */
    public function deactivateByCodeMessage(string $code, int $userId = null)
    {
        $condition = '[[message_id]] = (SELECT [[id]] FROM {{message}} WHERE code = :code)';
        $params    = [':code' => $code];
        empty($userId) ?: $condition = $condition.' AND [[user_id]] = :userId ';
        empty($userId) ?: $params[':userId'] = $userId;
        $result = Yii::$app->db->createCommand()->update(
            $this->getTableName(),
            [
                'active'     => 0,
                'updated_at' => new Expression('NOW()'),
            ],
            $condition,
            $params
        )->execute();

        return (bool) $result;
    }

    /**
     * Метод получения статуса уведомления пользователя
     *
     * @param      $idOrCodeMsg - идентификатор или код уведомления
     * @param null $userId      - идентификатор пользователя
     * @param bool $getAsId     - в каком виде получим ответ, как идентификатор или как название статуса
     *
     * @return string|false - в зависимости от параметра getAsId вернет либо идентификатор статуса,
     *                        либо название название статуса
     *                      иначе вернет ложь
     * @throws \Exception
     */
    public function getStatusByMessageAndUserId($idOrCodeMsg, $userId = null, $getAsId = true)
    {
        try {
            !empty($userId) ?: $userId = Yii::$app->user->id;
            $condition = '[[user_id]] = :userId AND ';
            $param     = [':userId' => $userId];
            $condition .= strlen($idOrCodeMsg) === 32
                ? '[[message_id]] = (SELECT [[id]] FROM {{message}} WHERE [[code]] =:code)'
                : '[[message_id]] =:messageId';
            strlen($idOrCodeMsg) === 32
                ? $param[':code'] = $idOrCodeMsg
                : $param[':messageId'] = $idOrCodeMsg;
            $result = Yii::$app->db
                ->createCommand(
                    'SELECT [[status_id]] FROM '.$this->getTableName().' WHERE '.$condition,
                    $param
                )->queryOne();
            if (!empty($result)) {
                $result = !empty($getAsId)
                    ? $result['status_id']
                    : MessageStatus::instance()->getRelation()[$result['status_id']];
            }

            return $result;
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения данных получателей уведомления из базы данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }
}
