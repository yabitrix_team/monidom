<?php
namespace app\modules\notification\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "message_message".
 *
 * @property int     $id
 * @property int     $parent_message_id ID сообщение-родитель
 * @property int     $child_message_id  ID сообщение-дитя
 * @property Message $childMessage
 * @property Message $parentMessage
 */
class MessageMessage extends Model
{
    public $parent_message_id;
    public $child_message_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_message_id', 'child_message_id'], 'required'],
            [['parent_message_id', 'child_message_id'], 'integer'],
        ];
    }

    /**
     * Получение названия таблицы
     *
     * @return string
     */
    protected function getTableName(): string
    {
        return 'message_message';
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            'id',
            'parent_message_id',
            'child_message_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'parent_message_id' => 'Parent Message ID',
            'child_message_id'  => 'Child Message ID',
        ];
    }

    /**
     * Метод создания записи в таблице БД
     *
     * @return int
     * @throws \yii\db\Exception
     */
    public function create()
    {
        $db  = Yii::$app->db;
        $cmd = $db->createCommand()->insert(
            'message_message',
            [
                "parent_message_id" => $this->parent_message_id,
                "child_message_id"  => $this->child_message_id,
            ]
        );

        return $cmd->execute() ? $db->getLastInsertID() : false;
    }

    /**
     * Метод получения данных ответного уведомления
     *
     * @param       $idOrCodeParentMsg - идентификатор или код родительского уведомления
     * @param array $fields            - поля для выборки
     * @param bool  $queryAll          - true выбрать все записи, false выбрать одну запись
     *
     * @return array|false - результирующий массив или ложь
     * @throws \Exception
     */
    public function getByParentMessage($idOrCodeParentMsg, array $fields = [], $queryAll = true)
    {
        try {
            $fields    = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields    = '[['.implode("]],[[", $fields).']]';
            $method    = empty($queryAll) ? 'queryOne' : 'queryAll';
            $condition = strlen($idOrCodeParentMsg) == 32
                ? '[[parent_message_id]] = (SELECT [[id]] FROM {{message}} WHERE [[code]] =:code)'
                : '[[parent_message_id]] =:parentMessageId';
            $param     = strlen($idOrCodeParentMsg) == 32
                ? [':code' => $idOrCodeParentMsg]
                : [':parentMessageId' => $idOrCodeParentMsg];

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE '.$condition,
                    $param
                )->{$method}();
        } catch (\Exception $e) {
            throw new \Exception(
                'Ошибка получения ответного уведомления из базы данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }
}
