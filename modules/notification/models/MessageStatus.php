<?php
namespace app\modules\notification\models;

use Yii;
use yii\base\Model;

/**
 * Класс работы с данными по статусам сообщений
 *
 * @property false|int $default  - статус по умолчанию
 * @property false|int $answered - Возвращает ID статуса ANSWERED "Отвеченое"
 * @property false|int $read     - Возвращает ID статуса READ "Прочтено"
 * @property false|int $new      - Возвращает ID статуса NEW "Новое"
 * @property array     $list     - массив основных данных по статусамв формате: [[id, name, is_default], ...]
 */
class MessageStatus extends Model
{
    use MessageHelper;
    /**
     * Константы статусов уведомлений
     */
    CONST NEW      = 'new';
    CONST READ     = 'read';
    CONST ANSWERED = 'answered';

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getList()
    {

        return Yii::$app->notify->connection->createCommand(
            'SELECT
[[id]],
[[name]],
[[is_default]],
[[active]]
FROM {{message_status}}'
        )
            ->queryAll();
    }

    /**
     * @see $read
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getRead()
    {

        return $this->findByField(self::READ);
    }

    /**
     * @see $new
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getNew()
    {

        return $this->findByField(self::NEW);
    }

    /**
     * @see $answered
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getAnswered()
    {

        return $this->findByField(self::ANSWERED);
    }

    /**
     * Метод получения связи идентификаторов статусов с их наименованием и наоборот
     *
     * @param bool $flip - флаг, отвечает за замену местами ключе со значениями в массиве
     *
     * @return array|null - вернет массив связей
     * @throws \yii\db\Exception
     */
    public function getRelation(bool $flip = false)
    {
        $result = $this->getList();
        if (!empty($result)) {
            $result = array_column($result, 'name', 'id');
            empty($flip) ?: $result = array_flip($result);
        }

        return $result;
    }
}