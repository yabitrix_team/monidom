<?php
namespace app\modules\notification\models;

use app\modules\app\v1\models\File;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * This is the model class for table "message_attache_file".
 *
 * @property int     $id
 * @property int     $message_id
 * @property int     $file_id
 * @property string  $created_at
 * @property string  $updated_at
 * @property int     $active
 * @property int     $sort
 * @property File    $file
 * @property Message $message
 */
class MessageAttacheFile extends Model
{
    /**
     * @var
     */
    public $message_id;
    /**
     * @var
     */
    public $file_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {

        return 'message_attache_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['message_id', 'file_id'], 'required'],
            [['message_id', 'file_id'], 'integer'],
        ];
    }

    /**
     * Метод создания записи в БД
     *
     * @return bool|string - вернет идентификтор записи или ложь
     * @throws \Exception
     */
    public function create()
    {
        try {
            $db  = Yii::$app->db;
            $cmd = $db->createCommand()->insert(
                "message_attache_file",
                [
                    "message_id" => $this->message_id,
                    "file_id"    => $this->file_id,
                ]
            );
            if ($cmd->execute()) {
                return $db->getLastInsertID();
            }

            return false;
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка добавления в базу данных связи файла с уведомлением, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Метод получения идентификаторов файлов
     *
     * @param  $idOrCode - id или код уведомления
     *
     * @return array|int - вернет список идентифкаторов файлов, либо пустой массив
     * @throws \Exception
     */
    public function getFileIdByMessage($idOrCode)
    {
        try {
            $condition = ' [[active]] = 1 AND ';
            $condition .= strlen($idOrCode) == 32
                ? '[[message_id]] = (SELECT [[id]] FROM {{message}} WHERE [[code]] =:code)'
                : '[[message_id]] =:messageId';
            $param     = strlen($idOrCode) == 32
                ? [':code' => $idOrCode]
                : [':messageId' => $idOrCode];
            $result    = Yii::$app->db
                ->createCommand(
                    'SELECT [[file_id]] FROM '.self::tableName().' WHERE '.$condition,
                    $param
                )->queryAll();

            return !empty($result) ? array_column($result, 'file_id') : [];
        } catch (\Exception $e) {
            throw new \Exception(
                'Ошибка получения файлов ответного уведомления из базы данных, обратитесь к администратору',
                $e->getCode(),
                $e
            );
        }
    }
}