<?php

namespace app\modules\notification\models;

use app\modules\app\v1\models\File;
use Yii;
use yii\base\Model;

/**
 * This is the model class for table "message_attache_file".
 *
 * @property int     $id
 * @property int     $message_id
 * @property int     $kk_id
 * @property int     $doc_type
 * @property int     $file_id
 * @property string  $created_at
 * @property string  $updated_at
 * @property int     $active
 * @property int     $sort
 *
 * @property File    $file
 * @property Message $message
 */
class MessageAttributes extends Model {
	/**
	 * Константа сценария создания заявки
	 */
	const SCENARIO_CREATE = 'create';
	/**
	 * Константа сценария обновления заявки
	 */
	const SCENARIO_LIST = 'list';
	/**
	 * @var
	 */
	public $message_id;
	/**
	 * @var
	 */
	public $kk_id;
	/**
	 * @var
	 */
	public $doc_type;
	/**
	 * @var
	 */
	public $rmq_data_packet_id;
	/**
	 * @var array - поля создания записи атрибута для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $fieldScenarioCreate = [
		'message_id',
		'kk_id',
		'doc_type',
		'rmq_data_packet_id',
	];
	/**
	 * @var array - поля создания записи атрибута для сценария Создание,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $fieldScenarioList = [
		'message_id',
	];

	/**
	 * Метод установки сценария для модели при валидации полей
	 *
	 * @return array - вернет массив сценариев,
	 * по умолчанию установлен в сценарии установлен DEFAULT
	 */
	public function scenarios() {

		$scenarios                          = parent::scenarios();
		$scenarios[ self::SCENARIO_CREATE ] = $this->fieldScenarioCreate;
		$scenarios[ self::SCENARIO_LIST ]   = $this->fieldScenarioList;

		return $scenarios;
	}

	/**
	 * @inheritdoc
	 */
	public static function getTableName() {

		return '{{message_attributes}}';
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected static function getFieldsNames(): array {

		return [
			'message_id',
			'kk_id',
			'doc_type',
			'rmq_data_packet_id',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {

		return [
			[ [ 'message_id' ], 'required' ],
			[ [ 'kk_id' ], 'required' ],
			[ [ 'doc_type' ], 'required' ],
			[ [ 'message_id' ], 'number' ],
			[ [ 'kk_id', 'doc_type' ], 'string' ],
			[ [ 'rmq_data_packet_id' ], 'integer' ],
		];
	}

	/**
	 * @return bool|string
	 * @throws \yii\db\Exception
	 */
	public function create() {

		if ( ! $this->validate() ) {
			return $this;
		}
        $db  = Yii::$app->getDb();
		$cmd = $db->createCommand()->insert( $this->getTableName(), $this->getAttributes() );
		if ( $cmd->execute() ) {
			return $db->getLastInsertID();
		}

		return false;
	}

	public function getAttrList() {

		if ( ! $this->validate() ) {
			return $this;
		}
        $db  = Yii::$app->getDb();
		return $db->createCommand(
			'SELECT [[' . implode( "]],[[", self::getFieldsNames() ) . ']] FROM ' . $this->getTableName() . ' WHERE [[message_id]] = :message_id',
			[ ':message_id' => $this->message_id ] )->queryOne();
	}
}