<?php
namespace app\modules\notification\models;

use app\modules\app\v1\helpers\DBHelper;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\Partners;
use app\modules\app\v1\models\Points;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use app\modules\app\v1\models\Users;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * @property string      $tableName
 * @property array       $fieldsNames
 * @property array|false $cabinets
 * @property mixed       $parentId
 * @property mixed       $childId
 * @property array       $prepareAttributes
 */
class Message extends Model
{
    use WritableTrait;
    use ReadableTrait;
    const SCENARIO_ADD           = 'add';
    const SCENARIO_CHANGE_STATUS = 'read';
    ///////////////////// properties start
    public $sort;
    public $code;
    public $message;
    public $id;
    public $sys_date;
    public $type_id;
    public $active;
    public $name;
    public $status_id;
    public $user_id_from = null;
    public $user_id_to   = null;
    public $order_id     = null;
    ///////////////////// properties end
    protected $_cabinets;
    protected $_parent_id;
    protected $_child_id;

    public function getCabinets()
    {
        return $this->_cabinets;
    }

    /**
     * @param array|null $cabinets
     *
     * @return bool
     * @throws \Exception
     */
    public function setCabinets(array $cabinets = null)
    {
        if (isset($cabinets) && !empty($cabinets)) {
            $this->_cabinets = $cabinets;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // todo[daglob]: 11.04.2018 16:17 необходимо выстроить схему валидируемых полей, зависимых от типа сообщения
        return [
            [["type_id", "status_id"], "filter", "filter" => "trim"],
            [["type_id", "status_id", "message"], "string"],
            [["id", "user_id_from", "user_id_to", "order_id"], "integer"],
            [
                "type_id",
                'in',
                'range' => array_column(MessageType::instance()->list, 'id'),
            ],
            [
                "status_id",
                'in',
                'range' => array_column(MessageStatus::instance()->list, 'id'),
            ],
            // устанавливаем дефодтные значения для обязательных полей (должны быть установлены в БД)
            ['type_id', 'default', 'value' => MessageType::instance()->default],
            ['status_id', 'default', 'value' => MessageStatus::instance()->default],
            [["type_id", "status_id"], "required"],
            ['active', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios                               = parent::scenarios();
        $scenarios[self::SCENARIO_ADD]           = ['type_id', 'status_id', 'active'];
        $scenarios[self::SCENARIO_CHANGE_STATUS] = ['id', 'type_id', 'status_id'];

        return $scenarios;
    }

    /**
     * @param null $id
     *
     * @return array|bool|false
     * @throws Exception
     */
    public function getOne($id = null)
    {
        if (isset($id)) {
            return Yii::$app->notify->connection
                ->createCommand(
                    'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[id]] = :id limit 1',
                    [':id' => $id]
                )
                ->queryOne();
        }

        return false;
    }

    /**
     * Получение списка названий полей модели
     *
     * @return array список полей
     */
    protected function getFieldsNames(): array
    {
        return [
            "id",
            "code",
            "name",
            "user_id_from",
            "user_id_to",
            "order_id",
            "message",
            "type_id",
            "status_id",
            "sys_date",
            "created_at",
            "updated_at",
            "active",
            "sort",
        ];
    }

    /**
     * Получение названия таблицы модели в БД.
     * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
     * <code>
     * return "{{auto_brands}}";
     * </code>
     *
     * @return string название таблицы
     */
    protected function getTableName(): string
    {
        return "message";
    }

    /**
     * @throws Exception
     * @throws \Exception
     * @return int
     */
    public function create()
    {
        $db         = Yii::$app->notify->connection;
        $this->code = $this->generateCode();
        $cmd        = $db->createCommand()->insert('message', $this->getPrepareAttributes());
        if (!$cmd->execute()) {
            throw new \Exception("Ошибка записи");
        }
        $mid = $db->getLastInsertID();
        $tr  = Yii::$app->db->beginTransaction();
        $tr->begin();
        try {
            if ($cabinets = $this->cabinets) {
                $mcf = MessageCabinetsFilter::instance();
                foreach ($this->cabinets as $cabinet) {
                    $mcf->message_id = $mid;
                    $mcf->cabinet_id = $cabinet;
                    $mcf->create();
                }
            }
            if ($this->parentId) {
                $mm = new MessageMessage(
                    [
                        "parent_message_id" => $this->parentId,
                        "child_message_id"  => $mid,
                    ]
                );
                if ($mm->validate()) {
                    $mm->create();
                }
            }
            if ($this->childId) {
                $mm = new MessageMessage(
                    [
                        "parent_message_id" => $mid,
                        "child_message_id"  => $this->childId,
                    ]
                );
                if ($mm->validate()) {
                    $mm->create();
                }
            }
            if (!empty($this->order_id) && empty($this->parentId) && empty($this->childId)) {
                (new MessageReceiver())->addByMessageType($this->type_id, $mid, $this->order_id);
            }
            $tr->commit();
            $tr->commit();
        } catch (\Exception $e) {
            $tr->rollBack();
            $tr->rollBack();
        }

        return $mid;
    }

    public function generateCode()
    {
        return md5($this->type_id.$this->status_id.$this->id.microtime());
    }

    /**
     * Выозвращает только те атрибуты, значение которых не равно NULL
     *
     * @return array
     */
    public function getPrepareAttributes()
    {
        $attr = [];
        foreach ($this->getAttributes() as $key => $value) {
            if (isset($value)) {
                $attr[$key] = $value;
            }
        }

        return $attr;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function setStatus()
    {
        return Yii::$app->notify->connection->createCommand()->update(
            'message',
            ["status_id" => $this->status_id],
            ["id" => $this->id]
        )->execute();
    }

    /**
     * Метод получения списка сообщений (к заявке)
     *
     * @param null        $RID      - RequestID
     * @param array       $statuses - массив ID статусов сообщений, разрешенных к выбору
     * @param array       $types    - список разрешенных типов
     * @param array|false $exclude  - массив ID ообщений, для исключения в выборе
     *
     * @return array|false
     * @throws Exception
     */
    public function getByRequestID($RID = null, $statuses = [], $types = [], $exclude = [])
    {
        $types = ($types
            ? (is_array($types) ? $types : [$types])
            : MessageType::instance()->publicTypes);
        /**
         * в массив добавляются типы сообщений, которые не надо выводить в истории и HistoryController.php line 115
         */
        foreach (Yii::$app->params['core']['notification']['exclude_types'] as $key => $value) {
            if (\in_array($key, $types)) {
                unset($types[(array_flip($types))[$key]]);
            }
        }
        if (!\is_array($exclude) && \is_int($exclude)) {
            $exclude = [$exclude];
        }
        if ($iCanSignature = array_search(MessageType::instance()->cansignature, $types)) {
            unset($types[$iCanSignature]);
        }
        if ($iReFirstPack = array_search(MessageType::instance()->refirstpack, $types)) {
            unset($types[$iReFirstPack]);
        }
        $types = implode(",", $types);
        if (!\is_array($exclude) && \is_int($exclude)) {
            $exclude = [$exclude];
        }
        $exclude = array_unique($exclude);
        if (!empty($exclude)) {
            $exclude = " AND `id` NOT IN (".implode(", ", $exclude).") ";
        } else {
            $exclude = '';
        }
        if (empty($statuses)) {
            $rs = Yii::$app->notify->connection
                ->createCommand(
                    'SELECT [['.implode("]],[[", self::getFieldsNames()).']]
 FROM '.self::getTableName().'
 WHERE [[order_id]] = :order_id AND [[type_id]] IN ('.$types.') '.$exclude.' ORDER BY [[created_at]] DESC',
                    [
                        'order_id' => $RID,
                    ]
                );
        } else {
            $statuses = implode(", ", $statuses);
            $rs       = Yii::$app->notify->connection
                ->createCommand(
                    'SELECT [['.implode("]],[[", self::getFieldsNames()).']] 
                    FROM '.self::getTableName().
                    ' WHERE [[order_id]] = :order_id 
                    AND [[status_id]] IN ('.$statuses.') 
                    AND [[type_id]] IN ('.$types.') '.$exclude.
                    ' ORDER BY [[created_at]] DESC',
                    [
                        'order_id' => $RID,
                    ]
                );
        }

        return $rs->queryAll();
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->_parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id): void
    {
        $this->_parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getChildId()
    {
        return $this->_child_id;
    }

    /**
     * @param mixed $child_id
     */
    public function setChildId($child_id): void
    {
        $this->_child_id = $child_id;
    }

    /**
     * Метод обновления уведомления
     *
     * @param null  $id
     * @param array $fields
     *
     * @return bool
     * @throws \RuntimeException
     */
    public function update($id = null, $fields = [])
    {
        try {
            if ($id !== null) {
                $db = Yii::$app->notify->connection;

                return false !== $db->createCommand()->update($this->getTableName(), $fields, ['id' => $id])->execute();
            }

            return false;
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка обновления уведомления в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения данных по идентификатору уведомления
     *
     * @param string $id     - идентификатор уведомления
     * @param array  $fields - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \RuntimeException
     */
    public function getById($id, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    ' WHERE [[id]] =:id',
                    [':id' => $id]
                )->queryOne();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения уведомления по коду из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения данных по коду
     *
     * @param string $code   - код уведомления
     * @param array  $fields - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \RuntimeException
     */
    public function getByCode(string $code, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    ' WHERE [[code]] =:code',
                    [':code' => $code]
                )->queryOne();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения уведомления по коду из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения связей идентификаторв файлов с кодами сообщений
     *
     * @param array $arIdFiles - массив идентификаторов файлов
     * @param bool  $flip      - флаг меняет местами ключи со значениями
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \RuntimeException
     */
    public function getRelationFileIdWithCode(array $arIdFiles, bool $flip = false)
    {
        try {
            $strFileId = implode(',', $arIdFiles);
            $result    = Yii::$app->db
                ->createCommand(
                    'SELECT [[m]].[[code]], [[mat]].[[file_id]] 
                FROM '.$this->getTableName().' AS [[m]]
                JOIN {{message_attache_file}} AS [[mat]] ON [[m]].[[id]] = [[mat]].[[message_id]]
                WHERE [[mat]].[[file_id]] IN ('.$strFileId.')'
                )->queryAll();
            if (!empty($result)) {
                $result = array_column($result, 'code', 'file_id');
                empty($flip) ?: $result = array_flip($result);
            }

            return $result;
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения связей кодов уведомления с файлами из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения ответного(-ых) уведомления на родительское
     *
     * @param       $idOrCode - id или код уведомления
     * @param array $fields   - поля для выборки
     * @param bool  $queryOne - способ выборки
     *
     * @return mixed - вернет результирующий массив
     * @throws \Exception
     */
    public function getChildByParent($idOrCode, array $fields = [], bool $queryOne = true)
    {
        try {
            $fields    = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields    = '[['.implode("]],[[", $fields).']]';
            $method    = empty($queryOne) ? 'queryAll' : 'queryOne';
            $condition =
                '[[id]] IN (SELECT [[child_message_id]] FROM {{message_message}} WHERE [[parent_message_id]] = ';
            $condition .= strlen($idOrCode) == 32
                ? ' (SELECT [[id]]) FROM '.$this->getTableName().' WHERE [[code]] =:code )'
                : ' :parentMessageId';
            $condition .= ')';
            $param     = strlen($idOrCode) == 32
                ? [':code' => $idOrCode]
                : [':parentMessageId' => $idOrCode];

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.' FROM '.$this->getTableName().' WHERE '.$condition,
                    $param
                )->{$method}();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения ответов на уведомление из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения совмещенных данных уведомления и заявки
     *
     * @param       $idOrCode           - id или код уведомления
     * @param array $fieldsMsg          - поля уведомления для выборки, пример
     *                                  [
     *                                  'id',
     *                                  'msg_code'=>'code' //ключ массива это псевдонимом значения(code as msg_code)
     *                                  ]
     * @param array $fieldsRequest      - поля заявки для выборки, пример соответствует примеру $fieldsMsg
     *
     * @return array - верент результирущий массив
     * @throws \RuntimeException
     */
    public function getJointMessageRequest($idOrCode, array $fieldsMsg = [], array $fieldsRequest = [])
    {
        try {
            $prefixM      = 'm';
            $prefixR      = 'r';
            $strFieldsM   = !empty($fieldsMsg)
                ? DBHelper::getPrepareFieldsForJoin($fieldsMsg, $prefixM)
                : DBHelper::getPrepareFieldsForJoin($this->getFieldsNames(), $prefixM, true);
            $strFieldsR   = !empty($fieldsRequest)
                ? DBHelper::getPrepareFieldsForJoin($fieldsRequest, $prefixR)
                : DBHelper::getPrepareFieldsForJoin(Requests::instance()->getFieldsNames(), $prefixR, true);
            $selectFields = $strFieldsM.','.$strFieldsR;
            $condition    = '[['.$prefixM.']].[[id]] = ';
            $condition    .= strlen($idOrCode) == 32
                ? '(SELECT [[id]] FROM '.$this->getTableName().' WHERE [[code]] =:code)'
                : ':id';
            $param        = strlen($idOrCode) == 32
                ? [':code' => $idOrCode]
                : [':id' => $idOrCode];

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$selectFields.'
                    FROM '.$this->getTableName().' AS [['.$prefixM.']]
                    JOIN {{requests}} AS [['.$prefixR.']] ON [['.$prefixM.']].[[order_id]] = [['.$prefixR.']].[[id]]
                    WHERE '.$condition,
                    $param
                )
                ->queryOne();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения уведомления и завяки из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения совмещенных данных уведомления и файлов
     *
     * @param       $idOrCode           - id или код уведомления
     * @param array $fieldsMsg          - поля уведомления для выборки, пример
     *                                  [
     *                                  'id',
     *                                  'msg_code'=>'code' //ключ массива это псевдонимом значения(code as msg_code)
     *                                  ]
     * @param array $fieldsFile         - поля файлов для выборки, пример соответствует примеру $fieldsMsg
     *
     * @return array - верент результирущий массив
     * @throws \RuntimeException
     */
    public function getJointMessageFile($idOrCode, array $fieldsMsg = [], array $fieldsFile = [])
    {
        try {
            $prefixM      = 'm';
            $prefixF      = 'f';
            $strFieldsM   = !empty($fieldsMsg)
                ? DBHelper::getPrepareFieldsForJoin($fieldsMsg, $prefixM)
                : DBHelper::getPrepareFieldsForJoin($this->getFieldsNames(), $prefixM, true);
            $strFieldsF   = !empty($fieldsFile)
                ? DBHelper::getPrepareFieldsForJoin($fieldsFile, $prefixF)
                : DBHelper::getPrepareFieldsForJoin(File::instance()->getFieldsNames(), $prefixF, true);
            $selectFields = $strFieldsM.','.$strFieldsF;
            $condition    = '[['.$prefixM.']].[[id]] = ';
            $condition    .= strlen($idOrCode) == 32
                ? '(SELECT [[id]] FROM '.$this->getTableName().' WHERE [[code]] =:code)'
                : ':id';
            $param        = strlen($idOrCode) == 32
                ? [':code' => $idOrCode]
                : [':id' => $idOrCode];

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$selectFields.'
                    FROM '.$this->getTableName().' AS [['.$prefixM.']]
                    JOIN {{message_attache_file}} AS [[maf]] ON [[maf]].[[message_id]] = [['.$prefixM.']].[[id]]
                    JOIN {{file}} AS [['.$prefixF.']] ON [['.$prefixF.']].[[id]] = [[maf]].[[file_id]]
                    WHERE '.$condition,
                    $param
                )
                ->queryAll();
        } catch (Exception $e) {
            throw new \RuntimeException(
                'Ошибка получения уведомления и файлов из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод оптравки ответного уведомления на почту для верификаторов
     *
     * @param string $msg               - ответное сообщение на родительское уведомление
     * @param        $idOrCode          - id или код уведомления
     * @param array  $listFilePath      - список путей файлов, пример:
     *                                  [
     *                                  'path/messages/128/CsG6MSpWAAAdTob.jpg',
     *                                  'path/messages/46/test.jpg',
     *                                  ]
     *
     * @return bool - вернет истину если отправка прошла успешно
     * @throws \Exception
     */
    public function sendMailVer(string $msg, $idOrCode, array $listFilePath = [])
    {
        $data = $this->getJointMessageRequest(
            $idOrCode,
            [
                'm_created_at' => 'created_at',
                'user_id_from',
                'message',
            ],
            [
                'r_created_at' => 'created_at',
                'num',
                'user_id',
                'point_id',
                'client_first_name',
                'client_last_name',
                'client_patronymic',
            ]
        );
        if (empty($data)) {
            throw new \RuntimeException(
                'Ошибка получения уведомелния, при отправке ответного уведомления верификатору, обратитесь к администартору'
            );
        }
        $userPartner  = '';
        $coWorkerName = 'Служба верификации';
        $modelU       = new Users();
        $arUser       = $modelU->getByListId(
            [$data['user_id_from'], $data['user_id']],
            ['id', 'first_name', 'last_name', 'second_name']
        );
        if (!empty($arUser)) {
            foreach ($arUser as $user) {
                $fio = $user['first_name'].' '.$user['last_name'].' '.$user['second_name'];
                $user['id'] != $data['user_id'] ?: $userPartner = $fio;
                $user['id'] != $data['user_id_from'] ?: $coWorkerName = $fio;
            }
        }
        $partnerName = !empty($data['point_id']) && ($partner = (new Partners())->getByPointId($data['point_id']))
            ? $partner['partner_name'] : '';
        $rcMail      = Yii::$app->mailer->compose();
        $rcMail->setFrom(Yii::$app->params['core']['email']['from']);
        $rcMail->setTo(Yii::$app->params['core']['email']['verifications']);
        $rcMail->setSubject('CM-'.$data['num'].' Ответ на обращение верификатора');
        //todo[echmaster]: 21.01.2019 Создать почтовый шаблон и перенести html туда
        $messageTemplate = <<<TXT
Ответ от партнера по заявке CM-{$data['num']}
<br><br>
{$msg}
<hr>
<b>Заявка:</b> CM-{$data['num']}<br>
<b>Партнер:</b> {$userPartner}<br>
<b>Номер партнера:</b> {$partnerName}<br>
<b>Дата/время заведения заявки:</b> {$data['r_created_at']}<br>
<b>ФИО сотрудника ОВ/ОЦ:</b> {$coWorkerName}<br>
<b>Дата отправки запроса:</b> {$data['m_created_at']}<br>
<b>ФИО клиента:</b> {$data['client_first_name']} {$data['client_last_name']} {$data['client_patronymic']} <br>
<b>Содержание запроса:</b> {$data['message']}<br>
TXT;
        $rcMail->setHtmlBody($messageTemplate);
        if (!empty($listFilePath)) {
            $pathWebRoot = rtrim(Yii::getAlias('@webroot'), '/').'/';
            foreach ($listFilePath as $path) {
                $fullPath = strpos($path, $pathWebRoot) === false ? $pathWebRoot.trim($path, '/') : $path;
                $rcMail->attach($fullPath);
            }
        }
        $result = $rcMail->send();
        if (!$result) {
            throw new \RuntimeException('Проблемы с отправкой сообщений на почту');
        }

        return $result;
    }
}