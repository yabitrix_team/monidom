<?php
/**
 * Project: lkp2.carmoney.loc
 * Created by PhpStorm.
 * User: daglob (User)
 * Date: 18.05.2018
 * Time: 14:15
 */

namespace app\modules\notification\models;
/**
 * Trait MessageHelper
 *
 * @property array $list - массив даннык
 *
 * @package app\modules\notification\models
 */
trait MessageHelper {
	/**
	 * М-д возвращает данные по ID
	 *
	 * @param null $id
	 *
	 * @return array|false
	 */
	public function getByID( $id = null ) {

		if ( isset( $id ) && ! empty( $this->list ) ) {
			return false !== ( $index = array_search( $id, array_column( $this->list, "id" ) ) ) ?
				$this->list[ $index ] : false;
		}

		return false;
	}

	/**
	 * Возвращает первый набор данных из массива, если в нем указано значение по умолчанию "Y"(должен быть отмечен в
	 * БД: is_default = Y)
	 *
	 * @return null|int если значение найдено - вернет ID статуса, иначе NULL
	 */
	public function getDefault() {

		return false !== ( $index = array_search( "Y", array_column( $this->list, "is_default" ) ) ) ?
			$this->list[ $index ]['id'] : null;
	}

	/**
	 * Возвращает значение поля $returnField из списка типов, выполняя поиск по значению поля $field
	 *
	 * @param null   $value       - знаяение для поиска
	 * @param string $field       - поле поиска
	 * @param string $returnField - возвращаемое значение
	 *
	 * @return bool
	 */
	private function findByField( $value = null, $field = "name", $returnField = "id" ) {

		if ( isset( $value ) ) {
			return false !== ( $index = array_search(
				trim( $value ),
				array_column( $this->list, $field )
			) )
				? $this->list [ $index ][ $returnField ]
				: false;
		}

		return false;
	}
}