<?php
/**
 * Project: lkp2.carmoney.loc
 * Created by PhpStorm.
 * User: daglob (User)
 * Date: 18.05.2018
 * Time: 16:57
 */

namespace app\modules\notification\models;

use Yii;
use yii\base\Model;

class MessageCabinetsFilter extends Model {
	use MessageHelper;
	const SCENARIO_ADD = 'add';
	public $message_id;
	public $cabinet_id;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {

		return 'message_cabinets_filter';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {

		// todo[daglob]: 11.04.2018 16:17 необходимо выстроить схему валидируемых полей, зависимых от типа сообщения
		return [
			[ [ "message_id", "cabinet_id" ], "filter", "filter" => "trim" ],
			[ [ "message_id", "cabinet_id" ], "integer" ],
			[ [ "message_id", "cabinet_id" ], "required" ],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {

		$scenarios                       = parent::scenarios();
		$scenarios[ self::SCENARIO_ADD ] = [ 'message_id', 'cabinet_id' ];

		return $scenarios;
	}

	/**
	 * @throws \Exception
	 * @return int
	 */
	public function create() {

		$db  = Yii::$app->notify->connection;
		$cmd = $db->createCommand()->upsert( 'message_cabinets_filter', [
			"message_id" => $this->message_id,
			"cabinet_id" => $this->cabinet_id,
		], false );
		if ( false === $cmd->execute() ) {
			throw new \Exception( "Ошибка записи" );
		}

		return $db->getLastInsertID();
	}
}