<?php
namespace app\modules\notification\models;

use Yii;
use yii\base\Model;

/**
 * Класс по работе с типами сообщений
 *
 * @property array     $list                - список типов
 * @property false|int $test                - ID типа ТЕСТ
 * @property false|int $status              - ID типа СТАТУС
 * @property false|int $crm                 - ID типа ЦРМ
 * @property false|int $sys                 - ID типа СИСТЕМНОЕ
 * @property false|int $ver                 - ID типа ВЕРИФИКАТОР
 * @property false|int $req                 - ID типа ЗАЯВКА
 * @property false|int $adv                 - ID типа РЕКЛАМА
 * @property false|int $firstpack           - ID типа ПЕРВЫЙ ПАКЕТ ДОК-В
 * @property false|int $secondpack          - ID типа ВТОРОЙ ПАКЕТ ДОК-В
 * @property false|int $requp               - ID типа ОБНОВЛЕНИЕ ПОЛЕЙ ЗАЯВКИ
 * @property false|int $cardVerifier        - ID типа проверка карты
 * @property mixed     $publicTypes         - список ID типов сообщений, доступных к публикации
 * @property array     $namesPublicTypes    - список NAME типов сообщений, доступных к публикации
 * @property array     $relationPublicTypes - массив связей идентификаторов с именами типов
 * @property false|int $default             - тип по умолчанию
 */
class MessageType extends Model
{
    use MessageHelper;
    /**
     * Константы типов уведомлений
     */
    CONST TEST           = 'test';
    CONST STATUS         = 'status';
    CONST CRM            = 'crm';
    CONST SYS            = 'sys';
    CONST VERIFIER       = 'ver';
    CONST REQUEST        = 'req';
    CONST ADVERTISING    = 'adv';
    CONST FIRST_PACK     = 'firstpack';
    CONST SECOND_PACK    = 'secondpack';
    CONST REQUEST_UPDATE = 'requp';
    CONST LKK_BLOCK      = 'lkkblock';

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getList()
    {
        return Yii::$app->notify->connection->createCommand(
            'SELECT
[[id]],
[[name]],
[[is_default]],
[[public]],
[[active]]
FROM {{message_type}}'
        )
            ->queryAll();
    }

    /**
     * Возвращает ID типа ТЕСТ
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getTest()
    {
        return $this->findByField(self::TEST);
    }

    /**
     * Возвращает ID типа СТАТУС
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getStatus()
    {
        return $this->findByField(self::STATUS);
    }

    /**
     * Возвращает ID типа СТАТУС
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getCrm()
    {
        return $this->findByField(self::CRM);
    }

    /**
     * Возвращает ID типа СТАТУС
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getSys()
    {
        return $this->findByField(self::SYS);
    }

    /**
     * Возвращает ID типа СТАТУС
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getVer()
    {
        return $this->findByField(self::VERIFIER);
    }

    /**
     * Возвращает ID типа ЗАЯВКА
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getReq()
    {
        return $this->findByField(self::REQUEST);
    }

    /**
     * Возвращает ID типа РЕКЛАМА
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getAdv()
    {
        return $this->findByField(self::ADVERTISING);
    }

    /**
     * Возвращает ID типа ПЕРВЫЙ ПАКЕТ ДОК-В
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getFirstpack()
    {
        return $this->findByField(self::FIRST_PACK);
    }

    /**
     * Возвращает ID типа ВТОРОЙ ПАКЕТ ДОК-В
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getSecondpack()
    {
        return $this->findByField(self::SECOND_PACK);
    }

    /**
     * Возвращает ID типа ВТОРОЙ ПАКЕТ ДОК-В
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getRequp()
    {
        return $this->findByField(self::REQUEST_UPDATE);
    }

    /**
     * Возвращает ID типа ВТОРОЙ ПАКЕТ ДОК-В
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getLkkblock()
    {
        return $this->findByField(self::LKK_BLOCK);
    }

    /**
     * Возвращает ID типа ДАННЫЕ ПО ЗАЯВКЕ
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getRequestData() {

        return $this->findByField( 'requestData' );
    }

    /**
     * Возвращает ID типа ПОВТОРНЫЙ ПЕРВЫЙ ПАКЕТ ДОК-В
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getReFirstPack()
    {

        return $this->findByField('refirstpack');
    }

    /**
     * Возвращает ID типа cardVerifier
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getCardVerifier()
    {

        return $this->findByField('cardVerifier');
    }

    /**
     * Возвращает ID типа возможность подписания второго пакета в ЛКВМ
     *
     * @return false|int если значение найдено - вернет ID статуса, иначе NULL
     */
    public function getCansignature()
    {

        return $this->findByField('cansignature');
    }

    /**
     * Список типов, доступных к отправке
     *
     * @return array
     */
    public function getPublicTypes()
    {
        return array_column(
            array_filter(
                $this->list,
                function ($value) {
                    return $value['public'] == 1;
                }
            ),
            'id'
        );
    }

    /**
     * Список имен(кодов) типов, доступных к отправке
     *
     * @return array
     */
    public function getNamesPublicTypes()
    {
        return array_column(
            array_filter(
                $this->list,
                function ($value) {
                    return $value['public'] == 1;
                }
            ),
            'name'
        );
    }

    /**
     * Метод получения массива связей идентификаторов с именами типов,
     * доступных к отправке
     *
     * @param bool $flip - флаг, отвечает за смену ключей с их значениями в массиве
     *
     * @return array|null
     */
    public function getRelationPublicTypes($flip = false)
    {
        $result = array_column(
            array_filter(
                $this->list,
                function ($value) {
                    return $value['public'] == 1;
                }
            ),
            'name',
            'id'
        );

        return empty($flip) ? $result : array_flip($result);
    }
}