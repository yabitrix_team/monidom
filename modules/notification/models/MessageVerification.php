<?php
namespace app\modules\notification\models;

use app\modules\app\v1\models\Users;
use Yii;
use yii\base\Model;

class MessageVerification extends Model
{
    /**
     * @param null $uid
     *
     * @return array|bool
     * @throws \yii\db\Exception
     */
    public function getByUserID($uid = null)
    {
        if ($uid !== null) {
            $type = MessageType::instance()->ver;
            if (Users::instance()->build($uid)->isClient() || Users::instance()->build($uid)->isMobileUser()) {
                $cmd = Yii::$app->db->createCommand(
                    'select
  m.id,
  m.type_id,
  m.message,
  m.created_at,
  r.code                                      as request_code,
  r.num                                       as request_num,
  if(mm.child_message_id && m2.active , 1, 0) as have_child,
  m2.id                                       as child_id,
  m2.type_id                                  as child_type_id,
  m2.message                                  as child_message,
  m2.created_at                               as child_created_at,
  m2.user_id_from                             as child_user_id_from,
  f.path                                      as file_path
from message as m
  left join requests r on r.id = m.order_id
  left join message_message mm on m.id = mm.parent_message_id
  left join message m2 on m2.id = mm.child_message_id
  left join users u on u.id = m2.user_id_from
  left join message_attache_file maf on maf.message_id = m2.id
  left join file f on f.id = maf.file_id
where m.order_id in
      (select id
       from requests
       where client_id = :uid)
      and m.type_id = :type
      and m.active = 1
order by m.created_at desc, m2.created_at desc;',
                    [
                        ':uid'  => $uid,
                        ':type' => $type,
                    ]
                );
            } else {
                $cmd = Yii::$app->db->createCommand(
                    'select
  m.id,
  m.type_id,
  m.message,
  m.created_at,
  r.code                                      as request_code,
  r.num                                       as request_num,
  if(mm.child_message_id && m2.active , 1, 0) as have_child,
  m2.id                                       as child_id,
  m2.type_id                                  as child_type_id,
  m2.message                                  as child_message,
  m2.created_at                               as child_created_at,
  m2.user_id_from                             as child_user_id_from,
  f.path                                      as file_path
from message as m
  left join requests r on r.id = m.order_id
  left join message_message mm on m.id = mm.parent_message_id
  left join message m2 on m2.id = mm.child_message_id
  left join users u on u.id = m2.user_id_from
  left join message_attache_file maf on maf.message_id = m2.id
  left join file f on f.id = maf.file_id
where m.order_id in
      (select id
       from requests
       where point_id IN
             (select users.point_id
              from users
              where users.id = :uid))
      and m.type_id = :type
      and m.active = 1
order by m.created_at desc, m2.created_at desc;',
                    [
                        ':uid'  => $uid,
                        ':type' => $type,
                    ]
                );
            }
            $lister = [];
            if ($all = $cmd->queryAll()) {
                array_walk(
                    $all,
                    function ($val) use (&$lister) {
                        if (!isset($lister[$val['id']])) {
                            $lister[$val['id']] = [
                                'id'           => $val['id'],
                                'type_id'      => $val['type_id'],
                                'message'      => $val['message'],
                                'request_code' => $val['request_code'],
                                'request_num'  => $val['request_num'],
                                'created_at'   => $val['created_at'],
                            ];
                        }
                        if (0 != (int) $val['have_child']) {
                            if (!isset($lister[$val['id']]['childs'][(string) $val['child_id']])) {
                                $lister[$val['id']]['childs'][(string) $val['child_id']] = [
                                    'id'           => $val['child_id'],
                                    'type_id'      => $val['child_type_id'],
                                    'message'      => $val['child_message'],
                                    'created_at'   => $val['child_created_at'],
                                    'user_id_from' => $val['child_user_id_from'],
                                    'file_link'    => [],
                                ];
                            }
                            if (!empty($val['file_path'])) {
                                $lister[$val['id']]['childs'][(string) $val['child_id']]['file_link'][] =
                                    $val['file_path'];
                            }
                            unset($lister[$val['child_id']]);
                        }
                    }
                );
            }

            return $lister;
        }

        return false;
    }
}