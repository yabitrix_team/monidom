<?php
/**
 * Created by PhpStorm.
 * User: daglob
 * Date: 12.04.2018
 * Time: 15:29
 */

namespace app\modules\notification\components;

use app\modules\notification\classes\WSMessageFilter;
use app\modules\notification\classes\WSSessionRegistration;
use app\modules\notification\classes\WSTicketRegistration;
use app\modules\notification\commands\constructors\MessageConstructor;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageStatus;
use app\modules\notification\models\MessageType;
use Redis;
use yii\base\Component;

/**
 * Class NotificationComponent
 *
 * @package app\modules\notification\components
 *
 * @property string                                                  $hashKey
 * @property \app\modules\notification\classes\WSTicketRegistration  $TRS
 * @property \app\modules\notification\classes\WSSessionRegistration $SRS
 * @property \app\modules\notification\classes\WSMessageFilter       $MFS
 * @property \app\modules\notification\classes\WSSessionRegistration $sessionRegistrationService
 * @property \app\modules\notification\classes\WSTicketRegistration  $ticketRegistrationService
 * @property \yii\db\Connection                                      $connection
 * @property \app\modules\notification\classes\WSMessageFilter       $messageFilterService
 */
class NotificationComponent extends Component {
	/** @var array */
	public $param;
	/** @var string */
	public $hash_key = "user_keys";
	/** @var int */
	public $ttl = 86400;
	/** @var  Redis */
	protected $redis;
	/** @var \yii\db\Connection */
	protected $_connection;

	/**
	 * @return \yii\db\Connection
	 * @throws \yii\db\Exception
	 */
	public function getConnection(): \yii\db\Connection {

		try {
			$this->_connection->createCommand( "DO 1;" )->execute();
		} catch ( \yii\db\Exception $e ) {
			$this->_connection->close();
			$this->_connection->open();
		}

		return $this->_connection;
	}

	public function setConnection( \yii\db\Connection $connection ) {

		$this->_connection = $connection;
	}

	/**
	 * Инициализация компонента RedisQueueSenderComponent
	 *
	 * @throws \Exception
	 */
	public function init() {

		parent::init();
		if ( ! class_exists( "Redis" ) ) {
			throw new \Exception( "Отсутствует класс Redis (php extension=redis.so(.ini))" );
		}
		if ( ! class_exists( "swoole_server" ) ) {
			throw new \Exception( "Отсутствует модуль Swoole (php extension=Swoole.so(.ini))" );
		}
	}

	/**
	 * Добавление сообщение в БД, с возможностью регистрации к отправке
	 *
	 * @param array $message
	 *
	 * @param bool  $register
	 *
	 * @return int
	 */
	public function addMessage( $message = [], $register = true ) {

		try {
			// todo[daglob]: 12.04.2018 16:49 необходимо проработать все сценарии добавления уведомления
			$message['scenario'] = Message::SCENARIO_ADD;
			$rsMessage           = new Message( $message );
			if ( ! $rsMessage->validate() ) {
				throw new \Exception( print_r( $rsMessage->errors, true ) );
			}
			if ( ! ( $id = $rsMessage->create() ) ) {
				throw new \Exception( print_r( $rsMessage->errors, true ) );
			}
			if ( $register ) {
				$this->publish( array_merge( $rsMessage->getAttributes(), [ "id" => $id ] ), \Yii::$app->notify->param['channels'] );
			}

			return $id;
		} catch ( \Exception $e ) {
			die( $e->getMessage() );
		}
	}

	/**
	 * Метод отправки сообщения напрямую в очередь, без регистрации
	 *
	 * @param null  $message
	 * @param array $channels
	 * @param bool  $additional
	 *
	 * @throws \Exception
	 */
	public function publish( $message = null, $channels = [], $additional = false ) {

		if ( isset( $message ) ) {
			if ( is_array( $message ) ) {

				$message = json_encode( $message, JSON_FORCE_OBJECT );
			} elseif ( ! $this->checkIsJSON( $message ) ) {
				throw new \Exception( "Переменная \$message не является массивом или строкой в формате JSON" );
			}
			$channels = is_array( $channels ) ? $channels : [ $channels ];
			if ( $additional ) {
				$conf_channels = ! empty( $this->param['channels'] ) ?
					( is_array( $this->param['channels'] ) ? $this->param['channels'] : [ $this->param['channels'] ] ) :
					[];
				$channels      = array_merge( $conf_channels, $channels );
			}
			if ( ! empty( $channels ) ) {
				foreach ( $channels as $channel ) {
					$this->getSRS()
					     ->getRedis()
					     ->PUBLISH( $channel, $message );
				}
			} else {
				throw new \Exception( 'Отсутствуют каналы для отправки сообщений' );
			}
		}
	}

	/**
	 * Метод проверяет валидность строки в формате JSON
	 *
	 * @param null $string
	 *
	 * @return bool
	 */
	private function checkIsJSON( $string = null ) {

		if ( isset( $string ) ) {
			json_decode( $string );

			return json_last_error() === JSON_ERROR_NONE;
		}

		return false;
	}

	/**
	 * @see getTicketRegistrationService
	 */
	public function getSRS() {

		return $this->getSessionRegistrationService();
	}

	/**
	 * М-д возвращает экземпляр WSSessionRegistration
	 *
	 * @return WSSessionRegistration
	 */
	public function getSessionRegistrationService() {

		return WSSessionRegistration::getInstance( $this->hash_key, [
			"host" => $this->param['ip'],
			"port" => $this->param['port'],
		] );
	}

	/**
	 * Получение данных по ИД сообщения
	 *
	 * @param null $MID
	 *
	 * @return array|bool|false
	 * @throws \yii\db\Exception
	 */
	public function getMessageData( $MID = null ) {

		if ( isset( $MID ) ) {
			$message = ( new Message() )->getOne( $MID );

			return $message;
		}

		return false;
	}

	/**
	 * @param null   $MID
	 * @param string $setStatus
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function readMessage( $MID = null, $setStatus = "read" ) {

		if ( isset( $MID ) ) {

			$statusList = ( new MessageStatus() )->list;
			if ( $index = array_search( $setStatus, array_column( $statusList, 'name', 'id' ) ) ) {
				if ( 0 < ( new Message( [
					                        "id"        => $MID,
					                        "status_id" => $index,
				                        ] ) )->update() ) {
					return true;
				}
				throw new \Exception( "Не найдена заявка с ID: $MID" );
			};
			throw new \Exception( "Статус '{$setStatus}' не обнаружен в списке допустимых статусов: \n" . print_r( array_column( $statusList, 'name' ), true ) );
		}

		return false;
	}

	/**
	 * @param $code
	 *
	 * @return array|bool
	 * @throws \RedisException
	 */
	public function checkTicketByCode( $code ) {

		return $this->getTRS()->checkTicketByCode( $code );
	}

	/**
	 * @see getTicketRegistrationService
	 */
	public function getTRS() {

		return $this->getTicketRegistrationService();
	}

	/**
	 * М-д возвращает экземпляр WSTicketRegistration
	 *
	 * @return WSTicketRegistration
	 */
	public function getTicketRegistrationService() {

		return WSTicketRegistration::getInstance( $this->hash_key, [
			"hash_key" => $this->hash_key,
			"ttl"      => $this->ttl,
			"host"     => $this->param['ip'],
			"port"     => $this->param['port'],
		] );
	}

	/**
	 * @param null $uid
	 *
	 * @return array|bool
	 * @throws \Exception
	 */
	public function createTicket( $uid = null ) {

		return $this->getTRS()->createTicket( $uid );
	}

	/**
	 * @return mixed
	 * @throws \RedisException
	 */
	public function resetTicketService() {

		return $this->getTRS()->resetTicketService();
	}

	/**
	 * @param null $uid
	 * @param null $id
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function setFilterMessage( $uid = null, $id = null ) {

		return $this->getMFS()->setFilterMessage( $uid, $id );
	}

	/**
	 * @see getMessageFilterService
	 */
	public function getMFS() {

		return $this->getMessageFilterService();
	}

	/**
	 * М-д возвращает экземпляр WSMessageFilter
	 *
	 * @return WSMessageFilter
	 */
	public function getMessageFilterService() {

		return WSMessageFilter::getInstance( $this->hash_key, [
			"host" => $this->param['ip'],
			"port" => $this->param['port'],
		] );
	}

	/**
	 * @param $uid
	 *
	 * @return array|false
	 * @throws \RedisException
	 */
	public function getFilterMessageByUID( $uid ) {

		return $this->getMFS()->getFilterMessageByUID( $uid );
	}

	/**
	 * @param $uid
	 *
	 * @return bool|mixed
	 * @throws \RedisException
	 */
	public function getFilterRequestByUID( $uid ) {

		return $this->getMFS()->getFilterRequestByUID( $uid );
	}

	/**
	 * @param null $uid
	 * @param null $id
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function setFilterByRequest( $uid = null, $id = null ) {

		return $this->getMFS()->setFilterRequest( $uid, $id );
	}

	/**
	 *
	 */
	public function filteredMessage() {

	}

	/**
	 * @return string
	 */
	public function getHashKey(): string {

		return $this->hash_key;
	}

	/**
	 * @param null $mid
	 *
	 * @return MessageConstructor|bool
	 * @throws \yii\db\Exception
	 * @throws \Exception
	 */
	public function buildMessageByID( $mid = null ) {

		if ( isset( $mid ) ) {
			if ( $messageData = Message::instance()->getOne( $mid ) ) {
				return $this->buildMessage(
					$messageData['type_id'], $messageData );
			};
		}

		return false;
	}

	/**
	 * @param string $messageType - ID типа сообщения
	 * @param array  $messageData - данные тела сообщения
	 *
	 * @return MessageConstructor|bool
	 * @throws \Exception
	 */
	public function buildMessage( $messageType = null, array $messageData = [] ) {

		if ( $messageType === null ) {
			$messageType = MessageType::instance()->default;
		} elseif ( ! in_array( $messageType, MessageType::instance()->publicTypes ) ) {
			throw new \Exception( 'Указанный тип сообщения не зарегестрирован в системе' );
		}
		$type         = MessageType::instance()->getByID( $messageType );
		$messageClass = 'app\modules\notification\commands\constructors\\' . "{$type['name']}Constructor";
		if ( ! class_exists( $messageClass ) ) {
			throw new \Exception( "Класс-конструктор для типа сообщения '{$messageType}' не найден" );
		}

		return new $messageClass( $messageData );
	}
}