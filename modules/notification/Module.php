<?php

namespace app\modules\notification;

use yii\base\BootstrapInterface;
use yii\console\Application;

/**
 * notification module definition class
 *
 * @property \yii\db\Connection $connection
 */
class Module extends \yii\base\Module implements BootstrapInterface {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\notification\controllers';
	public $swoole_params       = [
		'ip'   => '0.0.0.0',
		'port' => '9099',
	];
	public $redis_params        = [
		"ip"      => "127.0.0.1",
		"port"    => "6379",
		"channels" => [ "ch_notify_default" ],
	];
	public $tickets             = [
		"name" => "user_keys_notify_default",
		// в секундах, время жизни ключа
		"ttl"  => 86400,
	];

	public function getConnection() {

		if ( \Yii::$app instanceof Application ) {
			return \Yii::$app->pdb;
		}

		return \Yii::$app->db;
	}

	public function init() {

		\Yii::$app->setComponents( [
			                           "notify" => [
				                           'class'      => 'app\modules\notification\components\NotificationComponent',
				                           "param"      => $this->redis_params,
				                           "hash_key"   => $this->tickets['name'],
				                           "ttl"        => $this->tickets['ttl'],
				                           "connection" => $this->connection,
			                           ],
		                           ] );
		parent::init();
	}

	public function bootstrap( $app ) {

		if ( $app instanceof Application ) {
			$this->controllerMap['run']  = [
				"class"         => 'app\modules\notification\commands\RunController',
				"swoole_params" => [
					"ip"   => $this->swoole_params['ip'],
					"port" => $this->swoole_params['port'],
				],
			];
			$this->controllerMap['test'] = [
				"class" => 'app\modules\notification\commands\TestController',
			];
		}
	}
}
