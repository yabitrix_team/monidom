<?php

namespace app\modules\notification\controllers;

use app\modules\app\v1\models\Statuses;
use yii\rest\Controller;
use Yii;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\Requests;
use app\modules\notification\commands\constructors\reqConstructor;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageType;
use app\modules\app\v1\classes\traits\TraitConfigController;

class NotificationController extends Controller {
	use TraitConfigController;

	/**
	 * @throws \RedisException
	 * @throws \yii\db\Exception
	 * @throws \Exception
	 */
	public function actionIndex() {

		$idUser = Yii::$app->user->getId();
		///////////////////////////////////////////////////////////////////////////////
		/// Build messages list
		if ( ( $user = Users::instance()->build( $idUser ) ) && $user->hasGroup() ) {

			$NOT_IN = ( $filteredRequests = Yii::$app->notify->getFilterRequestByUID( $user->id ) ) ?
				implode( ', ', $filteredRequests ) : "''";
			// todo[daglob]: 09.05.2018 16:21 в sql запросах закомментированы списки статусы ДОПУСТИМЫХ к выбору по ним заявок для формирования списка уведомлений. Стоит вынести в параметры и/или привести к единому виду, т.к. ИД статусов могут меняться.
			if ( $user->isClient() || $user->isMobileUser() ) {
				$filteredTypes = implode( ", ", [ MessageType::instance()->sys ] ) ?: '';
				$rs            = Yii::$app->db->createCommand( "select
id              as id,
type_id         as tid,
status_id       as sid,
order_id        as rid,
max(created_at) as cr
from message
where order_id in (
select id
from requests
where client_id = :uid
and id not in ($NOT_IN)
)
and type_id not in ($filteredTypes)
group by order_id
order by cr", [
					':uid' => $user->id,
				] );
			}
			if ( $user->isPartnerUser() || $user->isAgent() ) {
				$rs = Yii::$app->db->createCommand( "select
id              as id,
type_id         as tid,
status_id       as sid,
order_id        as rid,
max(created_at) as cr
from message
where order_id in (
select id
from requests
where (user_id = :uid OR point_id = :pid)
AND id NOT IN ($NOT_IN)
)
group by order_id
order by cr", [
					':uid' => $user->id,
					':pid' => $user->point_id,
				] );
			}
			if ( $rs && $requests = $rs->queryAll() ) {
				/**
				 * Производим маппинг для сообщений в пуле для партнера
				 *
				 * @param $value
				 *
				 * @return mixed
				 */
				$partnerCallbackAlertsMapping = function( $value ) {

					if ( (int) $value['type_id'] === (int) MessageType::instance()->ver ) {
						$value['message'] = 'Запрос данных: ' . $value['message'];
					};

					return $value;
				};
				/**
				 * Ф-я позволяет отфильтровать по типу выводимые сообщения в пуле
				 *
				 * @param $value
				 *
				 * @return bool
				 */
				$clientCallbackAlertsFilter = function( $value ) {

					return ! \in_array( (int) $value['type_id'], [
						(int) MessageType::instance()->sys,
						(int) MessageType::instance()->test,
						(int) MessageType::instance()->adv,
                        (int) MessageType::instance()->lkkblock,
					], true );
				};
                $partnerCallbackAlertsFilter = function( $value ) {

                    return ! \in_array( (int) $value['type_id'], [
                        (int) MessageType::instance()->lkkblock,
                    ], true );
                };
				/**
				 * Производим маппинг для сообщений в пуле для клиента
				 *
				 * @param $value
				 *
				 * @return mixed
				 */
				$clientCallbackAlertsMapping = function( $value ) {

					if ( (int) $value['type_id'] === (int) MessageType::instance()->requp ) {
						$value['message'] = null;
					}
					if ( (int) $value['type_id'] === (int) MessageType::instance()->ver ) {
						$value['message'] = 'Запрос данных: ' . $value['message'];
					}

					return $value;
				};
				$messages = [];
				foreach ( $requests as $req ) {
					$messageData = [
						'order_id'   => $req['rid'],
						'user_id_to' => $user->id,
					];
					/** @var reqConstructor $rsMessage */
					$rsMessage = Yii::$app->notify->buildMessage( MessageType::instance()->req, $messageData );
					$rsMessage->getTargets();
					/**
					 * фильтруем сообщения с учетом "закрытыъ" данным пользователем
					 */
					if ( $filteredMessage = Yii::$app->notify->getFilterMessageByUID( $user->id ) ) {
						$rsMessage->setCallbackAlertsFilter( function( $value ) use ( $filteredMessage ) {

							return ! \in_array( $value['id'], $filteredMessage );
						}, $rsMessage::USE_FILTER );
					};
					/**
					 * настраиваем обработчики для сообщения для клиента
					 */
					if ( $user->isClient() || $user->isMobileUser() ) {
						$rsMessage->setCallbackAlertsFilter( $clientCallbackAlertsFilter, $rsMessage::USE_FILTER );
						$rsMessage->setCallbackAlertsFilter( $clientCallbackAlertsMapping, $rsMessage::USE_MAP );
					}
					/**
					 * настраиваем обработчики для сообщения для партнера
					 */
					if ( $user->isPartnerUser() || $user->isAgent()) {
                        $rsMessage->setCallbackAlertsFilter( $partnerCallbackAlertsFilter, $rsMessage::USE_FILTER );
					}
                    if ( $user->isPartnerUser()) {
                        $rsMessage->setCallbackAlertsFilter( $partnerCallbackAlertsMapping, $rsMessage::USE_MAP );
                    }

					$messages[] = $rsMessage->getMessage();
				}

                $data = ( new Statuses() )->getAll();
                $statuses = [];
                foreach($data as $status) {
                    $statuses[$status['id']] = $status;
                };

				return $this->getClassAppResponse()::get( ["messages" => $messages, "statuses" => $statuses] );
			}

			return $this->getClassAppResponse()::get( ["messages" => [], "statuses" => []] );
		}
		throw new \Exception( "Подключенный пользователь id={$idUser} не имеет групп" );
	}
}