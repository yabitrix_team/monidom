<?php
/**
 * Project: lkp2.carmoney.loc
 * Created by PhpStorm.
 * User: daglob (User)
 * Date: 27.04.2018
 * Time: 16:18
 */

namespace app\modules\notification\classes;
/**
 * Class WSMessageFilter
 *
 * @package app\modules\notification\classes
 */
class WSMessageFilter extends WSSessionRegistration {
	/**
	 * @var self
	 */
	protected static $instance;

	/**
	 * @param null $uid
	 * @param null $id
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function setFilterMessage( $uid = null, $ids = null ): bool {

		$ids = is_array($ids) ? $ids : [$ids];
		if ( $this->getRedis()->exists( 'notify_filter' ) &&
		     $this->getRedis()->hExists( 'notify_filter', "messages:{$uid}" ) ) {
			$list = json_decode( $this->getRedis()->hGet( 'notify_filter', "messages:{$uid}" ), true );
			foreach ($ids as $id) {
				if ( ! \in_array( $id, $list ) ) {
					$list[] = $id;
				}
			}
		} else {
			$list = $ids;
		}

		return false !== $this->getRedis()
		                      ->hSet( 'notify_filter', "messages:{$uid}", json_encode( $list, JSON_FORCE_OBJECT ) );
	}

	/**
	 * @param null $uid
	 * @param null $id
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function setFilterRequest( $uid = null, $ids = null ): bool {

		$ids = is_array($ids) ? $ids : [$ids];
		if ( $this->getRedis()->exists( 'notify_filter' ) &&
		     $this->getRedis()->hExists( 'notify_filter', "requests:{$uid}" ) ) {
			$list = json_decode( $this->getRedis()->hGet( 'notify_filter', "requests:{$uid}" ), true );
			foreach ($ids as $id) {
				if ( ! \in_array( $id, $list ) ) {
					$list[] = $id;
				}
			}
		} else {
			$list = $ids;
		}

		return false !== $this->getRedis()
		                      ->hSet( 'notify_filter', "requests:{$uid}", json_encode( $list, JSON_FORCE_OBJECT ) );
	}

	/**
	 * @param null $uid
	 *
	 * @return bool|array
	 * @throws \RedisException
	 */
	public function getFilterMessageByUID( $uid = null ) {

		if ( $uid !== null ) {
			if (
				$this->getRedis()->exists( 'notify_filter' ) &&
				$this->getRedis()->hExists( 'notify_filter', "messages:{$uid}" )
			) {
				return json_decode( $this->getRedis()->hGet( 'notify_filter', "messages:{$uid}" ), true );
			}
		}

		return false;
	}

	/**
	 * @param null $uid
	 *
	 * @return bool|mixed
	 * @throws \RedisException
	 */
	public function getFilterRequestByUID( $uid = null ) {

		if ( $uid !== null ) {
			if (
				$this->getRedis()->exists( 'notify_filter' ) &&
				$this->getRedis()->hExists( 'notify_filter', "requests:{$uid}" )
			) {
				return json_decode( $this->getRedis()->hGet( 'notify_filter', "requests:{$uid}" ), true );
			}
		}

		return false;
	}
}