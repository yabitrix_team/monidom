<?php
/**
 * Project: lkp2.carmoney.loc
 * Created by PhpStorm.
 * User: daglob (User)
 * Date: 29.03.2018
 * Time: 16:22
 */

namespace app\modules\notification\classes;

use Exception;

/**
 * Class WSSessionRegistration
 *
 * Позволяет работать сочередями подключений
 *
 * @package app\modules\notification\classes
 */
class WSSessionRegistration extends WSRedis {
	/**
	 * @var self
	 */
	protected static $instance;
	/**
	 * @var string имя списка регестрируемых подключений
	 */
	private $connection_key_list = 'connections';

	/**
	 * @param string $key
	 * @param array  $params
	 *
	 * @return WSSessionRegistration|static
	 */
	public static function getInstance( $key = 'default', array $params = [] ) {

		if ( static::$instance === null ) {
			static::$instance = new static( $key, $params );
		}

		return static::$instance;
	}

	/**
	 * Регистрация нового соединения с указанием автора
	 *
	 * @param null $uid UID пользователя
	 * @param null $fd  номер сокет подключения
	 *
	 * @throws Exception
	 */
	public function registerConnect( $uid = null, $fd = null ): void {

		if ( isset( $uid, $fd ) && ( $t = $this->getRedis()->multi() ) ) {
			$t->sAdd( "frames_$uid", (string) $fd );
			// todo[daglob]: 22.05.2018 14:55 $t->sAdd( "$this->connection_key_list", "$uid" );
			$t->hSet( "fr_$this->connection_key_list", $fd, $uid );
			$t->exec();
			if ( $err = $t->getLastError() ) {
				throw new Exception( $err );
			}
		}
	}

	/**
	 * Исключение соединения из именованных пулов соединений
	 *
	 * @param null $fd номер сокет подключения
	 *
	 * @throws \RedisException
	 */
	public function disChanel( $fd = null ): void {

		if ( ( $connectors = $this->getConnectors() ) && ! empty( $connectors ) && is_array( $connectors ) ) {
			foreach ( $connectors as $connector ) {
				if ( \in_array( $fd, $this->getChannels( $connector ) ) ) {
					$this->unRegisterConnect( $connector, $fd );
				}
			}
		}
	}

	/**
	 * Получение списка ключей пуллов
	 *
	 * @return array возвращает список UID пользователей, зарегестрированных в данный момент
	 * @throws \RedisException
	 */
	public function getConnectors(): array {

		// todo[daglob]: 22.05.2018 15:07 print_r( $this->getRedis()->hGetAll( "$this->connection_key_list" ) );
		return $this->getRedis()->hGetAll( "fr_$this->connection_key_list" );
	}

	/**
	 * Получение списка соединений по ключу пулла
	 *
	 * @param null $uid UID пользователя
	 *
	 * @return array список номеров сокет подключений
	 * @throws \RedisException
	 */
	public function getChannels( $uid = null ): array {

		// todo[daglob]: 22.05.2018 15:43 сделать перебор результата getConnectors, и переформирование в array_map
		return $this->getRedis()->sMembers( "frames_$uid" );
	}

	/**
	 * Удаление идентификатора соединения из пула
	 *
	 * @param null $uid UID пользователя
	 * @param null $fd  номер сокет подключения
	 *
	 * @throws \RedisException
	 */
	public function unRegisterConnect( $uid = null, $fd = null ): void {

		if ( $t = $this->getRedis()->multi() ) {
			$t->sRem( "frames_$uid", (string) $fd );
			if ( $t->hExists( "fr_$this->connection_key_list", $fd ) ) {
				$t->hDel( "fr_$this->connection_key_list", $fd );
			}
			if ( ! $t->getLastError() ) {
				$t->exec();
			}
			if ( ! $this->isOnline( $uid ) ) {
				if ( $t = $this->getRedis()->multi() ) {
					// todo[daglob]: 22.05.2018 15:08
					//					if ( $t->exists( "$this->connection_key_list" ) ) {
					//						$t->sRem( "$this->connection_key_list", "$uid" );
					//					}
					if ( $t->exists( "frames_$uid" ) ) {
						$t->delete( "frames_$uid" );
					}
					if ( ! $t->getLastError() ) {
						$t->exec();
					}
				}
			}
		}
	}

	/**
	 * Проверка наличия зарегестрированных соединений по ключу пула
	 *
	 * @param null $uid UID пользователя
	 *
	 * @return bool является ли пользователь онлайн
	 * @throws \RedisException
	 */
	public function isOnline( $uid = null ): bool {

		$connectors = $this->getConnectors();
		if ( $uid !== null && ! empty( $connectors ) && \is_array( $connectors ) && \in_array( $uid, $connectors ) && $this->isConnect( $uid ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Количество зарегестрированных соединений по ключу
	 *
	 * @param null $uid UID пользователя
	 *
	 * @return int количество подключений зарегестрированных в системе
	 * @throws \RedisException
	 */
	public function isConnect( $uid = null ): int {

		return $this->getRedis()->sCard( "frames_$uid" );
	}

	/**
	 * Метод последовательно удаляет пулы и соединения по ним
	 *
	 * @throws \RedisException
	 */
	public function shutdown() {

		$this->reset();
		$this->getRedis()->close();
	}

	/**
	 * Сброс данных о соединениях
	 *
	 * @throws \RedisException
	 */
	public function reset(): void {

		// todo[daglob]: 22.05.2018 15:10
		//		while ( $ar = $this->getRedis()->sPop( "$this->connection_key_list" ) ) {
		//			if ( $t = $this->getRedis()->multi() ) {
		//				$t->delete( "frames_$ar" );
		//				$t->exec();
		//			}
		//		}
		if ( $t = $this->getRedis()->multi() ) {
			$t->delete( "fr_$this->connection_key_list" );
			$t->exec();
		}
	}

	/**
	 * @param null $fd
	 *
	 * @return bool|string
	 * @throws \RedisException
	 */
	public function getConnectKeyByFID( $fd = null ) {

		if ( $fd !== null ) {
			if ( $this->getRedis()->hExists( "fr_$this->connection_key_list", $fd ) ) {
				return $this->getRedis()->hGet( "fr_$this->connection_key_list", $fd );
			}
		}

		return false;
	}
}