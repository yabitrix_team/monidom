<?php
/**
 * Project: lkp2.carmoney.loc
 * Created by PhpStorm.
 * User: daglob (User)
 * Date: 28.04.2018
 * Time: 12:43
 */

namespace app\modules\notification\classes;

use RedisException;

class WSRedis {
	/**
	 * @var \Redis
	 */
	private $redis;
	private $redis_params = [
		'key'  => 'default',
		'host' => '127.0.0.1',
		'port' => '6379',
	];

	/**
	 * WSCollector constructor.
	 *
	 * @param string $key
	 * @param array  $params
	 */
	protected function __construct( $key = 'default', array $params = [] ) {

		if ( ! $this->redis instanceof \Redis ) {
			ini_set( 'default_socket_timeout', - 1 );
			$this->redis_params = [
				'key'  => $key,
				'host' => $params['host'],
				'port' => $params['port'],
			];
		}
	}

	/**
	 * @return \Redis
	 * @throws RedisException
	 */
	public function getRedis(): \Redis {

		try {
			$this->redis->ping();
		} catch ( \RedisException $e ) {
			//
		}
		finally {
			$this->connect();

			return $this->redis;
		}
	}

	/**
	 * @throws RedisException
	 */
	protected function connect(): void {

		$this->redis = new \Redis();
		if ( ! $this->redis->pconnect( $this->redis_params['host'], $this->redis_params['port'] ) ) {
			throw new RedisException( 'error connection to redis server!' );
		}
		$this->redis->setOption( \Redis::OPT_READ_TIMEOUT, - 1 );
		$this->redis->setOption( \Redis::OPT_PREFIX, $this->redis_params['key'] . ':' );
	}
}