<?php

namespace app\modules\notification\classes;
/**
 * Class NotifyException - класс исключения для уведомлений,
 * исключения данного класса можно отображать пользователю
 *
 * @package app\modules\notification\classes
 */
class NotifyException extends \Exception {
}