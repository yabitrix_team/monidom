<?php
/**
 * Created by PhpStorm.
 * User: greb
 * Date: 08.05.18
 * Time: 14:10
 */

namespace app\modules\lead\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;
use Yii;

/**
 * Платежная система
 *
 * @package app\modules\payment\v1\models\base
 */
class PartnerLead extends Model {
	use WritableTrait;
	use ReadableTrait;
	/**
	 * @var int ID
	 */
	public $id;
	/**
	 * @var int сумма займа
	 */
	public $summ;
	/**
	 * @var string имя
	 */
	public $client_first_name;
	/**
	 * @var string фамилия
	 */
	public $client_last_name;
	/**
	 * @var string отчетсво
	 */
	public $client_patronymic;
	/**
	 * @var string мобильный телефон
	 */
	public $client_mobile_phone;
	/**
	 * @var string guid региона
	 */
	public $region;
	/**
	 * @var string guid модели машины
	 */
	public $auto_model_guid;
	/**
	 * @var string guid бренда машины
	 */
	public $auto_brand_guid;
	/**
	 * @var string паспорт
	 */
	public $client_passport_number;
	/**
	 * @var string серийный номер паспорта
	 */
	public $client_serial_number;
	/**
	 * @var string дата рождения
	 */
	public $client_birthday;
	/**
	 * @var string ID пользователя
	 */
	public $user_id;
	/**
	 * @var /date дата создания
	 */
	public $created_at;

	/**
	 * @var /date дата обновления
	 */
	public $updated_at;

	/**
	 * @var string guid заявки от CRM
	 */
	public $guid;

	/**
	 * @var string дата создания
	 */
	public $comment;

	public function rules() {

		return [
			[
				[
					'client_first_name',
					'client_mobile_phone',
					'guid',
					'user_id',
				],
				'required',
			],
			[
				[
					'summ',
					'region',
					'auto_model_guid',
					'auto_brand_guid',
					'client_last_name',
					'client_patronymic',
					'client_passport_number',
					'client_serial_number',
					'client_birthday',
					'updated_at',
					'comment',
				],
				'safe'
			],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{partner_leads}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			"id",
			"summ",
			"client_first_name",
			"client_last_name",
			"client_patronymic",
			"client_mobile_phone",
			"region",
			"auto_model_guid",
			"auto_brand_guid",
			"client_passport_number",
			"guid",
			"client_serial_number",
			"client_birthday",
			"comment",
			"user_id",
			"created_at",
			"updated_at",
		];
	}

	/**
	 * Добавление лида
	 *
	 * @return int id созданного лида
	 * @throws \yii\db\Exception
	 */
	public function createLead() {
		if ( $this->validate() ) {
			return $this->add( $this->getAttributes() );
		}
	}

	/**
	 * Поиск лидов по пользователю с пагинацией
	 *
	 * @return int id созданного лида
	 * @throws \yii\db\Exception
	 */
	public function getLeadsByUser( $page, $size, $userId ) {

		return Yii::$app->db->createCommand( "select * from `partner_leads` where user_id=" . $userId . " order by `created_at` DESC limit " . ( ( $page - 1 ) * $size ) . ", " . $size )
		                    ->queryAll();
	}

}