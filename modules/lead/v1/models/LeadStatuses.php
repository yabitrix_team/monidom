<?php
/**
 * Created by PhpStorm.
 * User: greb
 * Date: 08.05.18
 * Time: 14:10
 */

namespace app\modules\lead\v1\models;

use app\modules\app\v1\models\traits\ReadableTrait;
use app\modules\app\v1\models\traits\WritableTrait;
use yii\base\Model;
use Yii;

/**
 * Платежная система
 *
 * @package app\modules\payment\v1\models\base
 */
class LeadStatuses extends Model {

	use WritableTrait;
	use ReadableTrait;

	/**
	 * @var string guid статуса
	 */
	public $guid;

	/**
	 * @var string имя статуса
	 */
	public $name;


	public function rules() {

		return [
			[
				[
					'guid',
					'name',
				],
				'required'
			],
		];
	}

	/**
	 * Получение названия таблицы модели в БД.
	 *
	 * Рекомендуется название обернуть в {{...}}, для совместимости с разными драйверами, пример:
	 * <code>
	 * return "{{auto_brands}}";
	 * </code>
	 *
	 * @return string название таблицы
	 */
	protected function getTableName(): string {

		return "{{partner_leads_statuses}}";
	}

	/**
	 * Получение списка названий полей модели
	 *
	 * @return array список полей
	 */
	protected function getFieldsNames(): array {

		return [
			'guid',
			'name',
		];
	}

	/**
	 * Поиск лидов по пользователю с пагинацией
	 *
	 * @return int id созданного лида
	 * @throws \yii\db\Exception
	 */

}