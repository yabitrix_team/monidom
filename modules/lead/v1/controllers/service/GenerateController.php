<?php

namespace app\modules\lead\v1\controllers\service;

use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\modules\app\v1\controllers\service\GenerateController as GenerateControllerApp;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class GenerateController extends GenerateControllerApp {
	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		return [
			'contentNegotiator' => [
				'class'   => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'verbFilter'        => [
				'class'   => VerbFilter::class,
				'actions' => $this->verbs(),
			],
		];
	}
}
