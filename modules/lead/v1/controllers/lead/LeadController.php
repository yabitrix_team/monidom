<?php
namespace app\modules\lead\v1\controllers\lead;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\controllers\user\UserController as UserControllerApp;
use app\modules\app\v1\models\AutoBrands;
use app\modules\app\v1\models\AutoModels;
use app\modules\app\v1\models\Groups;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\Regions;
use app\modules\app\v1\models\soap\crm\LeadsLoad;
use app\modules\lead\v1\models\PartnerLead;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\UserGroup;
use Exception;
use Yii;
use yii\web\JsonParser;
use yii\rest\Controller;

class LeadController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        try {
            if (!$arUser = Users::instance()->getOne(Yii::$app->user->id)) {
                throw new LeadException('Пользователь не найден');
            }
            $arPost = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (
                (int) $arPost["client_region_id"] && (!$arRegion = Regions::instance()
                    ->getOne($arPost["client_region_id"]))
            ) {
                throw new LeadException('Регион не найден');
            }
            if (
                (int) $arPost["auto_brand_id"] && (!$arBrand = AutoBrands::instance()
                    ->getOne($arPost["auto_brand_id"]))
            ) {
                throw new LeadException('Бренд авто не найден');
            }
            if (
                (int) $arPost["auto_brand_id"] && (int) $arPost["auto_model_id"] && ((!$arModel = AutoModels::instance()
                        ->getOne($arPost["auto_model_id"])) ||
                                                                                     ((int) $arModel["auto_brand_id"] !==
                                                                                      (int) $arPost["auto_brand_id"]))
            ) {
                throw new LeadException('Модель не найдена');
            }
            $model          = new LeadsLoad(['scenario' => LeadsLoad::SCENARIO_LOAD_LEAD]);
            $sLeadAgentName = trim($arUser["last_name"]." ".$arUser["first_name"]." ".$arUser["second_name"]);
            if (
            !$model->load(
                [
                    "phone"           => $arPost["client_mobile_phone_pure"],
                    "channel"         => $sLeadAgentName,
                    "first_name"      => $arPost["client_first_name"],
                    "last_name"       => $arPost["client_last_name"],
                    "second_name"     => $arPost["client_patronymic"],
                    "birthday"        => $arPost["client_birthday"],
                    "region"          => $arRegion["name"],
                    "sum"             => $arPost["summ"],
                    "auto_brand"      => $arBrand["name"],
                    "auto_model"      => $arModel["name"],
                    "passport_serial" => $arPost["client_passport_serial_number"],
                    "passport_number" => $arPost["client_passport_number"],
                    "comment"         => $arPost["comment_client"],
                ],
                ''
            )
            ) {
                throw new LeadException('Не были указаны параметры для создания лида');
            }
            $res = $model->loadLead();
            if (is_a($res, LeadsLoad::class)) {
                Log::error(["text" => print_r($res->firstErrors, true)], "crm.lead.new");

                return $this->getClassAppResponse()::get(false, $res->firstErrors);
            }
            if (is_string($res)) {
                throw new LeadException($res);
            }
            //			if (!$res->GUID) {
            //				throw new \Exception( 'Невозможно сохранить заявку без GUID от CRM, обратитесь к администратору' );
            //			}
            $partnerLeadModel = new PartnerLead();
            $isLoaded         = $partnerLeadModel->load(
                [
                    "summ"                   => str_replace(' ', '', $arPost["summ"]),
                    "client_first_name"      => $arPost["client_first_name"],
                    "client_last_name"       => $arPost["client_last_name"],
                    "client_patronymic"      => $arPost["client_patronymic"],
                    "client_mobile_phone"    => $arPost["client_mobile_phone"],
                    "region"                 => $arRegion["guid"],
                    "auto_brand_guid"        => $arBrand["guid"],
                    "auto_model_guid"        => $arModel["guid"],
                    "client_passport_number" => str_replace(' ', '', $arPost["client_passport_number"]),
                    "client_serial_number"   => str_replace(' ', '', $arPost["client_passport_serial_number"]),
                    "client_birthday"        => $arPost["client_birthday"],
                    "updated_at"             => date("Y-m-d H:i:s"),
                    "user_id"                => $this->user->id,
                    "guid"                   => '472ff1e7-aaaa-bbbb-80f0-00155d641001',
                    // todo исправить после 18 релиза CRM
                    "comment"                => $arPost["comment_client"],
                ],
                ''
            );
            if ($isLoaded) {
                if ($partnerLeadModel->createLead()) {
                    return $this->getClassAppResponse()::get(['success' => true]);
                } else {
                    throw new LeadException('Произошла ошибка при сохранении лида');
                }
            } else {
                throw new LeadException('Произошла ошибка при создании модели');
            }
        } catch (LeadException $e) {
            Log::error(["text" => $e->getMessage()], "crm.lead.new");

            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $userId = $this->user->id;
        $page   = (int) Yii::$app->request->get("page") ?: 1;
        $size   = (int) Yii::$app->request->get("size") ?: 20;
        $cntAll =
            (int) Yii::$app->db->createCommand("select count(*) cnt from partner_leads where user_id=".$userId)
                ->queryOne()["cnt"];
        $model  = new PartnerLead();

        return $this->getClassAppResponse()::get(
            [
                "page"  => $page,
                "size"  => $size,
                "count" => ceil($cntAll / $size),
                "items" => $model->getLeadsByUser($page, $size, $userId),
            ]
        );
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                            'index',
                        ],
                        'roles'   => ['role_lead_agent'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнение операции.');
                    }
                },
            ],
        ];
    }
}

class LeadException extends Exception
{
}
