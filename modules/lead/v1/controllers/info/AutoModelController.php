<?php

namespace app\modules\lead\v1\controllers\info;

use app\modules\app\v1\controllers\info\AutoModelController as AutoModelControllerApp;

class AutoModelController extends AutoModelControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index', 'by-id' ],
						'roles'   => [ 'role_lead_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {

			$result = $this->unsetModelColumn( $result );
		}

		return $result;
	}

	private function unsetModelColumn( array $result ) {

		$result['data'] = array_map( function( $v ) {

			unset( $v['created_at'] );
			unset( $v['updated_at'] );
			unset( $v['active'] );
			unset( $v['sort'] );

			return $v;
		}, $result['data'] );

		return $result;
	}

	public function actionById( $id ) {

		$result = parent::actionById( $id );
		if ( ! empty( $result['data'] ) ) {

			$result = $this->unsetModelColumn( $result );
		}

		return $result;
	}
}
