<?php

namespace app\modules\lead\v1\controllers\user;

use app\modules\app\v1\controllers\user\UserController as UserControllerApp;
use app\modules\app\v1\models\Groups;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\UserGroup;
use Exception;
use Yii;
use yii\web\JsonParser;

class UserController extends UserControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index'
						],
						'roles'   => [ 'role_lead_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}
}
