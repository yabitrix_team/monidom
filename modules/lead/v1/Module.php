<?php

namespace app\modules\lead\v1;
/**
 * Class Module - модуль для работы с кабинетом Лидогенератора
 *
 * @package app\modules\panel\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\lead\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
