<?php

namespace app\modules\web\v1;
/**
 * Class Module - модуль для работы с Веб
 *
 * @package app\modules\web\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\web\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
