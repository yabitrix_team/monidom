<?php

namespace app\modules\web\v1\controllers\user;

use app\modules\app\v1\controllers\user\UserController as UserControllerApp;
use app\modules\app\v1\models\Partners;
use app\modules\web\v1\classes\traits\TraitConfigController;

class UserController extends UserControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'update',
							'verify',
							'verifysms',
						],
						'roles'   => [ 'role_user_partner' ],
					],
					[
						'allow'   => true,
						'actions' => [
							'index',
						],
						'roles'   => [ 'role_support', 'role_admin' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

    /**
     * @return array|mixed|\yii\web\User
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionIndex() {

		$result = parent::actionIndex();
		//Загружаем имя партнера по точке для пользователей web - роли role_admin, role_user_partner
		if ( empty( $result['data']['point_id'] ) ) {
			return $this->getClassAppResponse()::get( false, false, 'Не указана точка у пользователя.' );
		}
        $partner = ( new Partners() )->getByPointId( $result['data']['point_id'] );
        if ( ! empty( $partner ) ) {

            $result['data']['point_name']   = $partner['point_name'];
            $result['data']['partner_name'] = $partner['partner_name'];
            unset( $result['data']['point_id'] );

        }

		return $result;
	}
}
