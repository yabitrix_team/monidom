<?php
namespace app\modules\web\v1\controllers\user;

use app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\LeadsConsumer;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use Yii;
use yii\filters\AccessControl;
use yii\web\JsonParser;

class AuthController extends AuthControllerApp
{
    /**
     * Экшен авторизации пользователя
     *
     * @return array|mixed|\yii\web\User
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreate()
    {
        $result = parent::actionCreate();
        if (!empty($result['data'])) {
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            //если при авторизации у юзера есть лид, то отправляем события в CRM
            $lead = (new Leads())->getByPhone($reqBody["login"], ['guid'], LeadsConsumer::CLIENT);
            if (!empty($lead['guid'])) {
                //отправка события в CRM "Авторизация в ЛКК"
                EventsLoad::sendEventLoadBit(150, $lead['guid'], $reqBody["login"]);
            }
        }

        return $result;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'by-code'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['index', 'delete'],
                        'roles'   => ['role_user_partner', 'role_support', 'role_admin'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
