<?php

namespace app\modules\web\v1\controllers\request;

use app\modules\app\v1\controllers\request\SecondDocumentController as SecondDocumentControllerApp;
use app\modules\web\v1\classes\traits\TraitConfigController;
use app\modules\web\v1\models\Requests;

class SecondDocumentController extends SecondDocumentControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'create', 'by-code' ],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

	public function actionByCode( $code ) {

		$result = parent::actionByCode( $code );
		// Исключаем Анкета.pdf для Партнера из doc_pack_2
		if ( ! empty( $result['data']['docPack2'] ) ) {
			$newDocs = [];
			$request = ( new Requests() )->getOneByFilter( [ 'code' => $code ] );
			if ( empty( $request['is_pep'] ) ) {
				foreach ( $result['data']['docPack2'] as $subKey => $subArray ) {
					if ( $subArray['name'] != 'Анкета' ) {
						$newDocs[] = $result['data']['docPack2'][ $subKey ];
					}
				}
				$result['data']['docPack2'] = $newDocs;
			}
		}

		return $result;
	}
}
