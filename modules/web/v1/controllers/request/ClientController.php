<?php

namespace app\modules\web\v1\controllers\request;

use app\modules\app\v1\controllers\request\ClientController as ClientControllerApp;
use app\modules\web\v1\classes\traits\TraitConfigController;

/**
 * Class ClientController - контроллер для работы со списками заявок
 *
 * @package app\modules\web\v1\controllers
 */
class ClientController extends ClientControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'active',
							'closed',
							'wait',
						],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {
			foreach ( $result['data']['requests'] as &$item ) {

				unset($item['is_null']);
				unset($item['commission_cash_withdrawal']);
				unset($item['id']);
				unset($item['point_name']);
				unset($item['point_city']);
				unset($item['signer_first_name']);
				unset($item['signer_last_name']);
				unset($item['signer_second_name']);

				//Подставляем вместо null значение '' в статусах заявки
				$item = array_map( function( $v ) {
					if ( is_null( $v ) ) {
						return '';
					} else {
						return $v;
					}
				}, $item );
			}
		}

		return $result;
	}
}
