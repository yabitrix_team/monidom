<?php

namespace app\modules\web\v1\controllers\request;

use app\modules\app\v1\controllers\request\FileController as FileControllerApp;

class FileController extends FileControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'by-code',
							'preview-by-code',
							'doc-by-code',
							'zip-by-code',
							'accreditation-by-code',
							'delete',
							'create',
							'extra',
						],
						'roles'   => [ 'role_user_partner', 'role_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
