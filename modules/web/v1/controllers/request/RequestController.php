<?php

namespace app\modules\web\v1\controllers\request;

use app\modules\app\v1\controllers\request\RequestController as RequestControllerApp;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Regions;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use app\modules\web\v1\classes\traits\TraitConfigController;

/**
 * Class RequestController - контроллер для работы с заявкой
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends RequestControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {
			//Добавляем пустые значения для файлов которых нет в БД
			$arFileBindFlip  = array_flip( $this->arFileBind );
			$arFileBindEmpty = array_map( function( $v ) {

				return [];
			}, $arFileBindFlip );
			foreach ( $result['data'] as &$item ) {

				$arDiffFileBind = array_diff_key( $arFileBindEmpty, $item );
				$item           = array_merge( $item, $arDiffFileBind );
				$item           = array_map( function( $v ) {

					if ( is_null( $v ) || $v == '0.00' || $v == '0' ) {
						return '';
					} else {
						return $v;
					}
				}, $item );
			}
		}

		return $result;
	}

    /**
     * Экшен получения заявки по коду
     *
     * @param $code - Код заявки
     *
     * @return array|bool|false|string
     * @throws \Exception
     */
	public function actionByCode( $code ) {

		$result = parent::actionByCode( $code );
		if ( ! empty( $result['data'] ) ) {
			if ( $result['data']['auto_brand_id'] ) {
				$brandArray = [
					'id'   => $result['data']['auto_brand_id'],
					'name' => $result['data']['auto_brand_name'],
				];
				unset( $result['data']['auto_brand_id'], $result['data']['auto_brand_name'] );
				$result['data']['auto_brand_id'] = $brandArray;
			}
			if ( $result['data']['auto_model_id'] ) {
				$modelArray = [
					'id'   => $result['data']['auto_model_id'],
					'name' => $result['data']['auto_model_name'],
				];
				unset( $result['data']['auto_model_id'], $result['data']['auto_model_name'] );
				$result['data']['auto_model_id'] = $modelArray;
			}
			$result['data'] = array_map( function( $v ) {

				if ( is_null( $v ) || $v == '0.00' || $v == '0' ) {
					return '';
				} else {
					return $v;
				}
			}, $result['data'] );
			// Исключаем Анкета.pdf для Партнера из doc_pack_2
			if ( sizeof( $result['data']['doc_pack_2'] ) && empty( $result['data']['is_pep'] ) ) {
				foreach ( $result['data']['doc_pack_2'] as $subKey => $subArray ) {
					if ( $subArray['name'] == 'Анкета.pdf' ) {
						unset( $result['data']['doc_pack_2'][ $subKey ] );
					}
				}
			}
			$events = array_column( Events::instance()->getByRequestId( $result['data']['id'] ), 'code' );
            $eventsData = Events::instance()->getDataByRequestId( $result['data']['id'] );
			$result['data']['step'] = 2;
			if ( in_array( 701, $events ) && ! in_array( 702, $events ) ) { // заявка создана, но больше ничего не сделано
				$result['data']['step'] = 2;
			} elseif ( in_array( 702, $events ) && ! in_array( 711, $events ) ) { // отправлена полная
				$result['data']['step'] = 3;
			} elseif ( in_array( 711, $events ) && ! in_array( 101, $events ) ) { // отправлены сканы ожидаем приход документов
				$result['data']['step'] = 4;
			} elseif ( in_array( 101, $events ) && ! in_array( 712, $events ) ) { // 1 доки подписаны
				$result['data']['step'] = 5;
			} elseif ( in_array( 712, $events ) ) { // загрузили фото авто
				$result['data']['step'] = 6;
			}
			$requestStatus = ( new StatusesJournal() )->getCurrentByRequestID( $result['data']['id'] );
			// высчитываем позицию и цвет статуса
			$currentStatus                     = ( new Statuses() )->getOne( $requestStatus['status_id'] );
			$result['data']['bar']['progress'] = $currentStatus['progress'];
			$result['data']['bar']['color']    = $currentStatus['color'];
			$result['data']['bar']['percent']  = round( $currentStatus['progress'] / ( 100 / 100 ) );
			$result['data']['bar']['left']     = '15 минут';
			$result['data']['preApproved']     = 0;
			if ( $currentStatus['guid'] == 'cbb42353-51e6-4a88-b2ef-575729da0343' ) { // предварительно одобрено
				$result['data']['preApproved'] = 1;
			};
			if (
				$currentStatus['guid'] == 'abfa1724-c3fd-4b7f-9631-7572288a165b' || // Одобрено
				$currentStatus['guid'] == '74ed5b21-af9b-44f3-bda5-85fde6847b76' || // Ожидает подписания договора
				$currentStatus['guid'] == 'ff126df5-e381-4312-b4f3-8863314038b2'    // Клиент получает деньги
			) {
				if ( in_array( 102, $events ) ) {
					$result['data']['step'] = 8;
				} else {
					$result['data']['step'] = 7;
				};
			} elseif ( $currentStatus['guid'] == 'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e' ) { // статус "отказано"
				$result['data']['step'] = 10;
			} elseif ( $currentStatus['guid'] == 'c37fec4e-28b3-4239-8c2f-6812821b1b32' || $currentStatus['guid'] == 'e01a9627-195b-4422-860e-91ba96897840' ) { // статус "отозвано"
				$result['data']['step'] = 11;
			} elseif ( $currentStatus['guid'] == '2da7f37a-0226-11e6-80f9-00155d01bf07') { // статус "договор погашен"
				$result['data']['step'] = 12;
			} elseif ( $currentStatus['guid'] == '6818753f-7d4c-45e6-ad29-5f98db749508') { // Деньги выданы
                $result['data']['step'] = 13;
            };
			//todo[hdcy]: 03.05.2018 По возможности оптимизировать
			//Добавляем пустые значения для файлов которых нет в БД
			$arFileBindFlip  = array_flip( $this->arFileBind );
			$arFileBindEmpty = array_map( function( $v ) {

				return [];
			}, $arFileBindFlip );
			$arDiffFileBind  = array_diff_key( $arFileBindEmpty, $result['data'] );
			$result['data']  = array_merge( $result['data'], $arDiffFileBind );
			// вырезаем .pdf из документов
			$docPack1 = [];
			foreach ( $result['data']['doc_pack_1'] as $doc ) {
				$docPack1[] = [
					'code'        => $doc['code'],
					'name'        => reset( explode( '.', $doc['name'] ) ),
					'description' => $doc['description'],
					'url'         => $doc['url'],
				];
			};
			$result['data']['doc_pack_1'] = $docPack1;
			$docPack2                     = [];
			foreach ( $result['data']['doc_pack_2'] as $doc ) {
				$docPack2[] = [
					'code'        => $doc['code'],
					'name'        => reset( explode( '.', $doc['name'] ) ),
					'description' => $doc['description'],
					'url'         => $doc['url'],
				];
			};
			$result['data']['doc_pack_1']         = $docPack1;
			$result['data']['doc_pack_2']         = $docPack2;
			$result['data']['client_region_name'] = ( new Regions() )->getOne( $result['data']['client_region_id'] )['name'];

			if (strlen($result['data']['client_mobile_phone'])) {
				unset($result['data']['client_mobile_phone']);
			}

            $needFirstPackReSign = false;
			foreach ($eventsData as $event) {
			    if ((int)$event["code"] == 830 && !(int)$event["is_sended"]) {
                    $needFirstPackReSign = true;
                    break;
                }
            }

            $result['data']["reFirstPack"] = $needFirstPackReSign && (int)$result['data']['step'] !== 4;

		}

		return $result;
	}

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'by-code',
							'notice',
							'create',
							'update',
						],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
