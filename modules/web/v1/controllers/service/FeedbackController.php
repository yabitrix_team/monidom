<?php

namespace app\modules\web\v1\controllers\service;

use app\modules\app\v1\controllers\service\FeedbackController as FeedbackControllerApp;

/**
 * Class FeedbackController - контроллер взаимодействия с сервисом Feedback
 *
 * @package app\modules\web\v1\controllers\service
 */
class FeedbackController extends FeedbackControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'create',
						],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}