<?php

namespace app\modules\web\v1\controllers\service;

use app\modules\app\v1\controllers\service\SmsController as SmsControllerApp;

/**
 * Class SMSController - контроллер взаимодействия с SMS-сервисом
 *
 * @package app\modules\web\v1\controllers\service
 */
class SmsController extends SMSControllerApp {
	public function actionCreate() {

		$checkSessionResult = \app\modules\app\v1\classes\CheckSession::check();
		if ( is_string( $checkSessionResult ) ) {
			throw new \yii\web\HttpException( 403, $checkSessionResult );
		} elseif ( is_array( $checkSessionResult ) ) {
			return $this->getClassAppResponse()::get( false, $checkSessionResult );
		}

		return parent::actionCreate();
	}
}