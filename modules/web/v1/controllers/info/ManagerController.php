<?php

namespace app\modules\web\v1\controllers\info;

use app\modules\app\v1\controllers\info\ManagerController as ManagerControllerApp;

class ManagerController extends ManagerControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {
			$rp  = [];
			$mrp = [];
			foreach ( $result['data'] as $manager ) {
				if ( $manager['type'] == 'rp' ) {
					array_push( $rp, $manager );
				}
				if ( $manager['type'] == 'mrp' ) {
					array_push( $mrp, $manager );
				}
			}
			unset( $result['data'] );
			empty( $rp ) ?: $result['data']['rp'] = $rp;
			empty( $mrp ) ?: $result['data']['mrp'] = $mrp;
		}

		return $result;
	}
}
