<?php
namespace app\modules\web\v1\controllers\info;

use app\modules\app\v1\controllers\info\InstructionsPhotoDocsController as InstructionsPhotoDocsControllerApp;
use app\modules\web\v1\classes\traits\TraitConfigController;

/**
 * Class InstructionsPhotoDocsController - контроллер для получения статического контента
 * инстркуции фотографирования документов
 *
 * @package app\modules\mp\v1\controllers\info
 */
class InstructionsPhotoDocsController extends InstructionsPhotoDocsControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data']) && is_array($result['data'])) {
            $result['data'] = [
                'name' => $result['data']['name'],
                'text' => $result['data']['text'],
            ];
            unset($result['version']);
        }

        return $result;
    }

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::class,
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['role_user_partner'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}