<?php

namespace app\modules\web\v1\controllers\info;

use app\modules\app\v1\controllers\info\MethodIssuanceController as MethodIssuanceControllerApp;
use app\modules\app\v1\models\Points;
use Yii;

class MethodIssuanceController extends MethodIssuanceControllerApp {

	/**
	 * Экшен получения всех элементов
	 *
	 * @return array - Вернет сформированный массив
	 */
	public function actionIndex() {

		try {

			$data = Yii::$app->db
				->createCommand( "SELECT  
  `m`.`id` as `id`,
  `m`.`guid` as `guid`,
  `m`.`name` as `name`,
  `m`.`description` as `description`
FROM `methods_issuance` AS `m`
  LEFT JOIN `cabinets_methods_issuance` AS `cm` ON `cm`.`method_id` = `m`.`id`
  LEFT JOIN `cabinets` AS `c` ON `c`.`id` = `cm`.`cabinet_id`
WHERE `c`.`id` = 1")
				->queryAll();

			$newData = $data;

			return $this->getClassAppResponse()::get( $newData, false, false, false );
		} catch ( \Exception $e ) {
			return $this->getClassAppResponse()::get( false, false, $e->getMessage() );
		}
	}

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

}
