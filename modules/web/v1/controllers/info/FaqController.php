<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 03.05.2018
 * Time: 17:17
 */

namespace app\modules\web\v1\controllers\info;

use app\modules\app\v1\controllers\info\FaqController as FaqControllerApp;
use app\modules\web\v1\classes\traits\TraitConfigController;

class FaqController extends FaqControllerApp {
	use TraitConfigController;
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_user_partner' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}