<?php

namespace app\modules\web\v1\controllers\info;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\info\WorkplacePeriodController as WorkplacePeriodControllerApp;

class WorkplacePeriodController extends WorkplacePeriodControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_user_partner' ],
					],
					[
						'allow'   => false,
						'actions' => [],
						'roles'   => [ '?' ],
					],
				],
				'denyCallback' => function() {
					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {


			$result['data'] = array_map( function( $v ) {

				unset( $v['created_at'] );
				unset( $v['updated_at'] );
				unset( $v['active'] );
				unset( $v['sort'] );

				return $v;
			}, $result['data'] );
		}

		return $result;
	}
}
