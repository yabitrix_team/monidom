<?php

namespace app\modules\web\v1\models;

use app\modules\app\v1\models\Requests as RequestApp;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\Users;
use Yii;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends RequestApp {
	/**
	 * @var array - обязательные поля при создании заявки,
	 *            в наследнике переопределить под свои задачи
	 */
	protected $reqFieldCreate = [
		'summ',
		'date_return',
		'auto_brand_id',
		'auto_model_id',
        'auto_year',
		'credit_product_id',
		'client_first_name',
		'client_last_name',
		'client_passport_number',
		'client_passport_serial_number',
		'client_birthday',
		'client_region_id',
		'client_mobile_phone',
	];
	/**
	 * @var array - поля обновления заявки для сценария Обновление
	 */
	protected $fieldScenarioUpdate = [
		'client_home_phone',
		'client_email',
		'client_total_monthly_income',
		'client_total_monthly_outcome',
		'client_employment_id',
		'client_workplace_experience',
		'client_workplace_period_id',
		'client_workplace_phone',
		'client_workplace_name',
		'client_workplace_address',
		'client_guarantor_name',
		'client_guarantor_relation_id',
		'client_guarantor_phone',
		'comment_partner',
		'method_of_issuance_id',
		'bank_bik',
		'checking_account',
		'card_number',
		'card_holder',
		'place_of_stay',
	];

	/**
	 * Метод получения заявки по коду и по точке пользователя
	 *
	 * @param string $code - код заявки
	 *
	 * @return array|bool|false - вернет результат выборки или строку с иключением
	 */
	public function getByCode( string $code ) {

		try {
			if ( strlen( $code ) != 32 ) {
				throw new \Exception( 'При попытке получения заявки по коду был некорректно указан код заявки' );
			}

			return Yii::$app->db
				->createCommand( "SELECT  
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `card_holder`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`,
  `is_pep`,
  `place_of_stay`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `code` = :code AND `point_id` = :point_id", [
					':code'     => $code,
					':point_id' => ( ( new Users() )->getOne( Yii::$app->user->id ) )['point_id'],
				] )
				->queryOne();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод обновления заявки по коду(хэшу)
	 *
	 * @param string $code    - Код(хэш) заявки
	 * @param array  $params  - Параметры для обновления заявки
	 * @param bool   $prepare - флаг для подготовки данных
	 *
	 * @return bool|int - Вернет кол-во обновленных строк или выбросит исключение
	 */
	public function updateByCode( string $code, array $params = [], $prepare = true ) {

		$params['user_id'] = Yii::$app->user->getId();

		return parent::updateByCode( $code, $params, $prepare );
	}
}