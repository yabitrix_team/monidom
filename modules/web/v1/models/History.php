<?php

namespace app\modules\web\v1\models;

use app\modules\app\v1\models\History as HistoryApp;

/**
 * Class History - модель для работы со справочником История заявок
 *
 * @package app\modules\app\v1\models
 */
class History extends HistoryApp {
}