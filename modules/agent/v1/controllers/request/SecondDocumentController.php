<?php
namespace app\modules\agent\v1\controllers\request;

use app\modules\app\v1\controllers\request\SecondDocumentController as SecondDocumentControllerApp;
use app\modules\app\v1\models\File;
use app\modules\web\v1\classes\traits\TraitConfigController;
use Yii;

class SecondDocumentController extends SecondDocumentControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    public function behaviors()
    {

        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'by-code'],
                        'roles'   => ['role_agent'],
                    ],
                ],
                'denyCallback' => function () {

                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}
