<?php

namespace app\modules\agent\v1\controllers\request;

use app\modules\agent\v1\models\Requests;
use app\modules\app\v1\controllers\request\FileController as FileControllerApp;
use app\modules\app\v1\models\File;
use Yii;

class FileController extends FileControllerApp {

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'by-code',
							'preview-by-code',
							'doc-by-code',
							'zip-by-code',
							'accreditation-by-code',
							'delete',
							'create',
							'extra',
						],
						'roles'   => [ 'role_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
