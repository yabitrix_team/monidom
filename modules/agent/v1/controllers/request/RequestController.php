<?php

namespace app\modules\agent\v1\controllers\request;

use app\modules\agent\v1\classes\traits\TraitConfigController;
use app\modules\agent\v1\models\Requests;
use app\modules\app\v1\controllers\request\RequestController as RequestControllerApp;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use Yii;
use yii\web\JsonParser;

/**
 * Class RequestController - контроллер для работы с заявкой в ЛКВМ
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends RequestControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;
	private $imgFileType = [
		'foto_sts',
		'foto_pts',
		'foto_auto',
		'foto_passport',
		'foto_client',
	];

	/**
	 * Экшен получения всех заявок
	 *
	 * @return array - Вернет сформированный массив
	 */
	public function actionIndex() {

		$classModelRequest = $this->getModelRequest();
		$modelRequest      = new $classModelRequest;
		$result            = $this->getClassAppResponse()::get( $modelRequest->getAll() );
		if ( ! empty( $result['data'] ) ) {
			$data = [];
			//Добавляем пустые значения для файлов которых нет в БД
			$arFileBindFlip  = array_flip( $this->arFileBind );
			$arFileBindEmpty = array_map( function( $v ) {

				return [];
			}, $arFileBindFlip );
			foreach ( $result['data'] as $request ) {
				if ( $request['auto_brand_id'] ) {
					$brandArray = [
						'id'   => $request['auto_brand_id'],
						'name' => $request['auto_brand_name'],
					];
					unset( $request['auto_brand_id'], $request['auto_brand_name'] );
					$request['auto_brand_id'] = $brandArray;
				}
				if ( $request['auto_model_id'] ) {
					$modelArray = [
						'id'   => $request['auto_model_id'],
						'name' => $request['auto_model_name'],
					];
					unset( $request['auto_model_id'], $request['auto_model_name'] );
					$request['auto_model_id'] = $modelArray;
				}
				$request         = array_map( function( $v ) {

					if ( is_null( $v ) || $v == '0' || $v == '0.00' ) {
						return '';
					} else {
						return $v;
					}
				}, $request );
				$arDiffFileBind  = array_diff_key( $arFileBindEmpty, $request );
				$request         = array_merge( $request, $arDiffFileBind );
				$events          = array_column( ( new Events() )->getByRequestId( $request['id'] ), 'code' );
				$requestStatus   = ( new StatusesJournal() )->getCurrentByRequestID( $request['id'] );
				$currentStatus   = ( new Statuses() )->getOne( $requestStatus['status_id'] );
				$request['step'] = $this->getStep( $events, $currentStatus );
				$data[]          = $request;
			};
			$result['data'] = $data;
		}
		// обработаем разделив на активные и неактивные
		$newResult['active'] = [];
		$newResult['closed'] = [];
		foreach ( $result['data'] as $request ) {
			if ( $request['step'] > 5 ) {
				$newResult['closed'][] = $request;
			} else {
				$newResult['active'][] = $request;
			};
		};
		$result['data'] = $newResult;

		return $result;
	}

	/**
	 * Экшен просчета шага заявки
	 *
	 * @param array $events - Массив с эвентами по заявке
	 *
	 * @return int - Вернет цифру статуса
	 */
	private function getStep( $events, $currentStatus ) {

		//print_r($events);
		if ( ! in_array( 801, $events ) ) {
			$result = 2;
		} elseif ( in_array( 801, $events ) && ! in_array( 802, $events ) ) {
			$result = 3;
		} elseif ( in_array( 802, $events ) && ! in_array( 803, $events ) ) {
			$result = 4;
		} elseif ( in_array( 803, $events ) && ! in_array( 102, $events ) ) {
			$result = 5;
		} elseif ( in_array( 102, $events ) ) {
			$result = 6;
		};
		if (
			( $currentStatus['guid'] == 'abfa1724-c3fd-4b7f-9631-7572288a165b' || // Одобрено
			  $currentStatus['guid'] == '74ed5b21-af9b-44f3-bda5-85fde6847b76' || // Ожидает подписания договора
			  $currentStatus['guid'] == 'ff126df5-e381-4312-b4f3-8863314038b2' || // Клиент получает деньги
			  $currentStatus['guid'] == '6818753f-7d4c-45e6-ad29-5f98db749508' )    // Деньги выданы
			&&
			( in_array( 801, $events ) && in_array( 802, $events ) && in_array( 803, $events ) )
		) {
			if ( in_array( 102, $events ) ) {
				$result = 6;
			} else {
				$result = 5;
			}
		};
		if ( $currentStatus['guid'] == 'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e' ) { // статус "отказано"
			$result = 10;
		} elseif ( $currentStatus['guid'] == 'c37fec4e-28b3-4239-8c2f-6812821b1b32' ) { // статус "отозвано"
			$result = 11;
		} elseif ( $currentStatus['guid'] == 'e01a9627-195b-4422-860e-91ba96897840' ) { // статус "договор аннулирован"
			$result = 12;
		}

		return $result;
	}
	/**
	 * Экшен получения заявки по коду
	 *
	 * @param $code - Код заявки
	 *
	 * @return array|bool|false|string
	 */
	public function actionByCode( $code ) {

		$result = parent::actionByCode( $code );
		if ( ! empty( $result['data'] ) ) {
			if ( $result['data']['auto_brand_id'] ) {
				$brandArray = [
					'id'   => $result['data']['auto_brand_id'],
					'name' => $result['data']['auto_brand_name'],
				];
				unset( $result['data']['auto_brand_id'], $result['data']['auto_brand_name'] );
				$result['data']['auto_brand_id'] = $brandArray;
			}
			if ( $result['data']['auto_model_id'] ) {
				$modelArray = [
					'id'   => $result['data']['auto_model_id'],
					'name' => $result['data']['auto_model_name'],
				];
				unset( $result['data']['auto_model_id'], $result['data']['auto_model_name'] );
				$result['data']['auto_model_id'] = $modelArray;
			}
			$result['data'] = array_map( function( $v ) {


				if ( is_null( $v ) || $v == '0.00' || $v == '0' ) {
					return '';
				} else {
					return $v;
				}
			}, $result['data'] );
			// Переформатируем описание в doc_pack2
//			if ( ! empty( $result['data']['doc_pack_2'] ) ) {
//				$result['data']['doc_pack_2'] = array_map( function( $v ) {
//
//					$v['description'] = str_replace( 'Подписан', 'Распечатан', $v['description'] );
//
//					return $v;
//				}, $result['data']['doc_pack_2'] );
//			}
			//Форматируем thumbnail
			if ( ! empty( $result['data']['thumbnail'] ) ) {
				$resultDocumentList = $this->sortFileByType( $result['data']['thumbnail'], 'file_type' );
				unset( $result['data']['thumbnail'] );
				for ( $i = 0; $i < count( $resultDocumentList ); $i ++ ) {
					$result['data'] = $result['data'] + $resultDocumentList[ $i ];
				}
			}
			//Форматируем документы
			if ( ! empty( $result['data']['doc'] ) ) {
				$resultDocumentList = $this->sortFileByType( $result['data']['doc'], 'file_type' );
				unset( $result['data']['doc'] );
				for ( $i = 0; $i < count( $resultDocumentList ); $i ++ ) {
					$result['data'] = $result['data'] + $resultDocumentList[ $i ];
				}
			}
			//Добавляем пустые значения для файлов которых нет в БД
			$arFileBindFlip  = array_flip( $this->arFileBind );
			$arFileBindEmpty = array_map( function( $v ) {

				return [];
			}, $arFileBindFlip );
			$arDiffFileBind  = array_diff_key( $arFileBindEmpty, $result['data'] );
			$result['data']  = array_merge( $result['data'], $arDiffFileBind );
			//Остальные параметры
			$events = array_column( ( new Events() )->getByRequestId( $result['data']['id'] ), 'code' );
			// высчитываем позицию и цвет статуса
			$requestStatus                     = ( new StatusesJournal() )->getCurrentByRequestID( $result['data']['id'] );
			$currentStatus                     = ( new Statuses() )->getOne( $requestStatus['status_id'] );
			$result['data']['step']            = $this->getStep( $events, $currentStatus );
			$result['data']['bar']['progress'] = $currentStatus['progress'];
			$result['data']['bar']['color']    = $currentStatus['color'];
			$result['data']['bar']['percent']  = round( $currentStatus['progress'] * 100 / 100 ); // последний 100 ставить другой если надо пересчитать
			$result['data']['bar']['left']     = '15 минут';
			// смотрим, можно ли подписывать документы
			if (
				$currentStatus['guid'] == 'abfa1724-c3fd-4b7f-9631-7572288a165b' || // Одобрено
				$currentStatus['guid'] == '74ed5b21-af9b-44f3-bda5-85fde6847b76' || // Ожидает подписания договора
				$currentStatus['guid'] == 'ff126df5-e381-4312-b4f3-8863314038b2' || // Клиент получает деньги
				$currentStatus['guid'] == '6818753f-7d4c-45e6-ad29-5f98db749508'    // Деньги выданы
			) {
				$result['data']['canSignature'] = true;
			} else {
				$result['data']['canSignature'] = false;
			};

			unset( $result['data']['id'] );
		}

		return $result;
	}

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::class,
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'by-code',
							'notice',
							'create',
							'update',
							'is-old-files',
							'delete-old-files',
							'check-and-delete-old-files'
						],
						'roles'   => [ 'role_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}

}