<?php

namespace app\modules\agent\v1\controllers\info;

use app\modules\app\v1\controllers\info\AutoModelController as AutoModelControllerApp;

class AutoModelController extends AutoModelControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'by-id',
						],
						'roles'   => [ 'role_agent' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
