<?php

namespace app\modules\agent\v1;
/**
 * Class Module - модуль для работы с ЛКВМ
 *
 * @package app\modules\agent\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\agent\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
