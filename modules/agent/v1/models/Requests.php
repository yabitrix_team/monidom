<?php

namespace app\modules\agent\v1\models;

use app\modules\app\v1\models\Requests as RequestApp;
use app\modules\app\v1\models\Users;
use Yii;
use yii\db\Expression;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends RequestApp {
	/**
	 * @var array - поля обновления заявки для сценария Обновление
	 */
	protected $fieldScenarioUpdate = [
		'print_link',
	];

	/**
	 * @return array правила валидации передаваемых данных
	 */
	public function rules() {

		return [
			[
				'print_link',
				'string',
				'length' => 8,
			],
		];
	}

	/**
	 * Метод получения заявки по коду
	 *
	 * @param string $code - код заявки
	 *
	 * @return array|bool|false - вернет результат выборки
	 */
	public function getByCode( string $code ) {

		try {
			if ( strlen( $code ) != 32 ) {
				throw new \Exception( 'При попытке получения заявки по коду был некорректно указан код заявки' );
			}

			return Yii::$app->db
				->createCommand( "SELECT  
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `print_link`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `code` = :code AND `point_id` = :point_id", [
					':code'     => $code,
					':point_id' => ( ( new Users() )->getOne( Yii::$app->user->id ) )['point_id'],
				] )
				->queryOne();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
	/**
	 * Метод получения заявки по коду
	 *
	 * @param string $code - код заявки
	 *
	 * @return array|bool|false - вернет результат выборки
	 */
	public function getByCode1( string $code ) {

		try {
			if ( strlen( $code ) != 32 ) {
				throw new \Exception( 'При попытке получения заявки по коду был некорректно указан код заявки' );
			}

			return Yii::$app->db
				->createCommand( "SELECT  
  `r`.`id`,
  `r`.`auto_brand_id`,
  `auto_model_id`,
  `b`.`name`                                               AS `auto_brand_name`,
  `m`.`name`                                               AS `auto_model_name`,
  `auto_year`,
  `auto_price`,
  `summ`,
  `num`,
  `code`,
  `mode`,
  DATE_FORMAT(`date_return`, '%Y-%m-%dT00:00:00.000Z')     AS `date_return`,
  `credit_product_id`,
  `point_id`,
  `user_id`,
  `client_id`,
  `client_first_name`,
  `client_last_name`,
  `client_patronymic`,
  DATE_FORMAT(`client_birthday`, '%Y-%m-%dT00:00:00.000Z') AS `client_birthday`,
  `client_mobile_phone`,
  `client_email`,
  `client_passport_serial_number`,
  `client_passport_number`,
  `client_region_id`,
  `client_home_phone`,
  `client_employment_id`,
  `client_workplace_experience`,
  `client_workplace_period_id`,
  `client_workplace_address`,
  `client_workplace_phone`,
  `client_workplace_name`,
  `client_total_monthly_income`,
  `client_total_monthly_outcome`,
  `client_guarantor_name`,
  `client_guarantor_relation_id`,
  `client_guarantor_phone`,
  `method_of_issuance_id`,
  `card_number`,
  `bank_bik`,
  `checking_account`,
  `pre_crediting`,
  `accreditation_num`,
  `comment_client`,
  `comment_partner`,
  `print_link`,
  DATE_FORMAT(r.created_at, '%Y-%m-%dT00:00:00.000Z')      AS `created_at`
FROM `requests` AS `r`
  LEFT JOIN `auto_brands` AS `b` ON `r`.`auto_brand_id` = `b`.`id`
  LEFT JOIN `auto_models` AS `m` ON `r`.`auto_model_id` = `m`.`id`
WHERE `code` = :code", [
					':code'     => $code,

				] )
				->queryOne();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
	/**
	 * Метод создания заявки
	 *
	 * @param array $params - Параметры для создания заявки,
	 *                      можно использовать совместно с  $model->load
	 *
	 * @return bool|string|\Exception|$this - Вернет код(хэш) в случае успеха иначе ложь
	 *                                        текущий объект, если не пройдена валидация,
	 *                                        объект исключения, при выбросе исключения
	 */
	public function create( array $params = [] ) {

		$params['client_id'] = Yii::$app->user->id;

		return parent::create( $params );
	}

	/**
	 * Метод обновления заявки по коду(хэшу)
	 *
	 * @param string $code    - Код(хэш) заявки
	 * @param array  $params  - Параметры для обновления заявки
	 * @param bool   $prepare - флаг для подготовки данных
	 *
	 * @return bool|int - Вернет кол-во обновленных строк или выбросит исключение
	 */
	public function updateByCode( string $code, array $params = [], $prepare = true ) {

		$params['print_link_expired'] = new Expression( 'NOW()' );

		return parent::updateByCode( $code, $params, $prepare );
	}
}