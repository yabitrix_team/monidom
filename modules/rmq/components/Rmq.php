<?php
namespace app\modules\rmq\components;

use app\modules\app\v1\classes\logs\Log;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPExceptionInterface;
use PhpAmqpLib\Exception\AMQPInvalidArgumentException;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;
use yii\base\Component;

class Rmq extends Component
{
    /**
     * @var string Сервер
     */
    public $host = '127.0.0.1';
    /**
     * @var int Порт
     */
    public $port = 5672;
    /**
     * @var string Логин
     */
    public $login = 'guest';
    /**
     * @var string Пароль
     */
    public $password = 'guest';
    /**
     * @var string Виртуальный хост (адресное пространство внутри сервера)
     */
    public $vhost = '/';
    /**
     * @var string Название очереди
     */
    public $queueName;
    /**
     * @var bool Проверка на существование очереди с одинаковым названием
     */
    public $queuePassive = false;
    /**
     * @var bool Очередь сохраняется на диск и работает после перезагрузки сервера
     */
    public $queueDurable = true;
    /**
     * @var bool Очередь доступна только в одном канале
     */
    public $queueExclusive = false;
    /**
     * @var bool Очередь автоматически удаляется при закрытии канала
     */
    public $queueAutoDelete = false;
    /**
     * @var string Название обмена
     */
    public $exchangeName;
    /**
     * Отправка подписчикам:
     * * fanout - всем
     * * direct - по определенному ключу (routing key)
     * * topic - по маске ключа (topic)
     *
     * @var string Тип обмена
     */
    public $exchangeType;
    /**
     * @var bool Проверка на существование обмена с одинаковым названием
     */
    public $exchangePassive = false;
    /**
     * @var bool Обмен сохраняется на диск и работает после перезагрузки сервера
     */
    public $exchangeDurable = true;
    /**
     * @var bool Обмен автоматически удаляется при закрытии канала
     */
    public $exchangeAutoDelete = false;
    /**
     * @var string Идентификатор подписчика
     */
    public $consumerTag;
    /**
     * @var bool Получать сообщения отправленные текущим подписчиком
     */
    public $consumerNoLocal = false;
    /**
     * @var bool Автоматическое подтверждение получения пакетов
     */
    public $consumerNoAck = true;
    /**
     * @var bool Очередь доступна только одному подписчику
     */
    public $consumerExclusive = false;
    /**
     * @var bool Не ждать подтверждения от сервера очередей
     */
    public $consumerNoWait = false;
    /**
     * @var string|string[] Ключи привязки очереди к обмену
     */
    public $routingKey = [];
    /**
     * @var bool
     */
    public $debug = false;
    /**
     * @var AMQPStreamConnection
     */
    private $connection;
    /**
     * @var AMQPChannel
     */
    private $channel;

    private function importParams(array $params = [])
    {
        foreach ($params as $param) {
            if (array_key_exists($param, Yii::$app->params["core"]["rmq"])) {
                $this->$param = Yii::$app->params["core"]["rmq"][$param];
            }
        }
        $this->routingKey = is_array($this->routingKey) ?: [$this->routingKey];
    }

    public function init()
    {
        $this->importParams(["host", "login", "password", "port"]);

        try {
            $this->connection =
                new AMQPStreamConnection($this->host, $this->port, $this->login, $this->password, $this->vhost);
            $this->channel    = $this->connection->channel();
            parent::init();
        } catch (\Exception $e) {
            Log::error(["text" => $e->getMessage()], "rmq.init");
            $this->close();
        }
    }

    private function close()
    {
        if (is_a($this->channel, AMQPChannel::class)) {
            $this->channel->close();
        }
        if (is_a($this->channel, AMQPStreamConnection::class)) {
            $this->connection->close();
        }
    }

    private function logInfo(string $text)
    {

    }

    private function logError()
    {

    }

    public function publish(array $message)
    {
        try {
            $message = new AMQPMessage(json_encode($message, JSON_UNESCAPED_UNICODE));
            if (empty($this->exchangeName) && !empty($this->exchangeType)) {
                throw new AMQPInvalidArgumentException(
                    "Название обмена не может быть пустым, установите значение параметра exchangeName в настройках компонента"
                );
            }
            if (!empty($this->exchangeName)) {
                if (!in_array($this->exchangeType, ["fanout", "direct", "topic"])) {
                    throw new AMQPInvalidArgumentException(
                        "Недопустимый тип обмена exchangeType, поддерживаемые типы: fanout, direct, topic"
                    );
                }
                $this->channel->exchange_declare(
                    $this->exchangeName,
                    $this->exchangeType,
                    $this->exchangePassive,
                    $this->exchangeDurable,
                    $this->exchangeAutoDelete
                );
                if (!empty($this->routingKey)) {
                    foreach ($this->routingKey as $routingKey) {
                        $this->channel->basic_publish($message, $this->exchangeName, $routingKey);
                    }
                } else {
                    $this->channel->basic_publish($message, $this->exchangeName);
                }
                Log::info(
                    ["text" => print_r($message, 1)],
                    "rmq.publish"
                );
            } elseif (!empty($this->queueName)) {
                $this->channel->queue_declare(
                    $this->queueName,
                    $this->queuePassive,
                    $this->queueDurable,
                    $this->queueExclusive,
                    $this->queueAutoDelete
                );
                $this->channel->basic_publish($message, "", $this->queueName);
                Log::info(
                    ["text" => print_r($message, 1)],
                    "rmq.publish"
                );
            }
            $this->close();
        } catch (\Exception $e) {
            Log::error(["text" => $e->getMessage()], "rmq.publish");
            $this->close();
        }
    }

    public function listen(callable $callback)
    {
        try {
            if (!empty($this->queueName)) {
                $this->channel->queue_declare(
                    $this->queueName,
                    $this->queuePassive,
                    $this->queueDurable,
                    $this->queueExclusive,
                    $this->queueAutoDelete
                );
                $this->channel->basic_consume(
                    $this->queueName,
                    $this->consumerTag,
                    $this->consumerNoLocal,
                    $this->consumerNoAck,
                    $this->consumerExclusive,
                    $this->consumerNoWait,
                    function ($message) use ($callback) {
                        $logInfo = $message;
                        unset($logInfo->delivery_info['channel']);
                        Log::info(
                            ["text" => print_r($logInfo, 1)],
                            "rmq.listen"
                        );
                        $this->listenCallback($message, $callback);
                    }
                );
            }
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
            register_shutdown_function(
                function () {
                    $this->close;
                },
                $this->channel,
                $this->connection
            );
        } catch (\Exception $e) {
            Log::error(["text" => $e->getMessage()], "rmq.listen");
            $this->close();
        }
    }

    protected function listenCallback(AMQPMessage $message, callable $callback)
    {
        $message = json_decode($message->body, 1);
        if (is_array($message)) {
            try {
                Log::info(
                    ["text" => print_r($message, 1)],
                    "rmq.listen.callback"
                );
                $callback($message);
            } catch (\Exception $e) {
                Log::error(
                    ["text" => "Ошибка: ".$e->getMessage().",\nСообщение: ".print_r($message, 1)],
                    "rmq.listen.callback"
                );
            }
        } elseif (is_null($message)) {
            Log::error(
                ["text" => "Ошибка: ".json_last_error_msg().",\nСообщение: ".print_r($message, 1)],
                "rmq.listen.callback"
            );
        } else {
            Log::error(
                ["text" => "Ошибка: недопустимый формат,\nСообщение: ".print_r($message, 1)],
                "rmq.listen.callback"
            );
        }
    }
}