<?php
namespace app\modules\rmq\commands;

use app\modules\rmq\classes\CreditLimits;
use yii;
use yii\console\Controller;

/**
 * rmq manager
 *
 * @package app\modules\rmq\commands
 */
class ActionsController extends Controller
{
    /**
     * Starts exchange
     */
    public function actionListen()
    {
        $id = Yii::$app->queueMFOReceive->directAnonimousQueueListener();
    }

    public function actionListen2()
    {
        $id = Yii::$app->queueMFOSend->directAnonimousQueueListener();
    }

    public function actionListenCreditLimits()
    {
        Yii::$app->queueCreditLimits->directAnonimousQueueListener();
    }

    public function actionPublish()
    {
        $id = Yii::$app->queueMFOReceive->directAnonimousQueuePublisher(
            [
                // имя очереди инициатора
                "sourceRoutingKey" => "Foto",
                // инициирующая система система
                "sourceSystem"     => "mfo",
                // class, регистрозависимый
                "execute"          => "RmqMfoVerificatorsRequest",
                "params"           => [ //  параметры для обработки
                    "НомерЗаявки"  => "18112624720002",
                    "ИдЗапроса"    => "9ef543d1-01a6-4dfa-ab27-d5e50eceedf0",
                    "ТипДокумента" => "Паспорт",
                    "Комментарий"  => "Доработка фото Паспорта с КК ",
                ],
            ]
        );
    }

    public function actionPublish2()
    {
        $id = Yii::$app->queueMFOReceive->directAnonimousQueuePublisher(
            [
                // имя очереди инициатора
                "sourceRoutingKey" => "Foto",
                // инициирующая система система
                "sourceSystem"     => "mfo",
                // class, регистрозависимый
                "execute"          => "RmqMfoVerificatorsRequest",
                "params"           => [ //  параметры для обработки
                    "НомерЗаявки"  => "18112810640001",
                    "ИдЗапроса"    => "9ef543d1-01a6-4dfa-ab27-d5e50eceedf0",
                    "ТипДокумента" => "Паспорт",
                    "Комментарий"  => "Доработка фото Паспорта с КК 2",
                ],
            ]
        );
    }

    public function actionPublish3()
    {
        $id = Yii::$app->queueMFOSend->directAnonimousQueuePublisher(
            [
                // имя очереди инициатора
                "sourceRoutingKey" => "Foto",
                // инициирующая система система
                "sourceSystem"     => "mfo",
                // class, регистрозависимый
                "execute"          => "RmqMfoVerificatorsRequest",
                "params"           => [ //  параметры для обработки
                    "НомерЗаявки"  => "18112810640001",
                    "ИдЗапроса"    => "9ef543d1-01a6-4dfa-ab27-d5e50eceedf0",
                    "ТипДокумента" => "Паспорт",
                    "Комментарий"  => "Доработка фото Паспорта с КК 2",
                ],
            ]
        );
    }

    public function actionCreditLimitsPublish()
    {
        Yii::$app->queueCreditLimitsPublish->publish(
            [
                "data" => [
                    "phone"     => "9999999999",
                    "sum"       => "1000000",
                    "loanTime"  => "12",
                    "name"      => "ФАМИЛИЯ ИМЯ ОТЧЕСТВО",
                    "offerType" => "00000001",
                ],
            ]
        );
    }

    public function actionCreditLimitsListen()
    {
        Yii::$app->queueCreditLimitsListen->listen(
            function ($message) {
                CreditLimits::save($message);
            }
        );
    }
}