<?php

namespace app\modules\rmq\traits;

use app\modules\rmq\components\RmqBase;
use app\modules\rmq\models\RmqDataPacket;

/**
 * Created by PhpStorm.
 * User: ruffnec
 * Date: 27.09.2018
 * Time: 19:32
 */
trait RmqTrait {
	/**
	 * @var сообщение
	 */
	protected $message;
	/**
	 * @var id пакета в таблице rmq_data_packet
	 */
	protected $rmqDataPacletId;
	/**
	 * @var объект RMQ
	 */
	protected $envelope;

	/**
	 * Метод вызывается из класса BaseRmq, обработка сообщения
	 *
	 * @param array              $message - сообщение
	 * @param int|null           $rmqDataPacketId - id записи пакета в таблице rmq_data_packet
	 * @param \AMQPEnvelope|null $envelope - исходный пакет данных rmq, содержащий служебную информацию? в том числе и сообщение
	 *
	 * @return \Exception
	 */
	public function execute( array $message, int $rmqDataPacketId = null, \AMQPEnvelope $envelope = null ) {

		$this->message         = $message;
		$this->rmqDataPacletId = $rmqDataPacketId;
		$this->envelope        = $envelope;
		try {

			return $this->prepareExecute( $message, $rmqDataPacketId, $envelope );
		} catch ( \Exception $e ) {
			return $e;
		}
	}

	/**
	 * Отправка псообщения в RMQ
	 *
	 * @param RmqBase $component
	 * @param array   $params
	 *
	 * @return $this|\Exception
	 */
	public function response( RmqBase $component, array $params ) {

		try {
			/**
			 * проверка наличия необходимых полей
			 */
			if ( ! $this->checkResponseRequiredFields( $params ) ) {
				return $this;
			}
			$component->registerPublishCallBack( [ get_class( $this ), 'publishCallBack' ] );
			$packet = $this->prepareResponse( $params );
			$packet = $component->directAnonimousQueuePublisher( $packet );
			if ( is_a( $packet, \Exception::class ) ) {
				throw new \Exception( $packet->getMessage() );
			}

			return $packet;
		} catch ( \Exception $e ) {
			return $e;
		}
	}
	/**
	 * ф-я для предобработки пакета данных перед записью в БД, при прослушивании
	 * если функция определена, она должна возхвращать валидный пакет для записи
	 * если функция не определана, она долджна возвращать false
	 *
	 * @param array $message
	 *
	 * @return bool
	 */
	public function listenCallBack( array $message ) {

		return false;
	}

	/**
	 * ф-я для предобработки пакета данных перед записью в БД, при отправке
	 * если функция определена, она должна возхвращать валидный пакет для записи
	 * если функция не определана, она долджна возвращать false
	 *
	 * @param array $message
	 *
	 * @return bool
	 */
	public function publishCallBack( array $message ) {

		return false;
	}
	/**
	 * переменная для функции checkResponseRequiredFields()
	 *
	 * @var array
	 */
	protected function responseRequiredFields() {

		try {
			throw new \Exception( "ВНИМАНИЕ ! Обязательно реализовать функцию responseRequiredFields() в классе с массивом полей для контроля !!!" );

			// пример !!!
			return [
				'message_id' => 'message_id',
				'file_list'  => 'file_list',
			];
		} catch ( \Exception $e ) {
			return $e;
		}
	}

	/**
	 * @param array $needle
	 * @param array $haystack
	 *
	 * @return \Exception
	 */
	protected function checkResponseRequiredFields( array $haystack, array $needle = [] ) {

		try {
			$needle = ! empty( $needle ) ? $needle : $this->responseRequiredFields();
			// проверка необходимых полей
			foreach ( $needle as $protocolField ) {
				if ( ! array_key_exists( $protocolField, $haystack ) ) {
					throw new \Exception( "В пакете отсутвует обязательное поле '" . $protocolField . "'" );
				}
			}

			return true;
		} catch ( \Exception $e ) {
			return $e;
		}
	}

	/**
	 * ф-я формирования пакета из сохраненного пакета данных в БД rmq_data_packet
	 *
	 * @param int $id
	 */
	protected function formDataPacket( int $id, array $unset = [] ) {

		try {
			$RmqDataPacket = ( new RmqDataPacket() )->getOne( $id );
			if ( is_a( $RmqDataPacket, RmqDataPacket::class ) ) {
				throw new \Exception( "Ошибка выполнения " . print_r( $RmqDataPacket->firstErrors, 1 ) );
			}
			$packet = [];
			foreach ( RmqDataPacket::$fieldsToPacket as $dbField => $packetField ) {
				$packet[ $packetField ] = $RmqDataPacket[ $dbField ];
			}
			$packet['params'] = json_decode( $packet['params'], true ); // для наглядности
			foreach ( $unset as $value ) {
				unset( $packet['params'][ $value ] );
			}

			return $packet;
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}