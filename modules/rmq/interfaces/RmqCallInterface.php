<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\modules\rmq\interfaces;

use app\modules\rmq\components\RmqBase;
use yii\base\Component;

/**
 * RQM Call Interface.
 * интерфейс обработки сообщений RMQ
 *
 * @author ruffnec
 */
interface RmqCallInterface {

	/**
	 * @param array              $message         - сообщение по протоколу rmq JSON
	 * @param int                $rmqDataPacketId - id записи пакета в таблице rmq_data_packet
	 * @param \AMQPEnvelope|null $envelope        - объект RMQ
	 *
	 * @return mixed
	 */
	public function prepareExecute( array $message, int $rmqDataPacketId = null, \AMQPEnvelope $envelope = null );

	/**
	 * @param array              $message         - сообщение по протоколу rmq JSON
	 * @param int                $rmqDataPacketId - id записи пакета в таблице rmq_data_packet
	 * @param \AMQPEnvelope|null $envelope        - объект RMQ
	 *
	 * @return mixed
	 */
	public function execute( array $message, int $rmqDataPacketId = null, \AMQPEnvelope $envelope = null );

	/**
	 *
	 * ф-я в которой вы реализовывете формитирование пакета
	 * ф-я не вызывается напрямую.
	 * Вызывается через response с передачей параметров в $params
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function prepareResponse( array $params );

	/**
	 * ф-я, которую нужно вызвать для отправки сформированного пакета
	 *
	 * @param RmqBase $component
	 * @param array   $params
	 *
	 * @return mixed
	 */
	public function response( RmqBase $component, array $params );

	/**
	 * ф-я для предобработки пакета данных перед записью в БД, при прослушивании
	 * если функция определена, она должна возхвращать валидный пакет для записи
	 * если функция не определана, она долджна возвращать false
	 *
	 * @param array $message
	 *
	 * @return mixed
	 */
	public function listenCallBack( array $message );

	/**
	 * ф-я для предобработки пакета данных перед записью в БД, при отправке
	 * если функция определена, она должна возхвращать валидный пакет для записи
	 * если функция не определана, она долджна возвращать false
	 *
	 * @param array $message
	 *
	 * @return mixed
	 */
	public function publishCallBack( array $message );
}
