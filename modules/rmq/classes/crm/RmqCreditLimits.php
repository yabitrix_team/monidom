<?php
namespace app\modules\rmq\classes\crm;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\CreditLimit;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageAttributes;
use app\modules\notification\models\MessageType;
use app\modules\rmq\components\RmqBase;
use app\modules\rmq\interfaces\RmqCallInterface;
use app\modules\rmq\models\RmqDataPacket;
use app\modules\rmq\traits\RmqTrait;
use Exception;
use Yii;
use yii\base\Component;

class RmqCreditLimits implements RmqCallInterface
{
    use RmqTrait;
    /**
     * сопопставление типов 1С и WEB
     *
     * @var array
     */
    protected static $docType = [
        'foto_passport' => 'Паспорт',
        'foto_pts'      => 'ПТС',
        'foto_sts'      => 'СТС',
        'foto_auto'     => 'ФотографияАвтомобиля',
        'foto_client'   => 'ФотографияКлиента',
    ];
    /**
     * сопоставление названий полей 1С и БД
     *
     * @var array
     */
    protected static $messageAttributes = [
        'kk_id'    => 'ИдЗапроса',
        'doc_type' => 'ТипДокумента',
    ];

    /**
     * ф-я вызывается при получении пакета данных из очереди.
     * здесь вы обрабатываете полученное сообщение.
     * так же здесь вы можете отправить сообщение в этот обменник или в любой другой, в соответствии с конфигурацией.
     *
     * @param array              $message         - сообщение
     * @param int|null           $rmqDataPacketId - id записи пакета в таблице rmq_data_packet
     * @param \AMQPEnvelope|null $envelope        - исходный пакет данных rmq, содержащий служебную информацию? в том
     *                                            числе и сообщение
     *
     * @return \Exception|int|mixed
     */
    public function prepareExecute(array $message, int $rmqDataPacketId = null, \AMQPEnvelope $envelope = null)
    {
        try {
            $package = $this->message;
            $model   = new CreditLimit();
            $model->load(
                [
                    'phone'      => $package['data']['phone'],
                    'amount'     => $package['data']['sum'],
                    'period'     => $package['data']['loanTime'],
                    'full_name'  => $package['data']['name'],
                    'offer_type' => $package['data']['offerType'],
                ],
                ''
            );
            $model->save();
            Log::info(
                [
                    "text" => print_r($package, 1),
                ],
                'rmq.creditlimit'
            );
            return 1;
        } catch (Exception $e) {
            Log::error(
                [
                    "text" => $e->getMessage(),
                ],
                'rmq.creditlimit'
            );
            return $e;
        }
    }

    /**
     * в функции вы обязаны сформировать валидный пакет данных для отправки
     *
     * @param array $params
     *
     * @return array|\Exception|mixed
     * @throws \Exception
     */
    public function prepareResponse(array $params)
    {
        $messAttr = new MessageAttributes(['scenario' => MessageAttributes::SCENARIO_LIST]);
        $messAttr->load(['message_id' => $params['message_id']], '');
        $messAttrList = $messAttr->getAttrList();
        if (is_a($messAttrList, MessageAttributes::class)) {
            throw new Exception("Ошибка валидации ".print_r($messAttrList->firstErrors, 1));
        }
        $packet                           = $this->formDataPacket($messAttrList['rmq_data_packet_id'], ['Комментарий']);
        $packet['params']['Комментарий']  = $params['message'];
        $packet['params']['МассивФайлов'] = [];
        $fileList                         = $params['file_list'];
        if (!empty($fileList)) {
            $rcFile = new File();
            foreach ($fileList as $file) {
                $arFile                             = $rcFile->getOne($file);
                $pathOriginFile                     =
                    rtrim(Yii::getAlias('@webroot'), '/').'/'.trim($arFile['path'], '/');
                $extension                          =
                    strtolower(pathinfo(parse_url($pathOriginFile)['path'], PATHINFO_EXTENSION));
                $packet['params']['МассивФайлов'][] = [
                    'ИмяФайла'        => str_replace('.'.$extension, '', $arFile['name']),
                    'РасширениеФайла' => $extension,
                    'СодержимоеФайла' => base64_encode(file_get_contents($pathOriginFile)) // base64
                ];
            }
        };

        return $packet;
    }

    /**
     * массив обязательных полей в $params checkResponseRequiredFields()
     * вызов функции отправки сообщения будет проверен на налицие этих обязательных полей.
     * пустой массив - без проверки
     *
     * @var array
     */
    protected function responseRequiredFields()
    {
        return [
            'message_id' => 'message_id',
            'file_list'  => 'file_list',
        ];
    }

    /**
     * подготовить массив для записи сообщения
     *
     * @param $arr
     *
     * @return mixed
     */
    protected function prepareData(array $arr = [])
    {
        $arr              = empty($arr) ? $this->message : $arr;
        $data['code']     = $arr['params']['НомерЗаявки'];
        $data['message']  = $arr['params']['Комментарий'];
        $data['parentId'] = $arr['params']['НомерСообщения'];
        $data['kkId']     = $arr['params']['ИдЗапроса'];
        $data['docType']  = (array_flip(self::$docType))[$arr['params']['ТипДокумента']];

        return $data;
    }

    /**
     * подготовить поля для записи атрибутов сообщения
     *
     * @param array $arr
     *
     * @return mixed
     */
    protected function prepareDataMessageAttributes($message_id, array $arr = [])
    {
        $arr = empty($arr) ? $this->message : $arr;
        foreach (self::$messageAttributes as $key => $val) {
            $result[$key] = $arr['params'][$val];
        }
        $result['message_id']         = $message_id;
        $result['rmq_data_packet_id'] = $this->rmqDataPacletId;

        return $result;
    }

    /**
     * ф-я для предобработки пакета данных перед записью в БД, при отправке
     * если функция определена, она должна возхвращать валидный пакет для записи
     * если функция не определана, она долджна возвращать false
     *
     * @param array $message
     *
     * @return mixed
     */
    public function publishCallBack(array $message)
    {
        if (is_array($message)) {
            $arrMessage = $message;
            $arrParams  = json_decode($arrMessage['params'], 1);
            foreach ($arrParams['МассивФайлов'] as $item) {
                $item['СодержимоеФайла'] = "base64_data";
                $mf[]                    = $item;
            }
            $arrParams['МассивФайлов'] = $mf;
            $arrMessage['params']      = json_encode($arrParams, JSON_UNESCAPED_UNICODE);

            return $arrMessage;
        }

        return false;
    }
}