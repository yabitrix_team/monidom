<?php
namespace app\modules\rmq\classes;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\CreditLimit;

class CreditLimits
{
    public static function save(array $message)
    {
        try {
            $model = new CreditLimit();
            $model->load(
                [
                    'phone'      => $message['data']['phone'],
                    'amount'     => $message['data']['sum'],
                    'period'     => $message['data']['loanTime'],
                    'full_name'  => $message['data']['name'],
                    'offer_type' => $message['data']['offerType'],
                ],
                ''
            );
            $model->save();
            Log::info(
                [
                    "text" => print_r($message, 1),
                ],
                'rmq.creditlimit'
            );

            return 1;
        } catch (\Exception $e) {
            Log::error(
                [
                    "text" => $e->getMessage(),
                ],
                'rmq.creditlimit'
            );

            return $e;
        }
    }
}