<?php
namespace app\modules\rmq;

use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;
use yii\console\Application;

/**
 * Exchange module definition class
 */
class Module extends BaseModule implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\rmq\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $this->controllerNamespace = 'app\modules\rmq\commands';
        }
    }
}
