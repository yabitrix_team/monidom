<?php

namespace app\modules\mock\v1;
/**
 * Class Module - модуль для работы с Серивсами имитации
 *
 * @package app\modules\mock\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\mock\v1\controllers';
}
