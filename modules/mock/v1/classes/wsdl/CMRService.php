<?php

namespace app\modules\mock\v1\classes\wsdl;

use app\modules\mock\v1\classes\Tools;
use app\modules\mock\v1\models\Products;
use SoapFault;

/**
 * Class CMRService - класс генерации контента wsdl для сервиса CMRService
 *
 * @package app\modules\mock\v1\classes\wsdl
 */
class CMRService {
	/**
	 * @var int - длина строки идентификатора кредитного продукта, аналогично боевому сервису
	 */
	protected $lengthCreditProductID = 9;

	/**
	 * Метод имитации функции "Получение общих условий по кредитным продуктам"
	 *
	 * @param string $ClientID   - идентификатор клиента
	 * @param array  $Parameters - набор дополнительных параметров
	 *
	 * @return mixed
	 * @throws SoapFault
	 * @soap
	 **/
	public function GetCalcOptions( $ClientID, $Parameters ) {

		try {
			$obj     = new \stdClass();
			$err     = new \stdClass();
			$product = new Products();
			$res     = $product->getOptions();
			if ( is_string( $res ) ) {
				throw new \Exception( $res );
			}
			if ( empty ( $res ) ) {
				throw new \Exception( 'Ошибка в подготовке опций кредитных продуктов, обратитесь к администратору' );
			}
			foreach ( $res as $k => $item ) {
				$calcOptions          = new \stdClass();
				$calcOptions->Periods = intval( $item['month'] );
				$calcOptions->MinSum  = intval( $item['minSum'] );
				$calcOptions->MaxSum  = intval( $item['maxSum'] );
				$calcOptions->Step    = 1000;
				$obj->CalcOptions[]   = $calcOptions;
			}
			$err->ID          = 1;
			$err->Description = 'OK';
			$obj->ErrorData   = $err;

			return $obj;
		} catch ( \Exception $e ) {
			throw new SoapFault( __FUNCTION__, 'Не удалось получить опции кредитных продуктов: ' . $e->getMessage() );
		}
	}

	/**
	 * Метод имитации функции "Расчет полного и короткого графиков платежей"
	 *
	 * @param string $CreditProductId - код кредитного продукта
	 * @param float  $Sum             - сумма займа
	 * @param string $StartDate       - дата выдачи займа
	 * @param string $ReturnDate      - желаемая дата возврат займа
	 *
	 * @return mixed
	 * @throws SoapFault
	 * @soap
	 */
	public function GetPaymentChartv2( $CreditProductId, $Sum, $StartDate, $ReturnDate ) {

		try {
			$obj = new \stdClass();
			$err = new \stdClass();
			if ( strlen( $CreditProductId ) != $this->lengthCreditProductID ) {
				throw new \Exception( 'Идентификатор кредитного продукта должен быть длиной в ' . $this->lengthCreditProductID . ' символов' );
			}
			$productId = ltrim( $CreditProductId, '0' );
			$product   = new Products();
			$product->load( [
				                'sum'  => $Sum,
				                'date' => $ReturnDate,
			                ], '' );
			if ( ! $product->validate() ) {
				if ( key_exists( 'sum', $product->firstErrors ) ) {
					$err->ID          = - 2;
					$err->Description = $product->firstErrors['sum'];
				}
				if ( key_exists( 'date', $product->firstErrors ) ) {
					$err->ID          = - 1;
					$err->Description = $product->firstErrors['date'];
				}
			}
			$res = $product->getOne( $productId, [ 'guid', 'month_num', 'percent_per_day' ] );
			if ( is_string( $res ) ) {
				throw new \Exception( $res );
			}
			if ( empty( $res ) ) {
				$err->ID          = 300;
				$err->Description = 'Не найден кредитный продукт';
			}
			if ( empty( $err->ID ) ) {
				$res = reset( $res );
				//установка полного и короткого графика
				$resSchedule = $this->setSchedulePayment(
					$Sum,
					$ReturnDate,
					$res['percent_per_day'],
					$res['month_num'],
					$res['guid'],
					$obj
				);
				if ( is_string( $resSchedule ) ) {
					throw new \Exception( $resSchedule );
				}
				$err->ID          = 1;
				$err->Description = 'OK';
			}
			$obj->error = $err;

			return $obj;
		} catch ( \Exception $e ) {
			throw new SoapFault( __FUNCTION__, 'Не удалось получить график платежей: ' . $e->getMessage() );
		}
	}

	/**
	 * Метод имитации функции "Подбор кредитного продукта по параметрам с получением полного и короткого графика"
	 *
	 * @param float   $Sum             - сумма займа
	 * @param boolean $RegularCustomer - Признак "Повторный клиент"
	 * @param boolean $Recredit        - Признак "Докредитование"
	 * @param string  $ReturnDate      - желаемая дата возврат займа
	 *
	 * @return mixed
	 * @throws SoapFault
	 * @soap
	 */
	public function SmartPaymentChart( $Sum, $RegularCustomer, $Recredit, $ReturnDate ) {

		try {
			$obj     = new \stdClass();
			$err     = new \stdClass();
			$product = new Products();
			$product->load( [
				                'sum'  => $Sum,
				                'date' => $ReturnDate,
			                ], '' );
			$res = $product->getByParams();
			if ( is_string( $res ) ) {
				throw new \Exception( $res );
			}
			if ( empty( $res ) ) {
				$err->ID          = 300;
				$err->Description = 'Невозможно подобрать продукт по заданным параметрам';
			}
			if ( is_a( $res, Products::class ) ) {
				if ( key_exists( 'sum', $res->firstErrors ) ) {
					$err->ID          = - 2;
					$err->Description = $res->firstErrors['sum'];
				}
				if ( key_exists( 'date', $res->firstErrors ) ) {
					$err->ID          = - 1;
					$err->Description = $res->firstErrors['date'];
				}
			}
			if ( empty( $err->ID ) ) {
				//установка полного и короткого графика
				$resSchedule = $this->setSchedulePayment(
					$Sum,
					$ReturnDate,
					$res['percent_per_day'],
					$res['month_num'],
					$res['guid'],
					$obj
				);
				if ( is_string( $resSchedule ) ) {
					throw new \Exception( $resSchedule );
				}
				$err->ID          = 1;
				$err->Description = 'OK';
			}
			$obj->error = $err;

			return $obj;
		} catch
		( \Exception $e ) {
			throw new SoapFault( __FUNCTION__, 'Не удалось получить график платежей: ' . $e->getMessage() );
		}
	}

	/**
	 * Метод установки графиков платежей(полный и короткий) в результирующий объект
	 *
	 * @param           $sum         - сумма займа
	 * @param           $date        - срок займа
	 * @param           $prodPercent - процентная ставка кредитного продукта
	 * @param           $prodMonth   - срок займа кредитного продукта в месяцах
	 * @param           $prodGuid    - ГУИД кредитного продукта
	 * @param \stdClass $obj         - объект, который нужно дополнить графиками платежей
	 *
	 * @return bool|string - вернет истину в случае успеха
	 *                     при исключении вернет строку
	 */
	protected function setSchedulePayment( $sum, $date, $prodPercent, $prodMonth, $prodGuid, \stdClass &$obj ) {

		try {
			//расчет полного и короткого графика платежей
			$dateNow    = new \DateTime( date( 'Y-m-d 00:00:00' ) );
			$percentDay = floatval( $prodPercent ) * intval( $sum ) / 100;
			//-------------
			$dateEndFull     = ( clone $dateNow )->add( new \DateInterval( 'P' . $prodMonth . 'M' ) );
			$intervalFull    = $dateNow->diff( $dateEndFull );
			$periodFull      = $intervalFull->format( '%a' );
			$totalAmountFull = $sum + $percentDay * $periodFull;
			$annuity         = ceil( $totalAmountFull / $prodMonth );
			$scheduleFull    = Tools::generateSchedule( $prodMonth, $totalAmountFull );
			if ( is_string( $scheduleFull ) ) {
				throw new \Exception( 'Не удалось сгенерировать полный график платежей - ' . $scheduleFull );
			}
			//-------------
			$intervalEarly    = $dateNow->diff( new \DateTime( $date ) );
			$periodEarly      = $intervalEarly->format( '%a' );
			$totalAmountEarly = $sum + $percentDay * $periodEarly;
			$monthEarly       = ceil( $periodEarly / 30 );
			$scheduleEarly    = Tools::generateSchedule( $monthEarly, $totalAmountEarly );
			if ( is_string( $scheduleEarly ) ) {
				throw new \Exception( 'Не удалось сгенерировать короткий график платежей - ' . $scheduleEarly );
			}
			//формирование результирующего объекта
			$obj->product            = $prodGuid;
			$obj->period             = intval( $periodEarly );
			$obj->annuity            = $annuity;
			$obj->full               = new \stdClass();
			$obj->full->totalAmount  = $totalAmountFull;
			$obj->full->schedule     = $scheduleFull;
			$obj->early              = new \stdClass();
			$obj->early->totalAmount = $totalAmountEarly;
			$obj->early->schedule    = $scheduleEarly;

			return true;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}