<?php
namespace app\modules\mock\v1\classes\wsdl;

use app\modules\mock\v1\classes\Tools;
use app\modules\mock\v1\models\Contracts;
use SoapFault;

/**
 * Class AgreementsData - класс генерации контента wsdl для сервиса AgreementsData
 *
 * @package app\modules\mock\v1\classes\wsdl
 */
class AgreementsData
{
    /**
     * @var bool - признак Полной Стоимости Кредита
     *          если указана истина, то возвращается PSK в договорах
     */
    protected $psk = false;

    /**
     * Метод имитации функции "Информация по активному договору"
     *
     * @param string $AgreementString - номер или номера договоров разделенных запятой
     *
     * @return mixed
     * @throws SoapFault
     * @soap
     */
    public function ActiveAgreementsInfo($AgreementString)
    {

        return $this->agreements($AgreementString, [$this, 'activeAgreements'], 'ActiveAgreementInfo');
    }

    /**
     * Метод имитации функции "Информация по активному договору"
     *
     * @param string $AgreementString - номер или номера договоров разделенных запятой
     *
     * @return mixed
     * @throws SoapFault
     * @soap
     */
    public function ActiveAgreementsInfov2($AgreementString)
    {
        $this->psk = true;

        return $this->agreements($AgreementString, [$this, 'activeAgreements'], 'ActiveAgreementInfo');
    }

    /**
     * Метод имитации функции "Информация по закрытому договору"
     *
     * @param string $AgreementString - номер или номера договоров разделенных запятой
     *
     * @return mixed
     * @throws SoapFault
     * @soap
     **/
    public function ClosedAgreementsInfo($AgreementString)
    {
        $prepareAgreements = function (array $res) {
            $closedAgreementInfo = [];
            foreach ($res as $item) {
                $date                    = new \DateTime($item['date_start']);
                $interval                = $date->diff(new \DateTime($item['date_return']));
                $period                  = $interval->format('%a');
                $percentDay              = floatval($item['percent_per_day']) * $item['summ'] / 100;
                $totalAmount             = $item['summ'] + $percentDay * $period;
                $month                   = ceil($period / 30);
                $paymentsList            = Tools::generateSchedule(
                    $month,
                    $totalAmount,
                    $date,
                    [
                        'date'    => 'Date',
                        'annuity' => 'Summ',
                    ]
                );
                $agreement               = new \stdClass();
                $agreement->Number       = $item['code'];
                $agreement->Date         = $item['date_start'];
                $agreement->Summ         = $item['summ'];
                $agreement->CloseDate    = end($paymentsList)->Date;
                $agreement->PaymentsList = $paymentsList;
                $closedAgreementInfo[]   = $agreement;
            }

            return $closedAgreementInfo;
        };

        return $this->agreements($AgreementString, $prepareAgreements, 'ClosedAgreementInfo');
    }

    /**
     * Метод формирования ответа сервисов
     *
     * @param          $AgreementString   - номер или номера договоров разделенных запятой
     * @param callable $prepareAgreements - функция обратного вызова для формирования данных ответа сервиса
     * @param string   $response          - элемент массива результата
     *
     * @return \stdClass - вернет объект сервиса
     * @throws SoapFault - в случае ошибки выбросит исключение SoapFault
     */
    protected function agreements($AgreementString, callable $prepareAgreements, string $response)
    {
        try {
            $obj     = new \stdClass();
            $err     = new \stdClass();
            $errId   = 1;
            $errDesc = 'OK';
            $msg     = 'Ok';
            $res     = (new Contracts())->getAgreements($AgreementString, true);
            if (is_string($res)) {
                throw new \Exception($res);
            }
            if (empty($res)) {
                $msg     = 'Error';
                $errId   = 3;
                $errDesc = 'Договора не найдены';
            }
            if ($errId == 1) {
                //проверка на номера договоров отсутствующих в системе
                $arAgreements    = explode(',', $AgreementString);
                $countAgreements = strpos($AgreementString, ',') !== false ? count($arAgreements) : 1;
                if ($countAgreements > 1 && count($res) != $countAgreements) {
                    $codes = array_column($res, 'code');
                    array_walk(
                        $arAgreements,
                        function (&$v) {

                            $v = trim($v);
                        }
                    );
                    $failCodes = array_diff($arAgreements, $codes);
                    $msg       = implode(',', $failCodes);
                    $errId     = 2;
                    $errDesc   = 'Обращение отработано, но отсутствует ряд договоров';
                }
                $obj->{$response} = $prepareAgreements($res);
            }
            $err->ID          = $errId;
            $err->Description = $errDesc;
            $obj->ErrorData   = $err;
            $obj->MessageText = $msg;

            return $obj;
        } catch (\Exception $e) {
            throw new SoapFault(__FUNCTION__, 'Не удалось получить договора, ошибка: '.$e->getMessage());
        }
    }

    /**
     * Метод формирования тела активных договоров
     *
     * @param array $res - массив договоров
     *
     * @return array - вернет массив объектов активных договоров
     */
    protected function activeAgreements(array $res)
    {
        $activeAgreementInfo = [];
        foreach ($res as $item) {
            $date                          = new \DateTime($item['date_start']);
            $interval                      = $date->diff(new \DateTime($item['date_return']));
            $period                        = $interval->format('%a');
            $percentDay                    = floatval($item['percent_per_day']) * $item['summ'] / 100;
            $totalAmount                   = $item['summ'] + $percentDay * $period;
            $month                         = ceil($period / 30);
            $chart                         = Tools::generateSchedule(
                $month,
                $totalAmount,
                $date,
                [
                    'date'    => 'Date',
                    'annuity' => 'PaymentSumm',
                    'debt'    => 'Debt',
                    'percent' => 'Percents',
                    'rest'    => 'DebtRest',
                ]
            );
            $agreement                     = new \stdClass();
            $agreement->Number             = $item['code'];
            $agreement->Date               = $item['date_start'];
            $agreement->Summ               = $item['summ'];
            $agreement->CurrentPaymentSumm = 0;
            $curDate                       = date('Y-m-d');
            //подготовка текущих данных оплаты относительно графика
            foreach ($chart as $v) {
                if ($curDate < $v->Date) {
                    if (!empty($this->psk)) {
                        $agreement->CurrentPaymentSumm = $v->PaymentSumm + 494.35;
                        $agreement->NextPaymentSumm    = $agreement->CurrentPaymentSumm * 2 - 326;
                        $agreement->NextPaymentDate    = $v->Date;
                        $agreement->FullPaymentSumm    = $v->DebtRest;
                        $agreement->Annyity            = $v->PaymentSumm;
                    } else {
                        $agreement->CurrentPaymentSumm = $v->PaymentSumm;
                        $agreement->CurrentPaymentDate = $v->Date;
                        $agreement->Rest               = $v->DebtRest;
                    }
                    break;
                }
            }
            if (!empty($this->psk)) {
                $agreement->Percent = 88.2;
                $agreement->PSK     = 88.118;
            }
            $agreement->Chart      = $chart;
            $activeAgreementInfo[] = $agreement;
        }

        return $activeAgreementInfo;
    }
}