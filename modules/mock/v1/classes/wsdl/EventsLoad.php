<?php

namespace app\modules\mock\v1\classes\wsdl;

use app\modules\app\v1\models\EventsCrm;
use app\modules\app\v1\models\Requests;
use SoapFault;

/**
 * Class EventsLoad - класс генерации контента wsdl для сервиса EventsLoad
 *
 * @package app\modules\mock\v1\classes\wsdl
 */
class EventsLoad {
	/**
	 * Метод имитации функции "Получение события по заявке"
	 *
	 * @param string $Name        - ГУИД события CRM таблица events_crm (обязательный)
	 * @param string $ID          - ГУИД заявки (обязательный для получения события по заявке)
	 * @param string $PhoneNumber - номер телефона клиента (обязательный для получения события по клиенту)
	 * @param string $DateTime    - дата и время события
	 * @param array  $Param       - дополнительная информация по событию
	 *
	 * @return mixed
	 * @throws SoapFault
	 * @soap
	 **/
	public function eventsLoadBit( $Name, $ID, $PhoneNumber, $DateTime, $Param ) {

		try {

			$obj      = new \stdClass();
			$err      = new \stdClass();
			$errId    = 0;
			$errDesc  = [
				0    => '',
				1    => 'Не найдено событие',
				2    => 'Не найдена заявка',
				1000 => 'Передан короткий номер телефона (меньше 10 символов)',
			];
			$msg      = 'Success';
			$eventCrm = ( new EventsCrm() )->getByGuid( $Name );
			$request  = ( new Requests() )->getByGuid( $ID );
			if ( empty( $eventCrm ) || empty( $request ) || strlen( $PhoneNumber ) < 10 ) {
				$errId = empty( $eventCrm ) ? 1 : ( empty( $request ) ? 2 : 1000 );
				$msg   = 'Error';
			}
			$err->ID          = $errId;
			$err->Description = $errDesc[ $errId ];
			$obj->ErrorData   = $err;
			$obj->MessageText = $msg;

			return $obj;
		} catch ( \Exception $e ) {
			throw new SoapFault( __FUNCTION__, 'Не удалось получить событие по заявке: ' . $e->getMessage() );
		}
	}
}