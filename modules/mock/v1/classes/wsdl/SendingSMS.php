<?php
namespace app\modules\mock\v1\classes\wsdl;

use SoapFault;

/**
 * Class SendingSMS - класс генерации контента wsdl для сервиса SendingSMS
 *
 * @package app\modules\mock\v1\classes\wsdl
 */
class SendingSMS
{
    /**
     * @var int - длина строки идентификатора СМС
     */
    protected $lengthSmsId = 36;

    /**
     * Метод имитации функции "Получение статуса СМС"
     *
     * @param string $SmsId - идентификатор СМС полученный при отправке
     *
     * @return mixed
     * @throws SoapFault
     * @soap
     **/
    public function GetStatus($SmsId)
    {
        try {
            $obj = new \stdClass();
            $err = new \stdClass();
            if (strlen($SmsId) != $this->lengthSmsId) {
                $err->ID          = 505;
                $err->Description = 'Некорректный идентификатор СМС';
            }
            if (substr($SmsId, 0, 2) == 'nf') {
                $err->ID          = -1;
                $err->Description = 'Произошла неопределенная ошибка';
            }
            if (substr($SmsId, 0, 5) == 'nfsms') {
                $err->ID          = 506;
                $err->Description = 'Не нейдена СМС с указанным идентификатором';
            }
            if (empty($err->ID)) {
                $obj->MessageText  = 'OK';
                $obj->ProviderCode = 'send';
                $err->ID           = 1;
                $err->Description  = 'OK';
            }
            $obj->ErrorData = $err;

            return $obj;
        } catch (\Exception $e) {
            throw new SoapFault(__FUNCTION__, 'Не удалось получить статус СМС: '.$e->getMessage());
        }
    }

    /**
     * Метод имитации функции "Отправка простой смс по номеру телефона"
     *
     * @param string $Phone        - номер телефона
     * @param string $Message      - текст СМС
     * @param string $TestLogin    - логин для прокси для отправки смс в тестовом контуре(необязательный)
     * @param string $TestPassword - пароль для прокси для отправки смс в тестовом контуре(необязательный)
     *
     * @return mixed
     * @throws SoapFault
     * @soap
     **/
    public function SendData($Phone, $Message, $TestLogin, $TestPassword)
    {
        try {
            $obj = new \stdClass();
            $err = new \stdClass();
            if (strlen($Phone) != 10) {
                $err->ID          = 501;
                $err->Description = 'Телефонный номер слишком короткий';
            }
            if (!preg_match('/^\d+$/', $Phone)) {
                $err->ID          = 500;
                $err->Description = 'Телефон содержит не только цифры';
            }
            if (empty(trim($Message))) {
                $err->ID          = 502;
                $err->Description = 'Пустое тело СМС';
            }
            if ($Message == 'not_found') {
                $err->ID          = -1;
                $err->Description = 'Произошла неопределенная ошибка';
            }
            if (empty($err->ID)) {
                $alpha   = range('A', 'Z');
                $digit   = range(0, count($alpha));
                $arMerge = array_merge($alpha, $digit);
                $str     = strtolower(str_shuffle(implode('', $arMerge)));
                // пример сформированнаго ГУИД-а: 87215411-c863-11e8-8106-00155dc83d04
                $guid              = [
                    mt_rand(11111111, 99999999),
                    substr($str, 0, 4),
                    substr($str, 5, 4),
                    substr($str, 10, 4),
                    substr($str, 15, 12),
                ];
                $obj->SmsID        = implode('-', $guid);
                $obj->MessageText  = 'OK';
                $err->ID           = 1;
                $err->Description  = 'OK';
            }
            $obj->ErrorData = $err;

            return $obj;
        } catch (\Exception $e) {
            throw new SoapFault(__FUNCTION__, 'Не удалось отправить СМС: '.$e->getMessage());
        }
    }
}