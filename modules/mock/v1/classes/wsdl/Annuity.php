<?php

namespace app\modules\mock\v1\classes\wsdl;

use app\modules\mock\v1\models\Products;
use SoapFault;

/**
 * Class Annuity - класс генерации контента wsdl для сервиса Annuity
 *
 * @package app\modules\mock\v1\classes\wsdl
 */
class Annuity {
	/**
	 * @var int - длина строки идентификатора кредитного продукта, аналогично боевому сервису
	 */
	protected $lengthCreditProductID = 9;

	/**
	 * Метод имитации функции "Умное получение платежа и идентификатора кредитного продукта"
	 *
	 * @param float  $Sum        - сумма по займу
	 * @param string $Date       - дата ожидаемого погашения
	 * @param string $ClientID   -  идентификатор клиента
	 * @param array  $Parameters - массив доп.параметров, влияющих на расчет
	 *
	 * @return mixed
	 * @throws SoapFault
	 * @soap
	 **/
	public function GetSmartAnnuity( $Sum, $Date, $ClientID, $Parameters ) {

		try {

			$obj     = new \stdClass();
			$err     = new \stdClass();
			$product = new Products();
			$product->load( [
				                'sum'  => $Sum,
				                'date' => $Date,
			                ], '' );
			$res = $product->getByParams( 0, '', [ 'id', 'guid', 'month_num' ] );
			if ( is_string( $res ) ) {
				throw new \Exception( $res );
			}
			if ( empty( $res ) ) {
				$err->ID          = - 3;
				$err->Description = 'Невозможно подобрать продукт по заданным параметрам';
			}
			if ( is_a( $res, Products::class ) ) {
				if ( key_exists( 'sum', $res->firstErrors ) ) {
					$err->ID          = - 2;
					$err->Description = $res->firstErrors['sum'];
				}
				if ( key_exists( 'date', $res->firstErrors ) ) {
					$err->ID          = - 1;
					$err->Description = $res->firstErrors['date'];
				}
			}
			if ( empty( $err->ID ) ) {
				$creditProductId      = str_pad( $res['id'], $this->lengthCreditProductID, '0', STR_PAD_LEFT );
				$obj->Annuity         = strval( ceil( $Sum / $res['month_num'] ) );
				$obj->GUID            = $res['guid'];
				$obj->CreditProductID = $creditProductId;
				$err->ID              = 1;
				$err->Description     = 'OK';
			}
			$obj->ErrorData = $err;

			return $obj;
		} catch ( \Exception $e ) {
			throw new SoapFault( __FUNCTION__, 'Не удалось получить расчет аниутета: ' . $e->getMessage() );
		}
	}
}