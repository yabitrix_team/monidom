<?php

namespace app\modules\mock\v1\classes;

use app\modules\app\v1\classes\Tools as ToolsApp;

/**
 * Class Tools - класс содержащий дополнительные методы
 *
 * @package app\modules\mock\v1\classes
 */
class Tools extends ToolsApp {
	/**
	 * Метод генерации графика платежей
	 * в зависимости от параметров может генерировать полный или короткий график
	 *
	 * @param                $month       - срок займа в месяцах
	 * @param                $totalAmount - полная сумма займа с учетом процентного начисления
	 * @param \DateTime|null $dateStart   - объект DateTime содержащий дату начала займа
	 * @param array          $assocFields - ассоциации названия полей для выходного массива, по умолчанию:
	 *                                    [
	 *                                    'date'    => 'date',
	 *                                    'annuity' => 'total',
	 *                                    'debt'    => 'debt',
	 *                                    'percent' => 'percent',
	 *                                    'rest'    => 'rest',
	 *                                    ]
	 *                                    если параметр не будет указан, то примет ассоциации по умолчанию,
	 *                                    можно ограничить вывод в результирующем массиве, указав не все ассоциации
	 *
	 * @return array|string - вернет массив содержащий объекты данных графика платежей
	 *                      при исключении вернет строку
	 */
	public static function generateSchedule( $month, $totalAmount, \DateTime $dateStart = null, array $assocFields = [] ) {

		try {
			//ассоциации полей по умолчанию
			$defaultAssocFields = [
				'date'    => 'date',
				'annuity' => 'total',
				'debt'    => 'debt',
				'percent' => 'percent',
				'rest'    => 'rest',
			];
			$fields             = empty( $assocFields ) ? $defaultAssocFields :
				array_intersect_key( $assocFields, $defaultAssocFields );
			if (
				empty( $fields )
				|| ( ! empty( $assocFields ) && count( $fields ) != count( $assocFields ) )
			) {
				throw new \Exception( 'Некорректно указаны ассоциации полей в методе генерации графика платежей' );
			}
			$annuity    = ceil( $totalAmount / $month );
			$arSchedule = [];
			for ( $i = 1; $i <= $month; $i ++ ) {
				$dateNow      = empty( $dateStart ) ? new \DateTime( date( 'Y-m-d 00:00:00' ) ) : clone $dateStart;
				$dateSchedule = $dateNow->add( new \DateInterval( 'P' . $i . 'M' ) );
				if ( $i == $month ) {
					$balance = $totalAmount - $annuity * $i;
					$annuity = $balance > 0 ? $balance : $annuity;
					$debPerc = round( $annuity / 2, 2 );
					$rest    = 0;
				} else {
					$debPerc = round( $annuity / 2, 2 );
					$rest    = $totalAmount - $annuity * $i;
				}
				$objSchedule = new \stdClass();
				! isset( $fields['date'] ) ?: $objSchedule->{$fields['date']} = $dateSchedule->format( 'Y-m-d' );
				! isset( $fields['annuity'] ) ?: $objSchedule->{$fields['annuity']} = $annuity;
				! isset( $fields['debt'] ) ?: $objSchedule->{$fields['debt']} = $debPerc;
				! isset( $fields['percent'] ) ?: $objSchedule->{$fields['percent']} = $debPerc;
				! isset( $fields['rest'] ) ?: $objSchedule->{$fields['rest']} = $rest;
				$arSchedule[] = $objSchedule;
			}

			return $arSchedule;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}