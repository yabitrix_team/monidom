<?php

namespace app\modules\mock\v1\extensions\soapServer;

use PHP2WSDL\PHPClass2WSDL;
use SoapServer;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\caching\Cache;
use yii\di\Instance;

/**
 * Сервис инкапсулирует SoapServer и предоставляет веб-службу на основе WSDL.
 *
 * Служба использует {@see PHP2WSDL} и может генерировать WSDL
 * «на лету» без необходимости писать сложный WSDL.
 *
 * Примечание. Требуется расширение PHP SOAP.
 */
class Service extends Component {
	/**
	 * @var string|object - класс или объект поставщика веб-сервиса.
	 *                      Если указано как имя класса, это может быть псевдоним пути.
	 */
	public $provider;
	/**
	 * @var string - URL для веб-сервиса.
	 */
	public $serviceUrl;
	/**
	 * @var string - URL для WSDL.
	 */
	public $wsdlUrl;
	/**
	 * @var Cache|array|string - объект кеша или идентификатор компонента приложения кэш-объекта.
	 *                           WSDL будет кэшироваться с использованием этого объекта кэша.
	 *                           Обратить внимание: это свойство имеет смысл только
	 *                           в случае, если [[cachingDuration]] установлено на ненулевое значение.
	 *                           Начиная с версии 2.0.2, это также может быть массив конфигурации для создания объекта.
	 */
	public $cache = 'cache';
	/**
	 * @var integer - целое число в секундах, в течение которого WSDL может оставаться в кеше.
	 *                Используйте 0, чтобы указать, что кешированные данные никогда не истекут.
	 * @see enableCaching
	 */
	public $cachingDuration = 0;
	/**
	 * @var boolean - включить ли кэширование WSDL
	 */
	public $enableCaching = false;
	/**
	 * @var string - кодировка веб-службы. По умолчанию используется «UTF-8».
	 */
	public $encoding = 'UTF-8';
	/**
	 * @var array - список классов, объявленных как сложные типы в WSDL.
	 *              Это должен быть массив с типами WSDL в качестве ключей и имен классов PHP в качестве значений.
	 *              PHP-класс также может быть указан как псевдоним пути.
	 * @see http://www.php.net/manual/ru/soapserver.soapserver.php
	 */
	public $classMap = [];
	/**
	 * @var string - действующее лицо службы SOAP. По умолчанию используется значение null.
	 */
	public $actor;
	/**
	 * @var string - версия SOAP (например, «1.1» или «1.2»). По умолчанию используется значение null.
	 */
	public $soapVersion;
	/**
	 * @var integer - режим сохранения на сервере SOAP.
	 * @see http://www.php.net/manual/ru/soapserver.setpersistence.php
	 */
	public $persistence;

	/**
	 * Метод инициализации сервиса
	 *
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
		if ( empty( $this->provider ) ) {
			throw new InvalidConfigException( 'При инициализации сервиса SOAP не было или было некорректно установлено свойство "provider".' );
		}
		if ( empty( $this->serviceUrl ) || ! filter_var( $this->serviceUrl, FILTER_VALIDATE_URL ) ) {
			throw new InvalidConfigException( 'При инициализации сервиса SOAP не было или было некорректно установлено свойство "serviceUrl".' );
		}
		if ( empty( $this->wsdlUrl ) || ! filter_var( $this->wsdlUrl, FILTER_VALIDATE_URL ) ) {
			throw new InvalidConfigException( 'При инициализации сервиса SOAP не было или было некорректно установлено свойство "wsdlUrl".' );
		}
		if ( ! empty( $this->enableCaching ) ) {
			$this->cache = Instance::ensure( $this->cache, Cache::className() );
		}
		if ( YII_DEBUG ) {
			ini_set( "soap.wsdl_cache", "0" );
			ini_set( "soap.wsdl_cache_enabled", "0" );
		}
	}

	/**
	 * Метод генерации WSDL
	 * Кэшированная версия может использоваться, если WSDL найден в кеше.
	 *
	 * @return string - вернет созданный WSDL
	 */
	public function generateWsdl() {

		$providerClass = get_class( $this->provider );
		if ( $this->enableCaching ) {
			$key    = [
				__METHOD__,
				$providerClass,
				$this->serviceUrl,
			];
			$result = $this->cache->get( $key );
			if ( $result === false ) {
				$result = $this->generateWsdlInternal( $providerClass, $this->serviceUrl );
				$this->cache->set( $key, $result, $this->cachingDuration );
			}

			return $result;
		} else {
			return $this->generateWsdlInternal( $providerClass, $this->serviceUrl );
		}
	}

	/**
	 * Внутренний метод для генерации WSDL
	 *
	 * @param $className
	 * @param $serviceUrl
	 *
	 * @return mixed
	 * @see Service::generateWsdl()
	 */
	protected function generateWsdlInternal( $className, $serviceUrl ) {

		$wsdlGenerator = new PHPClass2WSDL( $className, $serviceUrl );
		$wsdlGenerator->generateWSDL( true );

		return $wsdlGenerator->dump();
	}

	/**
	 * Метод запуска сервиса
	 * Обрабатывает запрос веб-службы
	 *
	 * @return string
	 */
	public function run() {

		try {
			header( 'Content-Type: text/xml;charset=' . $this->encoding );
			$server = new SoapServer( $this->wsdlUrl, $this->getOptions() );
			if ( ! empty( $this->persistence ) ) {
				$server->setPersistence( $this->persistence );
			}
			if ( is_string( $this->provider ) ) {
				$provider = $this->provider;
				$provider = new $provider();
			} else {
				$provider = $this->provider;
			}
			$server->setObject( $provider );
			ob_start();
			$server->handle();
			$result = ob_get_contents();
			ob_end_clean();

			return $result;
		} catch ( \Exception $e ) {
			Yii::error( 'Ошибка инициализации SoapServer: ' . $e->getMessage(), __METHOD__ );
			if ( is_a( $server, SoapServer::class ) ) {
				$server->fault( $e->getCode(), $e->getMessage() );
			}
			exit;
		}
	}

	/**
	 * Метод получения опций для экземпляра SoapServer
	 *
	 * @return array - вернет массив с параметрами для создания экземпляра SoapServer
	 * @see http://www.php.net/manual/en/soapserver.soapserver.php
	 */
	protected function getOptions() {

		$options = [];
		if ( $this->soapVersion === '1.1' ) {
			$options['soap_version'] = SOAP_1_1;
		} elseif ( $this->soapVersion === '1.2' ) {
			$options['soap_version'] = SOAP_1_2;
		}
		if ( ! empty( $this->actor ) ) {
			$options['actor'] = $this->actor;
		}
		foreach ( $this->classMap as $type => $className ) {
			if ( is_int( $type ) ) {
				$type = $className;
			}
			$options['classmap'][ $type ] = $className;
		}
		$options['encoding'] = $this->encoding;

		return $options;
	}
}
