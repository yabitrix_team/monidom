<?php

namespace app\modules\mock\v1\extensions\soapServer;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\Response;

/**
 * Отдельное действие SOAP сервера - это базовый класс для классов действий, реализующих SOAP API.
 *
 * Данное действие выполняте две задачи:
 * 1) отображает контент WSDL, определяющий API-интерфейсы веб-сервисов
 * 2) вызов запрошенного API веб-сервиса
 *
 * Для отображения контента WSDL необходимо указывать расширение .wsdl, пример Annuity.wsdl
 * Без указания расширения вызовет soap сервер (веб-службу)
 *
 * Примечание. Для этого действия требуется расширение PHP SOAP.
 */
class Action extends \yii\base\Action {
	/**
	 * @var mixed - объект или имя класса поставщика веб-сервиса.
	 *              Если указано как имя класса, это может быть псевдоним пути.
	 *              По умолчанию используется значение null,
	 *              то есть текущий контроллер используется как поставщик услуг.
	 */
	public $provider;
	/**
	 * @var string - URL адрес веб-службы. Значение по умолчанию равно null,
	 *             что означает URL адрес сервиса формируется относительно
	 *             URL адреса WSDL путем исключения расширения .wsdl в URL, пример
	 *             $wsdlUrl = http://in.carmoneyzaruba.ru/mock/v1/cmr/Annuity.wsdl
	 *             то $serviceUrl будет http://in.carmoneyzaruba.ru/mock/v1/cmr/Annuity
	 */
	public $serviceUrl;
	/**
	 * @var array - задает значения свойств для объекта {@link Service}.
	 *              Ключами массива являются имена свойств {@link Service}
	 *              и значения массива являются значения свойств.
	 *
	 */
	public $serviceOptions = [];
	/**
	 * @var string - URL для WSDL, задавать относительно urlManager,
	 *             пример http://in.carmoneyzaruba.ru/mock/v1/cmr/Annuity.wsdl
	 */
	public $wsdlUrl;
	/**
	 * @var array - список классов PHP, объявленных как сложные типы в WSDL.
	 *              Это должен быть массив с типами WSDL в качестве ключей и
	 *              имен классов PHP в качестве значений.
	 *              PHP-класс также может быть указан как псевдоним пути.
	 * @see http://fi2.php.net/manual/ru/soapclient.soapclient.php
	 */
	public $classMap;
	/**
	 * @var Service - экземпляр SOAP сервиса.
	 */
	private $_service;

	/**
	 * Метод инициализации экшена
	 *
	 * @inheritdoc
	 */
	public function init() {

		//если поставщик wsdl не установлен, то выбираем методы текущего контроллера
		if ( empty( $this->provider ) ) {
			$this->provider = $this->controller;
		}
		if (
			empty( $this->wsdlUrl )
			|| ! filter_var( $this->wsdlUrl, FILTER_VALIDATE_URL )
		) {
			throw new InvalidConfigException( 'При инициализации экшена SOAP не было или было некорректно установлено обязательное свойство "wsdlUrl".' );
		}
		if ( empty( $this->serviceUrl ) || ! filter_var( $this->serviceUrl, FILTER_VALIDATE_URL ) ) {
			$this->serviceUrl = rtrim( $this->wsdlUrl, '.wsdl' );
		}
		// Отключение CSRF (Cross-Site Request Forgery) валидации в текущем экшене.
		Yii::$app->getRequest()->enableCsrfValidation = false;
	}

	/**
	 * Метод запуска действия
	 *
	 * @return string - если адрес вызова не содержит расширение .wsdl,
	 *                  то запустит веб-службу, иначе вернет контент WSDL
	 */
	public function run() {

		Yii::$app->getResponse()->format = Response::FORMAT_RAW;
		if ( strpos( Yii::$app->request->url, '.wsdl' ) === false ) {
			return $this->getService()->run();
		} else {
			$response = Yii::$app->getResponse();
			$response->getHeaders()->set( 'Content-Type', 'application/xml; charset=' . $response->charset );

			return $this->getService()->generateWsdl();
		}
	}

	/**
	 * Метод получения экземпляра веб-службы, с устанкой доп. настроек
	 *
	 * @return Service - вернет экземпляр SOAP сервиса
	 */
	public function getService() {

		if ( empty( $this->_service ) ) {
			$this->_service = new Service( [
				                               'provider'   => $this->provider,
				                               'serviceUrl' => $this->serviceUrl,
				                               'wsdlUrl'    => $this->wsdlUrl,
			                               ] );
			if ( is_array( $this->classMap ) ) {
				$this->_service->classMap = $this->classMap;
			}
			foreach ( $this->serviceOptions as $name => $value ) {
				$this->_service->$name = $value;
			}
		}

		return $this->_service;
	}
}
