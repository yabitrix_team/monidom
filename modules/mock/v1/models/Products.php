<?php

namespace app\modules\mock\v1\models;

use \app\modules\app\v1\models\Products as ProductsApp;
use Yii;

/**
 * Class Products - модель для работы со справочником Кредитные продукты
 *
 * @package app\modules\mock\v1\models
 */
class Products extends ProductsApp {
	/**
	 * @var int - сумма займа
	 */
	public $sum;
	/**
	 * @var string - дата срока займа
	 */
	public $date;

	/**
	 * Правила валидации
	 *
	 * @return array - вернет массив правил валидации
	 */
	public function rules() {

		return [
			[
				'sum',
				'required',
				'message' => 'Не указано обязательный параметр Сумма займа',
			],
			[
				'date',
				'required',
				'message' => 'Не указано обязательный параметр Срок займа',
			],
			[
				'sum',
				'number',
				'message'  => 'Сумма займа указана в некорректном формате',
				'min'      => 1,
				'tooSmall' => 'Сумма займа не может быть нулевой или отрицательной',
			],
			[
				'date',
				'match',
				'pattern' => '/^\d{4}-\d{1,2}-\d{1,2}/i',
				'message' => 'Дата срока займа указана в некорректном формате, ожидается формат 2018-12-25',
			],
			[
				'date',
				'validateDate',
			],
		];
	}

	/**
	 * Метод валидации для даты
	 *
	 * @param $attribute - атрибуты
	 * @param $params
	 */
	public function validateDate( $attribute, $params ) {

		if ( $this->{$attribute} < date( 'Y-m-d' ) ) {
			$this->addError( $attribute, 'Срок займа не может быть меньше текущей даты' );
		}
	}

	/**
	 * Метод получения данных кредитного продукта по параметрам суммы и даты
	 *
	 * @param int    $sum    - сумма займа
	 * @param string $date   - срок займа
	 * @param array  $fields - поля для выборки, обязтаельно выберет поля для вычисления:
	 *                       id, month_num, percent_per_day, summ_min, summ_max
	 *
	 * @return array|mixed|string - в случае подбора продукта вернет массив вида:
	 *                            {
	 *                              "id" =>" 54",
	 *                              "summ_min" =>" 151000",
	 *                              "summ_max" =>" 1000000",
	 *                              "month_num" =>" 24",
	 *                              "percent_per_day" =>" 0.24"
	 *                             }
	 *                            если не подберет, то вернет пустой массив,
	 *                            при выбросе исключения вернет строку
	 */
	public function getByParams( int $sum = 0, string $date = '', array $fields = [] ) {

		try {
			$this->sum  = empty( $sum ) ? $this->sum : $sum;
			$this->date = empty( $date ) ? $this->date : $date;
			if ( ! $this->validate() ) {
				return $this;
			}
			//обязательные поля для вычисления
			$requiredFields = [
				'id',
				'month_num',
				'percent_per_day',
				'summ_min',
				'summ_max',
			];
			//подготавливаем поля для получения данных
			$prepareFields = array_intersect( $fields, $this->getFieldsNames() );
			$fields        = ! empty( $prepareFields ) ? $prepareFields : $this->getFieldsNames();
			$fields        = array_unique( array_merge( $fields, $requiredFields ) );
			$strFields     = '[[' . implode( "]],[[", $fields ) . ']]';
			//получение кол-ва месяцев займа относительно даты срока займа
			$dateNow    = new \DateTime( date( 'Y-m-d 00:00:00' ) );
			$dateEnd    = new \DateTime( $this->date );
			$interval   = $dateNow->diff( $dateEnd );
			$countYears = intval( $interval->format( '%y' ) );
			$dNow       = $dateNow->format( 'j' );
			$dEnd       = $dateEnd->format( 'j' );
			$month      = intval( $interval->format( '%m' ) );
			if ( ! empty( $countYears ) ) {
				$month = $countYears * 12 + $month;
			}
			//при не равенстве дат, добавляем месяц
			$month = $dNow != $dEnd ? ++ $month : $month;
			$res   = Yii::$app->db->createCommand( 'SELECT ' . $strFields . ' 
			FROM ' . $this->getTableName() . ' 
			WHERE [[active]]=1  AND [[summ_min]]<=:sum AND [[summ_max]]>=:sum AND [[month_num]]>=:month',
			                                       [
				                                       ':sum'   => $this->sum,
				                                       ':month' => $month,
			                                       ] )
			                      ->queryAll();
			//параметры для подбора кредитного продукта
			$params = [
				'id'      => 0,
				'month'   => 0,
				'percent' => 0,
			];
			$data   = [];
			if ( ! empty( $res ) ) {
				$excludeProduct = Yii::$app->params['mp']['settings']['excludeProduct'];
				foreach ( $res as $item ) {
					//для МП исключаем не нужные кредитные продукты
					if ( in_array( $item['id'], $excludeProduct ) ) {
						continue;
					}
					$data[ $item['id'] ] = $item;
					//если месяцы не установлени или месяц текущего продукта меньше уже выбранного, то обновляем
					if (
						empty( $params['month'] )
						|| intval( $params['month'] ) > intval( $item['month_num'] )
					) {
						$params = [
							'id'      => $item['id'],
							'month'   => $item['month_num'],
							'percent' => $item['percent_per_day'],
						];
					}
					//если при равных месяцах проценты не установлены или они меньше текущего, то обновляем
					if (
						$params['month'] == $item['month_num']
						&& (
							empty( floatval( $params['percent'] ) )
							|| floatval( $params['percent'] ) > floatval( $item['percent_per_day'] )
						)
						&& ! empty( floatval( $item['percent_per_day'] ) )
					) {
						$params = [
							'id'      => $item['id'],
							'month'   => $item['month_num'],
							'percent' => $item['percent_per_day'],
						];
					}
				}
				$data = $data[ $params['id'] ];
			}

			return $data;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения опций активных кредитных продуктов для калькулятора
	 *
	 * @return array|string - в случае успеха вернет массив опций кредитных продуктов
	 *                       при выбросе исключения вернет строку
	 */
	public function getOptions() {

		try {
			$fields    = [
				'id',
				'summ_min',
				'summ_max',
				'month_num',
			];
			$strFields = '[[' . implode( "]],[[", $fields ) . ']]';
			$res       = Yii::$app->db->createCommand( 'SELECT ' . $strFields . ' 
			FROM ' . $this->getTableName() . ' WHERE [[active]]=1' )
			                          ->queryAll();
			if ( empty( $res ) ) {
				throw new \Exception( 'В системе отсутствуют активные кредитные продукты, обратитесь к администратору' );
			}
			$excludeProduct = Yii::$app->params['mp']['settings']['excludeProduct'];
			$data           = [];
			foreach ( $res as $item ) {
				//для МП исключаем не нужные кредитные продукты
				if ( in_array( $item['id'], $excludeProduct ) ) {
					continue;
				}
				$minSum                     = $data[ $item['month_num'] ]['minSum'];
				$maxSum                     = $data[ $item['month_num'] ]['maxSum'];
				$data[ $item['month_num'] ] = [
					'month'  => $item['month_num'],
					'minSum' => isset( $minSum ) && $minSum < $item['summ_min'] ? $minSum : $item['summ_min'],
					'maxSum' => isset( $maxSum ) && $maxSum > $item['summ_max'] ? $maxSum : $item['summ_max'],
				];
			}

			return $data;
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Метод получения данных кредитного продукта по его идентификатору
	 *
	 * @param       $id     - идентификатор кредитного продукта
	 * @param array $fields - поля для выборки
	 *
	 * @return array|false|string - вернет результрующий массив или ложь
	 *                            в случае исключения вернет строку
	 */
	public function getOne( $id, array $fields = [] ) {

		try {
			$prepareFields = array_intersect( $fields, $this->getFieldsNames() );
			$fields        = ! empty( $prepareFields ) ? $prepareFields : $this->getFieldsNames();
			$strFields     = '[[' . implode( "]],[[", $fields ) . ']]';

			return Yii::$app->db->createCommand( 'SELECT ' . $strFields . ' FROM ' . $this->getTableName() . ' WHERE [[id]]=:id',
			                                     [ ':id' => $id ] )
			                    ->queryAll();
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}