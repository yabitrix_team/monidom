<?php

namespace app\modules\mock\v1\models;

use \app\modules\app\v1\models\Contracts as ContractsApp;
use Yii;

/**
 * Class Contracts - модель по работе с Договорами заявок
 *
 * @package app\modules\mock\v1\models
 */
class Contracts extends ContractsApp {
	/**
	 * Метод получения договоров как открытых так и закрытых
	 *
	 * @param      $code   - номер или номера указанные через запятую
	 * @param null $active - параметр указывающий какие договора выбрать,
	 *                     если 0 то закрытые договора,
	 *                     если 1 то открытые договора,
	 *                     если null то параметр активности не учитывается
	 *
	 * @return array|string - вернет результирующий массив
	 *                      при исключении вернет строку
	 */
	public function getAgreements( $code, $active = null ) {

		try {
			$active          = ! isset( $active ) ? $active : ( empty( $active ) ? 0 : 1 );
			$conditionActive = ! isset( $active ) ? '' : ' AND [[c]].[[active]] = ' . $active;

			return Yii::$app->db->createCommand( '
SELECT 
[[c]].[[code]], 
[[r]].[[summ]], 
[[r]].[[date_return]], 
[[r]].[[updated_at]] AS [[date_start]],
[[p]].[[percent_per_day]],
[[p]].[[month_num]]
FROM ' . $this->getTableName() . ' AS [[c]]
LEFT JOIN {{requests}} AS [[r]] ON [[c]].[[request_id]] = [[r]].[[id]]
LEFT JOIN {{products}} AS [[p]] ON [[r]].[[credit_product_id]] = [[p]].[[id]]
WHERE [[c]].[[code]] IN (' . $code . ') ' . $conditionActive
			)->queryAll();
		} catch ( \Exception $e ) {
			return $e;
		}
	}
}