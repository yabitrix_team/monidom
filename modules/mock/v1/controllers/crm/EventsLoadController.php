<?php

namespace app\modules\mock\v1\controllers\crm;

use app\modules\mock\v1\classes\wsdl\EventsLoad;
use app\modules\mock\v1\controllers\ParentSoapController;

/**
 * Class EventsLoadController - контроллер для имитации сервиса "Обмен эвентами по заявкам"
 *
 * @package app\modules\mock\v1\controllers\crm
 */
class EventsLoadController extends ParentSoapController {
	/**
	 * @var string - URI(часть url адреса без указания хоста) для wsdl,
	 *              который настраивается в urlManager
	 */
	public $wsdlURI = '/mock/v1/crm/EventsLoad.wsdl';
	/**
	 * @var string - класс содержащий описание для генерации контента wsdl
	 */
	public $wsdlClass = EventsLoad::class;
}