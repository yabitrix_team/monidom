<?php

namespace app\modules\mock\v1\controllers\cmr;

use app\modules\mock\v1\classes\wsdl\AgreementsData;
use app\modules\mock\v1\controllers\ParentSoapController;

/**
 * Class AgreementsDataController - контроллер для имитации сервиса "Сведения по договорам"
 *
 * @package app\modules\mock\v1\controllers\cmr
 */
class AgreementsDataController extends ParentSoapController {
	/**
	 * @var string - URI(часть url адреса без указания хоста) для wsdl,
	 *              который настраивается в urlManager
	 */
	public $wsdlURI = '/mock/v1/cmr/AgreementsData.wsdl';
	/**
	 * @var string - класс содержащий описание для генерации контента wsdl
	 */
	public $wsdlClass = AgreementsData::class;
}