<?php

namespace app\modules\mock\v1\controllers\cmr;

use app\modules\mock\v1\classes\wsdl\CMRService;
use app\modules\mock\v1\controllers\ParentSoapController;

/**
 * Class CmrServiceController - контроллер для имитации сервиса "Сервисы ЦМР"
 *
 * @package app\modules\mock\v1\controllers\cmr
 */
class CmrServiceController extends ParentSoapController {
	/**
	 * @var string - URI(часть url адреса без указания хоста) для wsdl,
	 *              который настраивается в urlManager
	 */
	public $wsdlURI = '/mock/v1/cmr/CMRService.wsdl';
	/**
	 * @var string - класс содержащий описание для генерации контента wsdl
	 */
	public $wsdlClass = CMRService::class;
}