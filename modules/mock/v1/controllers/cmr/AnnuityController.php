<?php

namespace app\modules\mock\v1\controllers\cmr;

use app\modules\mock\v1\classes\wsdl\Annuity;
use app\modules\mock\v1\controllers\ParentSoapController;

/**
 * Class AnnuityController - контроллер для имитации сервиса "Расчет аннуитета"
 *
 * @package app\modules\mock\v1\controllers\cmr
 */
class AnnuityController extends ParentSoapController {
	/**
	 * @var string - URI(часть url адреса без указания хоста) для wsdl,
	 *              который настраивается в urlManager
	 */
	public $wsdlURI = '/mock/v1/cmr/Annuity.wsdl';
	/**
	 * @var string - класс содержащий описание для генерации контента wsdl
	 */
	public $wsdlClass = Annuity::class;
}