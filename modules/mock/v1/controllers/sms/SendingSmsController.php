<?php
namespace app\modules\mock\v1\controllers\sms;

use app\modules\mock\v1\classes\wsdl\SendingSMS;
use app\modules\mock\v1\controllers\ParentSoapController;

/**
 * Class SendingSms - контроллер для имитации сервиса "Отправка СМС"
 *
 * @package app\modules\mock\v1\controllers\sms
 */
class SendingSmsController extends ParentSoapController
{
    /**
     * @var string - URI(часть url адреса без указания хоста) для wsdl,
     *              который настраивается в urlManager
     */
    public $wsdlURI = '/mock/v1/sms/SendingSMS.wsdl';
    /**
     * @var string - класс содержащий описание для генерации контента wsdl
     */
    public $wsdlClass = SendingSMS::class;
}