<?php
namespace app\modules\mock\v1\controllers\web;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\SmsConfirm;
use yii\web\Controller;

/**
 * Class SmsController - контроллер по работе с моками СМС
 *
 * @package app\modules\mock\v1\controllers
 */
class SmsController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения кода подтверждения при работе с имитационными сервисами
     *
     * @param $phone - номер телефона
     *
     * @return array
     */
    public function actionByLogin($phone)
    {
        try {
            $model = new SmsConfirm();
            $model->load(['login' => $phone], '');
            if (!$model->validate(['login'])) {
                return $this->getClassAppResponse()::get(false, $model->firstErrors);
            }
            $res = $model->getByLogin(false, ['code']);
            if (!is_array($res)) {
                throw new \Exception(
                    'При попытке получения кода подтверждения произошла ошибка, обратитесь к администратору'
                );
            }

            return $this->getClassAppResponse()::get(
                [
                    'sms_code' => $res['code'],
                ]
            );
        } catch (\Exception $e) {
            return $this->getClassAppResponse()::get(false, false, $e->getMessage());
        }
    }
}