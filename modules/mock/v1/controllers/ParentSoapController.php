<?php

namespace app\modules\mock\v1\controllers;

use app\modules\mock\v1\extensions\soapServer\Action;
use yii\web\Controller;

/**
 * Class ParentSoapController - родительский контроллер для дочерних контроллеров имитационных сервисов
 *
 * @package app\modules\mock\v1\controllers
 */
class ParentSoapController extends Controller {
	/**
	 * @var string - URI(часть url адреса без указания хоста) для wsdl,
	 *              который настраивается в urlManager
	 */
	public $wsdlURI;
	/**
	 * @var string - класс содержащий описание для генерации контента wsdl
	 */
	public $wsdlClass;
	/*-- Далее указанные свойство являтся необязательными --*/
	/**
	 * @var boolean - включить ли кэширование WSDL
	 */
	public $enableCaching = false;
	/**
	 * @var \yii\caching\Cache|array|string - объект кеша или идентификатор компонента приложения кэш-объекта.
	 *                           WSDL будет кэшироваться с использованием этого объекта кэша.
	 *                           Обратить внимание: это свойство имеет смысл только
	 *                           в случае, если [[cachingDuration]] установлено на ненулевое значение.
	 *                           Начиная с версии 2.0.2, это также может быть массив конфигурации для создания объекта.
	 */
	public $cache = 'cache';
	/**
	 * @var integer - целое число в секундах, в течение которого WSDL может оставаться в кеше.
	 *                Используйте 0, чтобы указать, что кешированные данные никогда не истекут.
	 * @see enableCaching
	 */
	public $cachingDuration = 0;
	/**
	 * @var array - список классов PHP, объявленных как сложные типы в WSDL.
	 *              Это должен быть массив с типами WSDL в качестве ключей и
	 *              имен классов PHP в качестве значений.
	 *              PHP-класс также может быть указан как псевдоним пути.
	 * @see http://fi2.php.net/manual/ru/soapclient.soapclient.php
	 */
	public $classMap;

	/**
	 * Метод подключния отдельного экшена
	 *
	 * @return array - вернет массив настрое подключения отдельного экшена
	 */
	public function actions() {

		return [
			'index' => [
				'class'          => Action::class,
				'provider'       => new $this->wsdlClass(),
				'wsdlUrl'        => $this->getUrlWsdl(),
				'classMap'       => $this->classMap,
				'serviceOptions' => [
					'enableCaching'   => $this->enableCaching,
					'cache'           => $this->cache,
					'cachingDuration' => $this->cachingDuration,
				],
			],
		];
	}

	/**
	 * Метод формирования URL для WSDL относительно указанного URI
	 *
	 * @return string - вернет строку URL если указано URI
	 *                иначе вернет ложь
	 */
	protected function getUrlWsdl() {

		$host = ! empty( $_SERVER['HTTP_X_FORWARDED_SERVER'] )
			? $_SERVER['HTTP_X_FORWARDED_SERVER'] : $_SERVER['SERVER_NAME'];

		return ! empty( $this->wsdlURI )
			? $_SERVER['REQUEST_SCHEME'] . '://' . $host . '/' . trim( $this->wsdlURI, '/' )
			: false;
	}
}