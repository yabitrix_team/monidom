<?php

namespace app\modules\exchange;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;
use yii\console\Application;

/**
 * Exchange module definition class
 */
class Module extends BaseModule implements BootstrapInterface {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\exchange\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
		// custom initialization code goes here
		require __DIR__ . '/../../functions.php';
		Yii::setAlias( 'webroot', Yii::getAlias( '@app' ) . "/web" );
	}

	public function bootstrap( $app ) {

		if ( $app instanceof Application ) {

			$this->controllerNamespace = 'app\modules\exchange\commands';
		}
	}
}
