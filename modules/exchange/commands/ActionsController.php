<?php

namespace app\modules\exchange\commands;

use app\modules\app\v1\models\Contracts;
use app\modules\app\v1\models\UserGroup;
use app\modules\app\v1\models\Users;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\GetProcedure;
use app\modules\exchange\classes\RunLocker;
use app\modules\payment\v1\models\base\Payment;
use app\modules\payment\v1\models\cloud\Action;
use Exception;
use yii;
use yii\console\Controller;

/**
 * Exchange manager
 *
 * @package app\modules\exchange\commands
 */
class ActionsController extends Controller {
	/**
	 * Starts exchange
	 */
	public function actionStart() {

		// todo: вынести в конфиг
		ini_set( 'display_errors', 1 );
		ini_set( 'error_reporting', E_ERROR );
		error_reporting( E_ERROR );
		$gp = new GetProcedure();
		/** @var array $exchangeClasses - загрузка справочников
		 * Название класса => array( тип процедуры обмена, кол-во одновременных потоков, при успешной обработке данных "отметить" объект )
		 */
		$exchangeClasses = [
			/**
			 * Справочники
			 */
			/* Статусы заявок */
			[ "Statuses" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Занятость */
			[ "Employments" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Регионы */
			[ "Regions" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Марки автомобилей */
			[ "AutoBrands" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Модели автомобилей */
			[ "AutoModels" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Кредитные продукты */
			[ "Products" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Партнеры */
			[ "Partners" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Точки */
			[ "Points" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Пользователи */
			[ "Users" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Менеджеры  */
			[ "Managers" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/**
			 * Сохранение данных
			 */
			/** Заявки */
			[ "Requests" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Статусы заявок */
			[ "StatusesJournal" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/** Первый пакет док-в */
			[ "DocsFirstPack" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/** Второй пакет док-в */
			[ "DocsSecondPack" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/* Договоры */
			[ "Contracts" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/** Закрытые договоры */
			[ "ContractsClosed" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/** Комиссия */
			[ "RequestCommission" => [ $gp->GET_OBJECT_FROM, 5, true ], ],
			/**
			 * Отправка данных
			 */
			/* Платежи Cloud*/
			[ "PaymentsCloud" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Платежи W1*/
			[ "PaymentsW1" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Заявки */
			[ "Requests" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Фото документов */
			[ "PhotoDocs" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Фото клиента */
			[ "PhotoClient" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Фото ПТС в ЛКВМ */
			//			[ "PhotoPts" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/* Отправка событий: 101, 102, 201, 202 */
			[ "Events" => [ $gp->CREATE_OBJECT_TO, 5, true ], ],
			/**
			 * Сервис
			 */
			/* Управление зависшими статусами */
			[ "CheckStatuses" => [ $gp->GET_OBJECT_FROM, 1, false ], ],
			/* Проверка получения событий со стороны МФО */
			[ "CheckEvents" => [ $gp->GET_OBJECT_FROM, 1, false ], ],
		];
		/** cron / */
		do {
			echo "=========================================================\n";
			echo "Begin cycle >>>\n";
			echo "---------------------------------------------------------\n";
			foreach ( $exchangeClasses as $exchangeBlock ) {
				$class     = '\app\modules\exchange\classes\entity\\' . array_keys( $exchangeBlock )[0];
				$runParams = array_values( $exchangeBlock )[0];
				if ( ! class_exists( $class ) ) {
					echo $class . ' not found!!!' . PHP_EOL;
					continue;
				}
				echo array_keys( $exchangeBlock )[0] . " [" . $gp->{$runParams[0]} . "]" . PHP_EOL;
				if ( ! is_array( $runParams ) ) {
					echo $class . ' params not found!!!' . PHP_EOL;
					continue;
				}
				$RUN = new RunLocker( $class . "_" . md5( $class . serialize( $runParams ) ), md5( Yii::getAlias( '@app' ) ) );
				if ( ! $RUN->isLocked() && $RUN->lock() ) {
					/** @var array $flow_list - список потоков подключения на цикл */
					$flow_list = array_fill( 1, ( $runParams[1] ?: 10 ), true );
					/** @var Core $c */
					$c = new $class();
					// не убирать этот try..catch!
					try {
						while ( 0 < count( array_filter( $flow_list ) ) ) {
							array_walk(
								$flow_list,
								function( &$value, $key ) use ( $c, $runParams, $RUN ) {

									echo " flow $key" . PHP_EOL;
									$c->logInit();
									try {
										$value = $c->run( $runParams[0], $key, $runParams[2] );
									} catch ( Exception $e ) {
										$c->error( $e );
										$value = false;
									}
									$c->logSave();
									if ( $value ) {
										echo $RUN->unlock() ? "  ------->done (unlock)" . PHP_EOL :
											"  ------->done (can't unlock)" . PHP_EOL;
									};
									usleep( 100000 );
								}
							);
						}
					} catch ( Exception $e ) {
					}
				} else {
					echo "$class is run\n";
				}
				echo "----------------------------------" . PHP_EOL;
			}
			echo "---------------------------------------------------------" . PHP_EOL;
			echo "<<< End cycle\n";
		} while ( true );
	}

	/**
	 *  Load actual clients contracts
	 *
	 * @throws yii\db\Exception
	 * @throws yii\base\Exception
	 */
	public function actionContracts() {

		try {
			if ( ! $idGroupClient = (int) yii::$app->params["exchange"]["user_groups"]["clients"] ) {
				throw new Exception( "Не задан параметр clients в params.php. Установите id группы клиентов." );
			}
			$filePath    = rtrim( Yii::getAlias( '@app' ), '/' ) . "/runtime/contracts.csv";
			$arFile      = explode( "\n", file_get_contents( $filePath ) );
			$cntAll      = count( $arFile );
			$arContracts = [];
			foreach ( $arFile as $cnt => $sFields ) {
				$arFields      = explode( ";", $sFields );
				$arName        = explode( " ", $arFields[1] );
				$arContracts[] = [
					"username"        => $arFields[0],
					"first_name"      => $arName[1],
					"last_name"       => $arName[0],
					"second_name"     => $arName[2],
					"contract_code"   => $arFields[2],
					"contract_active" => ( $arFields[3] !== "Погашен" ),
					"pass"            => $arFields[5],
				];
			}
			$arUsers           = ( new Users() )->getAllByFilter( [ "username" => array_column( $arContracts, "username" ) ] );
			$arExistsUsers     = array_column( $arUsers, "id", "username" );
			$arAllContracts    = Contracts::instance()
			                              ->getAllByFilter( [ "code" => array_column( $arContracts, "contract_code" ) ] );
			$arExistsContracts = array_column( $arAllContracts, "code" );
			$arAddUserGroups   =
			$arAddContracts = [];
			echo "  Обработано 0 / $cntAll";
			yii::$app->db->createCommand( "SET unique_checks=0;" )->execute();
			yii::$app->db->createCommand( "SET foreign_key_checks=0;" )->execute();
			foreach ( $arContracts as $cnt => $arContract ) {
				if ( array_key_exists( $arContract["username"], $arExistsUsers ) ) {
					$idUser = $arExistsUsers[ $arContract["username"] ];
					if ( ! UserGroup::instance()->getOneByFilter( [
						                                              "user_id"  => $idUser,
						                                              "group_id" => $idGroupClient,
					                                              ] ) ) {
						if ( ! UserGroup::instance()->add( [
							                                   'user_id'  => $idUser,
							                                   "group_id" => $idGroupClient,
						                                   ] ) ) {
							throw new Exception( "Ошибка привязки пользователя id=" . $idUser . " к группе клиентов" );
						}
					}
				} else {
					$user = new Users();
					$user->hashPassword( $arContract['pass'] );
					if ( ! $idUser = $user->add( [
						                             'username'      => $arContract['username'],
						                             'password_hash' => $user->password_hash,
						                             'first_name'    => $arContract['first_name'],
						                             'last_name'     => $arContract['last_name'],
						                             'second_name'   => $arContract['second_name'],
						                             "active"        => 1,
					                             ] ) ) {
						throw new Exception( "Ошибка создания пользователя " . $arContract['username'] );
					}
					$arAddUserGroups[] = [
						$idUser,
						$idGroupClient,
						new Yii\db\Expression( 'NOW()' ),
					];
					//					if ( ! UserGroup::instance()->add( [
					//						                                   'user_id'  => $idUser,
					//						                                   "group_id" => $idGroupClient,
					//					                                   ] ) ) {
					//						throw new Exception( "Ошибка привязки пользователя id=" . $idUser . " к группе клиентов" );
					//					}
				}
				if ( ! in_array( $arContract["contract_code"], $arExistsContracts ) ) {
					$arAddContracts[] = [
						$arContract["contract_code"],
						$idUser,
						$arContract["contract_active"],
						new Yii\db\Expression( 'NOW()' ),
					];
					//					if ( ! Contracts::instance()->add( [
					//						                                   "code"    => $arContract["contract_code"],
					//						                                   "user_id" => $idUser,
					//						                                   "active"  => $arContract["contract_active"],
					//					                                   ] ) ) {
					//						throw new Exception( "Ошибка создания договора code=" . $arContract["contract_code"] );
					//					}
				}
				if ( ( $cnt + 1 ) % 100 == 0 || ( $cnt + 1 ) == $cntAll ) {
					if ( ! empty( $arAddUserGroups ) ) {
						yii::$app->db->createCommand()->batchInsert( 'user_group', [
							'user_id',
							'group_id',
							'created_at',
						], $arAddUserGroups )->execute();
						$arAddUserGroups = [];
					}
					if ( ! empty( $arAddContracts ) ) {
						yii::$app->db->createCommand()->batchInsert( 'contracts', [
							'code',
							'user_id',
							'active',
							'created_at',
						], $arAddContracts )->execute();
						$arAddContracts = [];
					}
				}
				echo str_repeat( chr( 8 ), strlen( $cnt . " / " . $cntAll ) ) . ( $cnt + 1 ) . " / " . $cntAll;
			}
		} catch ( Exception $e ) {
			yii::$app->db->createCommand( "SET unique_checks=1;" )->execute();
			yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
			echo "\n " . $e->getMessage() . "\n";
		}
		yii::$app->db->createCommand( "SET unique_checks=1;" )->execute();
		yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
		echo "\n";
	}

	/**
	 * Load failed payments
	 */
	public function actionPayments() {

		try {
			$filePath   = rtrim( Yii::getAlias( '@app' ), '/' ) . "/runtime/payments.csv";
			$arFile     = explode( "\n", file_get_contents( $filePath ) );
			$cntAll     = count( $arFile );
			$arPayments = [];
			foreach ( $arFile as $cnt => $sFields ) {
				$arFields     = explode( ";", $sFields );
				$arCard       = explode( "****", $arFields[4] );
				$arFields[1]  = preg_replace( "/^(\d{2})\.(\d{2})\.(\d{4})/", "$3-$2-$1", $arFields[1] );
				$arFields[2]  = preg_replace( "/^(\d{2})\.(\d{2})\.(\d{4})/", "$3-$2-$1", $arFields[2] );
				$arFields[5]  = str_replace( [
					                             "Январь ",
					                             "Февраль ",
					                             "Март ",
					                             "Апрель ",
					                             "Май ",
					                             "Июнь ",
					                             "Июль ",
					                             "Август ",
					                             "Сентябрь ",
					                             "Октябрь ",
					                             "Ноябрь ",
					                             "Декабрь ",
				                             ], [
					                             "01/",
					                             "02/",
					                             "03/",
					                             "04/",
					                             "05/",
					                             "06/",
					                             "07/",
					                             "08/",
					                             "09/",
					                             "10/",
					                             "11/",
					                             "12/",
				                             ], $arFields[5] );
				$arPayments[] = [
					"payment" => [
						"vendor"      => 1,
						"invoice"     => $arFields[13] . rand( 100000, 999999 ),
						"contract"    => $arFields[13],
						"transaction" => $arFields[0],
						"sum"         => str_replace( ",", ".", $arFields[8] ),
						"currency"    => 643,
						"description" => "Оплата в carmoney.ru",
					],
					"actions" => [
						[
							"action"            => "check",
							"status"            => "Completed",
							"type"              => "Payment",
							"date"              => $arFields[1],
							"name"              => $arFields[15],
							"email"             => $arFields[14],
							"card_number_first" => $arCard[0],
							"card_number_last"  => $arCard[1],
							"card_type"         => $arFields[3],
							"card_exp"          => $arFields[5],
							"ip"                => $arFields[16],
							"country"           => $arFields[17],
							"city"              => $arFields[18],
							"bank_name"         => $arFields[6],
							"data"              => json_encode( [ "sum" => str_replace( ",", ".", $arFields[25] ) ] ),
							"created_at"        => $arFields[1],
							"region"            => "",
							"district"          => "",
							"bank_country"      => "",
						],
						[
							"action"            => "pay",
							"status"            => "Completed",
							"type"              => "Payment",
							"date"              => $arFields[2],
							"name"              => $arFields[15],
							"email"             => $arFields[14],
							"card_number_first" => $arCard[0],
							"card_number_last"  => $arCard[1],
							"card_type"         => $arFields[3],
							"card_exp"          => $arFields[5],
							"ip"                => $arFields[16],
							"country"           => $arFields[17],
							"city"              => $arFields[18],
							"bank_name"         => $arFields[6],
							"data"              => json_encode( [ "sum" => str_replace( ",", ".", $arFields[25] ) ] ),
							"created_at"        => $arFields[2],
							"region"            => "",
							"district"          => "",
							"bank_country"      => "",
						],
					],
				];
			}
			echo "  Обработано 0 / $cntAll";
			foreach ( $arPayments as $cnt => $arPayment ) {
				$obPayment = new Payment( $arPayment["payment"] );
				if ( ! $obPayment->validate() ) {
					throw new Exception( print_r( $obPayment->getErrors(), true ) );
				}
				if ( ! $idPayment = $obPayment->add( $obPayment->getAttributes() ) ) {
					throw new Exception( "Ошибка записи платежа: " . print_r( $obPayment->getAttributes(), true ) );
				}
				foreach ( $arPayment["actions"] as $arAction ) {
					$obAction = new Action( array_merge( [ "payment" => $idPayment ], $arAction ) );
					if ( ! $obAction->validate() ) {
						throw new Exception( print_r( $obAction->getErrors(), true ) );
					}
					if ( ! $obAction->add( $obAction->getAttributes() ) ) {
						throw new Exception( "Ошибка записи платежа cloudpayments: " . print_r( $obAction->getAttributes(), true ) );
					}
				}
				echo str_repeat( chr( 8 ), strlen( $cnt . " / " . $cntAll ) ) . ( $cnt + 1 ) . " / " . $cntAll;
			}
		} catch ( Exception $e ) {
			echo "\n " . $e->getMessage() . "\n";
		}
		echo "\n";
	}

	public function actionTestPayments() {

		$filePath = rtrim( Yii::getAlias( '@app' ), '/' ) . "/runtime/test.pay.csv";
		$arFile   = explode( "\n", file_get_contents( $filePath ) );
		foreach ( $arFile as $sValues ) {
			$arValues = explode( ";", $sValues );
			$baseSum  = floatval( str_replace( [ ",", " " ], [ ".", "" ], $arValues[0] ) );
			$commSum  = floatval( round( $baseSum / ( ( 100 - 2 ) / 100 ), 2 ) );
			if ( $commSum !== (float) $arValues[1] ) {
				echo $commSum . " !== " . (float) $arValues[1] . PHP_EOL;
			}
		}
	}
}