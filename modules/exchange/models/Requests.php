<?php

namespace app\modules\exchange\models;

use app\modules\app\v1\models\Requests as RequestApp;
use app\modules\app\v1\models\traits\ReadableTrait;
use Yii;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends RequestApp {
	/**
	 * Подключение трейта, отвечающего за получение данных из таблицы
	 * методы из трейта объявляем под другими именами
	 */
	use ReadableTrait {
		getOne as getOneTrait;
		getAll as getAllTrait;
	}

	/**
	 * Метод получения заявки по идентификатору
	 *
	 * @param int $id - Идентификатор заявки
	 *
	 * @return array|false - Вернет массив или ложь
	 * @throws \yii\db\Exception
	 */
	public function getOne( int $id ) {

		return $this->getOneTrait( $id );
	}

	/**
	 * Метод получения всех Заявок
	 *
	 * @param bool $active - Параметр отвечающий за получение всех Заявок, в том числе неактивных
	 *                     true - по умолчанию, выбираем только активные
	 *                     false - выбираем только неактивные
	 *                     null - выбираем все и актиные и неактивные
	 *
	 * @return array - Вернет массив данных или ложь
	 * @throws \yii\db\Exception
	 */
	public function getAll( $active = true ) {

		return $this->getAllTrait( $active );
	}
}