<?php

namespace app\modules\exchange\classes;
/**
 * Class RunLocker контроль запуска определенных частей кода на исполнение
 */
interface  RunLockerHandlerInterface {
	function lock( RunLocker $locker );

	function check( RunLocker $locker );

	function isLocked( RunLocker $locker );

	function unlock( RunLocker $locker );

	function reSetLock( RunLocker $locker );
}

abstract class RunLockerHandler implements RunLockerHandlerInterface {
	function lock( RunLocker $locker ) {

		return $locker->lock();
	}

	function check( RunLocker $locker ) {

		return $locker->check();
	}

	function isLocked( RunLocker $locker ) {

		return $locker->isLocked();
	}

	function unlock( RunLocker $locker ) {

		return $locker->unlock();
	}

	function reSetLock( RunLocker $locker ) {

		return $locker->reSetLock();
	}
}

class RunLocker {
	protected $md5 = false;
	// todo: вынести в конфиг
	protected $target_mail = 'konev@carmoney.ru';
	protected $maxRunTime  = false;
	/** @var null|string уникальный идентификатор блокировки */
	private $pid;
	/** @var null|string если используется файловая блокировка можно указать директорию хранения локера */
	private $path;
	/** @var string файл-локер */
	private $lock;
	/** @var null|mixed указатель на файл-локер, полученный ф-ей fopen() */
	private $fp = null;
	/** @var null|RunLockerHandler */
	private $handler = null;
	/** @var bool состояние блокировки */
	private $locked = false;
	/** @var bool является владельцем блокировки (создал и/или захватил ключ) */
	private $haveLock = false;
	/** @var bool флаг регистрации обработчика после завршения выполнения скрипта с блокировкой */
	private $registerDownHandler = true;

	/**
	 * RunLocker constructor.
	 *
	 * @param null $pid
	 * @param null $path
	 * @param bool $registerDownHandler
	 */
	function __construct( $pid = null, $path = null, $registerDownHandler = true ) {

		$this->pid = isset( $pid ) ? ( 36 == strlen( $pid ) ? $pid : md5( $pid ) ) : $this->getPid();
		if ( isset( $path ) ) {
			$path = sys_get_temp_dir() . ( $path ? '/' . trim( $path, "\\/ " ) : '' );
			if ( ! file_exists( $path ) ) {
				mkdir( $path );
			}
			$this->path = $path;
		}
		$this->registerDownHandler = $registerDownHandler;
	}

	private function getPid() {

		return md5( rand( 1, 10000 ) * microtime() );
	}

	function isLocked() {

		if ( isset( $this->handler ) ) {
			$this->locked = $this->handler->isLocked( $this );

			return $this->locked;
		}

		return ( $this->fp && $this->locked ) || $this->check();
	}

	function check() {

		if ( isset( $this->handler ) ) {
			$this->locked = $this->handler->check( $this );

			return $this->locked;
		}
		if ( $this->fp ) {
			$this->locked = $this->haveLock || ! flock( $this->fp, LOCK_EX | LOCK_NB );
		}

		return $this->locked;
	}

	function reSetLock() {

		if ( isset( $this->handler ) ) {
			$this->locked = $this->handler->reSetLock( $this );

			return $this->locked;
		}
		if ( $this->locked ) {
			$this->locked = ( $this->unlock() && $this->lock() );
		}

		return $this->locked;
	}

	function unlock() {

		if ( isset( $this->handler ) ) {
			$this->locked = $this->handler->unlock( $this );

			return $this->locked;
		}
		if ( ! flock( $this->fp, LOCK_UN ) ) {
			/** заблокировать файл не удалось, значит запущена копия скрипта */
			$this->locked = true;

			return $this->locked;
		}
		fclose( $this->fp );
		unlink( $this->lock );
		rmdir( $this->path );
		$this->locked   = false;
		$this->haveLock = false;

		return $this->locked;
	}

	function lock() {

		if ( isset( $this->handler ) ) {
			$this->locked = $this->handler->lock( $this );

			return $this->locked;
		}
		$this->lock = $this->path . "/locker_{$this->pid}.lock";
		$aborted    = file_exists( $this->lock ) ? filemtime( $this->lock ) : false;
		$this->fp   = fopen( $this->lock, 'w' );
		if ( ! flock( $this->fp, LOCK_EX | LOCK_NB ) ) {
			/** заблокировать файл не удалось, значит запущена копия скрипта */
			$this->locked = true;
			echo "(is locked)\n";

			return $this->locked;
		}
		/** получили блокировку файла */
		/** если файл уже существовал значит предыдущий запуск кто-то прибил извне */
		if ( $aborted ) {
			echo sprintf( "(Запуск скрипта %s был завершен аварийно %s", $this->pid, date( 'c', $aborted ) ) . ")\n";
			//@error_log(sprintf( "Запуск скрипта %s был завершен аварийно %s: ", $this->pid, date( 'c', $aborted ) ), 1, $this->target_mail );
		}
		$this->locked   = true;
		$this->haveLock = true;
		if ( $this->registerDownHandler ) {
			/**
			 * снятие блокировки по окончанию работы
			 * если этот callback, не будет выполнен, то блокировка
			 * все равно будет снята ядром, но файл останется
			 */
			register_shutdown_function( function() {

				$this->unlock();
			} );
		}

		return $this->locked;
	}

	function setLockHandler( RunLockerHandler $handler ) {

		$this->handler = $handler;
	}

	/**
	 * Проверяет файла метки, если нет - создает, иначе проверяет дату последнего изменения файла-метки (touch) с
	 * переданной временной меткой.
	 *
	 * @param null $pid      - уникальный id файла-метки
	 * @param null $time     - время для проверки
	 * @param null $site_key - дополнительный ключ для многосайтовости (в случае наличия идентичных PID)
	 *
	 * @return bool
	 */
	function buildEventTimer( $pid = null, $time = null, $site_key = null ) {

		if ( ! is_null( $pid ) && ! is_null( $time ) ) {
			$this->maxRunTime = $time;
			if ( $this->md5 ) {
				$pid = md5( $pid );
			}
			$path = sys_get_temp_dir() . ( $site_key ? '/' . trim( $site_key, "\\/ " ) : '' );
			if ( ! file_exists( $path ) ) {
				mkdir( $path );
			}
			$lock    = $path . "/locker_timer_$pid.lock";
			$aborted = file_exists( $lock ) ? filemtime( $lock ) : false;
			if ( ! $aborted && $fp = fopen( $lock, 'w' ) ) {
				fclose( $fp );

				return true;
			} elseif (
				$aborted && $this->maxRunTime && ( $this->maxRunTime <= ( date( 'U' ) - $aborted ) )
			) {
				touch( $lock );

				return true;
			}
		}

		return false;
	}

	/**
	 * Функция создает файл-метку, которая удаляется после завершения выполнения скрипта автоматически.
	 * Если файл существует, то ф-я возвращает FALSE.
	 *
	 * Если файл не был удален по каким-либо причинам - ф-я произведет запись error_log с соответствующим уведомлением.
	 *
	 * @param null $pid  - уникальный id файла-метки
	 * @param null $path - папка расположения файла метки (полезно для группировки меток)
	 *
	 * @return bool
	 */
	function buildEvent( $pid = null, $path = null ) {

		if ( ! is_null( $pid ) ) {
			if ( $this->md5 ) {
				$pid = md5( $pid );
			}
			$path = sys_get_temp_dir() . ( $path ? '/' . trim( $path, "\\/ " ) : '' );
			if ( ! file_exists( $path ) ) {
				mkdir( $path );
			}
			$lock    = $path . "/locker_$pid.lock";
			$aborted = file_exists( $lock ) ? filemtime( $lock ) : false;
			$fp      = fopen( $lock, 'w' );
			if ( ! flock( $fp, LOCK_EX | LOCK_NB ) ) {
				if ( $aborted && $this->maxRunTime && ( $this->maxRunTime >= ( date( 'U' ) - $aborted ) ) ) {
					$msg = <<<TXT
Процесс обмена "$pid" превышает допустимый срок выполнения 
TXT;
					carLog( $msg );
				}

				/** заблокировать файл не удалось, значит запущена копия скрипта */
				return false;
			}
			/** получили блокировку файла */
			/** если файл уже существовал значит предыдущий запуск кто-то прибил извне */
			if ( $aborted ) {
				@error_log( sprintf( "Запуск скрипта %s был завершен аварийно %s: ", $pid, date( 'c', $aborted ) ), 1, $this->target_mail );
			}
			/**
			 * снятие блокировки по окончанию работы
			 * если этот callback, не будет выполнен, то блокировка
			 * все равно будет снята ядром, но файл останется
			 */
			register_shutdown_function( function() use ( $fp, $lock, $path ) {

				flock( $fp, LOCK_UN );
				fclose( $fp );
				unlink( $lock );
				rmdir( $path );
			} );

			return true;
		}

		return false;
	}
}