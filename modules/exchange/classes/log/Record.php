<?php

namespace app\modules\exchange\classes\log;
/**
 * Составной интерфейс для работы с записями сущностей
 *
 * @package app\modules\exchange\classes\log
 */
trait Record {
	use DB, Fields, FactoryMethod {
		DB::__construct as DB__construct;
		Fields::__construct as Fields__construct;
	}

	/**
	 * Record constructor.
	 *
	 * @param string $table
	 * @param array  $fields
	 * @param bool   $bAutoincrement
	 */
	public function __construct( $table, array $fields, $bAutoincrement = true ) {

		self::DB__construct( $table, $bAutoincrement );
		self::Fields__construct( $fields );
	}
}