<?php

namespace app\modules\exchange\classes\log;
/**
 * Класс обмена
 *
 * @package app\modules\exchange\classes\log
 */
class ExchangeStage {
	/**
	 * @use getInstanse
	 */
	use Record;
	/**
	 * @var int Служебное
	 */
	const System = 1;
	/**
	 * @var int Марки авто
	 */
	const AutoBrandsController = 2;
	/**
	 * @var int Поколения авто
	 */
	const AutoGenerationsController = 3;
	/**
	 * @var int Модели авто
	 */
	const AutoModelsController = 4;
	/**
	 * @var int Модификации авто
	 */
	const AutoModificationsController = 5;
	/**
	 * @var int Партнеры
	 */
	const PartnersController = 6;
	/**
	 * @var int Точки
	 */
	const PointsController = 7;
	/**
	 * @var int Кредитные продукты
	 */
	const ProductsController = 8;
	/**
	 * @var int Регионы
	 */
	const RegionsController = 9;
	/**
	 * @var int Типы статусов
	 */
	const StatusesController = 10;
	/**
	 * @var int Пакет документов 1
	 */
	const DocsFirstPackController = 11;
	/**
	 * @var int Пакет документов 2
	 */
	const DocsSecondPackController = 12;
	/**
	 * @var int Комиссия
	 */
	const CommissionController = 13;
	/**
	 * @var int Статусы
	 */
	const StatusesJournalController = 14;
	/**
	 * @var int Пользователи
	 */
	const UsersController = 15;
	/**
	 * @var int Занятность
	 */
	const EmploymentController = 16;
	/**
	 * @var int Менеджеры
	 */
	const ManagersController = 17;
	/**
	 * @var int События
	 */
	const EventsController = 18;
	/**
	 * @var int Заявки
	 */
	const RequestsController = 19;
	/**
	 * @var int Платежи CloudPayments
	 */
	const CloudPaymentsController = 20;
	/**
	 * @var int Фото документов
	 */
	const PhotoDocsController = 21;
	/**
	 * @var int Фото клиента
	 */
	const PhotoClientController = 22;
	/**
	 * @var int Графики по договору
	 */
	const LoanDataController = 23;
	/**
	 * @var int Закрытые договора
	 */
	const LoanClosedDataController = 24;
	/**
	 * @var int Клиенты
	 */
	const ClientsController = 25;
	/**
	 * @var int Платежи ExchangePayments
	 */
	const PaymentsExchangeController = 26;

	/**
	 * Получение экземпляра класса
	 *
	 * @return ExchangeStage
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_exchange_stage", [ "id", "code", "name" ]
		);
	}
}