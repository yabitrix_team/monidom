<?php

namespace app\modules\exchange\classes\log;
/**
 * Тип обмена
 *
 * @package app\modules\exchange\classes\log
 */
class ExchangeType {
	/**
	 * @use getInstanse
	 */
	use Record;
	/**
	 * @var int Служебное
	 */
	const System = 1;
	/**
	 * @var int На сайт
	 */
	const In = 2;
	/**
	 * @var int В 1С
	 */
	const Out = 3;

	/**
	 * Получение экземпляра класса
	 *
	 * @return ExchangeType
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_exchange_type", [ "id", "name" ]
		);
	}
}