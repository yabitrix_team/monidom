<?php

namespace app\modules\exchange\classes\log;
/**
 * Методы работы с локальным хранилищем данных
 *
 * @package app\modules\exchange\classes\log
 */
trait Fields {
	/**
	 * @var array локальное хранилище данных
	 */
	private $values = [];
	/**
	 * @var array список полей хранилища
	 */
	private $fields = [];

	/**
	 * Fields constructor.
	 *
	 * @param array $fields список полей хранилища
	 */
	public function __construct( array $fields ) {

		$this->setFields( $fields );
		$this->initValues();
	}

	/**
	 * Установка списка полей
	 *
	 * @return array список полей
	 */
	private function setFields( array $fields ) {

		$this->fields = $fields;
	}

	/**
	 * Инициализация локального хранилища
	 *
	 * @return array список полей
	 */
	private function initValues() {

		foreach ( $this->getFields() as $key ) {
			$this->values[ $key ] = null;
		}
	}

	/**
	 * Получение списка полей
	 *
	 * @return array список полей
	 */
	public function getFields() {

		return $this->fields;
	}

	/**
	 * Получение значений хранилища, либо одного при указании его кода
	 *
	 * @param $key string код поля
	 *
	 * @return array|mixed массив значений или значение конкретного поля
	 */
	public function getValues( $key = null ) {

		if ( strlen( $key ) ) {
			return $this->values[ $key ];
		}

		return $this->values;
	}

	/**
	 * Установка значений хранилища
	 *
	 * @param array $values массив значений
	 */
	public function setValues( array $values ) {

		foreach ( $values as $key => $value ) {
			if ( ! in_array( $key, $this->getFields() ) ) {
				continue;
			}
			$this->values[ $key ] = $value;
		}
	}

	/**
	 * Получение кода поля идентификатора
	 *
	 * @return mixed
	 */
	public function getPrimaryKey() {

		return reset( $this->getFields() );
	}
}