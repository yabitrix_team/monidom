<?php

namespace app\modules\exchange\classes\log;
/**
 * Заявка
 *
 * @package app\modules\exchange\classes\log
 */
class Order {
	/**
	 * @use getInstanse
	 */
	use Record;

	/**
	 * Получение экземпляра класса
	 *
	 * @return Order
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_order", [ "id", "id_xml", "id_1c" ], false
		);
	}
}