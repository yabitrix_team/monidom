<?php

namespace app\modules\exchange\classes\log;
/**
 * Методы используемые в клиенте логирования.
 * Все методы работают с интерфейсом Record, то есть ключи передаваемых значений соответствуют названием полей в
 * таблицах БД
 *
 * Пример использования:
 *
 * В классе Spr ставится наследование данного трейта:
 * <code>
 * class Spr ... {
 *      use app\modules\exchange\classes\log\Client;
 * </code>
 * В финальном  методе запускающим процедуры в TBD Spr::run() инициализируем лог:
 * <code>
 * $this->logInit();
 * </code>
 *
 * В теле методов Spr, в любых методах наследуемых классов, участвующих в обмене, расставляются вызовы:
 * * <code>
 * $this->logData(["data" => [массив данных], "query" => "текст mysql-запроса"]); // query|data не обязательны
 * </code>
 * * <code>
 * $this->logTbdError("текст ошибки");
 * </code>
 * * <code>
 * $this->logBitrixError("текст ошибки");
 * </code>
 * Закрывающий метод единицы лога в конце Spr::run():
 * * <code>
 * $this->logSave();
 * </code>
 *
 * @package app\modules\exchange\classes\log
 */
trait Client {
	/**
	 * @var Entry эксемпляр сущности записи таблицы логов
	 */
	protected $log_entry = null;

	/**
	 * Установка типа обмена
	 *
	 * @param int $idType номер типа обмена
	 *
	 * @see ExchangeType константы типов классов обмена
	 */
	public function logSetExchType( int $idType ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		$this->log_entry->setValues(
			[
				"exchange_type" => $idType,
			]
		);
	}

	/**
	 * Инициализация сущности логов.
	 *
	 * Вызывается в необходимый момент фиксирования времени начала работы потока
	 */
	public function logInit() {
		static $arExchangeStages = [];

		$this->log_entry = Entry::o();
		$this->log_entry->setValues(
			[
				"date_start" => date( "Y-m-d H:i:s", time() ),
			]
		);
		if ( $this->flow ) {
			$this->logSetFlow( $this->flow );
		}
		if ( $idName = constant( "app\modules\\exchange\classes\log\ExchangeName::" . $this->exchange_name ) ) {
			$this->logSetExchName( $idName );
		}

		// todo: закешировать $arExchangeStages
		if (empty($arExchangeStages)) {
			$arExchangeStages = array_column(ExchangeStage::o()->select()->queryAll(), "id", "code");
		}

		$sExchangeStage = ( new \ReflectionClass( $this ) )->getShortName();
		if (!$arExchangeStages[$sExchangeStage]) {
			$ob = ExchangeStage::o();
			$ob->setValues([
				               "code" => $sExchangeStage,
				               "name" => $sExchangeStage
			               ]);
			if ($id = $ob->add()) {
				$arExchangeStages[$sExchangeStage] = $id;
			}
		}
		$this->logSetExchStage( $arExchangeStages[$sExchangeStage] );
	}

	/**
	 * Установка номера потока.
	 *
	 * Если трейт наследуется не Spr классом (или его наследником) необходимо вызывать вручную (при отсутствии свойства
	 * flow)
	 *
	 * @param int $nFlow номер потока
	 */
	public function logSetFlow( int $nFlow ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		$this->log_entry->setValues(
			[
				"flow" => $nFlow,
			]
		);
	}

	/**
	 * Установка категории обмена
	 *
	 * Если трейт наследуется не Spr классом (или его наследником) необходимо вызывать вручную (при отсутствии свойства
	 * exchange_name)
	 *
	 * @param int $idName номер категории обмена
	 *
	 * @see ExchangeName константы номеров категорий обмена
	 */
	public function logSetExchName( int $idName ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		$this->log_entry->setValues(
			[
				"exchange_name" => $idName,
			]
		);
	}

	/**
	 * Установка класса обмена
	 *
	 * Если трейт наследуется не Spr классом (или его наследником) необходимо вызывать вручную
	 *
	 * @param int $idStage номер класса обмена
	 *
	 * @see ExchangeStage константы номеров классов обмена
	 */
	public function logSetExchStage( int $idStage ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		$this->log_entry->setValues(
			[
				"exchange_stage" => $idStage,
			]
		);
	}

	/**
	 * Логирование данных.
	 *
	 * Допустимые ключи:
	 * * log_id - id записи лога основной таблицы
	 * * data - массив данных
	 * * query - строка запроса
	 * * file - base64-строка файла
	 *
	 * @param array $data массив данных
	 */
	public function logData( array $data = [] ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		if ( is_null( $this->log_entry->order ) ) {
			$this->log_entry->order = Order::o();
		}
		// обработка информации по заявке/договору
		$idOrder = $this->log_entry->order->getValues( "id" );
		if ( !$idOrder && ($this->getOrderKeyID() || $this->getOrderKeyGUID())) {
			$xmlOrder = false;
			// определенная очередность полей запросов, без определенной необходимости не менять
			if ($this->getOrderKeyGUID() && $data["data"][$this->getOrderKeyGUID()] ) {
				$xmlOrder = $data["data"][$this->getOrderKeyGUID()];
			} elseif ($this->getOrderKeyID() && $data["data"][$this->getOrderKeyID()] ) {
				$idOrder = $data["data"][$this->getOrderKeyID()];
			}
			// если определился ID заявки в битриксе
			if ( $idOrder ) {
				$this->logOrder( [ "id" => $idOrder ] );
				// если определился GUID заявки
			} elseif ( $xmlOrder ) {
				$rs = Order::o()->select(
					[
						"filter" => [ "id_xml" => $xmlOrder ],
					]
				);
				// если в логах уже есть информация по этой заявке
				if ( $ar = $rs->queryOne() ) {
					$this->logOrder( [ "id" => $ar["id"], "id_xml" => $xmlOrder ] );
					// если в логах еще нет ничего по этой заявке - информация поступает впервые
				} elseif ( $this->getOrderKeyID() && $data["data"][$this->getOrderKeyID()] ) {
					$this->logOrder( [ "id" => $data["data"][$this->getOrderKeyID()], "id_xml" => $xmlOrder ] );
				}
			}
		}
		// сохранение файлов при наличии
		$obData = Data::o();
		if ( strlen( $data["data"]["p_Fayl"] ) ) {
			// когда base64 файла в теле запроса
			if ( strlen( $data["query"] ) && ! preg_match( "/sExchData/", $data["query"] ) ) {
				if ( preg_match_all( '/\'(.*?)\'/s', $data["query"], $mQuery ) ) {
					$numKeyPos     = array_search( "p_Fayl", array_keys( $data["data"] ) );
					$data["query"] = str_replace( $mQuery[1][ $numKeyPos - 1 ], "file_not_empty", $data["query"] );
				}
			}
			$data["file"]           = $data["data"]["p_Fayl"];
			$data["data"]["p_Fayl"] = "file_not_empty";
		}
		// запись данных в коллекцию в экземпляр единицы логирования
		$obData->setValues( $data );
		$this->log_entry->data[] = $obData;
	}

	/**
	 * Логирование заявки/договора.
	 *
	 * Допустимые ключи:
	 * * id_xml - GUID заявки (не обязательный)
	 * * id_1c - 1C ID заявки (не обязательный)
	 *
	 * @param array $order массив данных
	 */
	public function logOrder( array $order = [] ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		if ( is_null( $this->log_entry->order ) ) {
			$this->log_entry->order = Order::o();
		}
		$this->log_entry->order->setValues( $order );
	}

	/**
	 * Логирование TBD-ошибок, обертка logError()
	 *
	 * @param string $text текст ошибки
	 */
	public function logTbdError( string $text ) {

		$this->logError(
			[
				"text" => $text,
				"type" => ErrorType::tbd,
			]
		);
	}

	/**
	 * Логирование ошибок.
	 *
	 * Допустимые ключи:
	 * * log_id - id записи лога основной таблицы
	 * * type - номер типа ошибки
	 * * text - текст ошибки
	 *
	 * @param array $error массив ошибки
	 *
	 * @see ErrorType константы типов ошибок
	 */
	public function logError( array $error = [] ) {

		if ( is_null( $this->log_entry ) ) {
			$this->logInit();
		}
		$obError = Error::o();
		$obError->setValues( $error );
		$this->log_entry->error[] = $obError;
	}

	/**
	 * Логирование Yii-ошибок, обертка logError()
	 *
	 * @param string $text текст ошибки
	 */
	public function logYiiError( string $text ) {

		$this->logError(
			[
				"text" => $text,
				"type" => ErrorType::yii,
			]
		);
	}

	/**
	 * Запись сущности логов в БД.
	 *
	 * Запись производится, если в процессе работы потока было произведено одно из логирований:
	 * * заявки
	 * * данных
	 * * ошибок
	 *
	 * Фиксируется время завершения работы с потоком.
	 */
	public function logSave() {

		if ( is_null( $this->log_entry ) ) {
			return;
		}
		$this->log_entry->setValues(
			[
				"date_end" => date( "Y-m-d H:i:s", time() ),
			]
		);
		if (
			! is_null( $this->log_entry->order ) && $this->log_entry->order->getValues( "id" )
		) {
			$obOrder = $this->log_entry->order;
			$idOrder = $obOrder->getValues( "id" );
			$this->log_entry->setValues(
				[
					"order_id" => $idOrder,
				]
			);
			$checkExists = $obOrder->select(
				[
					"filter" => [ "id" => $idOrder ],
				]
			);
			if ( $checkExists->query()->getRowCount() ) {
				$obOrder->update();
			} else {
				$obOrder->add();
			}
		}
		if (
			! is_null( $this->log_entry->order ) || ! empty( $this->log_entry->data ) || ! empty( $this->log_entry->error )
		) {
			$idEntry = $this->log_entry->add();
			foreach ( $this->log_entry->data as $obData ) {
				$obData->setValues(
					[
						"log_id" => $idEntry,
					]
				);
				$obData->add();
			}
			foreach ( $this->log_entry->error as $obError ) {
				$obError->setValues(
					[
						"log_id" => $idEntry,
					]
				);
				$obError->add();
			}
		}
//		unset( $this->log_entry );
		$this->log_entry = null;
	}
}