<?php

namespace app\modules\exchange\classes\log;
/**
 * Лог
 *
 * @package app\modules\exchange\classes\log
 */
class Entry {
	/**
	 * @use getInstanse
	 * @use prepareAddFields
	 */
	use Record;
	/**
	 * @var Order $order экземпляр заявки
	 */
	public $order = null;
	/**
	 * @var array $data список экземпляров Data
	 */
	public $data = [];
	/**
	 * @var array $error список экземпляров Error
	 */
	public $error = [];

	/**
	 * Получение экземпляра класса
	 *
	 * @return Entry
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log", [
				         "id",
				         "exchange_name",
				         "exchange_type",
				         "exchange_stage",
				         "flow",
				         "date_add",
				         "date_start",
				         "date_end",
				         "order_id",
			         ]
		);
	}

	/**
	 * Формирование списка полей таблицы, участвующих при добавлении записи в БД.
	 * Учитывается использование инкрементального ID в таблице
	 *
	 * @return array список полей на добавление
	 */
	private function prepareAddFields() {

		$arReturn = [];
		foreach ( $this->getFields() as $key ) {
			if ( in_array( $key, [ "id", "date_add" ] ) ) {
				continue;
			}
			$arReturn[] = $key;
		}

		return $arReturn;
	}
}