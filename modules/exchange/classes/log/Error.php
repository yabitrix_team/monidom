<?php

namespace app\modules\exchange\classes\log;
/**
 * Ошибка
 *
 * @package app\modules\exchange\classes\log
 */
class Error {
	/**
	 * @use getInstanse
	 */
	use Record;

	/**
	 * Получение экземпляра класса
	 *
	 * @return Error
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_error", [ "id", "log_id", "type", "text" ]
		);
	}
}