<?php

namespace app\modules\exchange\classes\log;

use yii;

/**
 * Методы работы с базой данных
 *
 * @package app\modules\exchange\classes\log
 */
trait DB {
	/**
	 * @var string название таблицы
	 */
	private $table = null;
	/**
	 * @var mixed объект для работы с БД
	 */
	private $db = null;
	/**
	 * @var bool флаг использования инкрементального ID в таблице
	 */
	private $bAutoEncrement = null;

	/**
	 * DB constructor.
	 *
	 * @param string $table
	 * @param bool   $bAutoincrement
	 */
	public function __construct( string $table, bool $bAutoincrement ) {

		$this->table          = $table;
		$this->bAutoEncrement = ! ! $bAutoincrement;
		$this->db             = yii::$app->pdb;
	}

	/**
	 * Добавление записи в БД, на основании данных в локальном хранилище экземпляра класса
	 *
	 * @return int|bool ID записи, либо false в случае ошибки
	 */
	public function add() {

		$sql = "INSERT INTO " . $this->getTableName() . " ([[" . implode( "]], [[", $this->prepareAddFields() ) . "]]) VALUES (" . implode( ", ", $this->prepareAddValues() ) . ");";
//				var_dump($sql);
		if ( $this->db->createCommand( $sql )->execute() ) {
			$id = $this->db->getLastInsertId();
			$this->setValues(
				[
					$this->getPrimaryKey() => $id,
				]
			);

			return $id;
		}
		return false;
	}

	/**
	 * Экранирование названия таблицы
	 *
	 * @return string экранированное название
	 */
	protected function getTableName(): string {

		return "{{" . $this->table . "}}";
	}

	/**
	 * Формирование и предварительная обработка данных на добавление записи в БД
	 *
	 * @return array массив данных на добавление
	 */
	private function prepareAddValues() {

		$arReturn = [];
		foreach ( $this->getValues() as $key => $mixValue ) {
			if ( ! in_array( $key, $this->prepareAddFields() ) ) {
				continue;
			}
			if ( empty( $mixValue ) ) {
				$arReturn[ $key ] = "null";
			} elseif ( is_array( $mixValue ) ) {
				$arReturn[ $key ] = "'" . json_encode( $mixValue, JSON_HEX_APOS ) . "'";
			} elseif ( ! is_numeric( $mixValue ) ) {
				$arReturn[ $key ] = "'" . str_replace( "'", "\'", $mixValue ) . "'";
			} else {
				$arReturn[ $key ] = $mixValue;
			}
		}

		return $arReturn;
	}

	abstract public function getValues( $key = null );

	abstract public function getPrimaryKey();

	/**
	 * Обновление записи в БД, на основании данных в локальном хранилище экземпляра класса
	 *
	 * @param array $fields список полей на обновление (не обязательный)
	 *
	 * @return bool успешность выполнения запроса
	 */
	public function update( array $fields = [] ) {

		$sql = "update " . $this->getTableName() . " set " . implode( ", ", $this->prepareUpdateValues( $fields ) ) . " where " . $this->getPrimaryKey() . "=" . $this->getValues( $this->getPrimaryKey() ) . ";";
		//		var_dump($sql);
		if ( $this->db->createCommand( $sql )->execute() ) {
			return true;
		}
		return false;
	}

	/**
	 * Формирование массива данных на обновление записи в БД
	 *
	 * @param array $fields список полей на обновление (при отсутствии обновляются все поля)
	 *
	 * @return array массив данных на обновление
	 */
	private function prepareUpdateValues( array $fields = [] ) {

		$arReturn = [];
		foreach ( $this->prepareAddValues() as $key => $mixValue ) {
			if ( ! empty( $fields ) && ! in_array( $key, $fields ) ) {
				continue;
			}
			$arReturn[] = "[[" . $key . "]]=" . $mixValue;
		}

		return $arReturn;
	}

	/**
	 * Удаление записи из БД по ID из локального хранилища экземпляра класса
	 *
	 * @return bool успешность выполнения запроса
	 */
	public function delete() {

		$sql = "DELETE FROM " . $this->getTableName() . " WHERE [[" . $this->getPrimaryKey() . "]]=" . $this->getValues( $this->getPrimaryKey() ) . ";";
		//		var_dump($sql);
		if ( $this->db->createCommand( $sql )->execute() ) {
			return true;
		}
		return false;
	}

	/**
	 * Выборка данных из БД по заданному фильтру, сортировке и перечню полей.
	 *
	 * <code>
	 * $rs = Entry::o()->select(
	 *      "sort" => ["id" => "desc"],
	 *      "filter" => [">id" => 1, "?name" => "es"]
	 *      "select" => ["id", "name"]
	 * );
	 * if ($ar = $rs->fetch()) {
	 *      // работа с данными
	 * }
	 * </code>
	 * Параметры:
	 * * sort - сортировка, кол-во неограничено
	 * * filter - фильтр, значения могут быть массивами ()
	 * * select - поля выборки, если не указано выгребаются все
	 *
	 * Возможные префиксы фильтра:
	 * * &lt;, &gt;, &lt;=, &gt;=, = - сравнение
	 * * ! - отрицание
	 * * ? - поиск по подстроке
	 *
	 * @param array $params
	 *
	 * @return mixed объект результата запроса
	 */
	public function select( array $params = [] ) {

		$select = is_array( $params["select"] ) && ! empty( $params["select"] ) ? implode( ", ", $params["select"] ) :
			"*";
		$filter = $params["filter"] ? $params["filter"] : [];
		$sort   = "";
		if ( is_array( $params["sort"] ) && ! empty( $params["sort"] ) ) {
			$arSort = [];
			foreach ( $params["sort"] as $sortBy => $sortOrder ) {
				$arSort[] = "[[" . $sortBy . "]] " . $sortOrder;
			}
			$sort = implode( ", ", $arSort );
		}
		$sql = "select " . $select . " from " . $this->getTableName() . " where " . implode( " and ", $this->prepareSelectFilter( $filter ) ) . ( strlen( $sort )
				? " order by " . $sort : "" );

		//		var_dump($sql);
		return $this->db->createCommand( $sql );
	}

	/**
	 * Формирование фильтра для выборки данных (часть запроса после "where")
	 *
	 * @param array $filter массив фильтрации
	 *
	 * @return array части фильтра, которые будут объеденены через "and" в запросе
	 */
	private function prepareSelectFilter( array $filter = [] ) {

		$arReturn = [];
		foreach ( $filter as $key => $value ) {

			$prefix = "";
			if ( preg_match( '/^[\!\<\>\=]+/s', $key, $mKey ) ) {
				$prefix = reset( $mKey );
				$key    = str_replace( $prefix, "", $key );
			}
			$key = "[[" . $key . "]]";
			if ( is_array( $value ) ) {
				$bNumeric = true;
				foreach ( $value as $val ) {
					if ( ! is_numeric( $val ) ) {
						$bNumeric = false;
						break;
					}
				}
				$arReturn[] = $key . ( $prefix == "!" ? " not" : "" ) . " in (" . ( $bNumeric ? "" : "'" ) . implode(
						( $bNumeric ? ", " : "', '" ), $value
					) . ( $bNumeric ? "" : "'" ) . ")";
				continue;
			}
			$value = is_numeric( $value ) ? $value : "'" . $value . "'";
			switch ( $prefix ) {
				case "<":
				case "<=":
				case ">":
				case ">=":
					$arReturn[] = $key . $prefix . $value;
					break;
				case "!":
					$arReturn[] = $key . " not " . $value;
					break;
				case "?":
					$arReturn[] = $key . " like '%" . $value . "%'";
					break;
				default:
					$arReturn[] = $key . "=" . $value;
			}
		}
		$arReturn[] = "1";

		return $arReturn;
	}

	/**
	 * Формирование списка полей таблицы, участвующих при добавлении записи в БД.
	 * Учитывается использование инкрементального ID в таблице
	 *
	 * @return array список полей на добавление
	 */
	private function prepareAddFields() {

		$arReturn = [];
		$bFirst   = true;
		foreach ( $this->getFields() as $key ) {
			if ( $this->bAutoEncrement && $bFirst ) {
				$bFirst = false;
				continue;
			}
			$arReturn[] = $key;
		}

		return $arReturn;
	}

	abstract public function getFields();
}