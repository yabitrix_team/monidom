<?php

namespace app\modules\exchange\classes\log;
/**
 * Категория обмена
 *
 * @package app\modules\exchange\classes\log
 */
class ExchangeName {
	/**
	 * @use getInstanse
	 */
	use Record;
	/**
	 * @var int Служебное
	 */
	const System = 1;
	/**
	 * @var int Справочник
	 */
	const Reference = 2;
	/**
	 * @var int Событие
	 */
	const Event = 3;
	/**
	 * @var int Статус
	 */
	const Status = 4;
	/**
	 * @var int Файл
	 */
	const File = 5;
	/**
	 * @var int Заявка
	 */
	const Order = 6;

	/**
	 * Получение экземпляра класса
	 *
	 * @return ExchangeName
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_exchange_name", [ "id", "name" ]
		);
	}
}