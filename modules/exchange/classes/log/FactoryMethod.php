<?php

namespace app\modules\exchange\classes\log;
/**
 * Реализация фабричного метода
 *
 * @package app\modules\exchange\classes\log
 */
trait FactoryMethod {
	/**
	 * Возвращает экземпляр собственного класса
	 *
	 * @return object
	 */
	public static function o() {

		return self::getInstanse();
	}

	/**
	 * Генерация экземпляра собственного класса
	 *
	 * @return self
	 */
	abstract protected function getInstanse();
}