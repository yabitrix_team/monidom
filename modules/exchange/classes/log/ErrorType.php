<?php

namespace app\modules\exchange\classes\log;
/**
 * Тип ошибки
 *
 * @package app\modules\exchange\classes\log
 */
class ErrorType {
	/**
	 * @use getInstanse
	 */
	use Record;
	/**
	 * @var int Служебное
	 */
	const system = 1;
	/**
	 * @var int ТБД
	 */
	const tbd = 2;
	/**
	 * @var int Yii
	 */
	const yii = 3;

	/**
	 * Получение экземпляра класса
	 *
	 * @return ErrorType
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_error_type", [ "id", "name" ]
		);
	}
}