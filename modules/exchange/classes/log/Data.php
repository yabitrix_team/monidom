<?php

namespace app\modules\exchange\classes\log;
/**
 * Данные
 *
 * @package app\modules\exchange\classes\log
 */
class Data {
	/**
	 * @use getInstanse
	 */
	use Record;

	/**
	 * Получение экземпляра класса
	 *
	 * @return Data
	 */
	protected function getInstanse() {

		return new self(
			"pcm_log_data", [ "id", "log_id", "data", "query", "file" ]
		);
	}
}