<?php

namespace app\modules\exchange\classes;

use Yii;

class GetProcedure {
	const SITE   = '1C';
	const SYSTEM = '1S';
	public    $sql;
	public    $select;
	public    $db               = null;
	protected $procedureTypes   = [
		'CREATE_OBJECT_TO'           => 1,
		'GET_OBJECT_FROM'            => 2,
		'GET_OBJECTS_ROW_FROM'       => 3,
		'GET_OBJECTS_ROWSCOUNT_FROM' => 4,
		'SAVE_OBJECT_TO'             => 5,
		'OBJECT_IS_OBTAINED_FROM'    => 6,
		'ADD_OBJECTROWS_TO'          => 7,
	];
	protected $procedureName    = null;
	protected $current_type     = null;
	protected $procedure;
	protected $procedure_params = [];
	private   $root_name        = null;

	function __construct( $procedureName = null, $type = null, $from_1c = false ) {

		if ( isset( $procedureName ) && isset( $type ) ) {
			$this->root_name     = $procedureName;
			$this->current_type  = $type;
			$this->procedureName = $this->buildProcedureName( false, $from_1c );
		}
		$this->db = Yii::$app->tdb;
	}

	function buildProcedureName( $params = false, $from_1c = false ) {

		$type = $this->current_type;
		/**
		 * $this->$type
		 *
		 * данное обращение вызовет "магический" метод __get данного класса,
		 * в котором, в зависимости от параметра запроса (его типа),
		 * будет обработано и возвращено соответствующее значение.
		 */
		if ( $params === false ) {
			return $this->root_name . "_" . $this->$type . "_" . ( $from_1c ? $this::SYSTEM : $this::SITE );
		}

		return $this->root_name . "_" . $this->$type . "_" . ( $from_1c ? $this::SYSTEM :
				$this::SITE ) . "(" . $this->convertParams( $params ) . ")";
	}

	function convertParams( $param = [] ) {

		if ( ! empty( $param ) ) {
			/** @var string $mysql_func_param_list */
			$mysql_func_param_list = '';
			foreach ( $param as $paramData ) {
				$paramData = array_change_key_case( $paramData, CASE_UPPER );
				if ( isset( $paramData['TYPE'] ) ) {
					switch ( $paramData['TYPE'] ) {
						case "OUT":
						case "out":
							$mysql_func_param_list .= "@" . $this->clearOutVarName( $paramData['DATA'] ) . ",";
							break;
						case "IN":
						case "in":
						default:
							$mysql_func_param_list .= $this->clearInData( $paramData['DATA'], ( isset( $paramData['ARRAY_TYPE'] ) ?
									$paramData['ARRAY_TYPE'] : 'array' ) ) . ',';
							break;
					}
				}
			}

			return trim( $mysql_func_param_list, ", " );
		}

		return false;
	}

	function clearOutVarName( $outName = null ) {

		if ( ! is_null( $outName ) ) {
			switch ( strtoupper( gettype( $outName ) ) ) {
				case "BOOLEAN":
				case "INTEGER":
				case "DOUBLE":
				case "ARRAY":
				case "OBJECT":
				case "RESOURCE":
				case "NULL":
				case "UNKNOWN TYPE":
					return false;
					break;
				case "STRING":
					// todo: обработка запроса
//					return $DB->ForSql( $outName );
					return $outName;
					break;
			}
		}

		return false;
	}

	function clearInData( $data = null, $array_type = 'JSON' ) {

		$strNull = <<<TXT
NULL
TXT;
		switch ( strtolower( gettype( $data ) ) ) {
			case "object":
			case "resource":
			case "unknown type":
				return get_resource_type( $data );
				break;
			case "integer":
				return (int) $data;
				break;
			case "null":
				return $strNull;
				break;
			case "boolean":
				return $data ? 1 : 0;
			case "double":
			case "string":
				// todo: обработка запроса
				//			return "'" . $DB->ForSql( $data ) . "'";
				return "'" . $data . "'";
				break;
			case "array":
				switch ( strtoupper( $array_type ) ) {
					case false:
					case null:
						return false;
						break;
					case 'JSON':
						$data = json_encode( $data );
						break;
					case 'ARRAY':
					default:
						$data = serialize( $data );
						break;
				}
				// todo: обработка запроса
//				return "'" . $DB->ForSql( $data ) . "'";
				return "'" . $data . "'";
				break;
		}

		return false;
	}

	static function getType( $type = 0 ) {

		$self = new self;

		return $self->$type;
	}

	/**
	 * @return null
	 */
	public function getProcedureName() {

		return $this->procedureName;
	}

	/**
	 * @return array
	 */
	public function getProcedureParams() {

		return $this->procedure_params;
	}

	public function getCurrentProcedureParams() {

		return $this->procedure_params;
	}

	function showTypes( $show = false ) {

		if ( $show ) {
			echo "<pre>";
			print_r( $this->procedureTypes );
			echo "</pre>";

			return false;
		}

		return $this->procedureTypes;
	}

	function __get( $name ) {

		if ( is_numeric( $name ) && 0 < $name ) {
			return array_search( $name, $this->procedureTypes );
		} elseif ( 0 < strlen( $name ) ) {
			return $this->procedureTypes[ $name ];
		}

		return false;
	}

	function getSql( $params = [] ) {

		$procedure = $this->getProcedure( $this->root_name, $this->current_type, $params );
		$this->sql = "CALL $procedure;";
		if ( ! empty( $this->procedure_params ) ) {
			$getOut = '';
			foreach ( $this->procedure_params as $param ) {
				if ( strtoupper( $param['PARAMETER_MODE'] ) == 'OUT' ) {
					$getOut .= '@' . $param['PARAMETER_NAME'] . ',';
				}
			}
			$getOut = trim( $getOut, ', ' );
			if ( 0 < strlen( $getOut ) ) {
				$this->select = "SELECT $getOut;";
			}
		} else {
			// todo: throw new exception
		}

		return $this;
	}

	function getProcedure( $procedureName = false, $n = null, $params = [], $from_1c = false ) {

		if ( $procedureName && ! is_null( $n ) ) {
			$this->root_name    = $procedureName;
			$call_procedure_str = $this->buildProcedureName( $params, $from_1c );
			$this->getTheseByProcedureName( $from_1c );

			return $call_procedure_str;
		}

		return false;
	}

	function getTheseByProcedureName( $from_1c = false ) {

		$pn = isset( $this->procedureName ) ? $this->procedureName : $this->buildProcedureName( false, $from_1c );
		// todo: cache?
//		$cache = new CPHPCache();
//		$ttl   = 60 * 60 * 24; // 1 day
//		$key   = md5( $pn );
//		/** todo: delete this */
//		$cache->CleanDir('transaction');
//		if ( $cache->InitCache( $ttl, md5( $key ), 'transaction' ) ) {
//			$this->procedure_params = $cache->GetVars();
//		} elseif ( $cache->StartDataCache() ) {
//			$sql = <<<SQL
//SELECT
//  PARAMETER_NAME,
//  ORDINAL_POSITION,
//  PARAMETER_MODE,
//  DATA_TYPE,
//  CHARACTER_MAXIMUM_LENGTH
//FROM information_schema.parameters
//WHERE SPECIFIC_NAME = '$pn';
//SQL;
//			$rs  = $this->db->query( $sql );
//			while ( $rs && $ar = $rs->fetch() ) {
//				$this->procedure_params[ $ar['PARAMETER_NAME'] ] = $ar;
//			}
		$params = $this->db->createCommand( "SELECT
								  PARAMETER_NAME,
								  ORDINAL_POSITION,
								  PARAMETER_MODE,
								  DATA_TYPE,
								  CHARACTER_MAXIMUM_LENGTH
								FROM information_schema.parameters
								WHERE SPECIFIC_NAME=:pn" )
		                   ->bindValue( ":pn", $pn )
		                   ->queryAll();
		foreach ( $params as $ar ) {
			$this->procedure_params[ $ar['PARAMETER_NAME'] ] = $ar;
		}
//			$cache->EndDataCache( $this->procedure_params );
//		}
		return $this->procedure_params;
	}

	function getCurrentParams( $addProcedureSymbol = false ) {

		if ( ! empty( $this->procedure_params ) ) {
			$list = [];
			foreach ( $this->procedure_params as $paramName => $param ) {
				array_push( $list, ( $addProcedureSymbol ? $addProcedureSymbol . $paramName : $paramName ) );
			}

			return $list;
		}

		return false;
	}
}