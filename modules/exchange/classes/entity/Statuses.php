<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;

class Statuses extends Core {
	protected $modelName      = 'Statuses';
	protected $procedureName  = 'spravochnik_statusov';
	protected $fields         = [
		"guid",
		"progress",
		"name",
		"created_at",
		"active",
	];
	protected $maxRunTimeFlow = 300;
	protected $exchange_name  = "Reference";

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "name"       => "p_Naimenovanie",
				                       "guid"       => "p_ID_1C",
				                       "active"     => "p_Status_aktivnosti",
				                       "created_at" => "p_DateAdd",
				                       "progress"   => "p_Pozitsiya_dlya_sayta",
			                       ]
		);
	}
}
