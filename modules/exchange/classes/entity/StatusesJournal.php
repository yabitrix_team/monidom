<?php
namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\models\ExchangeStatusesJournal;
use app\modules\app\v1\models\FCM;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal as AppStatusesJournal;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\notification\models\MessageType;
use Exception;
use app\modules\exchange\classes\Core;
use app\modules\exchange\models\Requests;
use app\modules\notification\models\MessageStatus;
use Yii;

class StatusesJournal extends Core
{
    protected $modelName     = "StatusesJournal";
    protected $procedureName = 'Vygruzka_statusov';
    protected $order         = [];
    protected $fields        = [
        "request_id",
        "status_id",
    ];
    protected $exchange_name = "Status";

    function getAssociated()
    {
        return array_merge(
            parent::getAssociated(),
            [
                "request_guid" => "p_Zayavka",
                "request_num"  => "p_Nomer_zayavki_sayta",
                "status_id"    => "p_Status",
                "created_at"   => "p_Period",
                "guid"         => "p_ID_1C",
            ]
        );
    }

    /**
     * @param array $data
     *
     * @return array|bool
     * @throws \yii\db\Exception
     * @throws Exception
     */
    function after_getObjectFrom($data = [])
    {
        $values = $this->mapValues($data);
        if (
            ($this->order = (new Requests())->getOneByFilter(
                [
                    "or" => [
                        "guid" => $values['request_guid'],
                        "num"  => $values['request_num'],
                    ],
                ]
            ))
            && ($status = (new Statuses())->getByGuid($values['status_id']))
        ) {
            // исключаем сохранение и триггеры по повторным статусам
            if (
            (new AppStatusesJournal())->getOneByFilter(
                [
                    "request_id" => $this->order["id"],
                    "status_id"  => $status["id"],
                ]
            )
            ) {
                return $data;
            }
            if (
            (new AppStatusesJournal())->add(
                [
                    "request_id" => $this->order["id"],
                    "status_id"  => $status["id"],
                    "created_at" => $values["created_at"],
                ]
            )
            ) {
                //выстрел в сокет для ЛК
                Yii::$app->notify->addMessage(
                    [
                        "type_id"   => 1,
                        "status_id" => MessageStatus::instance()->new,
                        "order_id"  => $this->order["id"],
                    ],
                    true
                );
                // разрешение подписи 2 пакета в агенте
                if (
                in_array(
                    $data['p_Status'],
                    [
                        'abfa1724-c3fd-4b7f-9631-7572288a165b', // Одобрено
                        '74ed5b21-af9b-44f3-bda5-85fde6847b76', // Ожидает подписания договора
                        'ff126df5-e381-4312-b4f3-8863314038b2', // Клиент получает деньги
                        '6818753f-7d4c-45e6-ad29-5f98db749508'    // Деньги выданы
                    ]
                )
                ) {
                    if (
                    !Yii::$app->notify->addMessage(
                        [
                            "type_id"  => MessageType::instance()->cansignature,
                            "order_id" => $this->order["id"],
                            "message"  => json_encode(["num" => $this->order['num']], JSON_FORCE_OBJECT),
                        ],
                        true
                    )
                    ) {
                        throw new Exception(
                            "Не удается отправить cansignature сообщение в сокет о сформированных документах (пак 2) для агента по заявке с num={$request["num"]}"
                        );
                    }
                }
                //очистка полей и файлов заявки при опредленных статусах
                $cleaningStatuses = Yii::$app->params['core']['requests']['cleaningStatuses'];
                if (!isset($cleaningStatuses)) {
                    throw new Exception(
                        'Не удалось произвести очистку полей и файлов заявки при получении определенных статусов, так как в параметрах не указаны статусы очистки'
                    );
                }
                if (in_array($data['p_Status'], $cleaningStatuses)) {
                    $resClearReq = Tools::clearRequest($this->order['id'], $this->order['code']);
                    if (is_string($resClearReq)) {
                        throw new Exception($resClearReq);
                    }
                }
                //подготовка статусов для МП (необходимо во всех кабинетах, для продолжения работы с заявкой в МП)
                $assocStatuses = Yii::$app->params['mp']['settings']['assocStatuses'];
                if (!is_array($assocStatuses)) {
                    throw new Exception(
                        'В параметрах не указаны или указаны в некорректном формате значения ассоциации статусов заявки со статусами для МП ( assocStatuses ), в связи с этим при обмене статусов не удалось установить статус заявки для МП: код заявки - '.
                        $this->order["id"]
                    );
                }
                $identifier = $assocStatuses[$data['p_Status']];
                if (key_exists(trim($data['p_Status']), $assocStatuses)) {
                    //добавление статуса заявки МП
                    $modelStatusMp = new StatusesMP();
                    $statusMP      = $modelStatusMp->getByIdentifier($identifier);
                    $resSMPJ       = (new StatusesMPJournal())->add(
                        [
                            'request_id'   => $this->order["id"],
                            'status_mp_id' => $statusMP['id'],
                        ]
                    );
                    if (empty($resSMPJ)) {
                        throw new Exception(
                            'Во время обмена статусами заявки произошла ошибка записи связи заявки со статусом для МП: код заявки - '.
                            $this->order["id"].
                            ', статуса для мп - '.
                            $identifier
                        );
                    }
                }
                //подготовка с последующей отправкой пуш-уведомлений для МП
                $pushByStatus = \Yii::$app->params['mp']['settings']['pushByStatus'];
                if (!is_array($pushByStatus)) {
                    throw new Exception(
                        'В параметрах не указаны или указаны в некорректном формате значения ассоциации статусов заявки с текстами для пуш-уведомлений ( pushByStatus ), в связи с этим не удается произвести отправку пуш-уведомлений в мобильное приложение во время обмена: код заявки - '.
                        $this->order["id"].
                        ', статуса для мп - '.
                        $identifier
                    );
                }
                if (
                    key_exists(trim($data['p_Status']), $pushByStatus)
                    && !empty($this->order['requests_origin_id'])
                    && ($requestOrigin = (new RequestsOrigin())->getOne($this->order['requests_origin_id']))
                    && $requestOrigin['identifier'] == RequestsOrigin::IDENTIFIER_MOBILE
                ) {
                    $pushTitle = \Yii::$app->params['mp']['settings']['pushTitle'];
                    $modelFCM  = new FCM(['scenario' => FCM::SCENARIO_PUSH_STATUS]);
                    $arDataFCM = [
                        'user_id'      => $this->order['client_id'],
                        'title'        => !empty($pushTitle) ? $pushTitle : 'CarMoney',
                        'message'      => $pushByStatus[$data['p_Status']],
                        'request_code' => $this->order['code'],
                        'status_mp'    => $identifier,
                    ];
                    $modelFCM->load($arDataFCM, '');
                    $resultFCM = $modelFCM->send();
                    if (is_a($resultFCM, \Exception::class)) {
                        throw new Exception(
                            'Во время обмена статусами заявки не удалось отправить пуш-уведомление по заявке из мобильного приложения, произошла ошибка отправки, ознакомтесь с логами по адресу: runtime/logs/push_error.log Доп. информация: код заявки - '.
                            $this->order["id"].
                            ', статуса для мп - '.
                            $identifier.
                            ', результат отправки: '.
                            $resultFCM->getMessage()
                        );
                    }
                    if (is_a($resultFCM, FCM::class)) {
                        throw new Exception(
                            'Во время обмена статусами заявки не удалось отправить пуш-уведомление по заявке из мобильного приложения, произошла ошибка валидации загружаем данных в модель: '.
                            implode(' ', array_values($resultFCM->firstErrors)).
                            '. Доп. информация: код заявки - '.
                            $this->order["id"].
                            ', статуса для мп - '.
                            $identifier
                        );
                    }
                }

                return $data;
            }
        }

        return false;
    }

    /**
     * @param $key
     *
     * @throws \yii\db\Exception
     */
    public function after_obtain($key)
    {
        $savedStatuses = new ExchangeStatusesJournal();
        if ($status = $savedStatuses->getOneByFilter(["ID_1C" => $key])) {
            $savedStatuses->delete($status["id"]);
        }
    }

    /**
     * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
     *
     * @return string
     */
    public function getOrderKeyGUID()
    {
        return "p_Zayavka";
    }

    /**
     * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
     *
     * @return string
     */
    public function getOrderKeyID()
    {
        return "p_Nomer_zayavki_sayta";
    }
}