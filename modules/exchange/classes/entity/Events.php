<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\models\Events as AppEvents;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\GetProcedure;
use app\modules\exchange\models\Requests;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\Users;
use Exception;

/**
 * Class repeatedRequestDocuments
 *
 * Набор кодов:
 * 101 - подписан пакет документов номер 1
 * 102 - подписан пакет документов номер 2
 *
 * 201 - переформирование пакета док-в 1
 * 202 - переформирование пакета док-в 2
 *
 * 301 - редактирование заявки на втором шаге
 * 302 - редактирование заявки на третьем шаге
 * 303 - редактирование заявки на четвертом шаге
 * 310 - редактирование заявки на четвертом шаге
 *
 * страница формирования заявки удаленным колл-центром
 * 501 - клиент одобрен
 * 502 - клиент не одобрен
 */
class Events extends Core {
	static    $codeEventFirstPack    = 101;
	static    $codeEventSecondPack   = 102;
	static    $codeEventReFirstPack  = 201;
	static    $codeEventReSecondPack = 202;
	protected $procedureName         = 'Obmen_sobytiyami';
	protected $allEvents             = [];
	protected $idEvent               = 0;
	protected $idRequest             = 0;
	protected $siteKeySave           = "num";
	protected $exchange_name         = "Event";
	/** @var array - набор кодов и описание типов событий */
	private $listCodes = [
		101 => [ "description" => "подписан пакет документов номер 1" ],
		102 => [ "description" => "подписан пакет документов номер 2" ],
		201 => [ "description" => "переформирование пакета док-в 1" ],
		202 => [ "description" => "переформирование пакета док-в 2" ],
		301 => [ "description" => "Запрос на изменение шаг 2" ],
		302 => [ "description" => "Запрос на изменение шаг 3" ],
		303 => [ "description" => "Запрос на изменение шаг 4" ],
		310 => [ "description" => "Подтверждение приемы редактирования" ],
		501 => [ "description" => "клиент одобрен" ],
		502 => [ "description" => "клиент не одобрен" ],
	];

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "num"     => "p_ID_1S", // GUID
				                       "code"    => "p_KodSobytiya",
				                       "name"    => "p_Vid_sobytiya",
				                       "request" => "p_Zayavka", // GUID
				                       "user"    => "p_Podpisant", // GUID
				                       "info"    => "p_Opisanie",
				                       "guid"    => "p_ID_1C", // всегда пустой
			                       ]
		);
	}

	/**
	 * @return array
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	public function getDataForCreate() {

		$this->allEvents = array_column( ( new AppEvents() )->getAll(), "id", "code" );
		$requestsEvents  = new RequestsEvents();
		if ( $event = $requestsEvents->getOneByFilter( [
			                                               "event_id"  => [
				                                               $this->allEvents[ static::$codeEventFirstPack ],
				                                               $this->allEvents[ static::$codeEventSecondPack ],
				                                               $this->allEvents[ static::$codeEventReFirstPack ],
				                                               $this->allEvents[ static::$codeEventReSecondPack ],
			                                               ],
			                                               "is_sended" => 0,
		                                               ] )
		) {
			if ( $request = ( new Requests() )->getOne( $event["request_id"] ) ) {
				$this->idEvent   = $event["event_id"];
				$this->idRequest = $event["request_id"];

				// name формируется в before_createObjectTo
				return [
					"num"     => GUIDv4(),
					"code"    => array_flip( $this->allEvents )[ $event["event_id"] ],
					"request" => $request["guid"],
					"user"    => $request["user_id"],
				];
			} else {
				throw new Exception( "Заявка id=" . $event["request_id"] . " не найдена при отправке события id=" . $event["event_id"] );
			}
		}
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 * @throws \yii\db\Exception
	 */
	function before_createObjectTo( $data = [] ) {

		if ( $data['code'] && $this->listCodes[ $data['code'] ]['description'] ) {
			$data['name'] = $this->listCodes[ $data['code'] ]['description'];
		} else {
			$data['name'] = 'Код события не определился';
		}
		if ( $user = ( new Users )->getOne( $data['user'] ) ) {
			$data['user'] = $user['guid'];
		}

		return $data;
	}

	/**
	 * @param $key
	 *
	 * @throws \yii\db\Exception
	 */
	function after_saveObjectTo( $key ) {

		( new RequestsEvents() )->update( "request_id = " . $this->idRequest . " and event_id = " . $this->idEvent, [ "is_sended" => 1 ] );
	}

	/**
	 * @param bool $code
	 * @param bool $request
	 * @param bool $user
	 * @param bool $info
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function sendTo1C( $code = false, $request = false, $user = false, $info = false ) {

		$c      = new self();
		$values = [
			"num"     => GUIDv4(),
			"code"    => $code,
			"request" => $request,
			"user"    => $user,
			"info"    => $info,
		];
		$flow   = rand( 1, 100 );
		$cnt    = 0;
		$c->logInit();
		do {
			$res = $c->run( GetProcedure::getType( 'CREATE_OBJECT_TO' ), $flow, true, $values );
			$cnt ++;
		} while ( ! $res );
		$values['flow']     = $flow;
		$values['flow_cnt'] = $cnt;
		$this->logData( [ "data" => $values ] );
		$c->logSave();

		return $res;
	}

	function after_getObjectFrom( $data = [] ) {

		return false;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_Zayavka";
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
	 *
	 * @return string
	 */
	public function getOrderKeyID() {

		return "p_ID_1S";
	}
}