<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\PhotosTrait;
use Yii;

/**
 * Class PhotoDocs отправка фото документов в ТБД (док-ты: паспорт, ПТС, СТС)
 */
class PhotoDocs extends Core {
	use PhotosTrait;
	protected $procedureName = 'Foto_PTS_STS_i_pasport';
	protected $exchange_name = "File";
	protected $siteKeySave   = "num";

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "guid"       => "p_Zayavka",
				                       "num"        => "p_ID_1S",
				                       "user_id"    => "p_Podpisant",
				                       "created_at" => "p_Data_zagruzki",
				                       "file"       => "p_Fayl",
				                       "type"       => "p_Vid_dokumenta",
				                       "name"       => "p_Naimenovanie",
			                       ]
		);
	}

	protected function getEventID() {

		return yii::$app->params["exchange"]["events"]["photoDocs"];
	}

	protected function getBinds() {

		return [
			"foto_sts"      => "Изображения СТС",
			"foto_pts"      => "Изображения ПТС",
			"foto_passport" => "Изображения паспорта",
			"foto_card"     => "Изображения банковской карты",
		];
	}

	protected function getSiteKeySave() {

		return $this->siteKeySave;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
	 *
	 * @return string
	 */
	public function getOrderKeyID() {

		return "p_ID_1S";
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_Zayavka";
	}
}