<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\exchange\classes\Core;
use app\modules\payment\v1\models\base\Payment;

class PaymentsCloud extends Core {
	protected $procedureName = 'Ochered_Cloudpayments';
	protected $exchange_name = "Event";
	protected $siteKeySave   = "id";

	function getAssociated() {

		return array_merge( parent::getAssociated(), [
			"id"          => "p_ID_1S",
			"transaction" => "p_TransactionID",
			"sum"         => "p_Amount", // сумма без комиссии !
			"currency"    => "p_Currency",
			"contract"    => "p_AccountID", // номер договора клиента ?
			"name"        => "p_Name",
			"email"       => "p_Email",
			"date"        => "p_DateTime",
			"ip"          => "p_lpAdress",
			"country"     => "p_lpCountry",
			"city"        => "p_lpCity",
			"region"      => "p_lpRegion",
			"action"      => "p_Status",
			"vendor"      => "p_PlatezhnayaSistema" // платежная система (cloudpayments:w1pay)
		] );
	}

	/**
	 * Выгрузка платежей в МФО
	 *
	 * Условия выгрузки:
	 * - в pay_payments поле sended=0
	 * - по платежу есть запись в pay_cloud с типом pay и статусом Completed
	 *
	 * @return array|bool|false
	 * @throws \yii\db\Exception
	 */
	function getDataForCreate() {

		$sql = "select 
				p.`id`,
				p.`transaction`,
				cur.`code` currency,
				p.`contract`,
				c.`name`,
				c.`email`,
				c.`date`,
				c.`ip`,
				c.`country`,
				c.`city`,
				c.`region`,
				c.`action`,
				v.`code` vendor,
				c.`data`
			from `pay_payment` p 
				left join `pay_cloud` c 
					on c.`payment`=p.`id`
				left join `pay_vendor` v 
					on v.`id`=p.`vendor`
				left join `currency` cur 
					on cur.`id`=p.`currency`
			where p.`sended`=0 and c.`action`='pay' and c.`status`='Completed'
		";
		if ( $payment = $this->getDB()->createCommand( $sql )->queryOne() ) {
			Log::info( [
				             "contract" => $payment["contract"],
				             "text"     => print_r( $payment, true ),
			             ], "pay.cp.exchange" );

			return $payment;
		}

		return false;
	}

	function before_createObjectTo( $values = [] ) {

		$values["data"] = json_decode( $values["data"], true );
		$values["data"] = is_array( $values["data"] ) ? $values["data"] : [];
		$values["sum"]  = $values["data"]["sum"] ? str_replace( " ", "", $values["data"]["sum"] ) : 0;

		return $values;
	}

	/**
	 * @param $key
	 *
	 * @throws \yii\db\Exception
	 * @throws \Exception
	 */
	function after_saveObjectTo( $key ) {

		if ( $payment = ( new Payment )->getOne( $key ) ) {
			( new Payment )->update( $key, [ "sended" => 1 ] );
		} else {
			throw new \Exception( "Платеж с id=" . $key . " не найден" );
		}
	}
}