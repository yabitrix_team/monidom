<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\exchange\classes\Core;
use app\modules\exchange\models\Requests as AppRequests;
use Exception;

class RequestCommission extends Core {
	protected $procedureName = 'Kommisiya_partnera';
	protected $exchange_name = "Reference";
	protected $modelName     = "RequestCommission";
	protected $siteKeyUpdate = "request_id";
	protected $fields        = [
		"request_id",
		"is_null",
		"commission_contract",
		"commission_cash_withdrawal",
		"monthly_fee",
		"received",
		"created_at",
	];

	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "guid"                       => "p_ID_1C",
				                       "line"                       => "p_LineNo",
				                       "request"                    => "p_Zayavka", // GUID
				                       "is_null"                    => "p_Amount_of_loan",
				                       "created_at"                 => "p_DateAdd",
				                       "commission_contract"        => "p_Komissiya_za_zaklyuche",
				                       "commission_cash_withdrawal" => "p_Komissiya_za_vydachu",
				                       "monthly_fee"                => "p_Komissiya_za_mesyachny",
				                       "received"                   => "p_Vyplachena",
			                       ]
		);
	}

	/**
	 * @param array $data
	 *
	 * @return array|null
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	function after_getObjectFrom( $data = [] ) {

		$values         = $this->mapValues( $data );
		$checkDate      = ( new \DateTime() )->modify( "-1 hour" );
		$commissionDate = ( new \DateTime( $values["created_at"] ) );
		$bObtain        = $checkDate > $commissionDate;
		if ( ! strlen( $values["request"] ) ) {
			if ( $bObtain ) {
				$this->objectIsObtainedFrom( $values["guid"] );
				Log::error( [ "text" => "Удалена коммиссия " . $values["guid"] . " с пустой заявкой" ], "exchange.commission" );
			}
			throw new Exception( "guid заявки отсутствует в выгрузке" );
		}
		if ( ! $request = ( new AppRequests )->getByGuid( $values["request"] ) ) {
			if ( $bObtain ) {
				$this->objectIsObtainedFrom( $values["guid"] );
				Log::error( [ "text" => "Удалена коммиссия " . $values["guid"] . " с не существующей заявкой " . $values["request"] ], "exchange.commission" );
			}
			throw new Exception( "Заявка с guid=" . $values["request"] . " не найдена" );
		}
		$cnt = $this->getObjectsRowCountFrom( $values["guid"] );
		if ( 0 < $cnt ) {
			for ( $i = 1, $get = true; $i <= $cnt && $get; $i ++ ) {
				$params = [
					"guid" => $values["guid"],
					"line" => $i,
				];
				if ( $commission = $this->getObjectRow( $params ) ) {
					$valuesCommission = $this->mapValues( $commission );
					if ( ! $this->insertData( [
						                          "request_id"                 => $request["id"],
						                          "is_null"                    => $values["is_null"],
						                          "commission_contract"        => $valuesCommission["commission_contract"],
						                          "commission_cash_withdrawal" => $valuesCommission["commission_cash_withdrawal"],
						                          "monthly_fee"                => $valuesCommission["monthly_fee"],
						                          "received"                   => $valuesCommission["received"],
						                          "created_at"                 => $values["created_at"],
					                          ] ) ) {
						throw new Exception( "Ошибка записи/обновления комиссии с guid=" . $values["request"] );
					}
				} else {
					throw new Exception( "Не получены данные по комиссии по заявке с guid=" . $values["request"] );
				}
			}
		}

		return $data;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_Zayavka";
	}
}