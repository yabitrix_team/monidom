<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\VersionDictionaryTrait;

class Employments extends Core {
	/**
	 * Подключение трейта версионности справочников
	 */
	use VersionDictionaryTrait;
	protected $modelName      = 'Employments';
	protected $procedureName  = 'Spravochnik_Zanyatost';
	protected $fields         = [
		"guid",
		"name",
		"active",
	];
	protected $maxRunTimeFlow = false;
	protected $exchange_name  = "Reference";

	/**
	 * Метод получения названия версии справочника
	 *
	 * @return string - вернет строку с названием справочника
	 */
	public function getDictionaryName(): string {

		return 'employment';
	}

	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "name"   => "p_Naimenovanie",
				                       "guid"   => "p_ID_1C",
				                       "active" => "p_Nedeystvitelen",
			                       ]
		);
	}

	public function before_insertData( $data, &$values ) {

		$values["active"] = ! $values["active"];

		return true;
	}
}
