<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use Exception;
use app\modules\app\v1\models\Users;
use app\modules\exchange\models\Requests;

class Contracts extends Core {
	protected $modelName      = 'Contracts';
	protected $procedureName  = 'Grafik_po_dogovoru';
	protected $fields         = [
		"code",
		"request_id",
		"user_id",
		"active",
		"created_at",
	];
	protected $siteKeyUpdate  = "code";
	protected $maxRunTimeFlow = false;
	protected $exchange_name  = "Order";

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "code"       => "p_Nomer",
				                       "created_at" => "p_Data",
				                       "guid"       => "p_ID_1C",
				                       "phone"      => "p_MobilnyyTelefon",
				                       "request_id" => "p_Zayavka",
			                       ]
		);
	}

	function before_insertData( &$data = [], &$values = [] ) {

		if ( ! $user = ( new Users() )->getByLogin( $values["phone"] ) ) {
			throw new Exception( "Клиент с username=" . $values["phone"] . " не найден" );
		}
		$values["user_id"] = $user["id"];
		if ( ! $request = Requests::instance()->getOneByFilter( [ 'guid' => $values['request_id'] ] ) ) {
			throw new Exception( "Заявка с guid=" . $values["request_id"] . " не найдена" );
		}
		$values["request_id"] = $request["id"];

		return true;
	}

	function before_create( &$values = [] ) {

		$values["active"] = 1;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
	 *
	 * @return string
	 */
	public function getOrderKeyID() {

		return "p_ID_1S";
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_Zayavka";
	}
}