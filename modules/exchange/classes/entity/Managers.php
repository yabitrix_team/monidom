<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\models\Managers as AppManagers;
use app\modules\app\v1\models\ManagersPoints;
use app\modules\app\v1\models\Points;
use app\modules\exchange\classes\Core;

class Managers extends Core {
	protected $modelName            = 'Managers';
	protected $procedureName        = 'Spr_Men';
	protected $fields               = [
		"guid",
		"name",
		"last_name",
		"second_name",
		"phone",
		"email",
		"photo_id",
		"created_at",
		"active",
	];
	protected $get_row_prefix       = 'SvyazannyeTochki';
	protected $get_row_count_prefix = 'SvyazannyeTochki';
	protected $add_row_prefix       = 'SvyazannyeTochki';
	protected $exchange_name        = "Reference";

	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "guid"        => "p_ID_1C",
				                       "created_at"  => "p_DateAdd",
				                       "last_name"   => "p_Familiya",
				                       "name"        => "p_Imya",
				                       "second_name" => "p_Otchestvo",
				                       "phone"       => "p_TelefonMobilnyy",
				                       "photo_id"    => "p_Foto",
				                       "email"       => "p_AdresElPochty",
				                       "active"      => "p_Nedeystvitelen",
				                       "line"        => "p_LineNo",
			                       ]
		);
	}

	/**
	 * @param array $data
	 *
	 * @return array|bool|null
	 * @throws \yii\db\Exception
	 * @throws \Exception
	 */
	function after_getObjectFrom( $data = [] ) {

		$managerData           = $this->clearFields( $this->mapValues( $data ) );
		$managerData["active"] = ! $managerData["active"];
		/** create file */
		//todo: file
		//			if ( $elData['UF_FILE'] ) {
		//				$f_code    = md5( microtime() . Yii::$app->security->generateRandomString( 30 ) );
		//				$fileArray = [
		//					"name"        => $f_code . ".jpg",
		//					"MODULE_ID"   => "main",
		//					"description" => "Фотография менеджера",
		//					"content"     => base64_decode( $elData['UF_FILE'] ),
		//					"del"         => "Y",
		//				];
		//				$fileDir   = '/images/managers/';
		//				$FID       = CFile::SaveFile( $fileArray, $fileDir );
		//				if ( $FID ) {
		//					$elData['UF_FILE'] = $FID;
		//					$fullPath          = $_SERVER['DOCUMENT_ROOT'] . "/upload/" . $fileDir;
		//					/** если задача запускадась по CRON, то необходимо точно прописать права для папки пользователю Битрих */
		//					chown( $fullPath, 'bitrix' );
		//					chgrp( $fullPath, 'bitrix' );
		//					chown( $fullPath . "/" . $fileArray['name'], 'bitrix' );
		//					chgrp( $fullPath . "/" . $fileArray['name'], 'bitrix' );
		//					clearstatcache( true, $fullPath );
		//					clearstatcache( true, $fullPath . "/" . $fileArray['name'] );
		//				} else {
		//					$this->logYiiError( "Managers::after_getObjectFrom() ошибка сохранения файла \n" . print_r( $fileArray, true ) . "\n " . $fileDir );
		//				}
		//			}
		$manager       = new AppManagers();
		$managerPoints = new ManagersPoints();
		$point         = new Points();
		if ( $arManager = $manager->getByGuid( $managerData["guid"] ) ) {
			$idManager = $arManager["id"];
			if ( ! $manager->update( $idManager, $managerData ) ) {
				$this->logYiiError( "Managers::after_getObjectFrom() update error \n" . print_r( $managerData, true ) );

				return false;
			}
		} else {
			if ( ! $idManager = $manager->add( $managerData ) ) {
				$this->logYiiError( "Managers::after_getObjectFrom() add error \n" . print_r( $managerData, true ) );

				return false;
			}
		}
		$cnt = $this->getObjectsRowCountFrom( $managerData["guid"] );
		if ( 0 < $cnt ) {
			$managerPoints->delete( "[[manager_id]] = " . $idManager );
			for ( $i = 1, $get_data = true; $i <= $cnt && $get_data; $i ++ ) {
				if ( $get_data = $this->getObjectRow( [
					                                      "guid" => $managerData["guid"],
					                                      "line" => $i,
				                                      ] ) ) {

					if ( ! $arPoint = $point->getByGuid( $get_data['p_Tochka'] ) ) {
						$this->logYiiError( "Managers::after_getObjectFrom() point not found \n" . $get_data['p_Tochka'] );

						return false;
					}
					if ( $managerData["active"] ) {
						if ( ! $managerPoints->add( [
							                            "point_id"   => $arPoint["id"],
							                            "manager_id" => $idManager,
							                            "type"       => ( $get_data['p_Kod_dolzhnosti'] == 1 ?
								                            'mrp' : ( 2 ? 'rp' : null ) ),
						                            ] ) ) {
							$this->logYiiError( "Managers::after_getObjectFrom() manager points add error \n" . print_r( $get_data, true ) );

							return false;
						}
					}
				}
			}
		}

		return $data;
	}

	/**
	 * @param null $manager_GUID - XML_ID менеджера
	 */
	//	protected function resetManager( $manager_GUID = null ) {
	//
	//		if ( isset( $manager_GUID ) ) {
	//			$rs = CIBlockElement::GetList(
	//				[], [
	//					  "IBLOCK_ID" => $this->IBlockID,
	//					  [
	//						  "LOGIC" => "OR",
	//						  [
	//							  "PROPERTY_MANAGERS" => $manager_GUID,
	//						  ],
	//						  [
	//							  "PROPERTY_REGIONAL_MANAGERS" => $manager_GUID,
	//						  ],
	//					  ],
	//				  ]
	//			);
	//			while ( $rs && $ar = $rs->GetNextElement() ) {
	//				$field             = $ar->GetFields();
	//				$managers          = $ar->GetProperty( "MANAGERS" );
	//				$regional_managers = $ar->GetProperty( "REGIONAL_MANAGERS" );
	//				if ( in_array( $manager_GUID, $managers['VALUE'] ) ) {
	//					unset( $managers['VALUE'][ array_search( $manager_GUID, $managers['VALUE'] ) ] );
	//					$this->setPointsParams( $field['XML_ID'], "MANAGERS", array_unique( $managers['VALUE'] ) );
	//				}
	//				if ( in_array( $manager_GUID, $regional_managers['VALUE'] ) ) {
	//					unset(
	//						$regional_managers['VALUE'][ array_search(
	//							$manager_GUID, $regional_managers['VALUE']
	//						) ]
	//					);
	//					$this->setPointsParams( $field['XML_ID'], "REGIONAL_MANAGERS", array_unique( $regional_managers['VALUE'] ) );
	//				}
	//			}
	//		}
	//	}
	//
	//	protected function setPointsParams( $point_GUID = null, $POINT_PROPERTY_NAME = null, $arValues = [] ) {
	//
	//		if ( isset( $point_GUID, $POINT_PROPERTY_NAME ) ) {
	//			if ( $ID = $this->XMLToID( $point_GUID ) ) {
	//				if (
	//				! CIBlockElement::SetPropertyValuesEx(
	//					$ID, $this->IBlockID, [
	//						   $POINT_PROPERTY_NAME => empty( $arValues ) ? '' : $arValues,
	//					   ]
	//				)
	//				) {
	//
	//				}
	//				CPHPCache::Clean( 'points_' . md5( $point_GUID . $POINT_PROPERTY_NAME ), 'points_data' );
	//
	//				return true;
	//			};
	//		}
	//
	//		return false;
	//	}
	//
	//	protected function addManager( $point_GUID = null, $manager_GUID = null, $POINT_PROPERTY_NAME = null ) {
	//
	//		if ( isset( $point_GUID, $manager_GUID, $POINT_PROPERTY_NAME ) ) {
	//			$vals = $this->getPointManagers( $point_GUID, $POINT_PROPERTY_NAME );
	//			array_push( $vals, $manager_GUID );
	//			// $this->resetManagerOnPoint($point_GUID, $manager_GUID);
	//			$this->setPointsParams( $point_GUID, $POINT_PROPERTY_NAME, array_unique( $vals ) );
	//
	//			return true;
	//		}
	//
	//		return false;
	//	}
	//
	//	protected function getPointManagers( $point_GUID = null, $POINT_PROPERTY_NAME = null ) {
	//
	//		if ( isset( $point_GUID ) ) {
	//			$rs = CIBlockElement::GetList(
	//				[], [
	//					  "IBLOCK_ID" => $this->IBlockID,
	//					  "XML_ID"    => $point_GUID,
	//				  ]
	//			);
	//			if ( $rs && $ar = $rs->GetNextElement() ) {
	//				if ( isset( $POINT_PROPERTY_NAME ) ) {
	//					$prop = $ar->GetProperty( $POINT_PROPERTY_NAME );
	//
	//					return $prop['VALUE'] ?: [];
	//				}
	//				$prop["MANAGERS"]          = $ar->GetProperty( "MANAGERS" );
	//				$prop["REGIONAL_MANAGERS"] = $ar->GetProperty( "REGIONAL_MANAGERS" );
	//
	//				return $prop;
	//			}
	//		}
	//
	//		return [];
	//	}
	//
	//	protected function rmManager( $point_GUID = null, $manager_GUID = null, $POINT_PROPERTY_NAME = null ) {
	//
	//		if ( isset( $point_GUID, $manager_GUID, $POINT_PROPERTY_NAME ) ) {
	//			$vals = $this->getPointManagers( $point_GUID, $POINT_PROPERTY_NAME );
	//			if ( in_array( $manager_GUID, $vals ) ) {
	//				unset( $vals[ array_search( $manager_GUID, $vals ) ] );
	//
	//				return $this->setPointsParams( $point_GUID, $POINT_PROPERTY_NAME, array_unique( $vals ) );
	//			}
	//
	//			return true;
	//		}
	//
	//		return false;
	//	}
	//
	//	/**
	//	 * @see resetManager
	//	 *
	//	 * @param null $managerID - XML_ID менеджера
	//	 */
	//	protected function disableManager( $managerID = null ) {
	//
	//		$this->resetManager( $managerID );
	//	}
	//
	//	protected function resetManagerOnPoint( $point_GUID = null, $manager_GUID = null ) {
	//
	//		if ( isset( $point_GUID, $manager_GUID ) ) {
	//			$arProp = $this->getPointManagers( $point_GUID );
	//			if ( in_array( $manager_GUID, $arProp['MANAGERS']['VALUE'] ) ) {
	//				unset( $arProp['MANAGERS']['VALUE'][ array_search( $manager_GUID, $arProp['MANAGERS']['VALUE'] ) ] );
	//				$this->setPointsParams( $point_GUID, "MANAGERS", array_unique( $arProp['MANAGERS']['VALUE'] ) );
	//			}
	//			if ( in_array( $manager_GUID, $arProp['REGIONAL_MANAGERS']['VALUE'] ) ) {
	//				unset(
	//					$arProp['REGIONAL_MANAGERS']['VALUE'][ array_search(
	//						$manager_GUID, $arProp['REGIONAL_MANAGERS']['VALUE']
	//					) ]
	//				);
	//				$this->setPointsParams( $point_GUID, "REGIONAL_MANAGERS", array_unique( $arProp['REGIONAL_MANAGERS']['VALUE'] ) );
	//			}
	//
	//			return true;
	//		}
	//
	//		return false;
	//	}
}
