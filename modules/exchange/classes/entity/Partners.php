<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;

class Partners extends Core {
	protected $modelName     = 'Partners';
	protected $procedureName = 'spravochnik_partnerov';
	protected $fields        = [
		"name",
		"guid",
		"active",
		"comission_percentage",
		"comission_summ",
		"created_at",
	];
	protected $exchange_name = "Reference";

	public function getAssociated() {

		return array_merge( parent::getAssociated(), [
			"comission_percentage" => "p_Commission_percentage",
			"comission_summ"       => "p_Base_summ",
			"name"                 => "p_Naimenovanie",
			"guid"                 => "p_ID_1C",
			"active"               => "p_Status_aktivnosti",
			"created_at"           => "p_DateAdd",
		] );
	}
}