<?

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\VersionDictionaryTrait;

class Regions extends Core {
	/**
	 * Подключение трейта версионности справочников
	 */
	use VersionDictionaryTrait;
	protected $modelName     = 'Regions';
	protected $siteKeyUpdate = 'kladr';
	protected $procedureName = 'spravochnik_regionov';
	protected $fields        = [
		"name",
		"guid",
		"kladr",
		"created_at",
		"active",
	];
	protected $exchange_name = "Reference";

	/**
	 * Метод получения названия версии справочника
	 *
	 * @return string - вернет строку с названием справочника
	 */
	public function getDictionaryName(): string {

		return 'region';
	}

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "kladr"      => "p_Kod_regiona",
				                       "name"       => "p_Naimenovanie",
				                       "guid"       => "p_ID_1C",
				                       "active"     => "p_Status_aktivnosti",
				                       "created_at" => "p_DateAdd",
			                       ]
		);
	}

	public function before_insertData( &$data = [], &$values = [] ) {

		if ( ! empty( $values ) ) {
			/** Добавляем 0, если код региона не форматирован: "5" -> "05" */
			$values["kladr"] = (string) str_pad( trim( $values["kladr"] ), 2, "0", STR_PAD_LEFT );
			$values["kladr"] = (string) str_pad( $values["kladr"], 13, "0" );
		}

		return true;
	}
}