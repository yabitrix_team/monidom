<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\Points;
use app\modules\app\v1\models\UserGroup;
use app\modules\app\v1\models\Users as AppUsers;
use app\modules\exchange\classes\Core;
use Exception;
use Yii;

/**
 * Class Users
 *
 * @package app\modules\exchange\classes\entity
 */
class Users extends Core {
	/**
	 * @var string
	 */
	protected $modelName = 'Users';
	/**
	 * @var string
	 */
	protected $procedureName = 'spravochnik_polzovatel';
	/**
	 * @var array
	 */
	protected $fields = [
		"guid",
		"username",
		"password",
		"point_id",
		"active",
		"first_name",
		"last_name",
		"second_name",
		"email",
		"group",
		"is_accreditated",
	];
	/**
	 * @var string
	 */
	protected $exchange_name = "Reference";

	/**
	 * @return array
	 */
	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "guid"            => "p_ID_1C",
				                       "active"          => "p_Status_aktivnosti",
				                       "created_at"      => "p_DateAdd",
				                       "username"        => "p_Login",
				                       "password"        => "p_Parol",
				                       "point_id"        => "p_Tochka",
				                       "group"           => "p_Tip_polzovatelya",
				                       "email"           => "p_Adres_el__pochty",
				                       "first_name"      => "p_Imya",
				                       "last_name"       => "p_Familiya",
				                       "second_name"     => "p_Otchestvo",
				                       "is_accreditated" => "p_Akkreditatsiya",
			                       ]
		);
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function before_getObjectFrom() {

		if ( ! (int) yii::$app->params["exchange"]["user_groups"]["partner_admin"] ) {
			throw new Exception( "Не задан параметр partner_admin в params.php. Установите id группы админов партнера и перезапустите обмен." );
		}
		if ( ! (int) yii::$app->params["exchange"]["user_groups"]["partner_user"] ) {
			throw new Exception( "Не задан параметр partner_user в params.php. Установите id группы пользователей партнера и перезапустите обмен." );
		}
		if ( ! (int) yii::$app->params["exchange"]["user_groups"]["agents"] ) {
			throw new Exception( "Не задан параметр agents в params.php. Установите id группы выездных агентов и перезапустите обмен." );
		}

		return true;
	}

	/**
	 * @param array $data
	 *
	 * @return array|null
	 * @throws \yii\base\Exception
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	public function after_getObjectFrom( $data = [] ) {

		$userData = $this->clearFields( $this->mapValues( $data ) );
		array_walk(
			$userData, function( &$v, $k ) {

			$v = ( $k != 'created_at' ) ? str_replace( [ '¶', ' ' ], '', trim( $v ) ) : trim( $v );
		}
		);
		if ( ! strlen( $userData["email"] ) ) {
			throw new Exception( "Email пользователя не может быть пустым: " . print_r( $userData, true ) );
		}
		if ( $userData["point_id"] && ! $point = ( new Points() )->getByGuid( $userData["point_id"] ) ) {
			throw new Exception( "Не найдена точка пользователя: " . $userData["point_id"] );
		}
		$userData["point_id"] = $point["id"] ?: null;
		$transaction          = $this->getDB()->beginTransaction();
		$user                 = new AppUsers();
		$user->hashPassword( $userData["password"] );
		if ( $arUser = $user->getByGuid( $userData["guid"] ) ) {
			if ( ! $user->update( $arUser["id"], [
				'username'        => $userData["username"],
				'password_hash'   => $user->password_hash,
				'email'           => $userData["email"],
				'first_name'      => $userData["first_name"],
				'last_name'       => $userData["last_name"],
				'second_name'     => $userData["second_name"],
				"point_id"        => $userData["point_id"],
				"active"          => $userData["active"],
				"is_accreditated" => $userData["is_accreditated"],
			] ) ) {
				$transaction->rollBack();
				throw new Exception( "Ошибка обновлениия пользователя: " . $arUser["id"] );
			}
			if ( ! $userData["active"] ) {
				Log::info( [
					             "user_id"    => $arUser["id"],
					             "user_login" => $userData["username"],
				             ], "user.exchange.block" );
			} else {
				Log::info( [
					             "user_id"    => $arUser["id"],
					             "user_login" => $userData["username"],
					             "text"       => "Обновлена информация о пользователе: ".print_r( $userData, true ),
				             ], "user.exchange.update" );
			}
		} else {
			if ( ! $idUser = $user->add( [
				                             'username'        => $userData["username"],
				                             'password_hash'   => $user->password_hash,
				                             'email'           => $userData["email"],
				                             'first_name'      => $userData["first_name"],
				                             'last_name'       => $userData["last_name"],
				                             'second_name'     => $userData["second_name"],
				                             "point_id"        => $userData["point_id"],
				                             "guid"            => $userData["guid"],
				                             "active"          => $userData["active"],
				                             "is_accreditated" => $userData["is_accreditated"],
			                             ] ) ) {
				$transaction->rollBack();
				throw new Exception( "Ошибка добавления пользователя: " . print_r( $userData, true ) );
			}
			Log::info( [
				             "user_id"    => $idUser,
				             "user_login" => $userData["username"],
			             ], "user.exchange.register" );
			switch ( (int) $userData["group"] ) {
				case 1:
					$idGroup = yii::$app->params["exchange"]["user_groups"]["partner_admin"];
					break;
				case 2:
					$idGroup = yii::$app->params["exchange"]["user_groups"]["partner_user"];
					break;
				case 3:
					$idGroup = yii::$app->params["exchange"]["user_groups"]["agents"];
					break;
				default;
					$idGroup = yii::$app->params["exchange"]["user_groups"]["partner_user"];
			}
			if ( ! ( new UserGroup() )->add( [
				                                 'user_id'  => $idUser,
				                                 "group_id" => $idGroup,
			                                 ] ) ) {
				$transaction->rollBack();
				throw new Exception( "Ошибка привязки пользователя id=$idUser к группе id=$idGroup" );
			}
		}
		$transaction->commit();

		return $data;
	}
}