<?php
namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\models\Events as AppEvents;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\Users;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\DocsTrait;
use app\modules\exchange\models\Requests;
use app\modules\notification\models\MessageType;
use Exception;
use Yii;

class DocsFirstPack extends Core
{
    use DocsTrait;
    protected $procedureName  = 'Anketa_Soglasie_NBKI';
    protected $maxRunTimeFlow = 600;
    protected $exchange_name  = "File";

    public function after_getObjectFrom($data = [])
    {
        $guid = $data[$this->tdbKey];
        if ($request = (new Requests())->getByGuid($guid)) {
            if ($user = (new Users())->getOne($request['user_id'])) {
                $request['user_guid'] = $user["guid"];
            }
            $cnt = $this->getObjectsRowCountFrom($guid);
            if (0 < $cnt) {
                $requestPropertyValue = [];
                $server               = rtrim(Yii::getAlias('@app'), '/');
                $setSignatory         = true;
                for ($i = 1, $getFile = true; $i <= $cnt && $getFile; $i++) {
                    $params = [
                        "guid" => $guid,
                        "line" => $i,
                    ];
                    if ($getFile = $this->getObjectRow($params)) {
                        if (
                            $request['user_guid']
                            && $getFile['p_Podpisant'] != ''
                            && $request['user_guid'] != $getFile['p_Podpisant']
                        ) {
                            (new Events)->sendTo1C(201, $guid, $request['user_id']);

                            return $data;
                        } else {
                            // устанавливаем подписанта из док-та, если заявка была создана в 1с, и в ней не было подписанта
                            if ($request['user_guid'] == '' && $setSignatory) {
                                $setSignatory = $getFile['p_Podpisant'];
                            }
                            $decodedData = base64_decode($getFile["p_Fayl"]);
                            switch ($getFile['p_Kod_tipa_dokumenta']) {
                                case 1:
                                case 2:
                                case 4:
                                case 5:
                                    $fileDir = "/web/path/request/".$request["code"]."/doc_pack_1/";
                                    /** проверяем/создаем папку для файлов */
                                    if (!is_dir($server.$fileDir)) {
                                        mkdir($server.$fileDir, 0775, true);
                                        /** если задача запускадась по CRON, то необходимо точно прописать права для папки пользователю Битрих */
                                        clearstatcache(true, $server.$fileDir);
                                    }
                                    $fileName = $getFile['p_Naimenovanie'].".".$getFile['p_TipFayla'];
                                    $filePath = $server.$fileDir.$fileName;
                                    if (file_exists($filePath)) {
                                        unlink($filePath);
                                    }
                                    if (file_put_contents($filePath, $decodedData, LOCK_EX) !== false) {
                                        clearstatcache(true, $filePath);
                                        $transaction = $this->getDB()->beginTransaction();
                                        $filePathRel = "path/request/".$request["code"]."/doc_pack_1/".$fileName;
                                        if ($file = (new File())->getOneByFilter(["path" => $filePathRel])) {
                                            $idFile = $file["id"];
                                        } elseif (
                                        !$idFile = (new File())->add(
                                            [
                                                'name'        => $fileName,
                                                'path'        => $filePathRel,
                                                'width'       => 0,
                                                'height'      => 0,
                                                'type'        => mime_content_type($filePath),
                                                'size'        => filesize($filePath),
                                                'description' => $getFile["p_Kommentariy"]
                                                    ?: $getFile['p_Naimenovanie'],
                                                'code'        => md5(str_shuffle('file_code'.time()).microtime(true)),
                                                'sim_link'    => md5(
                                                    str_shuffle('file_sim_link'.time()).microtime(true)
                                                ),
                                            ]
                                        )
                                        ) {
                                            $transaction->rollBack();
                                            $this->logTbdError(
                                                "Первый пакет доков - Ошибка сохранения файла ".$filePath
                                            );
                                            $getFile = false;
                                            break;
                                        }
                                        $filterRequestFile = [
                                            'request_id' => $request['id'],
                                            'file_id'    => $idFile,
                                            'file_bind'  => RequestFile::BIND_DOC_PACK_1,
                                        ];
                                        if (
                                            !(new RequestFile())->getOneByFilter($filterRequestFile) &&
                                            !(new RequestFile())->add($filterRequestFile)
                                        ) {
                                            $transaction->rollBack();
                                            $this->logTbdError(
                                                "Первый пакет доков - Ошибка привязки файла к заявке ".$filePath
                                            );
                                            $getFile = false;
                                            break;
                                        }
                                        $transaction->commit();
                                        array_push(
                                            $requestPropertyValue,
                                            $fileDir.$fileName
                                        );
                                    } else {
                                        $this->logYiiError("ОШИБКА ! Ошибка записи файла  для заявки $guid( пакет 1)");
                                        $getFile = false;
                                    }
                                    break;
                                case 3:
                                    break;
                                default:
                                    $this->logTbdError("ОШИБКА !неизвестный тип док-та по заявке $guid(пакет 1)");
                                    $getFile = false;
                                    break;
                            }
                        }
                    } else {
                        $this->logTbdError("ОШИБКА! Нет документов по заявке $guid(пакет 1)");
                        $getFile = false;
                    }
                }
                /** Если возникла ошибка в получении док-в */
                if (!$getFile) {
                    /** удаляем список */
                    if (!empty($requestPropertyValue)) {
                        foreach ($requestPropertyValue as $filePathRel) {
                            if (file_exists($filePathRel)) {
                                unlink($filePathRel);
                            }
                        }
                    }
                    throw new Exception("Не получены документы по заявке guid=".$guid);
                } else {
                    // устанавливаем подписанта из док-та, если заявка была создана в 1с, и в ней не было подписанта
                    if (
                        $request['user_guid'] == '' &&
                        $setSignatory &&
                        $setUser = (new Users())->getByGuid($setSignatory)
                    ) {
                        (new Requests())->update($request["id"], ["user_id" => $setUser["id"]]);
                    }
                    //проверка установленных параметров точек
                    if (!$pointIdMP = Yii::$app->params['mp']['settings']['point_id']) {
                        throw new Exception(
                            'В параметрах не указано значение id точки мобильного приложения ( [\'mp\'][\'settings\'][\'point_id\'] )'
                        );
                    }
                    if (!$pointIdClient = Yii::$app->params['client']['settings']['point_id']) {
                        throw new Exception(
                            'В параметрах не указано значение id точки клиентского сайта ( [\'client\'][\'settings\'][\'point_id\'] )'
                        );
                    }
                    //подписание первого пакета доков, если место создания Клиент либо МП или если точка Клиент или МП
                    if (
                        (
                            (
                                !empty($request['requests_origin_id'])
                                && ($requestOrigin = (new RequestsOrigin())->getOne($request['requests_origin_id']))
                                && in_array(
                                    $requestOrigin['identifier'],
                                    [RequestsOrigin::IDENTIFIER_MOBILE, RequestsOrigin::IDENTIFIER_CLIENT]
                                )
                            )
                            || in_array($request['point_id'], [$pointIdMP, $pointIdClient])
                        )
                        && ($event = (new AppEvents())->getByCode(101))
                    ) {
                        (new RequestsEvents())->add(
                            [
                                'request_id' => $request['id'],
                                'event_id'   => $event['id'],
                                'is_sended'  => 0,
                            ]
                        );
                    }
                    $mess = [
                        "type_id"  => MessageType::instance()->firstpack,
                        "order_id" => $request['id'],
                    ];
                    if (!Yii::$app->notify->addMessage($mess, true)) {
                        throw new Exception(
                            "Не удается отправить сообщение в сокет о присвоении guid заявке с id=".$request['id']
                        );
                    }
                    if (
                    !Yii::$app->notify->addMessage(
                        [
                            "type_id"  => MessageType::instance()->sys,
                            "order_id" => $request["id"],
                            "message"  => "Получены согласия на подпись",
                        ],
                        true
                    )
                    ) {
                        throw new Exception(
                            "Не удается отправить SYS сообщение в сокет о сформированных документах (пак 1) по заявке с num={$request["num"]}"
                        );
                    }
                    // если переподписание 1 пакета
                    if ($data["p_PovtornyyPaket"] && ($event = (new AppEvents())->getByCode(830))) {
                        (new RequestsEvents())->add(
                            [
                                'request_id' => $request['id'],
                                'event_id'   => $event['id'],
                                'is_sended'  => 0,
                            ]
                        );
                        $mess = [
                            "type_id"  => MessageType::instance()->refirstpack,
                            "order_id" => $request["id"],
                            "message"  => json_encode(["num" => $request["num"]], JSON_FORCE_OBJECT),
                        ];
                        Yii::$app->notify->addMessage($mess, true);
                    }

                    return $data;
                }
            }

            return false;
        }
        throw new Exception("Заявка с guid=".$guid." не найдена");
    }
}