<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\logs\Log;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\GetProcedure;
use app\modules\app\v1\models\ExchangeStatusesJournal;
use Yii;

class CheckStatuses extends Core {
	protected $procedureName = 'Vygruzka_statusov';

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "request_guid" => "p_Zayavka",
				                       "request_num"  => "p_Nomer_zayavki_sayta",
				                       "status_id"    => "p_Status",
				                       "created_at"   => "p_Period",
				                       "guid"         => "p_ID_1C",
			                       ]
		);
	}

	/**
	 * @return array|bool|null
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
	function getObjectFrom() {

		$this->current_procedure = new GetProcedure( $this->procedureName, $this->current_procedure_type, true );
		$savedStatuses           = new ExchangeStatusesJournal();
		$checkDate               = ( new \DateTime() )->modify( '-3 min' )->format( 'Y-m-d H:i:s' );
		if ( $rows = $this->getTDB()
		                  ->createCommand( "select * from Vygruzka_statusov_Rekvizity_shapki_1C_1S where _SheduleNumber > 0 and DateAdd < '" . $checkDate . "'" )
		                  ->queryAll() ) {
			foreach ( $rows as $row ) {
				if ( $status = $savedStatuses->getOneByFilter( [ "ID_1C" => $row["ID_1C"] ] ) ) {
					$bDelete = $savedStatuses->update( $status["id"], [ "try" => $status["try"] + 1 ] );
				} else {
					$bDelete = $savedStatuses->add( [
						                                "ID_1C"                  => $row["ID_1C"],
						                                "DateAdd"                => $row["DateAdd"],
						                                "Zayavka"                => $row["Zayavka"],
						                                "Period"                 => $row["Period"],
						                                "Status"                 => $row["Status"],
						                                "Dopolnitelnoe_opisanie" => $row["Dopolnitelnoe_opisanie"],
						                                "Nomer_zayavki_sayta"    => $row["Nomer_zayavki_sayta"],
					                                ] );
				}
				if ( $bDelete ) {
					$this->objectIsObtainedFrom( $row["ID_1C"] );
				}
			}
		}

		// кладем в лог статусы которые удалим
		if ( $rowsDel = $savedStatuses->getAllByFilter( [ ">try" => 5 ]) ) {
			foreach($rowsDel as $statusForDelete){
				Log::info(["text" => print_r($statusForDelete, 1)], 'exchange.status.delete');
			}
		}

		// удаляем статусы с > 5 попытками восстановления
		$savedStatuses->delete( "try > 5" );
		$cntRows = $this->getTDB()
		                ->createCommand( "select count(*) cnt from Vygruzka_statusov_Rekvizity_shapki_1C_1S where 1" )
		                ->queryOne()["cnt"];
		// восстанавливаем статусы после 10 мин от предыдущей попытки
		$checkDate = ( new \DateTime() )->modify( '-10 min' )->format( 'Y-m-d H:i:s' );
		// только если есть место (1 из 5 потоков должен остаться свободным),сортировка по id - старые должны быть вверху
		if ( $cntRows < 4 && $statuses = $savedStatuses->getAllByFilter( [ "<updated_at" => $checkDate ], [ "id" => "asc" ] ) ) {
			// восстанавливаем первые {4-$cntRows} элемента
			$this->getTDB()->createCommand()
			     ->batchInsert( "Vygruzka_statusov_Rekvizity_shapki_1C_1S", [
				     "Zayavka",
				     "Period",
				     "Status",
				     "Dopolnitelnoe_opisanie",
				     "Nomer_zayavki_sayta",
				     "ID_1C",
				     "_SheduleNumber",
				     "DateAdd",
			     ], array_map( function( $status ) {

				     return [
					     $status["Zayavka"],
					     $status["Period"],
					     $status["Status"],
					     $status["Dopolnitelnoe_opisanie"],
					     $status["Nomer_zayavki_sayta"],
					     $status["ID_1C"],
					     0,
					     $status["DateAdd"],
				     ];
			     }, array_slice( $statuses, 0, ( 4 - $cntRows ) ) ) )->execute();
		}

		// возвращаем false чтобы не отработал objectIsObtainedFrom() текущего потока обмена CheckStatuses
		return false;
	}
}