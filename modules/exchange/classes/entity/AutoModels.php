<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\models\AutoBrands;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\VersionDictionaryTrait;

class AutoModels extends Core {
	/**
	 * Подключение трейта версионности справочников
	 */
	use VersionDictionaryTrait;
	protected $modelName     = 'AutoModels';
	protected $procedureName = 'spravochnik_modeli_avt';
	protected $fields        = [
		"name",
		"guid",
		"auto_brand_id",
		"created_at",
		"active",
	];
	protected $exchange_name = "Reference";

	/**
	 * Метод получения названия версии справочника
	 *
	 * @return string - вернет строку с названием справочника
	 */
	public function getDictionaryName(): string {

		return 'auto-model';
	}

	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "name"          => "p_Naimenovanie",
				                       "guid"          => "p_ID_1C",
				                       "active"        => "p_Status_aktivnosti",
				                       "created_at"    => "p_DateAdd",
				                       "auto_brand_id" => "p_Brend",
			                       ]
		);
	}

	public function before_insertData( &$data = [], &$values = [] ) {

		if ( ! empty( $values ) ) {
			$brand = new AutoBrands();
			if ( $arBrand = $brand->getByGuid( $values["auto_brand_id"] ) ) {
				$values["auto_brand_id"] = $arBrand["id"];
			}
		}

		return true;
	}
}