<?php
namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\AutoBrands;
use app\modules\app\v1\models\AutoModels;
use app\modules\app\v1\models\Employments;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\MethodsIssuance;
use app\modules\app\v1\models\Partners;
use app\modules\app\v1\models\PeopleRelations;
use app\modules\app\v1\models\Points;
use app\modules\app\v1\models\Products;
use app\modules\app\v1\models\Regions;
use app\modules\app\v1\models\RequestMP;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\Users;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\WorkplacePeriods;
use app\modules\exchange\classes\Core;
use app\modules\exchange\models\Requests as AppRequests;
use app\modules\app\v1\models\Statuses as AppStatuses;
use app\modules\app\v1\models\StatusesJournal as AppStatusesJournal;
use app\modules\notification\models\MessageType;
use DateTime;
use Exception;
use Yii;

class Requests extends Core
{
    static    $reLeading_no           = 'Нет';
    static    $reLeading_underCurrent = 'ДокредитованиеПодТекущийЗалог';
    static    $reLeading_parallel     = 'ПараллельныйЗаем';
    static    $defaultIssuance        = 'ЧерезКонтакт';
    static    $codeRequestEventShort  = 701;
    static    $codeRequestEventFull   = 702;
    protected $siteKeySave            = "num";
    protected $siteKeyUpdate          = "num";
    protected $siteKeyUpdate2         = "guid";
    protected $modelName              = 'Requests';
    protected $allEvents              = [];
    protected $sentEvents             = [];
    protected $procedureName          = 'Dokument_GP_Zayavka';
    protected $fields                 = [
        "guid",
        "num",
        "num_1c",
        "code",
        "mode",
        "auto_brand_id",
        "auto_model_id",
        "auto_year",
        "auto_price",
        "summ",
        "date_return",
        "credit_product_id",
        "point_id",
        "user_id",
        "client_first_name",
        "client_last_name",
        "client_patronymic",
        "client_birthday",
        "client_mobile_phone",
        "client_email",
        "client_passport_serial_number",
        "client_passport_number",
        "client_region_id",
        "client_home_phone",
        "client_employment_id",
        "client_workplace_experience",
        "client_workplace_period_id",
        "client_workplace_address",
        "client_workplace_phone",
        "client_workplace_name",
        "client_total_monthly_income",
        "client_total_monthly_outcome",
        "client_guarantor_name",
        "client_guarantor_relation_id",
        "client_guarantor_phone",
        "method_of_issuance_id",
        "card_number",
        "bank_bik",
        "checking_account",
        "pre_crediting",
        "accreditation_num",
        "roystat_id",
        "advert_channel",
        "referal_code",
        "request_source",
        "requests_origin_id",
        "comment_client",
        "comment_partner",
        "ready_to_exchange",
        "print_link",
        "created_at",
        "updated_at",
        "active",
        "sort",
        "lcrm_id",
    ];
    protected $maxRunTimeFlow         = 360;
    protected $exchange_name          = "Order";
    private   $oldFields              = [];

    function getAssociated()
    {
        return array_merge(
            parent::getAssociated(),
            [
                "guid"                          => "p_ID_1C",
                "code"                          => "p_Simvolnaya_ssylka_na_s",
                "num"                           => "p_ID_1S",
                "num_1c"                        => "p_NomerZayavki",
                "summ"                          => "p_Summa",
                "date_return"                   => "p_Data_vozvrata",
                "auto_brand_id"                 => "p_Brend",
                "auto_model_id"                 => "p_Model",
                "auto_year"                     => "p_God_vypuska_avtomobily",
                "auto_price"                    => "p_Stoimost_avto",
                "client_last_name"              => "p_Familiya_klienta",
                "client_first_name"             => "p_Imya_klienta",
                "client_patronymic"             => "p_Otchestvo_klienta",
                "client_mobile_phone"           => "p_Mobilnyy_telefon_podtv",
                "client_birthday"               => "p_Den_rozhdeniya_klienta",
                "client_passport_serial_number" => "p_Pasport_seriya",
                "client_passport_number"        => "p_Pasport_nomer",
                "client_region_id"              => "p_Region_klienta",
                "comment_client"                => "p_Kommentariy",
                "client_home_phone"             => "p_Dtel__klienta",
                "client_email"                  => "p_El_pochta_klienta",
                "client_workplace_name"         => "p_Mesto_raboty",
                "client_workplace_experience"   => "p_Prodolzhitelnost_rabot",
                "client_workplace_period_id"    => "p_Razmernost_stazha",
                "client_workplace_address"      => "p_Rabochiy_adres",
                "client_workplace_phone"        => "p_Rabochiy_tel_",
                "client_total_monthly_income"   => "p_Mesyachnyy_dokhod",
                "client_total_monthly_outcome"  => "p_Mesyachnaya_vyplata_po",
                "client_guarantor_name"         => "p_Poruchitel",
                "client_guarantor_relation_id"  => "p_Stepen_rodstva_KL",
                "client_guarantor_phone"        => "p_Kontaktnyy_tel__poruch",
                "credit_product_id"             => "p_Produkt",
                "user_id"                       => "p_Polzovatel_sozdavshiy_",
                "point_id"                      => "p_Tochka",
                "comment_partner"               => "p_Kommentariy_po_zayavke",
                "referal_code"                  => "p_REF_partnera",
                "roystat_id"                    => "p_RoiID",
                "advert_channel"                => "p_ReklamnyyKanal",
                "client_employment_id"          => "p_Zanyatost",
                "checking_account"              => "p_NomerSchetaPoluchately",
                "bank_bik"                      => "p_BIKBankaPoluchatelya",
                "method_of_issuance_id"         => "p_SposobVydachiZayma",
                "pre_crediting"                 => "p_Dokreditovanie",
                "accreditation_num"             => "p_Dop_identificator", // код аккредитации
                "card_number"                   => "p_NomerKartyPoluchatelya",
                "requests_origin_id"            => "p_MestoSozdaniyaZayavki", // место создания заявки,
                "lcrm_id"                       => "p_LCRM_ID",
                // поля нужные для tdb
                "PARTNER"                       => "p_Partner",
                "VIN"                           => "p_VIN_avto",
                "MARKET_PRICE"                  => "p_Rynochnaya_tsena",
                "NUM_DAYS_LOAN"                 => "p_Kolvo_dney_zaema",
                "FULL_ORDER"                    => "p_Flag_Eto_polnaya_zayav",
                "SNILS"                         => "p_SNILS",
                "WALLET_PHONE_NUMBER"           => "p_NomerKoshelkaViberWall",
                "is_pep"                        => "p_PriznakPEP",
                "place_of_stay"                 => "p_MestoPrebyvaniya",
                "card_token"                    => "p_TokenBankovskoyKarty" // varchar(20)
            ]
        );
    }

    /**
     * @return array|bool|false
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    function getDataForCreate()
    {
        $this->allEvents   = array_column((new Events())->getAll(), "id", "code");
        $statusPreApproved = (new AppStatuses)->getOneByFilter(["guid" => "cbb42353-51e6-4a88-b2ef-575729da0343"]);
        if (
        $rs = (new RequestsEvents())->getResByFilter(
            [
                "event_id"    => [
                    $this->allEvents[static::$codeRequestEventShort],
                    $this->allEvents[static::$codeRequestEventFull],
                ],
                "is_sended"   => 0,
                "!request_id" => array_column($this->pool, "id"),
            ]
        )
        ) {
            while ($event = $rs->read()) {
                // пропускаем если заявка была обработана в рамках пула, такое может быть когда одновременно 2 события по 1 заявке
                if (in_array($event["request_id"], array_column($this->pool, "id"))) {
                    continue;
                }
                // проверяем был ли по заявке статус "Предварительно одобрено"
                $bPreApproved = !!AppStatusesJournal::instance()->getOneByFilter(
                    ["status_id" => $statusPreApproved["id"], "request_id" => $event["request_id"]]
                );
                // Если событие "Отправлена полная заявка"
                if ($event["event_id"] == $this->allEvents[static::$codeRequestEventFull]) {
                    // Отправляем в МФО только если был статус "Предварительно одобрено"
                    if ($bPreApproved) {
                        // Отмечаем что и частичная, и полная отправка отправлена (чтобы исключить двойную отправку)
                        $this->sentEvents[(int) $event["request_id"]] = [
                            $this->allEvents[static::$codeRequestEventShort],
                            $this->allEvents[static::$codeRequestEventFull],
                        ];
                    } else {
                        continue;
                    }
                }
                // Если событие "Отправлена частичная заявка"
                if ($event["event_id"] == $this->allEvents[static::$codeRequestEventShort]) {
                    // Если был статус "Предварительно одобрено", то при наличии события отправки полной заявки - отмечаем и его (чтобы исключить двойную отправку)
                    if ($bPreApproved) {
                        $this->sentEvents[(int) $event["request_id"]] = [
                            $this->allEvents[static::$codeRequestEventShort],
                            $this->allEvents[static::$codeRequestEventFull],
                        ];
                    } else {
                        // Иначе отмечаем только частичную заявку, полная отправится позже когда придется статус "Предварительно одобрено"
                        $this->sentEvents[(int) $event["request_id"]] = [
                            $this->allEvents[static::$codeRequestEventShort],
                        ];
                    }
                }
                if ($request = (new AppRequests())->getOne($event["request_id"])) {
                    $this->pool[] = $request;

                    return $request;
                } else {
                    $error = "Событие с id = ".
                             $event["id"].
                             " ссылается на несуществующую в requests заявку с id = ".
                             $event["request_id"];
                    $this->logYiiError($error);
                }
            }
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return mixed
     * @throws \yii\db\Exception
     */
    function before_createObjectTo($data)
    {
        if (!strlen($data['guid'])) {
            $data['guid'] = $data['num'];
        }
        $data['bank_bik']                     = str_replace('-', '', $data['bank_bik']);
        $data['date_return']                  = date("Y-m-d H:i:s", strtotime($data['date_return']));
        $data['client_workplace_name']        =
            str_replace("'", "\"", htmlspecialchars_decode($data['client_workplace_name']));
        $data['client_birthday']              = date("Y-m-d H:i:s", strtotime($data['client_birthday']));
        $data['summ']                         = (int) trim(str_replace(" ", "", $data['summ']));
        $data['auto_price']                   = (int) trim(str_replace(" ", "", $data['auto_price']));
        $data['client_total_monthly_income']  = (int) trim(str_replace(" ", "", $data['client_total_monthly_income']));
        $data['client_total_monthly_outcome'] = (int) trim(str_replace(" ", "", $data['client_total_monthly_outcome']));
        // date_diff(new DateTime($arResult['PROPERTIES']['RETURN_DATE']['VALUE']), new DateTime())->days; // Клиентский сайт
        // date_diff(new DateTime($arResult['PROPERTIES']['RETURN_DATE']['VALUE']), new DateTime())->days+1; // партнерский
        $data['NUM_DAYS_LOAN'] = date_diff(new DateTime($data['date_return']), new DateTime())->days + 1;
        if (!$data["method_of_issuance_id"]) {
            //			$data["method_of_issuance_id"] = static::$defaultIssuance;
        } elseif ($issuance = (new MethodsIssuance())->getOne($data["method_of_issuance_id"])) {
            $data["method_of_issuance_id"] = $issuance["code_1c"];
        }
        $data['card_number'] = trim(str_replace("_", "", $data['card_number']));
        if ($data['pre_crediting']) {
            switch ($data['pre_crediting']) {
                case 1:
                    $data['pre_crediting'] = static::$reLeading_no;
                    break;
                case 2:
                    $data['pre_crediting'] = static::$reLeading_underCurrent;
                    break;
                case 3:
                    $data['pre_crediting'] = static::$reLeading_parallel;
                    break;
            }
        } else {
            $data['pre_crediting'] = static::$reLeading_no;
        }
        if (
            !empty($data['requests_origin_id'])
            && ($requestOrigin = (new RequestsOrigin())->getOne($data['requests_origin_id']))
        ) {
            $data['requests_origin_id'] = $requestOrigin['name_1c'];
        }
        if ($data['client_region_id']) {
            if ($region = (new Regions())->getOne($data['client_region_id'])) {
                $data['client_region_id'] = $region['name'];
            }
        }
        if ($data['auto_brand_id']) {
            if ($autoBrand = (new AutoBrands())->getOne($data['auto_brand_id'])) {
                $data["auto_brand_id"] = $autoBrand["guid"];
            }
        }
        if ($data['auto_model_id']) {
            if ($autoModel = (new AutoModels())->getOne($data['auto_model_id'])) {
                $data["auto_model_id"] = $autoModel["guid"];
            }
        }
        if ($data['client_workplace_period_id']) {
            if ($period = (new WorkplacePeriods())->getOne($data['client_workplace_period_id'])) {
                $data["client_workplace_period_id"] = $period["name"];
            }
        }
        if ($data['credit_product_id']) {
            if ($product = (new Products())->getOne($data['credit_product_id'])) {
                $data["credit_product_id"] = $product["guid"];
            }
        }
        if ($data['point_id']) {
            if ($point = (new Points())->getOne($data['point_id'])) {
                $data["point_id"] = $point["guid"];
                if ($partner = (new Partners())->getOne($point["partner_id"])) {
                    $data["PARTNER"] = $partner["guid"];
                }
            }
        }
        if ($data['client_employment_id']) {
            if ($employment = (new Employments())->getOne($data['client_employment_id'])) {
                $data['client_employment_id'] = $employment['guid'];
            }
        }
        // кем приходится
        if ($data['client_guarantor_relation_id']) {
            if ($peopleRelation = (new PeopleRelations())->getOne($data['client_guarantor_relation_id'])) {
                $data['client_guarantor_relation_id'] = $peopleRelation['name'];
            }
        }
        $phoneFields = [
            "client_mobile_phone",
            "client_workplace_phone",
            "client_guarantor_phone",
            "client_home_phone",
        ];
        foreach ($phoneFields as $phoneField) {
            if (preg_match("/8?(\d{3})(\d{3})(\d{4})/s", str_replace(" ", "", $data[$phoneField]), $mPhone)) {
                $data[$phoneField] = '+7('.$mPhone[1].')'.$mPhone[2].'-'.$mPhone[3];
            }
        }
        /**
         * Выставляем параметр "полная заявка", если она сйформирована на клиентском сайте, а именно проходит по логике:
         * отсутствует подписант и статус заявки ниже или равен 2
         */
        //		if ( $data['user_id'] ) {
        //			$data['FULL_ORDER'] = 0;
        if ($user = (new Users())->getOne($data['user_id'])) {
            $data['user_id'] = $user["guid"];
        }
        //		} else {
        $data['FULL_ORDER'] = 1;

        //		}
        return $data;
    }

    /**
     * @param $key
     *
     * @throws \yii\db\Exception
     */
    function after_saveObjectTo($key)
    {
        $this->delFromPool($key);
        if ($arRequest = (new AppRequests())->getOneByFilter([$this->siteKeySave => $key])) {
            // Отмечаем только те события об отправленных в МФО заявках, что были установлены в getDataForCreate()
            foreach ($this->sentEvents[(int) $arRequest["id"]] as $idEvent) {
                (new RequestsEvents())->update(
                    "request_id = ".((int) $arRequest["id"])." and event_id=".$idEvent,
                    ["is_sended" => 1]
                );
            }
        }
    }

    /**
     * @param array $data
     * @param array $elementData
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    function before_insertData(&$data = [], &$elementData = [])
    {
        if ($data['p_Dop_identificator']) {
            $elementData['accreditation_num'] = $data['p_Dop_identificator'];
        } else {
            $elementData['accreditation_num'] = "0000000";
        }
        if ($data['p_ID_1S']) {
            $elementData['num'] = $data['p_ID_1S'];
        } else {
            $elementData['num'] = (new AppRequests())->generateNum();
        }
        if (strlen($data['p_NomerZayavki'])) {
            $elementData['num_1c'] = $data['p_NomerZayavki'];
        }
        if ($data['p_Simvolnaya_ssylka_na_s'] == '' && $elementData['guid']) {
            if ($request = (new AppRequests())->getOneByFilter(["guid" => $elementData['guid']])) {
                $data['p_Simvolnaya_ssylka_na_s'] = $request['code'];
                $elementData['code']              = $request['code'];
            }
        }
        if ($data['p_God_vypuska_avtomobily']) {
            $elementData['auto_year'] = $data['p_God_vypuska_avtomobily'];
        }
        if ($data['p_Region_klienta']) {
            if ($region = (new Regions())->getByName($data['p_Region_klienta'])) {
                $elementData['client_region_id'] = $region['id'];
            }
        }
        if ($data['p_Zanyatost']) {
            if ($employment = (new Employments())->getByGuid($data['p_Zanyatost'])) {
                $elementData['client_employment_id'] = $employment['id'];
            }
        }
        if ($data['p_Stepen_rodstva_KL']) {
            $idExtraPerson = null;
            /**
             * todo: [05/12/2017] КОСТЫЛЬ - реализовано для поля "Кем приходится", так как значение поля "p_Stepen_rodstva_KL" передаваемое в обмене от 1С не соответствует точному значению поля из битры "Кем приходится", принято решение(Согласовано с Даниилом Коневым) подбирать id свойства для поля типа список "Кем приходится" относительно сравнения в значении первых трех символов отправленных от 1С в поле "p_Stepen_rodstva_KL" с тремя первыми символами из значения поля "Кем приходится"
             */
            //получаем первый 3 символа в нижнем регистре приходящего значения
            $subStepenRodstva = mb_strtolower(mb_substr($data['p_Stepen_rodstva_KL'], 0, 3));
            $relations        = (new PeopleRelations())->getAll();
            foreach ($relations as $item) {
                $subExtraPerson = mb_strtolower(mb_substr($item['name'], 0, 3));
                if ($subExtraPerson == $subStepenRodstva) {
                    $idExtraPerson = $item['id'];
                    break;
                }
            }
            $elementData['client_guarantor_relation_id'] = $idExtraPerson;
        }
        // мобильный телефон - приводим номер телефона к виду 9998887766 относительно переданного вида +7(999)888-6677
        if ($data['p_Mobilnyy_telefon_podtv']) {
            $elementData['client_mobile_phone'] = str_ireplace(
                [
                    '+7(',
                    ')',
                    '-',
                ],
                '',
                $data['p_Mobilnyy_telefon_podtv']
            );
        }
        // рабочий телефон - приводим номер телефона к виду 9998887766 относительно переданного вида +7(999)888-6677
        if ($data['p_Rabochiy_tel_']) {
            $elementData['client_workplace_phone'] = str_ireplace(
                [
                    '+7(',
                    ')',
                    '-',
                ],
                '',
                $data['p_Rabochiy_tel_']
            );
        }
        // мобильный телефон контактного лица - приводим номер телефона к виду 9998887766 относительно переданного вида +7(999)888-6677
        if ($data['p_Kontaktnyy_tel__poruch']) {
            $elementData['client_guarantor_phone'] = str_ireplace(
                [
                    '+7(',
                    ')',
                    '-',
                ],
                '',
                $data['p_Kontaktnyy_tel__poruch']
            );
        }
        // домашний телефон - приводим номер телефона к виду 9998887766 относительно переданного вида +7(999)888-6677
        if ($data['p_Dtel__klienta']) {
            $elementData['client_home_phone'] = str_ireplace(
                [
                    '+7(',
                    ')',
                    '-',
                ],
                '',
                $data['p_Dtel__klienta']
            );
        }
        $issuance = (new MethodsIssuance())->getAll();
        if ($data['p_SposobVydachiZayma'] != '') {
            foreach ($issuance as $item) {
                if ($data['p_SposobVydachiZayma'] == $item["code_1c"]) {
                    $elementData['method_of_issuance_id'] = $item["id"];
                    break;
                }
            }
        }
        if ($data['p_NomerKartyPoluchatelya'] != '') {
            $elementData['card_number'] = $data['p_NomerKartyPoluchatelya'];
        }
        if (
            $data['p_Dokreditovanie'] != ''
            && in_array(
                $data['p_Dokreditovanie'],
                [
                    static::$reLeading_no,
                    static::$reLeading_underCurrent,
                    static::$reLeading_parallel,
                ]
            )
        ) {
            switch ($data['p_Dokreditovanie']) {
                case static::$reLeading_no:
                    $elementData['pre_crediting'] = 1;
                    break;
                case static::$reLeading_underCurrent:
                    $elementData['pre_crediting'] = 2;
                    break;
                case static::$reLeading_parallel:
                    $elementData['pre_crediting'] = 3;
                    break;
                default:
                    break;
            }
        }
        //Место создания заявки
        if (
            !empty($data['p_MestoSozdaniyaZayavki'])
            && ($requestOrigin = (new RequestsOrigin())->getByName1C($data['p_MestoSozdaniyaZayavki']))
        ) {
            $elementData["requests_origin_id"] = $requestOrigin['id'];
        }
        if ($data['p_Brend'] != '') {
            if ($autoBrand = (new AutoBrands())->getByGuid($data['p_Brend'])) {
                $elementData["auto_brand_id"] = $autoBrand["id"];
            }
        }
        if ($data['p_Model'] != '') {
            if ($autoModel = (new AutoModels())->getByGuid($data['p_Model'])) {
                $elementData["auto_model_id"] = $autoModel["id"];
            }
        }
        if ($data['p_Razmernost_stazha'] != '') {
            if ($period = (new WorkplacePeriods())->getOneByFilter(["name" => $data['p_Razmernost_stazha']])) {
                $elementData["client_workplace_period_id"] = $period["id"];
            }
        }
        if ($data['p_Produkt'] != '') {
            if ($product = (new Products())->getByGuid($data['p_Produkt'])) {
                $elementData["credit_product_id"] = $product["id"];
            }
        }
        if ($data['p_Tochka'] != '') {
            if ($point = (new Points())->getByGuid($data['p_Tochka'])) {
                $elementData["point_id"] = $point["id"];
            }
        }
        if (strlen(trim($data['p_Polzovatel_sozdavshiy_']))) {
            if ($user = (new Users())->getByGuid($data['p_Polzovatel_sozdavshiy_'])) {
                $elementData["user_id"] = $user["id"];
            } else {
                $elementData["user_id"] = null;
            }
        }
        /* ~Обработка значений */
        /**
         * только, если заявка существует и не имеет подписанта (не открывалась партнером) делаем проверку на возможность перезаписи данных
         */
        // если из МФО пришло p_Edited = 1, значит данные были обновлены верификатором и их не нужно забирать из инфоблока а нужно забрать из МФО
        if ($data['p_ID_1S'] && $data['p_Edited'] !== "1") {
            $request = (new AppRequests())->getOneByFilter(["num" => $data['p_ID_1S']]);
            if ((int) $request["user_id"]) {
                // todo: статусы, для которых перетирать данные из 1с, значениями из ИБ (необходимо вывести возможность настройки в админке)
                $filteredStatuses = [
                    // на предварительном одобрении
                    "7d06fa22-3be6-4066-a5c7-6a0c6e7bd055",
                    // предварительно одобрено
                    "cbb42353-51e6-4a88-b2ef-575729da0343",
                    // заполнена не до конца
                    "769bd86a-cb49-490e-bbad-60db89c21c2a",
                    // отсутствует статус
                    "",
                ];
                // todo: список параметров, которые необходимо заполнять из ИБ, перед заполнением (необходимо вывести возможность настройки в админке)
                // todo: продумать логику и проверить возможность простого пропуска (удалять из массива $data)
                $filteredProperty = [
                    "client_last_name",
                    "client_first_name",
                    "client_patronymic",
                    "client_mobile_phone",
                    "client_birthday",
                    "client_passport_serial_number",
                    "client_passport_number",
                    "client_region_id",
                    "comment_client",
                    "client_home_phone",
                    "client_email",
                    "client_workplace_name",
                    "client_workplace_experience",
                    "client_workplace_period_id",
                    "client_workplace_address",
                    "client_workplace_phone",
                    "client_total_monthly_income",
                    "client_total_monthly_outcome",
                    "client_guarantor_name",
                    "client_guarantor_relation_id",
                    "client_guarantor_phone",
                    "comment_partner",
                    "client_employment_id",
                    "bank_bik",
                    "method_of_issuance_id",
                    "auto_brand_id",
                    "auto_model_id",
                    //"accreditation_num", // код аккредитации
                    "requests_origin_id", // место создания заявки
                    "checking_account",
                ];
                $arCurStatus      = $this->getDB()
                    ->createCommand(
                        "
												SELECT s.guid 
												FROM statuses_journal j
												 LEFT JOIN requests r
												  ON j.request_id = r.id
												  LEFT JOIN statuses s ON s.id = j.status_id
												WHERE r.num=:num 
												ORDER BY j.created_at DESC
												",
                        [":num" => $data['p_ID_1S']]
                    )
                    ->queryOne();
                if (in_array($arCurStatus["guid"], $filteredStatuses)) {
                    foreach ($filteredProperty as $propName) {
                        $elementData[$propName] = $request[$propName];
                    }
                }
                // если не на финальном одобрении и заявка не из ЛКК - оставляем какой был на сайте
                if (
                    $arCurStatus["guid"] !== "623bbcc6-ac4c-4e51-bdcf-f7c5a12b9abd" ||
                    (!$requestOrigin = (new RequestsOrigin())->getOne($request['requests_origin_id'])) ||
                    $requestOrigin["identifier"] !== RequestsOrigin::IDENTIFIER_CLIENT
                ) {
                    $elementData["user_id"] = $request["user_id"];
                }
            }
        }

        return true;
    }

    function before_create(&$values = [])
    {
        $values['request_source'] = '1C';
        $values['code']           = md5(microtime().$values['guid']);
    }

    /**
     * @param array $values
     * @param       $idUpdate
     *
     * @throws \yii\db\Exception
     * @throws Exception
     */
    function before_update(&$values = [], $idUpdate = null)
    {
        // num задается исключительно при создании
        if (isset($values["num"])) {
            unset($values["num"]);
        }
        if (isset($values["card_number"])) {
            unset($values["card_number"]);
        }
        $origin = (new RequestsOrigin)->getOne($values["requests_origin_id"]);
        if (!$origin) {
            throw new Exception("Не удается определить происхождение заявки с guid=".$values["guid"]);
        };
        //} elseif ( $origin["identifier"] !== "PARTNER" ) {
        //			return;
        //		}
        if (isset($idUpdate) && $request = (new AppRequests())->getOne($idUpdate)) {
            $this->oldFields = $request;
        } else {
            throw new Exception("Заявка с id=".$idUpdate." не найдена");
        }
    }

    /**
     * @param array $values
     * @param null  $idUpdate
     *
     * @throws Exception
     */
    function after_update(&$values = [], $idUpdate = null)
    {
        if (!empty($this->oldFields) && isset($idUpdate) && $request = (new AppRequests())->getOne($idUpdate)) {
            if ((int) $request['summ'] !== (int) $this->oldFields['summ']) {
                // код который вынесен в обмен, чтобы при смене суммы очищались файлы и эвенты агента, другие кабинеты это задеть не должно
                $requestDocs = (new File())->getDocByRequestCode($request['code'], 2);
                Log::info(
                    ["text" => $request['id'].' : изменили сумму клиента, И ЗДЕСЬ ОТПРАВЛЯЛСЯ ЛИШНИЙ 202 перезапрос'],
                    "exchange.event.202"
                );
                if (!empty($requestDocs)) {
                    Log::info(
                        [
                            "text" => $request['id'].
                                      ' : изменили сумму клиента, нашли документы и сделали 202 перезапрос',
                        ],
                        "exchange.event.202"
                    );
                    $event               = new RequestsEvents();
                    $event['event_id']   = 4;
                    $event['request_id'] = $request['id'];
                    $event['is_sended']  = 0;
                    if (!$result202 = $event->create()) {
                        throw new Exception($result202);
                    };
                }
                foreach ($requestDocs as $doc) {
                    $result = Yii::$app->file->delete($doc['code']);
                    if (is_string($result)) {
                        throw new \Exception($result);
                    }
                }
                (new RequestsEvents())->delete('request_id = '.$request['id'].' and event_id IN (9, 10, 11)');
                (new AppRequests())->updateOne($request['id'], ['print_link' => '']); // очистка ссылки print_link
                $mess = [
                    "type_id"  => MessageType::instance()->requp,
                    "order_id" => $request['id'],
                    "message"  => json_encode(["summ" => $request['summ']], JSON_FORCE_OBJECT),
                ];
                if (!Yii::$app->notify->addMessage($mess, true)) {
                    throw new Exception(
                        "Не удается отправить сообщение в сокет о наличии изменений полей в заявке с num={$request["num"]}"
                    );
                }
                // код который вынесен в обмен end
                if (
                !Yii::$app->notify->addMessage(
                    [
                        "type_id"  => MessageType::instance()->sys,
                        "order_id" => $request["id"],
                        "message"  => "Сумма займа изменена на ".$request['summ']." руб.",
                    ],
                    true
                )
                ) {
                    throw new Exception(
                        "Не удается отправить SYS сообщение в сокет о сформированных документах (пак 2) по заявке с num={$request["num"]}"
                    );
                }
            }
            if (
                $request['client_id'] && !in_array(
                    $request['point_id'],
                    [
                        Yii::$app->params['client']['settings']['point_id'],
                        Yii::$app->params['mp']['settings']['point_id'],
                    ]
                )
            ) {
                $mess = [
                    "type_id"  => MessageType::instance()->lkkblock,
                    "order_id" => $request['id'],
                    "message"  => json_encode(["num" => $request['num']], JSON_FORCE_OBJECT),
                ];
                if (!Yii::$app->notify->addMessage($mess, true)) {
                    throw new Exception(
                        "Не удается отправить сообщение в сокет о блокировке заявки в ЛКК с num={$request["num"]}"
                    );
                }
                //указываем блокировку заявки партнером для МП
                $model = new RequestMP();
                $model->load(
                    [
                        'request_id' => $request['id'],
                        'blocked_by' => RequestMP::BLOCKED_BY_PARTNER,
                    ],
                    ''
                );
                $model->save();
            }
            if (
                (int) $this->oldFields['point_id']
                && (int) $this->oldFields['point_id'] == (int) Yii::$app->params['client']['settings']['point_id']
                && (int) $request['point_id'] !== (int) Yii::$app->params['client']['settings']['point_id']
                && ($lead = Leads::instance()->getOneByFilter(['guid' => $request['guid']]))
            ) {
                Leads::instance()->deleteOne($lead["id"]);
            }
            /**
             * отправка сообщения об обновлении номера аккредитации
             */
            if (
                (empty($this->oldFields['accreditation_num']) || $this->oldFields['accreditation_num'] == '0000000') &&
                $request['accreditation_num']
            ) {
                $mess = [
                    "type_id"  => MessageType::instance()->requp,
                    "order_id" => $request['id'],
                    "message"  => json_encode(
                        ["accreditation_num" => $request['accreditation_num']],
                        JSON_FORCE_OBJECT
                    ),
                ];
                if (!Yii::$app->notify->addMessage($mess, true)) {
                    throw new Exception(
                        "Не удается отправить сообщение в сокет с номером аккредитации accreditation_num={$request['accreditation_num']}, заявка num={$request["accreditation_num"]}"
                    );
                }
            }
        }
    }

    /**
     * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
     *
     * @return string
     */
    public function getOrderKeyGUID()
    {
        return "p_ID_1C";
    }

    /**
     * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
     *
     * @return string
     */
    public function getOrderKeyID()
    {
        return "p_ID_1S";
    }
}