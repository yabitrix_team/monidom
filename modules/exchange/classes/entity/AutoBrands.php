<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\VersionDictionaryTrait;

class AutoBrands extends Core {
	/**
	 * Подключение трейта версионности справочников
	 */
	use VersionDictionaryTrait;
	protected $modelName     = 'AutoBrands';
	protected $procedureName = 'spravochnik_brend_avto';
	protected $fields        = [
		"name",
		"guid",
		"created_at",
		"active",
	];
	protected $exchange_name = "Reference";

	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "name"       => "p_Naimenovanie",
				                       "guid"       => "p_ID_1C",
				                       "active"     => "p_Status_aktivnosti",
				                       "created_at" => "p_DateAdd",
			                       ]
		);
	}

	/**
	 * Метод получения названия версии справочника
	 *
	 * @return string - вернет строку с названием справочника
	 */
	public function getDictionaryName(): string {

		return 'auto-brand';
	}
}