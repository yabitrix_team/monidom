<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;

class ContractsClosed extends Core {
	protected $modelName      = 'Contracts';
	protected $procedureName  = 'Dannye_po_zakrytym_dog';
	protected $fields         = [
		"code",
		"active",
	];
	protected $siteKeyUpdate  = "code";
	protected $maxRunTimeFlow = false;
	protected $exchange_name  = "Order";

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "code"       => "p_Nomer",
				                       "guid"       => "p_ID_1C",
			                       ]
		);
	}

	function before_update( &$values = [] ) {

		$values["active"] = 0;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
	 *
	 * @return string
	 */
	public function getOrderKeyID() {

		return "p_ID_1S";
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_Zayavka";
	}
}