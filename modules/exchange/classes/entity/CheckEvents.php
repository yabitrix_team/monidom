<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\GetProcedure;

class CheckEvents extends Core {
	protected $procedureName = 'Obmen_sobytiyami';

	/**
	 * @return array|bool|null
	 * @throws \yii\base\Exception
	 */
	function getObjectFrom() {

		$this->current_procedure = new GetProcedure( $this->procedureName, $this->current_procedure_type, true );
		if ( $data = $this->getTDB()->createCommand( "SELECT * FROM Obmen_sobytiyami_Rekvizity_shapki_1S_1C" )
		                  ->queryOne() ) {
			$data["p_DateAdd"] = $data["DateAdd"];
			$this->checkHangFlow( $data, __LINE__, __FILE__ );
		}

		return false;
	}
}