<?php

namespace app\modules\exchange\classes\entity;

use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\VersionDictionaryTrait;

class Products extends Core {
	/**
	 * Подключение трейта версионности справочников
	 */
	use VersionDictionaryTrait;
	protected $modelName     = 'Products';
	protected $procedureName = 'spravochnik_kreditnyy_';
	protected $fields        = [
		"guid",
		"created_at",
		"name",
		"summ_min",
		"summ_max",
		"percent_per_day",
		"loan_period_min",
		"loan_period_max",
		"age_min",
		"age_max",
		"active",
		"month_num",
		"is_additional_loan",
	];
	protected $exchange_name = "Reference";

	/**
	 * Метод получения названия версии справочника
	 *
	 * @return string - вернет строку с названием справочника
	 */
	public function getDictionaryName(): string {

		return 'product';
	}

	function getAssociated() {

		return array_merge( parent::getAssociated(), [
			"name"               => "p_Naimenovanie",
			"guid"               => "p_ID_1C",
			"active"             => "p_Status_aktivnosti",
			"created_at"         => "p_DateAdd",
			"summ_min"           => "p_Minimalnyy_zaem",
			"summ_max"           => "p_Maksimalnyy_zaem",
			"percent_per_day"    => "p_Protsent_v_den",
			"loan_period_min"    => "p_Minimalnyy_srok_kredit",
			"loan_period_max"    => "p_Maksimalnyy_srok_kredi",
			"age_min"            => "p_Minimalnyy_vozrast",
			"age_max"            => "p_Maksimalnyy_vozrast",
			"month_num"          => "p_Maksimalnyy_srok_kredi",
			"is_additional_loan" => "p_Dokreditovanie",
		] );
	}
}
