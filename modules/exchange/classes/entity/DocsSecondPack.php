<?php
namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\soap\cmr\AgreementsData;
use app\modules\app\v1\models\Users;
use app\modules\exchange\classes\Core;
use app\modules\exchange\classes\traits\DocsTrait;
use app\modules\exchange\models\Requests;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageType;
use Exception;
use Yii;

class DocsSecondPack extends Core
{
    use DocsTrait;
    protected $procedureName = 'Individualnye_usloviya';
    protected $exchange_name = "File";

    /**
     * @param array $data
     *
     * @return array|bool|null
     * @throws \yii\db\Exception
     * @throws Exception
     */
    public function after_getObjectFrom($data = [])
    {
        $guid = $data[$this->tdbKey];
        if ($request = (new Requests())->getByGuid($guid)) {
            if ($user = (new Users())->getOne($request['user_id'])) {
                $request['user_guid'] = $user["guid"];
            }
            $cnt = $this->getObjectsRowCountFrom($guid);
            if (0 < $cnt) {
                $requestPropertyValue = [];
                $server               = rtrim(Yii::getAlias('@app'), '/');
                $setSignatory         = true;
                for ($i = 1, $getFile = true; $i <= $cnt && $getFile; $i++) {
                    $params = [
                        "guid" => $guid,
                        "line" => $i,
                    ];
                    if ($getFile = $this->getObjectRow($params)) {
                        if (
                            $request['user_guid']
                            && $getFile['p_Podpisant'] != ''
                            && $request['user_guid'] != $getFile['p_Podpisant']
                        ) {
                            (new Events)->sendTo1C(202, $guid, $request['user_id']);
                            Log::info(
                                [
                                    "text" => $request['id'].
                                              ' / '.$request['user_guid'].
                                              ' : 202 по заявке отправлен напрямую в ТБД',
                                ],
                                "exchange.event.202"
                            );

                            return $data;
                        } else {
                            // устанавливаем подписанта из док-та, если заявка была создана в 1с, и в ней не было подписанта
                            if ($request['user_guid'] == '' && $setSignatory) {
                                $setSignatory = $getFile['p_Podpisant'];
                            }
                            $decodedData = base64_decode($getFile["p_Fayl"]);
                            switch ($getFile['p_Kod_tipa_dokumenta']) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                    $fileDir = "/web/path/request/".$request["code"]."/doc_pack_2/";
                                    if (!is_dir($server.$fileDir)) {
                                        mkdir($server.$fileDir, 0775, true);
                                        clearstatcache(true, $server.$fileDir);
                                    }
                                    $fileName = $getFile['p_Naimenovanie'].".".$getFile['p_TipFayla'];
                                    $filePath = $server.$fileDir.$fileName;
                                    if (file_exists($filePath)) {
                                        unlink($filePath);
                                    }
                                    if (file_put_contents($filePath, $decodedData, LOCK_EX) !== false) {
                                        clearstatcache(true, $filePath);
                                        $transaction = $this->getDB()->beginTransaction();
                                        $filePathRel = "path/request/".$request["code"]."/doc_pack_2/".$fileName;
                                        if ($file = (new File())->getOneByFilter(["path" => $filePathRel])) {
                                            $idFile = $file["id"];
                                        } elseif (
                                        !$idFile = (new File())->add(
                                            [
                                                'name'        => $fileName,
                                                'path'        => $filePathRel,
                                                'width'       => 0,
                                                'height'      => 0,
                                                'type'        => mime_content_type($filePath),
                                                'size'        => filesize($filePath),
                                                'description' => $getFile["p_Kommentariy"]
                                                    ?: $getFile['p_Naimenovanie'],
                                                'code'        => md5(str_shuffle('file_code'.time()).microtime(true)),
                                                'sim_link'    => md5(
                                                    str_shuffle('file_sim_link'.time()).microtime(true)
                                                ),
                                            ]
                                        )
                                        ) {
                                            $transaction->rollBack();
                                            $this->logYiiError(
                                                "Второй пакет доков - Ошибка сохранения файла ".$filePath
                                            );
                                            $getFile = false;
                                            break;
                                        }
                                        $filterRequestFile = [
                                            'request_id' => $request['id'],
                                            'file_id'    => $idFile,
                                            'file_bind'  => RequestFile::BIND_DOC_PACK_2,
                                        ];
                                        if (
                                            !(new RequestFile())->getOneByFilter($filterRequestFile) &&
                                            !(new RequestFile())->add($filterRequestFile)
                                        ) {
                                            $transaction->rollBack();
                                            $this->logYiiError(
                                                "Второй пакет доков - Ошибка привязки файла к заявке ".$filePath
                                            );
                                            $getFile = false;
                                            break;
                                        }
                                        $transaction->commit();
                                        array_push(
                                            $requestPropertyValue,
                                            $fileDir.$fileName
                                        );
                                        Log::info(
                                            [
                                                "text" => $request['id'].
                                                          ' / '.$request['user_guid'].
                                                          ' : сохранен файл 2 пакета документов, имя файла: '.$fileName,
                                            ],
                                            "exchange.event.202"
                                        );
                                        $arrData['docs'][] = [
                                            "src"  => '/doc/'.self::dataEncode(
                                                    [
                                                        'STEP'     => 1,
                                                        'DOC_NAME' => $fileName,
                                                        'XML_ID'   => $guid,
                                                    ]
                                                ),
                                            "name" => $fileName,
                                            "desc" => $getFile["p_Kommentariy"] ?: $getFile['p_Naimenovanie'],
                                        ];
                                    } else {
                                        $this->logTbdError("ОШИБКА ! Ошибка записи файла  для заявки $guid( пакет 2)");
                                        $getFile = false;
                                    }
                                    break;
                                default:
                                    $this->logTbdError("ОШИБКА !неизвестный тип док-та по заявке $guid(пакет 2)");
                                    $getFile = false;
                                    break;
                            }
                        }
                    } else {
                        $this->logTbdError("ОШИБКА! Нет документов по заявке $guid(пакет 2)");
                        $getFile = false;
                        break;
                    }
                }
                /** Если возникла ошибка в получении док-в */
                if (!$getFile) {
                    /** удаляем список */
                    if (!empty($requestPropertyValue)) {
                        foreach ($requestPropertyValue as $filePathRel) {
                            if (file_exists($filePathRel)) {
                                unlink($filePathRel);
                            }
                        }
                    }
                    throw new Exception("Не получены документы по заявке guid=".$guid);
                } else {
                    // устанавливаем подписанта из док-та, если заявка была создана в 1с, и в ней не было подписанта
                    if (
                        $request['user_guid'] == '' &&
                        $setSignatory &&
                        $setUser = (new Users())->getByGuid($setSignatory)
                    ) {
                        (new Requests())->update($request["id"], ["user_id" => $setUser["id"]]);
                    }
                    $arrData['docArchive'] = self::dataEncode(
                        [
                            'STEP'   => 1,
                            'TYPE'   => 'zip',
                            'XML_ID' => $guid,
                        ]
                    );
                    /**
                     * Формируем сообщение для сокета
                     */
                    $mess = [
                        "type_id"  => MessageType::instance()->secondpack,
                        "order_id" => $request['id'],
                    ];
                    if (!Yii::$app->notify->addMessage($mess, true)) {
                        throw new Exception(
                            "Не удается отправить сообщение в сокет о сформированных документах (пак 2) по заявке с num={$request["num"]}"
                        );
                    }
                    Log::info(
                        ["text" => $request['id'].' / '.$request['user_guid'].' : отправлен hasSecondPack по заявке'],
                        "exchange.event.202"
                    );
                    if (
                    !(new Message)->getOneByFilter(
                        [
                            "type_id"  => MessageType::instance()->sys,
                            "order_id" => $request["id"],
                            "message"  => "Получены индивидуальные условия на подпись",
                        ]
                    )
                    ) {
                        if (
                        !Yii::$app->notify->addMessage(
                            [
                                "type_id"  => MessageType::instance()->sys,
                                "order_id" => $request["id"],
                                "message"  => "Получены индивидуальные условия на подпись",
                            ],
                            true
                        )
                        ) {
                            throw new Exception(
                                "Не удается отправить SYS сообщение в сокет о сформированных документах (пак 2) по заявке с num={$request["num"]}"
                            );
                        }
                        if (strlen($request['num_1c'])) {
                            $agreementsData =
                                new AgreementsData(['scenario' => AgreementsData::SCENARIO_ACTIVE_AGREEMENTS_INFO]);
                            $agreementsData->load(['code' => $request['num_1c']], '');
                            $agreementsInfo = $agreementsData->activeAgreementsInfov4();
                            if (is_a($agreementsInfo, AgreementsData::class)) {
                                throw new Exception("Ошибка получения графика платежей на этапе 2 пакета документов");
                            }
                            if (is_a($agreementsInfo, \Exception::class)) {
                                throw $agreementsInfo;
                            }
                            $agreementsSumm    = number_format($agreementsInfo->Summ, 2, ".", " ");
                            $agreementsPerc    = $agreementsInfo->PSK;
                            $agreementsPayment = number_format($agreementsInfo->Annuity, 2, ".", " ");
                            if (
                            !Yii::$app->notify->addMessage(
                                [
                                    "type_id"  => MessageType::instance()->sys,
                                    "order_id" => $request["id"],
                                    "message"  => "Вам утверждена сумма {$agreementsSumm} руб.",
                                ],
                                true
                            )
                            ) {
                                throw new Exception(
                                    "Не удается отправить SYS сообщение в сокет о сумме, ставке, платеже по заявке с num={$request["num"]}"
                                );
                            }
                        }
                    }

                    return $data;
                }
            }

            return false;
        }
        throw new Exception("Заявка с guid=".$guid." не найдена");
    }
}