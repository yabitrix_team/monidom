<?php

namespace app\modules\exchange\classes\entity;

use app\modules\app\v1\models\Partners;
use app\modules\exchange\classes\Core;

class Points extends Core {
	protected $modelName     = 'Points';
	protected $procedureName = 'spravochnik_tochek';
	protected $fields        = [
		"guid",
		"name",
		"partner_id",
		"country",
		"index",
		"region",
		"area",
		"city",
		"locality",
		"street",
		"house",
		"housing",
		"office",
		"brand_name",
		"time_table",
		"coords",
		"created_at",
		"active",
	];
	protected $exchange_name = "Reference";

	function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "name"       => "p_Naimenovanie",
				                       "guid"       => "p_ID_1C",
				                       "created_at" => "p_DateAdd",
				                       "partner_id" => "p_Partner",
				                       "country"    => "p_Strana",
				                       "index"      => "p_Indeks",
				                       "region"     => "p_Region",
				                       "area"       => "p_Rayon",
				                       "city"       => "p_Gorod",
				                       "locality"   => "p_NaselennyyPunkt",
				                       "street"     => "p_Ulitsa",
				                       "house"      => "p_Dom",
				                       "housing"    => "p_Korpus",
				                       "office"     => "p_Kvartira",
				                       "coords"     => "p_Koordinaty",
				                       "active"     => "p_NeostupenDlyaKTs",
			                       ]
		);
	}

	public function before_insertData( &$data = [], &$values = [] ) {

		if ( ! empty( $values ) ) {
			$partner = new Partners();
			if ( $arPartner = $partner->getByGuid( $values["partner_id"] ) ) {
				$values["partner_id"] = $arPartner["id"];
			}
			$values["active"] = ! $values["active"];
		}

		return true;
	}
}