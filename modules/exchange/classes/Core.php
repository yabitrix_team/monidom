<?php

namespace app\modules\exchange\classes;

use app\modules\app\v1\classes\Tools;
use app\modules\app\v1\classes\logs\Log;
use app\modules\exchange\classes\log\Client as LogClient;
use app\modules\exchange\classes\log\ExchangeType as LogExchangeType;
use Exception;
use Yii;

class Core {
	use LogClient;
	const RULE = "/^[^\\d]*[a-z]{0}([\\d]+):([\\sА-Яа-яa-z]+)$/miu";
	/** @var mixed набор параметров подключения и организации обена (файл /config/transaction_connect.php) */
	protected $connector_params = false;
	protected $modelName        = '';
	protected $instance         = null;
	protected $siteKeyUpdate    = "guid";
	protected $siteKeyObtain    = "guid";
	protected $siteKeySave      = "guid";
	protected $tdbKey           = 'p_ID_1C';
	protected $tdbError         = 'p_Errors';
	protected $fields           = [];
	protected $procedureName    = null;
	/** @var null|GetProcedure */
	protected $current_procedure      = null;
	protected $defaultValues          = [
		"sort" => 1000,
	];
	protected $current_procedure_type = 0;
	protected $flow                   = 1;
	/** @var bool|int максимальное время работы потока (сравнивается с датой создания данных, в момент вызова потока) в секундах */
	protected $maxRunTimeFlow          = 180;
	protected $loop_flow_message_timer = 1; //300;
	protected $get_row_prefix          = 'Dokumenty';
	protected $get_row_count_prefix    = 'Dokumenty';
	protected $add_row_prefix          = 'Fayly';
	/** @var array пулл отправляемых данных */
	protected $pool         = [];
	protected $exchange_name;
	private   $poolLabelKey = "num";

	/**
	 * Core constructor.
	 */
	public function __construct() {

		if ( strlen( $this->modelName ) ) {
			$modelClass     = '\app\modules\app\v' . yii::$app->params['exchange']['model_version'] . '\models\\' . $this->modelName;
			$this->instance = new $modelClass();
		}
		$this->connector_params     = [
			'site_key' => yii::$app->params['exchange']['tdb_params']['site_key'],
		];
		$this->defaultValues["tdb"] = yii::$app->params['exchange']['tdb_params']['site_key'] . "/" . yii::$app->params['exchange']['tdb_params']['tdb_suffix'];
	}

	/**
	 * @param null  $idProcedure
	 * @param bool  $flow
	 * @param bool  $obtain
	 * @param array $values
	 *
	 * @return bool
	 * @throws \yii\base\Exception
	 * @throws Exception
	 */
	final function run( $idProcedure = null, $flow = false, $obtain = true, $values = [] ) {

		$this->current_procedure_type = $idProcedure;
		$this->flow                   = $flow ?: $this->flow;
		$rsGP                         = new GetProcedure( $this->procedureName );
		switch ( $idProcedure ) {
			case $rsGP->GET_OBJECT_FROM:
				$this->logSetExchType( LogExchangeType::In );
				$data = $this->getObjectFrom();
				if ( $data && $obtain ) {
					$data = $this->objectIsObtainedFrom( $data[ $this->tdbKey ] );
				}

				return (bool) $data;
			case $rsGP->CREATE_OBJECT_TO:
				$this->logSetExchType( LogExchangeType::Out );
				$values = $this->createObjectTo( $values );
				if ( $values && $obtain ) {
					$values = $this->saveObjectTo( $values[ $this->siteKeySave ] );
				}

				return (bool) $values;
			default:
				$this->logSetExchType( LogExchangeType::System );
				throw new Exception( "Не задан тип обмена" );
		}
	}

	/**
	 * @return array|bool|null
	 * @throws \yii\base\Exception
	 * @throws Exception
	 */
	function getObjectFrom() {

		$continue = true;
		if ( method_exists( $this, 'before_getObjectFrom' ) ) {
			$continue = $this->before_getObjectFrom();
		}
		if ( $continue ) {
			$TBD                     = $this->getTDB();
			$this->current_procedure = new GetProcedure( $this->procedureName, $this->current_procedure_type );
			$preparedParams          = $this->prepareForProcedure( [
				                                                       "flow" => $this->flow,
			                                                       ] );
			$query                   = $this->current_procedure->getSql( $preparedParams );
			$TBD->createCommand( $query->sql )->execute();
			if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
				if ( isset( $data[ "@" . $this->tdbError ] ) && ! empty( $this->parse_error( "Error: " . $data[ "@" . $this->tdbError ] ) ) ) {
					$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
					throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
				}
			} else {
				throw new Exception( "Запрос не отработал\n " . $query->sql );
			}
			/**
			 * забираем данные из временной таблицы
			 */
			if ( $data = $TBD->createCommand( "SELECT * FROM sExchData" )->queryOne() ) {
				$data = $this->convertFieldName( $data );
				$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
				$this->logData( [ "data" => $data, "query" => "SELECT * FROM sExchData;" ] );
				$this->checkHangFlow( $data );
				if ( method_exists( $this, 'after_getObjectFrom' ) ) {
					$data = $this->after_getObjectFrom( $data );
				}

				return $data;
			}
		}

		return false;
	}

	public function getTDB() {

		$connection = Yii::$app->tdb;
		try {
			$connection->createCommand( "DO 1;" )->execute();
		} catch ( \yii\db\Exception $e ) {
			var_dump( $e->getMessage() );
			$connection->close();
			$connection->open();
			echo "> getTDB() reconnected" . PHP_EOL;
		}

		return $connection;
	}

	function prepareForProcedure( $params = [] ) {

		$params = array_merge( $this->getDefaultValues(), $params );
		if (
			! empty( $params )
			&& isset( $this->current_procedure )
			&& ! empty( $this->current_procedure->getTheseByProcedureName() )
		) {
			$arrAssocInvert = array_flip( $this->getAssociated() );
			$prepareParams  = [];
			foreach ( $this->current_procedure->getCurrentProcedureParams() as $procedureParam ) {
				$in    = ( strtoupper( $procedureParam['PARAMETER_MODE'] ) == "IN" );
				$value = $params[ $arrAssocInvert[ $procedureParam['PARAMETER_NAME'] ] ];
				if ( ( $type = strtolower( gettype( $value ) ) ) ) {
					/**
					 * преобразование типов под тип процедур
					 * todo: предположительно можно вынести данную проверку на обработчики значения: getValue(), и т.д.,
					 * todo: чтоб конвертировать значение и возвращать в необходимом типе
					 */
					switch ( $procedureParam['DATA_TYPE'] ) {
						case "varchar":
							/** приведение GUID */
							if ( $procedureParam['CHARACTER_MAXIMUM_LENGTH'] == 36 ) {
								$value = ( is_null( $value ) || 0 == strlen( trim( (string) $value ) ) ) ? null :
									$value;
							}
							$value = (string) str_replace( "'", "\'", $value );
							break;
						case "int":
							$value = (int) $value;
							break;
						case "text":
							$value = (string) str_replace( "'", "\'", $value );
							break;
						case "float":
							$value = (float) $value;
							break;
						case "datetime":
							/*$value = $value;*/
							break;
						case "tinyint":
							if ( 4 < strlen( (int) $value ) ) {
								// todo: throw new exception
							}
							$value = (int) $value;
							break;
					}
				}
				$prepareParams[ $procedureParam['ORDINAL_POSITION'] ] = [
					"type" => ( $in ? "IN" : "OUT" ),
					"data" => ( $in ? $value : $procedureParam['PARAMETER_NAME'] ),
				];
			}

			return $prepareParams;
		}

		return $params;
	}

	/**
	 * @return array
	 */
	function getDefaultValues() {

		return $this->defaultValues ?: [];
	}

	protected function getAssociated() {

		return [
			"tdb"  => "SenderData",
			"flow" => "p_SheduleNumber",
		];
	}

	/**
	 * Функция разбирает входящую строку на код и сообщение ошибки
	 *
	 * @param null|string $str_error
	 *
	 * @return array|false
	 */
	function parse_error( $str_error = null ) {

		if ( isset( $str_error ) ) {
			preg_match( $this::RULE, trim( $str_error ), $matches );

			return [
				"message" => $matches[2],
				"code"    => $matches[1],
			];
		}

		return false;
	}

	function prepareDataArray( $prepareParams = [] ) {

		$arReturn = [];
		$arKeys   = array_keys( $this->current_procedure->getTheseByProcedureName() );
		foreach ( $prepareParams as $i => $arParam ) {
			$arReturn[ $arKeys[ $i - 1 ] ] = $arParam["data"];
		}

		return $arReturn;
	}

	function convertFieldName( $data = [] ) {

		$items = [];
		foreach ( array_keys( $data ) as $key ) {
			if ( ! is_numeric( $key ) ) {
				$items[ "p_" . $key ] = $data[ $key ];
			}
		}

		return $items;
	}

	/**
	 * Метод для проверки наличия залипших потоков
	 *
	 * @param array $data - Массив полученных данных из ТБД
	 *
	 * @return bool - Вернет истину в случае залипшего потока иначе ложь
	 * @throws \yii\base\Exception
	 */
	function checkHangFlow( $data = [], $line = __LINE__, $file = __FILE__ ) {

		if ( $this->maxRunTimeFlow && isset( $data['p_DateAdd'] ) && $data['p_DateAdd'] != '' ) {
			$dd             = date_diff( date_create( 'now' ), date_create( $data['p_DateAdd'] ) );
			$run_time       = ( ( $dd->d * 24 + $dd->h ) * 60 + $dd->i ) * 60 + $dd->s;
			$RUNNER         = new RunLocker();
			$procedure_name = $this->current_procedure->getProcedureName();
			if (
				$this->maxRunTimeFlow < $run_time
				&& $RUNNER->buildEventTimer( $procedure_name . "_" . $this->flow, $this->loop_flow_message_timer, $this->connector_params['site_key'] )
			) {
				$arData = print_r( $data, true );
				$msg    = <<<TXT
Внимание! Возникло зависание потока на срок выше указанного в настройках класса:\n
Вызываемая процедура: $procedure_name
Номер потока: $this->flow
Допустимый срок: $this->maxRunTimeFlow (сек)
Срок работы потока: $run_time (сек)\n
Строка вызова: $line 
Файл вызова: $file\n
Дополнительные данные:
$arData
TXT;
				carLog( $msg );
				$this->logTbdError( $msg );

				return true;
			}
		}

		return false;
	}

	/**
	 * @param array $data - массив полученных данных из ТБД
	 *
	 * @return null
	 * @throws \yii\db\Exception
	 */
	function after_getObjectFrom( $data = [] ) {

		$values   = $this->mapValues( $data );
		$continue = true;
		if ( method_exists( $this, 'before_insertData' ) ) {
			$continue = $this->before_insertData( $data, $values );
		}
		if ( $continue && $values = $this->insertData( $values ) ) {
			if ( method_exists( $this, 'after_insertData' ) ) {
				$this->after_insertData( $data, $values );
			}

			return $data;
		}

		return false;
	}

	public function mapValues( $data = [] ) {

		$values = [];
		foreach ( $this->getAssociated() as $set => $get ) {
			if ( ! isset( $data[ $get ] ) ) {
				continue;
			}
			$values[ $set ] = $data[ $get ];
		}

		return array_merge( $this->getDefaultValues(), $values );
	}

	/**
	 * @param array $values
	 *
	 * @return array|bool
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	function insertData( $values = [] ) {

		if ( ! empty( $values ) ) {
			$idUpdate = false;
			if (
				( $values[ $this->siteKeyUpdate ]
				  && $arItem = $this->instance->getOneByFilter( [ $this->siteKeyUpdate => $values[ $this->siteKeyUpdate ] ] ) )
				|| ( isset( $this->siteKeyUpdate2 ) && $values[ $this->siteKeyUpdate2 ]
				     && $arItem = $this->instance->getOneByFilter( [ $this->siteKeyUpdate2 => $values[ $this->siteKeyUpdate2 ] ] ) )
			) {
				$idUpdate = $arItem["id"];
			}
			$values = $this->clearFields( $values );
			if ( $idUpdate ) {
				if ( method_exists( $this, 'before_update' ) ) {
					$this->before_update( $values, $idUpdate );
				}
				if ( $this->instance->update( $idUpdate, $values ) ) {
					if ( method_exists( $this, 'after_update' ) ) {
						$this->after_update( $values, $idUpdate );
					}

					return array_merge( [ "id" => $idUpdate ], $values );
				} else {
					throw new Exception( "Ошибка обновления записи в БД. Данные: " . PHP_EOL . print_r( $values, true ) );
				}
			} else {
				if ( method_exists( $this, 'before_create' ) ) {
					$this->before_create( $values );
				}
				if ( $id = $this->instance->add( $values ) ) {
					return array_merge( [ "id" => $id ], $values );
				} else {
					throw new Exception( "Ошибка записи в БД. Данные: " . PHP_EOL . print_r( $values, true ) );
				}
			}
		}

		return false;
	}

	function clearFields( $values = [] ) {

		if ( empty( $values ) || empty( $this->fields ) ) {
			return [];
		}

		return array_intersect_key( $values, array_flip( $this->fields ) );
	}

	/**
	 * @param $key
	 *
	 * @return bool
	 * @throws Exception
	 */
	function objectIsObtainedFrom( $key ) {

		$TBD                     = $this->getTDB();
		$this->current_procedure = new GetProcedure( $this->procedureName, GetProcedure::getType( 'OBJECT_IS_OBTAINED_FROM' ) );
		$preparedParams          = $this->prepareForProcedure( [
			                                                       $this->siteKeyObtain => $key,
		                                                       ] );
		$query                   = $this->current_procedure->getSql( $preparedParams );
		$TBD->createCommand( $query->sql )->execute();
		if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
			$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
			if ( isset( $data[ "@" . $this->tdbError ] ) ) {
				throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
			}
		} else {
			throw new Exception( "Запрос не отработал\n " . $query->sql );
		}
		if ( method_exists( $this, 'after_obtain' ) ) {
			$this->after_obtain( $key );
		}

		return true;
	}

	/**
	 * @param array $values
	 *
	 * @return array|bool
	 * @throws Exception
	 */
	function createObjectTo( $values = [] ) {

		if ( empty( $values ) && method_exists( $this, 'getDataForCreate' ) ) {
			if ( ! $values = $this->getDataForCreate() ) {
				return false;
			}
		}
		if ( method_exists( $this, 'before_createObjectTo' ) ) {
			$values = $this->before_createObjectTo( $values );
		}
		$TBD                     = $this->getTDB();
		$this->current_procedure = new GetProcedure( $this->procedureName, $this->current_procedure_type );
		$preparedParams          = $this->prepareForProcedure( $values );
		$query                   = $this->current_procedure->getSql( $preparedParams );
		$TBD->createCommand( $query->sql )->execute();
		if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
			$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
			if ( isset( $data[ "@" . $this->tdbError ] ) ) {
				throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
			}
		} else {
			throw new Exception( "Запрос не отработал\n " . $query->sql );
		}
		if ( method_exists( $this, 'after_createObjectTo' ) ) {
			$values = $this->after_createObjectTo( $values );
		}

		return $values;
	}

	/**
	 * @param $key
	 *
	 * @return bool
	 * @throws Exception
	 */
	function saveObjectTo( $key ) {

		$continue = true;
		if ( method_exists( $this, 'before_saveObjectTo' ) ) {
			$continue = $this->before_saveObjectTo( $key );
		}
		if ( $continue ) {
			$TBD                     = $this->getTDB();
			$this->current_procedure = new GetProcedure( $this->procedureName, GetProcedure::getType( 'SAVE_OBJECT_TO' ) );
			$preparedParams          = $this->prepareForProcedure( [
				                                                       $this->siteKeySave => $key,
			                                                       ] );
			$query                   = $this->current_procedure->getSql( $preparedParams );
			$TBD->createCommand( $query->sql )->execute();
			if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
				$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
				if ( isset( $data[ "@" . $this->tdbError ] ) ) {
					throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
				}
			} else {
				throw new Exception( "Запрос не отработал\n " . $query->sql );
			}
			if ( method_exists( $this, 'after_saveObjectTo' ) ) {
				$this->after_saveObjectTo( $key );
			}
		}

		return true;
	}

	/**
	 * @return \yii\db\Connection
	 * @throws \yii\db\Exception
	 */
	public function getDB() {

		/** @var \yii\db\Connection $connection */
		$connection = Yii::$app->pdb;
		try {
			$connection->createCommand( "DO 1;" )->execute();
		} catch ( \yii\db\Exception $e ) {
			$connection->close();
			$connection->open();
			echo "> getDB() reconnected" . PHP_EOL;
		}

		return $connection;
	}

	/**
	 * @param array $params
	 *
	 * @return array|bool
	 * @throws Exception
	 */
	function getObjectRow( $params = [] ) {

		$TBD                     = $this->getTDB();
		$this->current_procedure = new GetProcedure( $this->procedureName . ( $this->get_row_prefix ?
			                                             "_" . $this->get_row_prefix :
			                                             "" ), GetProcedure::getType( 'GET_OBJECTS_ROW_FROM' ) );
		$preparedParams          = $this->prepareForProcedure( $params );
		$query                   = $this->current_procedure->getSql( $preparedParams );
		if ( $TBD->createCommand( $query->sql )->execute() ) {
			if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
				if ( isset( $data[ "@" . $this->tdbError ] ) && ! empty( $this->parse_error( "Error: " . $data[ "@" . $this->tdbError ] ) ) ) {
					$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
					throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
				}
			}
		} else {
			throw new Exception( "Запрос не отработал\n " . $query->sql );
		}
		if ( $data = $TBD->createCommand( "SELECT * FROM sExchData" )->queryOne() ) {
			$data = $this->convertFieldName( $data );
			$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
			$this->logData( [ "data" => $data, "query" => "SELECT * FROM sExchData;" ] );

			return $data;
		}

		return false;
	}

	/**
	 * @param $key
	 *
	 * @return bool|mixed
	 * @throws Exception
	 */
	function getObjectsRowCountFrom( $key ) {

		$TBD                     = $this->getTDB();
		$this->current_procedure = new GetProcedure( $this->procedureName . ( $this->get_row_count_prefix ?
			                                             "_" . $this->get_row_count_prefix :
			                                             "" ), GetProcedure::getType( 'GET_OBJECTS_ROWSCOUNT_FROM' ) );
		$preparedParams          = $this->prepareForProcedure( [
			                                                       $this->siteKeyObtain => $key,
		                                                       ] );
		$query                   = $this->current_procedure->getSql( $preparedParams );
		if ( $TBD->createCommand( $query->sql )->execute() ) {
			$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
		} else {
			throw new Exception( "Запрос не отработал\n " . $query->sql );
		}
		if ( $data = $TBD->createCommand( "SELECT * FROM sExchData" )->queryOne() ) {
			$data = $this->convertFieldName( $data );
			$this->logData( [ "data" => $data, "query" => "SELECT * FROM sExchData;" ] );

			return $data['p_LineCount'];
		}

		return false;
	}

	/**
	 * @param array $params
	 *
	 * @return bool
	 * @throws Exception
	 */
	function addObjectRowsTo( $params = [] ) {

		$TBD                     = $this->getTDB();
		$this->current_procedure = new GetProcedure( $this->procedureName . ( $this->add_row_prefix ?
			                                             "_" . $this->add_row_prefix :
			                                             "" ), GetProcedure::getType( 'ADD_OBJECTROWS_TO' ) );
		$preparedParams          = $this->prepareForProcedure( $params );
		$query                   = $this->current_procedure->getSql( $preparedParams );
		$TBD->createCommand( $query->sql )->execute();
		if ( $data = $TBD->createCommand( $query->select )->queryOne() ) {
			$this->logData( [ "data" => $this->prepareDataArray( $preparedParams ), "query" => $query->sql ] );
			if ( isset( $data[ "@" . $this->tdbError ] ) && ! empty( $this->parse_error( "Error: " . $data[ "@" . $this->tdbError ] ) ) ) {
				throw new Exception( "Ошибка вызова\n " . $query->sql . "\n " . $data[ "@" . $this->tdbError ] );
			}
		} else {
			throw new Exception( "Запрос не отработал\n " . $query->sql );
		}

		return true;
	}

	/**
	 * @return array
	 */
	public function getPool(): array {

		return $this->pool;
	}

	/**
	 * @param array $pool
	 */
	public function setPool( array $pool ) {

		$this->pool = $pool;
	}

	/**
	 * М-д удаляет из пула данные
	 *
	 * @param null $value
	 */
	public function delFromPool( $value = null ) {

		if ( isset( $value ) ) {
			unset( $this->pool[ array_search( $value, $this->pool ) ] );
		}
	}

	/**
	 * М-д возвращает набор меток пула данных в обмене
	 *
	 * @return false|array
	 */
	public function getPoolLabels() {

		return array_column( $this->pool, $this->getPoolLabelKey() );
	}

	/**
	 * М-д возвращает ключ, по которому происходит контроль данных в пуле данных обмена
	 *
	 * @return string
	 */
	public function getPoolLabelKey() {

		return $this->poolLabelKey;
	}

	/**
	 * М-д устанавливает ключ, по которому происходит контроль данных в пуле данных обмена
	 *
	 * @return string
	 */
	public function setPoolLabelKey() {

		$this->poolLabelKey = '';
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ИД заявки сайта
	 *
	 * @return string
	 */
	public function getOrderKeyID() {

		return "";
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "";
	}

	/**
	 * @param Exception $e
	 *
	 * @throws \yii\base\Exception
	 * @throws \ReflectionException
	 */
	public function error( Exception $e ) {

		echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" . PHP_EOL;
//		carLog( $e->getMessage(), $e->getFile(), $e->getLine() );
		var_dump( [ $e->getMessage(), $e->getFile(), $e->getLine() ] );
		echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" . PHP_EOL;
		$sClass = ( new \ReflectionClass( $this ) )->getShortName();
		if ( $sClass == "PaymentsCloud" ) {
			Log::error( [
				              "text" => $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ")",
			              ], "pay.cp.exchange" );
		}
		$this->logYiiError( $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ")" );
	}
}