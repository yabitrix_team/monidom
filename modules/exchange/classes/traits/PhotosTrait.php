<?php

namespace app\modules\exchange\classes\traits;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\Users;
use app\modules\exchange\models\Requests;
use Exception;
use Yii;

trait PhotosTrait {
	protected $allEvents = [];

	abstract function addObjectRowsTo( $params = [] );

	/**
	 * @return array|bool
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	function getDataForCreate() {

		$this->allEvents = array_column( ( new Events() )->getAll(), 'id', 'code' );
		$requestsEvents  = new RequestsEvents();
		$eventsCodes     = \is_array( $this->getEventID() ) ? $this->getEventID() : [ $this->getEventID() ];
		$eventsIDs       = [];
		foreach ( $eventsCodes as $code ) {
			$eventsIDs[] = $this->allEvents[ $code ];
		}
		if ( $event = $requestsEvents->getOneByFilter( [
			                                               'event_id'   => $eventsIDs,
			                                               'is_sended'  => 0,
			                                               'is_ignored' => 0,
		                                               ] )
		) {
			if ( $request = ( new Requests() )->getOne( $event['request_id'] ) ) {
				if ( $user = ( new Users() )->getOne( $request['user_id'] ) ) {
					if ( $request['guid'] ) {
						return [
							'id'      => $request['id'],
							'num'     => $request['num'],
							'guid'    => $request['guid'],
							'user_id' => $user['guid'],
						];
					}
					$this->ignoreEvent($event['request_id'], 'Фото заявки num=' . $request['num'] . ' по событию/ям ' . implode( ', ', $eventsCodes ) . ' не выгружены, guid заявки пуст');
				} else {
					$this->ignoreEvent($event['request_id'], 'Фото заявки num=' . $request['num'] . ' по событию/ям ' . implode( ', ', $eventsCodes ) . ' не выгружены, пользователь в заявке не установлен');
				}
			} else {
				( new RequestsEvents() )->update( $event['id'], [
					'is_ignored'    => 1,
					'ignore_reason' => 'Error: Заявка с id=' . $event['request_id'] . ' не найдена',
				] );
			}
		}

		return false;
	}

	function before_createObjectTo( $values ) {

		$values['created_at'] = date( 'Y-m-d H:i:s' );

		return $values;
	}

	/**
	 * @param $request
	 * @param $message
	 *
	 * @throws \yii\db\Exception
	 */
	private function ignoreEvent($request, $message)
	{
		$eventsCodes = is_array( $this->getEventID() ) ? $this->getEventID() : [ $this->getEventID() ];
		$eventsIDs   = [];
		foreach ( $eventsCodes as $code ) {
			$eventsIDs[] = $this->allEvents[ $code ];
		}
		( new RequestsEvents() )->update( 'request_id = ' . (int) $request . ' and event_id in (' . implode( ', ', $eventsIDs ) . ')', [
			'is_ignored'    => 1,
			'ignore_reason' => 'Error: ' . $message,
		] );
		Log::error( ["text" => $message], 'exchange.photos' );
		$this->logTbdError( $message );
	}

	/**
	 * @param $key
	 *
	 * @return bool
	 * @throws \yii\db\Exception
	 * @throws Exception
	 */
	function before_saveObjectTo( $key ) {

		Log::info( ["text" => "Старт отправки файлов по заявке с NUM = {$key}"], 'exchange.photos' );
		$webRoot = rtrim( Yii::getAlias( '@app' ), '/' ) . '/web';
		if ( ! $request = ( new Requests() )->getOneByFilter( [ $this->getSiteKeySave() => $key ] ) ) {
			throw new Exception( "Заявка не найдена с $this->getSiteKeySave()=" . $key );
		}
		Log::info( ["text" => "Данные по заявке с NUM = {$key}\n".print_r( $request, true )], 'exchange.photos' );
		if ( ! $request['files'] = ( new File() )->getByRequestId( $request['id'] ) ) {
			$this->ignoreEvent($request['id'], 'Файлы не найдены по заявке с id=' . $request['id']);
			return false;
		}
		Log::info( ["text" => "Список файлов по заявке с NUM = {$key}\n".print_r( $request['files'], true )], 'exchange.photos' );
		$arBinds = $this->getBinds();
		foreach ( $request['files'] as $file ) {
			if ( ! array_key_exists( $file['file_bind'], $arBinds ) ) {
				continue;
			}
			$file_data = print_r( $file, true );
			$filePath  = $webRoot . '/' . ltrim( $file['path'], '/' );
			if ( file_exists( $filePath ) ) {
				$fs = fopen( $filePath, 'rb' );
				if ( flock( $fs, LOCK_EX | LOCK_NB ) ) {
					if ( false !== ( $fData = fread( $fs, $file['size'] ) ) ) {
						if ( (int) $file['size'] !== ( $lenContent = \strlen( $fData ) ) ) {
							$this->ignoreEvent($request['id'], "Ошибка файла {$file['name']}\n в заявке с NUM = {$key}:\n 
							 не соответствует размер контента указанному в БД\n
							 db_data_size: {$file['size']}\n
							 content_file_size: {$lenContent}
							 \nFILE DATA:\n$file_data");
							return false;
						}
						unset( $lenContent );
						if ( false === ( $data_bin = base64_encode( $fData ) ) ) {
							$err = print_r( error_get_last(), true );
							$this->ignoreEvent($request['id'], "Ошибка base64_encode файла {$file['name']}\n в заявке с NUM = {$key}:\n $err \nFILE DATA:\n$file_data");
							return false;
						}
						$param = [
							'num'  => $key,
							'file' => $data_bin,
							'name' => $file['name'],
							'type' => $arBinds[ $file['file_bind'] ],
						];
						if ( ! $this->addObjectRowsTo( $param ) ) {
							$param = print_r( array_merge( $param, [ 'file' => 'file size ' . \strlen( $param['file'] ) ] ), true );
							$this->ignoreEvent($request['id'], "Ошибка выгрузки файла {$file['name']}\n в заявке с NUM = {$key}:\nошибка добавления строки Файл в ТБД\nFILE DATA:\n$file_data\nPARAMS DATA:\n$param");
							return false;
						}
					} else {
						$err = print_r( error_get_last(), true );
						$this->ignoreEvent($request['id'], "Ошибка выгрузки файла {$file['name']}\n в заявке с NUM = {$key}:\n $err \nFILE DATA:\n$file_data");
						return false;
					}
					flock( $fs, LOCK_UN );
					fclose( $fs );
				} else {
					$err = print_r( error_get_last(), true );
					$this->ignoreEvent($request['id'], "Ошибка выгрузки файла {$file['name']}\n в заявке с NUM = {$key}:\n $err \nFILE DATA:\n$file_data");
					return false;
				}
			} else {
				$err = print_r( error_get_last(), true );
				$this->ignoreEvent($request['id'], "Файл не существует {$filePath}: \n $err");
				return false;
			}
		}
		Log::info( ["text" => "Успешно отправлены файлы по заявке с NUM = {$key}"], 'exchange.photos' );

		return true;
	}

	/**
	 * @param $key
	 *
	 * @throws \yii\db\Exception
	 */
	function after_saveObjectTo( $key ) {

		if ( $arRequest = ( new Requests() )->getOneByFilter( [ $this->getSiteKeySave() => $key ] ) ) {
			$eventsCodes = \is_array( $this->getEventID() ) ? $this->getEventID() : [ $this->getEventID() ];
			$eventsIDs   = [];
			foreach ( $eventsCodes as $code ) {
				$eventsIDs[] = $this->allEvents[ $code ];
			}
			( new RequestsEvents() )->update( 'request_id = ' . (int) $arRequest['id'] . ' and event_id in (' . implode( ', ', $eventsIDs ) . ') and is_ignored = 0', [ 'is_sended' => 1 ] );
		}
	}

	/**
	 * @return int
	 */
	abstract protected function getEventID(): int;

	/**
	 * @return array
	 */
	abstract protected function getBinds(): array;

	/**
	 * @return string
	 */
	abstract protected function getSiteKeySave(): string;
}