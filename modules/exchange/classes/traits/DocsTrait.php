<?php

namespace app\modules\exchange\classes\traits;
trait DocsTrait {
	public function getAssociated() {

		return array_merge(
			parent::getAssociated(), [
				                       "guid" => "p_ID_1C",
				                       "line" => "p_LineNo",
			                       ]
		);
	}

	protected function dataEncode( $data ) {

		$code = false;
		if ( ! empty( $data ) ) {
			$converter = [ '+' => '-', '/' => '_', '=' => '' ];
			$code      = strtr( base64_encode( serialize( $data ) ), $converter );
			//если параметр в URL длинее 255 символов и нет слэша, то закрывает доступ к странице
			if ( strlen( $code ) > 250 ) {
				$code = substr_replace( $code, "/", 250, 0 );
			}
		}

		return $code;
	}

	/**
	 * Возвращает ключ таблицы обмена, в которой содержится ГУИД заявки из 1с
	 *
	 * @return string
	 */
	public function getOrderKeyGUID() {

		return "p_ID_1C";
	}
}