<?php

namespace app\modules\exchange\classes\traits;

use app\modules\app\v1\models\VersionsDictionaries;

/**
 * Trait VersionDictionaryTrait - трейт для работы с версионностью справочников
 */
trait VersionDictionaryTrait {
	/**
	 * Метод отрабатывает после вставки данных при обмене
	 *
	 * @param array $element
	 * @param array $data
	 */
	public function after_insertData( &$data = [], $values = [] ) {

		$model = new VersionsDictionaries();
		$model->load( [ 'dictionary' => $this->getDictionaryName() ], '' );
		$res = $model->save();
		if ( ! is_numeric( $res ) ) {
			$error = is_object( $res ) ? $res->errors : $res;
			$this->logYiiError( print_r( $error, true ) );
		}
	}

	/**
	 * Метод логирования ошибок
	 *
	 * @param string $text - текст ошибки
	 *
	 * @return mixed
	 */
	abstract public function logYiiError( string $text );

	/**
	 * Метод получения имени справочника, для которого записываем версию
	 * имя соответствует идентификатору контроллера
	 * пример: AboutCompanyController.php = about-company
	 *
	 * @return string
	 */
	abstract public function getDictionaryName(): string;
}