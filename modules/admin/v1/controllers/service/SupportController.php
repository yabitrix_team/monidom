<?php

namespace app\modules\admin\v1\controllers\service;

use app\modules\app\v1\controllers\service\SupportController as SupportControllerApp;

/**
 * Class SupportController - контроллер взаимодействия с сервисом Support
 *
 * @package app\modules\web\v1\controllers\service
 */
class SupportController extends SupportControllerApp {
}
