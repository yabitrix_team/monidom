<?php
namespace app\modules\admin\v1\controllers\info;

use app\modules\app\v1\controllers\info\StatusController as StatusControllerApp;
use app\modules\app\v1\models\Statuses;

/**
 * @inheritDoc
 */
class StatusController extends StatusControllerApp
{
    /**
     * @inheritDoc
     */
    public function actionIndex()
    {
        $data = (new Statuses())->getAll();
        /**
         * подготавливаем массив, очищая его от ненужных данных
         */
        array_walk(
            $data,
            function (&$item) {
                unset($item['active'], $item['sort'], $item['created_at'], $item['updated_at']);
            }
        );
        $newData = []; //Массив для вывода статусов в массиве по ID
        for ($i = 0; $i < count($data); $i++) {
            $newData = $newData + [strval($data[$i]['id']) => $data[$i]];
        }

        return $this->getClassAppResponse()::get($newData);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['role_partner_admin'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированны.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнения операции.');
                    }
                },
            ],
        ];
    }
}