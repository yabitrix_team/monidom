<?php

namespace app\modules\admin\v1\controllers\user;

use app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use yii\filters\AccessControl;

class AuthController extends AuthControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'create' ],
						'roles'   => [ '?', '@' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'index', 'delete' ],
						'roles'   => [ 'role_partner_admin' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}
}
