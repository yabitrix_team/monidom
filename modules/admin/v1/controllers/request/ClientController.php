<?php

namespace app\modules\admin\v1\controllers\request;

use app\modules\admin\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\request\ClientController as ClientControllerApp;
use app\modules\app\v1\models\Clients;
use app\modules\app\v1\models\Points;
use app\modules\app\v1\models\Requests;
use app\modules\app\v1\models\Statuses;
use app\modules\app\v1\models\StatusesJournal;
use app\modules\app\v1\models\Users;
use Yii;
use yii\web\JsonParser;

/**
 * Class ClientController - контроллер для работы со списками заявок
 *
 * @package app\modules\web\v1\controllers
 */
class ClientController extends ClientControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [
							'index',
							'active',
							'closed',
							'wait',
						],
						'roles'   => [ 'role_partner_admin' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированны.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнения операции.' );
					}
				},
			],
		];
	}



	/*
	 * Поле для передачи параметров пагинации
	 */
	private $params;
	/*
	 * Поле для передачи типа статуса
	 */
	private $state;
    /*
     * Поле для полного количества заявок
     */
	private $requestIdActual;

    /**
     * Получение всех заявок по статусу по партнеру (для кабинета partner-admin
     *
     * @param      $params    - параметры пагинации - смотри моде5ль
     * @param null $state
     *                        $this->requestWaitGUID; - подвисшие
     *                        $this->requestCloseGUID; - закрытые
     *                        $this->requestActiveGUID; - активные
     *                        null; - в работе ($this->requestInProgressGUID)
     *
     * @throws \Exception
     */
	private function getStatusesRequestsByState() {

		if (empty($this->params)) {
			throw new \Exception('На заданы параметры');
		}

		$flag = false; //Флаг что выводим заявки без статуса (только для раздела заявки в работе)
		if (empty($this->state)) {
			$this->state = $this->requestInProgressGUID;
			$flag = true;
		}

		//Сразу делаем валидацию для проверки входных данных
		$clients = new Clients();
		if ( ! $clients->load( $this->params, '' ) ) {
			throw new \Exception( 'Не удалось загрузить данные в модель. Обратиесь к администратору.' );
		} elseif ( ! $clients->validate() ) {
			return $this->getClassAppResponse()::get( false, $clients->firstErrors );
		}

		//Получаем ID всех заявок по точке пользователя
		$request = new Requests();
		$request->setDefaultSelectFields( [ 'id' ] );
		$pointId   = ( new Users() )->getOne( Yii::$app->user->id ) ['point_id'];
		$partnerId = ( new Points() )->getOne( $pointId ) ['partner_id'];
		// берем все заявки по партнеру todo[burovyuri]: вынести в модель
		$requestIdList = Yii::$app->db->createCommand( '
SELECT `r`.`id` FROM `requests` AS `r`
LEFT JOIN `points` AS `po` ON `po`.`id` = `r`.`point_id`
LEFT JOIN `partners` AS `pn` ON `pn`.`id` = `po`.`partner_id`
WHERE `pn`.`id`= :partner_id AND `r`.`created_at` >= :dateFrom AND `r`.`created_at` < :dateTo
ORDER BY `r`.`created_at` DESC
												', [
			'partner_id' => $partnerId,
			'dateFrom'   => $clients->dateFrom . ' 00:00:00',
			'dateTo'     => $clients->dateTo . '23:59:59',
		] )->queryAll();

        //Получаем статусы по всем заявкам
		$statusIdList = [];
		foreach ( $requestIdList as $id ) {
			$status = StatusesJournal::instance()->getCurrentByRequestID( $id['id'] );
			array_push( $statusIdList, empty( $status ) ? [] : $status );
		}

		//Оставляем только нужные нам статусы
		$statuses       = Statuses::instance()->getAllByFilter( [ 'guid' => $this->state  ] );
		$statusesActual = array_column( $statuses, 'id' );

        //Оставляем заявки только со статусом Actual и с пустым статусом
		//результирующий список заявок
		$requestIdActual = [];
		//результьтирующий список статусов
		$statusesIdActual = [];
		for ( $i = 0; $i < count( $requestIdList ); $i ++ ) {
			//тут указываем какие статусы из актуальных и если нужно указываем что нужны заявки без статуса

			if ($flag) { //Флаг что проверяем пустые статусы
				if ( empty( $statusIdList[ $i ] ) || in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
					array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
					array_push( $statusesIdActual, $statusIdList[ $i ] );
				}
			} else {
				if ( in_array( $statusIdList[ $i ]['status_id'], $statusesActual ) ) {
					array_push( $requestIdActual, $requestIdList[ $i ]['id'] );
					array_push( $statusesIdActual, $statusIdList[ $i ] );
				}
			}

		}

		if ( count( $requestIdActual ) == 0 ) {
			return 0;
		}
		//Обогащаем заявки
		//Получаем заявки по ID с учетом пагинации
		$result = $clients->getRequestWithPagination( $requestIdActual );

		//Чистим массив статусов нужно убрать пустые значения
		$statusesIdForMerg = [];
		foreach ( $statusesIdActual as $item ) {
			if ( $item != [] ) {
				array_push( $statusesIdForMerg, $item );
			}
		}
		//Обогащаем заявки статусами
		$result = array_map( function( $v ) use ( $statusesIdForMerg ) {

			$column = array_column( $statusesIdForMerg, 'request_id' );
			$key    = array_search( $v['id'], $column );
			if ( $key === false ) {
				$v['statuses_id'] = '';
			} else {
				$v['statuses_id'] = $statusesIdForMerg[ $key ]['status_id'];
			}
			unset($v['id']);

			return $v;
		}, $result );
		$this->requestIdActual = $requestIdActual;
		return $result;
	}

    /**
     * Экшен получения всех заявок в статусе работе
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */
	public function actionIndex() {

         $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );

         $this->params = $reqBody;
         $this->state = null; // null это - в работе ($this->requestInProgressGUID)
         $result = $this->getStatusesRequestsByState();

         if ($result==0) {
             return $this->getClassAppResponse()::get( [
                                                          'requests'      => [],
                                                          'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                          'total count'   => 0,
                                                      ] );
         }

         return $this->getClassAppResponse()::get( [
                                                      'requests'      => $result,
                                                      'count in page' => Yii::$app->params['web']['request']['pagination'],
                                                      'total count'   => count( $this->requestIdActual ),
                                                  ] );
	}

	public function actionActive() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );

        $this->params = $reqBody;
        $this->state = $this->requestActiveGUID;
        $result = $this->getStatusesRequestsByState();

        if ($result==0) {
            return $this->getClassAppResponse()::get( [
                'requests'      => [],
                'count in page' => Yii::$app->params['web']['request']['pagination'],
                'total count'   => 0,
            ] );
        }

        return $this->getClassAppResponse()::get( [
            'requests'      => $result,
            'count in page' => Yii::$app->params['web']['request']['pagination'],
            'total count'   => count( $this->requestIdActual ),
        ] );
	}

    /**
     * Экшен получения всех закрытых заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     */
	public function actionClosed() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );

        $this->params = $reqBody;
        $this->state = $this->requestCloseGUID;
        $result = $this->getStatusesRequestsByState();

        if ($result==0) {
            return $this->getClassAppResponse()::get( [
                'requests'      => [],
                'count in page' => Yii::$app->params['web']['request']['pagination'],
                'total count'   => 0,
            ] );
        }

        return $this->getClassAppResponse()::get( [
            'requests'      => $result,
            'count in page' => Yii::$app->params['web']['request']['pagination'],
            'total count'   => count( $this->requestIdActual ),
        ] );
	}

    /**
     * Экшен получения всех подвисших заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\web\BadRequestHttpException
     */
	public function actionWait() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );

        $this->params = $reqBody;
        $this->state = $this->requestWaitGUID;
        $result = $this->getStatusesRequestsByState();

        if ($result==0) {
            return $this->getClassAppResponse()::get( [
                'requests'      => [],
                'count in page' => Yii::$app->params['web']['request']['pagination'],
                'total count'   => 0,
            ] );
        }

        return $this->getClassAppResponse()::get( [
            'requests'      => $result,
            'count in page' => Yii::$app->params['web']['request']['pagination'],
            'total count'   => count( $this->requestIdActual ),
        ] );
	}
}
