<?php

namespace app\modules\admin\v1\models;

use app\modules\app\v1\models\Clients as ClientsApp;

/**
 * Class Clients - модель для работы со списком заявок
 *
 * @package app\modules\app\v1\models
 */
class Clients extends ClientsApp {

	/**
	 * Получение списка заявок по ID с учетом пагинации
	 */
	public function getRequestWithPagination( array $requestsId ) {

		if ( ! $this->validate() ) {
			throw new \Exception( 'Входные данные не проходят валидацию. Повторите запрос с валидными данными или обратитесь к администратору.' );
		}
		if ( ! yii::$app->params['web']['request']['pagination']
		     || ! is_int( yii::$app->params['web']['request']['pagination'] ) ) {
			throw new \Exception( "Не заданы параметры пагинации в настройках сервера. Обратитесь к администратору." );
		}
		if ( $this->pagin < 1 ) {
			throw new \Exception( "Неверно указана страница пагинации, должна начинаться с 1." );
		}
		$this->pagin --;

		return Yii::$app->db->createCommand( '
		SELECT
			`r`.`id`,
			`r`.`code`,
			`r`.`created_at`,
			`r`.`num`,
			`r`.`summ`,
			`r`.`client_first_name`,
			`r`.`client_last_name`,
			`c`.`is_null`,
			`c`.`commission_cash_withdrawal` as `comission`
		FROM ' . $this->getTableName() . ' as `r`
		left join `request_commission` as `c` ON `r`.`id` = `c`.`request_id`
			WHERE `r`.`id` IN (' . implode( ',', $requestsId ) . ')
				AND `r`.`created_at` BETWEEN \'' . $this->dateFrom . '\' AND \'' . $this->dateTo . ' 23:59:59\'
			ORDER BY `r`.`created_at` DESC
        LIMIT ' . yii::$app->params['web']['request']['pagination'] . '
        OFFSET ' . yii::$app->params['web']['request']['pagination'] * ( $this->pagin ) )
		                    ->queryAll();
	}

}