<?php
namespace app\modules\call\v1\models;

use app\modules\app\v1\models\Requests as AppRequests;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\Users;
use Yii;
use yii\web\JsonParser;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends AppRequests
{
    public    $lcrm_id;
    protected $client_id;
    protected $user_id;

    /**
     * @var array - поля создания заявки для сценария Создание,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioCreate = [
        'summ',
        'date_return',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'credit_product_id',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_passport_number',
        'client_passport_serial_number',
        'client_birthday',
        'client_region_id',
        'client_mobile_phone',
        'comment_client',
        'pre_crediting',
        'lcrm_id',
    ];
    protected $reqFieldCreate      = [
        'summ',
        'date_return',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'auto_price',
        'credit_product_id',
        'client_first_name',
        'client_last_name',
        'client_passport_number',
        'client_passport_serial_number',
        'client_birthday',
        'client_region_id',
        'client_mobile_phone',
    ];
    /**
     * @var string - место создания заявки, смотреть в таблице requests_origin
     * допустимые варианты: MOBILE, PARTNER, CALL, SIDES_CALL
     */
    protected $requestOrigin = RequestsOrigin::IDENTIFIER_SIDES_CALL;

    /**
     * Метод получения идентификатора точки
     *
     * @return integer - вернет идентификатор точки или выбросит исключение
     * @throws \InvalidArgumentException
     * @throws \yii\web\BadRequestHttpException
     */
    protected function getPointId()
    {
        $post = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');

        return $post["point_id"];
    }

    /**
     * Метод получения идентификатора пользоателя (подписант)
     *
     * @return integer - вернет идентификатор подписанта клиента или выбросит исключение
     * @throws \InvalidArgumentException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    protected function getUserId()
    {
        $post = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        if ($user = (new Users())->getOneByFilter(["point_id" => $post["point_id"], "active" => 1])) {
            return $user["id"];
        }
        throw new \Exception("По выбранной точке пользователи не найдены");
    }
}