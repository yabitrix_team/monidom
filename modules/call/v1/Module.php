<?php

namespace app\modules\call\v1;
/**
 * Class Module - модуль для работы с кабинетом удаленного КЦ
 *
 * @package app\modules\call\v1
 */
class Module extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\call\v1\controllers';

	/**
	 * @inheritdoc
	 */
	public function init() {

		parent::init();
	}
}
