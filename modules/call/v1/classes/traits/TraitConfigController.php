<?php
namespace app\modules\call\v1\classes\traits;

use app\modules\call\v1\classes\AppResponse;
use app\modules\app\v1\classes\traits\TraitConfigController as TraitConfigControllerApp;

/**
 * Trait TraitSomeController - трейт содержащий методы-настройки для контроллеров,
 * в данном трейте необходимо подлкючить трейт AbstractTraitSomeController
 * для приведения к однотипности трейтов-настройки
 *
 * @package app\modules\app\v1\classes\traits
 */
trait TraitConfigController
{
    use TraitConfigControllerApp;

    /**
     * Метод возвращающий namespace для класса AppResponse
     * при необходимости переопределяем данный метод и указываем
     * необходимый namespace в новом подключаемом trait
     *
     * @return string|AppResponse - вернет строку содержащую namespace класса AppResponse
     */
    protected function getClassAppResponse()
    {
        return AppResponse::class;
    }
}