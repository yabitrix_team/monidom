<?php
namespace app\modules\call\v1\classes\exceptions;

/**
 * Исключение по лиду
 *
 * @package app\modules\call\v1\classes\exceptions
 */
class LeadException extends \Exception implements \Throwable
{
}