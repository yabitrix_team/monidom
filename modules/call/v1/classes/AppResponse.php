<?php
namespace app\modules\call\v1\classes;

/**
 * Class AppResponse - класс формирования ответов веб-сервисами
 *
 * @package app\modules\app\v1\classes
 */
class AppResponse
{
    /**
     * Метод формирования ответа веб-сервисами
     *
     * @param array  $data      - Данные для передачи
     * @param array  $meta      - Схема отображения компонентов во frontend-е
     * @param array  $validates - Ошибки валидации полей, пример:
     *                          ['summ'=>'Поле указанно некорректно']
     * @param string $msg       - Сообщение исключения, пример:
     *                          'Параметры подключения указаны некорректно'
     *
     * @return array - Вернет сформированный массив, пример
     *               [
     *                  error=>[
     *                      validate => [
     *                          field_name => 'error message'
     *                      ]
     *                      message => 'fatal error',
     *                  ],
     *                  data=>[]
     *                  meta=>[]
     *              ]
     */
    public static function get($data = [], $meta = [], $validates = [], $msg = "", $code = 0)
    {
        $result          = [];
        $result['error'] = [];
        empty($validates) ?: $result['error']['validate'] = $validates;
        empty($msg) ?: $result['error']['message'] = $msg;
        empty($code) ?: $result['error']['code'] = $code;
        empty($version) ?: $result['version'] = $version;
        $result['data'] = !empty($data) ? $data : [];
        empty($meta) ?: $result['meta'] = $meta;

        return $result;
    }
}