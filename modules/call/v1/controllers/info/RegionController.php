<?php

namespace app\modules\call\v1\controllers\info;

use app\modules\app\v1\controllers\info\RegionController as RegionControllerApp;

class RegionController extends RegionControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => \yii\filters\AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'index' ],
						'roles'   => [ 'role_call' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированы.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}

	public function actionIndex() {

		$result = parent::actionIndex();
		if ( ! empty( $result['data'] ) ) {

			$result['data'] = array_map( function( $v ) {

				unset( $v['created_at'] );
				unset( $v['updated_at'] );
				unset( $v['active'] );
				unset( $v['sort'] );

				return $v;
			}, $result['data'] );
		}

		return $result;
	}
}
