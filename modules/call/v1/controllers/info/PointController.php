<?php
namespace app\modules\call\v1\controllers\info;

use app\modules\app\v1\controllers\info\PointController as AppPointController;
use app\modules\app\v1\models\Points;
use Yii;

/**
 * Class PointController - контроллер для работы со справочником Точки
 *
 * @package app\modules\app\v1\controllers
 */
class PointController extends AppPointController
{
    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $data = Yii::$app->db->createCommand(
            "select p.id, p.name from groups g
                    right join user_group ug
                        on g.id=ug.group_id
                    right join users u
                        on ug.user_id=u.id
                    right join points p
                        on p.id=u.point_id
                    where g.identifier='partner_user' and u.active=1 and u.id not in (:lkk, :mp)
                    group by p.id",
            [
                ":lkk" => (int) yii::$app->params['client']['settings']['user_id'],
                ":mp"  => (int) yii::$app->params['mp']['settings']['user_id'],
            ]
        )->queryAll();

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['role_call'],
                    ],
                ],
                'denyCallback' => function () {

                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированы.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнение операции.');
                    }
                },
            ],
        ];
    }
}