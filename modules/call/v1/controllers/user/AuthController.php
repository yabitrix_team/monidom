<?php

namespace app\modules\call\v1\controllers\user;

use app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use yii\filters\AccessControl;

class AuthController extends AuthControllerApp {
	public function behaviors() {

		return [
			'access' => [
				'class'        => AccessControl::className(),
				'rules'        => [
					[
						'allow'   => true,
						'actions' => [ 'create' ],
						'roles'   => [ '?', '@' ],
					],
					[
						'allow'   => true,
						'actions' => [ 'index', 'delete' ],
						'roles'   => [ 'role_call' ],
					],
				],
				'denyCallback' => function() {

					if ( \Yii::$app->user->isGuest ) {
						throw new \yii\web\HttpException( 401, 'Вы не аутентифицированы.' );
					} else {
						throw new \yii\web\HttpException( 403, 'У вас нет прав на выполнение операции.' );
					}
				},
			],
		];
	}
}
