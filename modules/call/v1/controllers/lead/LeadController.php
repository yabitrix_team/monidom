<?php
namespace app\modules\call\v1\controllers\lead;

use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\LcrmLead;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\Users;
use app\modules\call\v1\classes\exceptions\LeadException;
use app\modules\call\v1\models\Requests;
use app\modules\call\v1\classes\traits\TraitConfigController;
use Yii;
use yii\web\JsonParser;
use yii\rest\Controller;

class LeadController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $page                   = (int) Yii::$app->request->get("page") ?: 1;
        $size                   = (int) Yii::$app->request->get("size") ?: 10;
        $filter                 = Yii::$app->request->get("filter") ?: "";
        $leads                  = LcrmLead::instance()->getList($page, $size, $filter);
        $leads                  = array_map(
            function ($lead) {
                $lead["client_mobile_phone"] =
                    preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "+7 ($1) $2-$3", $lead["client_mobile_phone"]);

                return $lead;
            },
            $leads
        );
        $meta["mobile_headers"] = $meta["headers"] = [
            [
                "code" => "id",
                "name" => "ID",
            ],
            [
                "code" => "client_mobile_phone",
                "name" => "Телефон",
            ],
            [
                "code" => "code",
            ],
        ];

        $meta['server_time'] = date("Y-m-d\TH:i:s");
        return $this->getClassAppResponse()::get(
            [
                "page"  => $page,
                "size"  => $size,
                "count" => ceil(LcrmLead::instance()->getAllCnt($filter) / $size),
                "items" => $leads,
            ],
            $meta
        );
    }

    /**
     * @param $code
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        try {
            if (!strlen($code)) {
                throw new LeadException("Код лида не может быть пустым");
            }
            if (!$lead = LcrmLead::instance()->getOne($code)) {
                throw new LeadException("Лид не найден или уже отправлен", 404);
            }

            return $this->getClassAppResponse()::get($lead);
        } catch (LeadException $e) {
            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {
        try {
            $post = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (!$arLead = LcrmLead::instance()->getOne($post["code"])) {
                throw new LeadException("Лид с кодом ".$post["code"]." не найден или уже отправлен", 404);
            }
            // обновление лида
            $lead             = new LcrmLead();
            $lead->scenario   = LcrmLead::SCENARIO_UPDATE;
            $lead->attributes = $post;
            if (!$user = (new Users())->getOneByFilter(["point_id" => $post["point_id"], "active" => 1])) {
                throw new LeadException("По выбранной точке id=".$post["point_id"]." пользователи не найдены");
            }
            $lead->user_id    = $user["id"];
            $lead->updated_at = date("Y-m-d H:i:s");
            if (!$lead->validate()) {
                return $this->getClassAppResponse()::get([], [], $lead->firstErrors);
            }
            $lead->update($arLead["id"]);
            Log::info(
                [
                    "text"    => "Лид обновлен: ".print_r($lead->attributes, true),
                    "lcrm_id" => $arLead["lcrm_id"],
                    "user_id" => Yii::$app->user->getId(),
                ],
                "call.lead.create"
            );
            // создание заявки
            $request = new Requests(['scenario' => Requests::SCENARIO_CREATE]);
            if (!$request->load(array_merge($arLead, $post), '')) {
                throw new LeadException('Не переданы данные для создания заявки');
            }
            $result = $request->create();
            if (is_a($result, \Exception::class)) {
                throw new \Exception($result->getMessage(), $result->getCode());
            }
            if (is_a($result, Requests::class)) {
                return $this->getClassAppResponse()::get([], [], $result->firstErrors);
            }
            $requestId = Yii::$app->db->lastInsertID;
            Log::info(
                [
                    "text"       => "Заявка создана с id=".$requestId,
                    "lcrm_id"    => $arLead["lcrm_id"],
                    "request_id" => $requestId,
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "call.lead.create"
            );
            // привязка ID заявки к лиду
            $lead             = new LcrmLead();
            $lead->scenario   = LcrmLead::SCENARIO_REQUEST;
            $lead->request_id = $requestId;
            $lead->updated_at = date("Y-m-d H:i:s");
            $lead->update($arLead["id"]);
            Log::info(
                [
                    "text"       => "Лид привязан к заявке с id=".$requestId,
                    "lcrm_id"    => $arLead["lcrm_id"],
                    "request_id" => $requestId,
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "call.lead.create"
            );
            //выгрузка заявки в МФО
            $events = array_column(Events::instance()->getAll(), "id", "code");
            $event = new RequestsEvents();
            $event->load(["request_id" => $requestId, "event_id" => $events[701]], '');
            if (!$event->validate()) {
                throw new LeadException($event->firstErrors);
            }
            $result = $event->create();
            if (!is_numeric($result)) {
                throw new LeadException('Произошла ошибка при попытке прикрепления события к заявке');
            }
            Log::info(
                [
                    "text"       => "Заявка с id=".$requestId." выгружена в МФО",
                    "lcrm_id"    => $arLead["lcrm_id"],
                    "request_id" => $requestId,
                    "user_id"    => Yii::$app->user->getId(),
                ],
                "call.lead.create"
            );

            return $this->getClassAppResponse()::get(['success' => true]);
        } catch (LeadException $e) {
            Log::error(
                ["text" => $e->getMessage(), "lcrm_id" => $arLead["lcrm_id"], "user_id" => Yii::$app->user->getId()],
                "call.lead.create"
            );

            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $code
     *
     * @return array
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($code)
    {
        try{
            $lead        = new LcrmLead(['scenario' => LcrmLead::SCENARIO_UPDATE_CALLBACK]);
            $arLead = $lead->getOne($code);
            if (empty($arLead) && !is_array($arLead)) {
                $msg = empty($arLead)
                    ? 'При попытке обновления лида не уадлось найти лид, обратитесь к администратору'
                    : 'При попытке обновления лида произошла неизвестная ошибка, обратитесь к администратору';
                throw new LeadException($msg);
            }
            $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (!$lead->load($reqBody, '')) {
                throw new LeadException('Не переданы данные для обновления лида');
            }
            if (!$lead->validate()) {
                return $this->getClassAppResponse()::get(false, $lead->firstErrors);
            }
            /**
             * update возвращает кол-во затронутых строк. поэтому,
             * если посылать одинаковые данные, то во 2м, 3м ... случае
             * получаем 0.
             * исходя из этого исключения (если будут) выплывут на уровне
             * модели и здесь проверять результат нет смысла.
             */
            $lead->update($arLead["id"]);
            Log::info(
                [
                    "text"    => "Лид обновлен: ".print_r($lead->attributes, true),
                    "lcrm_id" => $arLead["lcrm_id"],
                    "user_id" => Yii::$app->user->getId(),
                ],
                "call.lead.update"
            );
            return $this->getClassAppResponse()::get(
                [
                    'update_lead' => true,
                ]
            );
        } catch (LeadException $e) {
            Log::error(
                ["text" => $e->getMessage(), "user_id" => Yii::$app->user->getId()],
                "call.lead.update"
            );

            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }

    }

    /**
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function actionNew()
    {
        try {
            $post = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            // создание заявки
            $request = new Requests(['scenario' => Requests::SCENARIO_CREATE]);
            if (!$request->load($post, '')) {
                throw new LeadException('Не переданы данные для создания заявки');
            }
            $result = $request->create();
            if (is_a($result, \Exception::class)) {
                throw new \Exception($result->getMessage(), $result->getCode());
            }
            if (is_a($result, Requests::class)) {
                return $this->getClassAppResponse()::get([], [], $result->firstErrors);
            }
            $requestId = Yii::$app->db->lastInsertID;
            Log::info(
                [
                    "text"       => "Заявка создана с id=".$requestId,
                    "user_id"    => Yii::$app->user->getId(),
                    "request_id" => $requestId,
                ],
                "call.lead.request"
            );
            //выгрузка заявки в МФО
            $events = array_column(Events::instance()->getAll(), "id", "code");
            $event = new RequestsEvents();
            $event->load(["request_id" => $requestId, "event_id" => $events[701]], '');
            if (!$event->validate()) {
                throw new LeadException($event->firstErrors);
            }
            $result = $event->create();
            if (!is_numeric($result)) {
                throw new LeadException('Произошла ошибка при попытке прикрепления события к заявке');
            }
            Log::info(
                [
                    "text"       => "Заявка с id=".$requestId." выгружена в МФО",
                    "user_id"    => Yii::$app->user->getId(),
                    "request_id" => $requestId,
                ],
                "call.lead.request"
            );

            return $this->getClassAppResponse()::get(['success' => true]);
        } catch (LeadException $e) {
            Log::error(
                ["text" => $e->getMessage(), "user_id" => Yii::$app->user->getId()],
                "call.lead.request"
            );

            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $code
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionDelete($code)
    {
        try {
            if (!strlen($code)) {
                throw new LeadException("Код лида не может быть пустым");
            }
            if (!$lead = LcrmLead::instance()->getOne($code)) {
                throw new LeadException("Лид не найден или уже удален", 404);
            }
            LcrmLead::instance()->delete($lead["id"]);
            Log::info(
                [
                    "text"    => "Лид удален с id=".$lead["id"],
                    "user_id" => Yii::$app->user->getId(),
                    "lcrm_id" => $lead["lcrm_id"],
                ],
                "call.lead.delete"
            );

            return $this->getClassAppResponse()::get(["success" => true]);
        } catch (LeadException $e) {
            Log::error(
                ["text" => $e->getMessage(), "user_id" => Yii::$app->user->getId()],
                "call.lead.delete"
            );

            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $code
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionRecover()
    {
        try {
            $post = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
            if (!strlen($post["code"])) {
                throw new LeadException("Код лида не может быть пустым");
            }
            if (LcrmLead::instance()->getOne($post["code"])) {
                throw new LeadException("Лид уже активен", 403);
            }
            if (!$lead = LcrmLead::instance()->getOne($post["code"], false)) {
                throw new LeadException("Лид не найден или уже отправлен", 404);
            }
            LcrmLead::instance()->recover($lead["id"]);
            Log::info(
                [
                    "text"    => "Лид восстановлен с id=".$lead["id"],
                    "user_id" => Yii::$app->user->getId(),
                    "lcrm_id" => $lead["lcrm_id"],
                ],
                "call.lead.recover"
            );

            return $this->getClassAppResponse()::get(["success" => true]);
        } catch (LeadException $e) {
            Log::error(
                ["text" => $e->getMessage(), "user_id" => Yii::$app->user->getId()],
                "call.lead.recover"
            );

            return $this->getClassAppResponse()::get([], [], [], $e->getMessage(), $e->getCode());
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class'        => \yii\filters\AccessControl::className(),
                'rules'        => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                            'update',
                            'index',
                            'by-code',
                            'new',
                            'delete',
                            'recover',
                        ],
                        'roles'   => ['role_call'],
                    ],
                ],
                'denyCallback' => function () {
                    if (\Yii::$app->user->isGuest) {
                        throw new \yii\web\HttpException(401, 'Вы не аутентифицированы.');
                    } else {
                        throw new \yii\web\HttpException(403, 'У вас нет прав на выполнение операции.');
                    }
                },
            ],
        ];
    }
}


