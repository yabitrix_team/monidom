<?php
namespace app\modules\mp\v2\models;

use app\modules\app\v1\models\File;
use app\modules\app\v1\models\RequestFile;
use app\modules\mp\v1\models\Requests as RequestApp;
use app\modules\notification\models\Message;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends RequestApp
{
    /**
     * Метод провеки доступа к отправке на верификацию заявки
     *
     * @param      $code     - код заявки
     * @param bool $checkPEP -флаг указывающий на необходимость проверки ПЭП в заявке
     *
     * @return bool - вернет истину при соблюдении условий для отправки на верификацию иначе ложь
     * @throws \Exception
     */
    public function accessToVerification($code, bool $checkPEP = true)
    {
        return parent::accessToVerification($code, $checkPEP);
    }

    /**
     * Метод получения файлов заявки
     *
     * @param       $id             - идентификатор заявки
     * @param array $arFields       - массив полей файла, которые необходимо вывести, если указать пустым,то выведет
     *                              все
     *                              "id": "1",
     *                              "code": "b49c35bab3c681dfcc8d2f3b254dd3a4",
     *                              "name": "joxi_screenshot_1521802657349.png",
     *                              "path": "/path/1-joxi_screenshot_1521802657349.png",
     *                              "height": "949",
     *                              "width": "1920",
     *                              "type": "image/png",
     *                              "size": "2092708",
     *                              "description": "",
     *                              "sim_link": "fd3a75d95785d147d99c36286cfb9b5e",
     *                              "created_at": "2018-04-05T00:00:00.000Z",
     *                              "updated_at": "2018-04-05T00:00:00.000Z",
     *                              "active": "1",
     *                              "sort": "500",
     *                              "url":  "" - доп. параметр, в таблице файлов нет, если файл картинка, то выведет
     *                              ссылку на превью, если документ, то специально сформированную ссылку для документов
     * @param array $arFileBind     - файлы, которые выводим в заявке, по умолчанию выводит все
     *                              допустимые значения ['foto_sts','foto_pts','foto_auto','foto_passport', 'foto_card'
     *                              'foto_client','foto_extra','doc_pack_1','doc_pack_2', 'foto_extra',
     *                              'foto_notification']
     *
     * @return array - веренет массив данных по файлам либо строку с исключением
     *                      "foto_pts": [
     *                              {
     *                              "code": "b49c35bab3c681dfcc8d2f3b254dd3a4",
     *                              ...
     *                              },
     *                      ]
     * @throws \Exception
     */
    public function getFiles($id, array $arFields = [], array $arFileBind = [])
    {
        $files = (new File())->getByRequestId($id);
        if (is_string($files)) {
            throw new \Exception($files);
        }
        $flipFields = !empty($arFields) ? array_flip($arFields) : $arFields;
        $result     = [];
        $arFileIdNt = [];
        foreach ($files as $file) {
            $fileBind = $file['file_bind'];
            $fileId   = $file['id'];
            unset($file['file_bind']);
            if (!empty($arFileBind) && !in_array($fileBind, $arFileBind)) {
                continue;
            }
            $file = !empty($flipFields) ? array_intersect_key($file, $flipFields) : $file;
            if ($fileBind == RequestFile::BIND_FOTO_EXTRA && key_exists('active', $flipFields)) {
                $file['sent'] = boolval($file['active']);
            }
            if ($fileBind == RequestFile::BIND_FOTO_NOTIFICATION) {
                !isset($file['active']) ?: $file['sent'] = boolval($file['active']);
                $file['file_id'] = $fileId;
                array_push($arFileIdNt, $fileId);
            }
            unset($file['active']);
            $result[$fileBind][] = $file;
        }
        if (!empty($arFileIdNt)) {
            $relation = Message::instance()->getRelationFileIdWithCode($arFileIdNt);
            array_walk(
                $result[RequestFile::BIND_FOTO_NOTIFICATION],
                function (&$v) use ($relation) {
                    $v['nt_code'] = key_exists($v['file_id'], $relation) ? $relation[$v['file_id']] : null;
                    unset($v['file_id']);
                }
            );
        }

        return $result;
    }
}