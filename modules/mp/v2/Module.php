<?php
namespace app\modules\mp\v2;

use app\modules\mp\v1\Module as ModuleV1;
use Yii;

/**
 * Class Module - модуль для работы с Мобильным приложением версии V2
 *
 * @package app\modules\mp\v2
 */
class Module extends ModuleV1
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\mp\v2\controllers';
}