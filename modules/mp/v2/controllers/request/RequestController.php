<?php
namespace app\modules\mp\v2\controllers\request;

use app\modules\mp\v1\controllers\request\RequestController as RequestControllerApp;
use app\modules\mp\v2\classes\traits\TraitConfigController;

/**
 * Class RequestController - контроллер для работы с заявкой
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends RequestControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив файлов, которые выводим вместе с заявкой
     */
    protected $arFileBind = [
        'foto_passport',
        'foto_pts',
        'foto_sts',
        'foto_auto',
        'foto_client',
        'foto_extra',
        'foto_notification',
    ];
    /**
     * @var array - массив полей файлов, выводимые в заявке
     */
    protected $arFileFields = [
        'code',
        'active',
    ];

    /**
     * Инициализация контроллера
     */
    public function init()
    {
        parent::init();
        $this->accessFields = array_merge($this->accessFields, ['foto_extra', 'foto_notification']);
    }

    /**
     * Метод подготовки контракта для МП
     *
     * @param array $item - массив содержащий данные по одной заявке
     *
     * @throws \Exception
     */
    protected function prepareResult(array &$item)
    {
        parent::prepareResult($item);
        unset($item['blocked']);
        $item['blocked_by'] = empty($this->blocked) ? null : $this->blocked;
    }
}