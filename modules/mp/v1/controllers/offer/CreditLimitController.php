<?php
namespace app\modules\mp\v1\controllers\offer;

use app\modules\app\v1\controllers\offer\CreditLimitController as CreditLimitControllerApp;
use app\modules\app\v1\classes\traits\TraitConfigController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use Yii;

/**
 * Class CreditLimits - контроллер взаимодействия с кредитными лимитам
 *
 * @package app\modules\mp\v1\controllers
 */
class CreditLimitController extends CreditLimitControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors(): array
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }
}