<?php
namespace app\modules\mp\v1\controllers\request;

use app\modules\app\v1\controllers\request\FileController as FileControllerApp;
use app\modules\app\v1\models\Leads;
use app\modules\app\v1\models\RequestFile;
use app\modules\app\v1\models\RequestMP;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\mp\v1\models\Requests;
use app\modules\mp\v1\models\Users;
use app\modules\notification\models\MessageAttacheFile;
use app\modules\notification\models\MessageMessage;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use app\modules\mp\v1\models\MessageReceiver;
use app\modules\notification\models\MessageStatus;

/**
 * Class FileController - контроллер для работы с файлами
 *
 * @package app\modules\app\v1\controllers
 */
class FileController extends FileControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен добавления файлов
     *
     * @return array - вернет массив содержащий код заявки или ошибку
     * @return array
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $code                 = Yii::$app->request->post("code");
        $bind                 = Yii::$app->request->post("bind");
        $bindParam            = Yii::$app->request->post("bind_param");
        $this->prefixFileName = Yii::$app->request->post("prefix");
        //проверка заявки на блокироку с пробросом исключения
        (new RequestMP())->checkBlocking($code);
        //при добавлении файла по увдеомлениям, проверяем что нет ответа на уведомление
        if (!empty($bindParam) && $bind == RequestFile::BIND_FOTO_NOTIFICATION) {
            $statusMsg = (new MessageReceiver())->getStatusByMessageAndUserId(
                $bindParam,
                Yii::$app->user->id,
                false
            );
            if (empty($statusMsg) || MessageStatus::ANSWERED == $statusMsg) {
                $msg = empty($statusMsg)
                    ? 'В системе не найдено уведомление для привязки файлов, обратитесь к администратору'
                    : 'Не удается загрузить файл для текущего уведомления, ответ на него уже получен';
                throw new \Exception($msg);
            }
        }
        //вызов родительского экшена с последующим расширением функционала
        $result = parent::actionCreate();
        if (!empty($result['data']) && !empty($this->request)) {
            //связь файлов с уведомлением
            if (!empty($bindParam) && $bind == RequestFile::BIND_FOTO_NOTIFICATION) {
                $relation = (new MessageMessage())->getByParentMessage($bindParam, ['child_message_id'], false);
                if (empty($relation)) {
                    throw new \Exception(
                        'Не удалось найти связь уведомлений для привязки файла, обратитесь к администратору'
                    );
                }
                $modelMAF = new MessageAttacheFile(
                    [
                        'message_id' => $relation['child_message_id'],
                        'file_id'    => $this->lastInsertIdFile,
                    ]
                );
                $modelMAF->create();
            }
            //обновление точки и подписанта при добавлении файлов для не заблокированных заявок
            (new Requests())->updateByCode($this->request['code']);
            // отправка события "Начало прикрепления фото в МП" в CRM при добавлении первого файла
            if (
                !empty($this->request['guid'])
                && !empty($this->request['client_id'])
                && ($login = (new Users())->getOne($this->request['client_id'], ['username']))
                && (new Leads())->checkExists($login, $this->request['guid'])
                && count((new RequestFile())->getByRequestId($this->request['id'])) == 1
            ) {
                EventsLoad::sendEventLoadBit(710, $this->request['guid'], $login);
                (new Leads)->deleteByPhone($login);
            }
        }

        return $result;
    }

    /**
     * Метод экшен получения файла по коду файла
     *
     * @param $code - код файла
     *
     * @return array|string - вернет объект Response для вывода файла в бинарном виде или строку с иключением
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        $request = (new Requests())->getByCodeFile($code);
        if (empty($request) || is_string($request)) {
            $msg = empty($request)
                ? 'Произошла ошибка при попытке получения фотографии, некорректно указан код файла'
                : 'Произошла ошибка при попытке получения фотографии, не удалось проверить принадлежность фотографии к заявке текущего пользователя, обратитесь к администратору';
            throw new \Exception($msg);
        }
        $result = Yii::$app->file->get($code, false);
        if (is_string($result)) {
            throw new \Exception($result);
        }

        //выводим файл в бинарном виде
        return $result;
    }

    /**
     * Метод экшен получения превью фотографии по коду файла
     *
     * @param $code - код файла
     *
     * @return array|string - вернет объект Response для вывода файла в бинарном виде или строку с иключением
     * @throws \Exception
     */
    public function actionPreviewByCode($code)
    {
        $request = (new Requests())->getByCodeFile($code);
        if (empty($request) || is_string($request)) {
            $msg = empty($request)
                ? 'Произошла ошибка при попытке получения фотографии, некорректно указан код файла'
                : 'Произошла ошибка при попытке получения фотографии, не удалось проверить принадлежность фотографии к заявке текущего пользователя, обратитесь к администратору';
            throw new \Exception($msg);
        }
        $result = Yii::$app->file->getPreview($code, false);
        if (is_string($result)) {
            throw new \Exception($result);
        }

        //выводим файл в бинарном виде
        return $result;
    }

    /**
     * Метод экшен удаления файла и превью по коду файла
     *
     * @param $code - код файла
     *
     * @return array - вернет массив содержащий результат удаления или ошибку
     * @throws \Exception
     */
    public function actionDelete($code)
    {
        $request = (new Requests())->getByCodeFile($code);
        if (empty($request) || is_string($request)) {
            $msg = empty($request)
                ? 'Произошла ошибка при попытке удаления фотографии, некорректно указан код файла'
                : 'Произошла ошибка при попытке удаления фотографии, не удалось проверить принадлежность фотографии к заявке текущего пользователя, обратитесь к администратору';
            throw new \Exception($msg);
        }
        //проверка заявки на блокироку с пробросом исключения
        (new RequestMP())->checkBlocking($request['id']);

        return parent::actionDelete($code);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }
}
