<?php
namespace app\modules\mp\v1\controllers\request;

use app\modules\app\v1\controllers\request\RequestController as RequestControllerApp;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\app\v1\models\RequestsEvents;
use app\modules\app\v1\models\RequestPep;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\RequestMP;
use app\modules\mp\v1\models\Requests;
use app\modules\app\v1\models\Events;
use app\modules\app\v1\models\Leads;
use yii\filters\auth\HttpBearerAuth;
use app\modules\mp\v1\models\Users;
use yii\filters\auth\CompositeAuth;
use yii\web\JsonParser;
use yii\db\Exception;
use Yii;

/**
 * Class RequestController - контроллер для работы с заявкой
 *
 * @package app\modules\app\v1\controllers
 */
class RequestController extends RequestControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив полей разрешенных для показа в мп
     *            во избежание нарушения контракта
     */
    protected $accessFields = [
        'code',
        'num_1c',
        'created_at',
        'summ',
        'date_return',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'method_of_issuance_id',
        'card_number',
        'card_token',
        'client_last_name',
        'client_first_name',
        'client_patronymic',
        'client_birthday',
        'client_passport_serial_number',
        'client_passport_number',
        'client_region_id',
        'client_email',
        'client_workplace_name',
        'client_workplace_experience',
        'client_workplace_address',
        'client_workplace_phone',
        'client_total_monthly_income',
        'client_total_monthly_outcome',
        'client_guarantor_name',
        'client_guarantor_relation_id',
        'client_guarantor_phone',
        'client_mobile_phone',
        'client_employment_id',
        'comment_client',
        'credit_product_id',
        'credit_limit_code',
        'place_of_stay',
        'is_pep',
        'foto_passport',
        'foto_pts',
        'foto_sts',
        'foto_auto',
        'foto_client',
    ];
    /**
     * @var array - массив полей файлов, выводимые в заявке
     */
    protected $arFileFields = [
        'code',
    ];
    /**
     * @var array - список статусов для МП, заведено аналогично поля identifier таблицы status_mp
     */
    protected $statusesMP = [
        'created'         => 'CREATED',
        'preApprovedSent' => 'PREAPPROVED_SENT',
        'preApproved'     => 'PREAPPROVED',
        'approvedSent'    => 'APPROVED_SENT',
        'approved'        => 'APPROVED',
        'denied'          => 'DENIED',
    ];
    /**
     * @var int - код события для отправки заявки на предварительную верификацию
     */
    protected $eventPreApproved = 701;
    /**
     * @var array - коды событий для отправки заявки на финальную верификацию
     */
    protected $eventsApproved = [702, 711, 712];
    /**
     * @var string - способ выдачи другими способами,не входящими
     *              в массив ассоциаций assocMethodOfIssuance в параметрах
     */
    protected $methodOfIssuanceOther = 'other';
    /**
     * @var - источник блокировки заявки: client | partner
     */
    protected $blocked;

    /**
     * Экшен для создания заявки
     *
     * @return array|bool|string
     * @throws \Exception
     */
    public function actionCreate()
    {
        $result = parent::actionCreate();
        if (!empty($result['data']) && !empty($this->request['id'])) {
            //получение lcrm_id с последующим добавлением в заявку
            $lcrmId = Yii::$app->lcrmLKK->leadsRegister(
                Yii::$app->user->identity->username,
                $this->request['id'],
                Yii::$app->user->id,
                $result['data']['code'], //согласовано c Перовым
                Yii::$app->user->identity->crib_client_id,
                Yii::$app->user->identity->created_at
            );
            if (!empty($lcrmId)) {
                (new Requests())->updateOne($this->request['id'], ['lcrm_id' => $lcrmId]);
            }
        }

        return $result;
    }

    /**
     * Экшен для обновления заявки
     *
     * @param $code - Код(хэш) заявки
     *
     * @return array
     * @throws \Exception
     */
    public function actionUpdate($code)
    {
        try {
            $result = parent::actionUpdate($code);
            if (!empty($result['data']) && is_array($result['data'])) {
                $result['data']['num_1c'] = $this->request['num_1c'] ?? null;
                $reqBody                  = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
                //добавлении информации о МП
                if (!empty($reqBody['service_info'])) {
                    $model = new RequestMP();
                    $model->load(
                        [
                            'request_id'   => $this->request['id'],
                            'service_info' => $reqBody['service_info'],
                        ],
                        ''
                    );
                    $saveRequestMP = $model->save();
                    if (is_a($saveRequestMP, \Exception::class) || is_a($saveRequestMP, RequestMP::class)) {
                        throw new \Exception(
                            'При попытке обновления заявки не удалось обновить информацию мобильного приложения, обратитесь к администратору'
                        );
                    }
                }
                //приведение формата даты относительно валидатора модели
                $model = new Requests();
                $model->load(['client_birthday' => $reqBody['client_birthday']], '');
                $model->validate('client_birthday');
                //сброс ПЭП при несоответствии определенных данных
                if (
                    (
                        !empty($reqBody['client_passport_serial_number'])
                        && !empty($this->request['client_passport_serial_number'])
                        && $reqBody['client_passport_serial_number'] != $this->request['client_passport_serial_number']
                    )
                    || (
                        !empty($reqBody['client_passport_number'])
                        && !empty($this->request['client_passport_number'])
                        && $reqBody['client_passport_number'] != $this->request['client_passport_number']
                    )
                    || (
                        !empty($reqBody['client_birthday'])
                        && !empty($this->request['client_birthday'])
                        && $model->client_birthday != $this->request['client_birthday']
                    )
                    || (
                        !empty($reqBody['client_first_name'])
                        && !empty($this->request['client_first_name'])
                        && $reqBody['client_first_name'] != $this->request['client_first_name']
                    )
                    || (
                        !empty($reqBody['client_last_name'])
                        && !empty($this->request['client_last_name'])
                        && $reqBody['client_last_name'] != $this->request['client_last_name']
                    )
                    || (
                        !empty($reqBody['client_patronymic'])
                        && !empty($this->request['client_patronymic'])
                        && $reqBody['client_patronymic'] != $this->request['client_patronymic']
                    )
                    || (
                        !empty($reqBody['client_mobile_phone'])
                        && !empty($this->request['client_mobile_phone'])
                        && $reqBody['client_mobile_phone'] != $this->request['client_mobile_phone']
                    )
                ) {
                    $updateRequest = (new Requests())->updateByCode($code, ['is_pep' => 0]);
                    if (is_a($updateRequest, \Exception::class)) {
                        throw new \Exception('Произошла ошибка при обновлении заявки, обратитесь к администратору');
                    }
                    if (!empty($this->request['id'])) {
                        (new RequestPep())->deleteByRequestId($this->request['id']);
                    }
                }
            }

            return $result;
        } catch (Exception $e) {
            throw new \Exception(
                'При обращении к сервису обновления заявки произошла ошибка в работе с базой данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Экшен получения всех заявок
     *
     * @return array - Вернет сформированный массив
     * @throws \Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data'])) {
            //подгатавливаем массив для вычесления пересечения масивов
            $flipAccessFields = array_flip($this->accessFields);
            foreach ($result['data'] as &$item) {
                //получаем тоько те поля, которые заданы в $this->accessFields
                $item = array_intersect_key($item, $flipAccessFields);
                $this->prepareResult($item);
            }
        }

        return $result;
    }

    /**
     * Экшен получения заявки по коду
     *
     * @param $code - код заявки
     *
     * @return array|bool|false|string
     * @throws \Exception
     */
    public function actionByCode($code)
    {
        $result = parent::actionByCode($code);
        if (!empty($result['data'])) {
            //подгатавливаем массив для вычесления пересечения масивов
            $flipAccessFields = array_flip($this->accessFields);
            //получаем тоько те поля, которые заданы в $this->accessFields
            $result['data'] = array_intersect_key($result['data'], $flipAccessFields);
            $this->prepareResult($result['data']);
        }

        return $result;
    }

    /**
     * Экшен верификации заявки
     * присваивает определенные статусы мп и события во время заполнения заявки
     *
     * @param $code - код заявки
     *
     * @return array - результирующий массив
     * @throws \Exception
     */
    public function actionVerification($code)
    {
        //проверка блокировки с пробросом исключения
        (new RequestMP())->checkBlocking($code);
        $modelRequestClass = $this->getModelRequest();
        (new $modelRequestClass)->accessToVerification($code);
        $modelStatusMPJournal = new StatusesMPJournal();
        $curStatusMP          = $modelStatusMPJournal->lastStatusByRequestCode($code);
        $request              = (new Requests())->getOne(
            $curStatusMP['request_id'],
            ['guid', 'client_id', 'card_token', 'method_of_issuance_id']
        );
        if (empty($curStatusMP)) {
            throw new \RuntimeException(
                'Не удалось отправить заявку на верификацию из-за отсутствия статуса для мобильного приложения либо некорректно указанного кода заявка для текущего пользователя, обратитесь к администратору'
            );
        }
        $modelStatusMp       = new StatusesMP();
        $modelRequestsEvents = new RequestsEvents();
        $modelEvents         = new Events();
        switch ($curStatusMP['identifier']) {
            //предварительная верификация
            case $this->statusesMP['created']:
                {
                    $newStatusMP = $modelStatusMp->getByIdentifier($this->statusesMP['preApprovedSent']);
                    if (empty($newStatusMP) || is_string($newStatusMP)) {
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию произошла ошибка получения идентификатора статуса заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    $modelStatusMPJournal->load(
                        [
                            'request_id'   => $curStatusMP['request_id'],
                            'status_mp_id' => $newStatusMP['id'],
                        ],
                        ''
                    );
                    if (!$modelStatusMPJournal->validate()) {
                        //todo[echmaster]: 07.05.2018 Добавить лог валидации данных при добавлении статусов для мп
                        $modelStatusMPJournal->firstErrors;
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию не удалось записать статус заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    $idRecordSMPJ = $modelStatusMPJournal->create();
                    if (!is_numeric($idRecordSMPJ)) {
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию произошла ошибка записи статуса заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    //обновляем точку и подписанта на предварительной верификации
                    $resUpdate = (new Requests())->updateByCode($code);
                    if (is_a($resUpdate, \Exception::class)) {
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw $resUpdate;
                    }
                    //добавляем событие 701 к заявке
                    if (!$event = $modelEvents->getByCode($this->eventPreApproved)) {
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию в системе был некорректно указан код события для заявки, обратитесь к администратору'
                        );
                    }
                    $modelRequestsEvents->load(
                        [
                            'request_id' => $curStatusMP['request_id'],
                            'event_id'   => $event['id'],
                        ],
                        ''
                    );
                    if (!$modelRequestsEvents->validate()) {
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        //todo[echmaster]: 07.05.2018 Добавить лог валидации данных при добавлении событий заявки
                        $modelRequestsEvents->firstErrors;
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию не удалось прикрепить события к заявке'
                        );
                    }
                    $result = $modelRequestsEvents->create();
                    if (!is_numeric($result)) {
                        //удаляем добавленный статус для мп если событие не прикрепилось
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw new \RuntimeException(
                            'При отправке заявки на предварительную верификацию произошла ошибка прикрепления события к заявке'
                        );
                    }
                    // отправка события "Предварительная верификация в МП" в CRM
                    if (
                        !empty($request)
                        && !empty($request['guid'])
                        && !empty($request['client_id'])
                        && ($login = (new Users())->getOne($request['client_id'], ['username']))
                        && (new Leads())->checkExists($login, $request['guid'])
                    ) {
                        EventsLoad::sendEventLoadBit(610, $request['guid'], $login);
                    }
                    break;
                }
            // финальная верификация
            case $this->statusesMP['preApproved']:
                {
                    if (
                        ($assocMethodOfIssuance = Yii::$app->params['mp']['settings']['assocMethodOfIssuance'])
                        && is_array($assocMethodOfIssuance)
                        && empty($request['card_token'])
                        && (int) $assocMethodOfIssuance['wo_card_confirm'] === (int) $request['method_of_issuance_id']
                    ) {
                        throw new \RuntimeException(
                            'Не удалось отправить заявку на верификацию, так как карта клиента не подтверждена'
                        );
                    }
                    //устанавливаем статус для МП отправлено на финальную верификацию
                    $newStatusMP = $modelStatusMp->getByIdentifier($this->statusesMP['approvedSent']);
                    if (empty($newStatusMP)) {
                        throw new \RuntimeException(
                            'При отправке заявки на финальную верификацию произошла ошибка получения идентификатора статуса заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    $modelStatusMPJournal->load(
                        [
                            'request_id'   => $curStatusMP['request_id'],
                            'status_mp_id' => $newStatusMP['id'],
                        ],
                        ''
                    );
                    if (!$modelStatusMPJournal->validate()) {
                        //todo[echmaster]: 07.05.2018 Добавить лог валидации данных при добавлении статусов для мп
                        $modelStatusMPJournal->firstErrors;
                        throw new \RuntimeException(
                            'При отправке заявки на на финальную верификацию не удалось записать статус заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    $idRecordSMPJ = $modelStatusMPJournal->create();
                    if (!is_numeric($idRecordSMPJ)) {
                        throw new \RuntimeException(
                            'При отправке заявки на финальную верификацию произошла ошибка записи статуса заявки для мобильного приложения, обратитесь к администратору'
                        );
                    }
                    //обновляем точку и подписанта на предварительной верификации
                    $resUpdate = (new Requests())->updateByCode($code);
                    if (is_a($resUpdate, \Exception::class)) {
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw $resUpdate;
                    }
                    //добавляем события 702, 711, 712 к заявке
                    if (
                        !($event = $modelEvents->getByCode($this->eventsApproved))
                        || count($event) !== count($this->eventsApproved)
                    ) {
                        //удаляем добавленный статус для мп если одно из событий не существует
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw new \RuntimeException(
                            'При отправке заявки на финальную верификацию в системе был некорректно указан один из кодов событий для заявки, обратитесь к администратору'
                        );
                    }
                    //вставляем события для финальной верификации заявки
                    $arEventId = array_column($event, 'id');
                    $res       = $modelRequestsEvents->batchInsert($curStatusMP['request_id'], $arEventId);
                    if (empty($res) || is_string($res)) {
                        //удаляем добавленный статус для мп если не удалось установить события для заявки
                        $modelStatusMPJournal->deleteOne($idRecordSMPJ);
                        throw new \RuntimeException(
                            'При отправке заявки на финальную верификацию не удалось установить события заявки, обратитесь к администратору'
                        );
                    }
                    break;
                }
            default:
                throw new \RuntimeException(
                    'Попытка верификации заявки при не соответствующем статусе заявки для мобильного приложения'
                );
        }

        return $this->getClassAppResponse()::get(['verification' => true]);
    }

    /**
     * Метод подготовки контракта для МП
     *
     * @param array $item - массив содержащий данные по одной заявке
     *
     * @throws \Exception
     */
    protected function prepareResult(array &$item)
    {
        $assocMethodOfIssuance = Yii::$app->params['mp']['settings']['assocMethodOfIssuance'];
        if (empty($assocMethodOfIssuance) || !is_array($assocMethodOfIssuance)) {
            throw new \RuntimeException(
                'Не указаны или указаны некорректно ассоциации способов выдачи для мобильного приложения, обратитесь к администратору'
            );
        }
        //преобраздование данных
        empty($item['created_at']) ?: $item['created_at'] = (new \DateTime($item['created_at']))->format('d.m.Y');
        empty($item['date_return']) ?: $item['date_return'] = (new \DateTime($item['date_return']))->format('d.m.Y');
        empty($item['client_birthday'])
            ?: $item['client_birthday'] = (new \DateTime($item['client_birthday']))->format('d.m.Y');
        !isset($item['is_pep']) ?: $item['is_pep'] = !empty($item['is_pep']);
        //подготавливаем способ выдачи для МП
        if (isset($item['method_of_issuance_id'])) {
            $arFlip                     = array_flip($assocMethodOfIssuance);
            $item['method_of_issuance'] = array_key_exists($item['method_of_issuance_id'], $arFlip)
                ? $arFlip[$item['method_of_issuance_id']]
                : $arFlip[$assocMethodOfIssuance[$this->methodOfIssuanceOther]];
            unset($item['method_of_issuance_id']);
        }
        //преобразование значений numeric в int
        !isset($item['summ']) ?: $item['summ'] = (int) $item['summ'];
        !isset($item['client_workplace_experience'])
            ?: $item['client_workplace_experience'] = (int) $item['client_workplace_experience'];
        !isset($item['client_total_monthly_income'])
            ?: $item['client_total_monthly_income'] = (int) $item['client_total_monthly_income'];
        !isset($item['client_total_monthly_outcome'])
            ?: $item['client_total_monthly_outcome'] = (int) $item['client_total_monthly_outcome'];
        // добавляем в вывод статус для МП
        $modelStatusMPJournal = new StatusesMPJournal();
        $curStatusMP          = $modelStatusMPJournal->lastStatusByRequestCode($item['code']);
        $item['status_mp']    = $curStatusMP['identifier'];
        //параметр блокировки заявки
        $this->blocked   = (new RequestMP())->getBlocking($item['code']);
        $item['blocked'] = !empty($this->blocked) && $this->blocked == RequestMP::BLOCKED_BY_CLIENT ? true : false;
        //подтверждение карты W1 по токену
        $item['card_confirm'] = (bool) $item['card_token'];
        unset($item['card_token']);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }
}