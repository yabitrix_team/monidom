<?php
namespace app\modules\mp\v1\controllers\admin\dev;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\mp\v1\models\dev\UserForm;
use app\modules\app\v1\models\AuthToken;
use yii\web\ForbiddenHttpException;
use yii\rest\Controller;
use yii\web\JsonParser;
use Yii;

/**
 * Class UserController - контроллер административного раздела
 *                        для работы с пользователем в режиме дев
 *
 * @package app\modules\mp\v1\controllers\admin
 */
class UserController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод инициализации контроллера
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function init()
    {
        parent::init();
        if (!YII_ENV_DEV) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        AuthToken::checkAccessKey();
    }

    /**
     * Экшен сброса пользователя
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionReset()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelUF = new UserForm();
        $modelUF->load($reqBody, '');

        return $this->getClassAppResponse()::get(['reset_user' => $modelUF->resetUsername()]);
    }
}