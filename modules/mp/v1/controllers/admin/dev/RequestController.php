<?php
namespace app\modules\mp\v1\controllers\admin\dev;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\AuthToken;
use app\modules\mp\v1\models\dev\RequestForm;
use yii\web\ForbiddenHttpException;
use yii\rest\Controller;
use yii\web\JsonParser;
use Yii;

/**
 * Class RequestController - контроллер административного раздела
 *                          для работы с заявкой только в режиме дев
 *
 * @package app\modules\mp\v1\controllers\admin
 */
class RequestController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод инициализации контроллера
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function init()
    {
        parent::init();
        if (!YII_ENV_DEV) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        AuthToken::checkAccessKey();
    }

    /**
     * Экшен получения активных и деактивных заявок по коду
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionIndex()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelRF = new RequestForm(['scenario' => RequestForm::SCENARIO_GET]);
        $modelRF->load($reqBody, '');

        return $this->getClassAppResponse()::get($modelRF->get());
    }

    /**
     * Экшен деактивации заявок по коду(кодам) или по логину
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDeactivate()
    {
        $reqBody           = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $reqBody['active'] = 0;
        $modelRF           = new RequestForm(['scenario' => RequestForm::SCENARIO_ACTIVATION]);
        $modelRF->load($reqBody, '');

        return $this->getClassAppResponse()::get(['deactivated' => $modelRF->activation()]);
    }

    /**
     * Экшен активации заявок по коду(кодам) или по логину
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionActivate()
    {
        $reqBody           = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $reqBody['active'] = 1;
        $modelRF           = new RequestForm(['scenario' => RequestForm::SCENARIO_ACTIVATION]);
        $modelRF->load($reqBody, '');

        return $this->getClassAppResponse()::get(['activated' => $modelRF->activation()]);
    }

    /**
     * Экшен блокировки заявки по коду(кодам)
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBlocking()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelRF = new RequestForm(['scenario' => RequestForm::SCENARIO_BLOCK]);
        $modelRF->load($reqBody, '');

        return $this->getClassAppResponse()::get(['set_block' => $modelRF->block()]);
    }

    /**
     * Экшен добавления договор к заявке
     *
     * @return array
     * @throws \Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionAddAgreement()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelRF = new RequestForm(['scenario' => RequestForm::SCENARIO_ADD_AGREEMENT]);
        $modelRF->load($reqBody, '');

        return $this->getClassAppResponse()::get(['added' => $modelRF->addAgreement()]);
    }
}