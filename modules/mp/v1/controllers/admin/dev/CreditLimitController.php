<?php
namespace app\modules\mp\v1\controllers\admin\dev;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\CreditLimit;
use app\modules\app\v1\models\AuthToken;
use yii\web\ForbiddenHttpException;
use yii\rest\Controller;
use yii\web\JsonParser;
use Yii;

/**
 * Class CreditLimitController - контроллер административного раздела
 *                        для работы с кредитными лимитами в режиме дев
 *
 * @package app\modules\mp\v1\controllers\admin
 */
class CreditLimitController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var - тип предложения для МП
     */
    protected $offerType;

    /**
     * Метод инициализации контроллера
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function init()
    {
        parent::init();
        if (!YII_ENV_DEV) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }
        AuthToken::checkAccessKey();
        if (!$this->offerType = Yii::$app->params['mp']['settings']['creditLimitsOfferType']) {
            throw new \RuntimeException(
                'В параметрах не указан тип предложения для кредитного лимита, обратитесь к администратору'
            );
        }
    }

    /**
     * Экшен сохранения кредитного лимита
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSave(): array
    {
        $reqBody               = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $reqBody['offer_type'] = $this->offerType;
        $modelCL               = new CreditLimit();
        $modelCL->load($reqBody, '');

        return $this->getClassAppResponse()::get(['saved' => (bool) $modelCL->save()]);
    }

    /**
     * Экшен удаления кредитного лимита
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionDelete(): array
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelCL = new CreditLimit();
        $modelCL->load($reqBody, '');
        if (!$modelCL->validate('phone')) {
            throw new \RuntimeException(reset($modelCL->firstErrors));
        }
        $result = $modelCL->deleteByPhoneAndOfferType($reqBody['phone'], $this->offerType);

        return $this->getClassAppResponse()::get(['deleted' => $result]);
    }
}