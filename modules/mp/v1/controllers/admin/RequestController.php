<?php
namespace app\modules\mp\v1\controllers\admin;

use app\modules\app\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\AuthToken;
use app\modules\app\v1\models\RequestMP;
use yii\rest\Controller;

/**
 * Class RequestController - контроллер административного раздела для работы с заявкой
 *
 * @package app\modules\mp\v1\controllers\admin
 */
class RequestController extends Controller
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод инициализации контроллера
     */
    public function init()
    {
        parent::init();
        AuthToken::checkAccessKey();
    }

    /**
     * Экшен получения информации по МФО-шному номеру заявки
     *
     * @param $id - МФО-шный номер заявки (пример: 18092089990001)
     *
     * @return array
     * @throws \Exception
     */
    public function actionById($id)
    {
        $result = (new RequestMP())->getByNum1C($id, ['service_info']);
        if (is_a($result, \Exception::class)) {
            throw new \Exception('Произошла ошибка при получении данных по заявке из БД, обратитесь к администратору');
        }
        if ($result === false) {
            throw new \Exception(
                'Не удалось получить дополнительные данные по заявке, некорректно указан номер заявки'
            );
        }

        return $this->getClassAppResponse()::get(['service_info' => $result]);
    }
}