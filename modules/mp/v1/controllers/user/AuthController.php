<?php

namespace app\modules\mp\v1\controllers\user;

use \app\modules\app\v1\controllers\user\AuthController as AuthControllerApp;
use app\modules\app\v1\models\AuthToken;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\mp\v1\models\Auth;
use app\modules\mp\v1\models\Users;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\JsonParser;

/**
 * Class AuthController - контроллер для работы с авторизацией
 *
 * @package app\modules\app\v1\controllers
 *
 */
class AuthController extends AuthControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Экшен авторизации пользователя с генерацией токена авторизации
     *
     * @return array|mixed|\yii\web\User
     * @throws \Exception
     */
	public function actionCreate() {

        $reqBody = ( new JsonParser() )->parse( Yii::$app->request->getRawBody(), 'json' );
        $model   = new Auth();
        if ( ! $model->load( $reqBody, '' ) ) {
            throw new \Exception( 'При попытке авторизации не были указаны параметры авторизации' );
        }
        $res = $model->signIn();
        if ( is_string( $res ) && strlen( $res ) != 32 ) {
            throw new \Exception( $res );
        }
        if ( is_object( $res ) ) {
            return $this->getClassAppResponse()::get( false, $res->firstErrors );
        }

        return $this->getClassAppResponse()::get( [ 'token' => $res ] );
	}

	/**
	 * Проверка аутинтефикации пользователя
	 *
	 * @return array
	 */
	public function actionIndex() {

        $token = AuthToken::getTokenInHeader();
        $auth  = ! empty( $token ) && ! empty( Users::findIdentityByAccessToken( $token ) ) ? true : false;

        return $this->getClassAppResponse()::get( [ 'authorized' => $auth ] );
	}

    /**
     * Деаутинтефикация пользователя
     *
     * @return array
     * @throws \Exception
     */
	public function actionDelete() {

        $res = ( new AuthToken() )->deleteByToken();
        if ( is_string( $res ) ) {
            throw new \Exception( 'При деавторизации пользователя произошла ошибка удаления токена, обратитесь к администратору' );
        }

        return $this->getClassAppResponse()::get( [ 'unauthorized' => ! empty( $res ) ] );
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		$arConfig                          = parent::behaviors();
		$arConfig['verbFilter']['actions'] = [
			'index'  => [ 'GET', 'OPTIONS' ],
			'create' => [ 'POST', 'OPTIONS' ],
			'delete' => [ 'DELETE', 'OPTIONS' ],
		];
		//добавляем проверку на авторизацию, если метод запроса не OPTIONS, но GET или DELETE
		if (
			! Yii::$app->request->getIsOptions()
			&& Yii::$app->request->getIsDelete()
		) {
			$arConfig['authenticator'] = [
				'class'       => CompositeAuth::class,
				'authMethods' => [ HttpBearerAuth::class ],
			];
		}

		return $arConfig;
	}
}