<?php
namespace app\modules\mp\v1\controllers\user;

use app\modules\app\v1\controllers\user\RegisterController as RegisterControllerApp;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\soap\crm\EventsLoad;
use app\modules\app\v1\models\soap\sms\SendingSMS;
use app\modules\app\v1\models\StatusesMPJournal;
use app\modules\app\v1\models\LeadsConsumer;
use app\modules\app\v1\models\SmsConfirm;
use app\modules\app\v1\models\StatusesMP;
use app\modules\app\v1\models\AuthToken;
use app\modules\app\v1\classes\logs\Log;
use app\modules\app\v1\models\Requests;
use app\modules\mp\v1\models\Register;
use app\modules\app\v1\models\Leads;
use yii\web\JsonParser;
use Yii;

/**
 * Class RegisterController - контроллер для регистрации пользователя через МП
 *
 * @package app\modules\app\v1\controllers
 */
class RegisterController extends RegisterControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен создания пользователя
     *
     * @return $this|array|int|string
     * @throws \Exception
     */
    public function actionCreate()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $model   = new Register();
        if (!$model->load($reqBody, '')) {
            throw new \Exception('Не переданы параметры для регистрации пользователя');
        }
        $userId = $model->signUp();
        if (is_string($userId)) {
            throw new \Exception($userId);
        }
        if (is_object($userId)) {
            return $this->getClassAppResponse()::get(false, $userId->firstErrors);
        }
        //создание преварительной заявки при наличии лида у нового юзера
        $modelLeads = new Leads();
        $lead       = $modelLeads->getByPhone(
            $model->login,
            [
                'guid',
                'code',
                'summ',
                'months',
                'client_first_name',
                'client_last_name',
                'client_patronymic',
                'client_mobile_phone',
                'client_passport_serial_number',
                'client_passport_number',
                'client_birthday',
                'auto_model_guid',
                'auto_year',
                'region',
                'leads_consumer_id',
            ],
            false
        );
        try {
            if (is_a($lead, \Exception::class)) {
                throw $lead;
            }
            if (!empty($lead)) {
                $modelLeadsConsumer = new LeadsConsumer();
                $fields             = ['identifier', 'requests_origin_id'];
                $dataConsumer       = !empty($lead['leads_consumer_id'])
                    ? $modelLeadsConsumer->getOne($lead['leads_consumer_id'], $fields)
                    : $modelLeadsConsumer->getByIdentifier($modelLeads->getConsumerDefault(), $fields);
                if (is_a($dataConsumer, \Exception::class)) {
                    throw $dataConsumer;
                }
                $data = $modelLeads->prepareRequest(
                    $dataConsumer['identifier'],
                    $dataConsumer['requests_origin_id'],
                    $userId,
                    $lead
                );
                if (is_a($data, \Exception::class)) {
                    throw $data;
                }
                $modelRequest = new Requests();
                $requestSave  = $modelRequest->save($data);
                if (is_a($requestSave, \Exception::class)) {
                    throw $requestSave;
                }
                //добавление статуса заявки для МП
                if ((int) $requestSave === 1) {
                    $resSetStatus = (new StatusesMPJournal)->setStatus(
                        Yii::$app->db->lastInsertID,
                        StatusesMP::getStatusCreated()
                    );
                    if (is_a($resSetStatus, \Exception::class)) {
                        return $resSetStatus;
                    }
                }
                // отправка события "Регистрация в МП" в CRM
                if (!empty($lead['guid'])) {
                    EventsLoad::sendEventLoadBit(510, $lead['guid'], $model->login);
                }
            }
        } catch (\Exception $e) {
            Log::error(
                [
                    'user_id'      => $userId,
                    'user_login'   => $model->login,
                    'user_ip'      => $_SERVER["HTTP_X_FORWARDED_FOR"],
                    'user_browser' => Yii::$app->getRequest()->getUserAgent(),
                    'text'         => $e->getMessage(),
                ],
                'user.register.mobile'
            );
        }
        //отправка сообщения с уведомлением о регистрации нового пользователя
        $sendMail = Yii::$app->mailer->compose(
            'newuser',
            [
                'name'     => $model->name,
                'phone'    => '+7 '.$model->login,
                'dateTime' => date('d.m.Y в H:i'),
            ]
        )
            ->setFrom(Yii::$app->params['core']['email']['from'])
            ->setTo(Yii::$app->params['core']['email']['toNewUser'])
            ->setSubject('МП новый клиент')
            ->send();
        if (empty($sendMail)) {
            //todo[echmaster]: 24.04.2018 Добавить логирование
            $msg = 'Произошла ошибка при отправке на почту регистрационных данных нового пользователя';
        }
        //при успешной регистрации авторизуем пользователя и возвращаем его токен
        $model = new AuthToken();
        $model->load(['user_id' => $userId], '');
        $token = $model->save();
        if (
            empty($token)
            || (is_string($token) && strlen($token) != 32)
            || is_object($token)
        ) {
            throw new \Exception(
                'После регистрации не удалось автоматически авторизовать пользователя, попробуйте авторизоваться вручную'
            );
        }

        return $this->getClassAppResponse()::get(['token' => $token]);
    }

    /**
     * Метод экшен проверки пользователя перед регистрацией с отправкой кода подтверждение через смс
     *
     * @param $login - логин пользователя в виде номера телефона
     *
     * @return array - вернет результат отправки кода подтверждения через смс или ошибку
     * @throws \Exception
     */
    public function actionByLogin($login)
    {
        $model        = new Register();
        $model->login = $login;
        if (!$model->validate('login')) {
            return $this->getClassAppResponse()::get(false, $model->firstErrors);
        }
        $modSmsConfirm = new SmsConfirm(['scenario' => SmsConfirm::SCENARIO_CREATE]);
        $modSmsConfirm->load(['login' => $login], '');
        //проверка блокировки отправки смс
        $modSmsConfirm->checkLockByLogin();
        $modSmsConfirm->generateSmsCode();
        $resSaveSmsCode = $modSmsConfirm->save();
        if (!is_numeric($resSaveSmsCode)) {
            throw new \Exception(
                'Перед регистрацией при проверке пользователя произошла ошибка генерации СМС кода, обратитесь к администратору'
            );
        }
        //при POST или GET запросах проверяем наличие hash_code для генерации текста СМС
        $reqBody  = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $hashCode = $reqBody['hash_code'] ?? Yii::$app->request->get('hash_code');
        $smsText  = $modSmsConfirm->getTextSmsConfirm($hashCode);
        $modelSms = new SendingSMS(['scenario' => SendingSMS::SCENARIO_SEND_DATA]);
        $modelSms->load(
            [
                'phone'   => $login,
                'message' => $smsText,
            ],
            ''
        );
        $modelSms->setLogCategory('sms.confirm.mobile')->sendData();

        return $this->getClassAppResponse()::get(
            [
                'send_code' => true,
                'lock_time' => Yii::$app->params['core']['sms']['lock_expired_at'] * 60,
            ]
        );
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig                          = parent::behaviors();
        $arConfig['verbFilter']['actions'] = [
            'create'   => ['POST', 'OPTIONS'],
            'by-login' => ['GET', 'POST', 'OPTIONS'],
        ];

        return $arConfig;
    }
}
