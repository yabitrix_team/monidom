<?php
namespace app\modules\mp\v1\controllers\info;

use app\modules\app\v1\controllers\info\ProductController as ProductControllerApp;
use app\modules\app\v1\models\VersionsDictionaries;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use Yii;

/**
 * Class ProductController - контроллер для работы со справочником Кредитные продукты
 *
 * @package app\modules\app\v1\controllers
 */
class ProductController extends ProductControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var bool - свойство указываеющее, что нужно выбрать все продукты: активные и не активные
     */
    public $active = null;
    /**
     * @var array - массив полей разрешенных для показа в мп
     *            во избежание нарушения контракта
     */
    protected $accessFields = [
        "id",
        "name",
        "percent_per_day",
        "month_num",
        "summ_min",
        "summ_max",
        "age_min",
        "age_max",
        "loan_period_min",
        "loan_period_max",
        "active",
    ];

    /**
     * @return array - вернет подготовленный массив данных для вывода
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data']) && is_array($result['data'])) {
            $flipAccessFields = array_flip($this->accessFields);
            $items            = [];
            foreach ($result['data'] as $v) {
                //ограничение выбора кредитных продуктов, относительно параметра excludeProduct
                $excludeProduct = Yii::$app->params['mp']['settings']['excludeProduct'];
                if (!empty($excludeProduct) && in_array($v['id'], $excludeProduct)) {
                    continue;
                }
                $item = array_intersect_key($v, $flipAccessFields);
                //преобразование значений numeric в int
                array_walk(
                    $item,
                    function (&$v, $k) {
                        $v = !in_array($k, ['id', 'percent_per_day', 'active']) && is_numeric($v)
                            ? intval($v)
                            : ($k == 'active' ? !empty($v) : ($k == 'percent_per_day' ? doubleval($v) : $v));
                    }
                );
                $items[] = $item;
            }
            //получение версии для справочника, с учетом getIsNeedVersion
            $controllerId   = end(explode('/', $this->id));
            $result['data'] = [
                'version' => !empty($this->getIsNeedVersion()) ?
                    (new VersionsDictionaries())->getByDictionary($controllerId) : 0,
                'items'   => $items,
            ];
            unset($result['version']);
        }

        return $result;
    }
}
