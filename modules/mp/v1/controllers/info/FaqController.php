<?php
namespace app\modules\mp\v1\controllers\info;

use app\modules\app\v1\controllers\info\FaqController as FaqControllerApp;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\VersionsDictionaries;
use app\modules\app\v1\models\Pages;

/**
 * Class FaqController - контроллер для получения статического контента F.A.Q.
 *
 * @package app\modules\mp\v1\controllers\info
 */
class FaqController extends FaqControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        // Получаем идентификатор контроллера about-company из info/about-company
        $controllerId    = end(explode('/', $this->id));
        $data            = (new Pages())->getPage($controllerId, $this->getConsumer());
        $data['text']    = htmlspecialchars_decode($data['text']);
        $data['version'] = !empty($this->getIsNeedVersion())
            ? (new VersionsDictionaries())->getByDictionary($controllerId) : 0;

        return $this->getClassAppResponse()::get($data);
    }
}