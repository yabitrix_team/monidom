<?php
namespace app\modules\mp\v1\controllers\info;

use app\modules\app\v1\controllers\info\EmploymentController as EmploymentControllerApp;
use app\modules\app\v1\models\VersionsDictionaries;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class EmploymentController - контроллер для работы со справочником Занятость
 *
 * @package app\modules\app\v1\controllers
 */
class EmploymentController extends EmploymentControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - массив полей разрешенных для показа в мп
     *            во избежание нарушения контракта
     */
    protected $accessFields = [
        "id",
        "name",
    ];

    /**
     * @return array - вернет подготовленный массив данных для вывода
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data']) && is_array($result['data'])) {
            $flipAccessFields = array_flip($this->accessFields);
            $items            = [];
            foreach ($result['data'] as $v) {
                $items[] = array_intersect_key($v, $flipAccessFields);
            }
            //получение версии для справочника, с учетом getIsNeedVersion
            $controllerId   = end(explode('/', $this->id));
            $result['data'] = [
                'version' => !empty($this->getIsNeedVersion()) ?
                    (new VersionsDictionaries())->getByDictionary($controllerId) : 0,
                'items'   => $items,
            ];
            unset($result['version']);
        }

        return $result;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }
}
