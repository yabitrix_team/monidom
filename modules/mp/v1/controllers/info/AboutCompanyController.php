<?php
namespace app\modules\mp\v1\controllers\info;

use app\modules\app\v1\controllers\info\AboutCompanyController as AboutCompanyControllerApp;
use app\modules\app\v1\models\VersionsDictionaries;
use app\modules\mp\v1\classes\traits\TraitConfigController;

/**
 * Class AboutCompanyController - контроллер для получения статического контента О компании
 *
 * @package app\modules\mp\v1\controllers\info
 */
class AboutCompanyController extends AboutCompanyControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения всех элементов
     *
     * @return array - Вернет сформированный массив
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data']) && is_array($result['data'])) {
            $controllerId   = end(explode('/', $this->id));
            $result['data'] = [
                'version' => !empty($this->getIsNeedVersion()) ?
                    (new VersionsDictionaries())->getByDictionary($controllerId) : 0,
                'name'    => $result['data']['name'],
                'text'    => $result['data']['text'],
            ];
            unset($result['version']);
        }

        return $result;
    }
}