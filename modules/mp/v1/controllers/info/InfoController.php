<?php
namespace app\modules\mp\v1\controllers\info;

use app\modules\app\v1\controllers\info\InfoController as InfoControllerApp;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\models\MPVersion;
use Yii;

/**
 * Class InfoController - контроллер для работы с версиями справочников
 *
 * @package app\controllers\app\v1\service
 */
class InfoController extends InfoControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Метод инициализации контроллера
     */
    public function init(): void
    {
        parent::init();
        if (!isset(Yii::$app->params['mp']['settings']['gps_refresh_interval'])) {
            throw new \RuntimeException('В параметрах не указан интервал обновления GPS, обратитесь к администратору');
        }
        if (!is_array(Yii::$app->params['mp']['settings']['assocMethodOfIssuance'])) {
            throw new \RuntimeException(
                'В параметрах некорректно указаны способы выдачи займа, обратитесь к администратору'
            );
        }
    }

    /**
     * Экшен получения всех версий справочников и дополнительных настроек
     *
     * @return array - вернет массив с версиями справочников
     * @throws \Exception
     */
    public function actionIndex()
    {
        $result = parent::actionIndex();
        if (!empty($result['data']) && is_array($result['data'])) {
            $versions = (new MPVersion())->getVersions();
            if (empty($versions)) {
                throw new \RuntimeException(
                    'Произошла ошибка получения параметров версий для мобильного приложения, обратитесь к администратору'
                );
            }
            $assocMethodOfIssuance = Yii::$app->params['mp']['settings']['assocMethodOfIssuance'];
            $length                = array_search('other', array_keys($assocMethodOfIssuance), true);
            $mp                    = array_merge(
                $versions,
                [
                    'settings' => [
                        'gps_refresh_interval' => Yii::$app->params['mp']['settings']['gps_refresh_interval'],
                        'methods_of_payment'   => array_keys(array_splice($assocMethodOfIssuance, 0, $length)),
                    ],
                ]
            );
            //преобразование значений numeric в int для версий справочников
            array_walk(
                $result['data'],
                function (&$v) {
                    $v = is_numeric($v) ? (int) $v : $v;
                }
            );
            $result['data'] = [
                'mp'           => $mp,
                'dictionaries' => $result['data'],
            ];
        }

        return $result;
    }
}