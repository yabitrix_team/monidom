<?php
namespace app\modules\mp\v1\controllers\notification;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\mp\v1\models\MessageReceiver;
use app\modules\mp\v1\models\Notification;
use app\modules\notification\controllers\NotificationController as NotificationControllerMain;
use app\modules\notification\models\Message;
use app\modules\notification\models\MessageMessage;
use app\modules\notification\models\MessageStatus;
use app\modules\notification\models\MessageType;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\JsonParser;

/**
 * Class NotificationController - контроллер взаимодействия с уведомлениями
 *
 * @package app\controllers\mp\v1\notification
 */
class NotificationController extends NotificationControllerMain
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения уведомлений пользователя
     *
     * @return array
     * @throws \Exception
     */
    public function actionIndex()
    {
        $data = (new MessageReceiver())->getUserMessages(Yii::$app->user->id, MessageType::VERIFIER);

        return $this->getClassAppResponse()::get($data);
    }

    /**
     * Экшен обновления статуса уведомления на прочитано (read)
     * с последующим созданием пустого ответа на уведомление
     *
     * @param $code - код уведомления
     *
     * @return array
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionUpdate($code)
    {
        if (!$message = Message::instance()->getByCode($code, ['id', 'type_id', 'order_id'])) {
            throw new \Exception('Уведомление не найдено в системе, обратитесь к администратору');
        }
        $userId  = Yii::$app->user->id;
        $modelMR = new MessageReceiver();
        if (MessageStatus::ANSWERED == $modelMR->getStatusByMessageAndUserId($message['id'], $userId, false)) {
            throw new \Exception('На данное уведомление уже получен ответ');
        }
        if (!$relation = MessageMessage::instance()->getByParentMessage($message['id'], ['id'])) {
            $modelM = new Message(
                [
                    'scenario' => Message::SCENARIO_ADD,
                    'parentId' => $message['id'],
                    'type_id'  => $message['type_id'],
                    'order_id' => $message['order_id'],
                    'active'   => 0,
                ]
            );
            if (!$modelM->validate()) {
                throw new \Exception('Ошибка при работе с веб-сервисом, обратитесь к администратору');
            }
            $modelM->create();
        }
        $result = $modelMR->updateStatusByIdOrCodeMessage($code, MessageStatus::READ, $userId);

        return $this->getClassAppResponse()::get(['set_status' => $result]);
    }

    /**
     * Экшен удаления(деактивации) уведомления у получателя
     *
     * @param $code - код уведомления
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionDelete($code)
    {
        $result = (new MessageReceiver())->deactivateByCodeMessage($code, Yii::$app->user->id);

        return $this->getClassAppResponse()::get(['deleted' => $result]);
    }

    /**
     * Экшен отправки ответа на уведомление
     *
     * @return array
     * @throws \Throwable
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionSend()
    {
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $modelN  = new Notification($reqBody);

        return $this->getClassAppResponse()::get(['sent' => $modelN->send()]);
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig                  = parent::behaviors();
        $arConfig['authenticator'] = [
            'class'       => CompositeAuth::class,
            'authMethods' => [HttpBearerAuth::class],
        ];

        return $arConfig;
    }
}