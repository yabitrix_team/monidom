<?php
namespace app\modules\mp\v1\controllers\service;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\service\SmsController as SmsControllerApp;

/**
 * Class SMSController - контроллер взаимодействия с SMS-сервисом
 *
 * @package app\modules\app\v1\controllers\service
 */
class SmsController extends SMSControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var string - категория лога для МП
     */
    protected $logCategory = 'sms.confirm.mobile';

    /**
     * Метод экшен - для создания и отправки смс сообщения с кодом подтверждения
     *
     * @return array - вернет результат отправки кода подтверждения или ошибку
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        return parent::actionCreate();
    }

    /**
     * Метод экшен - для проверки кода подтверждения операции
     *
     * @param $code - код подтверждения операции
     *
     * @return mixed - вернет результат проверки кода подтверждения или ошибку
     * @throws \yii\db\Exception
     */
    public function actionCheck($code)
    {
        return parent::actionCheck($code);
    }
}