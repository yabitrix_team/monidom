<?php

namespace app\modules\mp\v1\controllers\service;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\service\TimeController as TimeControllerApp;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class TimeController - контроллер взаимодействия с текущим временем на сервере
 *
 * @package app\controllers\app\v1\service
 */
class TimeController extends TimeControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

	/**
	 * Метод экшен - для получения текущего времени в милесекундах
	 *
	 * @return array - вернет текущее время на сервере
	 */
	public function actionIndex() {

		return parent::actionIndex();
	}

	/**
	 * Метод поведения для доп. настройки
	 *
	 * @return array - вернет массив, в котором указаны настройки
	 */
	public function behaviors() {

		$arConfig = parent::behaviors();
		//добавляем проверку на авторизацию, если метод запроса не OPTIONS
		if ( ! Yii::$app->request->getIsOptions() ) {
			$arConfig['authenticator'] = [
				'class'       => CompositeAuth::class,
				'authMethods' => [ HttpBearerAuth::class ],
			];
		}

		return $arConfig;
	}
}