<?php
namespace app\modules\mp\v1\controllers\service;

use app\modules\app\v1\controllers\service\CalcAnnuityController as CalcAnnuityControllerApp;
use app\modules\app\v1\models\Products;
use app\modules\app\v1\models\soap\cmr\Annuity;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use DateTime;
use Yii;
use yii\web\JsonParser;

/**
 * Class CalcAnnuityController - кконтроллер для расчета аннуитета
 *
 * @package app\modules\app\v1\controllers\service
 */
class CalcAnnuityController extends CalcAnnuityControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен - расчет аннуитета
     *
     * @throws \Exception
     */
    public function actionCreate()
    {
        //--- От даты срока займа отнимаем 1 день для учета отдаленных регионов и разницы по времени
        $reqBody = (new JsonParser())->parse(Yii::$app->request->getRawBody(), 'json');
        $model   = new Annuity(['date' => $reqBody['date']]);
        if (
            $model->validate('date')
            && preg_match('/^\d{4}-\d{2}-\d{2}/', $model->date)
        ) {
            $format = 'd.m.Y';
            $d      = new DateTime($model->date);
            $d->modify('-1 day');
            $reqBody['date'] = $d->format($format);
            if ($reqBody['date'] > date($format)) {
                Yii::$app->request->setRawBody(json_encode($reqBody));
            }
        }
        //---
        $result = parent::actionCreate();
        if (!empty($result['data'])) {
            //подготовка идентификатора кредитного продукта и удаление ГУИД
            if (empty($result['data'][$this->assocFields['GUID']])) {
                throw new \Exception(
                    'Не удалось получить ГУИД кредитного продукта для расчета калькулятора, обратитесь к администратору'
                );
            }
            $creditProductId = (new Products)->getDataByGuid($result['data'][$this->assocFields['GUID']], ['id']);
            if (is_a($creditProductId, \Exception::class)) {
                throw new \Exception(
                    'Не удалось получить кредитный продукт для расчета калькулятора, обратитесь к администратору'
                );
            }
            if (empty($creditProductId)) {
                throw new \Exception(
                    'В системе отсутствует кредитный продукт для расчета калькулятора, обратитесь к администратору'
                );
            }
            unset($result['data'][$this->assocFields['GUID']]);
            $result['data']['credit_product_id'] = $creditProductId;
            //todo[echmaster]: 15.10.2018 временное округление аннуитетного платежа
            if (!empty($result['data'][$this->assocFields['Annuity']])) {
                $result['data'][$this->assocFields['Annuity']] =
                    strval(floor($result['data'][$this->assocFields['Annuity']]));
            }
        }

        return $result;
    }
}