<?php
namespace app\modules\mp\v1\controllers\service;

use app\modules\app\v1\controllers\service\CardVerifierController as CardVerifierControllerApp;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use Yii;

/**
 * Class CardVerifierController - контроллер верификации карты клиента
 *
 * @package app\modules\mp\v1\controllers\service
 */
class CardVerifierController extends CardVerifierControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен редиректа на страницу подтверждения карты клиента
     *
     * @param $code - код заявки
     *
     * @return array|\yii\web\Response
     */
    public function actionByCode($code)
    {
        Yii::$app->request->setRawBody('{"code":"'.$code.'"}');
        $result = $this->actionCreate();
        if (!empty($result['data']['url'])) {
            return $this->redirect($result['data']['url']);
        }

        return $result;
    }

    /**
     * todo[echmaster]: 05.02.2019 При необходимости раскомментировать для контроля на авторизацию
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки

    public function behaviors()
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }*/
}