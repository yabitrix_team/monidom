<?php
namespace app\modules\mp\v1\controllers\service;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\service\CalcOptionsController as CalcOptionsControllerApp;

/**
 * Class CalcOptionsController - контроллер получения параметров расчета
 *                              для калькулятора, относительно кредитных продуктов
 *
 * @package app\modules\app\v1\controllers\service
 */
class CalcOptionsController extends CalcOptionsControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
}