<?php

namespace app\modules\mp\v1\controllers\service;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\service\FcmController as FcmControllerApp;

/**
 * Class FcmController - контроллер взаимодействия с текущим временем на сервере
 *
 * @package app\controllers\app\v1\service
 */
class FcmController extends FcmControllerApp {
	/**
	 * В данном трайте находятся методы для настройки контроллера,
	 * к примеру содержащие namespace для определенных класов, которые
	 * нужны для переключения классов в наследуемых контроллерах,
	 * к примеру класс AppResponse
	 */
	use TraitConfigController;

    /**
     * Метод экшен - для получения текущего времени в милесекундах
     *
     * @return array - вернет текущее время на сервере
     * @throws \Exception
     */
	public function actionCreate() {

		return parent::actionCreate();
	}
}