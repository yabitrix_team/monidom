<?php
namespace app\modules\mp\v1\controllers\service;

use app\modules\mp\v1\classes\traits\TraitConfigController;
use app\modules\app\v1\controllers\service\SchedulePaymentController as SchedulePaymentControllerApp;

/**
 * Class SchedulePaymentController - контроллер для работы с сервисом График платежей
 *
 * @package app\modules\app\v1\controllers
 */
class SchedulePaymentController extends SchedulePaymentControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;

    /**
     * Экшен получения графика платежей относительно функции GetPaymentChartv2 сервиса CMRService
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
    public function actionCreate()
    {
        $result = parent::actionCreate();
        if (!empty($result['data'])) {
            $result['data']->full->total_amount = floatval($result['data']->full->totalAmount);
            //todo[echmaster]: 15.10.2018 временное округление аннуитетного платежа
            //$result['data']->annuity            = floatval( $result['data']->annuity );
            $result['data']->annuity = floor($result['data']->annuity);
            unset(
                $result['data']->product,
                $result['data']->early,
                $result['data']->full->totalAmount
            );
            if (!empty($result['data']->full->schedule)) {
                foreach ($result['data']->full->schedule as &$item) {
                    list($y, $m, $d) = explode('-', $item->date);
                    $item->date    = $d.'.'.$m.'.'.$y;
                    $item->total   = floatval($item->total);
                    $item->debt    = floatval($item->debt);
                    $item->percent = floatval($item->percent);
                    unset($item->rest);
                }
            }
        }

        return $result;
    }
}
