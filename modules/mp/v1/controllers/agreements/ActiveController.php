<?php
namespace app\modules\mp\v1\controllers\agreements;

use app\modules\app\v1\controllers\agreements\ActiveController as ActiveControllerApp;
use app\modules\app\v1\models\Contracts;
use app\modules\mp\v1\classes\traits\TraitConfigController;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class ActiveController - контроллер для работы с активными договорами клиента
 *
 * @package app\modules\app\v1\controllers
 */
class ActiveController extends ActiveControllerApp
{
    /**
     * В данном трайте находятся методы для настройки контроллера,
     * к примеру содержащие namespace для определенных класов, которые
     * нужны для переключения классов в наследуемых контроллерах,
     * к примеру класс AppResponse
     */
    use TraitConfigController;
    /**
     * @var array - ассоциация верхнеуровневых полей полученных от сервиса через SOAP с полями контракта
     */
    protected $assocFieldsMain = [
        'Number'            => 'number',
        'Date'              => 'date',
        'Summ'              => 'amount',
        'MobilePaymentSumm' => 'next_payment',
        'NextPaymentDate'   => 'next_date',
        'FullPaymentSumm'   => 'balance',
    ];
    /**
     * @var array - ассоциация полей нижнего уровня(график) полученных от сервиса через SOAP с полями контракта
     */
    protected $assocFieldsChart = [
        'Date'        => 'date',
        'PaymentSumm' => 'total',
        'Debt'        => 'debt',
        'Percents'    => 'percent',
        'DebtRest'    => 'amount',
    ];
    /**
     * @var string - формат даты для метода asDate
     *             варанты: "short", "medium", "long", "full"
     *             так же можно указать формат вида: "dd.MM.yyyy"
     */
    protected $dateFormat = 'dd.MM.yyyy';
    /**
     * @var bool - изменять ли дату в графике платежей
     */
    protected $fixDateChart = true;

    /**
     * Экшен для получения активных договоров авторизованного пользователя
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
    public function actionIndex()
    {
        $this->assocFieldsChart = [];
        $result                 = parent::actionIndex();
        if (!empty($result['data'])) {
            $number  = array_column($result['data'], $this->assocFieldsMain['Number']);
            $relCode = (new Contracts())->relationCode($number);
            if (is_string($relCode)) {
                throw new \Exception(
                    'При попытке получения активных договоров не удалось установить связь договоров с заявками, обратитесь к администратору'
                );
            }
            if (!empty($relCode)) {
                array_walk(
                    $result['data'],
                    function (&$v) use ($relCode) {
                        $v['request_code'] = $relCode[$v['number']];
                    }
                );

                return $result;
            }
        }

        return $result;
    }

    /**
     * Экшен получения активного договора по его номеру(коду)
     *
     * @param $code - номер(код) договора
     *
     * @return array - вернет результирующий массив
     * @throws \yii\base\InvalidConfigException
     */
    public function actionByCode($code)
    {
        $this->assocFieldsMain = [
            'Summ'    => 'amount',
            'Annuity' => 'annuity',
            'PSK'     => 'psk',
            'Chart'   => 'chart',
        ];
        $result                = parent::actionByCode($code);
        if (!empty($result['data'])) {
            $result['data'] = reset($result['data']);
        }

        return $result;
    }

    /**
     * Метод поведения для доп. настройки
     *
     * @return array - вернет массив, в котором указаны настройки
     */
    public function behaviors()
    {
        $arConfig = parent::behaviors();
        //добавляем проверку на авторизацию, если метод запроса не OPTIONS
        if (!Yii::$app->request->getIsOptions()) {
            $arConfig['authenticator'] = [
                'class'       => CompositeAuth::class,
                'authMethods' => [HttpBearerAuth::class],
            ];
        }

        return $arConfig;
    }
}