<?php
namespace app\modules\mp\v1\models;

use app\modules\app\v1\models\Requests as RequestApp;
use app\modules\app\v1\models\RequestsOrigin;
use app\modules\app\v1\models\RequestMP;
use Yii;
use yii\base\Exception;
use yii\db\Expression;

/**
 * Class Requests - модель для работы с Заявкой
 *
 * @package app\modules\app\common\models
 */
class Requests extends RequestApp
{
    /**
     * @var string - способ выдачи относительно МП,
     *             учесть ассоциации из параметров
     */
    public $method_of_issuance;
    /**
     * @var array - обязательные поля при создании заявки,
     *            в наследнике переопределить под свои задачи
     */
    protected $reqFieldCreate = [
        'summ',
        'date_return',
        'credit_product_id',
    ];
    /**
     * @var array - поля создания заявки для сценария Создание
     */
    protected $fieldScenarioCreate = [
        'summ',
        'date_return',
        'credit_product_id',
        'credit_limit_code',
    ];
    /**
     * @var array - поля обновления заявки для сценария Обновление
     */
    protected $fieldScenarioUpdate = [
        'summ',
        'date_return',
        'credit_product_id',
        'auto_brand_id',
        'auto_model_id',
        'auto_year',
        'method_of_issuance', //отдельное свойство способа выдачи для МП, отсутствует в базе
        'card_number',
        'credit_product_id',
        'client_first_name',
        'client_last_name',
        'client_patronymic',
        'client_passport_number',
        'client_passport_serial_number',
        'client_birthday',
        'client_region_id',
        'client_mobile_phone',
        'comment_client',
        'client_email',
        'client_total_monthly_income',
        'client_total_monthly_outcome',
        'client_employment_id',
        'client_workplace_experience',
        'client_workplace_address',
        'client_workplace_phone',
        'client_workplace_name',
        'client_guarantor_name',
        'client_guarantor_relation_id',
        'client_guarantor_phone',
        'place_of_stay',
        'credit_limit_code',
    ];
    /**
     * @var string - место создания заявки, смотреть в таблице requests_origin
     * допустимые варианты: MOBILE, PARTNER, CALL, SIDES_CALL
     */
    protected $requestOrigin = RequestsOrigin::IDENTIFIER_MOBILE;
    /**
     * @var string - способ выдачи по умолчанию для МП
     */
    protected $methodOfIssuanceDefault = 'contact';
    /**
     * @var string - способ выдачи другими способами,не входящими
     *              в массив ассоциаций assocMethodOfIssuance в параметрах
     */
    protected $methodOfIssuanceOther = 'other';
    /**
     * @var array - массив ассоциаций способов выдачи
     *              получаем из параметров
     */
    protected $assocMethodOfIssuance;

    /**
     * Метод инициализации модели
     *
     * @throws \Exception
     */
    public function init()
    {
        //проверка на заведенные параметры ассоциации способов выдачи для МП, исключаем вариант другие способы
        $this->assocMethodOfIssuance = Yii::$app->params['mp']['settings']['assocMethodOfIssuance'];
        unset($this->assocMethodOfIssuance[$this->methodOfIssuanceOther]);
        if (
            empty($this->assocMethodOfIssuance)
            || !is_array($this->assocMethodOfIssuance)
            || !array_key_exists($this->methodOfIssuanceDefault, $this->assocMethodOfIssuance)
        ) {
            throw new \Exception(
                'Не указаны или указаны некорректно ассоциации способов выдачи для мобильного приложения, обратитесь к администратору'
            );
        }
        parent::init();
    }

    /**
     * Метод настройки валидации полей модели
     *
     * @return array правила валидации передаваемых данных
     */
    public function rules()
    {
        $rules = parent::rules();

        return array_merge(
            $rules,
            [
                [
                    'method_of_issuance',
                    function ($attribute, $params) {
                        if (
                            !empty($this->$attribute) &&
                            !array_key_exists($this->$attribute, $this->assocMethodOfIssuance)
                        ) {
                            $this->addError(
                                $attribute,
                                'Со стороны мобильного приложения некорректно передан символьный идентификатор способа выдачи займа, обратитесь к администратору'
                            );
                        }
                    },
                ],
            ]
        );
    }

    /**
     * Метод получения идентификатора точки
     *
     * @return integer - вернет идентификатор точки или выбросит исключение
     * @throws \InvalidArgumentException
     */
    protected function getPointId()
    {
        $pointId = Yii::$app->params['mp']['settings']['point_id'];
        if (empty($pointId) || !is_numeric($pointId)) {
            throw new \InvalidArgumentException(
                'В параметрах не указана или указана некорректно точка для мобильного приложения, обратитесь к администратору'
            );
        }

        return $pointId;
    }

    /**
     * Метод получения идентификатора пользоателя создавшего заявку
     *
     * @return int|string - вернет идентификатор пользователя
     */
    protected function getUserId()
    {
        $userId = Yii::$app->params['mp']['settings']['user_id'];
        if (empty($userId) || !is_numeric($userId)) {
            throw new \InvalidArgumentException(
                'В параметрах не указан или указан некорректно идентификатор подписанта для мобильного приложения, обратитесь к администратору'
            );
        }

        return $userId;
    }

    /**
     * Метод получения всех Заявок
     *
     * @param bool $active - Параметр отвечающий за получение всех Заявок, в том числе неактивных
     *                     true - по умолчанию, выбираем только активные
     *                     false - выбираем только неактивные
     *                     null - выбираем все и актиные и неактивные
     *
     * @return array|false|string - вернет массив данных или ложь, или строку с исключением
     */
    public function getAll($active = true)
    {
        try {
            $filter = is_null($active) ? [] : (empty($active) ? ['active' => 0] : ['active' => 1]);
            $filter = array_merge($filter, ['client_id' => Yii::$app->user->id]);

            return $this->getAllByFilter($filter, ['created_at' => 'desc']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения заявки по коду
     *
     * @param string $code       - код заявки
     * @param array  $fields     - поля для выборки
     * @param bool   $forCurUser - флаг, отвечает за строгую выборку только для авторизованного пользователя
     *
     * @return array|false - вернет результирующий массив при выборе нескольких полей
     *                                       вернет значение при выборе одного поля
     *                                       вернет ложь если не нашел запись
     *                                      при выбросе исключения вернет объект исключения
     * @throws \Exception
     */
    public function getByCode(string $code, array $fields = [], $forCurUser = true)
    {
        try {
            if (strlen($code) != 32) {
                throw new \Exception('При попытке получения заявки по коду был некорректно указан код заявки');
            }
            $fields    = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields    = '[['.implode("]],[[", $fields).']]';
            $condition = empty($forCurUser) ? '' : ' AND [[client_id]] = '.Yii::$app->user->id;

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    '  WHERE [[code]] =:code '.$condition,
                    [':code' => $code]
                )->queryOne();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения данных заявки по коду из базы данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод получения заявки по коду файла
     * с учетом того, что заявка принадлежить текущему пользователю
     *
     * @param $code - код файла
     *
     * @return array|false|string - в случае успеха вернет результирующий массив,
     *                            либо ложь, если к заяке не привязан файл,
     *                            либо строку с исключением
     */
    public function getByCodeFile($code)
    {
        try {
            return Yii::$app->db->createCommand(
                'SELECT [[r]].[['.implode(']],[[r]].[[', self::getFieldsNames()).']] 
FROM '.$this->getTableName().' AS [[r]] 
LEFT JOIN [[request_file]] AS [[rf]] ON [[r]].[[id]] = [[rf]].[[request_id]]
LEFT JOIN [[file]] AS [[f]] ON [[f]].[[id]] = [[rf]].[[file_id]]
WHERE [[r]].[[client_id]] = '.Yii::$app->user->id.' AND [[f]].[[code]] = :code',
                [':code' => $code]
            )
                ->queryOne();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных по клиенту
     *
     * @param       $phone  - номер телефона клиента
     * @param array $fields - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \Exception
     */
    public function getByClient($phone, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    ' WHERE [[client_id]] = (SELECT [[id]] FROM {{users}} WHERE [[username]] = :phone)',
                    [':phone' => $phone]
                )->queryAll();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения данных заявки по польователю в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод создания заявки
     *
     * @param array $params - Параметры для создания заявки,
     *                      можно использовать совместно с  $model->load
     *
     * @return bool|string - Вернет код(хэш) в случае успеха иначе ложь
     * @throws \Exception
     */
    public function create(array $params = [])
    {
        $params['client_id'] = Yii::$app->user->getId();
        //по умолчанию при создании заявки способ выдачи выставляем Через Контакт
        $params['method_of_issuance_id'] = $this->assocMethodOfIssuance[$this->methodOfIssuanceDefault];

        return parent::create($params);
    }

    /**
     * Метод обновления заявки по коду(хэшу)
     *
     * @param string $code    - Код(хэш) заявки
     * @param array  $params  - Параметры для обновления заявки
     * @param bool   $prepare - флаг для подготовки данных
     *
     * @return bool|\Exception|int - Вернет кол-во обновленных строк
     *                             при выбросе исключения вернет объект исключения
     */
    public function updateByCode(string $code, array $params = [], $prepare = true)
    {
        try {
            if (!empty($this->method_of_issuance)) {
                $params['method_of_issuance_id'] = $this->assocMethodOfIssuance[$this->method_of_issuance];
            }
            //так как св-в нет в таблице requests, то приравниваем к null, для исключения через prepareForDataExecute
            $this->method_of_issuance = null;
            $request                  = $this->getByCode($code, ['id']);
            if (empty($request) && is_string($request)) {
                $msg = empty($request)
                    ? 'При попытке обновления заявки не уадлось найти заявку, обратитесь к администратору'
                    : 'При попытке обновления заявки произошла неизвестная ошибка, обратитесь к администратору';
                throw new \Exception($msg);
            }
            //проверка блокировки с пробросом исключения
            (new RequestMP())->checkBlocking($request['id']);
            //если заявка не заблокирована, то задаем точку и подписанта
            $params['user_id']  = $this->getUserId();
            $params['point_id'] = $this->getPointId();

            return parent::updateByCode($code, $params, $prepare);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Метод обновления заявок по списку кодов
     *
     * @param array $codes  - массив кодов заявок
     * @param array $params - параметры для обновления заявки
     *
     * @return bool - вернет истину в случае усешного обновления иначе ложь
     * @throws \Exception
     */
    public function updateByListCode(array $codes, array $params)
    {
        try {
            $codes                = '"'.implode('","', $codes).'"';
            $params['updated_at'] = new Expression('NOW()');
            $result               = Yii::$app->db
                ->createCommand()
                ->update($this->getTableName(), $params, '[[code]] IN ('.$codes.')')
                ->execute();

            return (bool) $result;
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка обнолвения данных заявки по списку кодов в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод обновления заявок пользователя по его номеру телефона
     *
     * @param       $phone  - номер телефона
     * @param array $params - параметры для обновления
     *
     * @return bool - вернет истину в случае усешного обновления иначе ложь
     * @throws \Exception
     */
    public function updateByClient($phone, array $params)
    {
        try {
            $params['updated_at'] = new Expression('NOW()');
            $result               = Yii::$app->db
                ->createCommand()
                ->update(
                    $this->getTableName(),
                    $params,
                    '[[client_id]] = (SELECT [[id]] FROM {{users}} WHERE [[username]] = :phone)',
                    [':phone' => $phone]
                )
                ->execute();

            return (bool) $result;
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка обнолвения данных заявки по польователю в базе данных, обратитесь к администратору'
            );
        }
    }

    /**
     * Метод провеки доступа к отправке на верификацию заявки
     *
     * @param      $code     - код заявки
     * @param bool $checkPEP -флаг указывающий на необходимость проверки ПЭП в заявке
     *
     * @return bool - вернет истину при соблюдении условий для отправки на верификацию иначе ложь
     * @throws \Exception
     */
    public function accessToVerification($code, bool $checkPEP = false)
    {
        $requiredFields = [
            'summ',
            'date_return',
            'credit_product_id',
            'client_first_name',
            'client_last_name',
            'client_birthday',
            'client_region_id',
            'client_passport_number',
            'client_passport_serial_number',
            'auto_brand_id',
            'auto_model_id',
            'method_of_issuance_id',
        ];
        $requiredFields = !empty($checkPEP) ? array_merge($requiredFields, ['is_pep']) : $requiredFields;
        $request        = $this->getByCode($code, $requiredFields);
        if (empty($request) || is_a($request, \Exception::class)) {
            $msg = empty($request)
                ? 'Не удалось отправить заявку на верификацию, так как заявка отсутствует в системе'
                : 'При попытке получения данных заявки для верификации произошел сбой при работе с базой данных, обратитесь к администратору';
            throw new \RuntimeException($msg);
        }
        if (!empty($checkPEP) && empty($request['is_pep'])) {
            throw new \RuntimeException(
                'Не удалось отправить заявку на верификацию, так как в заявке отсутствует согласие на обработку персональных данных'
            );
        }
        unset($request['is_pep'], $request['method_of_issuance_id'], $request['card_token']);
        foreach ($request as $item) {
            if (!isset($item)) {
                throw new \RuntimeException(
                    'Не удалось отправить заявку на верификацию, так как не все обязательные поля заявки были заполнены'
                );
            }
        }

        return true;
    }
}