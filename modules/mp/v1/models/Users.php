<?php
namespace app\modules\mp\v1\models;

use \app\modules\app\v1\models\Users as UsersApp;
use Yii;
use yii\caching\TagDependency;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class Users - модель для работы с пользователями
 *
 * @package app\modules\mp\v1\models
 */
class Users extends UsersApp
{
    /**
     * @var - для авторизации по токену, данного поля в таблцие users нет
     *      берется из таблицы auth_token, в дальнейшем пересмотреть авторизацию
     *      в пользу хранения токенов в таблице users
     */
    public $expired_at;
    /**
     * Поля необходимые для создание user
     */
    protected $usersFieldCreate = [
        'username',
    ];
    /**
     * @var array - поля создания пользователя для сценария Создание,
     *            в наследнике переопределить под свои задачи
     */
    protected $fieldScenarioCreate = [
        'username',
        'first_name',
        'point_id',
        'crib_client_id',
    ];
    /**
     * @var string - тег для создания кэштега, по которому можно комплексно удалить кэш для всех пользователей
     *             должен совпадать со значением в моделе AuthToken
     */
    protected static $tag = 'users';

    /**
     * Метод для получение identity по токену
     *
     * @param      $token - авторизационный токен
     * @param null $type
     *
     * @return null|\yii\web\IdentityInterface|static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        try {
            $cache    = Yii::$app->cache;
            $cacheTag = md5(Yii::getAlias('@app').self::$tag);
            $cacheKey = md5($cacheTag.'_'.$token);
            $data     = $cache->get($cacheKey);
            //если не закэширован или закеширован, но время жизни токена истекло
            if (
                $data === false ||
                ($data && $data->expired_at <= time())
            ) {
                //если закэширован с просроченным временем жизни токена, то удаляем кэш
                if ($data) {
                    $cache->delete($cacheKey);
                }
                //получаем данные по токену с учетом времени жизни токена
                $result = Yii::$app->db->createCommand(
                    "SELECT [[u]].[[".implode("]],[[u]].[[", self::getFieldsNames())."]], [[at]].[[expired_at]]
FROM ".self::getTableName()." as [[u]]
LEFT JOIN {{auth_token}} as [[at]] ON [[u]].[[id]] = [[at]].[[user_id]] 
WHERE [[at]].[[expired_at]] > ".time()." AND [[at]].[[token]] = :token",
                    [':token' => $token]
                )
                    ->queryOne();
                if (empty($result)) {
                    return null;
                }
                $data = new static ($result);
                $cache->set($cacheKey, $data, $cache->defaultDuration, new TagDependency(['tags' => $cacheTag]));
            }

            return empty($data) ? null : $data;
        } catch (\Exception $e) {
            //todo[echmaster]: 24.04.2018 Добавить логирование
            $e->getMessage();

            return null;
        }
    }

    /**
     * Метод сохранения пользователя в таблице
     *
     * @return $this|int|string - в случае успеха вернет идентификатор пользователя,
     *                          либо объект, если валидация не прошла, доступ к ошибкам $this->firstErrors
     *                          либо строку, если выброшено исключение
     */
    public function save()
    {
        try {
            $this->point_id = empty($this->point_id)
                ? Yii::$app->params['mp']['settings']['point_id']
                : $this->point_id;
            if ($this->validate()) {
                $insert = $this->getAttributes(
                    [
                        'username',
                        'password_hash',
                        'first_name',
                        'point_id',
                        'crib_client_id',
                    ]
                );
                $res    = Yii::$app->db->createCommand()
                    ->insert(self::getTableName(), $insert)
                    ->execute();
                if (empty($res)) {
                    throw new \Exception(
                        'При попытке сохранения пользователя произошла ошибка вставки данных в таблицу, обратитесь к администратору'
                    );
                }

                return intval(Yii::$app->db->lastInsertID);
            } else {
                return $this;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Метод получения данных пользователя по его логину
     *
     * @param       $username - логин пользователя
     * @param array $fields   - поля для выборки
     *
     * @return array|false - вернет результирующий массив или ложь
     * @throws \Exception
     */
    public function getByUsername($username, array $fields = [])
    {
        try {
            $fields = !empty($fields) ? $fields : $this->getFieldsNames();
            $fields = '[['.implode("]],[[", $fields).']]';

            return Yii::$app->db
                ->createCommand(
                    'SELECT '.$fields.
                    ' FROM '.$this->getTableName().
                    ' WHERE [[username]] = '.$username,
                    [':username' => $username]
                )->queryAll();
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка получения данных пользователя по логину из базы данных, обратитесь к администратору'
            );
        }
    }
}