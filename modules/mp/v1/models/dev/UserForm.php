<?php
namespace app\modules\mp\v1\models\dev;

use app\modules\app\v1\validators\PhoneValidator;
use app\modules\app\v1\models\AuthToken;
use app\modules\mp\v1\models\Users;
use yii\base\Model;

/**
 * Class UserForm - модель взаимдоействия с экшенами контроллера пользователя в режиме дев
 *
 * @package app\modules\mp\v1\models\dev
 */
class UserForm extends Model
{
    /**
     * Поля модели
     */
    public $login;
    /**
     * @var - св-во для хранения данных пользователя
     */
    protected $user;

    /**
     * Метод получения правил валидации модели
     *
     * @return array - правила валидации
     */
    public function rules()
    {
        return [
            [
                'login',
                'required',
                'message' => 'Не указан логин',
            ],
            [
                'login',
                PhoneValidator::class,
            ],
            [
                'login',
                'validateLogin',
            ],
        ];
    }

    /**
     * Метод валидация логина
     *
     * @param $attribute
     * @param $params
     *
     * @throws \Exception
     */
    public function validateLogin($attribute, $params)
    {
        $this->user = (new Users)->getByUsername($this->{$attribute}, ['id']);
        if (empty($this->user)) {
            $this->addError($attribute, 'Неверно указан логин');
        }
    }

    /**
     * Метод сброса логина пользователя
     *
     * @return bool - вернет истину в случае успеха, иначе ложь
     * @throws \Exception
     */
    public function resetUsername()
    {
        if (!$this->validate()) {
            throw new \Exception(reset($this->firstErrors));
        }
        $newUsername = md5(str_shuffle('reset username'.time()).microtime(true));
        $result      = (new Users())->updateByUsername($this->login, ["username" => $newUsername]);
        if (!empty($result)) {
            (new AuthToken())->deleteByUserId($this->user['id']);
        }

        return boolval($result);
    }
}