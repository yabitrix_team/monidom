<?php
namespace app\modules\mp\v1\models\dev;

use app\modules\app\v1\models\Contracts;
use app\modules\app\v1\validators\PhoneValidator;
use app\modules\mp\v1\models\RequestMP;
use app\modules\mp\v1\models\Requests;
use DateTime;
use yii\base\Model;

/**
 * Class RequestForm - модель взаимдоействия с экшенами контроллера заявки в режиме дев
 *
 * @package app\modules\mp\v1\models\dev
 */
class RequestForm extends Model
{
    /**
     * Константы сценариев
     */
    const SCENARIO_GET           = 'get';
    const SCENARIO_BLOCK         = 'block';
    const SCENARIO_ACTIVATION    = 'activation';
    const SCENARIO_ADD_AGREEMENT = 'add_agreement';
    /**
     * Поля модели
     */
    public $code;
    public $login;
    public $blocked_by;
    public $active = 1;
    /**
     * @var array - массив полей валидации по сценариям
     */
    protected static $fieldsScenarios = [
        self::SCENARIO_GET           => [
            'login',
        ],
        self::SCENARIO_BLOCK         => [
            'code',
            'blocked_by',
        ],
        self::SCENARIO_ACTIVATION    => [
            'code',
            'login',
            'active',
        ],
        self::SCENARIO_ADD_AGREEMENT => [
            'code',
        ],
    ];
    /**
     * @var - св-во для хранения данных заявки
     */
    protected $request;

    /**
     * Метод получения правил валидации модели
     *
     * @return array - правила валидации
     */
    public function rules()
    {
        return [
            [
                'login',
                'required',
                'on'      => [self::SCENARIO_GET],
                'message' => 'Не указан логин',
            ],
            [
                'login',
                PhoneValidator::class,
            ],
            [
                'code',
                'required',
                'on'      => [self::SCENARIO_BLOCK, self::SCENARIO_ADD_AGREEMENT],
                'message' => 'Не указан код заявки',
            ],
            [
                'code',
                'string',
                'message' => 'Код указан в некорректном формате',
            ],
            [
                'code',
                'validateCode',
            ],
            [
                'code',
                'filter',
                'except' => [self::SCENARIO_ADD_AGREEMENT],
                'filter' => function ($val) {
                    empty($val) ?: $val = explode(',', str_replace(' ', "", $val));

                    return $val;
                },
            ],
            [
                'blocked_by',
                'required',
                'on'      => [self::SCENARIO_BLOCK],
                'message' => 'Не указан тип блокировки',
            ],
            [
                'blocked_by',
                'in',
                'range'   => [RequestMP::BLOCKED_BY_CLIENT, RequestMP::BLOCKED_BY_PARTNER, 'none'],
                'message' => 'Тип блокировки указан неверно',
            ],
            [
                'blocked_by',
                'filter',
                'filter' => function ($val) {
                    return $val == 'none' ? null : $val;
                },
            ],
            [
                'active',
                'filter',
                'filter' => function ($val) {
                    return intval($val);
                },
            ],
        ];
    }

    /**
     * Метод установки сценария для модели при валидации полей
     *
     * @return array - вернет массив сценариев,
     *                  по умолчанию установлен сценарий DEFAULT
     */
    public function scenarios()
    {
        $scenarios                            = parent::scenarios();
        $scenarios[self::SCENARIO_GET]        = self::$fieldsScenarios[self::SCENARIO_GET];
        $scenarios[self::SCENARIO_BLOCK]      = self::$fieldsScenarios[self::SCENARIO_BLOCK];
        $scenarios[self::SCENARIO_ACTIVATION] = self::$fieldsScenarios[self::SCENARIO_ACTIVATION];

        return $scenarios;
    }

    /**
     * Метод валидация кода заявки
     *
     * @param $attribute
     * @param $params
     *
     * @throws \Exception
     */
    public function validateCode($attribute, $params)
    {
        $this->request = (new Requests())->getByCode($this->{$attribute}, ['num_1c', 'id', 'client_id'], false);
        if (empty($this->request)) {
            $this->addError($attribute, 'Неверно указан код заявки');
        }
    }

    /**
     * Метод получения активных и деактивных заявок
     *
     * @return array - вернет результирующий массив
     * @throws \Exception
     */
    public function get()
    {
        if (!$this->validate()) {
            throw new \Exception(reset($this->firstErrors));
        }
        $result = [
            'active'   => [],
            'deactive' => [],
        ];
        $data   = (new Requests())->getByClient(
            $this->login,
            [
                'code',
                'summ',
                'date_return',
                'client_last_name',
                'client_first_name',
                'client_patronymic',
                'created_at',
                'active',
            ]
        );
        if (!empty($data)) {
            foreach ($data as $item) {
                $key = empty($item['active']) ? 'deactive' : 'active';
                unset($item['active']);
                $item['date_return'] = (new DateTime($item['date_return']))->format('d.m.Y');
                $item['created_at']  = (new DateTime($item['created_at']))->format('d.m.Y H:i:s');
                $result[$key][]      = $item;
            }
        }

        return $result;
    }

    /**
     * Метод активации и диактивации заявок пользователя
     *
     * @return bool - вернет истину в случае успеха иначе ложь
     * @throws \Exception
     */
    public function activation()
    {
        if (!$this->validate() || (empty($this->code) && empty($this->login))) {
            $msg = !$this->validate()
                ? reset($this->firstErrors) : 'Необходимо указать код заявки или логин пользователя';
            throw new \Exception($msg);
        }
        $modelR = new Requests();
        if (!empty($this->code)) {
            return $modelR->updateByListCode($this->code, ['active' => $this->active]);
        }

        return $modelR->updateByClient($this->login, ['active' => $this->active]);
    }

    /**
     * Метод изменения блокировки заявки
     *
     * @return bool - вернет истину в случае успеха иначе ложь
     * @throws \Exception
     */
    public function block()
    {
        if (!$this->validate()) {
            throw new \Exception(reset($this->firstErrors));
        }

        return (new RequestMP())->updateByListRequestCode($this->code, ['blocked_by' => $this->blocked_by]);
    }

    /**
     * Метод добавления договора к заявке
     *
     * @return bool - верет истину в случае успеха, иначе ложь
     * @throws \Exception
     */
    public function addAgreement()
    {
        if (!$this->validate()) {
            throw new \Exception(reset($this->firstErrors));
        }
        $result = (new Contracts())->save(
            [
                'code'       => $this->request['num_1c'],
                'request_id' => $this->request['id'],
                'user_id'    => $this->request['client_id'],
            ]
        );

        return boolval($result);
    }
}