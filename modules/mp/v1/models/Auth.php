<?php

namespace app\modules\mp\v1\models;

use app\modules\app\v1\models\AuthToken;
use app\modules\app\v1\validators\PhoneValidator;
use Yii;
use yii\base\Model;

/**
 * Class Auth - модель для работы с авторизацией пользователя
 *
 * @package app\modules\mp\v1\models
 */
class Auth extends Model {
	/**
	 * @var - логин пользователя в виде номера телефона
	 */
	public $login;
	/**
	 * @var - пароль пользователя
	 */
	public $password;
	/**
	 * @var - для заполнения данными юзера при валидации
	 */
	protected $user;

	/**
	 * Правила валидации
	 *
	 * @return array - вернет массив правил валидации
	 */
	public function rules() {

		return [
			[
				'login',
				'required',
				'message' => 'Номер телефона обязателен для заполнения',
			],
			[
				'password',
				'required',
				'message' => 'Пароль обязателен для заполнения',
			],
			[
				'login',
				PhoneValidator::class,
			],
			[ 'login', 'validateAuth' ],
		];
	}

	/**
	 * Валидация авторизации польователя, проверит логин на существование
	 * и корректность ввода пароля
	 *
	 * @param $attribute - атрибуты
	 * @param $params
	 */
	public function validateAuth( $attribute, $params ) {

		$res = Users::getByLogin( $this->{$attribute} );
		if ( is_string( $res ) ) {
			$this->addError( $attribute, 'При попытке авторизации не удалось проверить пользователя, обратитесь к администратору' );
		}
		if (
			empty( $res )
			|| empty( $res['password_hash'] )
			|| ! Yii::$app->getSecurity()->validatePassword( $this->password, $res['password_hash'] )
		) {
			$this->addError( $attribute, 'При попытке авторизации был некорректно указан номер телефона или пароль пользователя' );
		}
		//при валидации получаем данные пользователя
		$this->user = $res;
	}

	/**
	 * Метод авторизации пользователя
	 *
	 * @return $this|int|string - в случае успеха вернет токен авторизации,
	 *                          либо объект, если валидация не прошла, доступ к ошибкам $this->firstErrors
	 *                          либо строку, если выброшено исключение
	 */
	public function signIn() {

		try {
			if ( $this->validate() ) {
				$model = new AuthToken();
				$model->load( [ 'user_id' => $this->user['id'] ], '' );
				$res = $model->save();
				if (
					empty( $res )
					|| ( is_string( $res ) && strlen( $res ) != 32 )
					|| is_object( $res )
				) {
					throw new \Exception( 'При попытке авторизации пользователя не удалось записать токен авторизации, обратитесь к администратору' );
				}

				return $res;
			} else {
				return $this;
			}
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}
}