<?php
namespace app\modules\mp\v1\models;

use app\modules\app\v1\models\RequestMP as RequestMPApp;
use Yii;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class RequestsMP - модель для работы с таблицей содержащей
 * инфу по заявке для мобильного приложения
 *
 * @package app\modules\app\common\models
 */
class RequestMP extends RequestMPApp
{
    /**
     * Метод обновления данных по списку кодов заявок
     *
     * @param array $codes  - коды заявок
     * @param array $params - параметры обновления
     *
     * @return bool - вернет истину в случае успешного обновления инач ложь
     * @throws \Exception
     */
    public function updateByListRequestCode(array $codes, array $params)
    {
        try {
            $codes                = '"'.implode('","', $codes).'"';
            $params['updated_at'] = new Expression('NOW()');
            $result               = Yii::$app->db
                ->createCommand()
                ->update(
                    $this->getTableName(),
                    $params,
                    '[[request_id]] IN (SELECT [[id]] FROM {{requests}} WHERE [[code]] IN ('.$codes.'))'
                )
                ->execute();

            return boolval($result);
        } catch (Exception $e) {
            throw new \Exception(
                'Ошибка обнолвения дополнительных данных заявки по списку кодов в базе данных, обратитесь к администратору'
            );
        }
    }
}