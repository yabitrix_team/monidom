<?php
namespace app\modules\mp\v1\models;

use app\modules\app\v1\models\Register as RegisterApp;
use app\modules\app\v1\models\SmsConfirm;
use app\modules\app\v1\models\UserGroup;
use Yii;

/**
 * Class Register - модель для работы с регистрацей пользователя
 *
 * @package app\modules\mp\v1\models
 */
class Register extends RegisterApp
{
    /**
     * @var - идентификатор из системы CRIB
     */
    public $crib_client_id = '';
    /**
     * @var array - поля регистрации пользователя,
     *            в наследнике переопределить под свои задачи
     */
    protected $registerFieldCreate = [
        'login',
        'password',
    ];

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules(): array
    {
        $rules   = parent::rules();
        $rules[] = ['login', 'validateExists'];
        $rules[] = [
            'crib_client_id',
            'string',
            'message' => 'Идентификатор из системы CRIB указан в некорректном формате, обратитесь к администратору',
        ];

        return $rules;
    }

    /**
     * Валидация логина, проверка на существование в базе
     *
     * @param $attribute - атрибуты
     * @param $params
     */
    public function validateExists($attribute, $params): void
    {
        $res = Users::getByLogin($this->{$attribute});
        if (is_string($res)) {
            $this->addError(
                $attribute,
                'При регистрации не удалось проверить пользователя, обратитесь к администратору'
            );
        }
        if (!empty($res)) {
            //Пользователь с указанным номером телефона уже зарегистрирован
            $this->addError($attribute, 1150);
        }
    }

    /**
     * Метод регистрации пользователя
     *
     * @return $this|int|string - в случае успеха вернет идентификатор пользователя,
     *                          либо объект, если валидация не прошла, доступ к ошибкам $this->firstErrors
     *                          либо строку, если выброшено исключение
     */
    public function signUp()
    {
        try {
            if (!$this->validate()) {
                return $this;
            }
            $modSmsConfirm = new SmsConfirm();
            $smsConfirm    = $modSmsConfirm->getByLogin($this->login, ['status']);
            if (empty($smsConfirm)) {
                throw new \Exception(
                    'Для указанного пользователя отсутствует операция для подтверждения регистрации, обратитесь к администратору'
                );
            }
            if (is_string($smsConfirm)) {
                throw new \Exception(
                    'При попытке регистрации пользователя произошла ошика проверки кода подтверждения регистрации отправленного через СМС, обратитесь к администратору'
                );
            }
            if ($smsConfirm['status'] != 1) {
                throw new \Exception('Регистрация пользователя не подтверждена по СМС коду');
            }
            $modUsers = new Users();
            //регистрация юзера в системе CRIB с последующим записью полученного crib_client_id нашему юзеру
            $cribClientID = Yii::$app->cribMP->registryPush($this->login, $this->crib_client_id);
            $modUsers->load(
                [
                    'username'       => $this->login,
                    'first_name'     => $this->name,
                    'crib_client_id' => empty($cribClientID) ? null : $cribClientID,
                ],
                ''
            );
            $modUsers->hashPassword($this->password);
            $userId = $modUsers->save();
            //ошибка валидации или исключение, выводим сразу
            if (is_object($userId) || is_string($userId)) {
                return $userId;
            }
            //присваиваем группу
            $userGroupId = Yii::$app->params['mp']['settings']['user_group_id'];
            if (empty($userGroupId) || !is_numeric($userGroupId)) {
                $modUsers->deleteOne($userId);
                throw new \Exception(
                    'При регистрации пользователя не удалось получить группу для присвоения ее пользователю, обратитесь к администратору'
                );
            }
            $modGroups = new UserGroup();
            $modGroups->load(
                [
                    'user_id'  => $userId,
                    'group_id' => $userGroupId,
                ],
                ''
            );
            if (!$modGroups->validate() || !is_numeric($modGroups->create())) {
                throw new \Exception(
                    'При регистрации пользователя не удалось присовить группу пользователя, обратитесь к администратору'
                );
            }
            //удаляем запись подтверждения операции
            $modSmsConfirm->deleteByLogin($this->login);

            return $userId;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}