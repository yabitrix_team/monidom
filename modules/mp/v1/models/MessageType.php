<?php
namespace app\modules\mp\v1\models;

use app\modules\notification\models\MessageType as MessageTypeNotification;

/**
 * Класс по работе с типами сообщений
 *
 * @property array     $list                - список типов
 * @property false|int $test                - ID типа ТЕСТ
 * @property false|int $status              - ID типа СТАТУС
 * @property false|int $crm                 - ID типа ЦРМ
 * @property false|int $sys                 - ID типа СИСТЕМНОЕ
 * @property false|int $ver                 - ID типа ВЕРИФИКАТОР
 * @property false|int $req                 - ID типа ЗАЯВКА
 * @property false|int $adv                 - ID типа РЕКЛАМА
 * @property false|int $firstpack           - ID типа ПЕРВЫЙ ПАКЕТ ДОК-В
 * @property false|int $secondpack          - ID типа ВТОРОЙ ПАКЕТ ДОК-В
 * @property false|int $requp               - ID типа ОБНОВЛЕНИЕ ПОЛЕЙ ЗАЯВКИ
 * @property mixed     $publicTypes         - список ID типов сообщений, доступных к публикации
 * @property array     $namesPublicTypes    - список NAME типов сообщений, доступных к публикации
 * @property array     $relationPublicTypes - массив связей идентификаторов с именами типов
 * @property false|int $default             - тип по умолчанию
 */
class MessageType extends MessageTypeNotification
{
    /**
     * @var - массив ассоциаций констант типов уведомлений с типами для МП,
     *            (соблеюдение контратка для МП), значение ключей массива
     *             в контракте выводяться в nt_title
     */
    public static $assocType = [
        self::VERIFIER => 'LoanExtraPhoto',
    ];
}