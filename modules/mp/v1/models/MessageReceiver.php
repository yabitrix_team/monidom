<?php
namespace app\modules\mp\v1\models;

use app\modules\notification\models\MessageReceiver as MessageReceiverNotification;

/**
 * Class MessageReceiver - модель для работы с таблицей связей сообщения с пользователями
 *
 * @package app\modules\mp\v1\models
 */
class MessageReceiver extends MessageReceiverNotification
{
    /**
     * @var string - имя типа уведомления
     */
    protected $typeName = '';

    /**
     * Метод получения уведомлений пользователя
     * если не указать тип и статус вернет все увеодмления пользователя
     *
     * @param int              $userId - идентификатор пользователя
     * @param int|string|array $type   - тип увеодмления (принимет id или имя типа, а так же массив id или имен типов)
     * @param int|string|array $status - статус прочитанности (принимет id или имя или массив их)
     *
     * @return array - вернет массив уведомлений
     * @throws \Exception
     */
    public function getUserMessages(int $userId, $type = null, $status = null)
    {
        $relationType = MessageType::instance()->relationPublicTypes;
        //если type не массив, то устанавливаем имя типа, для подготовки SQL
        $this->typeName = !empty($type) && !is_array($type)
            ? (is_numeric($type) ? $relationType[$type] : $type)
            : $this->typeName;
        $result         = parent::getUserMessages($userId, $type, $status);
        //при неуказанном типе подготаливаем тип для контракта
        if (empty($this->typeName)) {
            $filter = false;
            array_walk(
                $result,
                function (&$v) use ($relationType, &$filter) {
                    $keyAssocType = $relationType[$v['nt_type']];
                    if (key_exists($keyAssocType, MessageType::$assocType)) {
                        $v['nt_type'] = MessageType::$assocType[$keyAssocType];
                    } else {
                        $v      = [];
                        $filter = true;
                    }
                }
            );
            empty($filter) ?: $result = array_filter($result);
        }

        return $result;
    }

    /**
     * Метод получения sql запроса для выборки уведомлений пользователя
     * вызывается в методе getUserMessages
     *
     * @param string $condition - дополнительные условия запроса
     *
     * @return string - вернет строку sql-запроса
     */
    protected function getSqlForUserMessages(string $condition = ''): string
    {
        //формирование выборки типа, при указанном имени типа и наличии ассоциации в sql nt_type
        $selectForType  = key_exists($this->typeName, MessageType::$assocType)
            ? '"'.MessageType::$assocType[$this->typeName].'" as nt_type'
            : '[[m]].[[type_id]] as nt_type';
        $this->typeName = key_exists($this->typeName, MessageType::$assocType) ? $this->typeName : '';

        return 'SELECT     
                   [[m]].[[code]] as [[nt_code]], 
                   '.$selectForType.', 
                   "" as [[nt_title]], 
                   [[m]].[[message]] as [[nt_text]], 
                   UNIX_TIMESTAMP([[m]].[[created_at]]) as [[nt_time]],
                   [[ms]].[[name]] as [[nt_status]],
                   [[r]].[[code]] as [[nt_request_code]]
            FROM '.$this->getTableName().' as [[mr]]
            JOIN {{message}} AS [[m]] ON [[m]].[[id]] = [[mr]].[[message_id]]
            JOIN {{requests}} AS [[r]] ON [[r]].[[id]] = [[m]].[[order_id]]
            JOIN {{message_status}} AS [[ms]] ON [[ms]].[[id]] = [[mr]].[[status_id]]
            WHERE [[mr]].[[user_id]] = :userId AND [[mr]].[[active]] = 1 AND [[m]].[[active]] = 1 '.$condition.'
                AND [[m]].[[id]] NOT IN (SELECT [[child_message_id]] FROM {{message_message}})
            ORDER BY [[m]].[[created_at]] DESC';
    }
}
