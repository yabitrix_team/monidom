<?php
namespace app\modules\mp\v1\models;

use app\modules\notification\models\MessageStatus;
use app\modules\notification\models\Message;
use app\modules\app\v1\models\File;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Notification - модель взаимодействия с уведомлениями
 *
 * @package app\modules\mp\v1\models
 */
class Notification extends Model
{
    /**
     * Поля модели
     */
    public $nt_code;//код родительского уведомления
    public $comment;//комментарий
    /**
     * @var - данные по уведомлению
     */
    protected $record;

    /**
     * Правила валидации
     *
     * @return array - вернет массив правил валидации
     */
    public function rules()
    {
        return [
            [
                'nt_code',
                'required',
                'message' => 'Не указан код уведомления',
            ],
            ['nt_code', 'validateCode'],
            [
                'comment',
                'string',
                'message' => 'Комментарий уведомления указан в некорреткном формате',
            ],
        ];
    }

    /**
     * Валидация кода родительского уведомления
     *
     * @param $attribute - атрибуты
     * @param $params
     *
     * @throws \Exception
     */
    public function validateCode($attribute, $params)
    {
        $modelM = new Message();
        $parent = $modelM->getByCode($this->{$attribute}, ['id', 'message', 'status_id', 'active']);
        if (empty($parent) || empty($parent['active'])) {
            $this->addError($attribute, 'Код уведомления указан неверно, обратитесь к администратору');
        }
        $relation = (new MessageStatus())->getRelation();
        $statusId = (new MessageReceiver())->getStatusByMessageAndUserId($parent['id']);
        //todo[echmaster]: 21.01.2019 ограничение один ответ на одно уведомление(возможно будет расширен)
        if (in_array(MessageStatus::ANSWERED, [$relation[$parent['status_id']], $relation[$statusId]])) {
            $this->addError($attribute, 'На данное уведомление ответ уже получен');
        }
        //проверка на наличие НЕ активного ответного уведомления
        $child = $modelM->getChildByParent($parent['id'], ['id', 'active']);
        if (empty($child) || !empty($child['active'])) {
            $this->addError($attribute, 'Не удается отправить ответ на уведомление, обратитесь к администратору');
        }
        $files = $modelM->getJointMessageFile($child['id'], ['m_id' => 'id'], ['f_id' => 'id', 'path']);
        if (empty($files)) {
            $this->addError($attribute, 'Необходимо загрузить файлы перед отправкой');
        }
        $this->record = [
            'parent_id'    => $parent['id'],
            'child_id'     => $child['id'],
            'statuses'     => array_flip($relation),
            'listFileId'   => array_column($files, 'f_id'),
            'listFilePath' => array_column($files, 'path'),
        ];
    }

    /**
     * Метод отправки ответного уведомления с последующим обновлением всех необходимы данных
     *
     * @return bool - вернет истину при усешной отправке и обновлении соотвтетствующих данных
     * @throws \Exception
     * @throws \Throwable
     */
    public function send()
    {
        if (!$this->validate()) {
            throw new \Exception(reset($this->firstErrors));
        }
        $result = false;
        Yii::$app->db->transaction(
            function ($db) use (&$result) {
                $modelM = new Message();
                //отправка ответного уведомления, если не будет выброшено исключение выполняется код дальше
                $modelM->sendMailVer($this->comment, $this->record['parent_id'], $this->record['listFilePath']);
                //после отправки файлов делаем их активными в таблцие file
                (new File())->updateActiveByListId($this->record['listFileId']);
                //после отправки ответного уведомления проставляем статус Отвечено у получателя родительского уведомления
                $idAnswered = $this->record['statuses'][MessageStatus::ANSWERED];
                (new MessageReceiver())->updateStatusByIdOrCodeMessage(
                    $this->record['parent_id'],
                    $idAnswered
                );
                //после отправки ответного уведомления проставляем статус Отвечено в родительском уведомлении
                $modelM->update($this->record['parent_id'], ['status_id' => $idAnswered]);
                //после отправки ответного уведомления в нем пишем коммент и делаем запись активной
                $modelM->update($this->record['child_id'], ['message' => $this->comment, 'active' => '1']);
                $result = true;
            }
        );

        return $result;
    }
}