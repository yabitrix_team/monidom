<?php
namespace app\modules\mp\v1;

use app\modules\mp\v1\components\ErrorComponent;
use app\modules\mp\v1\models\Users;
use Yii;

/**
 * Class Module - модуль для работы с Мобильным приложением
 *
 * @package app\modules\mp\v1
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\mp\v1\controllers';

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        Yii::$app->user->identityClass    = Users::class;
        Yii::$app->user->enableSession    = false; //для МП отключаем сессии
        Yii::$app->user->enableAutoLogin  = false; //для МП отключаем куки
        Yii::$app->cache->defaultDuration = 60 * 60 * 24 * 7;
        //регистрация компонента обработки ошибок
        $handler = new ErrorComponent();
        Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
}
