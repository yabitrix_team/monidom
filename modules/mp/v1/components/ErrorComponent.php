<?php
namespace app\modules\mp\v1\components;

use app\modules\mp\v1\classes\AppResponse;
use app\modules\app\v1\components\ErrorComponent as AppErrorComponent;
use yii\web\HttpException;

class ErrorComponent extends AppErrorComponent
{
    protected $_memoryReserve;

    /**
     * Converts an exception into an array.
     *
     * @param \Exception|\Error $exception the exception being converted
     *
     * @return array the array representation of the exception.
     */
    protected function convertExceptionToArray($exception)
    {
        return AppResponse::get(false, false, $exception->getMessage(), $exception->getCode());
    }

    /**
     * Renders the exception.
     *
     * @param \Exception|\Error $exception the exception to be rendered.
     */
    protected function renderException($exception)
    {
        if (!isset($exception->statusCode)) {
            $exception = new HttpException(200, $exception->getMessage(), $exception->getCode(), $exception);
        }
        parent::renderException($exception);
    }
}