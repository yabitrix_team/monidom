<?php
namespace app\modules\mp\v1\classes;

use Yii;
use \app\modules\app\v1\classes\AppResponse as AppResponseApp;

/**
 * Class AppResponse - класс формирования ответов веб-сервисами
 *
 * @package app\modules\app\v1\classes
 */
class AppResponse extends AppResponseApp
{
    /**
     * @var bool - параметр включения логирования вывода ответа веб-сервисов
     */
    protected static $log = false;

    /**
     * Метод формирования ответа веб-сервисами
     *
     * @param array $data      - Данные для передачи
     * @param bool  $validates - Ошибки валидации полей, пример:
     *                         ['summ'=>'Поле указанно некорректно']
     * @param bool  $msg       - Сообщения исключения
     * @param bool  $code      - Код ошибки
     * @param bool  $version   - Версия справочника при необходимости
     *
     * @return array - Вернет сформированный массив, пример всех полей
     *               [
     *                  error_code => 0
     *                  error_text = Message
     *                  error_validate => [
     *                          field_name => error message
     *                      ],
     *                  version => 0,
     *                  data=>[
     *                      version : 0
     *                  ]
     *              ]
     */
    public static function get($data = [], $validates = false, $msg = false, $code = false, $version = false)
    {
        $errorCodeValidate    = 205; //код валидации полей
        $result['error_code'] = 0;
        if (!empty($code)) {
            $result['error_code'] = $code;
            $result['error_text'] = Yii::$app->msg->get($code);
        }
        if (empty($code) && !empty($msg)) {
            $result['error_code'] = 1;//1 - зарезервированный код, для произвольных сообщений
            $result['error_text'] = $msg;
        }
        if (!empty($validates)) {
            $valid                = is_array($validates) ? reset($validates) : $validates;
            $result['error_code'] = is_numeric($valid)
                ? $valid
                : (empty($code) ? $errorCodeValidate : $code);
            $result['error_text'] = is_numeric($valid)
                ? Yii::$app->msg->get($valid)
                : (!empty($code) ? Yii::$app->msg->get($code) : $valid);
        }
        if (empty($result['error_code'])) {
            empty($version) ?: $result['version'] = $version;
            $result['data'] = $data;
        }

        return $result;
    }
}