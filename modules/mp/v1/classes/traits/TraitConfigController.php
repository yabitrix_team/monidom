<?php

namespace app\modules\mp\v1\classes\traits;

use app\modules\app\v1\classes\traits\TraitConfigController as TraitConfigControllerApp;
use app\modules\mp\v1\classes\AppResponse;
use app\modules\mp\v1\models\Requests;

/**
 * Trait TraitSomeController - трейт содержащий методы-настройки для контроллеров,
 * в данном трейте необходимо подлкючить трейт AbstractTraitSomeController
 * для приведения к однотипности трейтов-настройки
 *
 * @package app\modules\app\v1\classes\traits
 */
trait TraitConfigController {
	/**
	 * Подключаем трейт с абстрактными методами, которые нужно реализовать,
	 * приводим текущий трейт к однотипности трейтов-настройки
	 */
	use TraitConfigControllerApp;

	/**
	 * Метод возвращающий namespace для класса AppResponse
	 * при необходимости переопределяем данный метод и указываем
	 * необходимый namespace в новом подключаемом trait
	 *
	 * @return string|AppResponse - вернет строку содержащую namespace класса AppResponse
	 */
	protected function getClassAppResponse() {

		return AppResponse::class;
	}

	/**
	 * Метод  возвращающий потребителя, для которого выводим статическую страницу
	 * при необходимости переопределяем данный метод и указываем
	 * необходимое значение(partner|client|mp) в новом подключаемом trait
	 *
	 * @return string
	 */
	protected function getConsumer() {

		return 'mp';
	}

	/**
	 * Метод индикатор для получения версии справочников,
	 * если укажем false, то версии не добавляется в ответе веб-сервиса
	 *
	 * @return bool - вернет ложь, допустимо значение true
	 */
	protected function getIsNeedVersion() {

		return true;
	}

	/**
	 * Метод возвращающий namespace для модели Requests
	 * при необходимости переопределяем данный метод и указываем
	 * необходимый namespace в новом подключаемом trait
	 *
	 * @return string|Requests - вернет строку содержащую namespace модели Requests
	 */
	protected function getModelRequest() {

		return Requests::class;
	}
}