<?php
/**
 * вписать идентификаторы хостов и/или дополнить массив хостов
 * для отображения счетчика на хосте.
 */

$arr = [
	'carmoney.ru'=>'49886782',
	'carmoneyuat.ru'=>'',
	'carmoney54.ru'=>'49646518',
	'carmoney55.ru'=>'',
	'carmoney56.ru'=>'',
];

$host = ( ! empty( $_SERVER['HTTP_X_FORWARDED_SERVER'] ) ? $_SERVER['HTTP_X_FORWARDED_SERVER'] :
	$_SERVER['SERVER_NAME'] );

foreach($arr as $key=>$val){
	if(strstr($host, $key) && !empty($val)){
		return '
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter'.$val.' = new Ya.Metrika2({
                            id:'.$val.',
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/'.$val.'" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        ';
	}

}
