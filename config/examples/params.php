<?php

use app\modules\app\v1\components\CardVerifierComponent;
use app\modules\app\v1\components\CRIBComponent;
use app\modules\app\v1\components\LCRMComponent;
use app\modules\app\v1\components\SoapClientComponent;
use app\modules\rmq\components\RmqBase;
use app\modules\notification\Module;
use understeam\fcm\Client;

$db      = require __DIR__.'/db.php';
$private = require __DIR__.'/key-private.php';
$public  = require __DIR__.'/key-public.php';

return [
    //основные настройки, используются глобально
    'core'           => [
        'email'        => [
            'admin'             => 'zaruba@carmoney.ru',
            'from'              => 'backend@carmoney.ru',
            'extraPhoto'        => 'zaruba@carmoney.ru', //доп. фото отправляемые верификаторам
            'supportPhoto'      => 'zaruba@carmoney.ru', //Сообщения отправляемые в ТП
            'feedbackPhoto'     => 'zaruba@carmoney.ru', //Сообщения отправляемые с формы обратной связи
            'callPhoto'         => 'zaruba@carmoney.ru', //Сообщения отправляемые для обратного звонка
            'error'             => 'onishuk@carmoney.ru', //Ошибка отправляемые разработчикам
            'toNewUser'         => 'zaruba@carmoney.ru', //Регистрация нового пользователя (projectskc@carmoney.ru)
            /** список адресов верификаторов */
            'verifications'     => [
                'konev@carmoney.ru',
            ],
            /** список адресов представителей опер.центра */
            'operations_center' => [
                'konev@carmoney.ru',
            ],
        ],
        'sms'          => [
            'attempt'         => 3, //кол-во попыток
            'lock_expired_at' => 1, //время блокировки до следующей отправки смс в минутах
        ],
        'service'      => [
            'schedulePayment' => [
                'url'    => 'http://api2751.carmoney.ru/check/',
                'token'  => 'a09se8rjhq3oeQ089',
                'apiKey' => 'a98703wh92o3r87qhr3r1',
            ],
        ],
        //настройки для модуля пользователи
        'user'         => [
            //группа по умолчанию назначаемая пользователю при регистрации
            'groupGuidDefault' => 'client',
        ],
        //настройки касаемые заявок, своего рода секция по работе с заявками
        'requests'     => [
            //путь для сохранения файлов заявки относительно корня
            'pathSaveFile'     => '/path/request/',
            //статусы очистки полей заявки, если передать пустой массив, то перестанет очищать
            'cleaningStatuses' => [
                '6818753f-7d4c-45e6-ad29-5f98db749508', //деньги выданы
                '2da7f37a-0226-11e6-80f9-00155d01bf07', //договор погашен
                'ff126df5-e381-4312-b4f3-8863314038b2', //клиент получает деньги
                'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e', //отказано
                'c37fec4e-28b3-4239-8c2f-6812821b1b32', //отозвано
            ],
            //поля заявки, которые будут очищены при получении одного из статусов cleaningStatuses
            'cleaningFields'   => [
                'auto_price', //приблизительная стоимость авто
                'client_patronymic', //отчество
                'client_birthday', //дата рождение
                'client_mobile_phone', //мобильный телефон
                'client_email', //электронная почта
                'client_passport_serial_number', //серия паспорта
                'client_passport_number', //номер паспорта
                'client_region_id', //регион регистрации
                'client_home_phone', //домашний телефон
                'client_employment_id', //занятость клиента
                'comment_client', //комментарии клиента
                'client_workplace_experience', //стаж на месте работы
                'client_workplace_period_id', //период работы
                'client_workplace_address', //адрес места работы
                'client_workplace_phone', //телефон по месту работы
                'client_total_monthly_income', //месячынй доход
                'client_total_monthly_outcome', //сумма платежей по кредитам
                'client_guarantor_name', //ФИО доп. контактного лица
                'client_guarantor_relation_id', //кем приходится
                'client_guarantor_phone', //телефон контактного лица
                'method_of_issuance_id', //способ выдачи
                'bank_bik', //БИК банка
                'comment_partner',//комментарий партнера
                'client_workplace_name', //место работ
                'foto_passport', //фото паспорта
                'foto_pts', //фото ПТС
                'foto_sts', //фото СТС
                'foto_auto', //фото авто
                'foto_client', //фото клиента
                'foto_card', //фото кредитной карты
                'doc_pack_1', //первый пакет документов
                'doc_pack_2', //второй пакет документов
            ],
            'checkOldFiles'    => [
                'fileBind'     => [
                    'foto_auto',
                ],
                'checkTimeSec' => 43200,
            ],
        ],
        //Настройка домена второго уровня для поддержки кроссдоменной куки авторизации третьего уровня
        'domain'       => 'carmoney36.local',
        //Параметр включения имитации сторонних сервисов
        'isMock'       => false,
        // настройки rmq
        'rmq'          => [
            'rmq201' => true, // 201 по rmq
            'host' => 'rmq.dev.carmoney.ru',
            'login' => 'lk',
            'password' => 'lk',
        ],
        // логин и пароль пользователя в домене domain
        'domain_user'  => [
            'login'    => 'srv.bitrix',
            'password' => 'bwqqY9Nch6SP4zd',
        ],
        // массив типов сообщений, которые не надо показывать в хистори и справой стороны
        'notification' => [
            'exclude_types' => [
                //'?'=>'lkkblock'
                10 => 'requp',
                11 => 'сardVerifier',
            ],
        ],
        'statuses'     => [
            'not_full'           => '769bd86a-cb49-490e-bbad-60db89c21c2a',
            'pre_approving'      => '7d06fa22-3be6-4066-a5c7-6a0c6e7bd055',
            'pre_approved'       => 'cbb42353-51e6-4a88-b2ef-575729da0343',
            'docs_checking'      => 'e12788f0-7765-42bc-8ed3-828dab3fdf97',
            'docs_checked'       => '1b61eb3a-c293-4fca-8ee6-f672c43302ef',
            'final_approving'    => '623bbcc6-ac4c-4e51-bdcf-f7c5a12b9abd',
            'final_approved'     => 'abfa1724-c3fd-4b7f-9631-7572288a165b',
            'contract_signing'   => '74ed5b21-af9b-44f3-bda5-85fde6847b76',
            'money_take'         => 'ff126df5-e381-4312-b4f3-8863314038b2',
            'money_issued'       => '6818753f-7d4c-45e6-ad29-5f98db749508',
            'contract_completed' => '2da7f37a-0226-11e6-80f9-00155d01bf07',
            'denied'             => 'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e',
            'request_cancelled'  => 'c37fec4e-28b3-4239-8c2f-6812821b1b32',
            'contract_cancelled' => 'e01a9627-195b-4422-860e-91ba96897840',
            'empty'              => '',
        ],
        'cabinetAssoc' => [
            'partner' => 'web',
            'client'  => 'client',
            'backend' => 'mp',
        ]
	],
	//настройки для модуля веб
	'web'            => [
		"session" => [
			'attempt'         => 3, //кол-во попыток проведения операции за интервал lock_expired_at
			'lock_expired_at' => 5, //время в течение которого запрещено выполнять операцию, больше количества attempt
			'ttl'             => 5, //минимальное время между попытками отправки сообщений, в секундах
		],
		'request' => [
			'pagination' => 50, //Количество заявок выводимых на одну страницу пользователю
		],
	],
	//настройки для модуля агенты
	'agent'          => [
		//срок жизни ссылки в секундах
		'print_link_expired_at' => 300,
		//Каталог для хранения изображений print_link
		'print_link_dir'        => '/path/print_link/',
	],
	//настройки для модуля мобильное приложение
	'mp'             => [
		'settings' => [
			'accessKey'             => 'Fmq06HmOt9xsQE5Z0M1U6wSFu82GAm1W',
        	//----
			'gps_refresh_interval' => 60,
			'credit_product_date'  => '2017-12-20', //дата(Y-m-d) ограничивает выбор кредитных продуктов
			'point_id'             => 45, // id точки из таблицы points(указать значение текущего хоста)
			'user_id'              => 20, // id пользователя из таблицы users(указать значение текущего хоста)
			'user_group_id'        => 8, // id группы пользователя из таблицы groups(указать значение текущего хоста)
			'tokenExpired'         => 3600 * 24 * 7, //время жизни токена авторизации
			//массив id кредитных продуктов, которые нужно исключить из выборки
			'excludeProduct'       => [],
			//ассоциация статусов заявки со статусами для МП
			'assocStatuses'        => [
				'cbb42353-51e6-4a88-b2ef-575729da0343' => 'PREAPPROVED',//предварительно одобрено
				'abfa1724-c3fd-4b7f-9631-7572288a165b' => 'APPROVED',//одобрено
				'74ed5b21-af9b-44f3-bda5-85fde6847b76' => 'APPROVED',//ожидает подписания договора
				'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e' => 'DENIED',//отказано
				'c37fec4e-28b3-4239-8c2f-6812821b1b32' => 'CANCELED',//Заявка аннулирована
                'e01a9627-195b-4422-860e-91ba96897840' => 'CANCELED',//Договор аннулирован
			],
			//заголовк пуш-уведомления
			'pushTitle'            => 'CarMoney',
			//текст сообщения пуш-ведомления относительно статусов заявки
			'pushByStatus'         => [
				//предварительно одобрено
				'cbb42353-51e6-4a88-b2ef-575729da0343' => 'Ваша заявка предварительно одобрена. Завершите оформление заявки в приложении',
				//ожидает подписания договора
				'74ed5b21-af9b-44f3-bda5-85fde6847b76' => 'Вам одобрен займ! Для оформления документов с Вами свяжется сотрудник компании',
				//отказано
				'ffe4cfd9-e504-4183-b32a-9c9cc6a1af2e' => 'Уважаемый клиент, Вам отказано в выдаче займа. Спасибо за обращение!',
				//Заявка аннулирована
				'c37fec4e-28b3-4239-8c2f-6812821b1b32' => 'Заявка аннулирована. Оформите новую заявку в приложении',
			],
            //ассоциация способов выдачи для МП, учитывать порядок: первый по дефолту, после other способы не отображаются
			'assocMethodOfIssuance' => [
                'wo_card' => 2, //wallet one на банковскую карту
                'contact' => 8, //через контакт
                'other'   => 0, //другие способы выдачи
                'wo_card_confirm' => 11, //на банковскую карту через WalletOne с подтверждением
			],
		    //todo[echmaster]: 02.02.2019 после получения данных по типам предложений кред.лимитов, указать нужный для МП
            'creditLimitsOfferType' => 'docreditovanie',
        ],
	],
	// настройки для ЛКК
	'client' => [
		'settings' => [
			'point_id' => 1341, // id точки из таблицы points(указать значение текущего хоста)
			'user_id'  => 3928, // id пользователя из таблицы users(указать значение текущего хоста)
		],
	],
    'lcrm'    => [
        'secret' => 'a8f82517fcfcd09ce1d311fb0b6ea76a', // секретный ключ авторизации LCRM в ЛКЦ
    ],
    'crib'           => [
        'cid_cookie' => '_crib_cid',
    ],
    /**
     * блок структурпно дублирующий набор параметров стандартного приложения, в процессе подключения
     * в файле конфигурации расширяет набор дополняя/заменяя имеющиеся значения
     */
    "console_params" => [
        'params'     => [
            'exchange' => [
                'tdb_params'    => [
                    'site_key'   => 'dev_33',
                    'tdb_suffix' => 'tr_base',
                ],
                'user_groups'   => [
                    'partner_admin' => 7,
                    'partner_user'  => 8,
                    'clients'       => 5,
                    'agents'        => 3,
                ],
                'model_version' => '1',
                'events'        => [
                    'photoDocs'   => 711,
                    'photoClient' => [712, 713],
                ],
            ],
        ],
        'modules'    => [
            "notification" =>
                [
                    "class"         => Module::class,
                    "swoole_params" => [
                        'ip'   => '0.0.0.0',
                        'port' => '9035',
                    ],
                    "redis_params"  => [
                        "ip"       => "127.0.0.1",
                        "port"     => "6379",
                        "channels" => ["ch_konev"],
                    ],
                    "tickets"       => [
                        "name" => "konev",
                        // в секундах, время жизни ключа
                        "ttl"  => 86400, // сутки
                    ],
                ],
        ],
        "components" => [
            'queueMFOReceive' => [
                'class'              => RmqBase::class,
                'host'               => 'rmq.dev.carmoney.ru', // 10.10.199.30 // RMQ, server address or IP
                'vhost'              => 'MFO_DEV_ZARUBA00', // RMQ, vhost виртуальный хост в RMQ для разделения хостов
                'port'               => 5672, // RMQ, порт по умолчанию
                'user'               => 'user', // RMQ, логин
                'password'           => 'user', // RMQ, пасс
                'queueName'          => 'mfo.lk.RequestFoto',
                'exchangeName'       => 'MFO',
                'listenRoutingKeys'  => ['RequestFoto'],
                'publishRoutingKeys' => [],
                'exchangeType'       => 'direct', // ['direct', 'fanout', 'topic', 'headers'],
                'anonymousQueue'     => false, // анонимные очереди
                'verbosePublish'     => true,
                'verbose'            => true,
            ],
            'queueMFOSend'    => [
                'class'              => RmqBase::class,
                'host'               => 'rmq.dev.carmoney.ru', // 10.10.199.30 // RMQ, server address or IP
                'vhost'              => 'MFO_DEV_ZARUBA00', // RMQ, vhost виртуальный хост в RMQ для разделения хостов
                'port'               => 5672, // RMQ, порт по умолчанию
                'user'               => 'user', // RMQ, логин
                'password'           => 'user', // RMQ, пасс
                'queueName'          => 'lk.mfo.Foto',
                'exchangeName'       => 'LK',
                'listenRoutingKeys'  => [],
                'publishRoutingKeys' => ['Foto'],
                'exchangeType'       => 'direct', // ['direct', 'fanout', 'topic', 'headers'],
                'anonymousQueue'     => false, // анонимные очереди
                'verbosePublish'     => false,
                'verbose'            => false,
            ],
            'queueCreditLimitsListen'    => [
                'class'              => \app\modules\rmq\components\Rmq::class,
                'vhost'              => 'CRM.TEST00',
                'queueName'          => 'LK.CreditLimits',
            ],
            'queueCreditLimitsPublish'    => [
                'class'              => \app\modules\rmq\components\Rmq::class,
                'vhost'              => 'CRM.TEST00',
                'exchangeName'       => 'CreditLimits',
                'exchangeType'       => 'topic',
                'routingKey'         => 'CRM.CreditLimits.Load.1.1',
            ],
            //компонент интеграции с Firebase Cloud Messaging для отправки push уведомлений
            'fcm'             => [
                'class'  => Client::class,
                //server API Key (you can get it here: https://firebase.google.com/docs/server/setup#prerequisites)
                'apiKey' => 'AAAAfGhM-P4:APA91bGO02BLbJPm5QS4DG4rfEhFzg4KDphwIMpn4ORH3ax5pZB78xZYoyX3q_ddyEe_FmqnyHmOtCxsQE5Z0M1U6wSFuMeGAmIWY9RfvuUKZ23hAPu5R_rIMfR3S9dGI_-mUIbGMaVa',
            ],
            //компонент для интеграции по SOAP протоколу (soap client)
            'soapCMR'         => [
                'class'       => SoapClientComponent::class,
                'url'         => 'http://10.10.100.93/cmr_uat/ws/', //поменять на боевой
                'urlMock'     => 'http://in.carmoneyzaruba.ru/mock/v1/cmr/', //указать свой хост
                'login'       => 'obmen',
                'password'    => 'obmen123',
                'log'         => true,
                'logCategory' => 'soap',
            ],
            'soapCRM'         => [
                'class'       => SoapClientComponent::class,
                'url'         => 'http://10.10.100.93/cmr_uat/ws/', //поменять на боевой
                'urlMock'     => 'http://in.carmoneyzaruba.ru/mock/v1/crm/', //указать свой хост
                'login'       => 'EventLoadUser',
                'password'    => '123',
                'log'         => true,
                'logCategory' => 'soap',
            ],
        ],
    ],
    "web_params"     => [
        'params'     => [
            'payment' => [
                'mfo_service'  => [
                    'url'      => 'http://10.10.100.93/mfo_uat/ws/CheckData_CloudPayments?wsdl',
                    'login'    => 'RoiStat',
                    'password' => 'A123321',
                ],
                'multiplicity' => [                                     // режим множественности способов оплаты
                    'many' => false,
                ],
                'cloud'        => [
                    'name'     => 'CloudPayments',                      // Название платежной системы
                    'publicId' => 'pk_dd99e190825e9b3822845e1dcfcda',   // public key
                    'secret'   => '6886b3be92ae5a939f63234b790fd21c',   // private key
                    'paySys'   => true,                                 // признак платежной системы
                    'active'   => true,                                 // активность
                ],
                'w1'           => [
                    'name'     => 'WalletOne',                          // Название платежной системы
                    'publicId' => '',                                   // public key
                    'secret'   => '6886b3be92ae5a939f63234b790fd21c',   // private key
                    'paySys'   => true,                                 // признак платежной системы
                    'active'   => false,                                // активность
                ],
            ],
        ],
        'modules'    => [
            "notification" =>
                [
                    "class"         => Module::class,
                    "swoole_params" => [
                        'ip'   => '0.0.0.0',
                        'port' => '9035',
                    ],
                    "redis_params"  => [
                        "ip"       => "127.0.0.1",
                        "port"     => "6379",
                        "channels" => ["ch_konev"],
                    ],
                    "tickets"       => [
                        "name" => "konev",
                        // в секундах, время жизни ключа
                        "ttl"  => 86400, // сутки
                    ],
                ],
        ],
        "components" => [
            // CheckCard - компонент
            'cardVerifier' => [
                'class'       => CardVerifierComponent::class,
                'url'         => 'https://payment.hashconnect.eu',
                'key_private' => $private,
                'key_public'  => $public,
                'uri'         => [
                    'operations' => '/card/{siteId}/operations/{operationId}',
                    'link'       => '/paymentpageapi/{siteId}/create_payment_link',
                    'purchase'   => '/card/{siteId}/operations/purchase',
                    'refund'     => '/card/{siteId}/operations/{operationId}/refund',
                ],
                'site_id'     => '1-9',
                'debug'       => false,
                'https'       => true,
            ],
            'queueMFOSend' => [
                // класс обработчик
                'class'              => RmqBase::class,
                // 10.10.199.30 // RMQ, server address or IP
                'host'               => 'rmq.dev.carmoney.ru',
                // RMQ, vhost виртуальный хост в RMQ для разделения хостов
                'vhost'              => 'MFO_DEV_ZARUBA00',
                // RMQ, порт по умолчанию
                'port'               => 5672,
                // RMQ, логин
                'user'               => 'user',
                // RMQ, пасс
                'password'           => 'user',
                // RMQ, имя очереди
                'queueName'          => 'lk.mfo.Foto',
                // RMQ имя обменника
                'exchangeName'       => 'LK',
                // RMQ, массив routingKeys, на которые будут приходить данные из exchange. Пустой массив, если не предусмотрено получение данных этим объектом.
                'listenRoutingKeys'  => [],
                // RMQ, массив routingKeys на которые высылать ответ этим компонентом.
                'publishRoutingKeys' => ['Foto'],
                // RMQ, тип соединения. в данной реализации используется только direct, с рассылкой по routingKeys !! (['direct', 'fanout', 'topic', 'headers'])
                'exchangeType'       => 'direct',
                // анонимные очереди, выключено в реализации с МФО
                'anonymousQueue'     => false,
                // выводить debug при публикации сообщений
                'verbosePublish'     => false,
                // выводить debug при приеме сообщений
                'verbose'            => false,
            ],
            //компонент интеграции с Firebase Cloud Messaging для отправки push уведомлений
            'fcm'          => [
                'class'  => Client::class,
                //server API Key (you can get it here: https://firebase.google.com/docs/server/setup#prerequisites)
                'apiKey' => 'AAAAfGhM-P4:APA91bGO02BLbJPm5QS4DG4rfEhFzg4KDphwIMpn4ORH3ax5pZB78xZYoyX3q_ddyEe_FmqnyHmOtCxsQE5Z0M1U6wSFuMeGAmIWY9RfvuUKZ23hAPu5R_rIMfR3S9dGI_-mUIbGMaVa',
            ],
            //компонент для интеграции по SOAP протоколу (soap client)
            'soapCMR'      => [
                'class'       => SoapClientComponent::class,
                'url'         => 'http://10.10.100.93/cmr_uat/ws/', //поменять на боевой
                'urlMock'     => 'http://in.carmoneyzaruba.ru/mock/v1/cmr/', //указать свой хост
                'login'       => 'obmen',
                'password'    => 'obmen123',
                'log'         => true,
                'logCategory' => 'soap',
            ],
            'soapCRM'      => [
                'class'       => SoapClientComponent::class,
                'url'         => 'http://10.10.100.93/cmr_uat/ws/', //поменять на боевой
                'urlMock'     => 'http://in.carmoneyzaruba.ru/mock/v1/crm/', //указать свой хост
                'login'       => 'EventLoadUser',
                'password'    => '123',
                'log'         => true,
                'logCategory' => 'soap',
            ],
            'soapSMS'      => [
                'class'       => SoapClientComponent::class,
                'url'         => 'http://192.168.138.30/SMSSND/ws', //поменять на боевой
                'urlMock'     => 'http://in.carmoneyzaruba.ru/mock/v1/sms/', //указать свой хост
                'login'       => 'UserSMS',
                'password'    => 'Ni1vukyf',
                'log'         => true,
                'logCategory' => 'soap',
            ],
            'lcrmMP'       => [
                'class'       => LCRMComponent::class,
                //'url'         => 'https://lcrm.carmoney.ru/api/', //prod
                //'token'       => '0dd667eeeefa1ccd2cfd54a46926cc27ecb00af5', //prod
                'url'         => 'https://lcrm.carmoneyuat.ru/api/', //uat
                'token'       => '098f6bcd4621d373cade4e832627b4f6', //uat
                'target'      => 'registry_mobile_app',
                'logCategory' => 'lcrm.id.mp',
            ],
            'lcrmLKK'      => [
                'class'       => LCRMComponent::class,
                //'url'         => 'https://lcrm.carmoney.ru/api/', //prod
                //'token'       => '494366dcb856e797ee49fef5e2f48ddab3c8e25f', //prod
                'url'         => 'https://lcrm.carmoneyuat.ru/api/', //uat
                'token'       => '098f6bcd4621d373cade4e832627b4f6', //uat
                'target'      => 'registry_lkk',
                'logCategory' => 'lcrm.id.lkk',
            ],
            'cribMP'       => [
                'class'       => CRIBComponent::class,
                //'url'         => 'https://crib.carmoney.ru/api/', //prod
                //'token'       => 'ad58f595cabb4d22c2f7643f495b4bce6225977f', //prod
                'url'         => 'https://crib.carmoneyuat.ru/api/', //uat
                'token'       => '04d5927a35585eebf3f8819b717f27e9ba341ce8', //uat
                'target'      => 'mobile_app',
                'logCategory' => 'crib.id.mp',
            ],
            'cribLKK'      => [
                'class'       => CRIBComponent::class,
                //'url'         => 'https://crib.carmoney.ru/api/', //prod
                //'token'       => 'fb9db0d3fe5490c0ade0cf24f03b1b060f71a4d0', //prod
                'url'         => 'https://crib.carmoneyuat.ru/api/', //uat
                'token'       => '5e938bb0d12b7e43a1087b927ed25413c75f5a05', //uat
                'target'      => 'lk',
                'logCategory' => 'crib.id.lkk',
            ],
        ],
    ],
];