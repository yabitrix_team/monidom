<?php
return [
    //'class'    => 'yii\db\Connection', // old connection
    'class'    => 'app\modules\app\v1\classes\Connection',
	'dsn'      => 'mysql:host=127.0.0.1;dbname=db',
	'username' => 'user',
	'password' => 'password',
	'charset'  => 'utf8',
	// Schema cache options (for production environment)
	//'enableSchemaCache' => true,
	//'schemaCacheDuration' => 60,
	//'schemaCache' => 'cache',
];
