<?php

use yii\log\FileTarget;

$webLogs = [
    [
        'class'      => FileTarget::class,
        'categories' => ['mobile.push'],
        'logFile'    => '@app/runtime/logs/mobile.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['soap'],
        'logFile'    => '@app/runtime/logs/soap.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['exchange.event.202'],
        'logFile'    => '@app/runtime/logs/exchange.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => [
            'user.login',
            'user.change.login',
            'user.logout',
            'user.register.client',
            'user.register.partner',
            'user.register.mobile',
            'user.password',
            'user.link.get',
            'user.link.auth',
            'user.login.attempt',
        ],
        'logFile'    => '@app/runtime/logs/user.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => [
            'sms.confirm.request',
            'sms.confirm.mobile',
            'sms.confirm.client',
            'sms.confirm.reset',
            'sms.confirm.card',
            'sms.lead.client',
            'sms.enterlink.exists',
            'sms.enterlink.new',
            'sms.varification.service',
            'sms.varification.rmq',
        ],
        'logFile'    => '@app/runtime/logs/sms.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['pay.mfo.sms', 'pay.mfo.check', 'pay.cp.check', 'pay.cp.pay', 'pay.cp.fail'],
        'logFile'    => '@app/runtime/logs/pay.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['crm.lead.new', 'crm.events'],
        'logFile'    => '@app/runtime/logs/crm.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['photos.upload'],
        'logFile'    => '@app/runtime/logs/photos.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => [ 'call.lead.create', 'call.lead.update', 'call.lead.request', 'call.lead.delete', 'call.lead.recover' ],
        'logFile'    => '@app/runtime/logs/call.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['in.lcrm', 'in.service.crm', 'in.partner_lead', 'in.service.lead.create'],
        'logFile'    => '@app/runtime/logs/in.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['debug.exception', 'debug.fatal', 'debug.fallback'],
        'logFile'    => '@app/runtime/logs/debug.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['rmq.201'],
        'logFile'    => '@app/runtime/logs/rmq.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['crib.id.mp', 'crib.id.lkk'],
        'logFile'    => '@app/runtime/logs/crib.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['lcrm.id.mp', 'lcrm.id.lkk'],
        'logFile'    => '@app/runtime/logs/lcrm.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['card.verifier.log',],
        'logFile'    => '@app/runtime/logs/card.verifier.log',
    ],
];

return array_map(
    function ($log) {
        $log['levels']         = $log['levels'] ?: ['info', 'error', 'warning'];
        $log['exportInterval'] = $log['exportInterval'] ?: 1;
        $log['logVars']        = $log['logVars'] ?: [];
        $log['microtime']      = $log['microtime'] ?: true;

        return $log;
    },
    $webLogs
);
