<?php
$params = require __DIR__.'/params.php';
$db     = require __DIR__.'/db.php';
$logs   = require __DIR__.'/logs.web.php';
$config = [
    'id'           => 'basic',
    'basePath'     => dirname(__DIR__),
    'bootstrap'    => ['log', 'notification', 'queueMFOSend'],
    'defaultRoute' => "/",
    'language'     => 'ru-RU',
    'modules'      => [
        "app_v1"     => ["class" => 'app\modules\app\v1\Module'],
        "agent_v1"   => ["class" => 'app\modules\agent\v1\Module'],
        "client_v1"  => ["class" => 'app\modules\client\v1\Module'],
        "mp_v1"      => ["class" => 'app\modules\mp\v1\Module'],
        "mp_v2"      => ["class" => 'app\modules\mp\v2\Module'],
        "web_v1"     => ["class" => 'app\modules\web\v1\Module'],
        "in_v1"      => ["class" => 'app\modules\in\v1\Module'],
        "payment_v1" => ["class" => 'app\modules\payment\v1\Module'],
        "admin_v1"   => ["class" => 'app\modules\admin\v1\Module'],
        "mrp_v1"     => ["class" => 'app\modules\mrp\v1\Module'],
        "panel_v1"   => ["class" => 'app\modules\panel\v1\Module'],
        "lead_v1"    => ["class" => 'app\modules\lead\v1\Module'],
        "call_v1"    => ["class" => 'app\modules\call\v1\Module'],
        //		'exchange'     => [ 'class' => 'app\modules\exchange\Module' ],
        //		'payment'      => [ 'class' => 'app\modules\payment\v1\Module' ],
        //		'panel'        => [ 'class' => 'app\modules\panel\v1\Module' ],
    ],
    'aliases'      => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'   => [
        'request'           => [
            'enableCookieValidation' => true,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '4OBW3lrwOKpgy6ukO7WpagUdqflEP56Z',
        ],
        'formatter'         => [
            'class'    => 'yii\i18n\Formatter',
            'timeZone' => 'UTC',
        ],
        'response'          => [
            'class'         => 'yii\web\Response',
            'format'        => yii\web\Response::FORMAT_JSON,
            'formatters'    => [
                'json' => [
                    'class'         => 'yii\web\JsonResponseFormatter',
                    'prettyPrint'   => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
            'on beforeSend' =>
                function ($event) use ($params) {

                    $value = !Yii::$app->request->cookies->has("sess")
                        ? md5(time().Yii::$app->security->generateRandomString())
                        : Yii::$app->request->cookies->getValue("sess");
                    Yii::$app->response->cookies->add(
                        new \yii\web\Cookie(
                            [
                                "name"   => "sess",
                                "value"  => $value,
                                "domain" => '.'.$params['core']['domain'],
                                "expire" => time() + 3600,
                            ]
                        )
                    );
                },
        ],
        'cache'             => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'localhost',
                'port'     => 6379,
                'database' => 0,
            ],
        ],
        'user'              => [
            'identityClass'   => 'app\modules\app\v1\models\Users',
            'enableAutoLogin' => true,
            'enableSession'   => true,
            'identityCookie'  => [
                'name'     => '_identity',
                'httpOnly' => true,
                'domain'   => '.'.$params['core']['domain'],
            ],
            'loginUrl'        => 'http://login'.'.'.$params['core']['domain'],
        ],
        'mailer'            => [
            'class'            => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'db'                => $db,
        'urlManager'        => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'rules'               => [
                // редирект на W1 проверка карты
                'GET /cvr/<code:\w{32}>'                                                                                                                  => 'in_v1/service/card-verifier/redirect',
                'GET /<module:\w+>/<version:v\d+>/<_c:[\w-]+>/<_a:(params|cloud|w1)>'                                                                     => '<module>_<version>/<_c>/<_a>',
                'GET /doc/<code:\w{32}>'                                                                                                                  => 'web_v1/request/file/doc-by-code',
                'GET /doc/zip/<num:(1|2)>/<code:\w{32}>'                                                                                                  => 'web_v1/request/file/zip-by-code',
                'GET /doc/accreditation/<code:\w{32}>'                                                                                                    => 'web_v1/request/file/accreditation-by-code',
                'GET /<module:\w+>/<version:v\d+>'                                                                                                        => '<module>_<version>',
                //--- todo[echmaster]: Временное правило для переезда со старого АПИ на новое реализовано для МП, после переезда удалим
                'GET /<module:app>/<version:v\d+>/<dir:info>'                                                                                             => '<module>_<version>/<dir>/old-api',
                //--- получение определенного активного договора
                'GET /<module:\w+>/<version:v\d+>/<dir:agreements>/<_c:active>/<code:[\w]+>'                                                              => '<module>_<version>/<dir>/<_c>/by-code',
                //---
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<code:\w{32}>/<_a:notice>/<id:\d+>'                                                        => '<module>_<version>/<dir>/<dir>/<_a>',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<code:\w{32}>'                                                                 => '<module>_<version>/<dir>/<_c>/by-code',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<login:\d{10}>'                                                                => '<module>_<version>/<dir>/<_c>/by-login',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:print_link>/<code:\w{8}>'                                                              => '<module>_<version>/<dir>/<_c>/by-code',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<id:\d+>'                                                                      => '<module>_<version>/<dir>/<_c>/by-id',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<code:\w{32}>'                                                                             => '<module>_<version>/<dir>/<dir>/by-code',
                'GET /<module:\w+>/<version:v\d+>/<dir:lead>/<code:\w{8}>'                                                                                => '<module>_<version>/<dir>/<dir>/by-code',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<guid:[\w-]{36}>'                                                                          => '<module>_<version>/<dir>/<dir>/by-guid',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<_a:preview|doc>/<code:\w{32}>'                                                => '<module>_<version>/<dir>/<_c>/<_a>-by-code',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<id:\d+>'                                                                                  => '<module>_<version>/<dir>/<dir>/by-id',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>'                                                                               => '<module>_<version>/<dir>/<_c>',
                'GET /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/'                                                                                          => '<module>_<version>/<dir>/<dir>',
                'POST /<module:\w+>/<version:v\d+>/<dir:notification>/<_a:send>'                                                                          => '<module>_<version>/<dir>/<dir>/<_a>',
                // W1 проверка карты
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<_a:[\w-]+>/<code:\w{32}>'                                                => '<module>_<version>/<dir>/<_c>/<_a>',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_a:verification|is-old-files|delete-old-files|check-and-delete-old-files>/<code:\w{32}>' => '<module>_<version>/<dir>/<dir>/<_a>',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:verification>/'                                                                       => '<module>_<version>/<dir>/<_c>/create',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<login:\d{10}>'                                                               => '<module>_<version>/<dir>/<_c>/by-login',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:sms>/<code:\d+>'                                                                      => '<module>_<version>/<dir>/<_c>/check',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<code:\w{32}>'                                                                => '<module>_<version>/<dir>/<_c>/update',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<code:\w{32}>'                                                                            => '<module>_<version>/<dir>/<dir>/update',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<username:\d{10}>'                                                                        => '<module>_<version>/<dir>/<dir>/update',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_a:verifysms>'                                                                           => '<module>_<version>/<dir>/user/verifysms',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_a:verify>'                                                                              => '<module>_<version>/<dir>/user/verify',
                'POST /<module:\w+>/<version:v\d+>/client/'                                                                                               => '<module>_<version>/request/client',
                'POST /<module:\w+>/<version:v\d+>/client/<_a:[\w-]+>'                                                                                    => '<module>_<version>/request/client/<_a>',
                'POST /<module:\w+>/<version:v\d+>/history/'                                                                                              => '<module>_<version>/request/history',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>'                                                                              => '<module>_<version>/<dir>/<_c>/create',
                //'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<guid>'                        => '<module>_<version>/<dir>/<_c>/create',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<_a:(pdf|new)>'                                                               => '<module>_<version>/<dir>/<_c>/<_a>',
                'POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/'                                                                                         => '<module>_<version>/<dir>/<dir>/create',
                'POST /<module:\w+>/<version:v\d+>/<dir1:[\w-]+>/<dir2:[\w-]+>/<_c:[\w-]+>/<_a:[\w-]+>'                                                   => '<module>_<version>/<dir1>/<dir2>/<_c>/<_a>',
                'DELETE /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<code:\w{32}>/<_a:notice>/<id:\d+>'                                                     => '<module>_<version>/<dir>/<dir>/<_a>',
                'DELETE /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<code:\w{32}>'                                                              => '<module>_<version>/<dir>/<_c>/delete',
                'DELETE /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<code:\w{32}>'                                                                          => '<module>_<version>/<dir>/<dir>/delete',
                'DELETE /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>'                                                                            => '<module>_<version>/<dir>/<_c>/delete',
                '/<module:\w+>/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<_a:[\w-]+>'                                                                       => '<module>_<version>/<dir>/<_c>/<_a>',
                // for frontend routing
                'site/<dir:[\w-]+>'                                                                                                                       => 'site/<dir>',
            ],
        ],
        'session'           => [
            'class'        => 'yii\web\DbSession',
            'db'           => $db,
            'sessionTable' => 'sessions',
            'cookieParams' => [
                'domain'   => '.'.$params['core']['domain'],
                'httpOnly' => true,
            ],
            //'name' => 'PHPBACKSESSID',
        ],
        'authManager'       => [
            'class'        => 'yii\rbac\DbManager',
            'defaultRoles' => [
                'role_partner_admin',
                'role_agent',
                'role_client',
                'role_invest',
                'role_user_partner',
                'role_mobile_application',
                'role_support',
                'role_admin',
                'role_lead_agent',
                'role_call',
            ],
        ],
        'log'               => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => $logs,
        ],
        'file'              => [
            'class' => 'app\modules\app\v1\components\FileComponent',
        ],
        'accreditationFile' => [
            'class' => 'app\modules\app\v1\components\AccreditationFileComponent',
        ],
        'msg'               => [
            'class' => 'app\modules\app\v1\components\MsgComponent',
            'path'  => 'message/error.php',
        ],
        'errorHandler'      => [
            'class' => 'app\modules\app\v1\components\ErrorComponent',
        ],
    ],
    'params'       => $params,
];
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][]      = 'gii';
    $config['modules']['gii']   = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //		'allowedIPs' => [ '127.0.0.1', '::1', $_SERVER['REMOTE_ADDR'] ],
    ];
    //поключение имитации сторонних сервисов(mock)
    if (!empty($params['core']['isMock'])) {
        //подключение модуля
        $config['modules']['mock_v1'] = ["class" => 'app\modules\mock\v1\Module'];
        //правила url для сервисов имитации
        $config['components']['urlManager']['rules'] = array_merge(
            [
                'GET /module:(mock)/<version:v\d+>/<dir:[\w-]+>/<_c:[\w-]+>/<phone:\d{10}>'       => '<module>_<version>/<dir>/<_c>/by-phone',
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/AgreementsData<ext:(.wsdl)?>' => '<module>_<version>/<dir>/agreements-data',
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/Annuity<ext:(.wsdl)?>'        => '<module>_<version>/<dir>/annuity',
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/CMRService<ext:(.wsdl)?>'     => '<module>_<version>/<dir>/cmr-service',
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/EventsLoad<ext:(.wsdl)?>'     => '<module>_<version>/<dir>/events-load',
                'GET,POST /<module:\w+>/<version:v\d+>/<dir:[\w-]+>/SendingSMS<ext:(.wsdl)?>'     => '<module>_<version>/<dir>/sending-sms',
            ],
            $config['components']['urlManager']['rules']
        );
    }
}
//подключение доп. компонентов, параметры которых заводим в params.php
if (array_key_exists("web_params", $params)) {
    foreach ($params['web_params'] as $keyParam => $valueParam) {
        if (!empty($valueParam)) {
            $config[$keyParam] = array_merge(($config[$keyParam] ?: []), $valueParam);
        }
    }
}

return $config;
