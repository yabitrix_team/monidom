<?php

use yii\log\FileTarget;

$consoleLogs = [
    [
        'class'      => FileTarget::class,
        'categories' => ['sms.varification.rmq'],
        'logFile'    => '@app/runtime/logs/sms.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['mobile.push'],
        'logFile'    => '@app/runtime/logs/mobile.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['soap'],
        'logFile'    => '@app/runtime/logs/soap.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['exchange.commission', 'exchange.event.202', 'exchange.status.delete', 'exchange.photos'],
        'logFile'    => '@app/runtime/logs/exchange.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['user.exchange.register', 'user.exchange.update', 'user.exchange.block'],
        'logFile'    => '@app/runtime/logs/user.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['pay.cp.exchange'],
        'logFile'    => '@app/runtime/logs/pay.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['debug.exception', 'debug.fatal', 'debug.fallback'],
        'logFile'    => '@app/runtime/logs/debug.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['card.verifier.log',],
        'logFile'    => '@app/runtime/logs/check.card.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['rmq.*'],
        'logFile'    => '@app/runtime/logs/rmq.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['crib.id.mp', 'crib.id.lkk'],
        'logFile'    => '@app/runtime/logs/crib.log',
    ],
    [
        'class'      => FileTarget::class,
        'categories' => ['lcrm.id.mp', 'lcrm.id.lkk'],
        'logFile'    => '@app/runtime/logs/lcrm.log',
    ],
];

return array_map(
    function ($log) {
        $log['levels']         = $log['levels'] ?: ['info', 'error', 'warning'];
        $log['exportInterval'] = $log['exportInterval'] ?: 1;
        $log['logVars']        = $log['logVars'] ?: [];
        $log['microtime']      = $log['microtime'] ?: true;

        return $log;
    },
    $consoleLogs
);