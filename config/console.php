<?php
$params = require __DIR__ . '/params.php';
$db     = require __DIR__ . '/db.php';
$tdb    = require __DIR__ . '/tdb.php';
$logs   = require __DIR__ . '/logs.console.php';
$config = [
	'id'                  => 'basic-console',
	'basePath'            => dirname( __DIR__ ),
	'bootstrap'           => [ 'log', 'notification', 'exchange', 'tools', 'rmq', 'queueMFOReceive', 'queueMFOSend' ],
	'controllerNamespace' => 'app\commands',
	'components'          => [
		'cache'       => [
			'class' => 'yii\redis\Cache',
			'redis' => [
				'hostname' => 'localhost',
				'port'     => 6379,
				'database' => 0,
			],
		],
		'log'         => [
			'traceLevel'    => YII_DEBUG ? 3 : 0,
			'flushInterval' => 1,
			'targets'       => $logs,
		],
		'db'          => $db,
		'tdb'         => array_merge( $tdb, [
			'attributes' => [
				PDO::ATTR_PERSISTENT => true,
			],
		] ),
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],
		'file' => [
			'class' => 'app\modules\app\v1\components\FileComponent',
		],
        'accreditationFile' => [
            'class' => 'app\modules\app\v1\components\AccreditationFileComponent',
        ],
		'pdb' => array_merge( $db, [
			'attributes' => [
				PDO::ATTR_PERSISTENT => true,
			],
		] ),
	],
	'modules'             => [
		'exchange' => [
			'class' => 'app\modules\exchange\Module',
		],
		'payment'  => [
			'class' => 'app\modules\payment\v1\Module',
		],
		'tools'    => [
			'class' => 'app\modules\app\v1\Module',
		],
        'rmq' => [
            'class' => 'app\modules\rmq\Module',
        ],
	],
	'params'              => $params,
];
if ( YII_ENV_DEV ) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
	];
}
if ( array_key_exists( "console_params", $params ) ) {
	foreach ( $params['console_params'] as $keyParam => $valueParam ) {
		if ( ! empty( $valueParam ) ) {
			$config[ $keyParam ] = array_merge( ( $config[ $keyParam ] ?: [] ), $valueParam );
		}
	}
}

return $config;
