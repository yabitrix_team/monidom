<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lock_order`.
 */
class m180201_122712_create_lock_order_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'lock_order', [
			'id'          => $this->primaryKey(),
			'request_id'  => $this->integer( 11 )->notNull(),
			'user_id'     => $this->integer( 11 )->notNull(),
			'blocked_at'  => $this->datetime()->notNull(),
			'free_period' => $this->integer( 11 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'lock_order' );
	}
}
