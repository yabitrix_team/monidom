<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_commission`.
 */
class m180201_133744_create_order_commission_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'order_commission', [
			'id'                         => $this->primaryKey(),
			'guid'                       => $this->char( 36 )->notNull()->unique(),
			'name'                       => $this->string( 255 )->notNull(),
			'order_id'                   => $this->integer( 11 )->notNull(),
			'is_null'                    => $this->char( 1 )->notNull(),
			'commission_contract'        => $this->string( 255 )->notNull(),
			'commission_cash_withdrawal' => $this->string( 255 )->notNull(),
			'monthly_fee'                => $this->integer( 11 )->notNull(),
			'received'                   => $this->integer( 11 )->notNull(),
			'created_at'                 => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'                 => $this->datetime()->notNull()
			                                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'                     => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'                       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'order_commission' );
	}
}
