<?php

use yii\db\Migration;

/**
 * Handles adding code to table `message`.
 */
class m180411_132304_add_code_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'message', 'code', 'VARCHAR(32) NOT NULL AFTER id' );
		$this->addCommentOnColumn( 'message', 'code', 'Символьный код длинной 32 символа (MD5). Уникальные значения.' );
		$this->createIndex( 'code', 'message', 'code', true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'code', 'message' );
		$this->dropCommentFromColumn( 'message', 'code' );
		$this->dropColumn( 'message', 'code' );
	}
}
