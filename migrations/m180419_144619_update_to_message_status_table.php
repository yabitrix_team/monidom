<?php

use yii\db\Migration;

/**
 * Class m180419_144619_update_to_message_status_table
 */
class m180419_144619_update_to_message_status_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'message_status', [
			'is_default' => "Y",
		], [
			               "name" => "new",
		               ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'message_status', [
			'is_default' => "N",
		], [
			               "name" => "new",
		               ] );
	}
}
