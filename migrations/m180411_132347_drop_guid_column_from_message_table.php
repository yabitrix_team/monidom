<?php

use yii\db\Migration;

/**
 * Handles dropping guid from table `message`.
 */
class m180411_132347_drop_guid_column_from_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->dropIndex( 'guid', 'message' );
		$this->dropColumn( 'message', 'guid' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->addColumn( 'message', 'guid', 'VARCHAR(36) NOT NULL AFTER id' );
		$this->addCommentOnColumn( 'message', 'code', 'ГУИД' );
		$this->createIndex( 'guid', 'message', 'guid', true );
	}
}
