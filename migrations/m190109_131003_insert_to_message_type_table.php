<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m190109_131003_insert_to_message_type_table
 */
class m190109_131003_insert_to_message_type_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function safeUp() {

        $this->batchInsert( 'message_type',
            [ 'guid', 'name', 'active', 'public' ],
            [
                [ Uuid::uuid4(), 'cansignature', 1, 1 ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {

        $this->execute( "SET foreign_key_checks = 0;" );
        $this->delete( 'message_type', [
            'name' => 'cansignature',
        ] );
        $this->execute( "SET foreign_key_checks = 1;" );
    }
}
