<?php

use yii\db\Migration;

/**
 * Class m180404_093840_add_foreign_key_for_exchange_name_exchange_type_exchange_stage_id_order_to_pcm_log_table
 */
class m180404_093840_add_foreign_key_for_exchange_name_exchange_type_exchange_stage_id_order_to_pcm_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-pcm_log-exchange_name',  // это "условное имя" ключа
			'pcm_log', // это название текущей таблицы
			'exchange_name', // это имя поля в текущей таблице, которое будет ключом
			'pcm_log_exchange_name', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-pcm_log-exchange_type',  // это "условное имя" ключа
			'pcm_log', // это название текущей таблицы
			'exchange_type', // это имя поля в текущей таблице, которое будет ключом
			'pcm_log_error_type', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-pcm_log-exchange_stage',  // это "условное имя" ключа
			'pcm_log', // это название текущей таблицы
			'exchange_stage', // это имя поля в текущей таблице, которое будет ключом
			'pcm_log_exchange_name', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-pcm_log-order_id',  // это "условное имя" ключа
			'pcm_log', // это название текущей таблицы
			'order_id', // это имя поля в текущей таблице, которое будет ключом
			'pcm_log_order', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-pcm_log-exchange_name',
			'pcm_log'
		);
		$this->dropForeignKey(
			'fk-pcm_log-exchange_type',
			'pcm_log'
		);
		$this->dropForeignKey(
			'fk-pcm_log-exchange_stage',
			'pcm_log'
		);
		$this->dropForeignKey(
			'fk-pcm_log-order_id',
			'pcm_log'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_093840_add_foreign_key_for_exchange_name_exchange_type_exchange_stage_id_order_to_pcm_log_table cannot be reverted.\n";

		return false;
	}
	*/
}
