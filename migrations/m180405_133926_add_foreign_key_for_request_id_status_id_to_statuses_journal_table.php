<?php

use yii\db\Migration;

/**
 * Class m180405_133926_add_foreign_key_for_request_id_status_id_to_statuses_journal_table
 */
class m180405_133926_add_foreign_key_for_request_id_status_id_to_statuses_journal_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-statuses_journal-request_id',  // это "условное имя" ключа
			'statuses_journal', // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-statuses_journal-status_id',  // это "условное имя" ключа
			'statuses_journal', // это название текущей таблицы
			'status_id', // это имя поля в текущей таблице, которое будет ключом
			'statuses', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-statuses_journal-request_id',
			'statuses_journal'
		);
		$this->dropIndex(
			'fk-statuses_journal-request_id',
			'statuses_journal'
		);
		$this->dropForeignKey(
			'fk-statuses_journal-status_id',
			'statuses_journal'
		);
		$this->dropIndex(
			'fk-statuses_journal-status_id',
			'statuses_journal'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180405_133926_add_foreign_key_for_request_id_status_id_to_statuses_journal_table cannot be reverted.\n";

		return false;
	}
	*/
}
