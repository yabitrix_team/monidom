<?php

use yii\db\Migration;

/**
 * Handles the creation of table `document`.
 */
class m180802_151308_create_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documents', [
            'id' => $this->primaryKey(),
            'code'    => $this->string()->notNull()->unique(),
            'label'     => $this->string(),
            'html' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('documents');
    }
}
