<?php

use yii\db\Migration;

/**
 * Class m180307_133300_rename_column_from_id_order_to_order_id_in_pcm_log_table
 */
class m180307_133300_rename_column_from_id_order_to_order_id_in_pcm_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->renameColumn( 'pcm_log', 'id_order', 'order_id' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->renameColumn( 'pcm_log', 'order_id', 'id_order' );
	}
}
