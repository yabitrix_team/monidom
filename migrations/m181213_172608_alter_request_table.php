<?php

use yii\db\Migration;

/**
 * Class m181213_172608_alter_request_table
 */
class m181213_172608_alter_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requests', 'card_token', $this->string()->after('card_holder'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('requests', 'card_token');
    }
}
