<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_order`.
 */
class m180202_093155_create_pcm_log_order_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_order', [
			'id'     => $this->primaryKey(),
			'id_xml' => $this->char( 36 )->notNull(),
			'id_1c'  => $this->string( 255 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_order' );
	}
}
