<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180419_145642_insert_to_message_type_table
 */
class m180419_145642_insert_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->insert( 'message_type', [
			"guid"   => Uuid::uuid4(),
			"name"   => "test",
			"active" => 1,
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( "message_type", [
			"name" => "test",
		] );
	}
}
