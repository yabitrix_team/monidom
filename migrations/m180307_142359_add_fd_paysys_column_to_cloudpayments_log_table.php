<?php

use yii\db\Migration;

/**
 * Handles adding fd_paysys to table `cloudpayments_log`.
 */
class m180307_142359_add_fd_paysys_column_to_cloudpayments_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->addColumn( 'cloudpayments_log', 'fd_paysys', 'varchar(255) not null' );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropColumn( 'cloudpayments_log', 'fd_paysys' );
	}
}
