
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `com_offer`.
 */
class m180704_123537_create_com_offer_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('com_offer', [
			'id' => $this->primaryKey(),
			'code'=>$this->string(),
			'name'=>$this->string(),
			'active'=>$this->tinyInteger(1),
			'sort'=>$this->smallInteger(3),
		]);
		$this->batchInsert( 'com_offer', [ 'code', 'name' , 'active', 'sort'], [
			[ '000000095', 'Бизнес займ от 500 000 до 999 999 12 мес', '1', '110'],
			[ '000000096', 'Бизнес займ от 500 000 до 999 999 24 мес', '1', '120'],
			[ '000000097', 'Бизнес займ от 500 000 до 999 999 36 мес', '1', '130'],
			[ '000000098', 'Бизнес займ от 1 000 000 до 3 000 000 12 мес', '1', '140'],
			[ '000000099', 'Бизнес займ от 1 000 000 до 3 000 000 24 мес', '1', '150'],
			[ '000000100', 'Бизнес займ от 1 000 000 до 3 000 000 36 мес', '1', '160'],

		] );

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('com_offer');
	}
}
