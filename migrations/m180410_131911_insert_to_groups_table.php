<?php

use yii\db\Migration;

/**
 * Class m180410_131911_insert_to_groups_table
 */
class m180410_131911_insert_to_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->batchInsert("groups", ["guid", "name"], [
			["46b775b8-d8c0-11e7-814a-00155d01bf20", "Администраторы партнера"],
			["46b775b8-d8c0-11e7-814a-00155d01bf21", "Пользователи партнера"],
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->delete("groups", "guid = '46b775b8-d8c0-11e7-814a-00155d01bf20'");
	    $this->delete("groups", "guid = '46b775b8-d8c0-11e7-814a-00155d01bf21'");
    }
}
