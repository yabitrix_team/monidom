<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners`.
 */
class m180131_112819_create_partners_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'partners', [
			'id'                   => $this->primaryKey(),
			'guid'                 => $this->char( 36 )->notNull()->unique(),
			'name'                 => $this->string( 255 )->notNull(),
			'comission_percentage' => $this->integer( 11 )->notNull(),
			'comission_summ'       => $this->integer( 11 )->notNull(),
			'created_at'           => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'           => $this->datetime()->notNull()
			                               ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'               => $this->char( 1 )->notNull()->defaultValue( 'Y' ),
			'sort'                 => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'partners' );
	}
}
