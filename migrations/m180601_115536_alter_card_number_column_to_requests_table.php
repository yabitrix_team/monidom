<?php

use yii\db\Migration;

/**
 * Class m180601_115536_alter_card_number_column_to_requests_table
 */
class m180601_115536_alter_card_number_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'card_number', 'varchar(20) null' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'card_number', 'int(11) null' );
	}
}
