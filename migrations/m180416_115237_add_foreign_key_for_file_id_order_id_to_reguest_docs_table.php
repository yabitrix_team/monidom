<?php

use yii\db\Migration;

/**
 * Class m180404_115237_add_foreign_key_for_exchange_name_file_id_order_id_to_reguest_docs_table
 */
class m180416_115237_add_foreign_key_for_file_id_order_id_to_reguest_docs_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-request_file-order_id',  // это "условное имя" ключа
			'request_file', // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-request_file-file_id',  // это "условное имя" ключа
			'request_file', // это название текущей таблицы
			'file_id', // это имя поля в текущей таблице, которое будет ключом
			'file', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-request_file-file_id',
			'request_file'
		);
		$this->dropForeignKey(
			'fk-request_file-order_id',
			'request_file'
		);
		$this->dropIndex(
			'fk-request_file-file_id',
			'request_file'
		);
		$this->dropIndex(
			'fk-request_file-order_id',
			'request_file'
		);
	}
}
