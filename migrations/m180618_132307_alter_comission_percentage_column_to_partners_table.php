<?php

use yii\db\Migration;

/**
 * Class m180618_132307_alter_comission_percentage_column_to_partners_table
 */
class m180618_132307_alter_comission_percentage_column_to_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->alterColumn('partners', 'comission_percentage', $this->decimal(4, 2)->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn('partners', 'comission_percentage', $this->integer( 11 )->notNull());
    }


}
