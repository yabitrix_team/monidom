<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requests_origin`.
 */
class m180419_123814_create_requests_origin_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'requests_origin', [
			'id'         => $this->primaryKey(),
			'name_1c'    => $this->string( 255 ),
			'identifier'  => $this->string( 255 ),
			'updated_at' => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ),
			'created_at' => $this->datetime()->defaultExpression( 'CURRENT_TIMESTAMP' ),
			'active'     => $this->tinyInteger( 1 )->defaultValue( 1 ),
			'sort'       => $this->integer( 11 )->defaultValue( 1000 ),
		] );
		$this->createIndex( 'name_1c', 'requests_origin', 'name_1c', true );
		$this->createIndex( 'identifier', 'requests_origin', 'identifier', true );
		$this->batchInsert( 'requests_origin', [ 'name_1c', 'identifier' ], [
			[ 'МобильноеПриложение', 'MOBILE' ],
			[ 'ЛКПартнера', 'PARTNER' ],
			[ 'КоллЦентр', 'CALL' ],
			[ 'СтороннийКЦ', 'SIDES_CALL' ],
		] );
		$this->addCommentOnTable( 'requests_origin', 'Таблица содержит связи для места создания заявки относительно 1С и бэка. Идентификаторы данной таблицы подставлюятся в заявку в поле requests_origin_id при создании заявки и при обмене.' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropCommentFromTable( 'requests_origin' );
		$this->dropIndex( 'name_1c', 'requests_origin' );
		$this->dropIndex( 'identifier', 'requests_origin' );
		$this->truncateTable( 'requests_origin' );
		$this->dropTable( 'requests_origin' );
	}
}
