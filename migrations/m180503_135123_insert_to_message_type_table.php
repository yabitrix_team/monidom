<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180503_135123_insert_to_message_type_table
 */
class m180503_135123_insert_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'message_type',
		                    [ 'guid', 'name', 'active', 'public' ],
		                    [
			                    [ Uuid::uuid4(), 'firstpack', 1, 1 ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->delete( 'message_type', [
			'name' => 'firstpack',
		] );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
