<?php

use yii\db\Migration;

/**
 * Class m181212_112900_alter_blocked_column_to_request_mp_table
 */
class m181212_112900_alter_blocked_column_to_request_mp_table extends Migration
{
    /**
     * Константа блокировки по клиенту
     */
    CONST CLIENT = 'client';
    /**
     * Константа блокировки по партнеру
     */
    CONST PARTNER = 'partner';
    /**
     * @var string - название таблицы
     */
    protected $table = 'request_mp';

    /**
     * {@inheritdoc}
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $arId = $this->getIdBlocking();
        $this->renameColumn($this->table, 'blocked', 'blocked_by');
        $this->alterColumn($this->table, 'blocked_by', 'ENUM("'.self::CLIENT.'", "'.self::PARTNER.'")');
        if (!empty($arId)) {
            if (!empty($arId['blocked'])) {
                $strBlocked = '"'.implode('","', $arId['blocked']).'"';
                $this->update($this->table, ['blocked_by' => self::CLIENT], '[[id]] IN ('.$strBlocked.')');
            }
            if (!empty($arId['not_blocked'])) {
                $strNotBlocked = '"'.implode('","', $arId['not_blocked']).'"';
                $this->update($this->table, ['blocked_by' => null], '[[id]] IN ('.$strNotBlocked.')');
            }
        }
        $this->addCommentOnColumn(
            $this->table,
            'blocked_by',
            'Параметр указывающий на того, кем была заблокирована заявка'
        );
    }

    /**
     * {@inheritdoc}
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $this->renameColumn($this->table, 'blocked_by', 'blocked');
        $arId = $this->getIdBlocking();
        $this->alterColumn($this->table, 'blocked', $this->tinyInteger(1)->defaultValue(0));
        if (!empty($arId)) {
            if (!empty($arId['blocked'])) {
                $strBlocked = '"'.implode('","', $arId['blocked']).'"';
                $this->update($this->table, ['blocked' => 1], '[[id]] IN ('.$strBlocked.')');
            }
            if (!empty($arId['not_blocked'])) {
                $strNotBlocked = '"'.implode('","', $arId['not_blocked']).'"';
                $this->update($this->table, ['blocked' => 0], '[[id]] IN ('.$strNotBlocked.')');
            }
        }
        $this->addCommentOnColumn($this->table, 'blocked', 'Флаг, отвечающий за блокировку заявки в МП');
    }

    /**
     * Метод получения идентификаторов заблокированных и не заблокированных заявок
     *
     * @return array - вернет массив вида
     *               [
     *                  not_blocked => [4,5,6]
     *                  blocked => [1,2,3,7]
     *               ]
     * @throws \yii\db\Exception
     */
    protected function getIdBlocking()
    {
        $arId = [];
        $res  = Yii::$app->db->createCommand(
            '
        SELECT [[id]], [[blocked]] FROM '.$this->table
        )->queryAll();
        if (!empty($res) && is_array($res)) {
            foreach ($res as $item) {
                $key          = empty($item['blocked']) || $item['blocked'] == self::PARTNER
                    ? 'not_blocked'
                    : 'blocked';
                $arId[$key][] = $item['id'];
            }
        }

        return $arId;
    }
}
