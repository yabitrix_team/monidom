<?php

use yii\db\Migration;

/**
 * Class m180329_150758_rename_api_versions_dictionaries_table
 */
class m180329_150758_rename_api_versions_dictionaries_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'api_versions_dictionaries', 'bx_id', 'table_name' );
		$this->renameTable( 'api_versions_dictionaries', 'versions_dictionaries' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'versions_dictionaries', 'table_name', 'bx_id' );
		$this->renameTable( 'versions_dictionaries', 'api_versions_dictionaries' );
	}
}
