<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_attributes`.
 */
class m180924_160703_create_message_attributes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message_attributes', [
            'id' => $this->primaryKey(),
            'message_id'=>$this->integer(),
            'kk_id'=>$this->string()->null(),
            'doc_type'=>$this->string()->null(),
            'rmq_data_packet_id'=>$this->integer()->null()
        ]);
	    $this->addForeignKey(
		    'fk-message_attributes-message_id',  // это 'условное имя' ключа
		    'message_attributes', // это название текущей таблицы
		    'message_id', // это имя поля в текущей таблице, которое будет ключом
		    'message', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->execute( "SET foreign_key_checks = 0;" );
        $this->dropTable('message_attributes');
	    $this->execute( "SET foreign_key_checks = 1;" );
    }
}
