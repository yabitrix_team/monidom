<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_attache_file`.
 */
class m180201_153251_create_message_attache_file_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'message_attache_file', [
			'id'         => $this->primaryKey(),
			'message_id' => $this->integer( 11 )->notNull(),
			'file_id'    => $this->integer( 11 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'message_attache_file' );
	}
}
