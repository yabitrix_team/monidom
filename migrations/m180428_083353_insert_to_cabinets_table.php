<?php

use yii\db\Migration;

/**
 * Class m180428_083353_insert_to_cabinets_table
 */
class m180428_083353_insert_to_cabinets_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->insert( 'cabinets', [
			"code"       => 'eaadb3195d7bb1ba2a7f568f556a642b',
			"name"       => 'ЛКАП',
			"subdomain"  => 'partner-admin',
			"created_at" => date( 'Y-m-d H:i:s', time() ),
			"active"     => 1,
			"sort"       => 100,
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'cabinets', [ 'code' => 'eaadb3195d7bb1ba2a7f568f556a642b' ] );
	}
}
