<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner_leads_statuses`.
 */
class m180907_062631_create_partner_leads_statuses_table extends Migration
{

    protected $table_name = 'partner_leads_statuses';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id'   => $this->primaryKey(),
            'name' => $this->string( 50 )->notNull(),
            'guid' => $this->char( 36 )->notNull()->unique(),
        ]);

        $this->batchInsert( $this->table_name, [ 'name', 'guid'], [
            [ 'Лид не уникальный',         'da51eeb5-b1c8-11e8-8106-00155dc83d04'],
            [ 'Лид принят',                'da51eeb6-b1c8-11e8-8106-00155dc83d04'],
            [ 'Отказано',                  'da51eeb8-b1c8-11e8-8106-00155dc83d04'],
            [ 'Отказ клиента',             'da51eeb7-b1c8-11e8-8106-00155dc83d04'],
            [ 'Предварительное одобрение', 'da51eeb9-b1c8-11e8-8106-00155dc83d04'],
            [ 'Заем выдан',                'da51eeb4-b1c8-11e8-8106-00155dc83d04'],
            [ 'Аннулировано по сроку',     'da51eeb3-b1c8-11e8-8106-00155dc83d04'],
        ] );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }
}
