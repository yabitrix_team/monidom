<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employments`.
 */
class m180201_113451_create_employments_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'employments', [
			'id'         => $this->primaryKey(),
			'guid'       => $this->char( 36 )->notNull()->unique(),
			'name'       => $this->string( 255 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( 1 ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'employments' );
	}
}
