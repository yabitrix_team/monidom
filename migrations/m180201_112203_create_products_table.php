<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180201_112203_create_products_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'products', [
			'id'                 => $this->primaryKey(),
			'guid'               => $this->char( 36 )->notNull()->unique(),
			'name'               => $this->string( 255 )->notNull(),
			'is_additional_loan' => $this->string( 255 )->notNull(),
			'percent_per_day'    => $this->string( 255 )->notNull(),
			'month_num'          => $this->integer( 2 )->notNull(),
			'summ_min'           => $this->integer( 11 )->notNull(),
			'summ_max'           => $this->integer( 11 )->notNull(),
			'age_min'            => $this->integer( 2 )->notNull(),
			'age_max'            => $this->integer( 3 )->notNull(),
			'loan_period_min'    => $this->integer( 3 )->notNull(),
			'loan_period_max'    => $this->integer( 3 )->notNull(),
			'created_at'         => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'         => $this->datetime()->notNull()
			                             ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'             => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'               => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'products' );
	}
}
