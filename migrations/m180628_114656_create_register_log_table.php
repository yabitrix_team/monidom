<?php

use yii\db\Migration;

/**
 * Handles the creation of table `register_log`.
 */
class m180628_114656_create_register_log_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'register_log', [
			'id'              => $this->primaryKey(),
			'code'            => $this->string(),
			'user_id'         => $this->integer(),
			'login'           => $this->string(),
			'first_name'      => $this->string(),
			'last_name'       => $this->string(),
			'patronymic'      => $this->string(),
			'passport_serial' => $this->integer(),
			'passport_number' => $this->integer(),
			'sms_vendor'      => $this->string(),
			'user_agent'      => $this->string(),
			'document_type'   => $this->string(),
			'key_code'        => $this->smallInteger(),
			'key_sent'        => $this->tinyInteger(),
			'key_delivered'   => $this->tinyInteger(),
			'key_accepted'    => $this->tinyInteger(),
			'key_signed'      => $this->tinyInteger(),
			'created_at'      => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
			'updated_at'      => $this->dateTime(),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'register_log' );
	}
}
