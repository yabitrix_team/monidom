<?php

use yii\db\Migration;

/**
 * Class m180405_130749_add_foreign_key_for_partner_id_to_agents_table
 */
class m180405_130749_add_foreign_key_for_partner_id_to_agents_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->truncateTable( 'agents' );
		$this->addForeignKey(
			'fk-agents-partner_id',  // это "условное имя" ключа
			'agents', // это название текущей таблицы
			'partner_id', // это имя поля в текущей таблице, которое будет ключом
			'partners', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->execute( "SET foreign_key_checks = 1;" );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-agents-partner_id',
			'agents'
		);
		$this->dropIndex(
			'fk-agents-partner_id',
			'agents'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180405_130749_add_foreign_key_for_partner_id_to_agents_table cannot be reverted.\n";

		return false;
	}
	*/
}
