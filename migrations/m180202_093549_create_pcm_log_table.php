<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log`.
 */
class m180202_093549_create_pcm_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log', [
			'id'             => $this->primaryKey(),
			'exchange_name'  => $this->smallInteger( 1 )->notNull(),
			'exchange_type'  => $this->smallInteger( 1 )->notNull(),
			'exchange_stage' => $this->smallInteger( 1 )->notNull(),
			'flow'           => $this->smallInteger( 1 )->notNull(),
			'created_at'     => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'date_start'     => $this->datetime()->notNull(),
			'date_end'       => $this->datetime()->notNull(),
			'id_order'       => $this->integer( 11 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log' );
	}
}
