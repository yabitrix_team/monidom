<?php

use yii\db\Migration;

/**
 * Class m180622_091807_insert_to_events_crm_table
 */
class m180622_091807_insert_to_events_crm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

	    $this->batchInsert( 'events_crm', [ 'code', 'guid', 'name' , 'active', 'sort'], [
		    [ '110', 'a7dc22e3-7549-11e8-a2b7-00155d64d111', 'Авторизация в ЛКК', '1', 100 ],
		    [ '210', '3df541ab-754e-11e8-a2b7-00155d64d111', 'Подпись ПЭП ЛКК', '1', 110 ],
		    [ '310', '5580838b-754e-11e8-a2b7-00155d64d111', 'Заполнение данных ЛКК', '1', 120 ],
		    [ '320', '60a8a9c5-754e-11e8-a2b7-00155d64d111', 'Заполнение фото ЛКК', '1', 130 ],
		    [ '410', '6beba90f-754e-11e8-a2b7-00155d64d111', 'Ожидание статуса "Одобрение"', '1', 140 ],

	    ] );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->truncateTable('events_crm');

    }

}
