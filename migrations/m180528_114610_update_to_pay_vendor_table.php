<?php

use yii\db\Migration;

/**
 * Class m180528_114610_update_to_pay_vendor_table
 */
class m180528_114610_update_to_pay_vendor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->update("pay_vendor", ["code" => "cloudpayments"], "code = 'cloud'");
	    $this->update("pay_vendor", ["code" => "w1pay"], "code = 'w1'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->update("pay_vendor", ["code" => "cloud"], "code = 'cloudpayments'");
	    $this->update("pay_vendor", ["code" => "w1"], "code = 'w1pay'");
    }
}
