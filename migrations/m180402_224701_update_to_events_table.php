<?php

use yii\db\Migration;

/**
 * Class m180402_224701_update_to_events_table
 */
class m180402_224701_update_to_events_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->delete( 'events', [ 'code' => '804' ] );
		$this->delete( 'events', [ 'code' => '805' ] );
		$this->batchInsert( 'events', [ 'code', 'name' ], [
			[ '804', 'Агент подписал весь пакет документов с клиентом' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'events', [ 'code' => '804' ] );
		$this->batchInsert( 'events', [ 'code', 'name' ], [
			[ '804', 'Агент ввел коментарий о заявке клиента' ],
			[ '805', 'Агент подписал весь пакет документов с клиентом' ],
		] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180402_224701_update_to_events_table cannot be reverted.\n";

		return false;
	}
	*/
}
