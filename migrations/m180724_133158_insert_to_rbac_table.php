<?php

use yii\db\Migration;

/**
 * Class m180724_133158_insert_to_rbac_table
 */
class m180724_133158_insert_to_rbac_table extends Migration {
	/**
	 * {@inheritdoc}
	 * @throws Exception
	 */
	public function safeUp() {

		$auth = Yii::$app->authManager;
		//----------------------------------------------------------------------------------------------------------------------------
		//ПРАВИЛО ОПРЕДЕЛЕНИЯ ПОЛЬЗОВАТЕЛЕЙ ПО ГРУППЕ
		//----------------------------------------------------------------------------------------------------------------------------
		$ruleGroup = new \app\modules\app\v1\classes\rules\UserGroupRule;
		//$auth->add( $ruleGroup );
		// добавляем разрешение "accessPanel"
		$accessPanel              = $auth->createPermission( 'access_panel' );
		$accessPanel->description = 'Access to Panel';
		$auth->add( $accessPanel );
		//----------------------------------------------------------------------------------------------------------------------------
		//НАСТРАИВАЕМ РОЛИ
		//----------------------------------------------------------------------------------------------------------------------------
		// добавляем роль "roleSupport" и даём роли разрешение "access_panel"
		$roleSupport           = $auth->createRole( 'role_support' );
		$roleSupport->ruleName = $ruleGroup->name;
		$auth->add( $roleSupport );
		$auth->addChild( $roleSupport, $accessPanel );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'auth_item', [ 'name' => 'access_panel' ] );
		$this->delete( 'auth_item', [ 'name' => 'role_support' ] );
		$this->delete( 'auth_item_child', [ 'parent' => 'role_support' ] );
	}
}
