<?php

use yii\db\Migration;

/**
 * Class m180419_160615_alter_message_column_to_message_table
 */
class m180419_160615_alter_message_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'message', 'TEXT NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

	}
}
