<?php

use yii\db\Migration;

/**
 * Class m190123_183053_alter_file_bind_column_to_request_file_table
 */
class m190123_183053_alter_file_bind_column_to_request_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        $this->alterColumn( 'request_file', 'file_bind', "ENUM('foto_sts','foto_pts','foto_auto','foto_passport','foto_client','foto_extra','foto_card','doc_pack_1','doc_pack_2','doc_pack_3') NOT NULL" );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {

        $this->alterColumn( 'request_file', 'file_bind', "ENUM('foto_sts','foto_pts','foto_auto','foto_passport','foto_client','foto_extra','foto_card','doc_pack_1','doc_pack_2') NOT NULL" );
    }
}
