<?php

use yii\db\Migration;

/**
 * Class m180419_133803_update_to_cabinets_table
 */
class m180419_133803_update_to_cabinets_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'cabinets',
		               [ 'code' => 'aa73a53aee647e45429901a3a936c38e' ],
		               'code = \'caf2f5a8-714b-4c38-3a68-9\'' );
		$this->update( 'cabinets',
		               [ 'code' => '68a63e2f43c5f7eefdf2e8ff7106f5d4' ],
		               'code = \'caf2f5a8-714b-4c38-3a68-a\'' );
		$this->update( 'cabinets',
		               [ 'code' => 'd59e52a81d5132a8442914483af66b53' ],
		               'code = \'caf2f5a8-714b-4c38-3a68-b\'' );
		$this->update( 'cabinets',
		               [ 'code' => '625913633bc15e3642d022386b5d4b60' ],
		               'code = \'caf2f5a8-714b-4c38-3a68-c\'' );
		$this->update( 'cabinets',
		               [ 'code' => '3ecc1078f2206cb8fd8d058bba8f64d3' ],
		               'code = \'caf2f5a8-714b-4c38-3a68-d\'' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'cabinets',
		               [ 'code' => 'caf2f5a8-714b-4c38-3a68-9' ],
		               'code = \'aa73a53aee647e45429901a3a936c38e\'' );
		$this->update( 'cabinets',
		               [ 'code' => 'caf2f5a8-714b-4c38-3a68-a' ],
		               'code = \'68a63e2f43c5f7eefdf2e8ff7106f5d4\'' );
		$this->update( 'cabinets',
		               [ 'code' => 'caf2f5a8-714b-4c38-3a68-b' ],
		               'code = \'d59e52a81d5132a8442914483af66b53\'' );
		$this->update( 'cabinets',
		               [ 'code' => 'caf2f5a8-714b-4c38-3a68-c' ],
		               'code = \'625913633bc15e3642d022386b5d4b60\'' );
		$this->update( 'cabinets',
		               [ 'code' => 'caf2f5a8-714b-4c38-3a68-d' ],
		               'code = \'3ecc1078f2206cb8fd8d058bba8f64d3\'' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180419_133803_update_to_cabinets_table cannot be reverted.\n";

		return false;
	}
	*/
}
