<?php

use yii\db\Migration;

/**
 * Class m180307_124130_alter_guid_active_sort_column_to_employments_table
 */
class m180307_124130_alter_guid_active_sort_column_to_employments_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'employments', 'guid', 'char(36) null' );
		$this->alterColumn( 'employments', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'employments', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'employments', 'guid', 'char(36) not null' );
		$this->alterColumn( 'employments', 'active', 'char(1) not null' );
		$this->alterColumn( 'employments', 'sort', 'int(11) not null' );
	}
}
