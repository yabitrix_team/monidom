<?php

use yii\db\Migration;

/**
 * Handles the creation of table `consumer`.
 */
class m180322_162524_create_consumer_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'consumer', [
			'id'         => $this->primaryKey(),
			'name'       => $this->string( 255 ),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => 'TINYINT(1) NOT NULL DEFAULT 1',
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
		$time = date( 'Y-m-d H:i:s', time() );
		$this->batchInsert( 'consumer', [ 'name', 'created_at', 'updated_at' ], [
			[ 'partner', $time, $time ],
			[ 'client', $time, $time ],
			[ 'mp', $time, $time ],
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->delete( 'consumer' );
		$this->dropTable( 'consumer' );
	}
}
