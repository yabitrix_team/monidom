<?php

use yii\db\Migration;

/**
 * Class m180404_133308_add_foreign_key_for_auto_brand_id_to_auto_models_table
 */
class m180404_133308_add_foreign_key_for_auto_brand_id_to_auto_models_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-auto_models-auto_brand_id',  // это "условное имя" ключа
			'auto_models', // это название текущей таблицы
			'auto_brand_id', // это имя поля в текущей таблице, которое будет ключом
			'auto_brands', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-auto_models-auto_brand_id',
			'auto_models'
		);
		$this->dropIndex(
			'fk-auto_models-auto_brand_id',
			'auto_models'
		);
	}
}
