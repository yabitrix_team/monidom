<?php

use yii\db\Migration;

/**
 * Handles adding credit_limit_code to table `requests`.
 */
class m190201_202855_add_credit_limit_code_column_to_requests_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = '{{requests}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'credit_limit_code', $this->char(32)->after('print_link_expired'));
        $this->addForeignKey(
            'fk-requests-credit_limit_code',  // условное имя ключа
            $this->table, // название текущей таблицы
            'credit_limit_code', // имя поля в текущей таблице, которое будет ключом
            'credit_limit', // имя таблицы, с которой хотим связаться
            'code', // имя поля таблицы, с которым хотим связаться
            'SET NULL',
            'CASCADE'
        );
        $this->addCommentOnColumn(
            $this->table,
            'credit_limit_code',
            'Код кредитного лимита, связь с таблицей credit_limit'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0;');
        $this->dropForeignKey(
            'fk-requests-credit_limit_code',
            $this->table
        );
        $this->dropIndex(
            'fk-requests-credit_limit_code',
            $this->table
        );
        $this->execute('SET foreign_key_checks = 1;');
        $this->dropCommentFromColumn($this->table, 'credit_limit_code');
        $this->dropColumn($this->table, 'credit_limit_code');
    }
}
