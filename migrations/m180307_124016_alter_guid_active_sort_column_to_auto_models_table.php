<?php

use yii\db\Migration;

/**
 * Class m180307_124016_alter_guid_active_sort_column_to_auto_models_table
 */
class m180307_124016_alter_guid_active_sort_column_to_auto_models_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'auto_models', 'guid', 'char(36) null' );
		$this->alterColumn( 'auto_models', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'auto_models', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'auto_models', 'guid', 'char(36) not null' );
		$this->alterColumn( 'auto_models', 'active', 'char(1) not null' );
		$this->alterColumn( 'auto_models', 'sort', 'int(11) not null' );
	}
}
