<?php

use yii\db\Migration;

/**
 * Class m180614_133903_update_to_cabinets_table
 */
class m180614_133903_update_to_cabinets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('cabinets', ['subdomain' => 'client'], 'code = "3ecc1078f2206cb8fd8d058bba8f64d3"');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('cabinets', ['subdomain' => ''], 'code = "3ecc1078f2206cb8fd8d058bba8f64d3"');

    }

}
