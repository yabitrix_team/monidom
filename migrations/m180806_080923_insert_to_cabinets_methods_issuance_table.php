<?php

use yii\db\Migration;

/**
 * Class m180806_080923_insert_to_cabinets_methods_issuance_table
 */
class m180806_080923_insert_to_cabinets_methods_issuance_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->insert( 'cabinets_methods_issuance', [
			'cabinet_id' => 1,
			'method_id'  => 1,
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'cabinets_methods_issuance', [
			'cabinet_id' => 1,
			'method_id'  => 1,
		] );
	}
}
