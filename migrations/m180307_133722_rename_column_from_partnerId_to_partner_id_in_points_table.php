<?php

use yii\db\Migration;

/**
 * Class m180307_133722_rename_column_from_partnerId_to_partner_id_in_points_table
 */
class m180307_133722_rename_column_from_partnerId_to_partner_id_in_points_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->renameColumn( 'points', 'partnerId', 'partner_id' );
		$this->alterColumn( 'points', 'guid', 'char(36) null' );
		$this->alterColumn( 'points', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'points', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->renameColumn( 'points', 'partner_id', 'partnerId' );
		$this->alterColumn( 'points', 'guid', 'char(36) not null' );
		$this->alterColumn( 'points', 'active', 'char(1) not null' );
		$this->alterColumn( 'points', 'sort', 'int(11) not null' );
	}
}
