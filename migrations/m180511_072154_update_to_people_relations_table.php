<?php

use yii\db\Migration;

/**
 * Class m180511_072154_update_to_people_relations_table
 */
class m180511_072154_update_to_people_relations_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'people_relations', [ 'name' => 'Гражданский(-ая) муж\жена' ], 'guid = "03aa730e-dd5b-11e6-811d-00155d010003"' );
		$this->update( 'people_relations', [ 'name' => 'Супруг(-а)' ], 'guid = "03aa730e-dd5b-11e6-811d-00155d010007"' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'people_relations', [ 'name' => 'Гражданский(-ая_ муж\жена' ], 'guid = "03aa730e-dd5b-11e6-811d-00155d010003"' );
		$this->update( 'people_relations', [ 'name' => 'Супруг(-а_' ], 'guid = "03aa730e-dd5b-11e6-811d-00155d010007"' );
	}
}
