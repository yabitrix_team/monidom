<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180424_161110_insert_to_message_type_table
 */
class m180424_161110_insert_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'message_type',
		                    [ 'guid', 'name', 'active' ],
		                    [
			                    [ Uuid::uuid4(), 'status', 1 ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->delete( 'message_type', [
			'name' => 'status',
		] );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180424_161110_insert_to_message_type_table cannot be reverted.\n";

		return false;
	}
	*/
}
