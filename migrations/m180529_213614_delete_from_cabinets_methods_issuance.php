<?php

use yii\db\Migration;

/**
 * Class m180529_213614_delete_from_cabinets_methods_issuance
 */
class m180529_213614_delete_from_cabinets_methods_issuance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 1, 'method_id' => 3]);
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 1, 'method_id' => 6]);
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 1, 'method_id' => 7]);
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 1, 'method_id' => 8]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_213614_delete_from_cabinets_methods_issuance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_213614_delete_from_cabinets_methods_issuance cannot be reverted.\n";

        return false;
    }
    */
}
