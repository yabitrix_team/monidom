<?php

use yii\db\Migration;

/**
 * Handles adding crib_client_id to table `users`.
 */
class m181217_131710_add_crib_client_id_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn( 'users', 'crib_client_id', 'varchar(255) null after inside_support_token' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'crib_client_id');
    }
}
