<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m180622_125217_insert_to_status_mp_table
 */
class m180622_125217_insert_to_statuses_mp_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->insert( 'statuses_mp', [
			'name'       => 'Аннулировано',
			'identifier' => 'CANCELED',
			'updated_at' => new Expression( 'NOW()' ),
			'created_at' => new Expression( 'NOW()' ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'statuses_mp', 'identifier = "CANCELED"' );
	}
}
