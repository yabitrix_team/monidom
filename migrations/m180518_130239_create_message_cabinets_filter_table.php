<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_cabinets_filter`.
 */
class m180518_130239_create_message_cabinets_filter_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'message_cabinets_filter', [
			'message_id' => $this->integer()->notNull(),
			'cabinet_id' => $this->integer()->notNull(),
		] );
		$this->addCommentOnTable( 'message_cabinets_filter', 'Положительные связи, формирующие возможность отправки сообщения в конкретный кабинет. Связь формируется через таблицы: 
users -> user_group -> groups -> cabinets_groups -> cabinets' );
		$this->addForeignKey(
			'fk-message_id',
			'message_cabinets_filter',
			'message_id',
			'message',
			'id',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-cabinet_id',
			'message_cabinets_filter',
			'cabinet_id',
			'cabinets',
			'id',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-message_id',
			'message_cabinets_filter'
		);
		$this->dropIndex(
			'fk-message_id',
			'message_cabinets_filter'
		);
		$this->dropForeignKey(
			'fk-cabinet_id',
			'message_cabinets_filter'
		);
		$this->dropIndex(
			'fk-cabinet_id',
			'message_cabinets_filter'
		);
		$this->dropTable( 'message_cabinets_filter' );
	}
}
