<?php

use yii\db\Migration;

/**
 * Class m180402_131357_insert_to_events_table_80x
 */
class m180402_131357_insert_to_events_table_80x extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'events', [ 'code', 'name' ], [
			[ '801', 'Агент распечатал документы' ],
			[ '802', 'Агент провел идентификацию клиента' ],
			[ '803', 'Агент проверил документы клиента и сделал фото авто' ],
			[ '804', 'Агент ввел коментарий о заявке клиента' ],
			[ '805', 'Агент подписал весь пакет документов с клиентом' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'events', [ 'code' => '801' ] );
		$this->delete( 'events', [ 'code' => '802' ] );
		$this->delete( 'events', [ 'code' => '803' ] );
		$this->delete( 'events', [ 'code' => '804' ] );
		$this->delete( 'events', [ 'code' => '805' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180402_131357_insert_to_events_table_80x cannot be reverted.\n";

		return false;
	}
	*/
}
