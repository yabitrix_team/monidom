<?php

use yii\db\Migration;

/**
 * Handles adding mode to table `requests`.
 */
class m180323_152202_add_mode_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'requests', 'mode', 'varchar(6) not null after code' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'requests', 'mode' );
	}
}
