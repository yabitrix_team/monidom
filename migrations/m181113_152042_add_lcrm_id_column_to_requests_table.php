<?php

use yii\db\Migration;

/**
 * Class m181113_152042_add_lcrm_id_column_to_requests_table
 */
class m181113_152042_add_lcrm_id_column_to_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('requests', 'lcrm_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn( 'requests', 'lcrm_id' );
    }
}
