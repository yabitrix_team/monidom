<?php

use yii\db\Migration;

/**
 * Class m180530_211905_add_foreign_key_for_request_id_to_requests_events_table
 */
class m180530_211905_add_foreign_key_for_request_id_to_requests_events_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->addForeignKey(
			'fk-requests_events-request_id',  // это "условное имя" ключа
			'requests_events', // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->execute( "SET foreign_key_checks = 1;" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->dropForeignKey(
			'fk-requests_events-request_id',
			'requests_events'
		);
		$this->dropIndex(
			'fk-requests_events-request_id',
			'requests_events'
		);
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
