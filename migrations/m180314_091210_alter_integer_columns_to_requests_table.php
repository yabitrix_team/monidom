<?php

use yii\db\Migration;

/**
 * Class m180314_091210_alter_integer_columns_to_requests_table
 */
class m180314_091210_alter_integer_columns_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'auto_brand_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'auto_model_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'auto_year', 'int(11) null' );
		$this->alterColumn( 'requests', 'auto_price', 'int(11) null' );
		$this->alterColumn( 'requests', 'summ', 'int(11) null' );
		$this->alterColumn( 'requests', 'credit_product_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'point_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'user_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_region_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_employment_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_workplace_period_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_total_monthly_income', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_total_monthly_outcome', 'int(11) null' );
		$this->alterColumn( 'requests', 'client_guarantor_relation_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'method_of_issuance_id', 'int(11) null' );
		$this->alterColumn( 'requests', 'card_number', 'int(11) null' );
		$this->alterColumn( 'requests', 'bank_bik', 'int(11) null' );
		$this->alterColumn( 'requests', 'checking_account', 'int(1) null' );
		$this->alterColumn( 'requests', 'pre_crediting', 'int(1) null' );
		$this->alterColumn( 'requests', 'accreditation_num', 'int(11) null' );
		$this->alterColumn( 'requests', 'ready_to_exchange', 'int(1) null' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'auto_brand_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'auto_model_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'auto_year', 'int(11) not null' );
		$this->alterColumn( 'requests', 'auto_price', 'int(11) not null' );
		$this->alterColumn( 'requests', 'summ', 'int(11) not null' );
		$this->alterColumn( 'requests', 'credit_product_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'point_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'user_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_region_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_employment_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_workplace_period_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_total_monthly_income', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_total_monthly_outcome', 'int(11) not null' );
		$this->alterColumn( 'requests', 'client_guarantor_relation_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'method_of_issuance_id', 'int(11) not null' );
		$this->alterColumn( 'requests', 'card_number', 'int(11) not null' );
		$this->alterColumn( 'requests', 'bank_bik', 'int(11) not null' );
		$this->alterColumn( 'requests', 'checking_account', 'int(1) not null' );
		$this->alterColumn( 'requests', 'pre_crediting', 'int(1) not null' );
		$this->alterColumn( 'requests', 'accreditation_num', 'int(11) not null' );
		$this->alterColumn( 'requests', 'ready_to_exchange', 'int(1) not null' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180314_091210_alter_integet_columns_to_requests_table cannot be reverted.\n";

		return false;
	}
	*/
}
