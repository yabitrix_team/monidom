<?php

use yii\db\Migration;

/**
 * Class
 * m180322_084631_rename_column_from_name_to_code_from_sim_link_to_request_code_from_original_name_to_name_in_file_table
 */
class m180322_084631_rename_column_from_name_to_code_from_sim_link_to_request_code_from_original_name_to_name_in_file_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'file', 'name', 'code' );
		$this->renameColumn( 'file', 'original_name', 'name' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'file', 'name', 'original_name' );
		$this->renameColumn( 'file', 'code', 'name' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180322_084631_rename_column_from_name_to_code_from_sim_link_to_request_code_from_original_name_to_name_in_file_table cannot be reverted.\n";

		return false;
	}
	*/
}
