<?php

use yii\db\Migration;

/**
 * Class m180405_135201_add_foreign_key_for_loan_agreement_id_to_c_cmp_loan_agreement_fields_table
 */
class m180405_135201_add_foreign_key_for_loan_agreement_id_to_c_cmp_loan_agreement_fields_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-c_cmp_loan_agreement_fields-loan_agreement_id',  // это "условное имя" ключа
			'c_cmp_loan_agreement_fields', // это название текущей таблицы
			'loan_agreement_id', // это имя поля в текущей таблице, которое будет ключом
			'c_cmp_loan_agreement', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-c_cmp_loan_agreement_fields-loan_agreement_id',
			'c_cmp_loan_agreement_fields'
		);
		$this->dropIndex(
			'fk-c_cmp_loan_agreement_fields-loan_agreement_id',
			'c_cmp_loan_agreement_fields'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180405_135201_add_foreign_key_for_loan_agreement_id_to_c_cmp_loan_agreement_fields_table cannot be reverted.\n";

		return false;
	}
	*/
}
