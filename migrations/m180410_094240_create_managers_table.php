<?php

use yii\db\Migration;

/**
 * Handles the creation of table `managers`.
 */
class m180410_094240_create_managers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('managers', [
            'id' => $this->primaryKey(),
            'guid' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'second_name' => $this->string(255)->null(),
            'phone' => $this->string(255)->notNull(),
            'email' => $this->string(255)->null(),
            'photo_id' => $this->integer(11)->null(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'active' => $this->char(1)->notNull()->defaultValue(1),
            'sort' => $this->integer(11)->notNull()->defaultValue(1000),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('managers');
    }
}
