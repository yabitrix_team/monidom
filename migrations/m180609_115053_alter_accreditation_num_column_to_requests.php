<?php

use yii\db\Migration;

/**
 * Class m180609_115053_alter_accreditation_num_column_to_requests
 */
class m180609_115053_alter_accreditation_num_column_to_requests extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( "requests", "accreditation_num", "varchar(255) not null" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

	}
}
