<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_pep`.
 */
class m181012_094940_create_request_pep_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'request_pep';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'                 => $this->primaryKey(),
                'request_id'         => $this->integer(11),
                'confirm_type'       => "ENUM('sms','request','agreement')",
                'relation_request'   => $this->integer(11),
                'relation_agreement' => $this->string(50),
                'sms_code'           => $this->string(50),
                'sms_id'             => $this->string(50),
                'sms_provider'       => $this->string(50),
                'user_agent'         => $this->string(255),
                'updated_at'         => $this->timestamp('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                'created_at'         => $this->datetime()->defaultExpression('CURRENT_TIMESTAMP'),
            ]
        );
        $this->addForeignKey(
            'fk-'.$this->table.'-request_id',  // это 'условное имя' ключа
            $this->table, // это название текущей таблицы
            'request_id', // это имя поля в текущей таблице, которое будет ключом
            'requests', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->createIndex('request_id', $this->table, 'request_id', true);
        $this->addCommentOnTable($this->table, 'Таблица содержит информацию подтверждений ПЭП по заявкам');
        $this->addCommentOnColumn(
            $this->table,
            'confirm_type',
            'Тип подвтерждения ПЭП, ПЭП в заявке может быть проставлена: 
            через СМС подтверждение(sms); 
            при условии, что у клиента есть активная заявка от той же даты, что и оформляемая, с теми же паспортными данными и имеющая ПЭП подтверждение(request); 
            при условии, что у клиента есть активный договор заявки, у которой паспортные данные совпадают с оформляемой заявкой(agreements)'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
