<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sessions`.
 */
class m180404_132425_create_sessions_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'sessions', [
			'id'     => $this->char( 40 )->notNull()->unique(),
			'expire' => $this->integer()->notNull(),
			'data'   => $this->binary(),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'sessions' );
	}
}
