˘<?php

use yii\db\Migration;

/**
 * Class m180815_093934_alter_html_column_to_documents_table
 */
class m180815_093934_alter_html_column_to_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('documents', 'html', 'longtext');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('documents', 'html', $this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180815_093934_alter_html_column_to_documents_table cannot be reverted.\n";

        return false;
    }
    */
}
