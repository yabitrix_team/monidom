<?php

use yii\db\Migration;

/**
 * Class m180307_124119_alter_active_sort_column_to_client_statuses_table
 */
class m180307_124119_alter_active_sort_column_to_client_statuses_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'client_statuses', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'client_statuses', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'client_statuses', 'active', 'char(1) not null' );
		$this->alterColumn( 'client_statuses', 'sort', 'int(11) not null' );
	}
}
