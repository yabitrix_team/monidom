<?php

use yii\db\Migration;

/**
 * Class m181109_102709_insert_to_groups_cabinets_rbac_table
 */
class m181109_102709_insert_to_groups_cabinets_rbac_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function safeUp() {

        $this->insert( 'cabinets', [
            "code"      => "53b9e9679a8ea25880376080b76f98ad",
            "name"      => "КЦ",
            "subdomain" => "call",
            "active"    => 1,
        ] );
        $idCabinet = Yii::$app->db->getLastInsertId( "cabinets" );
        $this->insert( 'groups', [
            "identifier"       => "call",
            "name"             => "Операторы КЦ",
            "prior_cabinet_id" => $idCabinet,
        ] );
        $auth                    = Yii::$app->authManager;
        $ruleGroup               = new \app\modules\app\v1\classes\rules\UserGroupRule;
        $roleLeadAgent           = $auth->createRole( 'role_call' );
        $roleLeadAgent->ruleName = $ruleGroup->name;
        $auth->add( $roleLeadAgent );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {

        $this->delete( 'cabinets', [ 'code' => '53b9e9679a8ea25880376080b76f98ad' ] );
        $this->delete( 'groups', [ 'identifier' => 'call' ] );
        $this->delete( 'auth_item', [ 'name' => 'role_call' ] );
    }
}
