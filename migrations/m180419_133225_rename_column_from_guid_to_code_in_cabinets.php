<?php

use yii\db\Migration;

/**
 * Class m180419_133225_rename_column_from_guid_to_code_in_cabinets
 */
class m180419_133225_rename_column_from_guid_to_code_in_cabinets extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'cabinets', 'guid', 'code' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'cabinets', 'code', 'guid' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180419_133225_rename_column_from_guid_to_code_in_cabinets cannot be reverted.\n";

		return false;
	}
	*/
}
