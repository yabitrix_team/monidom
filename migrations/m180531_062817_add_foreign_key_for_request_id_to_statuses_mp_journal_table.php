<?php

use yii\db\Migration;

/**
 * Class m180531_062817_add_foreign_key_for_request_id_to_statuses_mp_journal_table
 */
class m180531_062817_add_foreign_key_for_request_id_to_statuses_mp_journal_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->execute( 'SET foreign_key_checks = 0;' );
		$this->addForeignKey(
			'fk-statuses_mp_journal-requests_id',  // это 'условное имя' ключа
			'statuses_mp_journal', // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->execute( 'SET foreign_key_checks = 1;' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( 'SET foreign_key_checks = 0;' );
		$this->dropForeignKey(
			'fk-statuses_mp_journal-requests_id',
			'statuses_mp_journal'
		);
		$this->dropIndex(
			'fk-statuses_mp_journal-requests_id',
			'statuses_mp_journal'
		);
		$this->execute( 'SET foreign_key_checks = 1;' );
	}
}
