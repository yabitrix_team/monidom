<?php

use yii\db\Migration;

/**
 * Class m180405_095229_seed_agents_points
 */
class m180405_095229_add_foreign_key_for_point_id_agent_id_to_users_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		/**
		 * 1. выбираем что есть
		 */
		$db = Yii::$app->db;
		if ( $allIDAgent = $db->createCommand( "SELECT *
FROM agents" )->queryOne() ) {
			$allIDAgent = $allIDAgent['id'];
		} else {
			$allIDAgent = $db->createCommand()->insert( 'agents', [
				'guid'       => md5( microtime( true ) ),
				'partner_id' => '1',
				'created_at' => '2018-04-04 14:25:53',
				'updated_at' => '2018-04-04 14:25:56',
				'active'     => '1',
				'sort'       => '1',
			] )->execute();
		}
		if ( $allIDPoint = $db->createCommand( "SELECT *
FROM points" )->queryOne() ) {
			$allIDPoint = $allIDPoint['id'];
		} else {
			$allIDPoint = $db->createCommand()->insert( 'points', [
				'guid'       => md5( microtime( true ) ),
				'name'       => 'a',
				'partner_id' => '1',
				'country'    => 'a',
				'index'      => 'a',
				'region'     => 'a',
				'area'       => 'a',
				'city'       => 'aa',
				'locality'   => 'aa',
				'street'     => 'a',
				'house'      => '1',
				'housing'    => '2',
				'office'     => '3',
				'brand_name' => 'a',
				'time_table' => 'a',
				'coords'     => 'aa',
				'created_at' => '2018-04-04 14:21:29',
				'updated_at' => '2018-04-04 14:21:33',
				'active'     => '1',
				'sort'       => '1',
			] )->execute();
		}
		/**
		 * 2. готовим данные
		 */
		$allUsers = $db->createCommand( "SELECT *
FROM users" )->queryAll();
		if ( $allUsers ) {

			array_walk( $allUsers, function( &$user ) use ( $allIDAgent, $allIDPoint ) {

				unset( $user['id'] );
				$user['point_id'] = $allIDAgent;
				$user['agent_id'] = $allIDPoint;
			} );
			$this->execute( "SET foreign_key_checks = 0;" );
			$db->createCommand( "TRUNCATE users;" )->execute();
			$this->execute( "SET foreign_key_checks = 1;" );
		}
		/**
		 * 3. заменяем
		 */
		$this->addForeignKey(
			'fk-users-point_id',  // это "условное имя" ключа
			'users', // это название текущей таблицы
			'point_id', // это имя поля в текущей таблице, которое будет ключом
			'points', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-users-agent_id',  // это "условное имя" ключа
			'users', // это название текущей таблицы
			'agent_id', // это имя поля в текущей таблице, которое будет ключом
			'agents', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		/**
		 * 4. возвращаем
		 */
		// @formatter:on
		$this->batchInsert( 'users', [
			"guid",
			"username",
			"email",
			"auth_key",
			"password_hash",
			"password_reset_token",
			"point_id",
			"agent_id",
			"first_name",
			"last_name",
			"second_name",
			"is_accreditated",
			"inside_support_token",
			"created_at",
			"updated_at",
			"active",
			"sort",
		], $allUsers );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-users-agent_id',
			'users'
		);
		$this->dropForeignKey(
			'fk-users-point_id',
			'users'
		);
		$this->dropIndex(
			'fk-users-agent_id',
			'users'
		);
		$this->dropIndex(
			'fk-users-point_id',
			'users'
		);
	}
}
