<?php

use yii\db\Migration;

/**
 * Class m180716_071642_alter_pay_w1_table
 */
class m180716_071642_alter_pay_w1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

	    yii::$app->db->createCommand( "SET foreign_key_checks=0;" )->execute();

	    $this->dropColumn('pay_w1', 'fee');

	    $this->addColumn('pay_w1', 'name', $this->string());
	    $this->addColumn('pay_w1', 'ip', $this->string());
	    $this->addColumn('pay_w1', 'country', $this->string());
	    $this->addColumn('pay_w1', 'city', $this->string());
	    $this->addColumn('pay_w1', 'region', $this->string());

	    $this->addColumn('pay_w1', 'wmi_invoice_operations', $this->string());
	    $this->addColumn('pay_w1', 'fd_main_commission_accept', $this->string());
	    $this->addColumn('pay_w1', 'fd_paysys', $this->string());
	    $this->addColumn('pay_w1', 'wmi_auto_accept', $this->string());
	    $this->addColumn('pay_w1', 'wmi_commission_amount', $this->string());
	    $this->addColumn('pay_w1', 'wmi_expired_date', $this->string());
	    $this->addColumn('pay_w1', 'wmi_last_notify_date', $this->string());
	    $this->addColumn('pay_w1', 'wmi_order_state', $this->string());
	    $this->addColumn('pay_w1', 'wmi_payment_amount', $this->string());
	    $this->addColumn('pay_w1', 'wmi_payment_type', $this->string());
	    $this->addColumn('pay_w1', 'wmi_signature', $this->string());

	    $sql = "ALTER TABLE pay_w1 MODIFY action ENUM('pay','check','fail')";
	    $this->execute($sql);

	    Yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    yii::$app->db->createCommand( "SET foreign_key_checks=0;" )->execute();

	    $this->addColumn('pay_w1', 'fee', $this->string());

	    $this->dropColumn('pay_w1', 'name');
	    $this->dropColumn('pay_w1', 'ip');
	    $this->dropColumn('pay_w1', 'country');
	    $this->dropColumn('pay_w1', 'city');
	    $this->dropColumn('pay_w1', 'region');

	    $this->dropColumn('pay_w1', 'wmi_invoice_operations');
	    $this->dropColumn('pay_w1', 'fd_main_commission_accept');
	    $this->dropColumn('pay_w1', 'fd_paysys');
	    $this->dropColumn('pay_w1', 'wmi_auto_accept');
	    $this->dropColumn('pay_w1', 'wmi_commission_amount');
	    $this->dropColumn('pay_w1', 'wmi_expired_date');
	    $this->dropColumn('pay_w1', 'wmi_last_notify_date');
	    $this->dropColumn('pay_w1', 'wmi_order_state');
	    $this->dropColumn('pay_w1', 'wmi_payment_amount');
	    $this->dropColumn('pay_w1', 'wmi_payment_type');
	    $this->dropColumn('pay_w1', 'wmi_signature');

	    $sql = "ALTER TABLE pay_w1 MODIFY action ENUM('pay', 'check', 'fail')";
	    $this->execute($sql);

	    Yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
    }

}
