<?php

use yii\db\Migration;

/**
 * Handles adding sended to table `pay_payment`.
 */
class m180525_115416_add_sended_column_to_pay_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('pay_payment', 'sended', "tinyint(1) not null default '0' after description");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('pay_payment', 'sended');
    }
}
