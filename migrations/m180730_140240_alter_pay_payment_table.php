<?php

use yii\db\Migration;

/**
 * Class m180730_140240_alter_pay_payment_table
 */
class m180730_140240_alter_pay_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('pay_payment', 'transaction', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn('pay_payment', 'transaction', $this->string()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180730_140240_alter_pay_payment_table cannot be reverted.\n";

        return false;
    }
    */
}
