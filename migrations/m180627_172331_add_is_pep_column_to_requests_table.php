<?php

use yii\db\Migration;

/**
 * Handles adding is_pep to table `requests`.
 */
class m180627_172331_add_is_pep_column_to_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('requests', 'is_pep', "tinyint(1) not null default '0'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('requests', 'is_pep');
    }
}
