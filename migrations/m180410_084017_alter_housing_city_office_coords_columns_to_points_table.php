<?php

use yii\db\Migration;

/**
 * Class m180410_084017_alter_housing_column_to_points_table
 */
class m180410_084017_alter_housing_city_office_coords_columns_to_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->alterColumn( 'points', 'housing', 'smallint(4) null' );
	    $this->alterColumn( 'points', 'house', 'smallint(4) null' );
	    $this->alterColumn( 'points', 'office', 'smallint(4) null' );
	    $this->alterColumn( 'points', 'city', 'varchar(255) null' );
	    $this->alterColumn( 'points', 'coords', 'varchar(255) null' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn( 'points', 'housing', 'smallint(4) not null' );
	    $this->alterColumn( 'points', 'house', 'smallint(4) not null' );
	    $this->alterColumn( 'points', 'office', 'smallint(4) not null' );
	    $this->alterColumn( 'points', 'city', 'varchar(255) not null' );
	    $this->alterColumn( 'points', 'coords', 'varchar(255) not null' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180410_084017_alter_housing_column_to_points_table cannot be reverted.\n";

        return false;
    }
    */
}
