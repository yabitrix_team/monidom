<?php

use yii\db\Migration;

/**
 * Handles the creation of table `file`.
 */
class m180201_134319_create_file_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'file', [
			'id'            => $this->primaryKey(),
			'guid'          => $this->char( 36 )->notNull()->unique(),
			'name'          => $this->string( 255 )->notNull(),
			'original_name' => $this->text()->notNull(),
			'path'          => $this->text()->notNull(),
			'height'        => $this->integer( 11 )->notNull(),
			'width'         => $this->integer( 11 )->notNull(),
			'type'          => $this->string( 255 )->notNull(),
			'size'          => $this->integer( 11 )->notNull(),
			'description'   => $this->text()->notNull(),
			'sim_link'      => $this->string( 255 )->notNull()->unique(),
			'created_at'    => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'    => $this->datetime()->notNull()
			                        ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'        => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'          => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'file' );
	}
}
