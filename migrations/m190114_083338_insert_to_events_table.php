<?php

use yii\db\Migration;

/**
 * Class m190114_083338_insert_to_events_table
 */
class m190114_083338_insert_to_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert( 'events', [ 'code' => 830, 'name' =>  'Переподписан 1 пакет документов'] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete( 'events', [ 'code' => '830' ] );
    }
}
