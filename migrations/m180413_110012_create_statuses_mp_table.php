<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses_mp`.
 */
class m180413_110012_create_statuses_mp_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'statuses_mp', [
			'id'         => $this->primaryKey(),
			'name'       => $this->string( 255 )->notNull(),
			'identifier' => $this->string( 255 )->notNull(),
			'updated_at' => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' )->notNull(),
			'created_at' => $this->datetime()->notNull(),
			'active'     => $this->tinyInteger( 1 )->notNull()->defaultValue( 1 ),
			'sort'       => $this->integer( 11 )->defaultValue( 1000 ),
		] );
		$this->createIndex( 'identifier', 'statuses_mp', 'identifier', true );
		$this->batchInsert( 'statuses_mp', [ 'name', 'identifier', 'created_at' ], [
			[ 'Создана заявка', 'CREATED', date( 'Y-m-d', time() ) ],
			[ 'Отправлено на предварительное одобрение', 'PREAPPROVED_SENT', date( 'Y-m-d', time() ) ],
			[ 'Предварительно одобрено', 'PREAPPROVED', date( 'Y-m-d', time() ) ],
			[ 'Отправлено на одобрение', 'APPROVED_SENT', date( 'Y-m-d', time() ) ],
			[ 'Одобрено', 'APPROVED', date( 'Y-m-d', time() ) ],
			[ 'Отказано', 'DENIED', date( 'Y-m-d', time() ) ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'identifier', 'statuses_mp' );
		$this->truncateTable( 'statuses_mp' );
		$this->dropTable( 'statuses_mp' );
	}
}
