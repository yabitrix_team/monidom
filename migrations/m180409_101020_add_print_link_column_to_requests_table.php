<?php

use yii\db\Migration;

/**
 * Handles adding print_link to table `requests`.
 */
class m180409_101020_add_print_link_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'requests', 'print_link', 'varchar(8) not null after ready_to_exchange' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'requests', 'print_link' );
	}
}
