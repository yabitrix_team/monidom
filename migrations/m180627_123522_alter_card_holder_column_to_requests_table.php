<?php

use yii\db\Migration;

/**
 * Class m180627_123522_alter_card_holder_column_to_requests_table
 */
class m180627_123522_alter_card_holder_column_to_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('requests', 'card_holder', 'varchar(50) not null after card_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('requests', 'card_holder');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_123522_alter_card_holder_column_to_requests_table cannot be reverted.\n";

        return false;
    }
    */
}
