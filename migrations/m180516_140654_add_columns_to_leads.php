<?php

use yii\db\Migration;

/**
 * Class m180516_140654_add_columns_to_leads
 */
class m180516_140654_add_columns_to_leads extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn( 'leads', 'region', 'varchar(255) not null after client_birthday' );
	    $this->addColumn( 'leads', 'auto_year', 'varchar(36) not null after client_birthday' );
	    $this->addColumn( 'leads', 'auto_model_guid', 'varchar(36) not null after client_birthday' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn( 'leads', 'auto_model_guid' );
	    $this->dropColumn( 'leads', 'auto_year' );
	    $this->dropColumn( 'leads', 'region' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180516_140654_add_columns_to_leads cannot be reverted.\n";

        return false;
    }
    */
}
