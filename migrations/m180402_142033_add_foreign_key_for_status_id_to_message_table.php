<?php

use yii\db\Migration;

/**
 * Class m180402_142033_add_foreign_key_for_status_id_to_message_table
 */
class m180402_142033_add_foreign_key_for_status_id_to_message_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-message-status_id',  // это "условное имя" ключа
			'message', // это название текущей таблицы
			'status_id', // это имя поля в текущей таблице, которое будет ключом
			'message_status', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-message-status_id',
			'message'
		);
	}
}
