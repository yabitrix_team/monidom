<?php

use yii\db\Migration;

/**
 * Handles adding guid to table `lead`.
 */
class m180322_143452_add_guid_column_to_lead_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'leads', 'guid', 'varchar(32) not null after id' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'leads', 'guid' );
	}
}
