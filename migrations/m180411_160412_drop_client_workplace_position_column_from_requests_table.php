<?php

use yii\db\Migration;

/**
 * Handles dropping client_workplace_position from table `requests`.
 */
class m180411_160412_drop_client_workplace_position_column_from_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->dropColumn("requests", "client_workplace_position");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->addColumn("requests", "client_workplace_position", "varchar(255) not null after client_employment_id");
    }
}
