<?php

use yii\db\Migration;

/**
 * Class m180329_141315_rename_api_sms_confirm_table
 */
class m180329_141315_rename_api_sms_confirm_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'api_sms_confirm', 'lock_expired_at', 'DATETIME NULL AFTER status' );
		$this->addColumn( 'api_sms_confirm', 'attempt', 'TINYINT(3) NOT NULL DEFAULT "0" AFTER status ' );
		$this->addColumn( 'api_sms_confirm', 'created_at', 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER date ' );
		$this->alterColumn( 'api_sms_confirm', 'status', 'TINYINT(1) NULL DEFAULT "0"' );
		$this->renameColumn( 'api_sms_confirm', 'date', 'updated_at' );
		$this->renameTable( 'api_sms_confirm', 'sms_confirm' );
		$this->createIndex( 'login', 'sms_confirm', 'login', true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'login', 'sms_confirm' );
		$this->dropColumn( 'sms_confirm', 'lock_expired_at' );
		$this->dropColumn( 'sms_confirm', 'created_at' );
		$this->dropColumn( 'sms_confirm', 'attempt' );
		$this->alterColumn( 'sms_confirm', 'status', 'CHAR(1) NOT NULL DEFAULT "N"' );
		$this->renameColumn( 'sms_confirm', 'updated_at', 'date' );
		$this->renameTable( 'sms_confirm', 'api_sms_confirm' );
	}
}
