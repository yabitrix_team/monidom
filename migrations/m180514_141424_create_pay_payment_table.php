<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_payment`.
 */
class m180514_141424_create_pay_payment_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pay_payment', [
			'id'          => $this->primaryKey(),
			'vendor'      => $this->integer()->notNull(),
			'invoice'     => $this->string()->notNull(),
			'contract'    => $this->string()->notNull(),
			'transaction' => $this->string()->notNull(),
			'sum'         => $this->decimal( 14, 2 )->notNull(),
			'currency'    => $this->integer(),
			'description' => $this->string(),
			'created_at'  => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
		] );
		$this->addForeignKey(
			'fk-pay_payment-vendor',  // это "условное имя" ключа
			'pay_payment', // это название текущей таблицы
			'vendor', // это имя поля в текущей таблице, которое будет ключом
			'pay_vendor', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-pay_payment-currency',  // это "условное имя" ключа
			'pay_payment', // это название текущей таблицы
			'currency', // это имя поля в текущей таблице, которое будет ключом
			'currency', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'pay_payment' );
	}
}
