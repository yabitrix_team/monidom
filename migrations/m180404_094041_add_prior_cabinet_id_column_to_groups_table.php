<?php

use yii\db\Migration;

/**
 * Handles adding prior_cabinet_id to table `groups`.
 */
class m180404_094041_add_prior_cabinet_id_column_to_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'groups', 'prior_cabinet_id', 'int(11) null after name' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'groups', 'prior_cabinet_id' );
	}
}
