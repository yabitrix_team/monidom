<?php

use yii\db\Migration;

/**
 * Class m180307_140847_alter_guid_active_sort_column_to_workplace_periods_table
 */
class m180307_140847_alter_guid_active_sort_column_to_workplace_periods_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'workplace_periods', 'guid', 'char(36) null' );
		$this->alterColumn( 'workplace_periods', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'workplace_periods', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'workplace_periods', 'guid', 'char(36) not null' );
		$this->alterColumn( 'workplace_periods', 'active', 'char(1) not null' );
		$this->alterColumn( 'workplace_periods', 'sort', 'int(11) not null' );
	}
}
