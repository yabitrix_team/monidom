<?php

use yii\db\Migration;

/**
 * Class m180404_134146_add_foreign_key_for_request_id_user_id_to_lock_order_table
 */
class m180404_134146_add_foreign_key_for_request_id_user_id_to_lock_order_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-lock_order-request_id',  // это "условное имя" ключа
			'lock_order', // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-lock_order-user_id',  // это "условное имя" ключа
			'lock_order', // это название текущей таблицы
			'user_id', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-lock_order-request_id',
			'lock_order'
		);
		$this->dropForeignKey(
			'fk-lock_order-user_id',
			'lock_order'
		);
		$this->dropIndex(
			'fk-lock_order-request_id',
			'lock_order'
		);
		$this->dropIndex(
			'fk-lock_order-user_id',
			'lock_order'
		);
	}
}
