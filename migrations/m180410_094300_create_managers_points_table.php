<?php

use yii\db\Migration;

/**
 * Handles the creation of table `managers_points`.
 */
class m180410_094300_create_managers_points_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('managers_points', [
            'id' => $this->primaryKey(),
            'point_id' => $this->integer(11)->notNull(),
            'manager_id' => $this->integer(11)->notNull(),
            'type' => 'enum(\'rp\',\'mrp\') COLLATE utf8_unicode_ci DEFAULT NULL',
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()->notNull(),
            'active' => $this->char(1)->notNull()->defaultValue(1),
            'sort' => $this->integer(11)->notNull()->defaultValue(1000),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('managers_points');
    }
}
