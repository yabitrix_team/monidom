<?php

use yii\db\Migration;

/**
 * Class m180307_132735_alter_guid_active_sort_column_to_order_commission_table
 */
class m180307_132735_alter_guid_active_sort_column_to_order_commission_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'order_commission', 'guid', 'char(36) null' );
		$this->alterColumn( 'order_commission', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'order_commission', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'order_commission', 'guid', 'char(36) not null' );
		$this->alterColumn( 'order_commission', 'active', 'char(1) not null' );
		$this->alterColumn( 'order_commission', 'sort', 'int(11) not null' );
	}
}
