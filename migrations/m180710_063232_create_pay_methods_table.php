<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_methods`.
 */
class m180710_063232_create_pay_methods_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pay_methods', [
			'id'              => $this->primaryKey(),
			'name'            => $this->string(),
			'rnko_mfk'        => $this->double( 2 ),
			'rnko_mfk_min'    => $this->double( 2 ),
			'rnko_client'     => $this->double( 2 ),
			'rnko_client_min' => $this->double( 2 ),
			'vendor_id'       => $this->integer(),
			'pseudonim'       => $this->string(),
			'active'          => $this->tinyInteger( 1 ),
			'sort'            => $this->smallInteger( 4 ),
			'created_at'      => $this->timestamp()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
		] );
		$comment = "Таблица хранит методы платежей платежных систем. rnko_mfk : %, взимаемый рнко с мфк; rnko_mfk_min : минимальная взимаемая сумма, взимаемая с мфк (руб); rnko_client : %, взимаемый рнко с клиента; rnko_client_min : минимальная взимаемая сумма, взимаемая с клиента (руб);";
		$this->addCommentOnTable( 'pay_methods', $comment );
		$this->batchInsert( 'pay_methods', [
			'name',
			'rnko_mfk',
			'rnko_mfk_min',
			'rnko_client',
			'rnko_client_min',
			'pseudonim',
			'vendor_id',
			'active',
			'sort',
		], [
			[
				'Банковские картыVISA/MasterCard/МИР',
				'0',
				'0',
				'1.25',
				'50',
				'CreditCardRUB',
				'2',
				'1',
				'110',
			],
			[
				'Интернет-банкинг Банк Русский стандарт',
				'0',
				'0',
				'2.75',
				'0',
				'RsbRUB',
				'2',
				'1',
				'120',
			],
			[
				'Интернет-банкинг Альфа-клик',
				'0',
				'0',
				'2.75',
				'0',
				'AlfaclickRUB',
				'2',
				'1',
				'130',
			],
			[
				'Интернет-банкинг PSB-Retail «Промсвязьбанк»',
				'0',
				'0',
				'2.75',
				'0',
				'PsbRetailRUB',
				'2',
				'1',
				'140',
			],
			[
				'Интернет-банкинг Московский кредитный банк',
				'0',
				'0',
				'2.75',
				'0',
				'',
				'2',
				'0',
				'150',
			],
			[
				'Интернет-банкинг Сбербанк Онл@йн',
				'0',
				'0',
				'2.75',
				'0',
				'SberOnlineRUB',
				'2',
				'1',
				'160',
			],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'pay_methods' );
	}
}
