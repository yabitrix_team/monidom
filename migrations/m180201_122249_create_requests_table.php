<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requests`.
 */
class m180201_122249_create_requests_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'requests', [
			'id'                            => $this->primaryKey(),
			'guid'                          => $this->char( 36 )->notNull()->unique(),
			'code'                          => $this->string( 255 )->notNull()->unique(),
			'auto_brand_id'                 => $this->integer( 11 )->notNull(),
			'auto_model_id'                 => $this->integer( 11 )->notNull(),
			'auto_year'                     => $this->integer( 4 )->notNull(),
			'auto_price'                    => $this->integer( 11 )->notNull(),
			'summ'                          => $this->integer( 11 )->notNull(),
			'date_return'                   => $this->date()->notNull(),
			'credit_product'                => $this->integer( 11 )->notNull(),
			'point_id'                      => $this->integer( 11 )->notNull(),
			'user_id'                       => $this->integer( 11 )->notNull(),
			'client_first_name'             => $this->string( 255 )->notNull(),
			'client_last_name'              => $this->string( 255 )->notNull(),
			'client_patronymic'             => $this->string( 255 )->notNull(),
			'client_birthday'               => $this->date()->notNull(),
			'client_mobile_phone'           => $this->string( 20 )->notNull(),
			'client_email'                  => $this->string( 255 )->notNull(),
			'client_passport_serial_number' => $this->char( 4 )->notNull(),
			'client_passport_number'        => $this->char( 6 )->notNull(),
			'client_region_id'              => $this->integer( 11 )->notNull(),
			'client_home_phone'             => $this->string( 20 )->notNull(),
			'client_employment'             => $this->integer( 11 )->notNull(),
			'client_workplace_position'     => $this->string( 255 )->notNull(),
			'client_workplace_experience'   => $this->string( 255 )->notNull(),
			'client_workplace_period'       => $this->string( 255 )->notNull(),
			'client_workplace_address'      => $this->string( 255 )->notNull(),
			'client_workplace_phone'        => $this->string( 20 )->notNull(),
			'client_total_monthly_income'   => $this->integer( 11 )->notNull(),
			'client_total_monthly_outcome'  => $this->integer( 11 )->notNull(),
			'client_guarantor_name'         => $this->string( 255 )->notNull(),
			'client_guarantor_relation_id'  => $this->integer( 11 )->notNull(),
			'client_guarantor_phone'        => $this->string( 20 )->notNull(),
			'method_of_issuance_id'         => $this->integer( 11 )->notNull(),
			'card_number'                   => $this->integer( 11 )->notNull(),
			'bank_bik'                      => $this->integer( 11 )->notNull(),
			'checking_account'              => $this->char( 1 )->notNull(),
			'pre_crediting'                 => $this->char( 1 )->notNull(),
			'accreditation_num'             => $this->integer( 11 )->notNull(),
			'roystat_id'                    => $this->string( 50 )->notNull(),
			'advert_channel'                => $this->string( 50 )->notNull(),
			'referal_code'                  => $this->string( 50 )->notNull(),
			'request_source'                => $this->string( 50 )->notNull(),
			'place_order_creation'          => $this->string( 50 )->notNull(),
			'comment_client'                => $this->text()->notNull(),
			'comment_partner'               => $this->text()->notNull(),
			'ready_to_exchange'             => $this->char( 1 )->notNull(),
			'created_at'                    => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'                    => $this->datetime()->notNull()
			                                        ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'                        => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'                          => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'requests' );
	}
}
