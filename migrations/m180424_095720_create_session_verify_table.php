<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session_verify`.
 */
class m180424_095720_create_session_verify_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'session_verify', [
			'id'              => $this->primaryKey(),
			'session_id'      => $this->char( 40 )->notNull()->unique(),
			'attempt'         => $this->integer(),
			'lock_expired_at' => $this->dateTime(),
			'created_at'      => $this->dateTime(),
			'updated_at'      => $this->dateTime(),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'session_verify' );
	}
}
