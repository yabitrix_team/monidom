<?php

use yii\db\Migration;

/**
 * Class m180307_112613_rename_column_from_parnerId_to_partner_id_in_agents_table
 */
class m180307_112613_rename_column_from_parnerId_to_partner_id_in_agents_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->renameColumn( 'agents', 'partnerId', 'partner_id' );
		$this->alterColumn( 'agents', 'guid', 'char(36) null' );
		$this->alterColumn( 'agents', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'agents', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->renameColumn( 'agents', 'partner_id', 'partnerId' );
		$this->alterColumn( 'agents', 'guid', 'char(36) not null' );
		$this->alterColumn( 'agents', 'active', 'char(1) not null' );
		$this->alterColumn( 'agents', 'sort', 'int(11) not null' );
	}
}
