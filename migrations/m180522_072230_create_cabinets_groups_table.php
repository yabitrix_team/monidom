<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cabinets_groups`.
 */
class m180522_072230_create_cabinets_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'cabinets_groups', [
			'cabinet_id' => $this->integer()->notNull(),
			'group_id'   => $this->integer()->notNull(),
		] );
		$this->addCommentOnTable( 'cabinets_groups', 'Таблица связей групп пользователей и кабинетов (доменов), формирующих возможность доступа пользователей к доменам' );
		$this->addForeignKey(
			'fk-cabinets_groups-cabinets_id',
			'cabinets_groups',
			'cabinet_id',
			'cabinets',
			'id',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-cabinets_groups-groups_id',
			'cabinets_groups',
			'group_id',
			'groups',
			'id',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-cabinets_groups-cabinets_id',
			'cabinets_groups'
		);
		$this->dropIndex(
			'fk-cabinets_groups-cabinets_id',
			'cabinets_groups'
		);
		$this->dropForeignKey(
			'fk-cabinets_groups-groups_id',
			'cabinets_groups'
		);
		$this->dropIndex(
			'fk-cabinets_groups-groups_id',
			'cabinets_groups'
		);
		$this->dropTable( 'cabinets_groups' );
	}
}
