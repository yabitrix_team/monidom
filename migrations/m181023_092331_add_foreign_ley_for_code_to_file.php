<?php

use yii\db\Migration;

/**
 * Class m181023_092331_add_foreign_ley_for_code_to_file
 */
class m181023_092331_add_foreign_ley_for_code_to_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createIndex('code', 'file', 'code', true);
    }

    public function down()
    {
        $this->dropIndex('code', 'file');
    }
}
