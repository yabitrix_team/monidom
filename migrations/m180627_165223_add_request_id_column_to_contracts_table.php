<?php

use yii\db\Migration;

/**
 * Handles adding request_id to table `contracts`.
 */
class m180627_165223_add_request_id_column_to_contracts_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'contracts', 'request_id', $this->integer( 11 )->after( 'code' ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'contracts', 'request_id' );
	}
}
