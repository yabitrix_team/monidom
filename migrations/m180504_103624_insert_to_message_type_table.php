<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180504_103624_insert_to_message_type_table
 */
class m180504_103624_insert_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'message_type',
		                    [ 'guid', 'name', 'active', 'public' ],
		                    [
			                    [ Uuid::uuid4(), 'secondpack', 1, 1 ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->delete( 'message_type', [
			'name' => 'secondpack',
		] );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
