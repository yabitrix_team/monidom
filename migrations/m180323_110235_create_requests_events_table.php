<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requests_events`.
 */
class m180323_110235_create_requests_events_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'requests_events', [
			'id'         => $this->primaryKey(),
			'request_id' => $this->integer( 11 )->null(),
			'event_id'   => $this->integer( 3 )->null(),
			'is_sended'  => $this->integer( 1 )->defaultValue( 0 ),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'requests_events' );
	}
}
