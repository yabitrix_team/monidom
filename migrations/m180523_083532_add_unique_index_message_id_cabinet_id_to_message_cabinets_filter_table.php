<?php

use yii\db\Migration;

/**
 * Class m180523_083532_add_unique_index_message_id_cabinet_id_to_message_cabinets_filter_table
 */
class m180523_083532_add_unique_index_message_id_cabinet_id_to_message_cabinets_filter_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createIndex( 'message_id_cabinet_id', 'message_cabinets_filter', [
			'message_id',
			'cabinet_id',
		], true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'message_id_cabinet_id', 'message_cabinets_filter' );
	}
}
