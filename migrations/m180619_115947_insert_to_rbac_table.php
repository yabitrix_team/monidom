<?php

use yii\db\Migration;

/**
 * Class m180619_115947_insert_to_rbac_table
 */
class m180619_115947_insert_to_rbac_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $auth = Yii::$app->authManager;
	    //----------------------------------------------------------------------------------------------------------------------------
	    //ПРАВИЛО ОПРЕДЕЛЕНИЯ ПОЛЬЗОВАТЕЛЕЙ ПО ГРУППЕ
	    //----------------------------------------------------------------------------------------------------------------------------
	    $ruleGroup = new \app\modules\app\v1\classes\rules\UserGroupRule;
	    //$auth->add( $ruleGroup );
	    // добавляем разрешение "accessLKPA"
	    $accessLkpa              = $auth->createPermission( 'access_lkpa' );
	    $accessLkpa->description = 'Access to LKPA';
	    $auth->add( $accessLkpa );
	    //----------------------------------------------------------------------------------------------------------------------------
	    //НАСТРАИВАЕМ РОЛИ
	    //----------------------------------------------------------------------------------------------------------------------------
	    // добавляем роль "roleAdmin" и даём роли разрешение "access_lkpa"
	    $roleAdmin           = $auth->createRole( 'role_partner_admin' );
	    $roleAdmin->ruleName = $ruleGroup->name;
	    $auth->add( $roleAdmin );
	    $auth->addChild( $roleAdmin, $accessLkpa );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180619_115947_insert_to_rbac_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180619_115947_insert_to_rbac_table cannot be reverted.\n";

        return false;
    }
    */
}
