<?php

use yii\db\Migration;

/**
 * Class m180404_133302_insert_to_pcm_log_tables
 */
class m180404_133302_insert_to_pcm_log_tables extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'pcm_log_exchange_name', [ 'name' ], [
			[ 'Служебное' ],
			[ 'Справочник' ],
			[ 'Событие' ],
			[ 'Статус' ],
			[ 'Файл' ],
			[ 'Заявка' ],
		] );
		$this->batchInsert( 'pcm_log_exchange_type', [ 'name' ], [
			[ 'Служебное' ],
			[ 'На сайт' ],
			[ 'В 1С' ],
		] );
		$this->batchInsert( 'pcm_log_exchange_stage', [ 'code', 'name' ], [
			[ 'System', 'Служебное' ],
			[ 'AutoBrandsController', 'Марки авто' ],
			[ 'AutoGenerationsController', 'Поколения авто' ],
			[ 'AutoModelsController', 'Модели авто' ],
			[ 'AutoModificationsController', 'Модификации авто' ],
			[ 'PartnersController', 'Партнеры' ],
			[ 'PointsController', 'Точки' ],
			[ 'ProductsController', 'Кредитные продукты' ],
			[ 'RegionsController', 'Регионы' ],
			[ 'StatusesController', 'Типы статусов' ],
			[ 'DocsFirstPackController', 'Пакет документов 1' ],
			[ 'DocsSecondPackController', 'Пакет документов 2' ],
			[ 'CommissionController', 'Комиссия' ],
			[ 'StatesController', 'Статусы' ],
			[ 'UsersController', 'Пользователи' ],
			[ 'EmploymentController', 'Занятность' ],
			[ 'ManagersController', 'Менеджеры' ],
			[ 'EventsController', 'События' ],
			[ 'RequestsController', 'Заявки' ],
			[ 'CloudPaymentsController', 'Платежи CloudPayments' ],
			[ 'PhotoDocsController', 'Фото документов' ],
			[ 'PhotoClientController', 'Фото клиента' ],
			[ 'LoanDataController', 'Графики по договору' ],
			[ 'LoanClosedDataController', 'Закрытые договора' ],
			[ 'ClientsController', 'Клиенты' ],
			[ 'PaymentsExchangeController', 'Платежи ExchangePayments' ],
		] );
		$this->batchInsert( 'pcm_log_error_type', [ 'name' ], [
			[ 'Служебное' ],
			[ 'ТБД' ],
			[ 'Yii' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->execute("SET foreign_key_checks = 0;");
	    $this->truncateTable('pcm_log_exchange_name');
		$this->truncateTable('pcm_log_exchange_type');
		$this->truncateTable('pcm_log_exchange_stage');
		$this->truncateTable('pcm_log_error_type');
		$this->execute("SET foreign_key_checks = 1;");
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_133302_insert_to_pcm_log_tables cannot be reverted.\n";

		return false;
	}
	*/
}
