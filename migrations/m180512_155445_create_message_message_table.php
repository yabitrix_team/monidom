<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_message`.
 */
class m180512_155445_create_message_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'message_message', [
			'id'                => $this->primaryKey(),
			'parent_message_id' => $this->integer()->notNull(),
			'child_message_id'  => $this->integer()->notNull(),
		] );
		$this->addCommentOnTable( "message_message", "Структура связей сообщений \"Родитель-дитя\" " );
		$this->addCommentOnColumn( "message_message", "parent_message_id", "ID сообщение-родитель" );
		$this->addCommentOnColumn( "message_message", "child_message_id", "ID сообщение-дитя" );
		$this->addForeignKey(
			'fk-parent_message_id',
			'message_message',
			'parent_message_id',
			'message',
			'id',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-child_message_id',
			'message_message',
			'child_message_id',
			'message',
			'id',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'message_message' );
	}
}
