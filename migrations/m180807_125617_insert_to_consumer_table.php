<?php

use yii\db\Migration;

/**
 * Class m180807_125617_insert_to_consumer_table
 */
class m180807_125617_insert_to_consumer_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'consumer',
		                    [ 'name', 'created_at', 'active', 'sort' ],
		                    [
			                    [
				                    'partner-admin',
				                    $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
				                    1,
				                    1000,
			                    ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'consumer', [
			'name' => 'partner-admin',
		] );
	}
}
