<?php

use yii\db\Migration;

/**
 * Class
 * m180405_160129_add_foreign_key_for_auto_brand_id_auto_model_id_credit_product_id_point_id_user_id_client_employment_id_client_workplace_period_And_other_fk__to_requests_table
 */
class m180405_160129_add_foreign_key_for_auto_brand_id_auto_model_id_credit_product_id_point_id_user_id_client_employment_id_client_workplace_period_And_other_fk__to_requests_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-requests-auto_brand_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'auto_brand_id', // это имя поля в текущей таблице, которое будет ключом
			'auto_brands', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-auto_model_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'auto_model_id', // это имя поля в текущей таблице, которое будет ключом
			'auto_models', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-credit_product_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'credit_product_id', // это имя поля в текущей таблице, которое будет ключом
			'products', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-point_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'point_id', // это имя поля в текущей таблице, которое будет ключом
			'points', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-user_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'user_id', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-client_region_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'client_region_id', // это имя поля в текущей таблице, которое будет ключом
			'regions', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-client_employment_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'client_employment_id', // это имя поля в текущей таблице, которое будет ключом
			'employments', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-client_workplace_period_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'client_workplace_period_id', // это имя поля в текущей таблице, которое будет ключом
			'workplace_periods', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-client_guarantor_relation_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'client_guarantor_relation_id', // это имя поля в текущей таблице, которое будет ключом
			'people_relations', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-requests-method_of_issuance_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'method_of_issuance_id', // это имя поля в текущей таблице, которое будет ключом
			'methods_issuance', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-requests-auto_brand_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-auto_brand_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-auto_model_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-auto_model_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-credit_product_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-credit_product_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-point_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-point_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-user_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-user_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-client_region_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-client_region_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-client_employment_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-client_employment_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-client_workplace_period_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-client_workplace_period_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-client_guarantor_relation_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-client_guarantor_relation_id',
			'requests'
		);
		$this->dropForeignKey(
			'fk-requests-method_of_issuance_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-method_of_issuance_id',
			'requests'
		);
	}
}
