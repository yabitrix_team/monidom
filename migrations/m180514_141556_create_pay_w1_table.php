<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_w1`.
 */
class m180514_141556_create_pay_w1_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pay_w1', [
			'id'         => $this->primaryKey(),
			'payment'    => $this->integer()->notNull(),
			'action'     => "enum('pay', 'fail')",
			'status'     => $this->string()->notNull(),
			'date'       => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
			'first_name' => $this->string()->notNull(),
			'last_name'  => $this->string()->notNull(),
			'email'      => $this->string()->notNull(),
			'fee'        => $this->decimal( 14, 2 )->notNull(),
			'data'       => $this->text()->notNull(),
			'created_at' => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
		] );
		$this->addForeignKey(
			'fk-pay_w1-payment',  // это "условное имя" ключа
			'pay_w1', // это название текущей таблицы
			'payment', // это имя поля в текущей таблице, которое будет ключом
			'pay_payment', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'pay_w1' );
	}
}
