<?php

use yii\db\Migration;

/**
 * Class m180403_145019_add_foreign_key_for_log_id_to_pcm_log_error_table
 */
class m180403_145019_add_foreign_key_for_log_id_to_pcm_log_error_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-pcm_log_error-log_id',  // это "условное имя" ключа
			'pcm_log_error', // это название текущей таблицы
			'log_id', // это имя поля в текущей таблице, которое будет ключом
			'pcm_log', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-pcm_log_error-log_id',
			'pcm_log_error'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180403_145019_add_foreign_key_for_log_id_to_pcm_log_error_table cannot be reverted.\n";

		return false;
	}
	*/
}
