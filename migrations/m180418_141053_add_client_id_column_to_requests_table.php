<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `requests`.
 */
class m180418_141053_add_client_id_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'requests', 'client_id', $this->integer( 11 )->after( 'user_id' ) );
		$this->addForeignKey(
			'fk-requests-client_id',  // это "условное имя" ключа
			'requests', // это название текущей таблицы
			'client_id', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-requests-client_id',
			'requests'
		);
		$this->dropIndex(
			'fk-requests-client_id',
			'requests'
		);
		$this->dropColumn( 'requests', 'client_id' );
	}
}
