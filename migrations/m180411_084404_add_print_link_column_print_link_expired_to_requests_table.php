<?php

use yii\db\Migration;

/**
 * Class m180411_084404_add_print_link_column_print_link_expired_to_requests_table
 */
class m180411_084404_add_print_link_column_print_link_expired_to_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn( 'requests', 'print_link_expired', 'datetime not null after print_link' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn( 'requests', 'print_link_expired' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180411_084404_add_print_link_column_print_link_expired_to_requests_table cannot be reverted.\n";

        return false;
    }
    */
}
