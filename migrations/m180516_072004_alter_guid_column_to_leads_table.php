<?php

use yii\db\Migration;

/**
 * Class m180516_072004_alter_guid_column_to_leads_table
 */
class m180516_072004_alter_guid_column_to_leads_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'leads', 'guid', 'varchar(36) not null after id' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'leads', 'guid', 'varchar(32) not null after id' );
	}
}
