<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requests_metrika`.
 */
class m180803_123932_create_requests_metrika_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('requests_metrika', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'metrika_id' => $this->string()->notNull()->unique(),
            'user_id' => $this->integer()->notNull(),
            'metrika_type'=>"enum('ya', 'goo')",
            'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
            'updated_at' => $this->datetime(),
        ]);
	    $this->execute( "SET foreign_key_checks = 0;" );
	    $this->addForeignKey(
		    'fk-requests_metrika-request_id',  // это "условное имя" ключа
		    'requests_metrika', // это название текущей таблицы
		    'request_id', // это имя поля в текущей таблице, которое будет ключом
		    'requests', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->execute( "SET foreign_key_checks = 1;" );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

	    $this->dropForeignKey('fk-requests_metrika-request_id', 'requests_metrika');
	    $this->dropTable('requests_metrika');

    }
}
