<?php

use yii\db\Migration;

/**
 * Class m180402_132740_add_foreign_key_for_message_id_to_message_attache_file_table
 */
class m180402_132740_add_foreign_key_for_message_id_to_message_attache_file_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-message_attache_file-message_id',  // это "условное имя" ключа
			'message_attache_file', // это название текущей таблицы
			'message_id', // это имя поля в текущей таблице, которое будет ключом
			'message', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-message_attache_file-message_id',
			'message_attache_file'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180402_132740_add_foreign_key_for_message_id_to_message_attache_file_table cannot be reverted.\n";

		return false;
	}
	*/
}
