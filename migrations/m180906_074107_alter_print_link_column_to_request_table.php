<?php

use yii\db\Migration;

/**
 * Class m180906_074107_alter_print_link_column_to_request_table
 */
class m180906_074107_alter_print_link_column_to_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->dropIndex( 'print_link', 'requests' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn( 'requests', 'print_link', 'varchar(8) unique' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_074107_alter_print_link_column_to_request_table cannot be reverted.\n";

        return false;
    }
    */
}
