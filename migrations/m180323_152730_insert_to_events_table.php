<?php

use yii\db\Migration;

/**
 * Class m180323_152730_insert_to_events_table
 */
class m180323_152730_insert_to_events_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'events', [ 'code', 'name' ], [
			[ '101', 'Подписан 1 пакет документов' ],
			[ '102', 'Подписан 2 пакет документов' ],
			[ '201', 'Перезапрос 1 пакета документов' ],
			[ '202', 'Перезапрос 2 пакета документов' ],
			[ '701', 'Отправлена предварительная заявка' ],
			[ '702', 'Отправлена полная заявка' ],
			[ '711', 'Отправлены файлы сканы документов' ],
			[ '712', 'Отправлены файлы фотографии авто' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'events', [ 'code' => '101' ] );
		$this->delete( 'events', [ 'code' => '102' ] );
		$this->delete( 'events', [ 'code' => '201' ] );
		$this->delete( 'events', [ 'code' => '202' ] );
		$this->delete( 'events', [ 'code' => '701' ] );
		$this->delete( 'events', [ 'code' => '702' ] );
		$this->delete( 'events', [ 'code' => '711' ] );
		$this->delete( 'events', [ 'code' => '712' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180323_152730_insert_to_events_table cannot be reverted.\n";

		return false;
	}
	*/
}
