<?php

use yii\db\Migration;

/**
 * Class m180503_160356_alter_commissions_columns_to_request_commission_table
 */
class m180503_160356_alter_commissions_columns_to_request_commission_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->dropColumn( "request_commission", "guid" );
		$this->dropColumn( "request_commission", "name" );
		$this->alterColumn( "request_commission", "commission_contract", "decimal(14,4) not null" );
		$this->alterColumn( "request_commission", "commission_cash_withdrawal", "decimal(14,4) not null" );
		$this->alterColumn( "request_commission", "monthly_fee", "decimal(14,4) not null" );
		$this->alterColumn( "request_commission", "received", "decimal(14,4) not null" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->addColumn( "request_commission", "guid", $this->char( 36 ) );
		$this->addColumn( "request_commission", "name", $this->string( 255 )->notNull() );
		$this->alterColumn( "request_commission", "commission_contract", $this->string( 255 )->notNull() );
		$this->alterColumn( "request_commission", "commission_cash_withdrawal", $this->string( 255 )->notNull() );
		$this->alterColumn( "request_commission", "monthly_fee", $this->integer( 11 )->notNull() );
		$this->alterColumn( "request_commission", "received", $this->integer( 11 )->notNull() );
	}
}
