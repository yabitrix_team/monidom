<?php

use yii\db\Migration;

/**
 * Handles the creation of table `c_cmp_loan_closed_agreement`.
 */
class m180202_082743_create_c_cmp_loan_closed_agreement_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'c_cmp_loan_closed_agreement', [
			'id'                    => $this->primaryKey(),
			'sid'                   => $this->string( 255 )->notNull(),
			'created_at'            => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'user_xml'              => $this->char( 36 )->notNull(),
			'order_id'              => $this->integer( 11 )->notNull(),
			'order_xml'             => $this->char( 36 )->notNull(),
			'order'                 => $this->string( 255 )->notNull(),
			'xml_id'                => $this->char( 36 )->notNull(),
			'status_xml'            => $this->char( 36 )->notNull(),
			'date_set_status'       => $this->datetime()->notNull(),
			'total_amount_payments' => $this->double()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'c_cmp_loan_closed_agreement' );
	}
}
