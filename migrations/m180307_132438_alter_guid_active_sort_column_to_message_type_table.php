<?php

use yii\db\Migration;

/**
 * Class m180307_132438_alter_guid_active_sort_column_to_message_type_table
 */
class m180307_132438_alter_guid_active_sort_column_to_message_type_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'message_type', 'guid', 'char(36) null' );
		$this->alterColumn( 'message_type', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'message_type', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'message_type', 'guid', 'char(36) not null' );
		$this->alterColumn( 'message_type', 'active', 'char(1) not null' );
		$this->alterColumn( 'message_type', 'sort', 'int(11) not null' );
	}
}
