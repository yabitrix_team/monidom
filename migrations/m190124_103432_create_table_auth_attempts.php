<?php

use yii\db\Migration;

/**
 * Class m190124_103432_create_table_auth_attempts
 */
class m190124_103432_create_table_auth_attempts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'auth_attempts',
            [
                'id'         => $this->primaryKey(),
                'user_id'    => $this->integer(),
                'failed'     => $this->integer()->defaultValue(0),
                'created_at' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
                'updated_at' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            ]
        );
        $this->addForeignKey(
            'fk-auth_attempts-user_id',  // это "условное имя" ключа
            'auth_attempts', // это название текущей таблицы
            'user_id', // это имя поля в текущей таблице, которое будет ключом
            'users', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('auth_attempts');
    }
}
