<?php

use yii\db\Migration;

/**
 * Class m180822_112842_add_columns_to_leads_table
 */
class m180822_112842_add_columns_to_leads_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'leads';

	/**
	 * {@inheritdoc}
	 */
	public function up() {

		$this->addColumn(
			$this->table,
			'sms_id',
			$this->string( 255 )->after( 'vin' )
		);
		$this->addColumn(
			$this->table,
			'leads_consumer_id',
			$this->integer( 11 )->after( 'vin' )
		);
		$this->alterColumn(
			$this->table,
			'is_closed',
			$this->tinyInteger( 1 )
			     ->defaultValue( 0 )
			     ->after( 'sms_id' )
		);
		$this->addForeignKey(
			'fk-' . $this->table . '-leads_consumer_id',  // это 'условное имя' ключа
			$this->table, // это название текущей таблицы
			'leads_consumer_id', // это имя поля в текущей таблице, которое будет ключом
			'leads_consumer', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function down() {

		$this->dropForeignKey(
			'fk-' . $this->table . '-leads_consumer_id',
			$this->table
		);
		$this->dropIndex(
			'fk-' . $this->table . '-leads_consumer_id',
			$this->table
		);
		$this->dropColumn( $this->table, 'leads_consumer_id' );
		$this->dropColumn( $this->table, 'sms_id' );
		$this->alterColumn(
			$this->table,
			'is_closed',
			$this->char( 1 )
			     ->defaultValue( 0 )
			     ->after( 'created_at' )
		);
	}
}
