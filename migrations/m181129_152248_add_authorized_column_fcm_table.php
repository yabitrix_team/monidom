<?php

use app\modules\app\v1\models\FCM;
use yii\db\Migration;

/**
 * Class m181129_152248_add_authorized_column_fcm_table
 */
class m181129_152248_add_authorized_column_fcm_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'fcm';
    /**
     * @var string - название дополнительной колонки
     */
    protected $column = 'authorized';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->tinyInteger(1)->defaultValue(0)->notNull()->after('os_type')
        );
        $this->addCommentOnColumn(
            $this->table,
            $this->column,
            'Колонка отображает показатель авторизован пользователь или нет'
        );
        $this->alterColumn($this->table, 'user_id', $this->integer(11));
        //меняем значение 0 на null в поле user_id
        Yii::$app->db
            ->createCommand()
            ->update($this->table, ['user_id' => null], 'user_id = 0')
            ->execute();
        $this->execute("SET foreign_key_checks = 0;");
        $this->addForeignKey(
            'fk-'.$this->table.'-user_id',  // это 'условное имя' ключа
            $this->table, // это название текущей таблицы
            'user_id', // это имя поля в текущей таблице, которое будет ключом
            'users', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE',
            'CASCADE'
        );
        $this->execute("SET foreign_key_checks = 1;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->dropForeignKey(
            'fk-'.$this->table.'-user_id',
            $this->table
        );
        $this->dropIndex(
            'fk-'.$this->table.'-user_id',
            $this->table
        );
        $this->execute("SET foreign_key_checks = 1;");
        //меняем значение null на о в поле user_id
        Yii::$app->db
            ->createCommand()
            ->update($this->table, ['user_id' => 0], 'user_id IS NULL')
            ->execute();
        $this->dropCommentFromColumn($this->table, $this->column);
        $this->dropColumn($this->table, $this->column);
        $this->alterColumn($this->table, 'user_id', $this->integer(11)->defaultValue(0)->notNull());
    }
}
