<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m180201_135933_create_message_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'message', [
			'id'           => $this->primaryKey(),
			'guid'         => $this->char( 36 )->notNull()->unique(),
			'name'         => $this->string( 255 )->notNull(),
			'user_id_from' => $this->integer( 11 )->notNull(),
			'user_id_to'   => $this->integer( 11 )->notNull(),
			'order_id'     => $this->integer( 11 )->notNull(),
			'message'      => $this->text()->notNull(),
			'type_id'      => $this->integer( 11 )->notNull(),
			'status_id'    => $this->integer( 11 )->notNull(),
			'created_at'   => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'   => $this->datetime()->notNull()
			                       ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'       => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'         => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'message' );
	}
}
