<?php

use yii\db\Migration;

/**
 * Class m180724_093007_insert_to_groups_table
 */
class m180724_093007_insert_to_groups_table extends Migration {
	// Use up()/down() to run migration code without a transaction.
	public function up() {

		$this->insert( 'cabinets', [
			"code"      => "f1e5d7a5fe13498abbdeb0f1f19136a8",
			"name"      => "ПУ",
			"subdomain" => "panel",
			"active"    => 1,
		] );
		$idCabinet = Yii::$app->db->getLastInsertId( "cabinets" );
		$this->insert( 'groups', [
			"identifier"       => "support",
			"name"             => "Сотрудники техподдержки",
			"prior_cabinet_id" => $idCabinet,
		] );
	}

	public function down() {

		$this->delete( 'cabinets', [ 'code' => 'f1e5d7a5fe13498abbdeb0f1f19136a8' ] );
		$this->delete( 'groups', [ 'identifier' => 'support' ] );
	}
}
