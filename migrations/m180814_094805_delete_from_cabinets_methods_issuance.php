<?php

use yii\db\Migration;

/**
 * Class m180814_094805_delete_from_cabinets_methods_issuance
 */
class m180814_094805_delete_from_cabinets_methods_issuance extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->delete( 'cabinets_methods_issuance', [
			'cabinet_id' => 1,
			'method_id'  => 1,
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->insert( 'cabinets_methods_issuance', [
			'cabinet_id' => 1,
			'method_id'  => 1,
		] );
	}
}
