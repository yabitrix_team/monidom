<?php

use yii\db\Migration;

/**
 * Class m180711_150039_alter_pay_vendor_table
 */
class m180711_150039_alter_pay_vendor_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		yii::$app->db->createCommand( "SET foreign_key_checks=0;" )->execute();
		$this->dropIndex( 'code', 'pay_vendor' );
		$this->dropTable( 'pay_vendor' );
		$this->createTable( 'pay_vendor', [
			'id'          => $this->primaryKey(),
			'name'        => $this->string()->notNull(),
			'code'        => $this->string()->notNull()->unique(),
			'table_name'  => $this->string(),
			'publicId'    => $this->string(),
			'secret'      => $this->string(),
			'merchant_id' => $this->string(),
			'url_success' => $this->string(),
			'url_fail'    => $this->string(),
			'secret_test'      => $this->string(),
			'merchant_id_test' => $this->string(),
			'url_success_test' => $this->string(),
			'url_fail_test'    => $this->string(),
			'description' => $this->string(),
			'currency_id' => $this->integer(),
			'file_oferta' => $this->string(),
			'test_field1' => $this->string(),
			'test_field2' => $this->string(),
			'active'      => $this->tinyInteger()->defaultValue( 1 )->notNull(),
			'created_at'  => $this->timestamp()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
		] );
		$this->batchInsert( 'pay_vendor', [
			'name',
			'code',
			'table_name',
			'publicId',
			'secret',
			'merchant_id',
			'url_success',
			'url_fail',
			'secret_test',
			'merchant_id_test',
			'url_success_test',
			'url_fail_test',
			'description',
			'currency_id',
			'file_oferta',
			'test_field1',
			'test_field2',
			'active',
		], [
			                    [
				                    'Cloud Payments',
				                    'CloudPayments',
				                    'pay_cloud',
				                    'pk_dd99e190825e9b3822845e1dcfcda',
				                    '6886b3be92ae5a939f63234b790fd21c',
				                    '',
				                    '',
				                    '',
				                    '',
				                    '',
				                    '',
				                    '',
				                    'Оплата в carmoney.ru',
				                    '643',
				                    '',
				                    'OperationType',
				                    'CardFirstSix',
				                    '1',
			                    ],
			                    [
				                    'Wallet One',
				                    'WalletOne',
				                    'pay_w1',
				                    '',
				                    '5d4d6864575a573044606f69345f5a4865355f6657666078626e55', // боевой аккаунт
				                    '177649516664',
				                    '/w1/success',
				                    '/w1/fail',
				                    '4e6b5c36634c314f69694170725a3866436f4b7763417a565a574c', // тестовый аккаунт
				                    '165319823612',
				                    '/w1/success',
				                    '/w1/fail',
				                    'Оплата в carmoney.ru',
				                    '643',
				                    'w1_oferta.pdf',
				                    'WMI_SIGNATURE',
				                    'WMI_ORDER_STATE',
				                    0,
			                    ],
		                    ] );
		//$this->createIndex('code', 'pay_vendor', 'code');
		Yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		Yii::$app->db->createCommand( "SET foreign_key_checks=0;" )->execute();
		$this->dropTable( 'pay_vendor' );
		$this->createTable( 'pay_vendor', [
			'id'     => $this->primaryKey(),
			'name'   => $this->string()->notNull(),
			'code'   => $this->string()->notNull()->unique(),
			'active' => $this->tinyInteger()->defaultValue( 1 )->notNull(),
		] );
		$this->batchInsert( 'pay_vendor', [ 'name', 'active', 'code' ], [
			[ 'Cloud Payments', 1, 'cloud' ],
			[ 'Wallet One', 1, 'w1' ],
		] );
		Yii::$app->db->createCommand( "SET foreign_key_checks=1;" )->execute();
	}
}
