<?php

use yii\db\Migration;

/**
 * Class m180418_133043_update_to_groups_table
 */
class m180418_133043_update_to_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//0-ЛКП, 1-ЛКК, 2-ЛКВМ, 3-ЛКИ
		$cabinets = Yii::$app->db->createCommand( 'SELECT
  *
FROM cabinets
WHERE GUID IN (
\'caf2f5a8-714b-4c38-3a68-9\', 
\'caf2f5a8-714b-4c38-3a68-a\',
\'caf2f5a8-714b-4c38-3a68-b\',
\'caf2f5a8-714b-4c38-3a68-c\'
)' )
		                         ->queryAll();
		$this->update( 'groups',
		               [ 'name' => 'Агенты' ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\''
		);
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $cabinets[2]['id'] ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\''
		);
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $cabinets[0]['id'] ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf21\''
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		//0-ЛКП, 1-ЛКК, 2-ЛКВМ, 3-ЛКИ
		$cabinets = Yii::$app->db->createCommand( 'SELECT
  *
FROM cabinets
WHERE GUID IN (
\'caf2f5a8-714b-4c38-3a68-9\', 
\'caf2f5a8-714b-4c38-3a68-a\',
\'caf2f5a8-714b-4c38-3a68-b\',
\'caf2f5a8-714b-4c38-3a68-c\'
)' )
		                         ->queryAll();
		$this->update( 'groups',
		               [ 'name' => 'Пользователи' ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\''
		);
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $cabinets[0]['id'] ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\''
		);
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => null ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf21\''
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180418_133043_update_to_groups_table cannot be reverted.\n";

		return false;
	}
	*/
}
