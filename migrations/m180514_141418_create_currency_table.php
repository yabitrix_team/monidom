<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m180514_141418_create_currency_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'currency', [
			'id'     => $this->integer()->unique()->notNull(),
			'code'   => $this->string()->notNull(),
			'symbol' => $this->string()->notNull(),
		] );
		$this->batchInsert( 'currency', [ 'id', 'code', 'symbol' ], [
			[ 643, 'RUB', '₽' ],
			[ 840, 'USD', '$' ],
			[ 978, 'EUR', '€' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'currency' );
	}
}
