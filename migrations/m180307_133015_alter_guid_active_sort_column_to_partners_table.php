<?php

use yii\db\Migration;

/**
 * Class m180307_133015_alter_guid_active_sort_column_to_partners_table
 */
class m180307_133015_alter_guid_active_sort_column_to_partners_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'partners', 'guid', 'char(36) null' );
		$this->alterColumn( 'partners', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'partners', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'partners', 'guid', 'char(36) not null' );
		$this->alterColumn( 'partners', 'active', 'char(1) not null' );
		$this->alterColumn( 'partners', 'sort', 'int(11) not null' );
	}
}
