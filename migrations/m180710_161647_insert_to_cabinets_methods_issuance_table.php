<?php

use yii\db\Migration;

/**
 * Class m180710_161647_insert_to_cabinets_methods_issuance_table
 */
class m180710_161647_insert_to_cabinets_methods_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 2]);
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '2',
		    "method_id"       => '2',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '2',
		    "method_id"       => '8',
	    ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_161647_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_161647_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }
    */
}
