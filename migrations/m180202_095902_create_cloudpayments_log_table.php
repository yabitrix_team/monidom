<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cloudpayments_log`.
 */
class m180202_095902_create_cloudpayments_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'cloudpayments_log', [
			'id'                => $this->primaryKey(),
			'created_at'        => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'type'              => $this->string( 9 )->notNull(),
			'transactionid'     => $this->string( 255 )->notNull(),
			'amount'            => $this->string( 255 )->notNull(),
			'currency'          => $this->string( 3 )->notNull(),
			'paymentamount'     => $this->string( 255 )->notNull(),
			'paymentcurrency'   => $this->string( 255 )->notNull(),
			'invoiceid'         => $this->string( 255 )->notNull(),
			'accountid'         => $this->string( 255 )->notNull(),
			'subscriptionid'    => $this->string( 255 )->notNull(),
			'name'              => $this->string( 255 )->notNull(),
			'email'             => $this->string( 255 )->notNull(),
			'datetime'          => $this->string( 255 )->notNull(),
			'ipaddress'         => $this->string( 255 )->notNull(),
			'ipcountry'         => $this->string( 255 )->notNull(),
			'ipcity'            => $this->string( 255 )->notNull(),
			'ipregion'          => $this->string( 255 )->notNull(),
			'ipdistrict'        => $this->string( 255 )->notNull(),
			'iplatitude'        => $this->string( 255 )->notNull(),
			'iplongitude'       => $this->string( 255 )->notNull(),
			'cardfirstsix'      => $this->integer( 11 )->notNull(),
			'cardlastfour'      => $this->integer( 11 )->notNull(),
			'cardtype'          => $this->string( 255 )->notNull(),
			'cardexpdate'       => $this->string( 255 )->notNull(),
			'issuer'            => $this->string( 255 )->notNull(),
			'issuerbankcountry' => $this->string( 255 )->notNull(),
			'description'       => $this->string( 255 )->notNull(),
			'testmode'          => $this->integer( 11 )->notNull(),
			'status'            => $this->string( 255 )->notNull(),
			'authcode'          => $this->string( 255 )->notNull(),
			'token'             => $this->string( 255 )->notNull(),
			'statuscode'        => $this->integer( 11 )->notNull(),
			'reason'            => $this->string( 255 )->notNull(),
			'reasoncode'        => $this->integer( 11 )->notNull(),
			'data'              => $this->string( 255 )->notNull(),
			'send_to_1c'        => $this->string( 255 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'cloudpayments_log' );
	}
}
