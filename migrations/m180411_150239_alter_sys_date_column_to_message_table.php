<?php

use yii\db\Migration;

/**
 * Class m180411_150239_alter_sys_date_column_to_message_table
 */
class m180411_150239_alter_sys_date_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'sys_date', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'sys_date', 'DATETIME NULL' );
	}
}
