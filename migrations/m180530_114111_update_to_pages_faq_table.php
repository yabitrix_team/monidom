<?php

use yii\db\Migration;

/**
 * Class m180530_114111_update_to_pages_faq_table
 */
class m180530_114111_update_to_pages_faq_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->update( 'pages_faq', ['active' => 0], 'name = "Базовые условия продукта"' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->update( 'pages_faq', ['active' => 1] ,  'name = "Базовые условия продукта"');



    }

}
