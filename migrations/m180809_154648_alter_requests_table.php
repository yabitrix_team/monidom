<?php

use yii\db\Migration;

/**
 * Class m180809_154648_alter_requests_table
 */
class m180809_154648_alter_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('requests', 'place_of_stay', $this->string());
		$this->addCommentOnColumn('requests', 'place_of_stay', 'Место пребывания');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropCommentFromColumn('requests', 'place_of_stay');
    	$this->dropColumn('requests', 'place_of_stay');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_154648_alter_requests_table cannot be reverted.\n";

        return false;
    }
    */
}
