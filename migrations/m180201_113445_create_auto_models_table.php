<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_models`.
 */
class m180201_113445_create_auto_models_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'auto_models', [
			'id'            => $this->primaryKey(),
			'guid'          => $this->char( 36 )->notNull()->unique(),
			'name'          => $this->string( 255 )->notNull(),
			'auto_brand_id' => $this->integer( 11 )->notNull(),
			'created_at'    => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'    => $this->datetime()->notNull()
			                        ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'        => $this->char( 1 )->notNull()->defaultValue( 1 ),
			'sort'          => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'auto_models' );
	}
}
