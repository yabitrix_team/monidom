<?php

use yii\db\Migration;

/**
 * Class m180404_152627_alter_point_id_agent_id_column_to_users_table
 */
class m180404_152627_alter_point_id_agent_id_column_to_users_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'users', 'point_id', $this->integer( 11 ) );
		$this->alterColumn( 'users', 'agent_id', $this->integer( 11 ) );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'users', 'point_id', $this->integer( 11 )->notNull() );
		$this->alterColumn( 'users', 'agent_id', $this->integer( 11 )->notNull() );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_152627_alter_point_id_agent_id_column_to_users_table cannot be reverted.\n";

		return false;
	}
	*/
}
