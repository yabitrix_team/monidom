<?php

use yii\db\Migration;

/**
 * Class m180507_111008_alter_kladr_column_to_regions_table
 */
class m180507_111008_alter_kladr_column_to_regions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn("regions", "kladr", $this->string()->unique()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn("regions", "kladr", $this->string()->notNull());
	    $this->dropIndex("kladr", "regions");
    }
}