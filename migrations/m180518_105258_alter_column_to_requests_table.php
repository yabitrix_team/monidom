<?php

use yii\db\Migration;

/**
 * Class m180518_105258_alter_column_to_requests_table
 */
class m180518_105258_alter_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'date_return', $this->date() );
		$this->alterColumn( 'requests', 'client_first_name', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_last_name', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_patronymic', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_mobile_phone', $this->string( 20 ) );
		$this->alterColumn( 'requests', 'client_email', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_passport_serial_number', $this->char( 4 ) );
		$this->alterColumn( 'requests', 'client_passport_number', $this->char( 6 ) );
		$this->alterColumn( 'requests', 'client_home_phone', $this->string( 20 ) );
		$this->alterColumn( 'requests', 'client_workplace_experience', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_workplace_address', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_workplace_phone', $this->string( 20 ) );
		$this->alterColumn( 'requests', 'client_guarantor_name', $this->string( 255 ) );
		$this->alterColumn( 'requests', 'client_guarantor_phone', $this->string( 20 ) );
		$this->alterColumn( 'requests', 'roystat_id', $this->string( 50 ) );
		$this->alterColumn( 'requests', 'advert_channel', $this->string( 50 ) );
		$this->alterColumn( 'requests', 'referal_code', $this->string( 50 ) );
		$this->alterColumn( 'requests', 'request_source', $this->string( 50 ) );
		$this->alterColumn( 'requests', 'comment_client', $this->text() );
		$this->alterColumn( 'requests', 'comment_partner', $this->text() );
		$this->alterColumn( 'requests', 'print_link_expired', $this->dateTime() );
		$this->alterColumn( 'requests', 'client_workplace_name', $this->string( 255 ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'date_return', $this->date()->notNull() );
		$this->alterColumn( 'requests', 'client_first_name', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_last_name', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_patronymic', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_mobile_phone', $this->string( 20 )->notNull() );
		$this->alterColumn( 'requests', 'client_email', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_passport_serial_number', $this->char( 4 )->notNull() );
		$this->alterColumn( 'requests', 'client_passport_number', $this->char( 6 )->notNull() );
		$this->alterColumn( 'requests', 'client_home_phone', $this->string( 20 )->notNull() );
		$this->alterColumn( 'requests', 'client_workplace_experience', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_workplace_address', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_workplace_address', $this->string( 20 )->notNull() );
		$this->alterColumn( 'requests', 'client_guarantor_name', $this->string( 255 )->notNull() );
		$this->alterColumn( 'requests', 'client_guarantor_phone', $this->string( 20 )->notNull() );
		$this->alterColumn( 'requests', 'roystat_id', $this->string( 50 )->notNull() );
		$this->alterColumn( 'requests', 'advert_channel', $this->string( 50 )->notNull() );
		$this->alterColumn( 'requests', 'referal_code', $this->string( 50 )->notNull() );
		$this->alterColumn( 'requests', 'request_source', $this->string( 50 )->notNull() );
		$this->alterColumn( 'requests', 'comment_client', $this->text()->notNull() );
		$this->alterColumn( 'requests', 'comment_partner', $this->text()->notNull() );
		$this->alterColumn( 'requests', 'print_link_expired', $this->dateTime()->notNull() );
		$this->alterColumn( 'requests', 'client_workplace_name', $this->string( 255 )->notNull() );
	}
}
