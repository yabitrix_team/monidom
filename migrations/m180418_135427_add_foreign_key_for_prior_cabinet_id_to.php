<?php

use yii\db\Migration;

/**
 * Class m180418_135427_add_foreign_key_for_prior_cabinet_id_to
 */
class m180418_135427_add_foreign_key_for_prior_cabinet_id_to extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addForeignKey(
		    'fk-group-prior_cabinet_id',  // это "условное имя" ключа
		    'groups', // это название текущей таблицы
		    'prior_cabinet_id', // это имя поля в текущей таблице, которое будет ключом
		    'cabinets', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropForeignKey(
		    'fk-group-prior_cabinet_id',
		    'groups'
	    );
	    $this->dropIndex(
		    'fk-group-prior_cabinet_id',
		    'groups'
	    );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180418_135427_add_foreign_key_for_prior_cabinet_id_to cannot be reverted.\n";

        return false;
    }
    */
}
