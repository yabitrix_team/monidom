<?php

use yii\db\Migration;

/**
 * Class m181212_125910_add_events_table
 */
class m181212_125910_add_events_table extends Migration
{

    protected $tableName = 'events';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert( $this->tableName, [ 'code', 'name', 'active' ], [
            [ '810', 'Запрос url для проверки карты', 1],
            [ '811', 'Проверка карты прошла успешно', 1],
            [ '812', 'Проверка карты прошла с ошибкой', 1],
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete($this->tableName, "code IN ('810','811','812')");
    }
}
