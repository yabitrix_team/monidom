<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `column_name_from_request_doc`.
 */
class m180322_134944_drop_column_name_from_request_doc_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->dropColumn( 'request_docs', 'name' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->addColumn( 'request_docs', 'name', $this->char()->after( 'id' ) );
	}
}
