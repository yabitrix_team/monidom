<?php

use yii\db\Migration;

/**
 * Class m181121_104020_insert_to_pages_faq_table
 */
class m181121_104020_insert_to_pages_faq_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $html = '<table style="width: 100%"><tr><td><a href="/path/docs/Pamyatka%20po%20oformleniy%20documentov.pdf" download target="_blank" title="Памятка по оформлению документов">Памятка по оформлению документов.pdf</a></td></tr></table>';
        $this->insert( 'pages_faq', [
            "name"        => 'Памятка по оформлению документов',
            "text"        => $html,
            "consumer_id" => 2,
            "active"      => 1,
            "sort"        => 900,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('pages_faq', [
           'name' => 'Памятка по оформлению документов'
        ]);
    }
}
