<?php

use yii\db\Migration;

/**
 * Class m180426_121254_add_color_columt_to_status_table
 */
class m180426_121254_add_color_column_to_statuses_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'statuses', 'color', 'VARCHAR(6) NOT NULL AFTER `progress`' );
		$this->update( 'statuses', ["color"=>"BBF4FE"], [ 'color' => '' ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'statuses', 'color' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180426_121254_add_color_columt_to_status_table cannot be reverted.\n";

		return false;
	}
	*/
}
