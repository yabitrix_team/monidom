<?php

use yii\db\Migration;

/**
 * Class m180302_074131_Seed_Regions
 */
class m180302_074131_Seed_Regions extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//seed regions
		$this->batchInsert( 'regions', [ 'guid', 'name', 'kladr', 'active', 'sort' ], [
			[ 'bae2fca6-610e-4be7-9cf7-7', 'Самарская обл', '6300000000000', '1', '100' ],
			[ 'cf3fefde-3dd6-4e02-a38a-f', 'Санкт-Петербург г', '7800000000000', '1', '200' ],
			[ 'e9a95739-d031-4f18-9a6a-b', 'Крым Респ', '9100000000000', '1', '300' ],
			[ 'f29778a5-e20c-4aad-a586-a', 'Свердловская обл', '6600000000000', '1', '400' ],
			[ '8e0b06a7-6c96-4b94-8d2f-7', 'Тверская обл', '6900000000000', '1', '500' ],
			[ '8fe74f31-ab19-4997-87e9-0', 'Псковская обл', '	6000000000000', '1', '600' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'regions', [ 'guid' => 'bae2fca6-610e-4be7-9cf7-7' ] );
		$this->delete( 'regions', [ 'guid' => 'cf3fefde-3dd6-4e02-a38a-f' ] );
		$this->delete( 'regions', [ 'guid' => 'e9a95739-d031-4f18-9a6a-b' ] );
		$this->delete( 'regions', [ 'guid' => 'f29778a5-e20c-4aad-a586-a' ] );
		$this->delete( 'regions', [ 'guid' => '8e0b06a7-6c96-4b94-8d2f-7' ] );
		$this->delete( 'regions', [ 'guid' => '8fe74f31-ab19-4997-87e9-0' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_074131_Seed_Regions cannot be reverted.\n";

		return false;
	}
	*/
}
