<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages_faq`.
 */
class m180428_114226_create_pages_faq_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pages_faq', [
			'id'          => $this->primaryKey(),
			'name'        => $this->string( 255 ),
			'text'        => $this->text()->notNull(),
			'consumer_id' => $this->integer( 11 ),
			'created_at'  => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'  => $this->datetime()->notNull()
			                      ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'      => 'TINYINT(1) NOT NULL DEFAULT 1',
			'sort'        => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'pages_faq' );
		$this->dropTable( 'pages_faq' );
	}
}
