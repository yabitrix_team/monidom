<?php

use app\modules\app\v1\models\RequestsOrigin;
use yii\db\Migration;

/**
 * Handles the creation of table `leads_consumer`.
 */
class m180822_102827_create_leads_consumer_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'leads_consumer';

	/**
	 * {@inheritdoc}
	 * @throws \yii\db\Exception
	 */
	public function safeUp() {

		$this->createTable( $this->table, [
			'id'                 => $this->primaryKey(),
			'name'               => $this->string( 255 ),
			'identifier'         => $this->string( 255 ),
			'requests_origin_id' => $this->integer( 11 ),
			'sms_text'           => $this->string( 255 ),
			'active'             => $this->tinyInteger( 1 )->defaultValue( 1 ),
			'updated_at'         => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ),
			'created_at'         => $this->datetime()->defaultExpression( 'CURRENT_TIMESTAMP' ),
		] );
		//добавялем индексы
		$this->createIndex( 'identifier', $this->table, 'identifier', true );
		$this->addForeignKey(
			'fk-' . $this->table . '-requests_origin_id',  // это 'условное имя' ключа
			$this->table, // это название текущей таблицы
			'requests_origin_id', // это имя поля в текущей таблице, которое будет ключом
			'requests_origin', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		//получение места создания заявки с последующей установкой значений
		$res   = ( new RequestsOrigin() )->getAll();
		$assoc = array_column( $res, 'id', 'identifier' );
		$this->batchInsert( $this->table, [ 'name', 'identifier', 'sms_text', 'requests_origin_id' ], [
			[
				'Мобильное Приложение',
				'mobile',
				'Вам предварительно сформирована заявка. Для продолжения оформления скачайте мобильное приложение по ссылке https://carmoney.ru/mobile-app/',
				$assoc['MOBILE'],
			],
			[
				'Личный Кабинет Клиента',
				'client',
				'Вам предварительно одобрен займ. Для продолжения оформления перейдите по ссылке %s',
				$assoc['CLIENT'],
			],
		] );
		$this->addCommentOnTable( $this->table, 'Таблица содержит потребителей лидов со связями с местом создания заявки' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( $this->table );
	}
}
