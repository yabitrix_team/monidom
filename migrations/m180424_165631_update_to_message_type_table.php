<?php

use yii\db\Migration;

/**
 * Class m180424_165631_update_to_message_type_table
 */
class m180424_165631_update_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'message_type', [
			'public' => 0,
		], [
			               "name" => "status",
		               ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'message_type', [
			'public' => 1,
		], [
			               "name" => "status",
		               ] );
	}
}
