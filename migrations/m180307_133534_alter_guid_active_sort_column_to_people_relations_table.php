<?php

use yii\db\Migration;

/**
 * Class m180307_133534_alter_guid_active_sort_column_to_people_relations_table
 */
class m180307_133534_alter_guid_active_sort_column_to_people_relations_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'people_relations', 'guid', 'char(36) null' );
		$this->alterColumn( 'people_relations', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'people_relations', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'people_relations', 'guid', 'char(36) not null' );
		$this->alterColumn( 'people_relations', 'active', 'char(1) not null' );
		$this->alterColumn( 'people_relations', 'sort', 'int(11) not null' );
	}
}
