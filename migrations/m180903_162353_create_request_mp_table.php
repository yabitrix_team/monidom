<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_mp`.
 */
class m180903_162353_create_request_mp_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'request_mp';

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( $this->table, [
			'id'           => $this->primaryKey(),
			'request_id'   => $this->integer( 11 ),
			'service_info' => $this->string( 255 ),
			'updated_at'   => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ),
			'created_at'   => $this->datetime()->defaultExpression( 'CURRENT_TIMESTAMP' ),
		] );
		$this->addForeignKey(
			'fk-' . $this->table . '-request_id',  // это 'условное имя' ключа
			$this->table, // это название текущей таблицы
			'request_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->createIndex( 'request_id', $this->table, 'request_id', true );
		$this->addCommentOnTable( $this->table, 'Таблица содержит информацию мобильного приложения по каждой заявке' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( $this->table );
	}
}
