<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_error_type`.
 */
class m180202_083908_create_pcm_log_error_type_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_error_type', [
			'id'   => $this->primaryKey(),
			'name' => $this->string( 255 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_error_type' );
	}
}
