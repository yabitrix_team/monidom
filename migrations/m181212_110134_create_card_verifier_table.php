<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bank_card`.
 */
class m181212_110134_create_card_verifier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('card_verifier', [
            'id' => $this->primaryKey(),
            'chain_id' => $this->string()->comment('Цепочка для связи событий'),
            'request_id' => $this->string()->comment('code заявки'),
            'external_id' => $this->string()->comment('внутренний идентификатор запроса'),
            'operation_id' => $this->string()->comment('код операции W1'),
            'amount' => $this->string()->comment('сумма операции'),
            'url' => $this->string()->comment('url фрейма'),
            'card' => $this->string()->comment('masked card'),
            'token' => $this->string()->comment('токен для карты'),
            'event_id' => $this->string()->comment('id события заявки'),
            'updated_at'   => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ),
            'created_at'   => $this->datetime()->defaultExpression( 'CURRENT_TIMESTAMP' ),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('card_verifier');
    }
}
