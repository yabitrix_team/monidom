<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_exchange_stage`.
 */
class m180202_093250_create_pcm_log_exchange_stage_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_exchange_stage', [
			'id'   => $this->primaryKey(),
			'code' => $this->string( 255 )->notNull(),
			'name' => $this->string( 255 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_exchange_stage' );
	}
}
