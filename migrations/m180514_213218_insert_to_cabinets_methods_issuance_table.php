<?php

use yii\db\Migration;

/**
 * Class m180514_213218_insert_to_cabinets_methods_issuance_table
 */
class m180514_213218_insert_to_cabinets_methods_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '1',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '2',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '3',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '4',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '5',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '6',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '7',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '8',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '2',
		    "method_id"       => '2',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '2',
		    "method_id"       => '4',
	    ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180514_213218_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180514_213218_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }
    */
}
