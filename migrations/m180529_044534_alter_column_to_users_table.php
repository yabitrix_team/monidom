<?php

use yii\db\Migration;

/**
 * Class m180529_044534_alter_column_to_users_table
 */
class m180529_044534_alter_column_to_users_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'users', 'username', $this->string( 255 ) );
		$this->alterColumn( 'users', 'email', $this->string( 255 ) );
		$this->alterColumn( 'users', 'auth_key', $this->string( 255 ) );
		$this->alterColumn( 'users', 'password_reset_token', $this->string( 255 ) );
		$this->alterColumn( 'users', 'last_name', $this->string( 255 ) );
		$this->alterColumn( 'users', 'second_name', $this->string( 255 ) );
		$this->alterColumn( 'users', 'is_accreditated', $this->char( 1 ) );
		$this->alterColumn( 'users', 'inside_support_token', $this->string( 255 ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'users', 'username', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'email', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'auth_key', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'password_reset_token', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'last_name', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'second_name', $this->string( 255 )->notNull() );
		$this->alterColumn( 'users', 'is_accreditated', $this->char( 1 )->notNull() );
		$this->alterColumn( 'users', 'inside_support_token', $this->string( 255 )->notNull() );
	}
}
