<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180405_135002_insert_to_message_status_table
 */
class m180405_135002_insert_to_message_status_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		// заполняем данными по статусам сообщений
		$this->batchInsert( 'message_status',
		                    [ 'guid', 'name', 'active', 'sort' ],
		                    [
			                    [ Uuid::uuid4(), 'new', 1, 'sort' ],
			                    [ Uuid::uuid4(), 'read', 1, 'sort' ],
			                    [ Uuid::uuid4(), 'answered', 1, 'sort' ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		// очищаем
		$this->execute( "SET foreign_key_checks = 0;" );
		$this->execute( "TRUNCATE message_status" );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
