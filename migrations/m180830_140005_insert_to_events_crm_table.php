<?php

use yii\db\Migration;

/**
 * Class m180830_140005_insert_to_events_crm_table
 */
class m180830_140005_insert_to_events_crm_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'events_crm';

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( $this->table, [ 'code', 'guid', 'name', 'active', 'sort' ], [
			[
				510,
				'0c6d5dd2-aba6-11e8-a2b9-00155d941906',
				'Регистрация в МП',
				1,
				150,
			],
			[
				610,
				'109ddc10-ac66-11e8-a2b9-00155d941906',
				'Предварительная верификация в МП',
				1,
				160,
			],
			[
				710,
				'2a163125-aba6-11e8-a2b9-00155d941906',
				'Начало прикрепления фото в МП',
				1,
				170,
			],
		] );
		$this->alterColumn(
			$this->table,
			'active',
			$this->tinyInteger( 1 )
			     ->defaultValue( 1 )
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( $this->table, 'code=510' );
		$this->delete( $this->table, 'code=610' );
		$this->delete( $this->table, 'code=710' );
		$this->alterColumn(
			$this->table,
			'active',
			$this->char( 1 )
			     ->defaultValue( 'Y' )
			     ->notNull()
		);
	}
}
