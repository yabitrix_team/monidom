<?php

use yii\db\Migration;

/**
 * Class m180307_135600_rename_column_credit_product_client_employment_client_workplace_period_in_requests_table
 */
class m180307_135600_rename_column_credit_product_client_employment_client_workplace_period_in_requests_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'client_workplace_period', 'int(11) not null' );
		$this->renameColumn( 'requests', 'credit_product', 'credit_product_id' );
		$this->renameColumn( 'requests', 'client_employment', 'client_employment_id' );
		$this->renameColumn( 'requests', 'client_workplace_period', 'client_workplace_period_id' );
		$this->alterColumn( 'requests', 'guid', 'char(36) null' );
		$this->alterColumn( 'requests', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'requests', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'client_workplace_period_id', 'varchar(255) not null' );
		$this->renameColumn( 'requests', 'credit_product_id', 'credit_product' );
		$this->renameColumn( 'requests', 'client_employment_id', 'client_employment' );
		$this->renameColumn( 'requests', 'client_workplace_period_id', 'client_workplace_period' );
		$this->alterColumn( 'requests', 'guid', 'char(36) not null' );
		$this->alterColumn( 'requests', 'active', 'char(1) not null' );
		$this->alterColumn( 'requests', 'sort', 'int(11) not null' );
	}
}
