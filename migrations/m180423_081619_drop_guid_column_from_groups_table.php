<?php

use yii\db\Migration;

/**
 * Handles dropping guid from table `groups`.
 */
class m180423_081619_drop_guid_column_from_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->dropIndex( 'guid', 'groups' );
		$this->dropColumn( 'groups', 'guid' );
		$this->addColumn( 'groups', 'identifier', $this->string( 150 )->after( 'id' ) );
		$this->createIndex( 'identifier', 'groups', 'identifier', true );
		$arIdent = [
			'admin'         => 'Администраторы',
			'manager'       => 'Менеджеры',
			'agent'         => 'Агенты',
			'client'        => 'Клиенты',
			'investor'      => 'Инвесторы',
			'partner_admin' => 'Администраторы партнера',
			'partner_user'  => 'Пользователи партнера',
			'mobile_user'   => 'Пользователи мобильного приложения',
		];
		foreach ( $arIdent as $k => $v ) {
			$this->update( 'groups', [ 'identifier' => $k ], 'name = "' . $v . '"' );
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'identifier', 'groups' );
		$this->dropColumn( 'groups', 'identifier' );
		$this->addColumn( 'groups', 'guid', $this->char( 36 )->after( 'id' ) );
		$this->createIndex( 'guid', 'groups', 'guid', true );
		$arGuid = [
			'46b775b8-d8c0-11e7-814a-00155d01bf16' => 'Администраторы',
			'46b775b8-d8c0-11e7-814a-00155d01bf17' => 'Менеджеры',
			'46b775b8-d8c0-11e7-814a-00155d01bf18' => 'Агенты',
			'46b775b8-d8c0-11e7-814a-00155d01bf19' => 'Клиенты',
			'46b775b8-d8c0-11e7-814a-00155d01bf1a' => 'Инвесторы',
			'46b775b8-d8c0-11e7-814a-00155d01bf20' => 'Администраторы партнера',
			'46b775b8-d8c0-11e7-814a-00155d01bf21' => 'Пользователи партнера',
			'46b775b8-d8c0-11e7-814a-00155d01bf22' => 'Пользователи мобильного приложения',
		];
		foreach ( $arGuid as $k => $v ) {
			$this->update( 'groups', [ 'guid' => $k ], 'name = "' . $v . '"' );
		}
	}
}
