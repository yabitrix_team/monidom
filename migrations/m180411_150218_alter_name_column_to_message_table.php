<?php

use yii\db\Migration;

/**
 * Class m180411_150218_alter_name_column_to_message_table
 */
class m180411_150218_alter_name_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'name', 'int(11) NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'name', 'int(11) NOT NULL' );
	}
}
