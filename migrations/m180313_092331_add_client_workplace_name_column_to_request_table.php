<?php

use yii\db\Migration;

/**
 * Handles adding client_workplace_name to table `request`.
 */
class m180313_092331_add_client_workplace_name_column_to_request_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->addColumn( 'requests', 'client_workplace_name', 'varchar(255) not null' );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropColumn( 'requests', 'client_workplace_name' );
	}
}
