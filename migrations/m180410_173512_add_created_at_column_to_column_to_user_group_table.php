<?php

use yii\db\Migration;

/**
 * Handles adding created_at_column_to to table `user_group`.
 */
class m180410_173512_add_created_at_column_to_column_to_user_group_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( "user_group", "created_at", "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( "user_group", "created_at" );
	}
}
