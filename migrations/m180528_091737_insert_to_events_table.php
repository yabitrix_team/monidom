<?php

use yii\db\Migration;

/**
 * Class m180528_091737_insert_to_events_table
 */
class m180528_091737_insert_to_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->insert("events", ["code" => 713, "name" => "Дополнительно отправлены фото клиента и авто"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->delete("events", "code = 713");
    }
}
