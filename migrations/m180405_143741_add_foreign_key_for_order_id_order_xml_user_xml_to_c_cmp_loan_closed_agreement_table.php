<?php

use yii\db\Migration;

/**
 * Class m180405_143741_add_foreign_key_for_order_id_order_xml_user_xml_to_c_cmp_loan_closed_agreement_table
 */
class m180405_143741_add_foreign_key_for_order_id_order_xml_user_xml_to_c_cmp_loan_closed_agreement_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-c_cmp_loan_closed_agreement-order_id',  // это "условное имя" ключа
			'c_cmp_loan_closed_agreement', // это название текущей таблицы
			'order_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-c_cmp_loan_closed_agreement-order_xml',  // это "условное имя" ключа
			'c_cmp_loan_closed_agreement', // это название текущей таблицы
			'order_xml', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'guid', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-c_cmp_loan_closed_agreement-user_xml',  // это "условное имя" ключа
			'c_cmp_loan_closed_agreement', // это название текущей таблицы
			'user_xml', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'guid', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-c_cmp_loan_closed_agreement-order_id',
			'c_cmp_loan_closed_agreement'
		);
		$this->dropIndex(
			'fk-c_cmp_loan_closed_agreement-order_id',
			'c_cmp_loan_closed_agreement'
		);
		$this->dropForeignKey(
			'fk-c_cmp_loan_closed_agreement-order_xml',
			'c_cmp_loan_closed_agreement'
		);
		$this->dropIndex(
			'fk-c_cmp_loan_closed_agreement-order_xml',
			'c_cmp_loan_closed_agreement'
		);
		$this->dropForeignKey(
			'fk-c_cmp_loan_closed_agreement-user_xml',
			'c_cmp_loan_closed_agreement'
		);
		$this->dropIndex(
			'fk-c_cmp_loan_closed_agreement-user_xml',
			'c_cmp_loan_closed_agreement'
		);
	}
}
