<?php

use yii\db\Migration;

/**
 * Class m180307_140524_alter_guid_active_sort_column_to_statuses_table
 */
class m180307_140524_alter_guid_active_sort_column_to_statuses_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'statuses', 'guid', 'char(36) null' );
		$this->alterColumn( 'statuses', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'statuses', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'statuses', 'guid', 'char(36) not null' );
		$this->alterColumn( 'statuses', 'active', 'char(1) not null' );
		$this->alterColumn( 'statuses', 'sort', 'int(11) not null' );
	}
}
