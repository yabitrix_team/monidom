<?php

use yii\db\Migration;

/**
 * Class m180411_150225_alter_message_column_to_message_table
 */
class m180411_150225_alter_message_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'message', 'int(11) NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'message', 'int(11) NOT NULL' );
	}
}
