<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cabinets_methods_issuance`.
 */
class m180514_210205_create_cabinets_methods_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable( 'cabinets_methods_issuance', [
		    'cabinet_id'    => $this->integer( 11 ),
		    'method_id'  => $this->integer( 11 ),
	    ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cabinets_methods_issuance');
    }
}
