<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_sms_confirm`.
 */
class m180202_100309_create_api_sms_confirm_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'api_sms_confirm', [
			'id'     => $this->primaryKey(),
			'login'  => $this->char( 10 )->notNull(),
			'code'   => $this->smallInteger( 5 )->notNull(),
			'status' => $this->char( 1 )->notNull(),
			'date'   => $this->timestamp()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'api_sms_confirm' );
	}
}
