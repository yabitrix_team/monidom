<?php

use yii\db\Migration;

/**
 * Handles the creation of table `c_cmp_loan_agreement_fields`.
 */
class m180202_082343_create_c_cmp_loan_agreement_fields_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'c_cmp_loan_agreement_fields', [
			'id'                => $this->primaryKey(),
			'created_at'        => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'loan_agreement_id' => $this->integer( 11 )->notNull(),
			'date_pay'          => $this->datetime()->notNull(),
			'principal_debt'    => $this->double()->notNull(),
			'percent'           => $this->double()->notNull(),
			'payable'           => $this->double()->notNull(),
			'balance_principal' => $this->double()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'c_cmp_loan_agreement_fields' );
	}
}
