<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_exchange_name`.
 */
class m180202_093751_create_pcm_log_exchange_name_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_exchange_name', [
			'id'   => $this->primaryKey(),
			'name' => $this->string( 255 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_exchange_name' );
	}
}
