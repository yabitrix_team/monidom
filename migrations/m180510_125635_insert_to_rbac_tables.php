<?php

use yii\db\Migration;

/**
 * Class m180510_125635_insert_to_rbac_tables
 */
class m180510_125635_insert_to_rbac_tables extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//Если данные в таблицах auth_item есть, то ничего не делаем
		$authItem = Yii::$app->db->createCommand( 'SELECT * 
FROM auth_item' )
		                         ->queryAll();
		if ( count( $authItem ) < 1 ) {
			$auth = Yii::$app->authManager;
			//----------------------------------------------------------------------------------------------------------------------------
			//ПРАВИЛО ОПРЕДЕЛЕНИЯ ПОЛЬЗОВАТЕЛЕЙ ПО ГРУППЕ
			//----------------------------------------------------------------------------------------------------------------------------
			$ruleGroup = new \app\modules\app\v1\classes\rules\UserGroupRule;
			$auth->add( $ruleGroup );
			//----------------------------------------------------------------------------------------------------------------------------
			//НАСТРАИВАЕМ РАЗРЕШЕНИЯ
			//----------------------------------------------------------------------------------------------------------------------------
			// добавляем разрешение "accessLkp"
			$accessLkp              = $auth->createPermission( 'access_lkp' );
			$accessLkp->description = 'Access to LKP2';
			$auth->add( $accessLkp );
			// добавляем разрешение "acessLka"
			$accessLka              = $auth->createPermission( 'access_lka' );
			$accessLka->description = 'Access to LKA';
			$auth->add( $accessLka );
			// добавляем разрешение "acessLkk"
			$accessLkk              = $auth->createPermission( 'access_lkk' );
			$accessLkk->description = 'Access to LKK';
			$auth->add( $accessLkk );
			// добавляем разрешение "accessLki"
			$accessLki              = $auth->createPermission( 'access_lki' );
			$accessLki->description = 'Access to LKI';
			$auth->add( $accessLki );
			// добавляем разрешение "accessMp"
			$accessMp              = $auth->createPermission( 'access_mp' );
			$accessMp->description = 'Access to MP';
			$auth->add( $accessMp );
			//----------------------------------------------------------------------------------------------------------------------------
			//НАСТРАИВАЕМ РОЛИ
			//----------------------------------------------------------------------------------------------------------------------------
			// добавляем роль "roleAdmin" и даём роли разрешение "role_admin"
			$roleAdmin           = $auth->createRole( 'role_admin' );
			$roleAdmin->ruleName = $ruleGroup->name;
			$auth->add( $roleAdmin );
			$auth->addChild( $roleAdmin, $accessLkp );
			$auth->addChild( $roleAdmin, $accessLka );
			$auth->addChild( $roleAdmin, $accessLkk );
			$auth->addChild( $roleAdmin, $accessLki );
			$auth->addChild( $roleAdmin, $accessMp );
			// добавляем роль "roleAgent" и даём роли разрешение "access_lka"
			$roleAgent           = $auth->createRole( 'role_agent' );
			$roleAgent->ruleName = $ruleGroup->name;
			$auth->add( $roleAgent );
			$auth->addChild( $roleAgent, $accessLkp );
			$auth->addChild( $roleAgent, $accessLka );
			// добавляем роль "roleClient" и даём роли разрешение "access_lkk"
			$roleClient           = $auth->createRole( 'role_client' );
			$roleClient->ruleName = $ruleGroup->name;
			$auth->add( $roleClient );
			$auth->addChild( $roleClient, $accessLkk );
			// добавляем роль "roleInvest" и даём роли разрешение "access_lki"
			$roleInvest           = $auth->createRole( 'role_invest' );
			$roleInvest->ruleName = $ruleGroup->name;
			$auth->add( $roleInvest );
			$auth->addChild( $roleInvest, $accessLki );
			// добавляем роль "roleUserPartner" и даём роли разрешение "access_lkp"
			$roleUserPartner           = $auth->createRole( 'role_user_partner' );
			$roleUserPartner->ruleName = $ruleGroup->name;
			$auth->add( $roleUserPartner );
			$auth->addChild( $roleUserPartner, $accessLkp );
			// добавляем роль "role_mobile_application" и даём роли разрешение "access_mp"
			$roleMobileApplication           = $auth->createRole( 'role_mobile_application' );
			$roleMobileApplication->ruleName = $ruleGroup->name;
			$auth->add( $roleMobileApplication );
			$auth->addChild( $roleMobileApplication, $accessMp );
			//----------------------------------------------------------------------------------------------------------------------------
			//НАСТРОЙКА ПРАВИЛА ПРОВЕРКИ ОБНОВЛЕНИЯ СОБСТВЕННОЙ ЗАЯВКИ
			//----------------------------------------------------------------------------------------------------------------------------
			//Добавляем правило на проверку редактирования заявки
			// add the rule
			$ruleRequest = new \app\modules\app\v1\classes\rules\RequestRule;
			$auth->add( $ruleRequest );
			// добавляем разрешение "updateOwnRequest" и привязываем к нему правило.
			$updateOwnRequest              = $auth->createPermission( 'updateOwnRequest' );
			$updateOwnRequest->description = 'Update own request';
			$updateOwnRequest->ruleName    = $ruleRequest->name;
			$auth->add( $updateOwnRequest );
			// "updateOwnRequest" будет использоваться в правиле "$accessLkp/$accessLka/$accessLkk"
			$auth->addChild( $updateOwnRequest, $accessLkp );
			$auth->addChild( $updateOwnRequest, $accessLka );
			$auth->addChild( $updateOwnRequest, $accessLkk );
			$auth->addChild( $updateOwnRequest, $accessMp );
			// разрешаем по ролям обновлять его заявки
			$auth->addChild( $roleAdmin, $updateOwnRequest );
			$auth->addChild( $roleAgent, $updateOwnRequest );
			$auth->addChild( $roleClient, $updateOwnRequest );
			$auth->addChild( $roleUserPartner, $updateOwnRequest );
			$auth->addChild( $roleMobileApplication, $updateOwnRequest );
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'auth_assignment' );
		$this->delete( 'auth_item' );
		$this->delete( 'auth_item_child' );
		$this->delete( 'auth_rule' );
		$this->delete( 'auth_token' );
	}
}
