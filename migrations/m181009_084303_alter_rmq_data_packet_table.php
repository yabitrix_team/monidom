<?php

use yii\db\Migration;

/**
 * Class m181009_084303_alter_rmq_data_packet_table
 */
class m181009_084303_alter_rmq_data_packet_table extends Migration
{
	protected $table_name = 'rmq_data_packet';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn($this->table_name, 'created_at', $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table_name, 'created_at');
    }

}
