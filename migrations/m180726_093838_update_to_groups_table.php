<?php

use yii\db\Migration;

/**
 * Class m180726_093838_update_to_groups_table
 */
class m180726_093838_update_to_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$cabinet   = Yii::$app->db->createCommand( "SELECT * FROM cabinets WHERE code='f1e5d7a5fe13498abbdeb0f1f19136a8'" )
		                          ->queryOne();
		$idCabinet = $cabinet["id"];
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $idCabinet ],
		               'identifier = \'admin\''
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'groups',
		               [ 'prior_cabinet_id' => 1 ],
		               'identifier = \'admin\''
		);
	}
}
