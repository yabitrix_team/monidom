<?php

use yii\db\Migration;

/**
 * Class m180307_124144_alter_guid_active_sort_column_to_groups_table
 */
class m180307_124144_alter_guid_active_sort_column_to_groups_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'groups', 'guid', 'char(36) null' );
		$this->alterColumn( 'groups', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'groups', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'groups', 'guid', 'char(36) not null' );
		$this->alterColumn( 'groups', 'active', 'char(1) not null' );
		$this->alterColumn( 'groups', 'sort', 'int(11) not null' );
	}
}
