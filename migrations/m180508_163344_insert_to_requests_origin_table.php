<?php

use yii\db\Migration;

/**
 * Class m180508_163344_insert_to_requests_origin_table
 */
class m180508_163344_insert_to_requests_origin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->insert( 'requests_origin',
	                        [
		                        'name_1c' => 'ЛККлиента',
		                        'identifier' => 'CLIENT',
		                        'active' => 1,
	                        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->delete( 'requests_origin', [
		    'identifier' => 'CLIENT',
	    ] );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180508_163344_insert_to_requests_origin_table cannot be reverted.\n";

        return false;
    }
    */
}
