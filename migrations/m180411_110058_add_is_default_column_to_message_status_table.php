<?php

use yii\db\Migration;

/**
 * Handles adding is_default to table `message_status`.
 */
class m180411_110058_add_is_default_column_to_message_status_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'message_status', 'is_default', 'CHAR(1) NULL DEFAULT "N" AFTER name' );
		$this->addCommentOnColumn( 'message_status', 'is_default', 'Является значением по умолчанию, для вновь созданных сообщений без указания соответствующего параметра' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropCommentFromColumn( 'message_status', 'is_default' );
		$this->dropColumn( 'message_status', 'is_default' );
	}
}
