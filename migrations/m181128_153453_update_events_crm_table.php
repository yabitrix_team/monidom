<?php

use yii\db\Migration;

/**
 * Class m181128_153453_update_events_crm_table
 */
class m181128_153453_update_events_crm_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'events_crm';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update($this->table, ['name' => 'Регистрация/Авторизация в ЛКК'], '[[code]] = 110');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update($this->table, ['name' => 'Регистрация в ЛКК'], '[[code]] = 110');
    }
}
