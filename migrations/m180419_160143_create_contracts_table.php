<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contracts`.
 */
class m180419_160143_create_contracts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contracts', [
            'id' => $this->primaryKey()->unsigned(),
            'code' => $this->string()->unique()->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'active' => $this->smallInteger(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contracts');
    }
}
