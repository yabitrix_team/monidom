<?php

use yii\db\Migration;

/**
 * Handles adding subdomain to table `cabinet`.
 */
class m180413_121726_add_subdomain_column_to_cabinet_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'cabinets', 'subdomain', 'VARCHAR(50) AFTER name' );
		$this->update( 'cabinets',
		               [ 'subdomain' => 'partner' ],
		               'guid = \'caf2f5a8-714b-4c38-3a68-9\'' );
		$this->update( 'cabinets',
		               [ 'subdomain' => 'client' ],
		               'guid = \'caf2f5a8-714b-4c38-3a68-a\'' );
		$this->update( 'cabinets',
		               [ 'subdomain' => 'agent' ],
		               'guid = \'caf2f5a8-714b-4c38-3a68-b\'' );
		$this->update( 'cabinets',
		               [ 'subdomain' => 'invest' ],
		               'guid = \'caf2f5a8-714b-4c38-3a68-c\'' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'cabinets', 'subdomain' );
	}
}
