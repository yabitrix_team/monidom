<?php

use yii\db\Migration;

/**
 * Class m180307_124138_alter_guid_active_sort_column_to_file_table
 */
class m180307_124138_alter_guid_active_sort_column_to_file_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'file', 'guid', 'char(36) null' );
		$this->alterColumn( 'file', 'sim_link', 'varchar(255) null' );
		$this->alterColumn( 'file', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'file', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'file', 'guid', 'char(36) not null' );
		$this->alterColumn( 'file', 'sim_link', 'varchar(255) not null' );
		$this->alterColumn( 'file', 'active', 'char(1) not null' );
		$this->alterColumn( 'file', 'sort', 'int(11) not null' );
	}
}
