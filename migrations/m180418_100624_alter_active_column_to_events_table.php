<?php

use yii\db\Migration;

/**
 * Class m180418_100624_alter_active_column_to_events_table
 */
class m180418_100624_alter_active_column_to_events_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'events', [ 'active' => 1 ] );
		$this->alterColumn( 'events', 'active', 'TINYINT(1) NOT NULL DEFAULT 1' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'events', 'active', 'CHAR(1) NOT NULL DEFAULT "Y"' );
		$this->update( 'events', [ 'active' => 'Y' ] );
	}
}
