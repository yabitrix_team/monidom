<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m181219_102307_insert_method_issuance_table
 */
class m181219_102307_insert_method_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert( 'methods_issuance', [ 'guid', 'name', 'code_1c', 'description', 'active', 'sort' ], [
            [ '300420', 'На банковскую карту через WalletOne', 'WalletOneНаБанковскуюКартуПоТокену', 'Получение денег на банковскую карту, с подтверждением карты', '1', '100' ],
        ] );
        $id = Yii::$app->db->createCommand('select `id` from `methods_issuance` where `guid`=300420')->queryColumn();

        $this->batchInsert( 'cabinets_methods_issuance', [ 'cabinet_id', 'method_id'], [
            [1, $id[0]],
            [2, $id[0]]
        ] );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $id = Yii::$app->db->createCommand('select `id` from `methods_issuance` where `guid`=300420')->queryColumn();
        $this->delete( 'cabinets_methods_issuance', [ 'cabinet_id' => 1, 'method_id' => $id[0] ] );
        $this->delete( 'cabinets_methods_issuance', [ 'cabinet_id' => 2, 'method_id' => $id[0] ] );
        $this->delete( 'methods_issuance', [ 'guid' => '300420' ] );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_102307_insert_method_issuance_table cannot be reverted.\n";

        return false;
    }
    */
}
