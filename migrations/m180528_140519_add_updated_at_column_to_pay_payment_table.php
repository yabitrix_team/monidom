<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `pay_payment`.
 */
class m180528_140519_add_updated_at_column_to_pay_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn("pay_payment", "updated_at",  $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropColumn("pay_payment", "updated_at");
    }
}
