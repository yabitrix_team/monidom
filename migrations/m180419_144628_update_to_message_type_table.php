<?php

use yii\db\Migration;

/**
 * Class m180419_144628_update_to_message_type_table
 */
class m180419_144628_update_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'message_type', [
			'is_default' => "Y",
		], [
			               "name" => "req",
		               ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'message_type', [
			'is_default' => "N",
		], [
			               "name" => "req",
		               ] );
	}
}
