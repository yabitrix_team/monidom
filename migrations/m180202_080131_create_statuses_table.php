<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses`.
 */
class m180202_080131_create_statuses_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'statuses', [
			'id'         => $this->primaryKey(),
			'guid'       => $this->char( 36 )->notNull()->unique(),
			'name'       => $this->string( 255 )->notNull(),
			'progress'   => $this->integer( 3 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'statuses' );
	}
}
