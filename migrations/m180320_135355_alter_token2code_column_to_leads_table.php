<?php

use yii\db\Migration;

/**
 * Class m180320_135355_alter_token2code_column_to_leads_table
 */
class m180320_135355_alter_token2code_column_to_leads_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'leads', 'token', 'code' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'leads', 'code', 'token' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180320_135355_alter_token2code_column_to_leads_table cannot be reverted.\n";

		return false;
	}
	*/
}
