<?php

use yii\db\Migration;

/**
 * Class m180424_161956_alter_first_name_column_to_users_table
 */
class m180424_161956_alter_first_name_column_to_users_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'users', 'first_name', $this->string( 255 ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'users', 'first_name', $this->string( 255 )->notNull() );
	}
}
