<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses_journal`.
 */
class m180202_080332_create_statuses_journal_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'statuses_journal', [
			'id'         => $this->primaryKey(),
			'request_id' => $this->integer( 11 )->notNull(),
			'status_id'  => $this->integer( 11 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'statuses_journal' );
	}
}
