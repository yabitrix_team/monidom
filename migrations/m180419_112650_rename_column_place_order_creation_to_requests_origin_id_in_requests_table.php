<?php

use yii\db\Migration;

/**
 * Class m180419_112650_rename_column_place_order_creation_to_requests_origin_id_in_requests_table
 */
class m180419_112650_rename_column_place_order_creation_to_requests_origin_id_in_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'requests', 'place_order_creation', 'requests_origin_id' );
		$this->alterColumn( 'requests', 'requests_origin_id', $this->integer( 11 ) );
		$this->addCommentOnColumn( 'requests', 'requests_origin_id', 'Источник создания заявки, старое название place_order_creation' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'requests', 'requests_origin_id', 'place_order_creation' );
		$this->alterColumn( 'requests', 'place_order_creation', $this->string( 50 )->notNull() );
	}
}
