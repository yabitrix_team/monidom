<?php

use yii\db\Migration;

/**
 * Class m180605_222845_insert_to_cabinets_methods_issuance_table
 */
class m180605_222845_insert_to_cabinets_methods_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->delete('cabinets_methods_issuance', ['cabinet_id' => 1]);
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '2',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '5',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '7',
	    ] );
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"       => '8',
	    ] );
	    $this->update( 'methods_issuance', [ 'description' => 'Получение наличных через систему денежных переводов «CONTACT» в ближайшем отделении банка-партнера.' ], 'id = 8' );
	    $this->update( 'methods_issuance', [ 'description' => 'Зачисление на открытый банковский счет по реквизитам.' ], 'id = 7' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180605_222845_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_222845_insert_to_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }
    */
}
