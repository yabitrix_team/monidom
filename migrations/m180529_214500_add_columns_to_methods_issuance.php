<?php

use yii\db\Migration;

/**
 * Class m180529_214500_add_columns_to_methods_issuance
 */
class m180529_214500_add_columns_to_methods_issuance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn( 'methods_issuance', 'description', 'varchar(255) not null after code_1c' );
	    $this->update( 'methods_issuance', [ 'description' => 'Карта принадлежит клиенту. Карта любого банка кроме СИТИбанка.' ], 'id = 2' );
	    $this->update( 'methods_issuance', [ 'description' => 'Получение наличных через систему денежных переводов «CONTACT» в ближайшем отделении банка-партнера.' ], 'id = 4' );
	    $this->update( 'methods_issuance', [ 'description' => 'Зачисление на открытый банковский счет по реквизитам.' ], 'id = 1' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('methods_issuance', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_214500_add_columns_to_methods_issuance cannot be reverted.\n";

        return false;
    }
    */
}
