<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lcrm_leads`.
 */
class m181110_182705_create_lcrm_leads_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'lcrm_leads',
            [
                'id'                            => $this->primaryKey(),
                'code'                          => $this->string()->notNull()->unique(),
                'lcrm_id'                       => $this->integer()->notNull()->unique(),
                'request_id'                    => $this->integer(),
                'point_id'                      => $this->integer(),
                'user_id'                       => $this->integer(),
                'client_mobile_phone'           => $this->string(20)->notNull(),
                'summ'                          => $this->integer(),
                'credit_product_id'             => $this->integer(),
                'auto_brand_id'                 => $this->integer(),
                'auto_model_id'                 => $this->integer(),
                'auto_year'                     => $this->integer(),
                'auto_price'                    => $this->integer(),
                'client_first_name'             => $this->string(),
                'client_last_name'              => $this->string(),
                'client_patronymic'             => $this->string(),
                'client_birthday'               => $this->date(),
                'client_passport_serial_number' => $this->char(4),
                'client_passport_number'        => $this->char(6),
                'client_region_id'              => $this->integer(),
                'date_return'                   => $this->date(),
                'created_at'                    => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
                'updated_at'                    => $this->datetime(),
            ]
        );
        $this->addForeignKey(
            'fk-lcrm_leads-credit_product_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'credit_product_id', // это имя поля в текущей таблице, которое будет ключом
            'products', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-auto_brand_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'auto_brand_id', // это имя поля в текущей таблице, которое будет ключом
            'auto_brands', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-auto_model_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'auto_model_id', // это имя поля в текущей таблице, которое будет ключом
            'auto_models', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-request_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'request_id', // это имя поля в текущей таблице, которое будет ключом
            'requests', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-client_region_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'client_region_id', // это имя поля в текущей таблице, которое будет ключом
            'regions', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-point_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'point_id', // это имя поля в текущей таблице, которое будет ключом
            'points', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-lcrm_leads-user_id',  // это "условное имя" ключа
            'lcrm_leads', // это название текущей таблицы
            'user_id', // это имя поля в текущей таблице, которое будет ключом
            'users', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lcrm_leads');
    }
}
