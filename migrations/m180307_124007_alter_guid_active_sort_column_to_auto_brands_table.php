<?php

use yii\db\Migration;

/**
 * Class m180307_124007_alter_guid_active_sort_column_to_auto_brands_table
 */
class m180307_124007_alter_guid_active_sort_column_to_auto_brands_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'auto_brands', 'guid', 'char(36) null' );
		$this->alterColumn( 'auto_brands', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'auto_brands', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'auto_brands', 'guid', 'char(36) not null' );
		$this->alterColumn( 'auto_brands', 'active', 'char(1) not null' );
		$this->alterColumn( 'auto_brands', 'sort', 'int(11) not null' );
	}
}
