<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_fcm`.
 */
class m180202_100656_create_api_fcm_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'api_fcm', [
			'id'        => $this->primaryKey(),
			'user_id'   => $this->integer( 11 )->notNull(),
			'fcm_token' => $this->string( 300 )->notNull(),
			'device_id' => $this->string( 50 )->notNull(),
			'os_type'   => "ENUM('android','ios') NOT NULL",
			'date'      => $this->timestamp()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'api_fcm' );
	}
}
