<?php

use yii\db\Migration;

/**
 * Class m180808_113155_update_pay_methods_table
 */
class m180808_113155_update_pay_methods_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->update( 'pay_methods',
		               [
			               'name' => 'Банковские карты VISA/MasterCard/МИР',
		               ],
		               [
			               'id' => 1,
		               ]
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		echo "m180808_113155_update_pay_methods_table cannot be reverted.\n";

	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180808_113155_update_pay_methods_table cannot be reverted.\n";

		return false;
	}
	*/
}
