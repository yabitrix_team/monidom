<?php

use yii\db\Migration;

/**
 * Class m180302_110652_seed_users_and_groups_and
 */
class m180302_110652_seed_users_and_groups_and extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//users
		$this->batchInsert( 'users', [
			'guid',
			'username',
			'email',
			'auth_key',
			'password_hash',
			'password_reset_token',
			'point_id',
			'agent_id',
			'first_name',
			'last_name',
			'second_name',
			'is_accreditated',
			'inside_support_token',
			'active',
			'sort',
		], [
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf07',
				                    'admin1',
				                    'admin1@mail.ru',
				                    'auth_key1',
				                    'passhashabcd1',
				                    'password_reset_token1',
				                    '1',
				                    '1',
				                    'ИмяA',
				                    'ФамилияА',
				                    'ОтчествоК',
				                    '1',
				                    'inside_support_token1',
				                    '1',
				                    '100',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf08',
				                    'admin2',
				                    'admin2@mail.ru',
				                    'auth_key2',
				                    'passhashabcd2',
				                    'password_reset_token2',
				                    '2',
				                    '2',
				                    'ИмяB',
				                    'ФамилияБ',
				                    'ОтчествоЛ',
				                    '0',
				                    'inside_support_token2',
				                    '1',
				                    '200',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf09',
				                    'Менеджер 1',
				                    'man1@mail.ru',
				                    'auth_key3',
				                    'passhashabcd3',
				                    'password_reset_token3',
				                    '3',
				                    '3',
				                    'ИмяC',
				                    'ФамилияВ',
				                    'ОтчествоМ',
				                    '1',
				                    'inside_support_token3',
				                    '1',
				                    '100',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf10',
				                    'Менеджер 2',
				                    'man2@mail.ru',
				                    'auth_key4',
				                    'passhashabcd4',
				                    'password_reset_token4',
				                    '4',
				                    '4',
				                    'ИмяD',
				                    'ФамилияГ',
				                    'ОтчествоН',
				                    '0',
				                    'inside_support_token4',
				                    '1',
				                    '200',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf11',
				                    'Пользователь 1',
				                    'user1@mail.ru',
				                    'auth_key5',
				                    'passhashabcd5',
				                    'password_reset_token5',
				                    '5',
				                    '5',
				                    'ИмяE',
				                    'ФамилияД',
				                    'ОтчествоО',
				                    '1',
				                    'inside_support_token5',
				                    '1',
				                    '100',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf12',
				                    'Пользователь 2',
				                    'user2@mail.ru',
				                    'auth_key6',
				                    'passhashabcd6',
				                    'password_reset_token6',
				                    '6',
				                    '6',
				                    'ИмяF',
				                    'ФамилияЕ',
				                    'ОтчествоП',
				                    '0',
				                    'inside_support_token6',
				                    '1',
				                    '200',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf13',
				                    'Клиент 1',
				                    'client1@mail.ru',
				                    'auth_key7',
				                    'passhashabcd7',
				                    'password_reset_token7',
				                    '7',
				                    '7',
				                    'ИмяAA',
				                    'ФамилияЖ',
				                    'ОтчествоР',
				                    '1',
				                    'inside_support_token7',
				                    '1',
				                    '100',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf14',
				                    'Клиент 2',
				                    'client2',
				                    'auth_key8',
				                    'passhashabcd8',
				                    'password_reset_token8',
				                    '8',
				                    '8',
				                    'ИмяAB',
				                    'ФамилияЗ',
				                    'ОтчествоС',
				                    '0',
				                    'inside_support_token8',
				                    '1',
				                    '100',
			                    ],
			                    [
				                    '46b775b8-d8c0-11e7-814a-00155d01bf15',
				                    'Клиент 3',
				                    'client3',
				                    'auth_key9',
				                    'passhashabcd9',
				                    'password_reset_token9',
				                    '9',
				                    '9',
				                    'ИмяAC',
				                    'ФамилияИ',
				                    'ОтчествоТ',
				                    '1',
				                    'inside_support_token9',
				                    '1',
				                    '200',
			                    ],
		                    ] );
		//groups
		$this->batchInsert( 'groups', [ 'guid', 'name', 'active', 'sort' ], [
			[ '46b775b8-d8c0-11e7-814a-00155d01bf16', 'Администраторы', '1', '100' ],
			[ '46b775b8-d8c0-11e7-814a-00155d01bf17', 'Менеджеры', '1', '200' ],
			[ '46b775b8-d8c0-11e7-814a-00155d01bf18', 'Пользователи', '1', '100' ],
			[ '46b775b8-d8c0-11e7-814a-00155d01bf19', 'Клиенты', '1', '100' ],
		] );
		$users  = Yii::$app->db->createCommand( 'SELECT
  *
FROM users
WHERE GUID in (
\'46b775b8-d8c0-11e7-814a-00155d01bf07\',
\'46b775b8-d8c0-11e7-814a-00155d01bf08\',
\'46b775b8-d8c0-11e7-814a-00155d01bf09\',
\'46b775b8-d8c0-11e7-814a-00155d01bf10\',
\'46b775b8-d8c0-11e7-814a-00155d01bf11\',
\'46b775b8-d8c0-11e7-814a-00155d01bf12\',
\'46b775b8-d8c0-11e7-814a-00155d01bf13\',
\'46b775b8-d8c0-11e7-814a-00155d01bf14\',
\'46b775b8-d8c0-11e7-814a-00155d01bf15\'
)' )
		                       ->queryAll();
		$groups = Yii::$app->db->createCommand( 'SELECT
  *
FROM groups
WHERE GUID in (
\'46b775b8-d8c0-11e7-814a-00155d01bf16\', 
\'46b775b8-d8c0-11e7-814a-00155d01bf17\', 
\'46b775b8-d8c0-11e7-814a-00155d01bf18\', 
\'46b775b8-d8c0-11e7-814a-00155d01bf19\')' )
		                       ->queryAll();
		//relations
		$this->batchInsert( 'user_group', [ 'user_id', 'group_id' ], [
			[ $users[0]["id"], $groups[0]["id"] ],
			[ $users[1]["id"], $groups[0]["id"] ],
			[ $users[2]["id"], $groups[1]["id"] ],
			[ $users[3]["id"], $groups[1]["id"] ],
			[ $users[4]["id"], $groups[2]["id"] ],
			[ $users[5]["id"], $groups[2]["id"] ],
			[ $users[6]["id"], $groups[3]["id"] ],
			[ $users[7]["id"], $groups[3]["id"] ],
			[ $users[8]["id"], $groups[3]["id"] ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf07' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf08' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf09' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf10' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf11' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf12' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf13' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf14' ] );
		$this->delete( 'users', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf15' ] );
		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf16' ] );
		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf17' ] );
		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf18' ] );
		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf19' ] );
		$this->delete( 'user_group', [ 'user_id' => '1' ] );
		$this->delete( 'user_group', [ 'user_id' => '2' ] );
		$this->delete( 'user_group', [ 'user_id' => '3' ] );
		$this->delete( 'user_group', [ 'user_id' => '4' ] );
		$this->delete( 'user_group', [ 'user_id' => '5' ] );
		$this->delete( 'user_group', [ 'user_id' => '6' ] );
		$this->delete( 'user_group', [ 'user_id' => '7' ] );
		$this->delete( 'user_group', [ 'user_id' => '8' ] );
		$this->delete( 'user_group', [ 'user_id' => '9' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_110652_seed_users_and_groups_and cannot be reverted.\n";

		return false;
	}
	*/
}
