<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_statuses`.
 */
class m180202_080622_create_client_statuses_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'client_statuses', [
			'id'         => $this->primaryKey(),
			'status_id'  => $this->integer( 11 )->notNull(),
			'user_id'    => $this->integer( 11 )->notNull(),
			'group_id'   => $this->integer( 11 )->notNull(),
			'name'       => $this->string( 255 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'client_statuses' );
	}
}
