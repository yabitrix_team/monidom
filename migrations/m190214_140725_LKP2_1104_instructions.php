<?php

use yii\db\Migration;

/**
 * Class m190214_140725_LKP2_1104_instructions
 */
class m190214_140725_LKP2_1104_instructions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->delete('pages_faq', [
		    'name' => 'Памятка по оформлению документов'
	    ]);

	    $text = htmlspecialchars( '  <div class="flh">
          <h2>Комплект документов:</h2>
          <ul>
            <li><span>Паспорт РФ</span></li>
            <li><span>Паспорт транспортного средства (ПТС)</span></li>
            <li><span>Свидетельство о регистрации транспортного средства (СТС)</span></li>
          </ul>
          <div class="image-wrap">
            <div>
              <img src="/static/booklet/passport.jpg" />
            </div>
            <div>
              <img src="/static/booklet/pts.jpg"/>
            </div>
            <div>
              <img src="/static/booklet/sor.jpg"/>
            </div>
          </div>
          <h2>Требования к документам</h2>
          <p>Четкие сканы разворотов документов, без срезанных краев, в цветном изображении.</p>
          <div class="image-wrap">
            <div>
              <img src="/static/booklet/passport2.jpg" />
            </div>
            <div>
              <img src="/static/booklet/passport3.jpg" />
            </div>
          </div>
          <h2>Ошибки при работе с документами</h2>
          <p>Четкие, не засвеченные сканы разворотов страниц паспорта 2-3,
            4-5 (и все последующие<br />
            страницы с пропиской), 14-15 (и все последующие страницы о семейном положении клиента).
          </p>
          <div class="image-wrap error">
            <div>
              <img src="/static/booklet/error1.jpg" />
            </div>
            <div>
              <img src="/static/booklet/error2.jpg" />
            </div>
            <div>
              <img src="/static/booklet/error3.jpg" />
            </div>
            <div>
              <img src="/static/booklet/error4.jpg" />
            </div>
            <div>
              <img src="/static/booklet/error5.jpg" />
            </div>
          </div>
          <h2>Требования к документам</h2>
          <p>
            Четкие, не засвеченные сканы разворотов страниц паспорта 2-3, 4-5 (и все последующие
            страницы с пропиской), 14-15 (и все последующие страницы о семейном положении клиента).
          </p>
          <div class="image-wrap">
            <div>
              <img src="/static/booklet/passport2.jpg" />
            </div>
            <div>
              <img src="/static/booklet/passport3.jpg" />
            </div>
            <div>
              <img src="/static/booklet/passport4.jpg" />
            </div>
          </div>
          <p>На одной скан-копии должен находиться один разворот документа.</p>
          <div class="image-wrap start">
            <div>
              <div class="image-wrap err">
                <div>
                  <img src="/static/booklet/passport2.jpg" />
                </div>
                <div>
                  <img src="/static/booklet/passport3.jpg" />
                </div>
              </div>
              <span class="error-text">Ошибка</span>
            </div>
            <div>
              <div class="image-wrap succ-wrap">
                <div class="succ">
                  <img src="/static/booklet/passport2.jpg" />
                </div>
                <div class="succ">
                  <img src="/static/booklet/passport2.jpg" />
                </div>
              </div>
              <span class="success-text">Верно</span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-7">
              <p>Четкие сканы разворотов ПТС, без обрезанных краев, с читаемыми данными:</p>
              <ul>
                <li><span>VIN-код,</span></li>
                <li><span>наименование собственника транспортного средства,</span></li>
                <li><span>регистрационный номер,</span></li>
                <li><span>серия и номер СТС,</span></li>
                <li><span>дата выдачи и организация, выдавшая ПТС,</span></li>
                <li><span>особые отметки.</span></li>
              </ul>
              <p>В ПТС необходимо наличие печати и подписи сотрудника ГИБДД.</p>
            </div>
            <div class="col-md-5">
               <div class="pts">
                <img src="/static/booklet/pts.jpg"/>
              </div>
            </div>
          </div>
          <p>
            Четкие сканы обеих сторон СТС, без обрезанных краев,
            с читаемым VIN-кодом и наименованием собственника<br/>
            транспортного средства, а также внутренний разворот, если на нем имеются записи.
          </p>
          <div class="image-wrap">
            <div>
              <img src="/static/booklet/sor2.jpg" />
            </div>
          </div>
          <div class="row icon-map">
            <div class="col-md-6">
              <p>
                В заявке необходимо указывать адрес регистрации.<br/>
                Если фактический адрес проживания отличается от адреса<br/>
                регистрации, необходимо указывать его полностью:<br/>
              </p>
              <ul>
                <li><span>страна</span></li>
                <li><span>индекс</span></li>
                <li><span>область</span></li>
                <li><span>город</span></li>
                <li><span>улица</span></li>
                <li><span>дом</span></li>
                <li><span>корпус (при наличии)</span></li>
                <li><span>квартира</span></li>
              </ul>
            </div>
            <div class="green">
              Например:<br/>
              Адрес регистрации: Россия, 140000, Московская обл.,
              г. Люберцы, ул. Котельническая, д. 14, к 1, кв. 56<br/>
              Адрес проживания: Россия, 121170, Московская обл.,
              г. Москва, пр-д Кутузовский, д. 5, кв. 12<br/>
            </div>
          </div>
        </div>' );
	    $this->insert( 'pages_faq', [
		    "name"        => 'Памятка по оформлению документов',
		    "text"        => $text,
		    "consumer_id" => 2,
		    "active"      => 1,
		    "sort"        => 900,
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->delete('pages_faq', [
		    'name' => 'Памятка по оформлению документов'
	    ]);

	    $html = '<table style="width: 100%"><tr><td><a href="/path/docs/Pamyatka%20po%20oformleniy%20documentov.pdf" download target="_blank" title="Памятка по оформлению документов">Памятка по оформлению документов.pdf</a></td></tr></table>';
	    $this->insert( 'pages_faq', [
		    "name"        => 'Памятка по оформлению документов',
		    "text"        => $html,
		    "consumer_id" => 2,
		    "active"      => 1,
		    "sort"        => 900,
	    ]);

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_140725_LKP2_1104_instructions cannot be reverted.\n";

        return false;
    }
    */
}
