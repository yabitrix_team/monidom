<?php

use yii\db\Migration;

/**
 * Handles adding public to table `message_type`.
 */
class m180424_164955_add_public_column_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'message_type', 'public', 'TINYINT(1) NOT NULL DEFAULT 1 AFTER `is_default`' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'message_type', 'public' );
	}
}
