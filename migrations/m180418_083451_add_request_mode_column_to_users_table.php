<?php

use yii\db\Migration;

/**
 * Handles adding request_mode to table `users`.
 */
class m180418_083451_add_request_mode_column_to_users_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'users', 'request_mode', "ENUM('photos','full') AFTER is_accreditated" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'users', 'request_mode' );
	}
}
