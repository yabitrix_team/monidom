<?php

use yii\db\Migration;

/**
 * Class m180412_102603_alter_id_xml_id_1c_column_to_pcm_log_order_table
 */
class m180412_102603_alter_id_xml_id_1c_column_to_pcm_log_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->alterColumn("pcm_log_order", "id_xml", "varchar(255) null");
	    $this->alterColumn("pcm_log_order", "id_1c", "varchar(255) null");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn("pcm_log_order", "id_xml", "varchar(255) not null");
	    $this->alterColumn("pcm_log_order", "id_1c", "varchar(255) not null");

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_102603_alter_id_xml_id_1c_column_to_pcm_log_order_table cannot be reverted.\n";

        return false;
    }
    */
}
