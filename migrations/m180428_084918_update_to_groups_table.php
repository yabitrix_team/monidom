<?php

use yii\db\Migration;

/**
 * Class m180428_084918_update_to_groups_table
 */
class m180428_084918_update_to_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$cabinets = Yii::$app->db->createCommand( 'SELECT
  *
FROM cabinets
WHERE code =  \'eaadb3195d7bb1ba2a7f568f556a642b\'' )
			->queryAll();
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $cabinets[0]['id'] ],
		               'identifier = \'partner_admin\'' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'groups',
		               [ 'prior_cabinet_id' => null ],
		               'identifier = \'partner_admin\'' );

	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180428_084918_update_to_groups_table cannot be reverted.\n";

		return false;
	}
	*/
}
