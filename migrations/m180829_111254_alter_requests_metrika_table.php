<?php

use yii\db\Migration;

/**
 * Class m180829_111254_alter_requests_metrika_table
 */
class m180829_111254_alter_requests_metrika_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->dropIndex('metrika_id', 'requests_metrika');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn('requests_metrika', 'metrika_id', $this->string()->notNull()->unique());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_111254_alter_requests_metrika_table cannot be reverted.\n";

        return false;
    }
    */
}
