<?php

use yii\db\Migration;

/**
 * Class m181017_094724_add_sms_id_column_sms_confirm_table
 */
class m181017_094724_add_sms_id_column_sms_confirm_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'sms_confirm';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            'sms_id',
            $this->string(255)->after('lock_expired_at')
        );
        $this->createIndex('sms_id', $this->table, 'sms_id', true);
        $this->addCommentOnColumn($this->table, 'sms_id', 'Идентификатор СМС-сообщения в виде ГУИД');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('sms_id', $this->table);
        $this->dropCommentFromColumn($this->table, 'sms_id');
        $this->dropColumn($this->table, 'sms_id');
    }
}
