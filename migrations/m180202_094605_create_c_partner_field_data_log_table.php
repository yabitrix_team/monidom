<?php

use yii\db\Migration;

/**
 * Handles the creation of table `c_partner_field_data_log`.
 */
class m180202_094605_create_c_partner_field_data_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'c_partner_field_data_log', [
			'id'                => $this->primaryKey(),
			'created_at'        => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'type'              => $this->string( 5 )->notNull(),
			'order_xml'         => $this->char( 36 )->notNull(),
			'order_id'          => $this->integer( 11 )->notNull(),
			'url'               => $this->text()->notNull(),
			'path'              => $this->text()->notNull(),
			'authorised'        => $this->char( 1 )->notNull(),
			'uid'               => $this->integer( 11 )->notNull(),
			'field_name'        => $this->string( 255 )->notNull(),
			'old_data'          => $this->string( 255 )->notNull(),
			'new_data'          => $this->string( 255 )->notNull(),
			'field_key_history' => $this->text()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'c_partner_field_data_log' );
	}
}
