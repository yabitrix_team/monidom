<?php

use yii\db\Migration;

/**
 * Class m180302_092605_seed_methods_issuance
 */
class m180302_092605_seed_methods_issuance extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'methods_issuance', [ 'guid', 'name', 'active', 'sort' ], [
			[ '300412', 'Безнал. на банковский счет', '1', '100' ],
			[ '300413', 'Безнал. на банковскую карту', '1', '100' ],
			[ '300414', 'Безнал. на кошелек ViberWallet', '1', '100' ],
			[ '300415', 'Наличными через КОНТАКТ', '1', '100' ],
			[ '300416', 'Наличными через ЮНИСТРИМ', '1', '100' ],
			[ '300417', 'Наличными через ЛИДЕР', '1', '100' ],
			[ '300418', 'Безнал. на банковский счет', '1', '100' ],
			[ '300419', 'Наличными через КОНТАКТ', '1', '100' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'methods_issuance', [ 'guid' => '300412' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300413' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300414' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300415' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300416' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300417' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300418' ] );
		$this->delete( 'methods_issuance', [ 'guid' => '300419' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_092605_seed_methods_issuance cannot be reverted.\n";

		return false;
	}
	*/
}
