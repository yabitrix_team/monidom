<?php

use yii\db\Migration;

/**
 * Class m180426_114939_rename_column_table_name_to_dictionary_in_versions_dictionaries_table
 */
class m180426_114939_rename_column_table_name_to_dictionary_in_versions_dictionaries_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'versions_dictionaries', 'table_name', 'dictionary' );
		$this->createIndex( 'dictionary', 'versions_dictionaries', 'dictionary', true );
		$this->addCommentOnColumn( 'versions_dictionaries', 'dictionary', 'Название справочника - соответствует наименованию идентификатора контроллера, пример: для контроллера AutoBrandController.php идентификатор auto-brand, для контроллера RegionController.php идентификатора region' );
		$this->alterColumn( 'versions_dictionaries', 'version', $this->integer( 11 )->defaultValue( 1 ) );
		$this->addColumn( 'versions_dictionaries', 'updated_at', $this->timestamp()
		                                                              ->defaultExpression( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ) );
		$this->addColumn( 'versions_dictionaries', 'created_at', $this->dateTime()
		                                                              ->defaultExpression( 'CURRENT_TIMESTAMP' ) );
		$this->batchInsert( 'versions_dictionaries', [ 'dictionary', 'version' ], [
			[ 'about-company', 1 ],
			[ 'about-product', 1 ],
			[ 'auto-brand', 1 ],
			[ 'auto-model', 1 ],
			[ 'employment', 1 ],
			[ 'people-relation', 1 ],
			[ 'product', 1 ],
			[ 'region', 1 ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->truncateTable( 'versions_dictionaries' );
		$this->alterColumn( 'versions_dictionaries', 'version', $this->integer( 10 )->notNull() );
		$this->dropColumn( 'versions_dictionaries', 'updated_at' );
		$this->dropColumn( 'versions_dictionaries', 'created_at' );
		$this->dropCommentFromColumn( 'versions_dictionaries', 'dictionary' );
		$this->dropIndex( 'dictionary', 'versions_dictionaries' );
		$this->renameColumn( 'versions_dictionaries', 'dictionary', 'table_name' );
	}
}
