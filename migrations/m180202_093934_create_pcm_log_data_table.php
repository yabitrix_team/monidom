<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_data`.
 */
class m180202_093934_create_pcm_log_data_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_data', [
			'id'     => $this->primaryKey(),
			'log_id' => $this->integer( 4 )->notNull(),
			'data'   => $this->text()->notNull(),
			'query'  => $this->text()->notNull(),
			'file'   => 'LONGTEXT NOT NULL',
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_data' );
	}
}
