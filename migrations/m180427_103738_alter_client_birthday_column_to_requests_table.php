<?php

use yii\db\Migration;

/**
 * Class m180427_103738_alter_client_birthday_column_to_requests_table
 */
class m180427_103738_alter_client_birthday_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'client_birthday', $this->date() );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'client_birthday', $this->date()->notNull() );
	}
}
