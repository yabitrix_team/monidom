<?php

use yii\db\Migration;

/**
 * Class m180503_125121_rename_order_commission_table_to_request_commission
 */
class m180503_125121_rename_order_commission_table_to_request_commission extends Migration
{
	/**
	 * {@inheritdoc}
	 */
    public function safeUp()
    {
	    $this->renameTable("order_commission", "request_commission");
	    $this->renameColumn("request_commission", "order_id", "request_id");
    }
	/**
	 * {@inheritdoc}
	 */
    public function safeDown()
    {
	    $this->renameColumn("request_commission", "request_id", "order_id");
	    $this->renameTable("request_commission", "order_commission");
    }
}
