<?php

use yii\db\Migration;

/**
 * Class m180424_051730_rename_api_token
 */
class m180424_051730_rename_api_token extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameTable( 'api_token', 'auth_token' );
		$this->createIndex( 'user_id', 'auth_token', 'user_id', true );
		$this->createIndex( 'token', 'auth_token', 'token', true );
		$this->addForeignKey(
			'fk-auth_token-user_id',  // это "условное имя" ключа
			'auth_token', // это название текущей таблицы
			'user_id', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addColumn( 'auth_token', 'updated_at', $this->timestamp()->notNull()
		                                                   ->defaultExpression( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' ) );
		$this->addColumn( 'auth_token', 'created_at', $this->datetime()->notNull()
		                                                   ->defaultExpression( "CURRENT_TIMESTAMP" ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-auth_token-user_id',
			'auth_token'
		);
		$this->dropIndex( 'user_id', 'auth_token' );
		$this->dropIndex( 'token', 'auth_token' );
		$this->dropColumn( 'auth_token', 'updated_at' );
		$this->dropColumn( 'auth_token', 'created_at' );
		$this->renameTable( 'auth_token', 'api_token' );
	}
}
