<?php

use yii\db\Migration;

/**
 * Class m180405_133307_add_foreign_key_for_status_id_user_id_group_id_to_client_statuses_table
 */
class m180405_133307_add_foreign_key_for_status_id_user_id_group_id_to_client_statuses_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-client_statuses-status_id',  // это "условное имя" ключа
			'client_statuses', // это название текущей таблицы
			'status_id', // это имя поля в текущей таблице, которое будет ключом
			'statuses', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-client_statuses-user_id',  // это "условное имя" ключа
			'client_statuses', // это название текущей таблицы
			'user_id', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
		$this->addForeignKey(
			'fk-client_statuses-group_id',  // это "условное имя" ключа
			'client_statuses', // это название текущей таблицы
			'group_id', // это имя поля в текущей таблице, которое будет ключом
			'groups', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-client_statuses-group_id',
			'client_statuses'
		);
		$this->dropIndex(
			'fk-client_statuses-group_id',
			'client_statuses'
		);
		$this->dropForeignKey(
			'fk-client_statuses-user_id',
			'client_statuses'
		);
		$this->dropIndex(
			'fk-client_statuses-user_id',
			'client_statuses'
		);
		$this->dropForeignKey(
			'fk-client_statuses-status_id',
			'client_statuses'
		);
		$this->dropIndex(
			'fk-client_statuses-status_id',
			'client_statuses'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180405_133307_add_foreign_key_for_status_id_user_id_group_id_to_client_statuses_table cannot be reverted.\n";

		return false;
	}
	*/
}
