<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exchange_statuses_journal`.
 */
class m180629_075309_create_exchange_statuses_journal_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'exchange_statuses_journal', [
			'id'                     => $this->primaryKey(),
			"ID_1C"                  => $this->string( 36 ),
			"DateAdd"                => $this->dateTime(),
			"Zayavka"                => $this->string( 36 ),
			"Period"                 => $this->dateTime(),
			"Status"                 => $this->string( 36 ),
			"Dopolnitelnoe_opisanie" => $this->string( 255 ),
			"Nomer_zayavki_sayta"    => $this->integer( 11 ),
			"created_at"             => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
			"updated_at"             => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
			"try"                    => $this->tinyInteger()->defaultValue(0),
		] );
		$this->addCommentOnTable( 'exchange_statuses_journal', 'Лог зависших статусов из ТБД' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'exchange_statuses_journal' );
	}
}
