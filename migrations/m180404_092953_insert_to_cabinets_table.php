<?php

use yii\db\Migration;

/**
 * Class m180404_092953_insert_to_cabinets_table
 */
class m180404_092953_insert_to_cabinets_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'cabinets', [ 'guid', 'name', 'active', 'sort' ], [
			[ 'caf2f5a8-714b-4c38-3a68-9', 'ЛКП', '1', '100' ],
			[ 'caf2f5a8-714b-4c38-3a68-a', 'ЛКК', '1', '100' ],
			[ 'caf2f5a8-714b-4c38-3a68-b', 'ЛКВМ', '1', '100' ],
			[ 'caf2f5a8-714b-4c38-3a68-c', 'ЛКИ', '1', '100' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'cabinets', [ 'guid' => 'caf2f5a8-714b-4c38-3a68-9' ] );
		$this->delete( 'cabinets', [ 'guid' => 'caf2f5a8-714b-4c38-3a68-a' ] );
		$this->delete( 'cabinets', [ 'guid' => 'caf2f5a8-714b-4c38-3a68-b' ] );
		$this->delete( 'cabinets', [ 'guid' => 'caf2f5a8-714b-4c38-3a68-c' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_092953_insert_to_cabinets_table cannot be reverted.\n";

		return false;
	}
	*/
}
