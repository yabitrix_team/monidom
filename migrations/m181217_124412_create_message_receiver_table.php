<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_receiver`.
 */
class m181217_124412_create_message_receiver_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'message_receiver';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'         => $this->primaryKey(),
                'message_id' => $this->integer(),
                'user_id'    => $this->integer(),
                'status_id'  => $this->integer(),
                'active'     => $this->tinyInteger(1)->defaultValue(1),
                'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                'created_at' => $this->datetime()->defaultExpression('CURRENT_TIMESTAMP'),
            ]
        );
        $this->addForeignKey(
            'fk-'.$this->table.'-user_id',  // это 'условное имя' ключа
            $this->table, // это название текущей таблицы
            'user_id', // это имя поля в текущей таблице, которое будет ключом
            'users', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-'.$this->table.'-message_id',  // это 'условное имя' ключа
            $this->table, // это название текущей таблицы
            'message_id', // это имя поля в текущей таблице, которое будет ключом
            'message', // это имя таблицы, с которой хотим связаться
            'id', // это поле таблицы, с которым хотим связаться
            'CASCADE'
        );
        $this->addCommentOnTable(
            $this->table,
            'Таблица содержит связь сообщения с пользователями, которые должны получить его, так же указываются статусы о прочитанности'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
