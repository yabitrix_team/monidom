<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_vendor`.
 */
class m180514_141414_create_pay_vendor_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pay_vendor', [
			'id'     => $this->primaryKey(),
			'name'   => $this->string()->notNull(),
			'code' => $this->string()->notNull()->unique(),
			'active' => $this->tinyInteger()->defaultValue( 1 )->notNull(),
		] );
		$this->batchInsert( 'pay_vendor', [ 'name', 'active', 'code' ], [
			[ 'Cloud Payments', 1, 'cloud' ],
			[ 'Wallet One', 1, 'w1' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'pay_vendor' );
	}
}
