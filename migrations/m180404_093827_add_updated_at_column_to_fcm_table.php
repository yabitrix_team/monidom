<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `fcm`.
 */
class m180404_093827_add_updated_at_column_to_fcm_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'fcm', 'created_at', 'updated_at' );
		$this->addColumn( 'fcm', 'created_at', 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' );
		$this->alterColumn( 'fcm', 'user_id', 'INT(11) NOT NULL DEFAULT "0"' );
		$this->alterColumn( 'fcm', 'fcm_token', 'VARCHAR(300) NULL' );
		$this->alterColumn( 'fcm', 'device_id', 'VARCHAR(50) NULL' );
		$this->createIndex( 'device_id', 'fcm', 'device_id', true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'device_id', 'fcm' );
		$this->dropColumn( 'fcm', 'created_at' );
		$this->renameColumn( 'fcm', 'updated_at', 'created_at' );
		$this->alterColumn( 'fcm', 'user_id', 'INT(11) NOT NULL' );
		$this->alterColumn( 'fcm', 'fcm_token', 'VARCHAR(300) NOT NULL' );
		$this->alterColumn( 'fcm', 'device_id', 'VARCHAR(50) NOT NULL' );
	}
}
