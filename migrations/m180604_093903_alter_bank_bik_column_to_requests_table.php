<?php

use yii\db\Migration;

/**
 * Class m180604_093903_alter_bank_bik_column_to_requests_table
 */
class m180604_093903_alter_bank_bik_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function up() {

		$this->alterColumn( "requests", "bank_bik", $this->string() );
	}

	/**
	 * {@inheritdoc}
	 */
	public function down() {

		$this->alterColumn( "requests", "bank_bik", $this->integer() );
	}
}
