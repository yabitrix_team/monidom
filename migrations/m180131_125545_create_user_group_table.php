<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_group`.
 * Has foreign keys to the tables:
 *
 * - `users`
 * - `groups`
 */
class m180131_125545_create_user_group_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'user_group', [
			'id'       => $this->primaryKey(),
			'user_id'  => $this->integer( 11 )->notNull(),
			'group_id' => $this->integer( 11 )->notNull(),
		] );
		// creates index for column `user_id`
		$this->createIndex(
			'idx-user_group-user_id',
			'user_group',
			'user_id'
		);
		// add foreign key for table `users`
		$this->addForeignKey(
			'fk-user_group-user_id',
			'user_group',
			'user_id',
			'users',
			'id',
			'CASCADE'
		);
		// creates index for column `group_id`
		$this->createIndex(
			'idx-user_group-group_id',
			'user_group',
			'group_id'
		);
		// add foreign key for table `groups`
		$this->addForeignKey(
			'fk-user_group-group_id',
			'user_group',
			'group_id',
			'groups',
			'id',
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		// drops foreign key for table `users`
		$this->dropForeignKey(
			'fk-user_group-user_id',
			'user_group'
		);
		// drops index for column `user_id`
		$this->dropIndex(
			'idx-user_group-user_id',
			'user_group'
		);
		// drops foreign key for table `groups`
		$this->dropForeignKey(
			'fk-user_group-group_id',
			'user_group'
		);
		// drops index for column `group_id`
		$this->dropIndex(
			'idx-user_group-group_id',
			'user_group'
		);
		$this->dropTable( 'user_group' );
	}
}
