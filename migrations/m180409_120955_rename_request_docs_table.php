<?php

use yii\db\Migration;

/**
 * Class m180409_120955_rename_request_docs_table
 */
class m180409_120955_rename_request_docs_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'request_docs', 'updated_at', 'TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' );
		$this->renameColumn( 'request_docs', 'file_type', 'file_bind' );
		$this->renameTable( 'request_docs', 'request_file' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameTable( 'request_file', 'request_docs' );
		$this->renameColumn( 'request_docs', 'file_bind', 'file_type' );
		$this->alterColumn( 'request_docs', 'updated_at', 'DATETIME NOT NULL' );
	}
}
