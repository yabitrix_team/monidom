<?php

use yii\db\Migration;

/**
 * Class m180808_141934_insert_to_groups_cabinets_rbac_table
 */
class m180808_141934_insert_to_groups_cabinets_rbac_table extends Migration {
	/**
	 * {@inheritdoc}
	 * @throws Exception
	 */
	public function safeUp() {

		$this->insert( 'cabinets', [
			"code"      => "13edbe8b95261c3a88680818cace44a4",
			"name"      => "ЛГ",
			"subdomain" => "lead",
			"active"    => 1,
		] );
		$idCabinet = Yii::$app->db->getLastInsertId( "cabinets" );
		$this->insert( 'groups', [
			"identifier"       => "lead_agent",
			"name"             => "Агенты лидогенератора",
			"prior_cabinet_id" => $idCabinet,
		] );
		$auth                    = Yii::$app->authManager;
		$ruleGroup               = new \app\modules\app\v1\classes\rules\UserGroupRule;
		$roleLeadAgent           = $auth->createRole( 'role_lead_agent' );
		$roleLeadAgent->ruleName = $ruleGroup->name;
		$auth->add( $roleLeadAgent );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'cabinets', [ 'code' => '13edbe8b95261c3a88680818cace44a4' ] );
		$this->delete( 'groups', [ 'identifier' => 'lead_agent' ] );
		$this->delete( 'auth_item', [ 'name' => 'role_lead_agent' ] );
	}
}
