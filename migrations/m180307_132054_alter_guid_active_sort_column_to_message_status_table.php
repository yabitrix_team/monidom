<?php

use yii\db\Migration;

/**
 * Class m180307_132054_alter_guid_active_sort_column_to_message_status_table
 */
class m180307_132054_alter_guid_active_sort_column_to_message_status_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'message_status', 'guid', 'char(36) null' );
		$this->alterColumn( 'message_status', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'message_status', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'message_status', 'guid', 'char(36) not null' );
		$this->alterColumn( 'message_status', 'active', 'char(1) not null' );
		$this->alterColumn( 'message_status', 'sort', 'int(11) not null' );
	}
}
