<?php

use yii\db\Migration;

/**
 * Handles adding sys_date to table `message`.
 */
class m180409_153417_add_sys_date_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'message', 'sys_date', 'DATETIME not null after status_id' );
		$this->addCommentOnColumn( 'message', 'sys_date', "Дата/время формирования сообщения, например в системе 1С" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'message', 'sys_date' );
	}
}
