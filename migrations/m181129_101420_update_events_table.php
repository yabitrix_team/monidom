<?php

use yii\db\Migration;

/**
 * Class m181129_101420_update_events_table
 */
class m181129_101420_update_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('events', [
            'sort' => 60
        ], ["code" => "101"]);
        $this->update('events', [
            'sort' => 70
        ], ["code" => "102"]);
        $this->update('events', [
            'sort' => 80
        ], ["code" => "201"]);
        $this->update('events', [
            'sort' => 90
        ], ["code" => "202"]);
        $this->update('events', [
            'sort' => 10
        ], ["code" => "701"]);
        $this->update('events', [
            'sort' => 20
        ], ["code" => "702"]);
        $this->update('events', [
            'sort' => 30
        ], ["code" => "711"]);
        $this->update('events', [
            'sort' => 65
        ], ["code" => "712"]);
        $this->update('events', [
            'sort' => 100
        ], ["code" => "801"]);
        $this->update('events', [
            'sort' => 110
        ], ["code" => "802"]);
        $this->update('events', [
            'sort' => 120
        ], ["code" => "803"]);
        $this->update('events', [
            'sort' => 130
        ], ["code" => "804"]);
        $this->update('events', [
            'sort' => 50
        ], ["code" => "713"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('events', [
            'sort' => 1000
        ], 1);
    }
}
