<?php

use yii\db\Migration;

/**
 * Class m180411_141821_add_code_1c_column_insert_to_methods_issuance_table
 */
class m180411_141821_add_code_1c_column_insert_to_methods_issuance_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( "methods_issuance", "code_1c", "varchar(255) null after name" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneНаБанковскийСчет" ], "guid='300412'" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneНаБанковскуюКарту" ], "guid='300413'" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneНаКошелекViberWallet" ], "guid='300414'" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneЧерезКонтакт" ], "guid='300415'" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneЧерезЮнистрим" ], "guid='300416'" );
		$this->update( "methods_issuance", [ "code_1c" => "WalletOneЧерезЛидер" ], "guid='300417'" );
		$this->update( "methods_issuance", [ "code_1c" => "НаРасчетныйСчет" ], "guid='300418'" );
		$this->update( "methods_issuance", [ "code_1c" => "ЧерезКонтакт" ], "guid='300419'" );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( "methods_issuance", "code_1c" );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180411_141821_add_code_1c_column_insert_to_methods_issuance_table cannot be reverted.\n";

		return false;
	}
	*/
}
