<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rmq_data_packet`.
 */
class m180926_145633_create_rmq_data_packet_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'rmq_data_packet', [
			'id'                 => $this->primaryKey(),
			'source_system'      => $this->string()->notNull()
			                             ->comment( 'sourceSystem - из какой системы пришел пакет (mfo|directum|crm|cmr ...)' ),
			'source_routing_key' => $this->string()->notNull()
			                             ->comment( 'sourceRoutingKey - по какому ключу высылать ответ' ),
			'execute'            => $this->string()->notNull()
			                             ->comment( 'execute - какой class-обработчик пакета' ),
			'params'             => $this->text()->null()
			                             ->comment( 'params - произвольные параметры ключ->значение, храняться в json' ),
			'direction'          => "enum('from', 'to')",
			'correlation_id'     => $this->string()->null()
			                             ->comment( 'correlation_id - свойство пакета RMQ, задается в ручную при отправке пакета' ),
			'reply_to'           => $this->string()->null()
			                             ->comment( 'reply_to - свойство пакета RMQ, задается в ручную при отправке пакета' ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->execute( "SET foreign_key_checks = 0;" );
		$this->dropTable( 'rmq_data_packet' );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
