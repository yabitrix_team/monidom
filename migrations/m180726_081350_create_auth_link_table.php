<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_link`.
 */
class m180726_081350_create_auth_link_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'auth_link', [
			'id'         => $this->primaryKey(),
			'hash'       => $this->string()->notNull(),
			'user_id'    => $this->integer()->notNull(),
			'active'     => $this->tinyInteger()->defaultValue( 1 ),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime(),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'auth_link' );
	}
}
