<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses_mp_journal`.
 */
class m180413_114830_create_statuses_mp_journal_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'statuses_mp_journal', [
			'id'           => $this->primaryKey(),
			'request_id'   => $this->integer( 11 )->notNull(),
			'status_mp_id' => $this->integer( 11 )->notNull(),
			'updated_at'   => $this->timestamp( 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' )->notNull(),
			'created_at'   => $this->datetime()->notNull(),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'statuses_mp_journal' );
	}
}
