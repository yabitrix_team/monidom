<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m180405_135017_insert_to_message_type_table
 */
class m180405_135017_insert_to_message_type_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		// заполняем данными по типам сообщений
		/**
		 * req - по заявке
		 * adv - реклама
		 * sys - системное
		 * crm - сообщение из CRM
		 * ver - месседж от верификатора
		 */
		$this->batchInsert( 'message_type',
		                    [ 'guid', 'name', 'active' ],
		                    [
			                    [ Uuid::uuid4(), 'req', 1 ],
			                    [ Uuid::uuid4(), 'adv', 1 ],
			                    [ Uuid::uuid4(), 'sys', 1 ],
			                    [ Uuid::uuid4(), 'crm', 1 ],
			                    [ Uuid::uuid4(), 'ver', 1 ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		// очищаем
		$this->execute( "SET foreign_key_checks = 0;" );
		$this->execute( "TRUNCATE message_type" );
		$this->execute( "SET foreign_key_checks = 1;" );
	}
}
