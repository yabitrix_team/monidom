<?php

use yii\db\Migration;

/**
 * Class m180529_044520_alter_checking_account_column_to_requests_table
 */
class m180529_044520_alter_checking_account_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'checking_account', $this->char( 20 ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'requests', 'checking_account', $this->integer( 1 ) );
	}
}
