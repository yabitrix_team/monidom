<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner_leads`.
 */
class m180821_120758_create_partner_leads_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'partner_leads', [
			'id'                     => $this->primaryKey(),
			'summ'                   => $this->integer( 11 )->null(),
			'client_first_name'      => $this->string( 255 )->notNull(),
			'client_last_name'       => $this->string( 255 )->notNull(),
			'client_patronymic'      => $this->string( 255 )->notNull(),
			'client_mobile_phone'    => $this->string( 20 )->notNull(),
			'region'                 => $this->char( 255 )->null(),
			'auto_brand_guid'        => $this->char( 36 )->null(),
			'auto_model_guid'        => $this->char( 36 )->null(),
			'client_passport_number' => $this->char( 6 )->null(),
			'client_serial_number'   => $this->char( 4 )->null(),
			'comment'                => $this->text()->null(),
			'client_birthday'        => $this->date()->null(),
			'user_id'                => $this->integer( 11 )->notNull(),
			'created_at'             => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'partner_leads' );
	}
}
