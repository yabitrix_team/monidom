<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_versions_dictionaries`.
 */
class m180202_101117_create_api_versions_dictionaries_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'api_versions_dictionaries', [
			'id'      => $this->primaryKey(),
			'version' => $this->integer( 10 )->notNull(),
			'bx_id'   => $this->string( 30 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'api_versions_dictionaries' );
	}
}
