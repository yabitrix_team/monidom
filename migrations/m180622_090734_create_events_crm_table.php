<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_crm`.
 */
class m180622_090734_create_events_crm_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'events_crm', [
			'id'         => $this->primaryKey(),
			'code'       => $this->char( 3 )->notNull()->unique(),
			'guid'       => $this->string( 36 )->notNull(),
			'name'       => $this->string( 255 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( 'Y' ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'events_crm' );
	}
}
