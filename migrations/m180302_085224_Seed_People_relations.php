<?php

use yii\db\Migration;

/**
 * Class m180302_085224_Seed_People_relations
 */
class m180302_085224_Seed_People_relations extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'people_relations', [ 'guid', 'name', 'active', 'sort' ], [
			[ '03aa730e-dd5b-11e6-811d-00155d010001', 'Родственник', '1', '100' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010002', 'Знакомый', '1', '200' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010003', 'Гражданский(-ая_ муж\жена', '1', '300' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010004', 'Коллега', '1', '800' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010005', 'Сосед(-ка)', '1', '500' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010006', 'Другое', '1', '600' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010007', 'Супруг(-а_', '1', '400' ],
			[ '03aa730e-dd5b-11e6-811d-00155d010008', 'Брат/сестра', '1', '700' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010001' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010002' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010003' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010004' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010005' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010006' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010007' ] );
		$this->delete( 'people_relations', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d010008' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_085224_Seed_People_relations cannot be reverted.\n";

		return false;
	}
	*/
}
