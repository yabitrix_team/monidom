<?php

use yii\db\Migration;

/**
 * Class m180329_143055_rename_api_fcm_table
 */
class m180329_143055_rename_api_fcm_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameColumn( 'api_fcm', 'date', 'created_at' );
		$this->renameTable( 'api_fcm', 'fcm' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->renameColumn( 'fcm', 'created_at', 'date' );
		$this->renameTable( 'fcm', 'api_fcm' );
	}
}
