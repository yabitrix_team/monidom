<?php

use yii\db\Migration;

/**
 * Class m180605_135114_insert_to_cabinets_table
 */
class m180605_135114_insert_to_cabinets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert( 'cabinets', [
            "code"       => '95c5d6f60139ecc50e56ae4030b258db',
            "name"       => 'ЛКСВ',
            "subdomain"  => 'service',
            "created_at" => date( 'Y-m-d H:i:s', time() ),
            "active"     => 1,
            "sort"       => 100,
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('cabinets', [ 'code' => '95c5d6f60139ecc50e56ae4030b258db' ]);

    }
}
