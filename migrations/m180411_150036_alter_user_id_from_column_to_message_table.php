<?php

use yii\db\Migration;

/**
 * Class m180411_150036_alter_user_id_from_column_to_message_table
 */
class m180411_150036_alter_user_id_from_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'user_id_from', 'int(11) NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'user_id_from', 'int(11) NOT NULL' );
	}
}
