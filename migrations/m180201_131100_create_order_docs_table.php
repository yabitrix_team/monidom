<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_docs`.
 */
class m180201_131100_create_order_docs_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'order_docs', [
			'id'         => $this->primaryKey(),
			'guid'       => $this->char( 36 )->notNull()->unique(),
			'name'       => $this->string( 255 )->notNull(),
			'order_id'   => $this->integer( 11 )->notNull(),
			'file_id'    => $this->integer( 11 )->notNull(),
			'file_type'  => "ENUM('foto_sts', 'foto_pts', 'foto_car', 'foto_passport', 'foto_client', 'doc_pack_1', 'doc_pack_2') NOT NULL",
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( 1 ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'order_docs' );
	}
}
