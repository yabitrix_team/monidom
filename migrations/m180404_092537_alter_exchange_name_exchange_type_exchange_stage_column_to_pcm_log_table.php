<?php

use yii\db\Migration;

/**
 * Class m180404_092537_alter_exchange_name_exchange_type_exchange_stage_column_to_pcm_log_table
 */
class m180404_092537_alter_exchange_name_exchange_type_exchange_stage_column_to_pcm_log_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'pcm_log', 'exchange_name', $this->integer( 1 )->notNull() );
		$this->alterColumn( 'pcm_log', 'exchange_type', $this->integer( 1 )->notNull() );
		$this->alterColumn( 'pcm_log', 'exchange_stage', $this->integer( 1 )->notNull() );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'pcm_log', 'exchange_name', $this->smallInteger( 1 )->notNull() );
		$this->alterColumn( 'pcm_log', 'exchange_type', $this->smallInteger( 1 )->notNull() );
		$this->alterColumn( 'pcm_log', 'exchange_stage', $this->smallInteger( 1 )->notNull() );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_092537_alter_exchange_name_exchange_type_exchange_stage_column_to_pcm_log_table cannot be reverted.\n";

		return false;
	}
	*/
}
