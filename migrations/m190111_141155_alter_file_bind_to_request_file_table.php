<?php

use yii\db\Migration;

/**
 * Class m190111_141155_alter_file_bind_to_request_file_table
 */
class m190111_141155_alter_file_bind_to_request_file_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'request_file';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(
            $this->table,
            'file_bind',
            "ENUM('foto_sts','foto_pts','foto_auto','foto_passport','foto_client', 'foto_extra', 'foto_card', 'doc_pack_1','doc_pack_2','foto_notification') NOT NULL"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(
            $this->table,
            'file_bind',
            "ENUM('foto_sts','foto_pts','foto_auto','foto_passport','foto_client', 'foto_extra', 'foto_card', 'doc_pack_1','doc_pack_2') NOT NULL"
        );
    }
}
