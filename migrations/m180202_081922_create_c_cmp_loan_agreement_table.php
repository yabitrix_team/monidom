<?php

use yii\db\Migration;

/**
 * Handles the creation of table `c_cmp_loan_agreement`.
 */
class m180202_081922_create_c_cmp_loan_agreement_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'c_cmp_loan_agreement', [
			'id'                    => $this->primaryKey(),
			'sid'                   => $this->string( 255 )->notNull(),
			'created_at'            => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'order_id'              => $this->integer( 11 )->notNull(),
			'order_xml'             => $this->char( 36 )->notNull(),
			'order'                 => $this->string( 255 )->notNull(),
			'user_xml'              => $this->char( 36 )->notNull(),
			'user_id'               => $this->integer( 11 )->notNull(),
			'status'                => $this->string( 6 )->notNull(),
			'date_loan'             => $this->datetime()->notNull(),
			'date_closed'           => $this->datetime()->notNull(),
			'date_last_update'      => $this->datetime()->notNull(),
			'loan_amount'           => $this->double()->notNull(),
			'interest_rate'         => $this->double()->notNull(),
			'total_amount_payments' => $this->double()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'c_cmp_loan_agreement' );
	}
}
