<?php

use yii\db\Migration;

/**
 * Class m180404_133032_alter_columns_in_pcm_logs_tables
 */
class m180404_133032_alter_columns_in_pcm_logs_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->alterColumn( 'pcm_log_exchange_name', 'id', 'TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_exchange_type', 'id', 'TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_exchange_stage', 'id', 'TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT' );

	    $this->alterColumn( 'pcm_log_error_type', 'id', 'TINYINT(1) UNSIGNED NOT NULL AUTO_INCREMENT' );

	    $this->alterColumn( 'pcm_log_error', 'id', 'INT(4) UNSIGNED NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_error', 'log_id', 'INT(4) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log_error', 'type', 'TINYINT(1) UNSIGNED NOT NULL' );

	    $this->alterColumn( 'pcm_log_order', 'id', 'INT(4) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log_order', 'id_xml', 'VARCHAR(255) NOT NULL' );

	    $this->alterColumn( 'pcm_log_data', 'id', 'INT(4) UNSIGNED NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_data', 'log_id', 'INT(4) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'data', 'TEXT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'query', 'TEXT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'file', 'LONGTEXT NULL' );

	    $this->alterColumn( 'pcm_log', 'id', 'INT(4) UNSIGNED NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log', 'exchange_name', 'TINYINT(1) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'exchange_type', 'TINYINT(1) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'exchange_stage', 'TINYINT(1) UNSIGNED NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'flow', 'TINYINT(1) UNSIGNED NULL' );
	    $this->alterColumn( 'pcm_log', 'created_at', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP' );
	    $this->alterColumn( 'pcm_log', 'date_start', 'TIMESTAMP NULL' );
	    $this->alterColumn( 'pcm_log', 'date_end', 'TIMESTAMP NULL' );
	    $this->alterColumn( 'pcm_log', 'order_id', 'INT(4) UNSIGNED NULL' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->alterColumn( 'pcm_log_exchange_name', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_exchange_type', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_exchange_stage', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );

	    $this->alterColumn( 'pcm_log_error_type', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );

	    $this->alterColumn( 'pcm_log_error', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_error', 'log_id', 'INT(4) NOT NULL' );
	    $this->alterColumn( 'pcm_log_error', 'type', 'INT(1) NOT NULL' );

	    $this->alterColumn( 'pcm_log_order', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_order', 'id_xml', 'CHAR(36) NOT NULL' );

	    $this->alterColumn( 'pcm_log_data', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log_data', 'log_id', 'INT(4) NOT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'data', 'TEXT NOT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'query', 'TEXT NOT NULL' );
	    $this->alterColumn( 'pcm_log_data', 'file', 'LONGTEXT NOT NULL' );

	    $this->alterColumn( 'pcm_log', 'id', 'INT(11) NOT NULL AUTO_INCREMENT' );
	    $this->alterColumn( 'pcm_log', 'exchange_name', 'INT(1) NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'exchange_type', 'INT(1) NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'exchange_stage', 'INT(1) NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'flow', 'SMALLINT(1) NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'created_at', 'DATETIME NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'date_start', 'DATETIME NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'date_end', 'DATETIME NOT NULL' );
	    $this->alterColumn( 'pcm_log', 'order_id', 'INT(11) NOT NULL' );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180404_133032_alter_columns_in_pcm_logs_tables cannot be reverted.\n";

        return false;
    }
    */
}
