<?php

use yii\db\Migration;

/**
 * Class m180625_144304_update_events_crm_table
 */
class m180625_144304_update_events_crm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->update('events_crm', ['name' => 'Отправка СМС с ссылкой клиенту'], 'guid = "6beba90f-754e-11e8-a2b7-00155d64d111"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->update('events_crm', ['name' => 'Ожидание статуса "Одобрение'], 'guid = "6beba90f-754e-11e8-a2b7-00155d64d111"');
    }

}
