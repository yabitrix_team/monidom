<?php

use yii\db\Migration;

/**
 * Class m180402_141513_add_foreign_key_for_type_id_to_message_table
 */
class m180402_141513_add_foreign_key_for_type_id_to_message_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-message-type_id',  // это "условное имя" ключа
			'message', // это название текущей таблицы
			'type_id', // это имя поля в текущей таблице, которое будет ключом
			'message_type', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-message-type_id',
			'message'
		);
	}
}
