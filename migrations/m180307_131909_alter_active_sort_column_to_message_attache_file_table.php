<?php

use yii\db\Migration;

/**
 * Class m180307_131909_alter_active_sort_column_to_message_attache_file_table
 */
class m180307_131909_alter_active_sort_column_to_message_attache_file_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'message_attache_file', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'message_attache_file', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'message_attache_file', 'active', 'char(1) not null' );
		$this->alterColumn( 'message_attache_file', 'sort', 'int(11) not null' );
	}
}
