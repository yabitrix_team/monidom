<?php

use yii\db\Migration;

/**
 * Class m180907_065606_alter_partner_leads_table
 */
class m180907_065606_alter_partner_leads_table extends Migration
{

    protected $table_name = 'partner_leads';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table_name, 'status_guid', 'varchar(36) not null after summ');
        $this->addColumn($this->table_name, 'guid', 'varchar(36) not null after summ');
        $this->addColumn($this->table_name, 'updated_at', $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('partner_leads', 'status_guid');
        $this->dropColumn('partner_leads', 'guid');
        $this->dropColumn('partner_leads', 'updated_at');
    }
}
