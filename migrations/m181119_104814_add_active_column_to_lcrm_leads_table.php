<?php

use yii\db\Migration;

/**
 * Class m181119_104814_alter_active_column_to_lcrm_leads_table
 */
class m181119_104814_add_active_column_to_lcrm_leads_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('lcrm_leads', 'active', 'tinyint(1) not null default 1 after date_return');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn( 'lcrm_leads', 'active' );
    }
}
