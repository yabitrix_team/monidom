<?php

use yii\db\Migration;

/**
 * Class m180913_095322_alter_client_birthday_to_leads
 */
class m180913_095322_alter_client_birthday_to_leads extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'leads';

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( $this->table, 'client_birthday', $this->date() );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( $this->table, 'client_birthday', $this->date()->notNull() );
	}
}
