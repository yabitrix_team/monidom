<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay_cloud`.
 */
class m180514_141538_create_pay_cloud_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'pay_cloud', [
			'id'                => $this->primaryKey(),
			'payment'           => $this->integer()->notNull(),
			'action'            => "enum('check', 'pay', 'fail')",
			'status'            => $this->string()->notNull(),
			'type'              => $this->string()->notNull(),
			'date'              => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
			'name'              => $this->string()->notNull(),
			'email'             => $this->string()->notNull(),
			'card_number_first' => $this->integer()->notNull(),
			'card_number_last'  => $this->integer()->notNull(),
			'card_type'         => $this->string()->notNull(),
			'card_exp'          => $this->string()->notNull(),
			'ip'                => $this->string()->notNull(),
			'country'           => $this->string()->notNull(),
			'city'              => $this->string()->notNull(),
			'region'            => $this->string()->notNull(),
			'district'          => $this->string()->notNull(),
			'bank_name'         => $this->string()->notNull(),
			'bank_country'      => $this->string()->notNull(),
			'token_payee'       => $this->string()->notNull(),
			'token_card'        => $this->string()->notNull(),
			'error'             => $this->string()->notNull(),
			'error_code'        => $this->integer()->notNull(),
			'data'              => $this->text()->notNull(),
			'created_at'        => $this->dateTime()->defaultValue( ( new \yii\db\Expression( 'now()' ) ) ),
		] );
		$this->addForeignKey(
			'fk-pay_cloud-payment',  // это "условное имя" ключа
			'pay_cloud', // это название текущей таблицы
			'payment', // это имя поля в текущей таблице, которое будет ключом
			'pay_payment', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE',
			'CASCADE'
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'pay_cloud' );
	}
}
