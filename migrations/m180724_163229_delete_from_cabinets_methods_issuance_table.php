<?php

use yii\db\Migration;

/**
 * Class m180724_163229_delete_from_cabinets_methods_issuance_table
 */
class m180724_163229_delete_from_cabinets_methods_issuance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->delete('cabinets_methods_issuance', [
	    	'cabinet_id' => 1,
		    'method_id'  => 7,
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->insert( 'cabinets_methods_issuance', [
		    'cabinet_id' => '1',
		    "method_id"  => '7',
	    ] );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_163229_delete_from_cabinets_methods_issuance_table cannot be reverted.\n";

        return false;
    }
    */
}
