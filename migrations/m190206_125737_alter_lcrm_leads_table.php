<?php

use yii\db\Migration;

/**
 * Class m190206_125737_alter_request_table
 */
class m190206_125737_alter_lcrm_leads_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lcrm_leads', 'comment', $this->string());
        $this->addCommentOnColumn('lcrm_leads', 'comment', 'Коментарий');
        $this->addColumn('lcrm_leads', 'callback_time', $this->dateTime());
        $this->addCommentOnColumn('lcrm_leads', 'callback_time', 'Дата обратного звонка');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('lcrm_leads', 'comment');
        $this->dropColumn('lcrm_leads', 'callback_time');
    }
}
