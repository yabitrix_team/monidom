<?php

use yii\db\Migration;

/**
 * Class m180302_133055_seed_workplace_periods
 */
class m180302_133055_seed_workplace_periods extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'workplace_periods', [ 'guid', 'name', 'active', 'sort' ], [
			[ '46b775b8-d8c0-11e7-814a-01155d01bf50', 'Год', '1', '100' ],
			[ '46b775b8-d8c0-11e7-814a-02155d01bf51', 'Месяц', '1', '200' ],
			[ '46b775b8-d8c0-11e7-814a-03155d01bf52', 'День', '1', '150' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'workplace_periods', [ 'guid' => '46b775b8-d8c0-11e7-814a-01155d01bf50' ] );
		$this->delete( 'workplace_periods', [ 'guid' => '46b775b8-d8c0-11e7-814a-02155d01bf51' ] );
		$this->delete( 'workplace_periods', [ 'guid' => '46b775b8-d8c0-11e7-814a-03155d01bf52' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_133055_seed_workplace_periods cannot be reverted.\n";

		return false;
	}
	*/
}
