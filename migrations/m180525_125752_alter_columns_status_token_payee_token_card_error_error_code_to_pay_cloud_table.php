<?php

use yii\db\Migration;

/**
 * Class m180525_125752_alter_columns_status_token_payee_token_card_error_error_code_to_pay_cloud_table
 */
class m180525_125752_alter_columns_status_token_payee_token_card_error_error_code_to_pay_cloud_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'pay_cloud', 'status', $this->string() );
		$this->alterColumn( 'pay_cloud', 'token_payee', $this->string() );
		$this->alterColumn( 'pay_cloud', 'token_card', $this->string() );
		$this->alterColumn( 'pay_cloud', 'error', $this->string() );
		$this->alterColumn( 'pay_cloud', 'error_code', $this->integer() );

		$this->alterColumn( 'pay_payment', 'invoice', $this->string()->unique()->notNull() );
		$this->alterColumn( 'pay_payment', 'transaction', $this->string()->unique()->notNull() );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'pay_cloud', 'status', $this->string()->notNull() );
		$this->alterColumn( 'pay_cloud', 'token_payee', $this->string()->notNull() );
		$this->alterColumn( 'pay_cloud', 'token_card', $this->string()->notNull() );
		$this->alterColumn( 'pay_cloud', 'error', $this->string()->notNull() );
		$this->alterColumn( 'pay_cloud', 'error_code', $this->integer()->notNull() );

		$this->alterColumn( 'pay_payment', 'invoice', $this->string()->notNull() );
		$this->alterColumn( 'pay_payment', 'transaction', $this->string()->notNull() );
		$this->dropIndex(
			'invoice',
			'pay_payment'
		);
		$this->dropIndex(
			'transaction',
			'pay_payment'
		);
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180525_125752_alter_columns_status_token_payee_token_card_error_error_code_to_pay_cloud_table cannot be reverted.\n";

		return false;
	}
	*/
}
