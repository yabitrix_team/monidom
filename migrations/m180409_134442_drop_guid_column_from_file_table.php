<?php

use yii\db\Migration;

/**
 * Handles dropping guid from table `file`.
 */
class m180409_134442_drop_guid_column_from_file_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->dropIndex( 'guid', 'file' );
		$this->dropColumn( 'file', 'guid' );
		$this->alterColumn( 'file', 'updated_at', 'TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP' );
		$this->alterColumn( 'file', 'height', 'INT(11) NULL' );
		$this->alterColumn( 'file', 'width', 'INT(11) NULL' );
		$this->alterColumn( 'file', 'description', 'TEXT NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->addColumn( 'file', 'guid', 'CHAR(36) NULL' );
		$this->createIndex( 'guid', 'file', 'guid', true );
		$this->alterColumn( 'file', 'updated_at', 'DATETIME NOT NULL' );
		$this->alterColumn( 'file', 'height', 'INT(11) NOT NULL' );
		$this->alterColumn( 'file', 'width', 'INT(11) NOT NULL' );
		$this->alterColumn( 'file', 'description', 'TEXT NOT NULL' );
	}
}
