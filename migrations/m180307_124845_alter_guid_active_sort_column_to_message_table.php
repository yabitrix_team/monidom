<?php

use yii\db\Migration;

/**
 * Class m180307_124845_alter_guid_active_sort_column_to_message_table
 */
class m180307_124845_alter_guid_active_sort_column_to_message_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'guid', 'char(36) null' );
		$this->alterColumn( 'message', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'message', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'guid', 'char(36) not null' );
		$this->alterColumn( 'message', 'active', 'char(1) not null' );
		$this->alterColumn( 'message', 'sort', 'int(11) not null' );
	}
}
