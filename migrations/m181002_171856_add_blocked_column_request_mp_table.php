<?php

use yii\db\Migration;

/**
 * Class m181002_171856_add_blocked_column_request_mp_table
 */
class m181002_171856_add_blocked_column_request_mp_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'request_mp';

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn(
			$this->table,
			'blocked',
			$this->tinyInteger( 1 )->defaultValue( 0 )->after( 'service_info' )
		);
		$this->addCommentOnColumn( $this->table, 'blocked', 'Флаг, отвечающий за блокировку заявки в МП' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( $this->table, 'blocked' );
	}
}
