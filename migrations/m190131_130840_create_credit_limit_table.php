<?php

use yii\db\Migration;

/**
 * Handles the creation of table `credit_limit`.
 */
class m190131_130840_create_credit_limit_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = '{{credit_limit}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'         => $this->primaryKey(),
                'code'       => $this->char(32)->unique(),
                'phone'      => $this->char(10),
                'amount'     => $this->integer(),
                'period'     => $this->smallInteger(),
                'full_name'  => $this->string(255),
                'offer_type' => $this->string(255),
                'active'     => $this->tinyInteger(1)->defaultValue(1),
                'created_at' => $this->datetime()->defaultExpression('CURRENT_TIMESTAMP'),
                'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            ]
        );
        $this->addCommentOnColumn(
            $this->table,
            'code',
            'Символьный код кредитного лимита, генериться с помощью md5 относительно связки номера телефона и типа предложения'
        );
        $this->addCommentOnColumn($this->table, 'period', 'Срок займа, указывается в месяцах');
        $this->addCommentOnColumn($this->table, 'offer_type', 'Тип предложения');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
