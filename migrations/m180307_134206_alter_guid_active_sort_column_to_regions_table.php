<?php

use yii\db\Migration;

/**
 * Class m180307_134206_alter_guid_active_sort_column_to_regions_table
 */
class m180307_134206_alter_guid_active_sort_column_to_regions_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'regions', 'guid', 'char(36) null' );
		$this->alterColumn( 'regions', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'regions', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'regions', 'guid', 'char(36) not null' );
		$this->alterColumn( 'regions', 'active', 'char(1) not null' );
		$this->alterColumn( 'regions', 'sort', 'int(11) not null' );
	}
}
