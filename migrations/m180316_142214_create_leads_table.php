<?php

use yii\db\Migration;

/**
 * Handles the creation of table `leads`.
 */
class m180316_142214_create_leads_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->createTable( 'leads', [
			'id'                            => $this->primaryKey(),
			'token'                         => $this->string( 32 )->notNull(),
			'summ'                          => $this->integer( 11 )->null(),
			'months'                        => $this->integer( 2 )->null(),
			'client_first_name'             => $this->string( 255 )->notNull(),
			'client_last_name'              => $this->string( 255 )->notNull(),
			'client_patronymic'             => $this->string( 255 )->notNull(),
			'client_mobile_phone'           => $this->string( 20 )->notNull(),
			'client_passport_serial_number' => $this->char( 4 )->null(),
			'client_passport_number'        => $this->char( 6 )->null(),
			'client_birthday'               => $this->date()->notNull(),
			'vin'                           => $this->string( 255 )->notNull(),
			'created_at'                    => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'is_closed'                     => $this->char( 1 )->null()->defaultValue( 0 ),
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropTable( 'leads' );
	}
}
