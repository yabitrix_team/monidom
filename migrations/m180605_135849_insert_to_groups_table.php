<?php

use yii\db\Migration;

/**
 * Class m180605_135849_insert_to_groups_table
 */
class m180605_135849_insert_to_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert( "groups", [
            'identifier'   => 'verificator',
            'name'   => 'Верификаторы',
            "created_at" => date( 'Y-m-d H:i:s', time() ),
            'active' => '1',
            'sort'   => '100',
        ] );
        $cabinets = Yii::$app->db->createCommand( 'SELECT
  *
FROM cabinets
WHERE code IN (
\'95c5d6f60139ecc50e56ae4030b258db\'
)' )
            ->queryAll();
        //relations
        $this->update( 'groups',
            [ 'prior_cabinet_id' => $cabinets[0]['id'] ],
            'identifier = \'verificator\''
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('groups', [ 'identifier' => 'verificator' ]);
    }


}
