<?php

use yii\db\Migration;

/**
 * Handles adding num to table `requests`.
 */
class m180322_162220_add_num_column_to_requests_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( 'requests', 'num', 'int(11) null after guid' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( 'requests', 'num' );
	}
}
