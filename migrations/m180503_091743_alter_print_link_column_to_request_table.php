<?php

use yii\db\Migration;

/**
 * Class m180503_091743_alter_print_link_column_to_request_table
 */
class m180503_091743_alter_print_link_column_to_request_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'requests', 'print_link', 'varchar(8) null' );
		$this->db->createCommand( "UPDATE requests SET print_link = (NULL) WHERE print_link = ''" )->execute();
		$this->alterColumn( 'requests', 'print_link', 'varchar(8) unique' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropIndex( 'print_link', 'requests' );
		$this->alterColumn( 'requests', 'print_link', 'varchar(8) not null' );
	}
}
