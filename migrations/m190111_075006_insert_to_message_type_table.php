<?php

use Ramsey\Uuid\Uuid;
use yii\db\Migration;

/**
 * Class m190111_075006_insert_to_message_type_table
 */
class m190111_075006_insert_to_message_type_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function safeUp() {

        $this->batchInsert( 'message_type',
            [ 'guid', 'name', 'active', 'public' ],
            [
                [ Uuid::uuid4(), 'refirstpack', 1, 1 ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {

        $this->execute( "SET foreign_key_checks = 0;" );
        $this->delete( 'message_type', [
            'name' => 'refirstpack',
        ] );
        $this->execute( "SET foreign_key_checks = 1;" );
    }
}
