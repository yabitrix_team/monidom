<?php

use yii\db\Migration;

/**
 * Class m180307_132613_alter_guid_active_sort_column_to_methods_issuance_table
 */
class m180307_132613_alter_guid_active_sort_column_to_methods_issuance_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'methods_issuance', 'guid', 'char(36) null' );
		$this->alterColumn( 'methods_issuance', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'methods_issuance', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'methods_issuance', 'guid', 'char(36) not null' );
		$this->alterColumn( 'methods_issuance', 'active', 'char(1) not null' );
		$this->alterColumn( 'methods_issuance', 'sort', 'int(11) not null' );
	}
}
