<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mp_version`.
 */
class m181221_125140_create_mp_version_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'mp_version';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            $this->table,
            [
                'id'         => $this->primaryKey(),
                'os_type'    => "ENUM('android','ios')",
                'minimal'    => $this->string(30),
                'current'    => $this->string(30),
                'updated_at' => $this->timestamp('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                'created_at' => $this->datetime()->defaultExpression('CURRENT_TIMESTAMP'),
            ]
        );
        $this->batchInsert(
            $this->table,
            ['os_type', 'minimal', 'current'],
            [
                ['android', '1.3.7', '2.3.7'],
                ['ios', '1.1.0', '2.2.1'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
