<?php

use yii\db\Migration;

/**
 * Handles adding contract to table `requests`.
 */
class m180727_131313_add_num_1c_column_to_requests_table extends Migration {
	/**
	 * @var string - название таблицы
	 */
	protected $table = 'requests';

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->addColumn( $this->table, 'num_1c', $this->string()->after( 'code' ) );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->dropColumn( $this->table, 'num_1c' );
	}
}
