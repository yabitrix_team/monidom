<?php

use yii\db\Migration;

/**
 * Class m180404_132802_drop_foreign_keys_from_pcm_log_tables
 */
class m180404_132802_drop_foreign_keys_from_pcm_log_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->dropForeignKey(
		    'fk-pcm_log_data-log_id',
		    'pcm_log_data'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log_error-log_id',
		    'pcm_log_error'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log_error-type',
		    'pcm_log_error'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log-exchange_name',
		    'pcm_log'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log-exchange_type',
		    'pcm_log'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log-exchange_stage',
		    'pcm_log'
	    );
	    $this->dropForeignKey(
		    'fk-pcm_log-order_id',
		    'pcm_log'
	    );

	    $this->dropIndex(
		    'fk-pcm_log_data-log_id',
		    'pcm_log_data'
	    );
	    $this->dropIndex(
		    'fk-pcm_log_error-log_id',
		    'pcm_log_error'
	    );
	    $this->dropIndex(
		    'fk-pcm_log_error-type',
		    'pcm_log_error'
	    );
	    $this->dropIndex(
		    'fk-pcm_log-exchange_name',
		    'pcm_log'
	    );
	    $this->dropIndex(
		    'fk-pcm_log-exchange_type',
		    'pcm_log'
	    );
	    $this->dropIndex(
		    'fk-pcm_log-exchange_stage',
		    'pcm_log'
	    );
	    $this->dropIndex(
		    'fk-pcm_log-order_id',
		    'pcm_log'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->addForeignKey(
		    'fk-pcm_log_data-log_id',  // это "условное имя" ключа
		    'pcm_log_data', // это название текущей таблицы
		    'log_id', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log_error-log_id',  // это "условное имя" ключа
		    'pcm_log_error', // это название текущей таблицы
		    'log_id', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log_error-type',  // это "условное имя" ключа
		    'pcm_log_error', // это название текущей таблицы
		    'type', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log_error_type', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log-exchange_name',  // это "условное имя" ключа
		    'pcm_log', // это название текущей таблицы
		    'exchange_name', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log_exchange_name', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log-exchange_type',  // это "условное имя" ключа
		    'pcm_log', // это название текущей таблицы
		    'exchange_type', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log_error_type', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log-exchange_stage',  // это "условное имя" ключа
		    'pcm_log', // это название текущей таблицы
		    'exchange_stage', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log_exchange_name', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
	    $this->addForeignKey(
		    'fk-pcm_log-order_id',  // это "условное имя" ключа
		    'pcm_log', // это название текущей таблицы
		    'order_id', // это имя поля в текущей таблице, которое будет ключом
		    'pcm_log_order', // это имя таблицы, с которой хотим связаться
		    'id', // это поле таблицы, с которым хотим связаться
		    'CASCADE'
	    );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180404_132802_drop_foreign_keys_from_pcm_log_tables cannot be reverted.\n";

        return false;
    }
    */
}
