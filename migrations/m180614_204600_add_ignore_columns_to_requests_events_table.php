<?php

use yii\db\Migration;

/**
 * Handles adding ignore to table `requests_events`.
 */
class m180614_204600_add_ignore_columns_to_requests_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('requests_events', 'is_ignored', "tinyint(1) not null default '0' after is_sended");
	    $this->addColumn('requests_events', 'ignore_reason', "varchar(512) not null after is_ignored");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('requests_events', 'is_ignored');
	    $this->dropColumn('requests_events', 'ignore_reason');
    }
}
