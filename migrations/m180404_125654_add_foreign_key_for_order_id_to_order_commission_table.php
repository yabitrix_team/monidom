<?php

use yii\db\Migration;

/**
 * Class m180404_125654_add_foreign_key_for_order_id_to_order_commission_table
 */
class m180404_125654_add_foreign_key_for_order_id_to_order_commission_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-order_commission-order_id',  // это "условное имя" ключа
			'order_commission', // это название текущей таблицы
			'order_id', // это имя поля в текущей таблице, которое будет ключом
			'requests', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-order_commission-order_id',
			'order_commission'
		);
		$this->dropIndex(
			'fk-order_commission-order_id',
			'order_commission'
		);

	}
}
