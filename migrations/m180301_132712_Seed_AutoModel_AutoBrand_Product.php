<?php

use yii\db\Migration;

/**
 * Class m180301_132712_Seed_AutoModel_AutoBrand_Product
 */
class m180301_132712_Seed_AutoModel_AutoBrand_Product extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//seed auto_brands
		$this->batchInsert( 'auto_brands', [ 'guid', 'name', 'active', 'sort' ], [
			[ 'd83751sdfsddfsdsffdsfssdfwe23fwe', 'Ford', '1', '15' ],
			[ 'd83752sdfsddfsdsffdsfssdfwe23fwe', 'MySQLvdv', '1', '50' ],
			[ 'd83753sdfsddfsdsffdsfssdfwe23fwe', 'YIIdVa', '1', '30' ],
			[ 'd83754sdfsddfsdsffdsfssdfwe23fwe', 'Porshe', '1', '10' ],
			[ 'd83755sdfsddfsdsffdsfssdfwe23fwe', 'mazda', '1', '20' ],
			[ 'd83756sdfsddfsdsffdsfssdfwe23fwe', 'Kia', '1', '20' ],
			[ 'd83757sdfsddfsdsffdsfssdfwe23fwe', 'ZHiguly', '1', '30' ],
		] );
		//seed auto_models
		$this->batchInsert( 'auto_models', [ 'guid', 'name', 'auto_brand_id', 'active', 'sort' ], [
			[ 'd83132sdfsddfsdsffdsfssdfwe23fwe', 'Car model 1', '1', '1', '10' ],
			[ 'd83232sdfsddfsdsffdsfssdfwe23fwe', 'Car model 2', '1', '1', '900' ],
			[ 'd83332sdfsddfsdsffdsfssdfwe23fwe', 'Car model 3', '1', '1', '20' ],
			[ 'd83432sdfsddfsdsffdsfssdfwe23fwe', 'Car model 4', '2', '1', '310' ],
			[ 'd83532sdfsddfsklffdsfssdfwe23fwe', 'Car model 5', '2', '1', '360' ],
			[ 'd83632sdfsddfsdsffdsfssdfwe23fwe', 'Car model 6', '2', '1', '50' ],
			[ 'd83772sdfsddfsdsffdsfssdfwe23fwe', 'Car model 7', '3', '0', '30' ],
			[ 'd83832sdfsddfsdsffdsfssdfwe23fwe', 'Car model 8', '3', '1', '270' ],
			[ 'd83932sdfsddfsdsffdsfssdfwe23fwe', 'Car model 9', '4', '1', '45' ],
			[ 'd83712sdfsddfsdsffdsfssdfwe23fwe', 'Car model 10', '4', '1', '60' ],
			[ 'd83722sdfsddfsdsffdsfssdfwe23fwe', 'Car model 11', '5', '1', '150' ],
			[ 'd83532sdfsdtfsdsffdsfssdfwe23fwe', 'Car model 12', '5', '0', '90' ],
			[ 'd83742sdfsddfsdsffdsfssdfwe23fwe', 'Car model 13', '6', '1', '80' ],
			[ 'd83752sdfsddfsdsffdsfssdfwe23fwe', 'Car model 14', '6', '1', '700' ],
			[ 'd83762sdfsddfsdsffdsfssdfwe23fwe', 'Car model 15', '7', '1', '100' ],
			[ 'd83742sdfsddfsdsffdsfssdfwe23f5e', 'Car model 16', '7', '1', '50' ],
		] );
		//seed product
		$this->batchInsert( 'products', [
			'guid',
			'name',
			'is_additional_loan',
			'percent_per_day',
			'month_num',
			'summ_min',
			'summ_max',
			'age_min',
			'age_max',
			'loan_period_min',
			'loan_period_max',
			'active',
			'sort',
		], [
			                    [
				                    'd83132sdfsddfsd5ffdsfssdfwe23fwe',
				                    'Автомобиль остается с Вами 24V! (докредитование) 1',
				                    '1',
				                    '0.22',
				                    '24',
				                    '75000',
				                    '1000000',
				                    '21',
				                    '67',
				                    '24',
				                    '24',
				                    '1',
				                    '10',
			                    ],
			                    [
				                    'd83232sdfsddfsd6ffdsfssdfwe23fwe',
				                    'Автомобиль остается с Вами 24V! (докредитование) 2',
				                    '1',
				                    '0.22',
				                    '24',
				                    '75000',
				                    '1000000',
				                    '21',
				                    '67',
				                    '24',
				                    '24',
				                    '1',
				                    '80',
			                    ],
			                    [
				                    'd83332sdfsddfsd7ffdsfssdfwe23fwe',
				                    'Автомобиль остается с Вами 36V! (докредитование) 3',
				                    '1',
				                    '0.22',
				                    '36',
				                    '75000',
				                    '1000000',
				                    '21',
				                    '67',
				                    '36',
				                    '36',
				                    '1',
				                    '66',
			                    ],
			                    [
				                    'd83432sdfsddfsd8ffdsfssdfwe23fwe',
				                    'Автомобиль остается с Вами 36V! (докредитование) 4',
				                    '1',
				                    '0.22',
				                    '36',
				                    '75000',
				                    '1000000',
				                    '21',
				                    '67',
				                    '36',
				                    '36',
				                    '1',
				                    '900',
			                    ],
			                    [
				                    'd83532sdfsddfsd0ffdsfssdfwe23fwe',
				                    'Автомобиль остается с Вами 12V!  5',
				                    '1',
				                    '0.22',
				                    '12',
				                    '75000',
				                    '1000000',
				                    '21',
				                    '67',
				                    '12',
				                    '12',
				                    '1',
				                    '15',
			                    ],
		                    ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'auto_brands', [ 'guid' => 'd83751sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83752sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83753sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83754sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83755sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83756sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_brands', [ 'guid' => 'd83757sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83132sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83232sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83332sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83432sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83532sdfsddfsklffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83632sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83772sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83832sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83932sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83712sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83722sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83532sdfsdtfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83742sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83752sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83762sdfsddfsdsffdsfssdfwe23fwe' ] );
		$this->delete( 'auto_models', [ 'guid' => 'd83742sdfsddfsdsffdsfssdfwe23f5e' ] );
		$this->delete( 'products', [ 'guid' => 'd83132sdfsddfsd5ffdsfssdfwe23fwe' ] );
		$this->delete( 'products', [ 'guid' => 'd83232sdfsddfsd6ffdsfssdfwe23fwe' ] );
		$this->delete( 'products', [ 'guid' => 'd83332sdfsddfsd7ffdsfssdfwe23fwe' ] );
		$this->delete( 'products', [ 'guid' => 'd83432sdfsddfsd8ffdsfssdfwe23fwe' ] );
		$this->delete( 'products', [ 'guid' => 'd83532sdfsddfsd0ffdsfssdfwe23fwe' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180301_132712_Seed_AutoModel_AutoBrand_Product cannot be reverted.\n";

		return false;
	}
	*/
}
