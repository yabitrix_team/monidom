<?php

use yii\db\Migration;

/**
 * Handles the creation of table `points`.
 */
class m180131_123312_create_points_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'points', [
			'id'         => $this->primaryKey(),
			'guid'       => $this->char( 36 )->notNull()->unique(),
			'name'       => $this->string( 255 )->notNull(),
			'partnerId'  => $this->integer( 11 )->notNull(),
			'country'    => $this->string( 255 )->notNull(),
			'index'      => $this->string( 255 )->notNull(),
			'region'     => $this->string( 255 )->notNull(),
			'area'       => $this->string( 255 )->notNull(),
			'city'       => $this->string( 255 )->notNull(),
			'locality'   => $this->string( 255 )->notNull(),
			'street'     => $this->string( 255 )->notNull(),
			'house'      => $this->smallInteger( 4 )->notNull(),
			'housing'    => $this->smallInteger( 4 )->notNull(),
			'office'     => $this->smallInteger( 4 )->notNull(),
			'brand_name' => $this->string( 255 )->notNull(),
			'time_table' => $this->string( 255 )->notNull(),
			'coords'     => $this->string( 255 )->notNull(),
			'created_at' => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at' => $this->datetime()->notNull()
			                     ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'     => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'       => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'points' );
	}
}
