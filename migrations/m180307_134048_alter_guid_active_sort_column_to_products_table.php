<?php

use yii\db\Migration;

/**
 * Class m180307_134048_alter_guid_active_sort_column_to_products_table
 */
class m180307_134048_alter_guid_active_sort_column_to_products_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'products', 'guid', 'char(36) null' );
		$this->alterColumn( 'products', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'products', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'products', 'guid', 'char(36) not null' );
		$this->alterColumn( 'products', 'active', 'char(1) not null' );
		$this->alterColumn( 'products', 'sort', 'int(11) not null' );
	}
}
