<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_token`.
 */
class m180202_100925_create_api_token_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'api_token', [
			'id'         => $this->primaryKey(),
			'user_id'    => $this->integer( 11 )->notNull(),
			'token'      => $this->char( 32 )->notNull(),
			'expired_at' => $this->integer( 10 )->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'api_token' );
	}
}
