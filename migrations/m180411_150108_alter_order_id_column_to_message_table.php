<?php

use yii\db\Migration;

/**
 * Class m180411_150108_alter_order_id_column_to_message_table
 */
class m180411_150108_alter_order_id_column_to_message_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->alterColumn( 'message', 'order_id', 'int(11) NULL' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->alterColumn( 'message', 'order_id', 'int(11) NOT NULL' );
	}
}
