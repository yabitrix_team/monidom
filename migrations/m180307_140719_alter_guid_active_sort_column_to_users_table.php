<?php

use yii\db\Migration;

/**
 * Class m180307_140719_alter_guid_active_sort_column_to_users_table
 */
class m180307_140719_alter_guid_active_sort_column_to_users_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->alterColumn( 'users', 'guid', 'char(36) null' );
		$this->alterColumn( 'users', 'active', 'tinyint(1) not null default "1"' );
		$this->alterColumn( 'users', 'sort', 'int(11) not null default "1000"' );
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->alterColumn( 'users', 'guid', 'char(36) not null' );
		$this->alterColumn( 'users', 'active', 'char(1) not null' );
		$this->alterColumn( 'users', 'sort', 'int(11) not null' );
	}
}
