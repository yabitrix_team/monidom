<?php

use yii\db\Migration;

/**
 * Class m180419_132631_insert_to_cabinet_table
 */
class m180419_132631_insert_to_cabinet_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'cabinets', [ 'guid', 'name', 'subdomain', 'active', 'sort' ], [
			[ 'caf2f5a8-714b-4c38-3a68-d', 'МП', '', '1', '100' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'cabinets', [ 'guid' => 'caf2f5a8-714b-4c38-3a68-d' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180419_132631_insert_to_cabinet_table cannot be reverted.\n";

		return false;
	}
	*/
}
