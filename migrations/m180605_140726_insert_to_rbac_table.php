<?php

use yii\db\Migration;

/**
 * Class m180605_140726_insert_to_rbac_table
 */
class m180605_140726_insert_to_rbac_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $auth = Yii::$app->authManager;
            //----------------------------------------------------------------------------------------------------------------------------
            //ПРАВИЛО ОПРЕДЕЛЕНИЯ ПОЛЬЗОВАТЕЛЕЙ ПО ГРУППЕ
            //----------------------------------------------------------------------------------------------------------------------------
            $ruleGroup = new \app\modules\app\v1\classes\rules\UserGroupRule;
            //$auth->add( $ruleGroup );
            // добавляем разрешение "accessLKSV"
            $accessLksv              = $auth->createPermission( 'access_lksv' );
            $accessLksv->description = 'Access to LKSV';
            $auth->add( $accessLksv );
            //----------------------------------------------------------------------------------------------------------------------------
            //НАСТРАИВАЕМ РОЛИ
            //----------------------------------------------------------------------------------------------------------------------------
            // добавляем роль "roleVerificator" и даём роли разрешение "access_lksv"
            $roleVerificator           = $auth->createRole( 'role_verificator' );
            $roleVerificator->ruleName = $ruleGroup->name;
            $auth->add( $roleVerificator );
            $auth->addChild( $roleVerificator, $accessLksv );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('auth_item', [ 'name' => 'access_lksv' ]);
        $this->delete('auth_item', [ 'name' => 'role_verificator' ]);
        $this->delete('auth_item_child', [ 'parent' => 'role_verificator' ]);

    }


}
