<?php

use yii\db\Migration;

/**
 * Class m180419_135823_insert_to_group_table
 */
class m180419_135823_insert_to_group_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		//0-МП
		$cabinetMP = Yii::$app->db->createCommand( 'SELECT
  *
FROM `cabinets`
WHERE `CODE` IN (
\'3ecc1078f2206cb8fd8d058bba8f64d3\'
)' )
		                          ->queryAll();
		$this->batchInsert( 'groups', [ 'guid', 'name', 'prior_cabinet_id', 'active', 'sort' ], [
			[
				'46b775b8-d8c0-11e7-814a-00155d01bf22',
				'Пользователи мобильного приложения',
				$cabinetMP[0]['id'],
				'1',
				'100',
			],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf22' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180419_135823_insert_to_group_table cannot be reverted.\n";

		return false;
	}
	*/
}
