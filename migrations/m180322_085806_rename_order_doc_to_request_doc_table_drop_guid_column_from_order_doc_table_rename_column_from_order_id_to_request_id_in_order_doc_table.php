<?php

use yii\db\Migration;

/**
 * Class
 * m180322_085806_rename_order_doc_to_request_doc_table_drop_guid_column_from_order_doc_table_rename_column_from_order_id_to_request_id_in_order_doc_table
 */
class m180322_085806_rename_order_doc_to_request_doc_table_drop_guid_column_from_order_doc_table_rename_column_from_order_id_to_request_id_in_order_doc_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->renameTable( 'order_docs', 'request_docs' );
		$this->dropColumn( 'request_docs', 'guid' );
		$this->renameColumn( 'request_docs', 'order_id', 'request_id' );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->addColumn( 'request_docs', 'guid', $this->char( 36 )->notNull()->unique()->after( 'id' )
		                                               ->defaultValue( null ) );
		$this->renameColumn( 'request_docs', 'request_id', 'order_id' );
		$this->renameTable( 'request_docs', 'order_docs' );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180322_085806_rename_order_doc_to_request_doc_table_drop_guid_column_from_order_doc_table_rename_column_from_order_id_to_request_id_in_order_doc_table cannot be reverted.\n";

		return false;
	}
	*/
}
