<?php

use yii\db\Migration;

/**
 * Class m180302_082026_Seed_Employments
 */
class m180302_082026_Seed_Employments extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->batchInsert( 'employments', [ 'guid', 'name', 'active', 'sort' ], [
			[ '03aa730e-dd5b-11e6-811d-00155d01bf07', 'Владелец бизнеса', '1', '200' ],
			[ '0ea570bb-dd5b-11e6-811d-00155d01bf07', 'Руководител', '1', '100' ],
			[ '1b9259c7-dd5b-11e6-811d-00155d01bf07', 'Менеджер', '1', '100' ],
			[ '26b4eff1-dd5b-11e6-811d-00155d01bf07', 'Специалист', '1', '300' ],
			[ '2ff6eb6e-dd5b-11e6-811d-00155d01bf07', 'Рабочий', '1', '100' ],
			[ '387ee754-dd5b-11e6-811d-00155d01bf07', 'Работа на дому (дистанционная работа)', '1', '100' ],
			[ '3e79999a-dd5b-11e6-811d-00155d01bf07', 'Безработный', '1', '100' ],
			[ '45271dbd-dd5b-11e6-811d-00155d01bf07', 'Пенсионер', '1', '600' ],
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->delete( 'employments', [ 'guid' => '03aa730e-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '0ea570bb-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '1b9259c7-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '26b4eff1-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '2ff6eb6e-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '387ee754-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '3e79999a-dd5b-11e6-811d-00155d01bf07' ] );
		$this->delete( 'employments', [ 'guid' => '45271dbd-dd5b-11e6-811d-00155d01bf07' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180302_082026_Seed_Employments cannot be reverted.\n";

		return false;
	}
	*/
}
