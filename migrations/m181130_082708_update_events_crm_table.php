<?php

use yii\db\Migration;

/**
 * Class m181130_082708_update_events_crm_table
 */
class m181130_082708_update_events_crm_table extends Migration
{
    /**
     * @var string - название таблицы
     */
    protected $table = 'events_crm';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update($this->table, ['name' => 'Регистрация в ЛКК'], '[[code]] = 110');
        $this->insert(
            $this->table,
            [
                'code' => '150',
                'guid' => 'e35fd645-f477-11e8-b812-00155d36c907',
                'name' => 'Авторизация в ЛКК',
                'sort' => '105',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update($this->table, ['name' => 'Регистрация/Авторизация в ЛКК'], '[[code]] = 110');
        $this->delete($this->table, '[[code]]=150');
    }
}
