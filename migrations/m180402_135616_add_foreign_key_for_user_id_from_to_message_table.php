<?php

use yii\db\Migration;

/**
 * Class m180402_135616_add_foreign_key_for_user_id_from_to_message_table
 */
class m180402_135616_add_foreign_key_for_user_id_from_to_message_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function safeUp() {

		$this->addForeignKey(
			'fk-message-user_id_from',  // это "условное имя" ключа
			'message', // это название текущей таблицы
			'user_id_from', // это имя поля в текущей таблице, которое будет ключом
			'users', // это имя таблицы, с которой хотим связаться
			'id', // это поле таблицы, с которым хотим связаться
			'CASCADE'
		);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {

		$this->dropForeignKey(
			'fk-message-user_id_from',
			'message'
		);
	}
}
