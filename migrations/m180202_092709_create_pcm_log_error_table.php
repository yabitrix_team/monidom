<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pcm_log_error`.
 */
class m180202_092709_create_pcm_log_error_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'pcm_log_error', [
			'id'     => $this->primaryKey(),
			'log_id' => $this->integer( 4 )->notNull(),
			'type'   => $this->integer( 1 )->notNull(),
			'text'   => $this->text()->notNull(),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'pcm_log_error' );
	}
}
