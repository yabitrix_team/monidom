<?php

use yii\db\Migration;

/**
 * Class m180404_094328_update_to_groups_table
 */
class m180404_094328_update_to_groups_table extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {

		$this->insert( "groups", [
			'guid'   => '46b775b8-d8c0-11e7-814a-00155d01bf1a',
			'name'   => 'Инвесторы',
			'active' => '1',
			'sort'   => '100',
		] );
		$cabinets = Yii::$app->db->createCommand( 'SELECT
  *
FROM cabinets
WHERE GUID IN (
\'caf2f5a8-714b-4c38-3a68-9\',
\'caf2f5a8-714b-4c38-3a68-a\',
\'caf2f5a8-714b-4c38-3a68-b\',
\'caf2f5a8-714b-4c38-3a68-c\'
)' )
		                         ->queryAll();
		//relations
		$this->update( 'groups',
		               [ 'prior_cabinet_id' => $cabinets[0]['id'] ],
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf16\''
		); //Администратор
		$this->update( 'groups', [ 'prior_cabinet_id' => $cabinets[0]['id'] ],  //Партнер менеджер
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf17\''
		);
		$this->update( 'groups', [ 'prior_cabinet_id' => $cabinets[0]['id'] ],  //Пользователь агент
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\''
		);
		$this->update( 'groups', [ 'prior_cabinet_id' => $cabinets[1]['id'] ],  //Клиент
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf19\''
		);
		$this->update( 'groups', [ 'prior_cabinet_id' => $cabinets[3]['id'] ],  //Инвестор
		               'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf1a\''
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {

		$this->update( 'groups', [ 'prior_cabinet_id' => '' ], 'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf16\'' ); //Администратор
		$this->update( 'groups', [ 'prior_cabinet_id' => '' ], 'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf17\'' ); //Партнер менеджер
		$this->update( 'groups', [ 'prior_cabinet_id' => '' ], 'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf18\'' ); //Пользователь агент
		$this->update( 'groups', [ 'prior_cabinet_id' => '' ], 'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf19\'' ); //Клиент
		$this->update( 'groups', [ 'prior_cabinet_id' => '' ], 'guid = \'46b775b8-d8c0-11e7-814a-00155d01bf1a\'' ); //Инвестор
		$this->delete( 'groups', [ 'guid' => '46b775b8-d8c0-11e7-814a-00155d01bf1a' ] );
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m180404_094328_update_to_groups_table cannot be reverted.\n";

		return false;
	}
	*/
}
