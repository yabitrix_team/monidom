<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180131_124527_create_users_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable( 'users', [
			'id'                   => $this->primaryKey(),
			'guid'                 => $this->char( 36 )->notNull()->unique(),
			'username'             => $this->string( 255 )->notNull(),
			'email'                => $this->string( 255 )->notNull(),
			'auth_key'             => $this->string( 255 )->notNull(),
			'password_hash'        => $this->string( 255 )->notNull(),
			'password_reset_token' => $this->string( 255 )->notNull(),
			'point_id'             => $this->integer( 11 )->notNull(),
			'agent_id'             => $this->integer( 11 )->notNull(),
			'first_name'           => $this->string( 255 )->notNull(),
			'last_name'            => $this->string( 255 )->notNull(),
			'second_name'          => $this->string( 255 )->notNull(),
			'is_accreditated'      => $this->char( 1 )->notNull(),
			'inside_support_token' => $this->string( 255 )->notNull(),
			'created_at'           => $this->datetime()->notNull()->defaultExpression( "CURRENT_TIMESTAMP" ),
			'updated_at'           => $this->datetime()->notNull()
			                               ->defaultExpression( "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" ),
			'active'               => $this->char( 1 )->notNull()->defaultValue( "Y" ),
			'sort'                 => $this->integer( 11 )->notNull()->defaultValue( 1000 ),
		] );
	}

	/**
	 * @inheritdoc
	 */
	public function down() {

		$this->dropTable( 'users' );
	}
}
