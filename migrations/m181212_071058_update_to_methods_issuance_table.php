<?php

use yii\db\Migration;

/**
 * Class m181212_071058_update_to_methods_issuance_table
 */
class m181212_071058_update_to_methods_issuance_table extends Migration
{
    protected $table = 'methods_issuance';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update(
            $this->table,
            [
                'description' => 'выпущенную любым банком, кроме Ситибанка',
                'name'        => 'На банковскую карту заёмщика',
            ],
            '[[id]] = 2'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update(
            $this->table,
            [
                'description' => 'Карта принадлежит клиенту. Карта любого банка кроме СИТИбанка.',
                'name'        => 'Безнал. на банковскую карту',
            ],
            '[[id]] = 2'
        );
    }
}
