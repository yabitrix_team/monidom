<?php

use yii\db\Migration;

/**
 * Class m190125_104031_insert_to_versions_dictionaries_table
 */
class m190125_104031_insert_to_versions_dictionaries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert( 'versions_dictionaries', [ 'dictionary', 'version' ], [
            [ 'faq', 1 ],
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete( 'versions_dictionaries', [ 'dictionary' => 'faq' ] );
    }


}
